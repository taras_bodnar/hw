{*
 * Company Otakoyi.com.
 * Author Володимир
 * Date: 21.07.2015
 * Time: 09:24
 * Name: Авторизація успішна
 * Description: Авторизація успішна
 *}
[[chunk:head]]
<div class="l-wrapper">
    [[chunk:header]]
    <main class="l-main" role="main">
        <section class="l-title">
            <div class="row">
                <h1>{$page.name}</h1>
            </div>
        </section><!-- .l-title-page-->
        <section class="l-page-message">
            <div class="container">
                {$page.content}
            </div>
        </section>

    </main><!-- .l-main -->
</div><!-- .l-wrapper -->
[[chunk:footer]]