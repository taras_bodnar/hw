{*
 * Company Otakoyi.com.
 * Author Володимир
 * Date: 16.07.2015
 * Time: 10:51
 * Name: Новини
 * Description: Список новин
 *}{*
 * Company Otakoyi.com.
 * Author Будяк
 * Date: 09.06.2015
 * Time: 12:21
 * Name: main
 * Description: main
 *}
{*
 * Company Otakoyi.com.
 * Author Volodymyr
 * Date: 09.06.2015
 * Time: 12:21
 * Name: main
 * Description: main
 *}
[[chunk:head]]
<div class="l-wrapper">
    [[chunk:header]]
    <main class="l-main" role="main">
        <section class="l-title">
            <div class="row">
                <div class="container"><h1>{$page.name}</h1></div>
            </div>
        </section><!-- .l-title-page-->

        {$page.module}

        <section class="l-page">
            <div class="container">
                <div class="cms_content">
                    {$page.content}
                </div>
                {if $page.isfolder == 0}
                <div class="date">[[mod:News::pubDate]]</div>
                {/if}
            </div>
        </section><!-- .l-title-page-->


    </main><!-- .l-main -->
</div><!-- .l-wrapper -->
[[chunk:footer]]
