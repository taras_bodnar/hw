{*
 * Company Otakoyi.com.
 * Author Володимир
 * Date: 31.08.2015
 * Time: 16:03
 * Name: calendar
 * Description: calendar
 *}

[[chunk:head]]
<div class="l-wrapper">
    [[chunk:header]]
    <main class="l-main" role="main">
        <section class="l-title">
            <div class="row">
                <h1>{$page.name}</h1>
            </div>
        </section><!-- .l-title-page-->
        {if !empty($page.content)}
            <section class="l-page">
                <div class="container">
                    <div class="cms_content">
                        {$page.content}
                    </div>
                </div>
            </section><!-- .l-title-page-->
        {/if}
        {$page.module}
    </main><!-- .l-main -->
</div><!-- .l-wrapper -->
[[chunk:footer]]