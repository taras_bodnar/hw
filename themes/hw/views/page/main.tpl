{*
 * Company Otakoyi.com.
 * Author Volodymyr
 * Date: 09.06.2015
 * Time: 12:21
 * Name: main
 * Description: main
 *}
[[chunk:head]]
<div class="l-wrapper">
    [[chunk:header]]

    <main class="l-main" role="main">
        <section class="l-what-hotwed">
            <div class="container">
                <h1>{$t.main_ab_title}</h1>
                <hr/>
                <div class="cms-content">
                    <p>{$t.main_ab_content}</p>
                </div>
                <a href="2" class="btn png-red">{$t.ab_more}</a>
            </div>
        </section><!-- .l-what-hotwed-->

        [[mod:Catalog::categoriesOnMain]]

        <section class="l-section-register">
            <div class="border">
                <h2>{$t.w_reg_title}</h2>
                <a href="ajax/Users/signUp" class="btn white-green b-register pop-up">{$t.b_register}</a>
                <p>{$t.w_reg_desc}</p>
            </div>
        </section>

        [[mod:News::widget]]

        [[mod:Catalog::mainGallery]]

        [[mod:Blog::widget]]

    </main><!-- .l-main -->
</div><!-- .l-wrapper -->
[[chunk:footer]]