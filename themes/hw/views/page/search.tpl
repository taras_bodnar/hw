{*
 * Company Otakoyi.com.
 * Author Будяк
 * Date: 18.06.2015
 * Time: 09:10
 * Name: search
 * Description: sarch
 *}

[[chunk:head]]
<body class="is not-js">
<div class="main-body">
    <div class="fon-header">
        [[chunk:header]]
        [[mod:Nav::catalog]]
    </div>
    <div class="content">
        [[mod:Breadcrumb]]
        [[mod:Shop::search]]
    </div>
    <div class="h-footer"></div>
</div>
[[chunk:footer]]
</body>
</html>