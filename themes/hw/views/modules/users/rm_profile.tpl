<form action="ajax/Users/deleteProfile" id="deleteProfile" method="post">
    <h3>{$t.del_profile_title}</h3>
    <p style="margin-bottom: 1em;">{$t.del_profile_desc}</p>
    <input type="hidden" name="skey" value="{$skey}">
    <div class="row" id="dpFc">
        <div class="m-form-box">
            <div class="row group-input">
                <label class="input-label" for="comment">{$t.ui_info}</label>
                <div class="input">
                    <textarea id="comment" name="comment" required>{$ui.info}</textarea>
                </div>
            </div>
            <div class="row send">
                <button class="btn red" name="bSubmit2" id="bSubmit2" type="submit">{$t.b_send_request}</button>
            </div>
        </div>
    </div>
</form>