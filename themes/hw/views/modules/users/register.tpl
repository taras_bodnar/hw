<div class="response"></div>
<form id="usersRegister" action="ajax/Users/register" method="post">
    <div class="row input-box">
        <label for="data_email2">{$t.u_email}</label>
        <input type="email" name="data[email]" required id="data_email2" />
    </div>
    <div class="row input-box">
        <label for="data_name">{$t.u_name}</label>
        <input type="text" name="data[name]" required id="data_name" />
    </div>
    <div class="row input-box">
        <label for="data_password2">{$t.u_password}</label>
        <input type="password" name="data[password]" required id="data_password2" />
    </div>
    <div class="row input-box">
        <label for="data_confirm_password" >{$t.u_re_password}</label>
        <input type="password" name="data[confirm_password]" required id="data_confirm_password" />
    </div>
    <div class="row footer">
        <div class="row type">
            {foreach $groups as $i=>$item}
                <label><input {if $i==0}checked{/if} name="data[users_group_id]" type="radio" value="{$item.id}" /><span>{$item.name}</span></label>
            {/foreach}
        </div>
        <div class="row regulations">
            <label><input type="checkbox" name="confirm" required id="confirm" /><span>{$t.confirm_rules}</span></label>
        </div>
        <div class="row">
            <button class="btn red b-submit">{$t.b_register}</button>
        </div>
    </div>
    <input type="hidden" name="skey" value="{$skey}" >
    <input type="hidden" name="process" value="1" >
</form>