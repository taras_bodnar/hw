<div class="l-personal-information">
    <div class="container">
        <form method="post" action="ajax/Users/selectGroup" id="selectGroup">

            <div class="m-form-box">
                <div class="row group-input">
                    <label class="input-label" for="users_group_id">{$t.ui_select_group}</label>
                    <div class="input">
                        <select name="gid" id="users_group_id">
                            <option value="3">{$t.ui_client}</option>
                            {foreach $group as $item}
                                <option {if $ui.users_group_id == $item.id}selected{/if} value="{$item.id}">{$item.name}</option>
                            {/foreach}
                        </select>
                    </div>
                </div>
                <div class="row send">
                    <button class="btn red" type="submit">{$t.b_save}</button>
                </div>
            </div>
            <input type="hidden" name="skey" value="{$skey}">
            <div class="row">
                <div class="response"></div>
            </div>
        </form>
    </div>
</div>