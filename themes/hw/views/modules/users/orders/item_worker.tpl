{*<pre>{print_r($item)}</pre>*}
<div id="o-item-{$item.id}" class="item {if $item.status == 'pending'}new{/if}">
    <div class="column-l">
        <div class="row">
            <div class="item-header">
                <div class="date-user">
                    <div class="name"><a href="{if $item.type='user'}45;p={$item.users_id}{/if}">{$item.username}</a></div>
                    <div class="phone">
                        <i class="icon-phone"></i>
                        <span>{$item.phone}</span>
                    </div>
                    <div class="email"><i class="icon-mail"></i><span>{$item.email}</span></div>
                </div>
                <div class="info-message">
                    <ul class="date-time">
                        <li>
                            <i class="icon-calendar"></i>
                            <span>{$item.dd}</span>
                        </li>
                        <li>
                            <i class="icon-clok"></i>
                            <span>{$item.hh}</span>
                        </li>
                    </ul>
                </div>
                <div class="mark">{$t.new}</div>
            </div>
        </div>
        <div class="row">
            <div class="item-content">
                <div class="text">
                    <p>
                        {str_replace(array('{odate}','{amount}', '{termin}'), array($item.odate, $item.amount, $item.termin), ($item.utype == 'worker') ? $t.user_o_am_text : $t.o_user_o)}
                    </p>
                </div>
            </div>
        </div>
        {if $item.status == 'pending'}
        <div class="row">
            <div class="item-footer">
                <div class="col-4">
                    <a href="50;p={$item.users_id}" class="btn icons-png-red icons-open_mail"><span>{$t.write_msg}</span></a>
                </div>
                <div class="col-6">
                    <div class="btn-group">
                        <a class="btn icons-png-red icons-calendar" href="9"><span>{$t.calendar}</span></a>
                        <a class="btn green b-change-status" href="#" data-action="dismiss" data-id="{$item.id}">{$t.dismiss}</a>
                        <a class="btn red b-change-status" href="#" data-action="confirmed" data-id="{$item.id}">{$t.confirm}</a>
                    </div>
                </div>
            </div>
        </div>
        {/if}
    </div>
    {if $item.status == 'confirmed'}
        <div class="column-r"><div class="status"></div></div>
    {/if}
    {if $item.status == 'dismiss'}
        <div class="column-r"><div class="status unconfirmed"></div></div>
    {/if}
</div><!-- .item -->