<div id="o-item-{$item.id}" class="item {if $item.status == 'pending'}considered{/if}">
    <div class="item-header">
        <div class="mark">{$t.pending}</div>
        <div class="description">
            {str_replace(array('{odate}','{amount}', '{termin}'), array($item.odate, $item.amount, $item.termin), $t.o_user_o)}
        </div>
    </div>
    <div class="item-content">
        <div class="column-l">
            <div class="user">
                <div class="img-autor">
                    <a href="45;p={$item.users_id}">
                        <img alt="" src="{$item.avatar}">
                    </a>
                </div>
                <div class="description-autor">
                    <div class="name">
                        <a href="45;p={$item.users_id}">{$item.username}</a>
                    </div>
                    <div class="directions">
                        <a href="{$item.content_id}">{$item.group_name}</a>
                    </div>
                    <div class="town">
                        <a href="3;filter[city_id]={$item.city_id}">
                            <span>{$item.city}</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="column-r">
            {if $item.status == 'confirmed'}
                <div class="status"></div>
            {elseif $item.status == 'dismiss' || $item.status == 'canceled'}
                <div class="status unconfirmed"></div>
            {elseif $item.status == 'pending'}
            <div class="btn-group">
                <a href="50;p={$item.users_id}" class="btn icons-png-red icons-open_mail"><span>{$t.write_msg}</span></a>
                <a class="btn green b-change-status" href="#" data-action="canceled" data-id="{$item.id}">{$t.cancel}</a>
            </div>
            {/if}
        </div>
    </div>
</div>