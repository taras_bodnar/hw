<div class="l-message l-notification">
    <div class="container">
        <div class="header">
            <div class="info">{$title}</div>
            <br>
        </div>
        <div class="content" id="notifications">{$items}</div>
        <div class="footer">
            <div class="col-3">
                {if $total>0}
                    <a class="btn icons-png-red icons-around b-more-notify" data-p="{$start}" href="{$root_id}">
                        <span>{$t.b_news_more}</span>
                    </a>
                {/if}
            </div>
            <div class="col-9 m-pagination" id="pagination">
                {$pagination}
            </div>
        </div>
    </div>
</div>