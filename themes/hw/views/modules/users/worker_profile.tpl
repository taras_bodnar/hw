<div class="l-page-personal-information">
    <div class="container">
         {*<pre>{print_r($ui)}</pre>*}
            
            <div class="row">
                <div class="m-form-box">
                    <div class="column-l">
                    <form method="post" name="usersProfile" action="ajax/Users/profile" id="usersProfile">
                        <div class="row group-input">
                            <label for="data_surname" class="input-label">{$t.ui_surname}</label>
                            <div class="input tooltip-right" title="{$t.profile_pib_info}">
                                <input type="text" data-error="{$t.e_user_surname}" name="data[surname]" id="data_surname" required value="{$ui.surname}">
                            </div>
                        </div>
                        <div class="row group-input">
                            <label for="data_name" class="input-label">{$t.ui_name}</label>
                            <div class="input">
                                <input class="m-tooltip" data-error="{$t.e_user_surname}" name="data[name]" id="data_name" required type="text" value="{$ui.name}" >
                            </div>
                        </div>
                        <div class="row group-input">
                            <label for="data_email" class="input-label">{$t.ui_email}</label>
                            <div class="input">
                                <input type="email" data-error="{$t.user_login_e_bad_email}" name="data[email]" required id="data_email" value="{$ui.email}">
                            </div>
                        </div>
                        <div class="row group-input">
                            <label for="data_phone" class="input-label">{$t.ui_phone}</label>
                            <div class="input">
                                <input type="text" name="data[phone]" required id="data_phone" value="{$ui.phone}">
                            </div>
                        </div>
                        <div class="row group-input">
                            <label class="input-label " for="users_group_id">{$t.ui_group}</label>
                            <div class="input">
                                <select name="data[users_group_id]" id="users_group_id">
                                    <option value=""></option>
                                    {foreach $group as $item}
                                        <option {if $ui.users_group_id == $item.id}selected{/if} value="{$item.id}">{$item.name}</option>
                                    {/foreach}
                                </select>
                            </div>
                        </div>
                        <div class="row group-input">
                            <label class="input-label" for="data_city_id">{$t.ui_city}</label>
                            <div class="input">
                                <select id="data_city_id" name="data[city_id]">
                                    <option value=""></option>
                                    {foreach $city as $item}
                                        <option {if $ui.city_id == $item.id}selected{/if} value="{$item.id}">{$item.name}</option>
                                    {/foreach}
                                </select>
                            </div>
                        </div>

                        <div class="row group-input" >
                            <label class="input-label" for="currency_id">{$t.sel_currency}</label>
                            <div class="input">
                                <select id="data_currency_id" name="data[currency_id]">
                                    <option value=""></option>
                                    {foreach $currency as $item}
                                        <option {if $ui.currency_id == $item.id}selected{/if} value="{$item.id}">{$item.code} ({$item.symbol})</option>
                                    {/foreach}
                                </select>
                            </div>
                        </div>
                        <div class="row" id="default-prices">
                            <div class="row group-input" >
                                <label class="input-label" for="price_per_hour">{$t.price_per_hour}</label>
                                <div class="input">
                                    <input type="text" id="price_per_hour" class="tooltip-right" title="{$t.i_price_per_day}" name="data[price_per_hour]" value="{$ui.price_per_hour}" required>
                                </div>
                            </div>
                            <div class="row group-input">
                                <label class="input-label" for="price_per_day">{$t.price_per_day}</label>
                                <div class="input">
                                    <input type="text" id="price_per_day" name="data[price_per_day]" value="{$ui.price_per_day}" required>
                                </div>
                            </div>
                        </div>
                        <div class="row group-input" id="p_per_service" style="display: none;" >
                            <label class="input-label" for="price_per_service">{$t.p_per_service}</label>
                            <div class="input">
                                <input type="text" id="price_per_service" name="data[price_per_day]" value="{$ui.price_per_day}" required>
                            </div>
                        </div>
                        <div class="row group-input" id="p_per_place" style="display: none;" >
                            <label class="input-label" for="price_per_place">{$t.p_per_place}</label>
                            <div class="input">
                                <input type="text" id="price_per_place" name="data[price_per_day]" value="{$ui.price_per_day}" required>
                            </div>
                        </div>

                        <div class="row group-input">
                            <label class="input-label">{$t.discount_per_week}</label>
                            <div class="input">
                                {foreach $discounts as $item}
                                    <label><input {if $ui.discount_per_week == $item.name}checked{/if} name="data[discount_per_week]" type="radio" value="{$item.name}"><span>{$item.name}</span></label>
                                {/foreach}
                            </div>
                        </div>
                        <div class="row group-input">
                            <label class="input-label">{$t.discount_per_month}</label>
                            <div class="input">
                                {foreach $discounts as $item}
                                    <label><input {if $ui.discount_per_month == $item.name}checked{/if} name="data[discount_per_month]" type="radio" value="{$item.name}"><span>{$item.name}</span></label>
                                {/foreach}
                            </div>
                        </div>
                        <div class="row group-input">
                            <label class="input-label">{$t.discount_per_2_month}</label>
                            <div class="input">
                                {foreach $discounts as $item}
                                    <label><input {if $ui.discount_per_2_month == $item.name}checked{/if}  name="data[discount_per_2_month]" type="radio" value="{$item.name}"><span>{$item.name}</span></label>
                                {/foreach}
                            </div>
                        </div>

{*
                        <div class="row group-input">
                            <label for="data_phone" class="input-label">Facebook ID</label>
                            <div class="input tooltip-right" title="{$t.facebook_id}">
                                <input type="text" name="oauth[facebook]"  id="oauth_facebook" value="{$ui.oauth.facebook}">
                            </div>
                        </div>
                        <div class="row group-input">
                            <label for="data_phone" class="input-label">VK ID</label>
                            <div class="input tooltip-right" title="{$t.vk_id}">
                                <input type="text" name="oauth[vk]"  id="oauth_vk" value="{$ui.oauth.vk}">
                            </div>
                        </div>
                        <div class="row group-input">
                            <label for="data_phone" class="input-label">Google+ ID</label>
                            <div class="input tooltip-right" title="{$t.google_id}">
                                <input type="text" name="oauth[google]"  id="oauth_google" value="{$ui.oauth.google}">
                            </div>
                        </div>
*}
                        <div class="row group-input">
                            <label class="input-label" for="data_site">{$t.ui_site}</label>
                            <div class="input">
                                <input type="text" id="data_site" name="data[site]" value="{$ui.site}">
                            </div>
                        </div>
                        <div class="row group-input">
                            <label class="input-label" for="data_blog">{$t.ui_blog}</label>
                            <div class="input">
                                <input type="text" id="data_blog" name="data[blog]" value="{$ui.blog}" >
                            </div>
                        </div>
                        <div class="row group-input">
                            <label class="input-label" for="data_facebook">Facebook</label>
                            <div class="input">
                                <input type="text" id="data_facebook" name="data[facebook]" value="{$ui.facebook}" >
                            </div>
                        </div>
                        <div class="row group-input">
                            <label class="input-label" for="data_vk">Vk</label>
                            <div class="input">
                                <input type="text" id="data_vk" name="data[vk]" value="{$ui.vk}">
                            </div>
                        </div>
                        <div class="row group-input">
                            <label class="input-label" for="data_instagram">Instaram</label>
                            <div class="input">
                                <input type="text" name="data[instagram]" id="data_instagram" value="{$ui.instagram}">
                            </div>
                        </div>
                        <div class="row group-input">
                            <label class="input-label" for="data_info">{$t.ui_info}</label>
                            <div class="input">
                                <textarea id="data_info" name="data[info]" >{$ui.info}</textarea>
                            </div>
                        </div>

                        <div class="row send">
                            <button class="btn red" name="bSubmit" id="bSubmit" type="submit">{$t.b_save}</button>
                        </div>

                        <input type="hidden" name="skey" value="{$skey}">
                        <input type="hidden" name="process" value="1">
                        <input type="hidden" name="gid" value="{$ui.users_group_id}">
                        </form>
                    </div>
                    <div class="column-r">
                        <form method="post" enctype="multipart/form-data" name="usersAvatar" action="ajax/Users/avatar" id="usersAvatar">
                            <div class="m-upload-photo">
                                <input type="file" name="avatar" id="data_avatar" data-img="{$ui.avatar}" onchange="document.usersAvatar.bbSubmit.click();" />
                            </div>
                            <input type="hidden" name="skey" value="{$skey}">
                            <button class="btn red" style="display: none;" name="bbSubmit" id="bbSubmit" type="submit"></button>

                        </form>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="response"></div> 
            </div>
        {$rm_profile}
    </div>
</div>
