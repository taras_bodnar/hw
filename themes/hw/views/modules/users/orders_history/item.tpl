<div id="o-item-{$item.id}" class="item">
    <div class="item-header">
        <div class="description-reservation">
            {str_replace(array('{odate}','{amount}', '{termin}'), array($item.odate, $item.amount, $item.termin), $t.o_user_o)}
        </div>
        <ul class="date-time">
            <li>
                <i class="icon-calendar"></i>
                <span>{$item.dd}</span>
            </li>
            <li>
                <i class="icon-clok"></i>
                <span>{$item.hh}</span>
            </li>
        </ul>
    </div>
    <div class="item-content">
        <div class="column-l">
            <div class="img-autor">
                <a href="45;p={$item.id}">
                    <img alt="" src="{$item.avatar}">
                </a>
            </div>
            <div class="description-autor">
                <div class="name"><a href="45;p={$item.id}">{$item.username}</a></div>
                <div class="row">
                    <div class="directions">
                        <a href="{$item.content_id}">{$item.group_name}</a>
                    </div>
                    <div class="town">
                        <a href="3;filter[city_id]={$item.city_id}">
                            <span>{$item.city}</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="column-r">
            <a href="50;p={$item.users_id}" class="btn icons-png-red icons-open_mail"><span>{$t.write_msg}</span></a>
        </div>
    </div>
</div><!-- .item -->