<div class="signup">
    <div class="tabs">

        <ul class="tabs__caption">
            <li class="active"><span>{$t.tab_login}</span></li>
            <li><span>{$t.tab_register}</span></li>
        </ul>

        <div class="tabs__content active">
            {$login}
        </div>

        <div class="tabs__content registration">
            {$register}
        </div>
    </div><!-- .tabs-->
</div>
<script>new Users();</script>