<div class="item">
    <div class="item-header">
        <div class="name"><a href="45;p={$item.id}{if $smarty.get.filter.df};filter[df]={$smarty.get.filter.df}{/if}">{$item.name} {$item.surname}</a></div>
        <div class="town">
            <a href="3;filter[city_id]={$item.city_id}{if $smarty.get.filter.df}&filter[df]={$smarty.get.filter.df}{/if}">
                <span>{$item.city}</span>
            </a>
        </div>
    </div>
    <div class="item-content">
        <div class="author">
            <div class="row">
                <div class="column-l">
                    <div class="img-autor">
                        <a href="45;p={$item.id}{if $smarty.get.filter.df};filter[df]={$smarty.get.filter.df}{/if}">
                            <img src="{if $item.avatar == ''}/themes/hotwed/assets/img/avatar.jpg{else}{$item.avatar}{/if}" alt=""/>
                        </a>
                    </div>
                    <div class="number-comments">
                        <span>{$item.comments}</span><i class="icon-massage"></i>
                    </div>
                </div>
                <div class="column-r">
                    <div class="directions"><a href="{$item.content_id}{if $smarty.get.filter.df};filter[df]={$smarty.get.filter.df}{/if}">{$item.group_name}</a></div>

                        {if $item.content_id == 64}
                            <div class="price-day"><span>{$t.item_per_place}</span><abbr title="{$t.sprice}{$item.sppd}%">{$item.price_per_day}{$item.symbol}</abbr></div>
                        {elseif $item.content_id == 42}
                            <div class="price-day"><span>{$t.item_per_service}</span><abbr title="{$t.sprice}{$item.sppd}%">{$item.price_per_day}{$item.symbol}</abbr></div>
                        {else}
                            <div class="price-day"><span>{$t.price_per_d}</span><abbr title="{$t.sprice}{$item.sppd}%">{$item.price_per_day}{$item.symbol}</abbr></div>
                        {/if}

                        {if $item.price_per_hour > 0}
                            <div class="price-hour"><span>{$t.price_per_h}</span><abbr title="{$t.sprice}{$item.spph}%">{$item.price_per_hour}{$item.symbol}</div>
                        {/if}

                </div>
            </div>
        </div>
        <div class="portfolio">
            {foreach $item.img as $img}
            <div class="img">
                <a class="pf-items {if $img.type=='video'} video fancybox.iframe{/if}" rel="group{$item.id}" href="{$img.big}">
                    <span>
                         <img class="lazy" data-src="{$img.thumb}" src="" alt=""/>
                    </span>
                </a>
            </div><!-- .img -->
            {/foreach}
        </div>
    </div>
</div><!-- .item -->