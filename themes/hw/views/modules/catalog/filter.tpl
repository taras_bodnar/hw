<form method="get" action="3" id="catalogFilter">
    <div class="m-filter-people">
        <div class="filter-content">
            <div class="filter-border">
                <div class="col-4">
                    <div class="title">{$t.catalog_f_date}</div>
                    <div class="date" id="datepickerBox">
                        <div class="bg-input" >
                            <input type="text" required name="filter[df]" id="datepicker" readonly value="{$smarty.get.filter.df}">
                            <i class="icon-calendar_2"></i>
                        </div>
                    </div>
                </div><!-- .col-4 -->
                <div class="col-4">
                    <div class="title">{$t.catalog_f_ug}</div>
                    <select id="filter_gid" >
                        <option value="" data-href="3"></option>
                        {foreach $group as $item}
                            <option data-href="{$item.content_id}" {if $page.id == $item.content_id}selected{/if} value="{$item.id}">{$item.name}</option>
                        {/foreach}
                    </select>
                </div><!-- .col-4 -->
                <div class="col-4">
                    <div class="title">{$t.catalog_f_city}</div>
                    <select name="filter[city_id]" id="filter_city_id" >
                        <option value=""></option>
                        {foreach $city as $item}
                            <option {if $smarty.get.filter.city_id == $item.id}selected{/if} value="{$item.id}">{$item.name}</option>
                        {/foreach}
                    </select>
                </div><!-- .col-4 -->
            </div>
        </div>
        <div class="footer">
            <div class="row">
                <button class="btn red" type="submit">{$t.b_filter}</button>
                {if $page.id != 1 && $smarty.get.filter}
                <a  href="3" class="reset"><span>{$t.b_filter_reset}</span><i class="icon-clancel_circle"></i></a>
                {/if}
            </div>
        </div>
    </div><!-- .m-filter-people -->
</form>