<main class="l-main" role="main">
    <section class="l-title">
        <div class="row">
            <div class="container">
                <h1>{$t.catalog_title}</h1>
               [[mod:Catalog::filter]]
            </div>
        </div>
    </section><!-- .l-title-page-->

    <section class="l-people-list">
        <div class="container">
            <form class="header" action="">
                <div class="col-9">
                    {if $smarty.get.q}
                    <h5>{str_replace('{total}', $t_total, $t.search_total)}</h5>
                    {/if}
                    &nbsp; 
 
                </div>
                <div class="col-3">
                    <select name="order" onchange="submit();"> 
                        {foreach $sorting as $item}
                            <option {if isset($smarty.get.order) && $smarty.get.order == $item.id}selected{/if} value="{$item.id}">{$item.name}</option>
                        {/foreach}
                    </select>
                </div>
                {if isset($smarty.get.filter.df) && $smarty.get.filter.df != ''}
                    <input type="hidden" name="filter[df]" value="{$smarty.get.filter.df}">
                {/if}
                {if isset($smarty.get.filter.city_id) && $smarty.get.filter.city_id != ''}
                    <input type="hidden" name="filter[city_id]" value="{$smarty.get.filter.city_id}">
                {/if}
                {if isset($smarty.get.q) && $smarty.get.q != ''}
                    <input type="hidden" name="q" value="{$smarty.get.q}">
                {/if}
            </form>
            <div class="content" id="catalogWorkers">{$items}</div>
            <div class="footer">
                <div class="col-3">
                    {if $total>0}
                    <a class="btn icons-png-red icons-around b-more-workers" data-p="{$start}" href="{$root_id}">
                        <span>{$t.b_catalog_more}</span>
                    </a>
                    {/if}
                </div>
                <div class="col-9 m-pagination" id="pagination">
                    {$pagination}
                </div>
            </div>
        </div>
    </section>



</main><!-- .l-main -->