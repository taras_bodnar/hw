<form id="order" action="ajax/Orders/process" method="post">
    <div class="calendar">
        <div id="odate"></div>
        <div class="selected-date">
            <i class="icon-calendar"></i>
            <span class="title">{$t.w_selected_date}</span>
            <span class="date">{$worker.odate}</span>
        </div>
    </div>

    <div class="pricehour">
        <div class="b-1">
            {if $worker.users_group_id == 12}
                <div class="price-hour">
                    <i class="icon-clok_arrow"></i>
                    <span class="title">{$t.item_per_place}</span>
                    <span class="price" title="{$t.sprice}{$user.sppd}%">{round($worker.price_per_day, 2)} {$worker.symbol}</span>
                </div>
            {elseif $worker.users_group_id == 8}

                <div class="price-hour">
                    <i class="icon-clok_arrow"></i>
                    <span class="title">{$t.item_per_service}</span>
                    <span class="price" title="{$t.sprice}{$user.sppd}%">{round($worker.price_per_day, 2)} {$worker.symbol}</span>
                </div>

            {else}
                <div class="price-hour">
                    <i class="icon-clok_arrow"></i>
                    <span class="title">{$t.price_per_d}</span>
                    <span class="price" title="{$t.sprice}{$user.sppd}%">{round($worker.price_per_day, 2)} {$worker.symbol}</span>
                </div>
            {/if}

            {if $worker.price_per_hour > 0}
                <div class="price-day">
                    <i class="icon-hourglass"></i>
                    <span class="title">{$t.price_per_h}</span>
                    <span class="price" title="{$t.sprice}{$user.spph}%">{round($worker.price_per_hour, 2)} {$worker.symbol}</span>
                </div>
            {/if}
        </div>
        <div class="b-2">
            <div class="text-1">{$t.discount_info}</div>
            <div class="text-2">
                {$t.dpw} - <strong>{$worker.discount_per_week}%</strong>
                {$t.dpm} - <strong>{$worker.discount_per_month}%</strong>
                {$t.dp2m} - <strong>{$worker.discount_per_2_month}%</strong>
            </div>
        </div>
        <div class="b-3">
            <div class="text-1">
                {$t.worker_amount}
            </div>
            <div class="text-2">

                {if $worker.users_group_id == 12}
                    <div class="price-hour">
                        <i class="icon-clok_arrow"></i>
                        <span class="title">{$t.item_per_place}</span>
                        <span class="price" title="{$t.sprice}{$user.sppd}%">{round($worker.calc_price_per_day, 2)} {$worker.symbol}</span>
                    </div>
                {elseif $worker.users_group_id == 8}

                    <div class="price-hour">
                        <i class="icon-clok_arrow"></i>
                        <span class="title">{$t.item_per_service}</span>
                        <span class="price" title="{$t.sprice}{$user.sppd}%">{round($worker.calc_price_per_day, 2)} {$worker.symbol}</span>
                    </div>

                {else}
                    <div class="price-hour">
                        <i class="icon-clok_arrow"></i>
                        <span class="title">{$t.price_per_d}</span>
                        <span class="price" title="{$t.sprice}{$user.sppd}%">{round($worker.calc_price_per_day, 2)} {$worker.symbol}</span>
                    </div>
                {/if}

                {if $worker.calc_price_per_hour > 0}

                    <div class="price-day">
                        <i class="icon-hourglass"></i>
                        <span class="title">{$t.price_per_h}</span>
                        <span class="price">{round($worker.calc_price_per_hour, 2)} {$worker.symbol}</span>
                    </div>
                {/if}
            </div>
        </div>
        <div class="b-4">
            <div class="term-services">
                {if !in_array($worker.users_group_id, array(8, 12))}
                <div class="row">
                    <label class="title">{$t.worker_per}</label>
                </div>
                {/if}
                <div class="row">

                    {if $worker.users_group_id == 12}
                        <input name="data[termin]" value="day" type="hidden" />
                    {elseif $worker.users_group_id == 8}
                        <input name="data[termin]" value="day" type="hidden" />
                    {else}
                        {if $worker.price_per_hour > 0}
                            <label class="radio"><input name="data[termin]" id="data_period_hour" value="hour" class="period" type="radio"/><span>{$t.price_per_h}</span></label>
                        {/if}
                        <label class="radio"><input checked name="data[termin]" id="data_period_day" value="day" class="period" type="radio"/><span>{$t.price_per_d}</span></label>
                    {/if}
                </div>
            </div>
            {if $can_order}
            <div class="form-send">
                <button class="btn red" type="submit" id="bSubmit">{$t.b_order}</button>
            </div>
            {/if}
        </div>
    </div>
    <input type="hidden" name="skey" value="{$skey}">
    <input type="hidden" name="data[workers_id]" id="worker_id" value="{$worker.id}">
    <input type="hidden" name="data[odate]" value="{$worker.odate}">
</form>