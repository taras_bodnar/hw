<div class="m-main-menu">
    <div class="button-main-menu">
        <i></i>
        <i></i>
        <i></i>
    </div>
    <div class="main-menu">
        <a href="#" onclick="return false;" class="close-main-menu">
            <i class="icon-cancel"></i>
        </a>
        <div class="table">
            <div class="table-cell">
                <ul class="menu-list level-1"> 
                    {foreach $items as $i=>$item}
                        <li>
                            <a href="{$item.id}" title="{$item.title}">{$item.name}</a>
                        </li>
                    {/foreach}
                    <li>
                        <div class="m-lang">
                            <ul class="lang">
                                {foreach $languages as $item}
                                    <li>
                                        <a class="{if $languages_id == $item.id }active{/if}" href="{$page.id};l={$item.id};{if $p};p={$p}{/if}">{$item.name}</a>
                                    </li>
                                {/foreach}
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="m-lang">
    <ul class="lang">
        {foreach $languages as $item}
            <li>
                <a class="{if $languages_id == $item.id }active{/if}" href="{$page.id};l={$item.id};{if $p};p={$p}{/if}">{$item.name}</a>
            </li>
        {/foreach}
    </ul>
</div>