<div class="m-main-menu">
    <div class="button-main-menu">
        <i></i>
        <i></i>
        <i></i>
    </div>
    <div class="main-menu">
        <a href="#" onclick="return false;" class="close-main-menu">
            <i class="icon-cancel"></i>
        </a>
        <div class="table">
            <div class="table-cell">
                <ul class="menu-list h100 level-1">
                    {foreach $items as $i=>$item}
                        <li {if $item.isfolder}class="dropdown"{/if}>
                            <a href="{$item.id}" title="{$item.title}">{$item.name}</a>
                            {if $item.isfolder}
                                <ul class="level-2">
                                    {foreach $item.children as $c}
                                    <li><a href="{$c.id}">{$c.name}</a></li>
                                    {/foreach}
                                </ul>
                            {/if}
                        </li>
                    {/foreach}
                    <li>
                        <div class="m-lang">
                            <ul class="lang">
                                {foreach $languages as $item}
                                    <li>
                                        <a class="{if $languages_id == $item.id }active{/if}" href="{$page.id};l={$item.id};">{$item.name}</a>
                                    </li>
                                {/foreach}
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="m-lang">
    <ul class="lang">
        {foreach $languages as $item}
            <li>
                <a class="{if $languages_id == $item.id }active{/if}" href="{$page.id};l={$item.id};">{$item.name}</a>
            </li>
        {/foreach}
    </ul>
</div>