<ul class="lang">
    {foreach $items as $item}
        <li>
            <a class="{if $id == $item.id }active{/if}" href="{$page.id};l={$item.id};">{$item.name}</a>
        </li>
    {/foreach}
</ul>