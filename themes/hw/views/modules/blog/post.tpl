<section class="info-page">
    <div class="container-fixed-width has-padding">
        <div class="text-container cms_content">
            <h1>{$page.name}</h1>
            {$page.content}

            <div class="social">
                <div class="likes facebook">
                    <a href="#"></a>
                    <span>0</span>
                </div>
                <div class="likes vk">
                    <a href="#"></a>
                    <span>+1</span>
                </div>
                <div class="likes google">
                    <a href="#"></a>
                    <span>0</span>
                </div>
            </div>
        </div>

        <div class="content-nav">
            <aside>
                <h2>{$t.read_more_old}</h2>
                <ul class="nav-list">
                    {foreach $items as $item}
                        <li><a href="{$item.id}" title="{$item.title}">{$item.name}</a></li>
                    {/foreach}
                </ul>
                <a href="5" class="btn-small-blue">{$t.all_news}</a>
            </aside>
        </div>
    </div>
</section>