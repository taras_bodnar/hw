<section class="l-page-dialog">
    <div class="container">
        <div class="header">
            <a href="#" onclick="self.history.back(-1); return false;" class="btn icons-png-red arrow-left"><span>{$t.b_self_back}</span></a>
            {* <a href="#" class="m-star m-tooltip tooltip-top" title="Для уточнення деталей ви можете написати листа виконвцю">
                <i class="icon-star_1"></i>
            </a> *}
        </div>
        <div class="content" id="messages">{$items}</div>
        <div class="footer">
            <form id="sendMessage" action="ajax/Messages/send" method="post">
                <div class="send-message">
                    <div class="row">
                        <textarea id="data_message" placeholder="{$t.message_placeholder}" required name="data[message]"></textarea>
                    </div>
                    <div class="row">
                        <button title="CTRL+ENTER" class="btn red" id="bSubmit" type="submit">{$t.b_send}</button>
                    </div>
                </div>
                <input type="hidden" name="skey" value="{$skey}">
                <input type="hidden" name="data[to_id]" id="to_id" value="{$to_id}">
            </form>
        </div>
    </div>
</section><!-- .l-title-page-->