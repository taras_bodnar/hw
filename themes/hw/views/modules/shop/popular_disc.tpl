<div class="main-row clearfix has-filter">
    <h1>{$t.disc_popular}</h1>
    <div class="product-content" id="pc2">

        <div class="slider-navigation">
            <a href="8" class="all">{$t.link_all_disc}</a>
            <a class="btn prev"></a>
            <span></span>
            <a class="btn next"></a>
        </div>
        <div class="owl-carousel owl-theme">
            {$items}
        </div>
    </div>
    <aside class="side-filter">
        <h6>{$t.link_brand_disc}:</h6>
        {foreach $vendors as $row}
        <ul class="brand-links">
            {foreach $row as $item}
            <li><a href="8;filter[f][9][]={$item.id}">{$item.name}</a></li>
            {/foreach}
        </ul>
        {/foreach}
        <a href="27">{$t.filter_all_vendors}</a>
    </aside>
</div>