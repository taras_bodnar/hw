<div class="main-row clearfix">
    <h2>{$t.products_viewed}</h2>
    <div class="product-content" id="pc3">

        <div class="slider-navigation">
            <a href="#" class="all">{$t.view_all}</a>
            <a class="btn prev"></a>
            <span></span>
            <a class="btn next" ></a>
        </div>

        <div class="owl-carousel owl-theme">
            {$viewed_items}
        </div>
    </div>
</div>