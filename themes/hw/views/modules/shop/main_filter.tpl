<section class="main-filter">
    <div class="container-fixed-width">
        <div class="tab-container main">
            <ul class='etabs large'>
                <li class='tab tab-main'>
                    <a href="#tires">
                        <div class="nav-image left-img"></div>
                        <span>{$t.link_tires}</span>
                    </a>
                </li>
                <li class='tab tab-main'>
                    <a href="#disc">
                        <div class="nav-image right-img"></div>
                        <span>{$t.link_disc}</span>
                    </a>
                </li>
            </ul>
            <div class="panel-container pan-lg">
                <div id="tires">
                    <div class="tab-container level-2">
                        <div class="tabs-title">
                            <span>{$t.sel_tires}</span>
                        </div>
                        <ul class='etabs'>
                            <li class='tab'><a href="#tab-d"><span>{$t.sel_by_params}</span></a></li>
                            <li class='tab'><a href="#tab-e"><span>{$t.sel_by_auto}</span></a></li>
                            <li class='tab'><a href="#tab-f"><span>{$t.sel_by_vendor}</span></a></li>
                        </ul>
                        <div class="link-to-all">
                            <a href="7"><span>{$t.link_all_tires}</span></a>
                        </div>
                        <div class="panel-container">
                            <div id="tab-d">
                                <!--filter-->
                                <form class="form-filter" action="7" method="get">
                                    {if isset($tires.width)}
                                    <div class="each-el-box">
                                        <span>{$tires.width.name}:</span>
                                        <label class="lb width"></label>
                                        <select data-placeholder="{$t.filter_sel_all}" name="filter[f][{$tires.width.id}]">
                                            <option value="">{$t.filter_sel}</option>
                                            {foreach $tires.width.values as $item}
                                                <option value="{$item.id}">{$item.value}</option>
                                            {/foreach}
                                        </select>
                                    </div>
                                    {/if}
                                    {if isset($tires.height)}
                                    <div class="each-el-box">
                                        <span>{$tires.height.name}:</span>
                                        <label class="lb profil"></label>
                                         <select data-placeholder="{$t.filter_sel_all}" name="filter[f][{$tires.height.id}]">
                                            <option value="">{$t.filter_sel}</option>
                                            {foreach $tires.height.values as $item}
                                                <option value="{$item.id}">{$item.value}</option>
                                            {/foreach}
                                        </select>
                                    </div>
                                    {/if}
                                    {if isset($tires.diametr)}
                                    <div class="each-el-box">
                                        <span>{$tires.diametr.name}:</span>
                                        <label class="lb diametr"></label>
                                         <select data-placeholder="{$t.filter_sel_all}" name="filter[f][{$tires.diametr.id}]">
                                            <option value="">{$t.filter_sel}</option>
                                            {foreach $tires.diametr.values as $item}
                                                <option value="{$item.id}">{$item.value}</option>
                                            {/foreach}
                                        </select>
                                    </div>
                                    {/if}
                                    {if isset($tires.season)}
                                    <div class="each-el-box">
                                        <span>{$tires.season.name}:</span>
                                            {foreach $tires.season.values as $item}
                                                <input  type="radio" name="filter[f][{$tires.season.id}]"
                                                        id="r-{$item.id}" value="{$item.id}"
                                                        class="
                                                         {if $item.id==10}rad-winter{/if}
                                                         {if $item.id==11}rad-summer{/if}
                                                         rad-{$item.id}
                                                         ">
                                                <label for='r-{$item.id}' class="radio">{$item.value}</label>
                                            {/foreach}
                                        </div>
                                    {/if}
                                    <div class="each-el-box btn-box">
                                        <button type="submit" class="btn-mid-blue">{$t.btn_filter}</button>
                                    </div>
                                </form>
                                <!--end-->
                            </div>
                            <div id="tab-e">
                                <!--filter-->
                                <form class="form-filter no-label" method="get" action="7">
                                    <div class="each-el-box">
                                        <span>{$t.filter_vendor}</span>
                                         <select data-placeholder="{$t.filter_sel_all}" name="filter[car][vendor_id]" id="filter_car_vendor_id">
                                            {foreach $car_vendors as $item}
                                            <option value="{$item.id}">{$item.vendor}</option>
                                            {/foreach}
                                        </select>
                                    </div>
                                    <div class="each-el-box">
                                        <span>{$t.filter_model}</span>
                                         <select data-placeholder="{$t.filter_sel_all}" name="filter[car][model_id]" id="filter_car_model_id" disabled></select>
                                    </div>
                                    <div class="each-el-box">
                                        <span>{$t.filter_year}</span>
                                         <select data-placeholder="{$t.filter_sel_all}" name="filter[car][year_id]" id="filter_car_year_id" disabled></select>
                                    </div>
                                    <div class="each-el-box">
                                        <span>{$t.filter_modification}</span>
                                         <select data-placeholder="{$t.filter_sel_all}" name="filter[car][modification_id]" id="filter_car_modification_id" disabled></select>
                                    </div>
                                    <div class="each-el-box btn-box">
                                        <button type="submit" class="btn-mid-blue">{$t.btn_filter}</button>
                                    </div>
                                </form>
                                <!--end-->
                            </div>
                            <div id="tab-f">
                                <form class="form-filter no-label" method="get" action="7">
                                    {if isset($tires.vendor)}
                                        <div class="each-el-box">
                                            <span>{$tires.vendor.name}:</span>
                                             <select data-placeholder="{$t.filter_sel_all}" name="filter[f][{$tires.vendor.id}][]">
                                                {foreach $tires.vendor.values as $item}
                                                    <option value="{$item.id}">{$item.value}</option>
                                                {/foreach}
                                            </select>
                                        </div>
                                    {/if}
                                    <div class="each-el-box">
                                        <a href="26" class="all-link">{$t.filter_all_vendors}</a>
                                    </div>
                                    <div class="each-el-box"></div>
                                    <div class="each-el-box"></div>
                                    <div class="each-el-box btn-box">
                                        <button type="submit" class="btn-mid-blue">{$t.btn_filter}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="disc">
                    <div class="tab-container level-2">
                        <div class="tabs-title">
                            <span>{$t.sel_disc}</span>
                        </div>
                        <ul class='etabs'>
                            <li class='tab'><a href="#tab-a"><span>{$t.sel_by_params}</span></a></li>
                            <li class='tab'><a href="#tab-b"><span>{$t.sel_by_auto}</span></a></li>
                            <li class='tab'><a href="#tab-c"><span>{$t.sel_by_vendor}</span></a></li>
                        </ul>
                        <div class="link-to-all">
                            <a href="8"><span>{$t.all_disc}</span></a>
                        </div>
                        <div class="panel-container">
                            <div id="tab-a">
                                <form class="form-filter" action="8" method="get">
                                    {if isset($disc.width)}
                                        <div class="each-el-box smaller-box">
                                            <span>{$disc.width.name}:</span>
                                            <label class="lb d-width"></label>
                                             <select data-placeholder="{$t.filter_sel_all}" name="filter[f][{$disc.width.id}]">
                                                <option value="">{$t.filter_sel}</option>
                                                {foreach $disc.width.values as $item}
                                                    <option value="{$item.id}">{$item.value}</option>
                                                {/foreach}
                                            </select>
                                        </div>
                                    {/if}
                                    {if isset($disc.diametr)}
                                        <div class="each-el-box smaller-box">
                                            <span>{$disc.width.name}:</span>
                                            <label class="lb d-diam"></label>
                                             <select data-placeholder="{$t.filter_sel_all}" name="filter[f][{$disc.diametr.id}]">
                                                <option value="">{$t.filter_sel}</option>
                                                {foreach $disc.diametr.values as $item}
                                                    <option value="{$item.id}">{$item.value}</option>
                                                {/foreach}
                                            </select>
                                        </div>
                                    {/if}
                                    {if isset($disc.pcd)}
                                        <div class="each-el-box smaller-box">
                                            <span>{$disc.pcd.name}:</span>
                                            <label class="lb d-pcd"></label>
                                             <select data-placeholder="{$t.filter_sel_all}" name="filter[f][{$disc.pcd.id}]">
                                                <option value="">{$t.filter_sel}</option>
                                                {foreach $disc.pcd.values as $item}
                                                    <option value="{$item.id}">{$item.value}</option>
                                                {/foreach}
                                            </select>
                                        </div>
                                    {/if}
                                    {if isset($disc.et)}
                                        <div class="each-el-box smaller-box">
                                            <span>{$disc.et.name}:</span>
                                            <label class="lb d-et"></label>
                                             <select data-placeholder="{$t.filter_sel_all}" name="filter[f][{$disc.et.id}]">
                                                <option value="">{$t.filter_sel}</option>
                                                {foreach $disc.et.values as $item}
                                                    <option value="{$item.id}">{$item.value}</option>
                                                {/foreach}
                                            </select>
                                        </div>
                                    {/if}
                                    {if isset($disc.type)}
                                        <div class="each-el-box smaller-box wide-select">
                                            <span>{$disc.type.name}:</span>
                                             <select data-placeholder="{$t.filter_sel_all}" name="filter[f][{$disc.type.id}]">
                                                <option value="">{$t.filter_sel}</option>
                                                {foreach $disc.type.values as $item}
                                                    <option value="{$item.id}">{$item.value}</option>
                                                {/foreach}
                                            </select>
                                        </div>
                                    {/if}
                                    <div class="each-el-box smaller-box btn-box">
                                        <button type="submit" class="btn-mid-blue">{$t.btn_filter}</button>
                                    </div>
                                </form>
                            </div>
                            <div id="tab-b">
                                <form class="form-filter no-label" method="get" action="8">
                                    <div class="each-el-box">
                                        <span>{$t.filter_vendor}</span>
                                         <select data-placeholder="{$t.filter_sel_all}" name="filter[car][vendor_id]" id="filter_car_vendor_id2">
                                            {foreach $car_vendors as $item}
                                                <option value="{$item.id}">{$item.vendor}</option>
                                            {/foreach}
                                        </select>
                                    </div>
                                    <div class="each-el-box">
                                        <span>{$t.filter_model}</span>
                                         <select data-placeholder="{$t.filter_sel_all}" name="filter[car][model_id]" id="filter_car_model_id2" disabled></select>
                                    </div>
                                    <div class="each-el-box">
                                        <span>{$t.filter_year}</span>
                                         <select data-placeholder="{$t.filter_sel_all}" name="filter[car][year_id]" id="filter_car_year_id2" disabled></select>
                                    </div>
                                    <div class="each-el-box">
                                        <span>{$t.filter_modification}</span>
                                         <select data-placeholder="{$t.filter_sel_all}" name="filter[car][modification_id]" id="filter_car_modification_id2" disabled></select>
                                    </div>
                                    <div class="each-el-box btn-box">
                                        <button type="submit" class="btn-mid-blue">{$t.btn_filter}</button>
                                    </div>
                                </form>
                            </div>
                            <div id="tab-c">
                                <form class="form-filter no-label" action="8" method="get">
                                    {if isset($disc.vendor)}
                                        <div class="each-el-box ">
                                            <span>{$disc.vendor.name}:</span>
                                             <select data-placeholder="{$t.filter_sel_all}" name="filter[f][{$disc.vendor.id}][]">
                                                {foreach $disc.vendor.values as $item}
                                                    <option value="{$item.id}">{$item.value}</option>
                                                {/foreach}
                                            </select>
                                        </div>
                                    {/if}
                                    <div class="each-el-box">
                                        <a href="27" class="all-link">{$t.filter_all_vendors}</a>
                                    </div>
                                    <div class="each-el-box"></div>
                                    <div class="each-el-box"></div>
                                    <div class="each-el-box btn-box">
                                        <button type="submit" class="btn-mid-blue">{$t.btn_filter}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>