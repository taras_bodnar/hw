<div class="each-box">
    <div class="product-box">
        <a href="{$item.id}" title="{$item.title}" class="pr-image">
            <img src="{$item.img.src}" class="lazyOwl">
        </a>
        <div class="product-info">
            {if isset($item.season)}
                <div class="br"><span class="season {$item.season}"></span></div>
            {/if}
            {if $item.sale}
                <div class="br"><span class="sales action">{$t.label_sale}</span></div>
            {/if}
            {if $item.hit}
                <div class="br"><span class="sales top">{$t.label_hit}</span></div>
            {/if}
            <p><a href="{$item.id}">{$item.name}</a></p>
            <div class="br"><span class="price regular">{$t.p_from} <span>{round($item.price,2)}</span> {$item.symbol}</span></div>
        </div>
        <div class="number">{$item.id}</div>
    </div>
</div>