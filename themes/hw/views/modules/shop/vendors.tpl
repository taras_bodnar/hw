<section class="brands" style="padding-top: 0;">
    <div class="container-fixed-width has-padding">
        {foreach $letters as $row}
        <div class="row-line">
            {foreach $row as $k=>$letter}
            <div class="links-container">
                <div class="round-marker">
                    <span>{$letter.letter}</span>
                </div>
                <ul>
                    {foreach $letter.items as $item}
                    <li><a href="{$cat_id};filter[f][{$features_id}][]={$item.id}">{$item.value}</a></li>
                    {/foreach}
                </ul>
            </div>
            {/foreach}
        </div>
        {/foreach}
        {if $page.id == 27}
        <a  href="26" class="disc-brands">{$t.link_brand_tires}</a>
        {else}
        <a  href="27" class="disc-brands">{$t.link_brand_disc}</a>
        {/if}
    </div>
</section>