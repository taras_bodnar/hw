<div class="filter-navigation shop-sorting">
    <span class="fl-tl">{$t.shop_sorting}</span>
    {foreach $items as $i=>$item}
        <div class="fl-arrow-bt
        {if $i==0 && !isset($smarty.request.sort) || $smarty.request.sort == $item.id}active{/if}
        ">
            <a href="{if $uri == ''}{$cat_id};{/if}{$uri}{if $uri !=''}?{/if}sort={$item.id}&order=asc" data-id="{$item.id}" data-sort="asc" class="tr-title s-sorting"><span>{$item.name}</span></a>
            {if $item.id != 8}
            <a class="up s-sorting {if $smarty.request.sort == $item.id && $smarty.request.order == 'asc'}active{/if}"   href="{if $uri == ''}{$cat_id};{/if}{$uri}{if $uri !=''}?{/if}sort={$item.id}&order=asc"  data-id="{$item.id}" data-sort="asc"></a>
            <a class="down s-sorting {if $smarty.request.sort == $item.id && $smarty.request.order == 'desc'}active{/if}" href="{if $uri == ''}{$cat_id};{/if}{$uri}{if $uri !=''}?{/if}sort={$item.id}&order=desc" data-id="{$item.id}" data-sort="desc"></a>
            {/if}
        </div>
    {/foreach}
</div>