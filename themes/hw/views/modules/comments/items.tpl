<div class="reviews">
    <div class="reviews-header">
        <h2>{$t.comments_title}<span>({$comments_total})</span></h2>
        {if ! $is_worker}
        <a class="btn icons-png-red icons-massage" id="addComment" href="#" data-id="{$workers_id}"><span>{$t.comments_btn}</span></a>
        {/if}
    </div>
    <div class="reviews-content" id="comments">
        {$comments_items}
    </div>
    {if $comments_show_more}
    <div class="reviews-footer">
        <a class="btn icons-png-red icons-around comments-more" href="javascript:;">
            <span>{$t.comments_more}</span>
        </a>
    </div>
    {/if}
</div>