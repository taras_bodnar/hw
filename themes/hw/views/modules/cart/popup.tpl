<div class="cart-window">
    <div class="window-content">
        <h2>{$t.cart_title}</h2>
        {foreach $items as $item}
        <div class="win-row" id="cart-row-{$item.id}">
            <a href="#" data-id="{$item.id}" class="del-from-cart">
                <span></span>
            </a>
            <div class="tb-cell pr-photo">
                <img src="{$item.img.src}">
            </div>
            <div class="tb-cell pr-desc">
                <p>{$item.name}</p>
                {if $item.sale}
                    <div class="br"><span class="sales action">{$t.label_sale}</span></div>
                {/if}
                {if $item.hit}
                    <div class="br"><span class="sales top">{$t.label_hit}</span></div>
                {/if}
            </div>
            <div class="tb-cell pr">
                <span class="regular">{number_format($item.price, 0, ',', ' ')}  {$item.symbol}</span>
            </div>
            <div class="tb-cell">
                <input
                        type="number"
                        class="cart-qtt"
                        name="quantity[{$item.id}]"
                        value="{$item.quantity}"
                        data-price="{$item.price}"
                        data-id="{$item.id}"
                        >
            </div>
            <div class="tb-cell total">
                <strong><span data-price="{$item.price * $item.quantity}" class="s-total s-total-{$item.id}">{number_format($item.price * $item.quantity, 0, ',', ' ')}</span>  {$item.symbol}</strong>
            </div>
        </div>
        {/foreach}
    </div>
    <div class="window-footer">
        <a href="#" class="back-link close-cart">
            <span class="arr"></span>
            <span class="arr-text"><span>{$t.cart_more}</span></span>
        </a>
        <p>{$t.cart_total} <span id="p-f-total">0<span>грн</span></span></p>
        <button onclick='self.location.href="46"' class="btn-mid-blue bt" type="button">{$t.cart_buy}</button>
    </div>
</div>