{if !empty($e)}
    <p>{$e}</p>
{else}
    <div class="m-list-search">
        <div class="m-content">
            <p>{$m}</p>
            <ul>
                {foreach $results as $item}
                <li><a href="{$item.id}" title="{$item.title}">{$item.name}</a></li>
                {/foreach}
            </ul>
        </div>
    </div>
{/if}