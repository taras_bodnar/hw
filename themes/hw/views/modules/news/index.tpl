<section class="l-news">
    <div class="container">
        <div class="list-news" id="newsContent">
            {$items}
        </div>
        <div class="footer">
            <div class="col-3">
                {if $total>0}
                    <a class="btn icons-png-red icons-around b-more-news" data-p="{$start}" href="#">
                        <span>{$t.b_news_more}</span>
                    </a>
                {/if}
            </div>
            <div class="col-9 m-pagination" id="pagination">
                {$pagination}
            </div>
        </div>

    </div>
</section><!-- .l-title-page-->