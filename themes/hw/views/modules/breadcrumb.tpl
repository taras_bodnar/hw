<div class="frame-crumbs">
    <div class="crumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
        <div class="container">
            <ul class="items items-crumbs">
                {foreach $items as $i=>$item}
                    {if $i != $tc}
                        <li class="btn-crumb">
                            <a href="{$item.id}" title="{$item.title}" typeof="v:Breadcrumb">
                                <span class="text-el">{$item.name}<span class="divider"><span class="icon-two-arr"></span></span></span>
                            </a>
                        </li>
                        {else}
                        <li class="btn-crumb">
                            <button typeof="v:Breadcrumb" disabled="disabled">
                                <span class="text-el">{$item.name}</span>
                            </button>
                        </li>
                    {/if}
                {/foreach}
            </ul>
        </div>
    </div>
</div>