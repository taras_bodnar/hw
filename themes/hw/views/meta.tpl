<!-- (c) Developed by Otakoyi.com | http://www.otakoyi.com/ -->
<!-- (c) Powered by OYi.Engine | http://www.engine.otakoyi.com/ -->
<base href="{$appurl}" />
<title>{$page.title}{if $page.author}: {$page.author} {/if}{if $page.tag} / {$t.tag_title}: {$page.tag} {/if}</title>
<meta name="keywords" content="{$page.keywords}{if $page.author} : {$page.author} {/if}{if $page.tag} / {$t.tag_title}: {$page.tag} {/if}"/>
<meta name="description" content="{$page.description}{if $page.author} : {$page.author} {/if}{if $page.tag} / {$t.tag_title}: {$page.tag} {/if}"/>
<meta name="generator" content="OYi.Engine 6">

<meta name="Reply-to" content="support@otakoyi.com" />

{if $page.canonical}
    <link rel="canonical" href="{$page.canonical}">
{/if}


<meta name="document-state"           content="Dynamic" />
<meta name="document-rights"          content="Public Domain" />
<meta name="distribution"             content="Global" />
<meta name="robots"                   content="/,follow" />
<meta name='yandex-verification' content='519ffc91768aeae2' />

<meta itemprop="name" content="{$page.title}{if $page.author}: {$page.author} {/if}{if $page.tag} / {$t.tag_title}: {$page.tag} {/if}">
<meta itemprop="description" content="{$page.description}{if $page.author}: {$page.author} {/if}{if $page.tag} / {$t.tag_title}: {$page.tag} {/if}">
<meta property="og:title" content="{$page.title}{if $page.author}: {$page.author} {/if}{if $page.tag} / {$t.tag_title}: {$page.tag} {/if}">
<meta property="og:type" content="Page">
<meta property="og:url" content="{$page.url}">
<meta property="og:description" content="{$page.description}{if $page.author}: {$page.author} {/if}{if $page.tag} / {$t.tag_title}: {$page.tag} {/if}">