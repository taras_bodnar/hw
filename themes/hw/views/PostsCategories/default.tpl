{*
 * Company Otakoyi.com.
 * Author Демоверсія OYi.Engine
 * Date: 17.03.2015
 * Time: 14:05
 * Name: Default
 * Description: Default template
 *}

[[chunk:head]]
<div class="l-wrapper">
    [[chunk:header]]
    <main class="l-main" role="main">
        <section class="l-title">
            <div class="row">
                <h1>{$page.name}</h1>
            </div>
        </section><!-- .l-title-page-->

        [[mod:Blog]]

        <section class="l-page">
            <div class="container">
                <div class="cms_content">
                    {$page.content}
                </div>
            </div>
        </section><!-- .l-title-page-->

    </main><!-- .l-main -->
</div><!-- .l-wrapper -->
[[chunk:footer]]