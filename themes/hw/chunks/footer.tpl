{*
 * Company Otakoyi.com.
 * Author Володимир
 * Date: 22.01.2015
 * Time: 00:01
 * Name: footer
 *}

<footer class="l-footer">
    <div class="row top">
        <div class="container">
            <div class="logo">
                <a href="1">
                    <img src="{$template_url}assets/img/images/color-logo.png" alt=""/>
                </a>
            </div>
            <div class="footer-menu">
                <div class="table">
                    <div class="table-cell">
                        [[mod:Nav::bottom]]
                    </div>
                </div>
            </div>
            <div class="btn-group">
                <a href="#" class="btn png-red b-feedback">{$t.f_feedback}</a>

                {if !isset($smarty.session.app.user.id)}
                    <a class="btn red pop-up"  href="ajax/Users/signUp">{$t.f_register}</a>   
                {/if}
                
                
           
            </div>
        </div>
    </div>
    <div class="row bottom">
        <div class="container">
            <div class="copyright">© 2015{if date('Y') > 2015} - {date('Y')}{/if} HotWed</div>
            <div class="copyright-text">{$t.copy}</div>
            <div class="copyright-otakoyi">{$t.oyi_copy}</div>
        </div>
    </div>
</footer><!-- .l-footer -->

<div class="m-scroll-top default"> 
    <i class="icon-arrow_square"></i>
</div>

<section id="search-panel" class="l-search-panel">
    <button class="close-search-panel" type="button">
        <i class="icon-cancel"></i>
    </button>
    <form action="3">
        <div class="container h100">
            <div class="table">
                <div class="table-cell">
                    <div class="search-box">
                        <input type="search" placeholder="{$t.p_searcch}" name="q" required>
                        <button class="submit" type="submit">
                            <i class="icon-search"></i>
                        </button>
                    </div>
                    <button class="btn png-white-red">{$t.b_filter}</button>
                </div>
            </div>
        </div>
    </form>
</section><!-- .l-search -->


<div id="hotwed-preloader">
    <div class="table">
        <div class="table-cell">
            <div class="preloader-wrapper">
                <div class="heart">
                    {*<i class="preloader i-1"></i>*}
                    {*<i class="preloader i-2"></i>*}
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="223.131px" height="130.021px" viewBox="0 0 223.131 130.021" enable-background="new 0 0 223.131 130.021" xml:space="preserve">
                        <path id="p1" class="path" fill="none" d="M109.439 130.021c0 0-0.897-9.334 2.5-12.36c3.578-3.188 8.563-0.391 9.1 1.1 c0.378 1.133-1.396 2.051-4.344 0.297c-3.852-2.292-4.188-5.688-3.688-7.625s3.521-4.817 8.375-3.78 c5.125 1.1 6.9 4 6.6 5.166c-0.479 1.833-10.873-3.312-13.167-9.667c-3-8.313 11.063-11.602 19.625-3.313 c6.844 6.625-4.07 2.827-5.5 2.25c-4.198-1.695-13.053-6.188-14.125-11.563c-1.268-6.353 14.031-5.063 20.594-2.844 s13.405 4.3 14.7 8.219c0.969 2.969-12.563-3.031-18-6.156s-15.125-8.094-19.156-14.25s4.719-7.813 9.875-7.563 s21.313 3.9 31.9 9.906s12.75 8.7 9.8 9.063s-22.344-7.469-37.906-16.031s-18.698-17.885 0.469-14.885 s24.416 5.7 31.6 8.537s32.757 17.4 17.3 12.63s-41.168-20.583-46.834-23.917s-22.584-15.75-5.75-14.167 s43.251 12.3 53.1 16.833s31.25 15.3 21.8 16.833s-65.084-31.25-71.417-36.5s-16.196-12.974-8-13.167 s53.916 14.8 71.8 22.417s30.749 15.9 24.6 17.75s-47.976-17.619-65-27.417s-41.834-25.75-31-27.667 s61.417 18 71.7 22.917s43.834 20.4 28.2 18.583s-65-26.75-70.25-29.167c-3.053-1.405-15.233-7.672-21.604-13.069 c-4.584-3.883-4.467-7.788 1.171-7.399c23.684 1.6 68.7 26.9 68.7 26.9"/>
                        <path id="p2" class="path" fill="none" d="M114.592 130.021c0 0 0.904-9.334-2.519-12.36c-3.607-3.188-8.63-0.391-9.135 1.1 c-0.381 1.1 1.4 2.1 4.4 0.297c3.882-2.292 4.221-5.688 3.717-7.625s-3.549-4.817-8.442-3.78 c-5.165 1.095-6.958 4.021-6.656 5.166c0.483 1.8 10.959-3.312 13.271-9.667c3.024-8.313-11.15-11.602-19.78-3.313 c-6.898 6.6 4.1 2.8 5.5 2.25c4.231-1.695 13.156-6.188 14.237-11.563c1.278-6.353-14.142-5.063-20.757-2.844 s-13.511 4.289-14.804 8.219c-0.977 3 12.662-3.031 18.143-6.156c5.48-3.125 15.245-8.094 19.308-14.25 s-4.756-7.813-9.954-7.563c-5.197 0.25-21.481 3.906-32.127 9.906c-10.646 6-12.851 8.656-9.89 9.063s22.521-7.469 38.207-16.031 c15.687-8.563 18.847-17.885-0.473-14.885c-19.318 3-24.609 5.74-31.833 8.537c-7.223 2.796-33.016 17.38-17.471 12.6 S89.049 56.6 94.8 53.245s22.762-15.75 5.795-14.167c-16.967 1.583-43.594 12.333-53.505 16.8 c-9.911 4.5-31.498 15.333-21.922 16.833s65.6-31.25 71.983-36.5c6.383-5.25 16.324-12.974 8.063-13.167 c-8.261-0.192-54.343 14.833-72.402 22.417S1.78 61.4 8 63.245S56.351 45.6 73.5 35.8 c17.159-9.798 42.166-25.75 31.246-27.667s-61.904 18-72.235 22.917S-11.661 51.5 4.1 49.7 c15.791-1.833 65.515-26.75 70.807-29.167c3.077-1.405 15.354-7.672 21.776-13.069c4.621-3.883 4.502-7.788-1.18-7.399 c-23.872 1.634-69.229 26.95-69.229 26.9"/>
                    </svg>
                    
                </div>
                <div class="title">
                    <i class="preloader i-3"></i>
                </div>
            </div>
        </div>
    </div>
</div>
{literal}
    <script>
        var $buoop = {c:2, reminder: 0};
        function $buo_f(){
            var e = document.createElement("script");
            e.src = "//browser-update.org/update.min.js";
            document.body.appendChild(e);
        };
        try {document.addEventListener("DOMContentLoaded", $buo_f,false)}
        catch(e){window.attachEvent("onload", $buo_f)}
    </script>
{/literal}
<!-- js -->
<script src="{$template_url}assets/js/vendor/jquery-1.11.2.min.js"></script>
<script src="{$template_url}assets/js/vendor/jquery-ui.min.js"></script>
<script src="{$template_url}assets/js/vendor/jquery.formstyler.min.js"></script>
<script src="{$template_url}assets/js/vendor/owl.carousel.min.js"></script>
<script src="{$template_url}assets/js/vendor/TweenMax.min.js"></script>
<script src="{$template_url}assets/js/vendor/jquery.fancybox.js"></script>
<script src="{$template_url}assets/js/vendor/jquery.form.min.js"></script>
<script src="{$template_url}assets/js/vendor/jquery.tooltipster.min.js"></script>
<script src="{$template_url}assets/js/vendor/es5-shim.min.js"></script>
<script src="{$template_url}assets/js/vendor/jquery.maskedinput.min.js"></script>
<script src="{$template_url}assets/js/vendor/moment.min.js"></script>
<script src="{$template_url}assets/js/vendor/fullcalendar.min.js"></script>
<script src="{$template_url}assets/js/vendor/lang/{$page.languages_code}.js"></script>
<script src="{$template_url}assets/js/vendor/i18n/datepicker-{$page.languages_code}.js"></script>
<script src="{$template_url}assets/js/vendor/jquery.qtip.min.js"></script>
<script src="{$template_url}assets/js/vendor/jquery.lazy.js"></script>
<script>
    window.page = {
        id: {$page.id},
        templates_id: {$page.templates_id},
        type_id:      {$page.type_id},
        langCode: '{$page.languages_code}'
    };
    window.lang = {
        fileBrowse : '{$t.fileBrowse}',
        filePlaceholder : '{$t.filePlaceholder}',
        is_disabled: '{$t.is_disabled}'
    };
    var SKEY = '{$skey}',
        I_ONLINE = {if $smarty.session.app.user.id}true{else}false{/if};
</script>
<script src="{$template_url}assets/js/main.js?_={time()}"></script>
</body>
</html>
