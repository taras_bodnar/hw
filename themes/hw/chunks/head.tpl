<!DOCTYPE html>
<html>
<head>
    [[app:metatags]]
    <meta charset="utf-8" />
    
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link href="{$template_url}assets/img/favicon.ico" rel="shortcut icon" type="image/x-icon" />

    <script src="{$template_url}assets/js/vendor/modernizr.custom.18042.js"></script>

    <!-- css -->
    <link href="{$template_url}assets/css/vendor/preloader.css" rel="stylesheet"> 
    <link href="{$template_url}assets/css/vendor/owl.carousel.css" rel="stylesheet">
    <link href="{$template_url}assets/css/vendor/jquery.fancybox.css" rel="stylesheet">

    <link href="{$template_url}assets/css/vendor/fullcalendar.min.css" rel="stylesheet">
    <link href="{$template_url}assets/css/vendor/jquery.qtip.min.css" rel="stylesheet">
    <link href="{$template_url}assets/css/style.css" rel="stylesheet">
</head>
<body>