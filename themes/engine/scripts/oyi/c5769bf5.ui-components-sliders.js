$(document).ready(function() {
    !verboseBuild || console.log('-- starting oyi.uiComponentsSliders build');
    
    oyi.uiComponentsSliders.build();
});

oyi.uiComponentsSliders = {
	build: function () {
		// Initiate Sliders
		$('.bslider').slider();
		
		!verboseBuild || console.log('            oyi.uiComponentsSliders build DONE');
	}
}