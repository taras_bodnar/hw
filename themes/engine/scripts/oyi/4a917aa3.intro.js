$(document).ready(function() {
    !verboseBuild || console.log('-- starting oyi.intro build');
    
    oyi.intro.build();
});

oyi.intro = {
	build: function () {
		if (!$('.sidebar').is('.extended')){
			oyi.sidebar.toogleSidebar();
		}
		setTimeout(function() {
			introJs().setOptions({'showStepNumbers': false}).start();
		}, 1300);
	}	
}