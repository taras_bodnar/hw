$(document).ready(function() {
    !verboseBuild || console.log('-- starting engine.dashboard build');
    engine.dashboard.build();
});

engine.dashboard = {
	build: function () {
		engine.dashboard.events();
		engine.dashboard.quickLaunchSort();
		engine.dashboard.loadWidgetPositions()
		engine.dashboard.setBlankWidgets();
		engine.dashboard.widgetSort();
		engine.dashboard.drawCharts();
		engine.dashboard.select2();
		engine.dashboard.lightUp();
		engine.dashboard.alerts();

		!verboseBuild || console.log('            engine.dashboard build DONE');

		// Morris Charts
	},
	events : function () {
		!verboseBuild || console.log('            engine.dashboard binding events');

		// toggle dashboar menu
		$(document).on('click', '.dashboard-menu', function(event) {
			event.preventDefault();
			$(this).toggleClass('expanded');
			$('.menu-state-icon').toggleClass('active');
		});
		// toggle widget setup state
		$(document).on('click', '.toggle-widget-setup', function(event) {
			event.preventDefault();
			$(this).parents('.engine-widget').toggleClass('setup');
		});
	},
	quickLaunchSort : function () {
		!verboseBuild || console.log('            engine.dashboard.quickLaunchSort()');

		engine.dashboard.isDragActive = false;
		$( ".quick-launch-bar ul" ).sortable({
		    containment: 'parent',
		    tolerance: 'pointer',
		    start: function(event, ui) {
		        engine.dashboard.isDragActive = true;
		        $('.tooltip').tooltip('hide');
		    },
		    stop: function(event, ui) {
		        engine.dashboard.isDragActive = false;
		    }
		});
	},
	lightUp : function () {
		!verboseBuild || console.log('            engine.dashboard.lightUp()');

		var numWidgets = $('.engine-widget').length;
		var currentWidget = 0;
		setTimeout(showWidget, 200);

		function showWidget () {
			$('.engine-widget').eq(currentWidget).addClass('lit');
			if(currentWidget == numWidgets) return;
			currentWidget++;
			setTimeout(showWidget, 100);
		}
	},
	widgetPositions : [],
	loadWidgetPositions : function () {
		!verboseBuild || console.log('            engine.dashboard.loadWidgetPositions()');
		var positionArray = engine.dashboard.widgetPositions;
		var positionsFromCookie = $.cookie('engine_widgetPositions') || false;
		if(positionsFromCookie){
			positionArray = positionsFromCookie.split(',');
			$.each(positionArray, function(index, val) {
				$('#' + val).appendTo('.widget-group');
			});
		}
		else engine.dashboard.saveWidgetPositions();
	},
	saveWidgetPositions : function () {
		!verboseBuild || console.log('            engine.dashboard.saveWidgetPositions()');
		
		var positionArray = engine.dashboard.widgetPositions = [];
		$('.engine-widget').not('.placeholder').each(function(index, el) {
			var wid = $(el).attr('id');
			positionArray.push(wid);
		});
		$.cookie('engine_widgetPositions', positionArray, {
	        expires: 365,
	        path: '/'
	    });
	},
	widgetSort : function () {
		!verboseBuild || console.log('            engine.dashboard.widgetSort()');

		engine.dashboard.isDragActive = false;
		$( ".widget-group" ).sortable({
		    cancel: '.placeholder, .flip-it',
		    placeholder: 'drag-placeholder',
		    start: function(event, ui) {
		        engine.dashboard.isDragActive = true;
		        $('.tooltip').tooltip('hide');
		    },
		    stop: function(event, ui) {
		        engine.dashboard.saveWidgetPositions();
		        engine.dashboard.isDragActive = false;
		    },
		    tolerance: 'pointer',
		    handle: ".panel-heading"
		});
	},
	setBlankWidgets: function () {
		!verboseBuild || console.log('            engine.dashboard.setBlankWidgets()');

		var realWidgetNum = $('.engine-widget').not('.placeholder').length;
		var placeholderNum = $('.engine-widget.placeholder').length;

		var availableWidth = $('.widget-group').width();
		var widgetWidth = $('.engine-widget').outerWidth(true);
		var widgetsPerRow = Math.floor(availableWidth / widgetWidth);
		var widgetRows = Math.ceil(realWidgetNum / widgetsPerRow);

		var newPlaceholderNum = (widgetRows * widgetsPerRow) - realWidgetNum;

		$('.engine-widget.placeholder').appendTo('.widget-group');
		if(newPlaceholderNum === placeholderNum){
			return;
		}
		if(newPlaceholderNum <= placeholderNum){
			for (var i = placeholderNum - newPlaceholderNum; i > 0; i--) {
			    $('.engine-widget.placeholder').last().remove();
			}
			return;
		}
		if(newPlaceholderNum >= placeholderNum){
			for (var i = newPlaceholderNum - placeholderNum; i > 0; i--) {
			    $('<div class="engine-widget placeholder lit"></div>').appendTo('.widget-group');
			}
			return;
		}
	},
	graph : {},
	drawCharts : function () {
		!verboseBuild || console.log('            engine.dashboard.drawCharts()');

		engine.dashboard.graph.Donut = Morris.Donut({
		    element: 'hero-donut',
		    data: [
		      {label: 'Production', value: 30 },
		      {label: 'R&D', value: 40 },
		      {label: 'Marketing', value: 25 },
		      {label: 'Sales', value: 5 }
		    ],
		    formatter: function (y) { return y + "%" },
		    colors : ['#428bca', '#5cb85c', '#d9534f', '#5bc0de']
		});

		engine.dashboard.graph.Bar = Morris.Bar({
		    element: 'hero-bar',
		    data: [
		      {year: '2008', income: 5346},
		      {year: '2009', income: 11437},
		      {year: '2010', income: 22475},
		      {year: '2011', income: 33840},
		      {year: '2012', income: 32655},
		      {year: '2013', income: 95471}
		    ],
		    xkey: 'year',
		    ykeys: ['income'],
		    labels: ['Income'],
		    barRatio: 0.1,
		    xLabelAngle: 90,
		    hideHover: 'auto'
		});
	},
	select2 : function () {
		!verboseBuild || console.log('            engine.dashboard.select2()');
		
        $('.select2').select2({ maximumSelectionSize: 6 });
	},
	alerts : function () {
		!verboseBuild || console.log('            engine.dashboard.alerts()');

		// Set up notifications
		$.pnotify.defaults.delay = 7000;
		$.pnotify.defaults.shadow = false;
		$.pnotify.defaults.cornerclass = 'ui-pnotify-sharp';
		$.pnotify.defaults.stack = {"dir1": "down", "dir2": "left", "push": "bottom", "spacing1": 5, "spacing2": 5};

		setTimeout(function(){
		    $.pnotify({
		        title: 'Drag & Drop',
		        type: 'success',
		        history: false,
		        text: 'Reorder Widgets or Quicklaunch bar items by dragging & dropping them.'
		    });
		}, 2000);
		setTimeout(function(){
		    $.pnotify({
		        title: 'Widget Settings',
		        type: 'info',
		        history: false,
		        text: 'Hover over widget, than click on a gear icon to set widget options.'
		    });
		}, 8000);
	}
}