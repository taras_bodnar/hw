engine.ordersStatus = {
    create: function()
    {
        $.get(
            'ordersStatus/create/',
            function(d){
                engine.modal.dialog({
                    content: d,
                    title  : engine.lang.ordersStatus.title,
                    buttons: [
                        {
                            text  : engine.lang.core.save,
                            class : "btn btn-primary",
                            click : function(){
                                $('#ordersStatusForm').submit();
                            }
                        }
                    ]
                });
            }
        );
    },
    edit: function(id)
    {
        $.get(
            'ordersStatus/edit/'+id,
            function(d){
                engine.modal.dialog({
                    content: d,
                    title  : engine.lang.ordersStatus.title,
                    buttons: [
                        {
                            text  : engine.lang.core.save,
                            class : "btn btn-primary",
                            click : function(){
                                $('#ordersStatusForm').submit();
                            }
                        }
                    ]
                });
            }
        );
    },
    del : function(id)
    {
        return engine.modal.confirm(
            engine.lang.ordersStatus.remove_item,
            function(){
                $.get('ordersStatus/delete/'+id,function(d){
                    if(d > 0){
                        var oTable = $('#ordersStatus').dataTable();
                        oTable.fnDraw(oTable.fnSettings());
                        $(".modal").modal('hide');
                    }
                });
            });
    }
};