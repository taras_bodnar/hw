engine.manufacturers = {

    delete : function(id)
    {
        return engine.modal.confirm(
            engine.lang.manufacturers.remove_item,
            function(){
                $.get('pages/delete/'+id,function(d){
                    if(d > 0){
                        var oTable = $('#manufacturers').dataTable();
                        oTable.fnDraw(oTable.fnSettings());
                        $(".modal").modal('hide');
                    }
                });
            });
    },
    pub : function(id,s)
    {
        $.get('pages/pub/'+id+'/'+s+'/',function(d){
            if(d > 0){
                $.pnotify({
                    title: engine.lang.core.info,
                    type: 'success',
                    history: false,
                    text: engine.lang.core.status_changed
                });
                var oTable = $('#manufacturers').dataTable();
                oTable.fnDraw(oTable.fnSettings());
            }
        });
    }
};