
engine.callback = {
    edit: function(id)
    {
        $.get(
            './callback/edit/'+id,
            function(d){
                engine.modal.dialog({
                    content: d,
                    title  : engine.lang.callback.title,
                    buttons: [
                        {
                            text  : engine.lang.core.save,
                            class : "btn btn-primary",
                            click : function(){
                                $('#CallbackForm').submit();
                            }
                        }
                    ]
                });
            }
        );
    },
    del : function(id)
    {
        return engine.modal.confirm(
            engine.lang.callback.remove_item,
            function(){
                $.get('./callback/delete/'+id,function(d){
                    if(d > 0){
                        var oTable = $('#Callback').dataTable();
                        oTable.fnDraw(oTable.fnSettings());
                        $(".modal").modal('hide');
                    }
                });
            });
    }
};