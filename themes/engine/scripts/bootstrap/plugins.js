/**
 * Created by wg on 20.12.14.
 */

engine.plugins = {
    install : function(plugin)
    {
        $.ajax({
            type: "POST",
            url:'plugins/install',
            data: {
                plugin:plugin
            },
            success: function(d){
                engine.modal.dialog({
                    content: d,
                    title  : engine.lang.plugins.install_title,
                    buttons: [
                        {
                            text  : engine.lang.plugins.install,
                            class : "btn btn-primary",
                            click : function(){
                                $('#pluginsForm').submit();
                            }
                        }
                    ]
                });
            },
            dataType: 'html'
        });
    },
    edit : function(id)
    {
        $.ajax({
            type: "GET",
            url:'plugins/edit/'+id,
            success: function(d){
                engine.modal.dialog({
                    content: d,
                    title  : engine.lang.plugins.edit_title,
                    buttons: [
                        {
                            text  : engine.lang.core.save,
                            class : "btn btn-primary",
                            click : function(){
                                $('#pluginsForm').submit();
                            }
                        }
                    ]
                });
            },
            dataType: 'html'
        });
    },
    uninstall : function(id)
    {
        return engine.modal.confirm(
            engine.lang.plugins.uninstall_confirm,
            function(){
                $.ajax({
                    type: "POST",
                    url:'plugins/uninstall',
                    data: {
                        'id' : id
                    },
                    success: function(d){
                        if(d > 0){
                            var oTable = $('#plugins').dataTable();
                            oTable.fnDraw(oTable.fnSettings());
                            $(".modal").modal('hide');
                        }
                    },
                    dataType: 'html'
                });
            });
    }
};