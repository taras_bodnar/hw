/**
 * Created by user on 19.12.14.
 */

/**
 * Engine anounces Categories
 * @type {{category: {create: create}}}
 */
engine.comments = {

    pub : function(id,s)
    {
        $.get('comments/pub/'+id+'/'+s+'/',function(d){
            if(d > 0){
                $.pnotify({
                    title: engine.lang.core.info,
                    type: 'success',
                    history: false,
                    text: engine.lang.core.status_changed
                });

                $('.pub-comment-'+id).remove();

                var oTable = $('#comments').dataTable();
                oTable.fnDraw(oTable.fnSettings());
            }
        });
        return 1;
    },

    delete:  function(id){
        return engine.modal.confirm(
            'ви збираєтесь видалити коментар. Продовжити?',
            function(){
                $.get('comments/delete/'+id,function(d){
                    if(d > 0){
                        var oTable = $('#comments').dataTable();
                        oTable.fnDraw(oTable.fnSettings());
                        $('.comment-'+id).remove();
                        $(".modal").modal('hide');
                    }
                });
            });
    },
    reply: function(id)
    {
        $.get('comments/reply/'+id, function(d){
            engine.modal.dialog({
                content: d,
                title  : engine.lang.comments.reply_title,
                buttons: [
                    {
                        text  : engine.lang.comments.send,
                        class : "btn btn-success",
                        click : function(){
                            $('#form').submit();
                        }
                    }
                ]
            });
        });
    }
};
