/**
 * Created by wg on 20.12.14.
 */

engine.promoCodes = {
    create : function()
    {
        $.ajax({
            type: "GET",
            url:'PromoCodes/create',
            success: function(d){
                engine.modal.dialog({
                    content: d,
                    title  : engine.lang.promo_codes.create_title,
                    buttons: [
                        {
                            text  : engine.lang.core.create,
                            class : "btn btn-primary",
                            click : function(){
                                $('#form').submit();
                            }
                        }
                    ]
                });
            },
            dataType: 'html'
        });
    },
    edit : function(id)
    {
        $.ajax({
            type: "GET",
            url:'PromoCodes/edit/'+id,
            success: function(d){
                engine.modal.dialog({
                    content: d,
                    title  : engine.lang.promo_codes.edit_title,
                    buttons: [
                        {
                            text  : engine.lang.core.save,
                            class : "btn btn-primary",
                            click : function(){
                                $('#form').submit();
                            }
                        }
                    ]
                });
            },
            dataType: 'html'
        });
    },
    del : function(id)
    {
        return engine.modal.confirm(
            engine.lang.promo_codes.remove_item,
            function(){
                $.get('PromoCodes/delete/'+id,function(d){
                    if(d > 0){
                        var oTable = $('#PromoCodes').dataTable();
                        oTable.fnDraw(oTable.fnSettings());
                        $(".modal").modal('hide');
                    }
                });
            });
    }
};