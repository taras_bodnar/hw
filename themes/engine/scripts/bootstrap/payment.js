/**
 * payment methods
 * @type {{pub: pub, del: del}}
 */
engine.payment = {
    init: function(){
        $(document).on('change', "#data_module", function(){
            var m = this.value, ps = $('#paymentSettings'),payment_id=ps.data('payment-id');
            if(m =='') {
                ps.html('');
                return;
            }
            $.get('payment/displaySettings/' + payment_id+'/'+m, function(d){ps.html(d);})
        });
    },
    pub : function(id,s)
    {
        $.get('payment/pub/'+id+'/'+s+'/',function(d){
            if(d > 0){
                $.pnotify({
                    title: engine.lang.core.info,
                    type: 'success',
                    history: false,
                    text: engine.lang.core.status_changed
                });

                var oTable = $('#payment').dataTable();
                oTable.fnDraw(oTable.fnSettings());
            }
        });
        return 1;
    },
    del:  function(id){
        return engine.modal.confirm(
            engine.lang.payment.remove_item,
            function(){
                $.get('payment/delete/'+id,function(d){
                    if(d > 0){
                        var oTable = $('#payment').dataTable();
                        oTable.fnDraw(oTable.fnSettings());
                        $(".modal").modal('hide');
                    }
                });
            });
    }
};

$(document).ready(function(){
    engine.payment.init();
});