engine.customersGroup = {
    delete : function(id)
    {
        return engine.modal.confirm(
            engine.lang.users_group.remove_item,
            function(){
                $.get('CustomersGroup/delete/'+id,function(d){
                    if(d > 0){
                        var oTable = $('#users_group').dataTable();
                        oTable.fnDraw(oTable.fnSettings());
                        $("#tree").jstree("refresh");
                        $(".modal").modal('hide');
                    }
                });
            });
    }
};