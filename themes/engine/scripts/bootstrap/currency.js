
engine.currency = {
    create: function()
    {
        $.get(
            'Currency/create/',
            function(d){
                engine.modal.dialog({
                    content: d,
                    title  : engine.lang.currency.title,
                    buttons: [
                        {
                            text  : engine.lang.core.save,
                            class : "btn btn-primary",
                            click : function(){
                                $('#currencyForm').submit();
                            }
                        }
                    ]
                });
            }
        );
    },
    edit: function(id)
    {
        $.get(
            'Currency/edit/'+id,
            function(d){
                engine.modal.dialog({
                    content: d,
                    title  : engine.lang.currency.title,
                    buttons: [
                        {
                            text  : engine.lang.core.save,
                            class : "btn btn-primary",
                            click : function(){
                                $('#currencyForm').submit();
                            }
                        }
                    ]
                });
            }
        );
    },
    delete : function(id)
    {
        return engine.modal.confirm(
            engine.lang.currency.remove_item,
            function(){
                $.get('currency/delete/'+id,function(d){
                    if(d > 0){
                        var oTable = $('#currency').dataTable();
                        oTable.fnDraw(oTable.fnSettings());
                        $(".modal").modal('hide');
                    }
                });
            });
    },
    changeMain : function(id, status)
    {
        status = status == 1 ? 0 : 1;
        $.get(
            'currency/changeMain/'+id+'/'+status,
            function(s)
            {
                var oTable = $('#currency').dataTable();
                oTable.fnDraw(oTable.fnSettings());
            }
        );
    },
    update: function(e)
    {
        $(e).attr('disabled', true);
        e.value = parseFloat(e.value);
        if(e.value == 'NaN') {
            e.value=1;
        }
        $.ajax({
            type: "POST",
            url: 'currency/updateRate',
            data:
            {
                rate : e.value,
                id   : $(e).data('id')
            },
            success: function(d){
                $(e).removeAttr('disabled');
                if(d > 0){
                    $.pnotify({
                        title: engine.lang.core.info,
                        type: 'success',
                        history: false,
                        text: engine.lang.currency.update_rate_success,
                        stack: {"dir1": "down", "dir2": "right"}
                    });
                }
            },
            dataType: 'html'
        });
    }
};