/**
 * Created by maxim on 06.04.15.
 */
engine.twilio = {
    delete : function(id)
    {
        return engine.modal.confirm(
            engine.lang.letters.remove,
            function(){
                $.get('Twilio/delete/'+id,function(d){
                    if(d > 0){
                        var oTable = $('#Twilio').dataTable();
                        oTable.fnDraw(oTable.fnSettings());
                        $(".modal").modal('hide');
                    }
                });
            });
    },
    changeStatus : function(id, value)
    {
        $.ajax({
            type: "POST",
            url:'orders/changeStatus/',
            data : {
                'id' : id,
                'value' : value
            },
            success: function(d){
                $.pnotify({
                    title: 'Статус змінено',
                    type: 'success',
                    history: false,
                    text:''
                });
            }
        });
    },
    askBeforeSend : function(id) {
        return engine.modal.confirm(
            engine.lang.letters.asksend,
            function(){
                $.get('Twilio/getInforSend/'+id,function(d){
                    if(d.length){
                        $("#alias-box").css("display","block");
                        var obj = $.parseJSON(d);
                        engine.twilio.send(id, obj.total, obj.start);
                        $(".modal").modal('hide');
                    }
                });
            });
    },
    send : function(lid, total, start) {

        if(start >= total) {
            $.ajax({
                type: "POST",
                url: 'Twilio/sendProccess',
                data: {'updatels':1,lid: lid}
            });
            var oTable = $('#Twilio').dataTable();
            oTable.fnDraw(oTable.fnSettings());
            $("#alias-box").css("display","none");
            engine.alert(
                engine.lang.letters.msg_send,
                engine.lang.letters.msg_send_tip.replace("{c}",total),
                'success',
                '#notification',
                'html'
            );
            return false;
        }

        var percent =  100 / total, done = Math.round( start * percent ) ;
        $("#progress").find('div').css('width', done + '%');
        $(".start").html(start);
        $(".total").html(total);
        $.ajax({
            type: "POST",
            url: 'Twilio/sendProccess',
            data: {
                start: start,
                lid: lid
            },
            success: function(d){
                if(d > 0){
                    start++;
                    setTimeout(function(){
                        engine.twilio.send(lid, total, start);
                    },1000);

                }
            },
            dataType: 'html'
        });

        return false;
    }
}
engine.letters = {
    delete : function(id)
    {
        return engine.modal.confirm(
            engine.lang.letters.remove,
            function(){
                $.get('Letters/delete/'+id,function(d){
                    if(d > 0){
                        var oTable = $('#Letters').dataTable();
                        oTable.fnDraw(oTable.fnSettings());
                        $(".modal").modal('hide');
                    }
                });
            });
    },
    changeStatus : function(id, value)
    {
        $.ajax({
            type: "POST",
            url:'orders/changeStatus/',
            data : {
                'id' : id,
                'value' : value
            },
            success: function(d){
                $.pnotify({
                    title: 'Статус змінено',
                    type: 'success',
                    history: false,
                    text:''
                });
            }
        });
    },
    askBeforeSend : function(id) {
        return engine.modal.confirm(
            engine.lang.letters.asksend,
            function(){
                $.get('Letters/getInforSend/'+id,function(d){
                    if(d.length){
                        $("#alias-box").css("display","block");
                        var obj = $.parseJSON(d);
                        engine.letters.send(id, obj.total, obj.start);
                        $(".modal").modal('hide');
                    }
                });
            });
    },
    send : function(lid, total, start) {

           if(start >= total) {
               $.ajax({
                   type: "POST",
                   url: 'Letters/sendProccess',
                   data: {'updatels':1,lid: lid}
               });
               var oTable = $('#Letters').dataTable();
               oTable.fnDraw(oTable.fnSettings());
               $("#alias-box").css("display","none");
                engine.alert(
                    engine.lang.letters.msg_send,
                    engine.lang.letters.msg_send_tip.replace("{c}",total),
                    'success',
                    '#notification',
                    'html'
                );
                return false;
            }

            var percent =  100 / total, done = Math.round( start * percent ) ;
            $("#progress").find('div').css('width', done + '%');
             $(".start").html(start);
             $(".total").html(total);
         $.ajax({
                type: "POST",
                url: 'Letters/sendProccess',
                data: {
                    start: start,
                    lid: lid
                },
                success: function(d){
                    if(d > 0){
                        start++;
                        setTimeout(function(){
                            engine.letters.send(lid, total, start);
                        },1000);

                    }
                },
                dataType: 'html'
            });

            return false;
        }
};