engine.categories = {
    delete : function(id)
    {
        return engine.modal.confirm(
            engine.lang.categories.remove_item,
            function(){
                $.get('pages/delete/'+id,function(d){
                    if(d > 0){
                        var oTable = $('#categories').dataTable();
                        $("#tree").jstree("refresh");
                        oTable.fnDraw(oTable.fnSettings());
                        $(".modal").modal('hide');
                    }
                });
            });
    },
    pub : function(id,s)
    {
        $.get('pages/pub/'+id+'/'+s+'/',function(d){
            if(d > 0){
                $.pnotify({
                    title: engine.lang.core.info,
                    type: 'success',
                    history: false,
                    text: engine.lang.core.status_changed
                });
                var oTable = $('#categories').dataTable();
                oTable.fnDraw(oTable.fnSettings());
            }
        });
    }
};