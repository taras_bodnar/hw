/**
 * Created by wg on 04.06.14.
 */
engine.translations = {
    delete : function(id)
    {
        return engine.modal.confirm(
            engine.lang.translations.remove_item,
            function(){
                $.get('translations/delete/'+id,function(d){
                    if(d > 0){
                        var oTable = $('#translations').dataTable();
                        oTable.fnDraw(oTable.fnSettings());
                        $(".modal").modal('hide');
                    }
                });
             });
    }
};
