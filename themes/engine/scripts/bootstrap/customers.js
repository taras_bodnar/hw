engine.customers = {
    delete : function(id)
    {
        return engine.modal.confirm(
            engine.lang.users.remove_item,
            function(){
                $.get('customers/delete/'+id,function(d){
                    if(d > 0){
                        var oTable = $('#users').dataTable();
                        oTable.fnDraw(oTable.fnSettings());
                        $("#tree").jstree("refresh");
                        $(".modal").modal('hide');
                    }
                });
            });
    }
};