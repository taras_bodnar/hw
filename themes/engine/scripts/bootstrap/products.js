/**
 * Created by wg on 04.06.14.
 */
engine.products = {
    filter: function()
    {
        var mid = $('#filter_manufacturers_id').find('option:selected').val();
        var status = $('#filter_status').find('option:selected').val();

        var oTable = $('#products').dataTable();
        var oS = oTable.fnSettings();
        var data = {
            'filter[mid]' : mid,
            'filter[status]' : status
        };
        $.extend( oS.ajax.data, data );
//        console.log(oS.ajax.data);
        oTable.fnDraw(oS);
    },
    build: function()
    {
        var f = $('#shopFilter');
        if(f.length == 0) return 0;
        var t = this;
        f.find('select').change(function(){
            t.filter();
        });

        $('#resetFilter').click(function(){
//            alert('click');
            f.find('select').each(function(){
                $(this).find('option:first').attr('selected', true).siblings().removeAttr('selected');
            });
            f.find('select').select2().trigger('change');

            return false;
        });

        $(document).on(
            'change',
            '.check-all',
            function()
            {
                if($(this).is(':checked')){
                    $('.group-actions').removeClass('hide');
                } else{
                    $('.group-actions').addClass('hide');
                }
            }
        );
        $(document).on(
            'change',
            '.dt-check',
            function()
            {
                var s = false;
                $('.dt-check').each(function(){
                    if($(this).is(':checked')){
                        s=true;
                    }
                });
                if(s){
                    $('.group-actions').removeClass('hide');
                } else{
                    $('.group-actions').addClass('hide');
                }
            }
        );

/**
 *  GROUP FUNCTIONS
 */

        $(document).on(
            'click',
            '#g_remove',
            function()
            {
                return engine.modal.confirm(
                engine.lang.products.g_delete_confirm,
                function(){
                    t.processDeleteGroup();
                });

            }
        );

        $(document).on(
            'click',
            '#g_pub',
            function()
            {
                var items = t.getSelectedItems();
                $.ajax({
                    type: "POST",
                    url:'products/pubGroup/',
                    data: {
                        items:items,
                        status: 1
                    },
                    success: function(d){
                        if(d > 0) {
                            var oTable = $('#products').dataTable();
                            oTable.fnDraw(oTable.fnSettings());
                            $('.check-all').removeAttr('checked').trigger('change');
                        }
                    },
                    dataType: 'html'
                });
            }
        );

        $(document).on(
            'click',
            '#g_un_pub',
            function()
            {
                var items = t.getSelectedItems();
                $.ajax({
                    type: "POST",
                    url:'products/pubGroup/',
                    data: {
                        items:items,
                        status: 0
                    },
                    success: function(d){
                        if(d > 0) {
                            var oTable = $('#products').dataTable();
                            oTable.fnDraw(oTable.fnSettings());
                            $('.check-all').removeAttr('checked').trigger('change');
                        }
                    },
                    dataType: 'html'
                });
            }
        );

        return 1;
    },
    processDeleteGroup: function(){
        var items = this.getSelectedItems();
        $.ajax({
            type: "POST",
            url:'products/deleteGroup/',
            data: {items:items},
            success: function(d){
                if(d > 0) {
                    var oTable = $('#products').dataTable();
                    oTable.fnDraw(oTable.fnSettings());
                    $('.check-all').removeAttr('checked').trigger('change');
                    $(".modal").modal('hide');
                }
            },
            dataType: 'html'
        });
    },
    getSelectedItems: function(){
        var items = [];
        $('.dt-check ').each(function(){
            if($(this).is(':checked')){
                items.push($(this).val());
            }
        });

        return items;
    },
    delete : function(id)
    {
        return engine.modal.confirm(
            engine.lang.products.remove_item,
            function(){
                $.get('pages/delete/'+id,function(d){
                    if(d > 0){
                        var oTable = $('#products').dataTable();
                        $("#tree").jstree("refresh");
                        oTable.fnDraw(oTable.fnSettings());
                        $(".modal").modal('hide');
                    }
                });
             });
    },
    pub : function(id,s)
    {
        $.get('pages/pub/'+id+'/'+s+'/',function(d){
            if(d > 0){
                $.pnotify({
                    title: engine.lang.core.info,
                    type: 'success',
                    history: false,
                    text: engine.lang.core.status_changed
                });
                var oTable = $('#products').dataTable();
                oTable.fnDraw(oTable.fnSettings());
            }
        });
    },
    sale : function(id,s)
    {
        $.get('products/toggleStatus/sale/'+id+'/'+s+'/',function(d){
            if(d > 0){
                $.pnotify({
                    title: engine.lang.core.info,
                    type: 'success',
                    history: false,
                    text: engine.lang.core.status_changed
                });
                var oTable = $('#products').dataTable();
                oTable.fnDraw(oTable.fnSettings());
            }
        });
    },
    hit : function(id,s)
    {
        $.get('products/toggleStatus/hit/'+id+'/'+s+'/',function(d){
            if(d > 0){
                $.pnotify({
                    title: engine.lang.core.info,
                    type: 'success',
                    history: false,
                    text: engine.lang.core.status_changed
                });
                var oTable = $('#products').dataTable();
                oTable.fnDraw(oTable.fnSettings());
            }
        });
    },
    toggleNew : function(id,s)
    {
        $.get('products/toggleStatus/new/'+id+'/'+s+'/',function(d){
            if(d > 0){
                $.pnotify({
                    title: engine.lang.core.info,
                    type: 'success',
                    history: false,
                    text: engine.lang.core.status_changed
                });
                var oTable = $('#products').dataTable();
                oTable.fnDraw(oTable.fnSettings());
            }
        });
    },
    /**
     * update product price
     * @param id
     * @param price
     */
    updatePrice : function(id, price)
    {
        $.get('products/updatePrice/'+id+'/'+price+'/',function(d){
            if(d > 0){
                $.pnotify({
                    title: engine.lang.core.info,
                    type: 'success',
                    history: false,
                    text: engine.lang.products.price_updated
                });
//                var oTable = $('#products').dataTable();
//                oTable.fnDraw(oTable.fnSettings());
            }
        });
    },
    createMaunfacturers: function()
    {
        $.getJSON('products/createMaunfacturers/',function(d){
            engine.modal.dialog({
                title  : d.t,
                content: d.c,
                buttons: [
                    {
                        text  : engine.lang.core.create,
                        class : "btn btn-success",
                        click : function(){
                            $('#createManufacturerForm').submit();
//                            $('.modal').modal('hide');
                        }
                    }
                ]
            });
        });
    }
};