/**
 * Engine Blog
 * @type {{category: {create: create}}}
 */
engine.blog = {
    posts: {
        del : function(id)
        {
            return engine.modal.confirm(
                engine.lang.blog.remove_item,
                function(){
                    $.get('posts/delete/'+id,function(d){
                        if(d > 0){
                            var oTable = $('#Posts').dataTable();
                            oTable.fnDraw(oTable.fnSettings());
                            $(".modal").modal('hide');
                        }
                    });
                });
        },
        pub : function(id,s)
        {
            $.get('pages/pub/'+id+'/'+s+'/',function(d){
                if(d > 0){
                    $.pnotify({
                        title: engine.lang.core.info,
                        type: 'success',
                        history: false,
                        text: engine.lang.core.status_changed
                    });
                    var oTable = $('#Posts').dataTable();
                    oTable.fnDraw(oTable.fnSettings());
                }
            });
        }
    },
    categories: {
        create: function(parent_id)
        {
            if(typeof parent_id == 'undefined') parent_id=0;

            $.get(
                'PostsCategories/create/'+parent_id,
                function(d){
                    engine.modal.dialog({
                        content: d,
                        title  : engine.lang.blog.cat_title,
                        buttons: [
                            {
                                text  : engine.lang.core.save,
                                class : "btn btn-primary",
                                click : function(){
                                    $('#PostsCategoriesForm').submit();
                                }
                            }
                        ]
                    });
                }
            );
        },
        edit: function(id)
        {
            $.get(
                'PostsCategories/edit/'+id,
                function(d){
                    engine.modal.dialog({
                        content: d,
                        title  : engine.lang.blog.cat_title,
                        buttons: [
                            {
                                text  : engine.lang.core.save,
                                class : "btn btn-primary",
                                click : function(){
                                    $('#PostsCategoriesForm').submit();
                                }
                            }
                        ]
                    });
                }
            );
        },
        del : function(id)
        {
            return engine.modal.confirm(
                engine.lang.blog.remove_cat_item,
                function(){
                    $.get('PostsCategories/delete/'+id,function(d){
                        if(d > 0){
                            var oTable = $('#PostsCategories').dataTable();
                            oTable.fnDraw(oTable.fnSettings());
                            $("#tree").jstree("refresh");
                            $(".modal").modal('hide');
                        }
                    });
                });
        }
    }
};