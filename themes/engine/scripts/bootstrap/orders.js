/**
 * Created by wg on 04.06.14.
 */
engine.orders = {
    filter: function()
    {
        var mid = $('#filter_manufacturers_id').find('option:selected').val();
        var status = $('#filter_status').find('option:selected').val();

        var oTable = $('#orders').dataTable();
        var oS = oTable.fnSettings();
        var data = {
            'filter[mid]' : mid,
            'filter[status]' : status
        };
        $.extend( oS.ajax.data, data );
//        console.log(oS.ajax.data);
        oTable.fnDraw(oS);
    },
    build: function()
    {
        var f = $('#shopFilter');
        //if(f.length == 0) return 0;
        var t = this;
        f.find('select').change(function(){
            t.filter();
        });

        $('#resetFilter').click(function(){
//            alert('click');
            f.find('select').each(function(){
                $(this).find('option:first').attr('selected', true).siblings().removeAttr('selected');
            });
            f.find('select').select2().trigger('change');

            return false;
        });
/*
        $(document).on(
            'click',
            '#addOwner',
            function()
            {
                var id= parseInt($(this).data('id'));
                t.addOwner(id);
                return false;
            }
        );
*/
    },
    del : function(id)
    {
        return engine.modal.confirm(
            engine.lang.orders.remove_item,
            function(){
                $.get('orders/delete/'+id,function(d){
                    if(d > 0){
                        var oTable = $('#orders').dataTable();
                        oTable.fnDraw(oTable.fnSettings());
                        $(".modal").modal('hide');
                    }
                });
             });
    }
};