/**
 * Delivery methods
 * @type {{pub: pub, del: del}}
 */
engine.delivery = {
    pub : function(id,s)
    {
        $.get('delivery/pub/'+id+'/'+s+'/',function(d){
            if(d > 0){
                $.pnotify({
                    title: engine.lang.core.info,
                    type: 'success',
                    history: false,
                    text: engine.lang.core.status_changed
                });

                var oTable = $('#delivery').dataTable();
                oTable.fnDraw(oTable.fnSettings());
            }
        });
        return 1;
    },
    del:  function(id){
        return engine.modal.confirm(
            engine.lang.delivery.remove_item,
            function(){
                $.get('delivery/delete/'+id,function(d){
                    if(d > 0){
                        var oTable = $('#delivery').dataTable();
                        oTable.fnDraw(oTable.fnSettings());
                        $(".modal").modal('hide');
                    }
                });
            });
    }
};