engine.car = {
    vendors: {
        create: function () {
            $.get(
                'CarVendors/create/',
                function (d) {
                    engine.modal.dialog({
                        content: d,
                        title: 'Додати виробника',
                        buttons: [
                            {
                                text: engine.lang.core.save,
                                class: "btn btn-primary",
                                click: function () {
                                    $('#CarVendorsForm').submit();
                                }
                            }
                        ]
                    });
                }
            );
        },
        edit: function (id) {
            $.get(
                'CarVendors/edit/' + id,
                function (d) {
                    engine.modal.dialog({
                        content: d,
                        title: 'Редагувати виробника',
                        buttons: [
                            {
                                text: engine.lang.core.save,
                                class: "btn btn-primary",
                                click: function () {
                                    $('#CarVendorsForm').submit();
                                }
                            }
                        ]
                    });
                }
            );
        },
        del: function (id) {
            return engine.modal.confirm(
                'Ви збираєтесь видалити виробника. Продовжити?',
                function () {
                    $.get('CarVendors/delete/' + id, function (d) {
                        if (d > 0) {
                            var oTable = $('#CarVendors').dataTable();
                            oTable.fnDraw(oTable.fnSettings());
                            $(".modal").modal('hide');
                        }
                    });
                });
        }
    },
    models: {
        create: function (vendor_id) {
            $.get(
                'CarModels/create/'+vendor_id,
                function (d) {
                    engine.modal.dialog({
                        content: d,
                        title: 'Додати модель',
                        buttons: [
                            {
                                text: engine.lang.core.save,
                                class: "btn btn-primary",
                                click: function () {
                                    $('#CarModelsForm').submit();
                                }
                            }
                        ]
                    });
                }
            );
        },
        edit: function (id) {
            $.get(
                'CarModels/edit/' + id,
                function (d) {
                    engine.modal.dialog({
                        content: d,
                        title: 'Редагувати модель',
                        buttons: [
                            {
                                text: engine.lang.core.save,
                                class: "btn btn-primary",
                                click: function () {
                                    $('#CarModelsForm').submit();
                                }
                            }
                        ]
                    });
                }
            );
        },
        del: function (id) {
            return engine.modal.confirm(
                'Ви збираєтесь видалити модель. Продовжити?',
                function () {
                    $.get('CarModels/delete/' + id, function (d) {
                        if (d > 0) {
                            var oTable = $('#CarModels').dataTable();
                            oTable.fnDraw(oTable.fnSettings());
                            $(".modal").modal('hide');
                        }
                    });
                });
        }
    },
    years: {
        create: function (vendor_id) {
            $.get(
                'CarYears/create/'+vendor_id,
                function (d) {
                    engine.modal.dialog({
                        content: d,
                        title: 'Додати рік випуску',
                        buttons: [
                            {
                                text: engine.lang.core.save,
                                class: "btn btn-primary",
                                click: function () {
                                    $('#CarYearsForm').submit();
                                }
                            }
                        ]
                    });
                }
            );
        },
        edit: function (id) {
            $.get(
                'CarYears/edit/' + id,
                function (d) {
                    engine.modal.dialog({
                        content: d,
                        title: 'Редагувати рік випуску',
                        buttons: [
                            {
                                text: engine.lang.core.save,
                                class: "btn btn-primary",
                                click: function () {
                                    $('#CarYearsForm').submit();
                                }
                            }
                        ]
                    });
                }
            );
        },
        del: function (id) {
            return engine.modal.confirm(
                'Ви збираєтесь видалити рік випуску. Продовжити?',
                function () {
                    $.get('CarYears/delete/' + id, function (d) {
                        if (d > 0) {
                            var oTable = $('#CarYears').dataTable();
                            oTable.fnDraw(oTable.fnSettings());
                            $(".modal").modal('hide');
                        }
                    });
                });
        }
    },
    modification: {
        create: function (vendor_id) {
            $.get(
                'CarModification/create/'+vendor_id,
                function (d) {
                    engine.modal.dialog({
                        content: d,
                        title: 'Додати модифікацію',
                        buttons: [
                            {
                                text: engine.lang.core.save,
                                class: "btn btn-primary",
                                click: function () {
                                    $('#CarModificationForm').submit();
                                }
                            }
                        ]
                    });
                }
            );
        },
        edit: function (id) {
            $.get(
                'CarModification/edit/' + id,
                function (d) {
                    engine.modal.dialog({
                        content: d,
                        title: 'Редагувати модифікацію',
                        buttons: [
                            {
                                text: engine.lang.core.save,
                                class: "btn btn-primary",
                                click: function () {
                                    $('#CarModificationForm').submit();
                                }
                            }
                        ]
                    });
                }
            );
        },
        del: function (id) {
            return engine.modal.confirm(
                'Ви збираєтесь видалити модифікацію. Продовжити?',
                function () {
                    $.get('CarModification/delete/' + id, function (d) {
                        if (d > 0) {
                            var oTable = $('#CarModification').dataTable();
                            oTable.fnDraw(oTable.fnSettings());
                            $(".modal").modal('hide');
                        }
                    });
                });
        }
    },
    products: {
        create: function (products_id) {
            $.get(
                'plugins/Car/createVariant/'+products_id,
                function (d) {
                    engine.modal.dialog({
                        content: d,
                        title: 'Додати варіант',
                        buttons: [
                            {
                                text: engine.lang.core.save,
                                class: "btn btn-primary",
                                click: function () {
                                    $('#carProductsForm').submit();
                                }
                            }
                        ]
                    });
                    var $vendor = $('#data_vendors_id'),
                        $model = $('#data_models_id'),
                        $year = $('#data_years_id'),
                        $modification = $('#data_modification_id');

                    $vendor.change(function(){
                        var vendor_id= this.value;
                        $model.html('').attr('disabled',true).select2();
                        $year.html('').attr('disabled',true).select2();
                        $modification.html('').attr('disabled',true).select2();

                        $.ajax({
                            url : 'plugins/Car/getModels/' + vendor_id,
                            type: 'post',
                            success: function(d)
                            {
                                if(d == '') {
                                    $model.attr('disabled', true).select2();
                                    return;
                                }

                                $model.html(d).removeAttr('disabled').trigger('change').select2();
                            }
                        });
                    }).trigger('change');

                    $model.change(function(){
                        var model_id= this.value;
                        $year.html('').attr('disabled',true).select2();
                        $modification.html('').attr('disabled',true).select2();

                        $.ajax({
                            url : 'plugins/Car/getYears/' + model_id,
                            type: 'post',
                            success: function(d)
                            {
                                if(d == '') {
                                    $year.attr('disabled', true).select2();
                                    return;
                                }
                                $year.html(d).removeAttr('disabled').trigger('change').select2();
                            }
                        });
                    });

                    $year.change(function(){
                        var year_id= this.value;
                        $modification.html('').attr('disabled',true).select2();

                        $.ajax({
                            url : 'plugins/Car/getModification/' + year_id,
                            type: 'post',
                            success: function(d)
                            {
                                if(d == '') {
                                    $modification.attr('disabled', true).select2();
                                    return;
                                }
                                $modification.html(d).removeAttr('disabled').trigger('change').select2();
                            }
                        });
                    });
                }
            );
        },

        del: function (id) {
            return engine.modal.confirm(
                'Ви збираєтесь видалити вибрану модель автомобіля. Продовжити?',
                function () {
                    $.get('plugins/Car/deleteVariant/' + id, function (d) {
                        if (d > 0) {
                            var oTable = $('#carProducts').dataTable();
                            oTable.fnDraw(oTable.fnSettings());
                            $(".modal").modal('hide');
                        }
                    });
                });
        }
    }
};