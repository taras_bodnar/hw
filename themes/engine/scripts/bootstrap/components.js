engine.components = {
    build : function()
    {

    },
    install : function(id)
    {
        $.get('components/install/'+id, function(d){
            engine.modal.dialog({
                content: d,
                title  : engine.components.install_title,
                buttons: [
                    {
                        text  : engine.lang.components.btn_install,
                        class : "btn btn-primary",
                        click : function(){
                            $('#formInstall').submit();
                        }
                    }
                ]
            });
        });
    },
    delete : function(id)
    {
        return engine.modal.confirm(
            engine.lang.components.remove_item,
            function(){
                $.get('components/delete/'+id,function(d){
                    if(d > 0){
                        var oTable = $('#components').dataTable();
                        oTable.fnDraw(oTable.fnSettings());
                        $("#tree").jstree("refresh");
                        $(".modal").modal('hide');
                    }
                });
            });
    },
    delSelected: function(){
        engine.modal.alert('Delete all action');
        return false;
    },
    pub : function(id,s)
    {
        $.get('components/pub/'+id+'/'+s+'/',function(d){
            if(d > 0){
                $.pnotify({
                    title: engine.lang.core.info,
                    type: 'success',
                    history: false,
                    text: engine.lang.core.status_changed
                });
                var oTable = $('#components').dataTable();
                oTable.fnDraw(oTable.fnSettings());
            }
        });
    }
};