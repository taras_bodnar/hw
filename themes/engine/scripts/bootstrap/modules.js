
engine.modules = {
    build : function(){
//        if($('#modules_wrapper'))
    },
    install : function(name)
    {
        $.getJSON('modules/install/'+name,function(d){
            if(d.s > 0){
                $.pnotify({
                    title: engine.lang.core.success,
                    type: '',
                    history: false,
                    text: engine.lang.modules.install_success
                });
                setTimeout(function(){location.reload(true);}, 1500);
            } else{
                engine.alert(engine.lang.core.error, d.m,'error');
            }
        });
    },
    uninstall : function(name)
    {
        return engine.modal.confirm(
            engine.lang.modules.uninstall_confirm,
            function(){
                $.getJSON('modules/uninstall/'+name,function(d){
                    if(d.s > 0){
                        $(".modal").modal('hide');
                        $.pnotify({
                            title: engine.lang.core.success,
                            type: '',
                            history: false,
                            text: engine.lang.modules.uninstall_success
                        });
                        setTimeout(function(){location.reload(true);}, 1500);
                    } else{
                        engine.alert(engine.lang.core.error, d.m,'error');
                    }
                });
            });
    },
    edit: function(id)
    {
        $.ajax({
            type: "GET",
            url:'modules/edit/'+id,
            success: function(d){
                engine.modal.dialog({
                    content: d,
                    title  : engine.lang.modules.edit_title,
                    buttons: [
                        {
                            text  : engine.lang.core.save,
                            class : "btn btn-primary",
                            click : function(){
                                $('#settingForm').submit();
                            }
                        }
                    ]
                });
            },
            dataType: 'html'
        });
    }
};