var verboseBuild = !0, screenXs = 480, screenMd = 992, ltIE9 = !1, log = function(msg){};
!$("html").is(".lt-ie9") || (ltIE9 = !0), ltIE9 && (verboseBuild = !1), Modernizr.addTest("ipad", function () {
    return!!navigator.userAgent.match(/iPad/i)
}), Modernizr.addTest("iphone", function () {
    return!!navigator.userAgent.match(/iPhone/i)
}), Modernizr.addTest("ipod", function () {
    return!!navigator.userAgent.match(/iPod/i)
}), Modernizr.addTest("appleios", function () {
    return Modernizr.ipad || Modernizr.ipod || Modernizr.iphone
}), Modernizr.appleios && $("html").addClass("ios-device"), !verboseBuild ||
    log("Starting builds:"), $(document).ready(function () {
    !verboseBuild || log("-- starting engine.common build"), engine.common.build()
}), engine.common = {build: function () {
    engine.common.events(), engine.common.enableTooltips(), engine.common.enableScrollSpy();

    var a, b = 50;

    $(window).resize(function () {
        clearTimeout(a), a = setTimeout(function () {
            engine.common.onResizeEnd()
        }, b)
    }), ltIE9 || Modernizr.mq("(min-width:" + screenXs + "px)") ? (setTimeout(function () {
        $(".sidebar").addClass("animated fadeInLeft"), setTimeout(function () {
            $(".sidebar").removeClass("animated fadeInLeft").css("opacity", "1")
        }, 1050)
    }, 50), setTimeout(function () {
        $("body:not(.login-page) .wrapper").addClass("animated fadeInRight"), setTimeout(function () {
            $("body:not(.login-page) .wrapper").removeClass("animated fadeInRight").css("opacity", "1")
        }, 1050)
    }, 150)) : setTimeout(function () {
        $(".sidebar, .wrapper").addClass("animated fadeInUp"), setTimeout(function () {
            $(".sidebar, .wrapper").removeClass("animated fadeInUp").css("opacity", "1")
        }, 1050)
    }, 50), !verboseBuild || log("            engine.common build DONE")
}, events: function () {
    !verboseBuild || log("            engine.common binding events"), $(document).on("touchmove", function (a) {
        a.preventDefault()
    }), $("body").on("touchmove", ".scrollable, nav", function (a) {
        a.stopPropagation()
    }), $("body").on("touchstart", ".scrollable", function (a) {
        0 === a.currentTarget.scrollTop ? a.currentTarget.scrollTop = 1 : a.currentTarget.scrollHeight === a.currentTarget.scrollTop + a.currentTarget.offsetHeight && (a.currentTarget.scrollTop -= 1)
    })
}, onResizeEnd: function () {
    !verboseBuild || log("Window size changed"), !verboseBuild || log("            engine.common.onResizeEnd()"), !engine.userNav || engine.userNav.shuffleUserNav(), setTimeout(function () {
        !(engine.graphsStats && engine.graphsStats.redrawCharts) || engine.graphsStats.redrawCharts(), !(engine.userProfile && engine.userProfile.redrawCharts) || engine.userProfile.redrawCharts()
    }, 1e3), !engine.sidebar || engine.sidebar.retractOnResize(), !engine.sidebar || engine.sidebar.setSidebarMobHeight()
}, enableTooltips: function () {
    !verboseBuild || log("            engine.common.enableTooltips()"), $(".uses-tooltip").tooltip({container: "body"}), $(".progress-bar").each(function () {
        var a = Math.round(100 * (parseInt($(this).css("width")) / parseInt($(this).parent().css("width")))) + "%";
        $(this).tooltip({container: "body", title: a})
    })
}, enableScrollSpy: function () {
}}, $(document).ready(function () {
    !verboseBuild || log("-- starting engine.mainNav build"), engine.mainNav.build()
}), engine.mainNav = {build: function () {
    engine.mainNav.events(), !verboseBuild || log("            engine.mainNav build DONE")
}, events: function () {
    !verboseBuild || log("            engine.mainNav binding events"), $(".touch nav.main-menu").on("click", function () {
        return $(this).addClass("expanded"), !1
    }), $(".main-menu-access").on("click", function () {
        return $("nav.user-menu > section .active").removeClass("active"), $(".nav-view").fadeOut(30), $(this).is(".active") ? ($(this).removeClass("active"), $("nav.main-menu").removeClass("expanded")) : ($("nav.main-menu").addClass("expanded"), $(this).addClass("active")), !1
    }), $(".touch body").on("click touchstart", function () {
        $(".touch nav.main-menu").is(".expanded") && ($(".main-menu-access").removeClass("active"), $("nav.main-menu").find(".active").removeClass("active"), $(".touch nav.main-menu").removeClass("expanded"), $("html, body").animate({scrollTop: 0}, 300, "swing")), $("nav.user-menu > section .active").removeClass("active"), $(".nav-view").fadeOut(30)
    }), $(".touch nav.main-menu, .touch nav.user-menu").on("touchstart", function (a) {
        a.stopPropagation()
    }), $(".touch nav.main-menu ul").on("click", "li", function (a) {
        return $(this).is(".active") ? ($(this).removeClass("active"), a.stopPropagation(), void 0) : ($("nav.main-menu").find("li").removeClass("active"), $(this).addClass("active"), void 0)
    })
}}, $(document).ready(function () {
    !verboseBuild || log("-- starting engine.userNav build"), engine.userNav.build()
}), engine.userNav = {build: function () {
    engine.userNav.events(), engine.userNav.shuffleUserNav(), setTimeout(function () {
        engine.userNav.bounceCounter()
    }, 3e3), !verboseBuild || log("            engine.userNav build DONE")
}, events: function () {
    !verboseBuild || log("            engine.userNav binding events"), $(document).on("click", ".user-menu-wrapper a", function () {
        var a = $(this).attr("data-expand");
        return $(this).is(".unread") && ($(this).removeClass("unread"), $(this).find(".menu-counter").fadeOut("100", function () {
            $(this).remove()
        })), $("nav.main-menu").removeClass("expanded"), $(".main-menu-access").removeClass("active"), $("nav.user-menu > section .active").not(this).removeClass("active"), $(this).toggleClass("active"), $(".nav-view").not(a).fadeOut(60), setTimeout(function () {
            $(a).fadeToggle(60)
        }, 60), !1
    }), $(document).on("click", ".close-user-menu", function () {
        $("nav.user-menu > section .active").removeClass("active"), $(".nav-view").fadeOut(30)
    }), $(document).on("click", ".theme-view li", function () {
        var a = $(this).attr("data-theme");
        $("body").removeClass(function (a, b) {
            return(b.match(/\btheme-\S+/g) || []).join(" ")
        }), $.cookie("engineTheme", a, {expires: 7, path: "/"}), "default" !== a && $("body").addClass(a)
    })
}, shuffleUserNav: function () {
    !verboseBuild || log("            engine.userNav.shuffleUserNav()"), ltIE9 || Modernizr.mq("(min-width:" + screenXs + "px)") ? $("body > .user-menu").prependTo(".wrapper") : $(".wrapper .user-menu").prependTo("body")
}, bounceCounter: function () {
    $(".menu-counter").length && ($(".menu-counter").toggleClass("animated bounce"), setTimeout(function () {
        $(".menu-counter").toggleClass("animated bounce")
    }, 1e3), setTimeout(function () {
        engine.userNav.bounceCounter()
    }, 5e3))
}};

String.prototype.replaceAll = function(search, replace){
    return this.split(search).join(replace);
}

engine.imageGallery = {
    build: function () {
           if($('#imageGalleryDropzone').length == 0) return;
           // Initiate imageGallery events
           engine.imageGallery.events();

            Dropzone.options.imageGalleryDropzone = false; // Prevent Dropzone from auto discovering this element
            var content_id = $('#content_id').val();
            var dropZoneTemplate = $.get('./content/images/getTemplate/'+content_id, function(template) {
                engine.imageGallery.makeDropzone(template); // Make Dropzone after loading template html
            })
            .fail(function() {
                alert( "Image Gallery Error: could not load gallery html template" );
            });
    },
    events: function () {

        $('.gallery-uploader').on('click', '.remove-item', function(event) {
            event.preventDefault();
            $(this).parents('.dz-preview').fadeOut(250, function () {
                $(this).remove();
            });
        });

        $('.gallery-uploader').on('click', '.trash-item, .remove-cancel', function(event) {
            event.preventDefault();
            $(this).parents('.dz-preview').find('.controls').fadeToggle('150');
        });
        $('.gallery-uploader').on('click', '.add', function(event) {
            event.preventDefault();
            $(this).fadeOut(75, function () {
                $(this).parents('.gallery-uploader').toggleClass('active');
                $(this).siblings('.add').fadeIn(150);
            });
        });



        $('#edit-image-modal').on('click', '.btn-success', function(event) {
            $('.editing-item .dz-filename span').text($('#edit-image-modal #image-caption').val());
        });
    },
    makeDropzone: function (template) {
        var imagesContainer = $('#imageGalleryDropzone');
        var url = imagesContainer.data('target');
        imagesContainer.dropzone({
            paramName: "image", // The name that will be used to transfer the file
            maxFilesize: 10, // MB
            acceptedFiles: '.jpg,.jpeg,.png,.gif',
            uploadMultiple: true,
            previewsContainer: '.gallery-container',
            previewTemplate: template,
            parallelUploads:1,
            url: url,
            thumbnailWidth: 125,
            thumbnailHeight: 125,
            init: function() {
                this.on("addedfile", function(file) {
//                    console.log(file);
                });
//                this.on("success", function(file) {
//                    console.log(file);
//                });
            },
            success: function(file, data) {
                data = $.parseJSON(data);

                var div = $('.gallery-container > div:last');
                div.attr('id', data.id);
                var str = div.html()
                    .toString()
                    .replace (/{imageId}/g, data.id)
                    .replace (/{imageName}/g, data.name);
                div.html(str);
                return true;
            }
        });
    }
}
engine.modal = {
    time   : new Date().getTime(),
    tpl :   '<div class="modal fade" id="modal_%t%" tabindex="-1" role="dialog"  aria-hidden="true">' +
        '    <div class="modal-dialog">' +
        '        <div class="modal-content"> ' +
        '           <div class="modal-header"> ' +
        '               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> ' +
        '               <h4 class="modal-title" id="myModalLabel">%title%</h4>  ' +
        '          </div>' +
        '           <div class="modal-body">%body%</div>' +
        '          %footer%' +
        '        </div>' +
        '    </div>' +
        '</div>',
    footer: '<div class="modal-footer"></div>',
    alert: function(msg)
    {
        $('.modal,.modal-backdrop').remove();
        var modal = this.tpl
            .replace('%t%', this.time)
            .replace('%title%', engine.lang.core.info)
            .replace('%body%', msg)
            .replace('%footer%', '');

        $(modal).appendTo('body').modal('show');
    },
    confirm: function(msg, action)
    {
        $('.modal,.modal-backdrop').remove();
        var options  = {
            content : msg,
            buttons: [
                {
                    text  : engine.lang.core.btn_yes,
                    class : "btn btn-danger",
                    click : action
                },
                {
                    text  : engine.lang.core.btn_no,
                    click : function () {
                        $(".modal").modal('hide');
                    }
                }
            ]
        };

        return this.dialog(options);
    },
    dialog: function(options)
    {
        $('.modal,.modal-backdrop').remove();

        options  = jQuery.extend({
            title : engine.lang.core.info, // modal title
            effect : 'hide fade',
            autoOpen : true,
            content : '', // modal body
            buttons : {}
        }, options);

//        console.log(options);

        var $modal = $(
                        this.tpl
                        .replace('%t%',      this.time)
                        .replace('%title%',  options.title)
                        .replace('%body%',   options.content)
                        .replace('%footer%', this.footer)
                    );

        if($modal.find('.modal-footer').length == 0){
            $modal.find('.modal-content').append('<div class="modal-footer"></div>')
        }

        jQuery.each( options.buttons, function( name, props ) {

            props = jQuery.isFunction( props ) ? { click: props, html: name } : props;

            var click = props.click;
            delete props.click;

            var button = jQuery( '<button class="btn"></button>' )
                .attr( props, true )
                .text( props.text )
                .unbind( "click" )
                .click(function() {
                    click.apply( button, arguments );
                    return false;
                });

            $modal.find('.modal-footer').append(button);
        });

        $modal.appendTo('body').modal('show');
    }
};


engine.alert = function(title, message, status, element, mode)
{
    var i = '<i class="icon-check-sign"></i>';
    switch (status){
        case 'success':
            i = '<i class="icon-check-sign"></i>';
            break;
        case 'info':
            i = '<i class="icon-info-sign"></i>';
            break;
        case 'warning':
            i = '<i class="icon-warning-sign"></i>';
            break;
        case 'error':
            i = '<i class="icon-remove-sign"></i>';
            break;
        default :
            i = '<i class="icon-check-sign"></i>';
            break;
    }

    if(typeof element == 'undefined') element = '#notification';
//    if(typeof mode == 'undefined') mode = '';


    var alert = $('<div class="alert alert-dismissable alert-'+status+' fade in">' +
        '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">' +
        '<i class="icon-remove"></i>' +
        '</button>' +
        '<span class="title">'+ i +' '+ title +'</span>' + message +
        '</div>');

        if(mode == 'prependTo') {
            alert.prependTo(element)
                .delay(10000)
                .fadeOut();
        } else {
            $(element).html(alert);
             alert.delay(10000)
                .fadeOut();
        }
};

engine.translator = {
    translateAll: function(id, code, el)
    {
        var btn = $(el);
        btn.button('loading');
        var form = btn.parents('form');
        var data = form.find('[id*=info_]').serialize();
        data += '&'+ $('.s-languages').serialize();
        this.translate(id, code, data, btn, form);

    },
    translateSelected: function (id, code, el)
    {
        var btn = $(el);
        btn.button('loading');

        var data =  $('[id^=info_'+id+'],[id^=info_]:visible').serialize();
        data += '&'+ $('.s-languages').serialize();

        this.translate(id, code, data, btn);
    },
    translateValue: function (id, code, el)
    {
        var btn = $(el);
        btn.button('loading');

        var data =  $(el).parent('div.col-xs-3').next('.col-xs-7').find('[id^=options_value_info_],[id^=new_options_value_info_]').serialize();
        data += '&'+ $('.s-languages').serialize();

        $.post('./translator/translateOptionsValue/'+code+'/', data ,
            function(d){
                $(d).each(function(i,e){
                    $('#' + e.n).val(e.v).trigger('change');
                });
                btn.button('complete');
                $.pnotify({
                    title: engine.lang.core.info,
                    type: '',
                    history: false,
                    text: engine.lang.core.translation_complete
                });
            },'json');
    },
    translateNewValue: function (id, code, el)
    {
        var btn = $(el);
        btn.button('loading');
        var data = $('#newValue').find('[id^=value_]').serialize();
        data += '&'+ $('.s-languages').serialize();

        $.post('./translator/translateOptionsValue/'+code+'/', data ,
            function(d){
                $(d).each(function(i,e){
                    $('#' + e.n).val(e.v).trigger('change');
                });
                btn.button('complete');
                $.pnotify({
                    title: engine.lang.core.info,
                    type: '',
                    history: false,
                    text: engine.lang.core.translation_complete
                });
            },'json');
    },
    translateNewOptionName: function (id, code, el)
    {
        var btn = $(el);
        btn.button('loading');
        var data = $('#newValue').find('[id^=value_]').serialize();
        data += '&'+ $('.s-languages').serialize();

        $.post('./translator/translateOptionsValue/'+code+'/', data ,
            function(d){
                $(d).each(function(i,e){
                    $('#' + e.n).val(e.v).trigger('change');
                });
                btn.button('complete');
                $.pnotify({
                    title: engine.lang.core.info,
                    type: '',
                    history: false,
                    text: engine.lang.core.translation_complete
                });
            },'json');
    },
    translate: function(id, code, data, btn, form)
    {
        $.ajax({
            type: "POST",
            url: './translator/translateAll/'+id+'/'+code+'/',
            data: data,
            success: function(d){
                $(d).each(function(i,e){
                    form.find('#' + e.n).val(e.v).trigger('change');
                });
                btn.button('complete');
                $.pnotify({
                    title: engine.lang.core.info,
                    type: '',
                    history: false,
                    text: engine.lang.core.translation_complete,
                    before_open: function(pnotify){
                        pnotify.css({
                            "top":"50px",
                            "left": ($(window).width() / 2) - (pnotify.width() / 2)
                        });
                    }
                });
            },
            dataType: 'json'
        });
    },
    translateModalOptionsName: function (id, code, el)
    {
        var btn = $(el);
        btn.button('loading');

        var data =  $(el)
            .parents('form#modalOptionsForm')
            .find('[id^=info_]')
            .serialize();
        data += '&'+ $('.ms-languages').serialize();

        $.post('./translator/translateModalOptionsName/'+code+'/', data ,
            function(d){
                $(d).each(function(i,e){
//                    console.log(e.n, e.v);
                    $('#modalOptionsForm').find('#' + e.n).val(e.v);//.trigger('change');
                });
                btn.button('complete');
                $.pnotify({
                    title: engine.lang.core.info,
                    type: '',
                    history: false,
                    text: engine.lang.core.translation_complete
                });
            },'json');
    }
};
engine.content = {};

engine.pages = {
    delete : function(id)
    {
        return engine.modal.confirm(
            engine.lang.pages.remove_item,
            function(){
                $.get('pages/delete/'+id,function(d){
                    if(d > 0){
                        var oTable = $('#pages').dataTable();
                        oTable.fnDraw(oTable.fnSettings());
                        $("#tree").jstree("refresh");
                        $(".modal").modal('hide');
                    }
                });
            });
    },
    delSelected: function(){
        engine.modal.alert('Delete all action');
        return false;
    },
    pub : function(id,s)
    {
        $.get('content/pub/'+id+'/'+s+'/',function(d){
            if(d > 0){
                $.pnotify({
                    title: engine.lang.core.info,
                    type: 'success',
                    history: false,
                    text: engine.lang.core.status_changed
                });
                var oTable = $('#pages').dataTable();
                oTable.fnDraw(oTable.fnSettings());
            }
        });
    },
    mkAlias: function(target,id,languages_id,languages_code,text)
    {
        $.post('content/mkAlias/'+id+'/'+languages_id+'/'+languages_code+'/',{alias: text}, function(t){
            target.val(t).trigger('change');
        })
    }
};

/**
 * Content type
 * @type {{delete: delete}}
 */
engine.content.type = {
    delete : function(id)
    {
        return engine.modal.confirm(
            engine.lang.content_type.remove_item,
            function(){
                $.get('content/type/delete/'+id,function(d){
                    if(d > 0){
                        var oTable = $('#content_type').dataTable();
                        oTable.fnDraw(oTable.fnSettings());
                        $(".modal").modal('hide');
                    }
                });
            });
    }
};

engine.languages = {
    delete : function(id)
    {
        return engine.modal.confirm(
            engine.lang.languages.remove_item,
            function(){
                $.get('languages/delete/'+id,function(d){
                    if(d > 0){
                        var oTable = $('#languages').dataTable();
                        oTable.fnDraw(oTable.fnSettings());
                        $(".modal").modal('hide');
                    }
                });
            });
    },
    autoGenerateAlias: function(lang)
    {
        $.get('languages/generateAlias/'+lang, function(d){
            $('#notification').html(d);
        });
    }
};

engine.content.templates = {
    delete : function(id)
    {
        return engine.modal.confirm(
            engine.lang.content_templates.remove_item,
            function(){
                $.get('content/templates/delete/'+id,function(d){
                    if(d > 0){
                        var oTable = $('#content_templates').dataTable();
                        oTable.fnDraw(oTable.fnSettings());
                        $(".modal").modal('hide');
                    }
                });
            });
    }
};
engine.translations = {
    delete : function(id)
    {
        return engine.modal.confirm(
            engine.lang.translations.remove_item,
            function(){
                $.get('translations/delete/'+id,function(d){
                    if(d > 0){
                        var oTable = $('#translations').dataTable();
                        oTable.fnDraw(oTable.fnSettings());
                        $(".modal").modal('hide');
                    }
                });
            });
    }
};

engine.guides = {
    delete : function(id)
    {
        return engine.modal.confirm(
            engine.lang.guides.remove_item,
            function(){
                $.get('guides/delete/'+id,function(d){
                    if(d > 0){
                        var oTable = $('#guides').dataTable();
                        oTable.fnDraw(oTable.fnSettings());
                        $(".modal").modal('hide');
                    }
                });
            });
    }
};

engine.content.chunks = {
    delete : function(id)
    {
        return engine.modal.confirm(
            engine.lang.chunks.remove_item,
            function(){
                $.get('content/chunks/delete/'+id,function(d){
                    if(d > 0){
                        var oTable = $('#chunks').dataTable();
                        oTable.fnDraw(oTable.fnSettings());
                        $(".modal").modal('hide');
                    }
                });
            });
    }
};
engine.routers = {
    delete : function(id)
    {
        return engine.modal.confirm(
            engine.lang.routers.remove_item,
            function(){
                $.get('routers/delete/'+id,function(d){
                    if(d > 0){
                        var oTable = $('#routers').dataTable();
                        oTable.fnDraw(oTable.fnSettings());
                        $(".modal").modal('hide');
                    }
                });
            });
    }
};

engine.images ={};
engine.images.sizes = {
    delete : function(id)
    {
        return engine.modal.confirm(
            engine.lang.images_sizes.remove_item,
            function(){
                $.get('content/images/sizes/delete/'+id,function(d){
                    if(d > 0){
                        var oTable = $('#images_sizes').dataTable();
                        oTable.fnDraw(oTable.fnSettings());
                        $(".modal").modal('hide');
                    }
                });
            });
    },
    crop : function(id)
    {
        return engine.modal.confirm(
            engine.lang.images_sizes.crop_images,
            function(){
                $.get('content/images/sizes/crop/'+id,function(d){
                    $('#notification').html(d);
                    $(".modal").modal('hide');
                });
            });
    }
};

engine.content.images = {
    crop: function(content_id,image_id)
    {
        $.get('content/images/crop/'+content_id + '/' + image_id,function(d){
            engine.modal.dialog({
                content: d,
                buttons: [
                    {
                        text  : engine.lang.core.save,
                        class : "btn btn-primary",
                        click : function(){
                            $('#imagesCrop').submit();
                        }
                    }
                ]
            });
        });
    },
    delete : function(id)
    {
        $.get('content/images/delete/'+id,function(d){
            if(d > 0){
                $('.dz-file-preview#' + id).fadeOut(300, function(){$(this).remove();});
            }
        });
    },
    editInfo: function(id)
    {
        $.get('content/images/editInfo/'+id,function(d){
            engine.modal.dialog({
                content: d,
                buttons: [
                    {
                        text  : engine.lang.core.save,
                        class : "btn btn-primary",
                        click : function(){
                            $('#imagesInfo').submit();
                        }
                    }
                ]
            });
        });
    },
    crop: function(content_id,image_id)
    {
        $.get('content/images/crop/'+content_id + '/' + image_id,function(d){
            engine.modal.dialog({
                content: d,
                buttons: [
                    {
                        text  : engine.lang.core.save,
                        class : "btn btn-primary",
                        click : function(){
                            $('#imagesCrop').submit();
                        }
                    }
                ]
            });
        });
    }
};

engine.themes = {
    activate: function(theme)
    {
        return engine.modal.confirm(
            engine.lang.themes.confirm_activate,
            function(){
                $.get('themes/activate/'+theme, function(d){
                    if(d > 0){
                        window.location.reload(true);
                    }
                });
            });
    }
};

engine.nav = {
    delete : function(id)
    {
        return engine.modal.confirm(
            engine.lang.nav.remove_item,
            function(){
                $.get('nav/delete/'+id,function(d){
                    if(d > 0){
                        var oTable = $('#nav_menu').dataTable();
                        oTable.fnDraw(oTable.fnSettings());
                        $(".modal").modal('hide');
                    }
                });
            });
    }
};
engine.emailTemplates = {
    delete : function(id)
    {
        return engine.modal.confirm(
            engine.lang.EmailTemplates.remove_item,
            function(){
                $.get('EmailTemplates/delete/'+id,function(d){
                    if(d > 0){
                        var oTable = $('#EmailTemplates').dataTable();
                        oTable.fnDraw(oTable.fnSettings());
                        $(".modal").modal('hide');
                    }
                });
            });
    }
};

engine.users = {
    delete : function(id)
    {
        return engine.modal.confirm(
            engine.lang.users.remove_item,
            function(){
                $.get('users/delete/'+id,function(d){
                    if(d > 0){
                        var oTable = $('#users').dataTable();
                        oTable.fnDraw(oTable.fnSettings());
                        $(".modal").modal('hide');
                    }
                });
            });
    },
    group: {
        delete : function(id)
        {
            return engine.modal.confirm(
                engine.lang.users_group.remove_item,
                function(){
                    $.get('UsersGroup/delete/'+id,function(d){
                        if(d > 0){
                            var oTable = $('#users_group').dataTable();
                            oTable.fnDraw(oTable.fnSettings());
                            $("#tree").jstree("refresh");
                            $(".modal").modal('hide');
                        }
                    });
                });
        }
    }
};
engine.dashboard = {
    build: function () {
        engine.dashboard.loadWidgetPositions()
        engine.dashboard.setBlankWidgets();
        engine.dashboard.widgetSort();

        var t = 300;
        $(".widget-group>div").each(function(){
            var $t = $(this);
            t+=500;
            setTimeout(function(){
                $t.animate({opacity:1}, 300);
            }, t);
        });

        // Morris Charts
    },
    quickLaunchConfig : function()
    {
        $.get('dashboard/quickLaunchConfig',function(d){
            engine.modal.dialog({
                content: d,
                title  : engine.dashboard.ql_modal_title,
                buttons: [
                    {
                        text  : engine.lang.core.save,
                        class : "btn btn-primary",
                        click : function(){
                            $('#quickLaunchConfigForm').submit();
                        }
                    }
                ]
            });
        });
    },
    quickLaunchConfigProcess: function(d)
    {
        if(d > 0){
            $(".modal").modal('hide');
            this.refreshQuickLaunch();
        }
        return false;
    },
    refreshQuickLaunch: function()
    {
        var self = this;
        var ul = $('#quick_launch_items');
        ul.html('');
        $.getJSON('dashboard/refreshQuickLaunch', function(d)
        {
            $(d.items).each(function(i,e){
                $('<li id="'+ e.id+'" class="animated fadeIn"><a href="'+ e.url +'"><i class="'+ e.icon +'"></i><span>'+ e.name +'</span></a></li>').appendTo(ul);
            });
            self.quickLaunchInitSortable();
        });
    },
    quickLaunchInitSortable: function()
    {
        $("#quick_launch_items").sortable({
            update: function(event, ui) {
                var newOrder = $(this).sortable('toArray').toString();
                $.get('dashboard/quickLaunchSort/'+newOrder);
            },
            placeholder: "drop-here"
        }).disableSelection();
    },

        widgetPositions : [],
        loadWidgetPositions : function () {
        var positionArray = engine.dashboard.widgetPositions;
        var positionsFromCookie = $.cookie('engine_widgetPositions') || false;
        if(positionsFromCookie){
            positionArray = positionsFromCookie.split(',');
            $.each(positionArray, function(index, val) {
                $('#' + val).appendTo('.widget-group');//.animate({opacity:1}, 300);
            });
        }
        else engine.dashboard.saveWidgetPositions();
    },
    saveWidgetPositions : function () {
//        !verboseBuild || console.log('            engine.dashboard.saveWidgetPositions()');

        var positionArray = engine.dashboard.widgetPositions = [];
        $('.engine-widget').not('.placeholder').each(function(index, el) {
            var wid = $(el).attr('id');
            positionArray.push(wid);
        });
        $.cookie('engine_widgetPositions', positionArray, {
            expires: 365,
            path: '/'
        });
    },
    widgetSort : function () {
//        !verboseBuild || console.log('            engine.dashboard.widgetSort()');

        engine.dashboard.isDragActive = false;
        $( ".widget-group" ).sortable({
            cancel: '.placeholder, .flip-it',
            placeholder: 'drag-placeholder',
            start: function(event, ui) {
                engine.dashboard.isDragActive = true;
                $('.tooltip').tooltip('hide');
            },
            stop: function(event, ui) {
                engine.dashboard.saveWidgetPositions();
                engine.dashboard.isDragActive = false;
            },
            tolerance: 'pointer',
            handle: ".panel-heading"
        });
    },
    setBlankWidgets: function () {

        var realWidgetNum = $('.engine-widget').not('.placeholder').length;
        var placeholderNum = $('.engine-widget.placeholder').length;

        var availableWidth = $('.widget-group').width();
        var widgetWidth = $('.engine-widget').outerWidth(true);
        var widgetsPerRow = Math.floor(availableWidth / widgetWidth);
        var widgetRows = Math.ceil(realWidgetNum / widgetsPerRow);

        var newPlaceholderNum = (widgetRows * widgetsPerRow) - realWidgetNum;

        $('.engine-widget.placeholder').appendTo('.widget-group');
        if(newPlaceholderNum === placeholderNum){
            return;
        }
        if(newPlaceholderNum <= placeholderNum){
            for (var i = placeholderNum - newPlaceholderNum; i > 0; i--) {
                $('.engine-widget.placeholder').last().remove();
            }
            return;
        }
        if(newPlaceholderNum >= placeholderNum){
            for (var i = newPlaceholderNum - placeholderNum; i > 0; i--) {
                $('<div class="engine-widget placeholder lit"></div>').appendTo('.widget-group');
            }
            return;
        }
    }

};
engine.updates = {
   get : function()
   {
       //notifications
       $.get('update/get', function(d){
           $('#notifications').html(d);
       });
   },
    disable : function()
    {
        $.get('update/disable', function(d){
            $('#notifications > div').fadeOut(300, function(){$(this).remove()});
        });
    }
};

$(document).ready(function() {
    
    engine.dashboard.build();
    engine.imageGallery.build();

    if(typeof engine.products !== 'undefined'){
        engine.products.build();
    }
});