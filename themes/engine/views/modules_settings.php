<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 28.08.14 21:24
 */

use \controllers\engine\Form;

defined("SYSPATH") or die();

Form::open("modules/updateSettings/$id", array('id'=>'settingForm'));
    foreach($settings as $row){
        Form::formGroup(
            $row['description'],
            '',
            Form::input(array(
                'type'        => 'text',
                'name'        => "data[{$row['name']}]",
                'required'    => 'required',
                'value'       => $row['value']
            ))
        );
    }
Form::customSuccessAction("
    if(d>0){
        $('.modal').modal('hide');
        $.pnotify({
                title: engine.lang.core.success,
                type: '',
                history: false,
                text: engine.lang.modules.settings_save
        });
    }
");
Form::close();
