<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 20.12.14 23:52
 */
defined("SYSPATH") or die();
?>
<div class="panel panel-default panel-block ">
    <div class="list-group">
        <div class="list-group-item">
            <h4 class="section-title">Підходить для наступних моделей авомобілів</h4>
            <div class="form-group ">
                <div style="text-align: right;"><?=$btn_add?></div>
                <br/>
                <?=$table?>
            </div>
        </div>
    </div>
</div>