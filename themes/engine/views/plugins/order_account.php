<div id="plan-history-wrapper" class="panel panel-default panel-block ">
    <div class="list-group">
        <div class="list-group-item">
            <h4 class="section-title">Історія замовлень</h4>
            <div class="form-group ">
                <div style="text-align: right;">
                    <button type="button" class="btn btn-primary" id="add-plan">Додати</button>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <tr>
                        <th>Код</th>
                        <th>Телефон</th>
                        <th>Сума оплати</th>
                        <th>Тип плану</th>
                        <th>Активний</th>
                        <th>Оплачено</th>
                        <th>Дата оплати</th>
                        <th>Дата завершення</th>
                        </tr>
                        <tbody>
                        <?php $status = array("Ні","Так");
                        foreach($items as $item) :  ?>
                            <tr id="<?=$item['id']?>">
                                <td style="padding: 5px;"><?=$item['code']?></td>
                                <td style="padding: 5px;"><?=$item['phone']?></td>
                                <td style="padding: 5px;">UAH <?=$item['price']?></td>
                                <td style="padding: 5px;"><?=$type[$item['type']]?></td>
                                <td style="padding: 5px;"><?=$status[$item['status_id']]?></td>
                                <td style="padding: 5px;"><?=$status[$item['pay']]?></td>
                                <td style="padding: 5px;"><?=$item['pay_date']?></td>
                                <td style="padding: 5px;"><?=$item['end_date']?></td>
                                <td style="padding: 5px;  text-align: center;vertical-align: middle">
                                    <?php if(!empty($item['status_id'])) :?>
                                        <i class="remove-order-hist icon-remove"
                                           style="cursor: pointer; color: red"></i>
                                        <?php endif; ?>

                                        <?php if(empty($item['status_id'])) : ?>
                                        <i alt="Активувати" class="activate-order-hist icon-plus-sign"
                                           style="cursor: pointer; color: green"></i>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $(document).on(
            'click',
            '.remove-order-hist',
            function(){
                var id=$(this).parents('tr').attr('id'), self=$(this);
                return engine.modal.confirm(
                    'Ви збираєтесь деактивувати замовлення тарифу. Продовжити?',
                    function () {
                        $.get('plugins/OrderAccounts/deleteItem/'+id,function(d){
                            if(d>0){
                                $("#plan-history-wrapper").load("plugins/OrderAccounts/edit/<?=$uid?>");
                                $('.modal').modal('hide');
                            }
                        });
                    });
            }
        );

        $(document).on(
            'click',
            '.activate-order-hist',
            function(){
                var id=$(this).parents('tr').attr('id'), self=$(this);
                return engine.modal.confirm(
                    'Ви збираєтесь активувати замовлення тарифу. Продовжити?',
                    function () {
                        $.get('plugins/OrderAccounts/activateItem/'+id,function(d){
                            if(d>0){
                                $("#plan-history-wrapper").load("plugins/OrderAccounts/edit/<?=$uid?>");
                                $('.modal').modal('hide');
                            }
                        });
                    });
            }
        );

        $(document).on('click','#add-plan',function(){
            $.ajax({
                type: 'post',
                url: 'plugins/OrderAccounts/add',
                data:({user_id:<?=$uid?>}),
                success: function(d)
                {
                    engine.modal.dialog({
                        title  : 'Створити план',
                        content: d,
                        buttons: [
                            {
                                text  : engine.lang.core.edit,
                                class : "btn btn-success",
                                click : function(){
                                    $('#addPlanForm').submit();
                                }
                            }
                        ],
                        bottombtn:true
                    });

                    $('#addPlanForm').ajaxForm({
                        target: '#responce',
                        type: 'post',
                        dataType: 'json',
                        success: function (d) {
                            if(d.s==1){
                                $("#plan-history-wrapper").load("plugins/OrderAccounts/edit/<?=$uid?>");
                                $('.modal').modal('hide');
                            } else {
                                $("#error_str").html(d.e);
                            }
                        }
                    });
                }
            });
        });
    });
</script>