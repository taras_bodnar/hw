<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 20.12.14 23:52
 */
defined("SYSPATH") or die();
?>
<div class="panel panel-default panel-block ">
    <div class="list-group">
        <div class="list-group-item" id="меню">
            <h4 class="section-title">Період подорожі</h4>
            <div class="form-group ">
                <input name="travel_date" type="text" value="<?php if(isset($travel_date)) echo $travel_date; ?>" id="maxPicks">
            </div>
        </div>
    </div>
</div>

<script>
 $(document).ready(function(){
     $('#maxPicks').multiDatesPicker({
         maxPicks: 2,
         minDate: 0,
         dateFormat: 'yy-mm-dd'
     });

 })
</script>