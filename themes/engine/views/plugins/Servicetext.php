<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 20.12.14 23:52
 */
defined("SYSPATH") or die();
?>

<div class="panel panel-default panel-block ">
    <div class="list-group">
        <div class="list-group-item" id="">
            <h4 class="section-title">Параметри</h4>
            <script type="text/javascript">
                $(document).ready(function() {
                    $(".js-example-basic-single").select2();
                });
            </script>
            <div class="form-group ">
                <label for="sel1">Оберіть спеціалізацію</label>
                <select class="js-example-basic-single" id="sel1" name="service">
                    <?php foreach ($service as $item) { ?>
                        <option <?=$item['selected']?> value="<?=$item['id'] ?>"><?= $item['name'] ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group ">
                <label for="sel2">Оберіть місто</label>
                <select class="js-example-basic-single" id="sel2" name="city">
                    <?php foreach ($city as $item) { ?>
                        <option <?=$item['selected']?> value="<?= $item['id'] ?>"><?= $item['name'] ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
    </div>
</div>