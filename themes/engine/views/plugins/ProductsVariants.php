<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 20.12.14 23:52
 */
defined("SYSPATH") or die();
?>
<div class="panel panel-default panel-block ">
    <div class="list-group">
        <div class="list-group-item">
            <h4 class="section-title">Варіанти товару</h4>
            <div class="form-group ">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead class="">
                        <tr>
                            <th>#</th>
                            <th>Назва варіанту</th>
                            <th>Артикул</th>
                            <th>Ціна</th>
                            <th>Кількість</th>
                        </tr>
                        </thead>
                        <tbody id="varaibles">
                        <?php foreach($items as $item) : ?>
                            <tr id="<?=$item['id']?>">
                                <td><i class="icon-reorder" style="cursor: move;opacity:0.5;line-height: 35px"></i></td>
                                <td>
                                    <input type="hidden" name="variants[id][]" value="<?=$item['id']?>" />
                                    <input name="variants[name][]" value="<?=$item['name']?>" placeholder="введіть назву" class="inp-name form-control" type="text"/>
                                </td>
                                <td><input name="variants[code][]" value="<?=$item['code']?>" placeholder="[a-zA-Z0-9]+" class="form-control" type="text"/></td>
                                <td><input name="variants[price][]" value="<?=$item['price']?>" placeholder="0.00" class="form-control" type="text"/></td>
                                <td>
                                    <input name="variants[quantity][]" value="<?=$item['quantity']?>" placeholder="0 шт." class="form-control " style="width: 70%; float: left; " type="text"/>
                                    <i class="icon-remove remove-variable" data-id="<?=$item['id']?>" style="margin-left: 10px;line-height: 35px; cursor: pointer; color: red"></i>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        <tr class="v-template hide">
                            <td><i class="icon-reorder" style="cursor: move;opacity:0.5;line-height: 35px" id=""></i></td>
                            <td>
                                <input type="hidden" name="variants[id][]" />
                                <input name="variants[name][]" placeholder="введіть назву" class="inp-name form-control" type="text"/>
                            </td>
                            <td><input name="variants[code][]" placeholder="[a-zA-Z0-9]+" class="form-control" type="text"/></td>
                            <td><input name="variants[price][]" placeholder="0.00" class="form-control" type="text"/></td>
                            <td>
                                <input name="variants[quantity][]" placeholder="0 шт." class="form-control " style="width: 70%; float: left; " type="text"/>
                                <i class="icon-remove remove-variable" style="margin-left: 10px;line-height: 35px; cursor: pointer; color: red"></i>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <button class="btn btn-success" type="button" id="addVariable">Додати варіант</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    engine.products.variants = {
        init: function()
        {
            $(document).on(
                'click',
                '#addVariable',
                function(){
                    var tpl = $('.v-template').clone();
                    tpl.removeClass('v-template hide').find('td:eq(1)').find('input.inp-name').attr('required', true);
                    $('#varaibles').append(tpl);
                }
            );
            $(document).on(
                'click',
                '.remove-variable',
                function(){
                    var id=$(this).data('id'), self=$(this);
                    if(typeof id != 'undefined'){
                        $.get('plugins/ProductsVariants/deleteItem/'+id,function(d){
                            if(d>0){self.parents('tr').remove();}
                        });

                    } else{
                        $(this).parents('tr').remove();
                    }
                }
            );

            $("#varaibles").sortable({
                handle: ".icon-reorder",
                update: function (event, ui)
                {
//                    var o = $(this).sortable('toArray').toString();
//                    $.get('table/sort/products_variants/id/sort/' + o);
                }
            });
        }
    };

    $(document).ready(function(){
        engine.products.variants.init();
    });
</script>