<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 01.06.14 10:46
 */
use \controllers\engine\Form;

defined('SYSPATH') or die();

Form::open("plugins/Car/createProcess", array('id' => 'carProductsForm'));
Form::html('<span class="response"></span>');
Form::formGroup(
    'Марка',
    '',
    Form::select(
        array(
            'type'        => 'text',
            'name'        => "data[vendors_id]",
            'required'    => 'required',
            'value'       => isset($data['vendors_id']) ? $data['vendors_id'] : ''
        ),
        $vendors
    )
);
Form::formGroup(
    'Модель',
    '',
    Form::select(
        array(
            'type'        => 'text',
            'name'        => "data[models_id]",
            'required'    => 'required',
            'disabled'    => 'disabled',
            'value'       => isset($data['models_id']) ? $data['models_id'] : ''
        ),
        array()
    )
);
Form::formGroup(
    'Рік випуску',
    '',
    Form::select(
        array(
            'type'        => 'text',
            'name'        => "data[years_id]",
            'required'    => 'required',
            'disabled'    => 'disabled',
            'value'       => isset($data['years_id']) ? $data['years_id'] : ''
        ),
        array()
    )
);
Form::formGroup(
    'Модифікація',
    '',
    Form::select(
        array(
            'type'        => 'text',
            'name'        => "data[modification_id]",
            'required'    => 'required',
            'disabled'    => 'disabled',
            'value'       => isset($data['modification_id']) ? $data['modification_id'] : ''
        ),
        array()
    )
);

Form::hidden('data[products_id]', $products_id);

Form::customSuccessAction("
            if(d>0){
                var oTable = $('#carProducts').dataTable();
                oTable.fnDraw(oTable.fnSettings());
                $('.modal').modal('hide');
            } else{
                alert(d);
            }
        ");
Form::close();