<style>
    html, body, #map-canvas {
        height: 100%;
        margin: 0px;
        padding: 0px
    }
    .controls {
        margin-top: 16px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
    }

    #pac-input {
        background-color: #fff;
        padding: 0 11px 0 13px;
        width: 400px;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        text-overflow: ellipsis;
    }

    #pac-input:focus {
        border-color: #4d90fe;
        margin-left: -1px;
        padding-left: 14px;  /* Regular padding-left + 1. */
        width: 401px;
    }

    .pac-container {
        font-family: Roboto;
    }

    #type-selector {
        color: #fff;
        background-color: #4d90fe;
        padding: 5px 11px 0px 11px;
    }

    #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
    }
    #target {
        width: 345px;
    }


</style>

<form accept-charset="utf-8" method="post" id="addPlanForm" action="plugins/OrderAccounts/add">
        <input id="user_id" type="hidden" name="data[users_id]" value="<?=$user_id?>">
        <div class="form-group ">
            <label>Телефон</label>
            <input type="text" name="data[phone]" class="form-control phone">
        </div>
        <div class="form-group ">
            <label>Ціна (UAH)</label>
            <input required="required" type="text" name="data[price]" class="form-control">
        </div>
        <div class="form-group ">
            <label>Тип тарифного плану</label>
            <select required name="data[type]" class="form-control" id="">
                <?php foreach($type as $key=>$item):?>
                    <option value="<?=$key?>"><?=$item?></option>
                <?php endforeach;?>
            </select>
        </div>
        <div class="form-group ">
            <label>Оплачено</label>
            <select required name="data[pay]" class="form-control" id="">
                <option value="0">Ні</option>
                <option value="1">Так</option>
            </select>
        </div>
        <div class="form-group ">
            <label>Тип оплати</label>
            <select required name="data[payment_id]" class="form-control" id="">
                <?php foreach($payments as $payment) :?>
                    <option value="<?=$payment["id"]?>"><?=$payment["name"]?></option>
                <?php endforeach;?>
            </select>
        </div>
        <div class="form-group ">
            <label>Дата оплати</label>
            <input required type="text" class="form-control date-picker" name="data[pay_date]">
        </div>
        <div class="form-group ">
            <label>Дата завершення</label>
            <input type="text" class="form-control date-picker" name="data[end_date]">
        </div>
    <input type="hidden" name="action" value="add"/>
</form>
<div id="error_str" style="color: red"></div>

<script>
    $(".date-picker").datepicker({
        dateFormat: "yy-mm-dd"
    })

    $(".phone").mask("+38 (999)-999-99-99");

</script>
