<div id="worker-cities" class="panel panel-default panel-block ">
    <div class="list-group">
        <div class="list-group-item">
            <h4 class="section-title">Міста</h4>
            <div class="form-group ">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <tbody>
                        <?php foreach($items as $item) :  ?>
                            <?php if(empty($item['selected'])) continue; ?>
                            <tr id="<?=$item['id']?>">
                                <td style="padding: 5px;"><?=$item['name']?></td>
                                <td style="padding: 5px;  text-align: center;vertical-align: middle">
                                    <input name="" class="chage-main-city" type="checkbox" <?php if(!empty($item['main_city'])) :?>checked<?php endif;?>>
                                </td>
                                <td style="padding: 5px;  text-align: center;vertical-align: middle">
                                    <i class="remove-worker-city icon-remove"
                                       style="cursor: pointer; color: red"></i>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                    <input type="text" id="worker-cities-select" class="form-control" />
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $("#worker-cities-select").select2({
            placeholder: "Виберіть місто",
            minimumInputLength: 0,
            ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
                url: "plugins/WorkerCity/getJSON",
                dataType: 'json',
                quietMillis: 250,
                type: 'POST',
                data: function (term, page) {
                    return {
                        q: term, // search term
                        'uid': <?=$uid?>
                    };
                },
                results: function (data, page) { // parse the results into the format expected by Select2.
                    // since we are using custom formatting functions we do not need to alter the remote JSON data
                    return { results: data.items };
                },
                cache: true
            },
//            formatResult: repoFormatResult, // omitted for brevity, see the source of this page
//            formatSelection: repoFormatSelection, // omitted for brevity, see the source of this page
            dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
            escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
        }).on("select2-selecting", function(e) {
            var item = e.object;
            $.get('plugins/WorkerCity/add/<?=$uid?>/'+item.id, function(d)
            {
                if(d>0) {
                    $("#worker-cities").load("plugins/WorkerCity/edit/<?=$uid?>");
                }
            });
        });
        $(document).on(
            'click',
            '.remove-worker-city',
            function(){
                var id=$(this).parents('tr').attr('id'), self=$(this);
                $.get('plugins/WorkerCity/deleteItem/<?=$uid?>'+'/'+id,function(d){
                    if(d>0){self.parents('tr').remove();}
                });
            }
        );

        $(document).on(
            'click',
            '.chage-main-city',
            function(){
                var id=$(this).parents('tr').attr('id'), self=$(this);
                $.get('plugins/WorkerCity/setDefaultCity/<?=$uid?>'+'/'+id,function(d){
                    if(d>0){
                        $("#worker-cities").load("plugins/WorkerCity/edit/<?=$uid?>");
                    }
                });
            }
        );


    });
</script>