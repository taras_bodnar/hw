<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 20.12.14 23:52
 */
defined("SYSPATH") or die();
?>
<div class="panel panel-default panel-block ">
    <div class="list-group">
        <div class="list-group-item">
            <h4 class="section-title">Пов'язані товари</h4>
            <div class="form-group ">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <tbody id="relatedProducts">
                            <?php foreach($items as $item) :  ?>
                                <tr id="<?=$item['id']?>">
                                    <td style="padding: 5px; text-align: center;vertical-align: middle"><i style="cursor: move" class="icon-reorder"></i></td>
                                    <td style="padding: 5px; text-align: center;vertical-align: middle"><img style="max-width:40px;max-height:40px;"
                                                                   src="<?=$item['img']?>"></td>
                                    <td style="padding: 5px;"><a href="products/edit/<?=$item['pid']?>" target="_blank"><?=$item['name']?></a></td>
                                    <td style="padding: 5px;  text-align: center;vertical-align: middle"><i class="remove-related icon-remove"
                                                                 style="cursor: pointer; color: red"></i>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                    <input type="text" id="related" class="form-control" />
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $("#related").select2({
            placeholder: "Виберіть товар",
            minimumInputLength: 0,
            ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
                url: "plugins/ProductsRelated/getJSON",
                dataType: 'json',
                quietMillis: 250,
                type: 'POST',
                data: function (term, page) {
                    return {
                        q: term, // search term
                        'pid': <?=$products_id?>
                    };
                },
                results: function (data, page) { // parse the results into the format expected by Select2.
                    // since we are using custom formatting functions we do not need to alter the remote JSON data
                    return { results: data.items };
                },
                cache: true
            },
//            formatResult: repoFormatResult, // omitted for brevity, see the source of this page
//            formatSelection: repoFormatSelection, // omitted for brevity, see the source of this page
            dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
            escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
        }).on("select2-selecting", function(e) {
            var item = e.object;
            $.get('plugins/ProductsRelated/add/<?=$products_id?>/'+item.id, function(d)
            {
               if(d>0) {
                   var tpl = $('<tr id="'+d+'">' +
                       '<td><i style="cursor: move" class="icon-reorder"></i></td>' +
                       '<td style=" text-align: center;vertical-align: middle"><img src="'+item.img+'" style="max-width:40px;max-height:40px;" /></td>' +
                       '<td><a target="_blank" href="products/edit/'+item.id+'">'+item.text+'</a></td>' +
                       '<td style=" text-align: center;vertical-align: middle"><i style="cursor: pointer; color: red" class="remove-related icon-remove"></i></td>' +
                       '</tr>');
                   $(tpl).find('td').css({
                       padding: 5
                   });
                   $('#relatedProducts').append(tpl);
               }
            });
        });
        $("#relatedProducts").sortable({
            handle: ".icon-reorder",
            update: function (event, ui)
            {
                var o = $(this).sortable('toArray').toString();
                $.ajax({
                    type: "POST",
                    url:'plugins/ProductsRelated/sort',
                    data: {sort: o},
                    success: function(d){
                        if(d > 0){
                            $.pnotify({
                                title: engine.lang.core.info,
                                type: 'success',
                                history: false,
                                text: engine.lang.options.sorting_success
                            });
                        }
                    },
                    dataType: 'html'
                });
            }
        });
        $(document).on(
            'click',
            '.remove-related',
            function(){
                var id=$(this).parents('tr').attr('id'), self=$(this);
                $.get('plugins/ProductsRelated/deleteItem/'+id,function(d){
                    if(d>0){self.parents('tr').remove();}
                });
            }
        );


    });
</script>