<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 20.12.14 23:52
 */
defined("SYSPATH") or die();
?>
<div class="panel panel-default panel-block ">
    <div class="list-group">
        <div class="list-group-item" id="меню">
            <h4 class="section-title">Група виконавців</h4>
            <div  class="form-group ">
                <label for="workers_id" >Виберіть групу </label>
                <select class="form-control"  name="workers_id" id="workers_id">
                    <option value="0">Для всіх</option>
                    <?php if(isset($items)) foreach ($items as $item) : ?>
                        <option <?php if(isset($group_id) && $group_id == $item['id']) echo 'selected'; ?> value="<?=$item['id']?>"><?=$item['name']?></option>
                    <?php endforeach; ?>
                </select>

                <script>$('#workers_id').select2();</script>
            </div>
        </div>
    </div>
</div>