<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 20.12.14 23:52
 */
defined("SYSPATH") or die();
?>
<div class="panel panel-default panel-block ">
    <div class="list-group">
        <div class="list-group-item" id="меню">
            <h4 class="section-title">Сортувння</h4>
            <div class="form-group ">
                <select id="city" style="width:100%;" name="city" placeholder="Виберіть місто">
                    <option value=""></option>
                    <?php foreach ( $cities as $item ):?>
                        <option <?=$item['selected']?> value="<?=$item['id']?>"><?=$item['name']?></option>
                    <?php endforeach;?>
                </select>
            </div>
            <div class="form-group ">
               <input type="text" name="sortU" value="<?php if(isset($sort)) echo $sort; ?>" class="form-control" >
            </div>
        </div>
    </div>
</div>

<script>
 $(document).ready(function(){
     $("#city").select2();
 })
</script>