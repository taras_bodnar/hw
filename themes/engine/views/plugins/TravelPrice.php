<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 20.12.14 23:52
 */
defined("SYSPATH") or die();
?>
<div class="panel panel-default panel-block ">
    <div class="list-group">
        <div class="list-group-item" id="меню">
            <h4 class="section-title">Ціни</h4>
            <div class="form-group ">
                Ціна : <input class="form-control" name="travel_price" type="text" value="<?php if(isset($price['travel_price'])) echo $price['travel_price']; ?>">
               <br> Стара ціна : <input class="form-control" name="travel_old_price" type="text" value="<?php if(isset($price['travel_old_price'])) echo $price['travel_old_price']; ?>">
            </div>
        </div>
    </div>
</div>
