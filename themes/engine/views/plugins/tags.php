<?php
/**
 * Company Otakoyi.com
 * Author: wg
 * Date: 20.12.14 23:52
 */
defined("SYSPATH") or die();
?>
<div class="panel panel-default panel-block ">
    <div class="list-group">
        <div class="list-group-item">
            <h4 class="section-title">Мітки </h4>

            <?php foreach($languages as $l) : ?>
            <div class="form-group ">
                <label><?=$l['name']?></label>
                <input id="tags" data-category="<?=$id?>" data-lang="<?=$l['id']?>" type="text" name="tags[<?=$l['id']?>]"
                       placeholder="введіть мітки через кому"
                       value="<?php if(isset($tags[$l['id']])) echo implode(',', $tags[$l['id']]); ?>" data-role="tagsinput" />
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>