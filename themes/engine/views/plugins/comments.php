<?php
/**
 * Company Otakoyi.com
 * Author: wg
 * Date: 20.12.14 23:52
 */
defined("SYSPATH") or die();
if(empty($items)) return ''; // примусово щоб не показувати  в інших сторінках
    $t = count($items);
?>
<div class="panel panel-default panel-block " id="commentsInline">
    <div class="list-group">
        <div class="list-group-item">
            <h4 class="section-title">Коментарі <?php if($t>0) echo "($t)"; ?></h4>
            <?php if(empty($items)) : ?>
                <p>Коментарі для цієї сторінки відсутні</p>
            <?php else :  ?>
                <?php foreach($items as $item) : ?>
                   <div class="comment-<?=$item['id']?>">
                       <h4><?=$item['pib']?></h4>
                       <p class="small text-info">Надіслано <?=$item['dt']?> в <?=$item['hi']?></p>
                       <p><?=$item['message']?></p>
                       <div class="clearfix"></div>
                       <?php if($item['published'] == 0) : ?>
                           <a title="Опублікувати відгук" onclick="engine.comments.pub(<?=$item['id']?>,<?=$item['published']?>);" href="javascript:void(0);" class="btn text-success pub-comment-<?=$item['id']?>"><i class="icon-eye-open"></i> опублікувати</a>
                       <?php endif; ?>
                       <a href="javascript:void(0);"  onclick="engine.comments.delete(<?=$item['id']?>);" title="Видалити відгук" class="btn btn-link btn-danger"><i class="icon-remove"></i> видаити</a>
                       <hr/>
                   </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
</div>