<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 01.06.14 10:46
 */
 use \controllers\engine\Form;

 defined('SYSPATH') or die();
Form::open("Letters/filter/",array("id"=>"filter-form"));
Form::openRow();
Form::openCol('col-lg-12');
Form::openPanel('Фільтр');
Form::formGroup(
    $lang->letters['client_group'],
    $lang->letters['client_group_tip'],
    Form::select(
        array(
            'name' => 'client_group[]',
            'multiple' =>'multiple',
        ),
        $client_group
    )
);



Form::formGroup(
    'Місто',
    'Місто',
    Form::select(
        array(
            'name' => 'cities[]',
            'multiple' =>'multiple',

        ),
        $cities
    )
);

Form::formGroup(
    'Остання дата входу',
    'Остання дата входу',
    Form::input(array(
        'type'        => 'date',
        'name'        => "filter[last_log_date]",
        'placeholder' => 'Остання дата входу',
        'value'       => isset($data['last_log_date']) ? $data['last_log_date'] : ''
    ))
);

Form::formGroup(
    'Давність оновлення портфоліо',
    'Давність оновлення портфоліо',
    Form::input(array(
        'type'        => 'date',
        'name'        => "filter[last_update]",
        'placeholder' => 'Давність оновлення портфоліо',
        'value'       => isset($data['last_update']) ? $data['last_update'] : ''
    ))
);

Form::formGroup(
    'Заповненість портфоліо',
    'Заповненість портфоліо',
    Form::input(array(
        'type'        => 'text',
        'name'        => "filter[completeness]",
        'placeholder' => 'Заповненість портфоліо',
        'value'       => isset($data['completeness']) ? $data['last_update'] : ''
    ))
);

Form::formGroup(
    '',
    '',
    Form::button('Фільтрувати',"",array("id"=>"filter-btn","class" => "btn-success"))
);
Form::close();


Form::closePanel();
Form::closeCol();
Form::closeRow();

    Form::open("Letters/process/$id");

//        Form::html("
//            <link rel=\"stylesheet\" href=\"{$template_url}scripts/vendor/codemirror/lib/codemirror.css\">
//            <script src=\"{$template_url}scripts/vendor/codemirror/lib/codemirror.js\"></script>
//            <script src=\"{$template_url}scripts/vendor/codemirror/mode/css.js\"></script>
//            <script src=\"{$template_url}scripts/vendor/codemirror/mode/htmlmixed.js\"></script>
//        ");

        Form::openRow();
            Form::openCol('col-lg-6');

                Form::openPanel($lang->letters['name']);

                Form::formGroup(
                    $lang->letters['name'],
                    $lang->letters['name_tip'],
                    Form::input(array(
                        'type'        => 'text',
                        'name'        => "data[name]",
                        'required'    => 'required',
                        'placeholder' => $lang->letters['name_placeholder'],
                        'value'       => isset($data['name']) ? $data['name'] : ''
                    ))
                );

                Form::formGroup(
                    $lang->EmailTemplates['email'],
                    $lang->EmailTemplates['email_tip'],
                    Form::textarea(
                        array(
                            'name' => 'emails',
                            'rows' => 100,
                            'multiple' =>'multiple',
                            'required' =>'required',
                            'value'     => isset($emails) ? $emails: '',
                        )
                    )
                );

                Form::closePanel();

            Form::closeCol();

            Form::openCol('col-lg-6');
                Form::openPanel($lang->letters['params']);
                    Form::formGroup(
                      $lang->letters['fromto'],
                      $lang->letters['fromto_tip'],
                        Form::input(array(
                            'type'=>'text',
                            'name'=>"data[fromto]",
                            'required'    => 'required',
                            'placeholder' => $lang->letters['fromto_placeholder'],
                            'value'       => isset($data['fromto']) ? $data['fromto'] : ''
                        ))
                    );
                Form::formGroup(
                      $lang->letters['themes'],
                      $lang->letters['themes_tip'],
                        Form::input(array(
                            'type'=>'text',
                            'name'=>"data[themes]",
                            'required'    => 'required',
                            'placeholder' => $lang->letters['themes_placeholder'],
                            'value'       => isset($data['themes']) ? $data['themes'] : ''
                        ))
                    );
                Form::formGroup(
                      $lang->letters['content'],
                      $lang->letters['content_tip'],
                        Form::editor(array(
                            'type'=>'text',
                            'name'=>"data[content]",
                            'required'    => 'required',
                            'placeholder' => $lang->letters['content_placeholder'],
                            'value'       => isset($data['content']) ? $data['content'] : ''
                        ))
                    );

Form::formGroup(
    $lang->letters['content'].' (РУС)',
    $lang->letters['content_tip'].' (РУС)',
    Form::editor(array(
        'type'=>'text',
        'name'=>"data[content_ru]",
        'required'    => 'required',
        'placeholder' => $lang->letters['content_placeholder'].' (РУС)',
        'value'       => isset($data['content_ru']) ? $data['content_ru'] : ''
    ))
);
                Form::closePanel();
            Form::closeCol();
        Form::closeRow();
        Form::hidden('action',$action);

    Form::close();

?>

<script>

//$("#client_group").on("change",function(){
//   var cg = $(this).val();
//    var type = $(this).select2().find(":selected:last").data("type");
//    console.log($(this).select2().find(":selected:last").data("type"));
//    type = typeof type!='undefined'?type:false;
////    $("#emails").val("");
//    $.ajax({
//        type:"post",
//        dataType:"html",
//        data:({'cg':cg,'type':type,emails:$("#emails").val()}),
//        url: "Letters/getEmails/",
//        success: function(d) {
//           // console.log(d);
//            var obj = $.parseJSON(d);
//            $("#emails").val(obj.emails);
//           // var obj = $.parseJSON(d);
//            //console.log(obj.emails)
//           /* $(obj.emails).each(function(i,e){
//                $("#emails").val(e.name);
//            });*/
//        }
//    })
//});

$("#filter-btn").on('click',function (e) {
    e.preventDefault();
    $("#filter-form").submit();
})

$("#filter-form").ajaxForm({
    target: '#responce',
    type: 'post',
    dataType: 'json',
    success: function (d) {
        console.log(d.emails);
        $("#emails").val();
//        var obj = $.parseJSON(d);
        $("#emails").val(d.emails);
    }
});


</script>