<?php
/**
 *
 */
use \controllers\engine\Form;
//print_r($filterContent);
defined("SYSPATH") or die();
Form::open('', array('id' => 'filter'));
    Form::openPanel($lang->customers['filter']);

Form::openRow();

Form::openCol('col-lg-4');

Form::formGroup(
    'Фільтр по містах',
    '',
    Form::select(
        array(
            'name' => 'filter[city_id]'
        ),
        $filterContent
    )
);
Form::closeCol();
Form::openCol('col-lg-4');
Form::formGroup(
    '<div class="clearfix" style="margin-top: 40px;"></div>',
    '',
    Form::button($lang->products['f_reset'],
        Form::icon('icon-refresh'),
        array(
            'class' =>'btn-primary',
            'id'=>'resetFilter',
            'onclick'=>'window.location.reload()'
        )
    )
);
Form::hidden("city_id");
Form::closeCol();
Form::closeRow();
Form::closePanel();
Form::close();
?>
<script>
    $(document).ready(function(){
        $('#filter_city_id').on('change',function() {
//            console.log("dsd");
            var type = $('#filter_city_id').find('option:selected').val();
            $("#city_id").val(type);
            // alert(type);
            var oTable = $('#users').dataTable();
            var oS = oTable.fnSettings();
//            oS.sAjaxSource = "Customers/items/6/"+type;
//            oTable.fnReloadAjax(oS);
            var data = {
                'filter[city_id]' : type,
                'order[0][column]' : 'us.sort' ,
                'order[0][dir]'  : 'asc',
                'sCol' : 'us.sort'

            };
            $.extend( oS.ajax.data, data );
//       console.log(oS.ajax.data);
//            oTable.fnDraw(oS);

            oTable.fnReloadAjax(oS,null,function (){
                $(".dataTables_wrapper ").find('.icon-reorder').each(function (i, e) {
                    $(this).parents("tr").attr("id", $(e).attr("id"));
//                    console.log($(e).attr("id"));
                });
            });
//            oTable.fnInitComplete({
//                $(".dataTables_wrapper ").find('.icon-reorder').each(function (i, e) {
//                    $(this).parents("tr").attr("id", $(e).attr("id"));
//                    console.log($(e).attr("id"));
//                });
//        });

        });
    });
</script>

