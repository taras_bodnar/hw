<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 01.06.14 10:46
 */
 use \controllers\engine\Form;

 defined('SYSPATH') or die();

    Form::open("{$form_action}/$id");
        Form::openRow();
            Form::openCol('col-lg-8');

                Form::openPanel($lang->users_group['title']);
                $hide='';
                if(count($languages) > 1) {
                    $hide='hide';
                    Form::languagesSwitch(
                        $lang->core['sel_languages'],
                        $lang->core['sel_languages_tip'],
                        $languages,
                        $lang
                    );
                }
                foreach ($languages as $row) {
                    Form::html("<div class=\"language-switch $hide language-{$row['id']}\">");
                    Form::formGroup(
                        $lang->users_group['name'],
                        $lang->users_group['name_tip'],
                        Form::input(array(
                            'type'        => 'text',
                            'name'        => "info[{$row['id']}][name]",
                            'required'    => 'required',
                            'placeholder' => $lang->users_group['name_placeholder'],
                            'data-parsley-required' => 'true',
                            'value'       => isset($info[$row['id']]['name']) ? $info[$row['id']]['name'] : ''
                        ))
                    );
                    Form::formGroup(
                        $lang->users_group['desc'],
                        $lang->users_group['desc_tip'],
                        Form::textarea(array(
                            'name' => "info[{$row['id']}][description]",
                            'placeholder' => $lang->users_group['desc_placeholder'],
                            'value'       => isset($info[$row['id']]['description']) ? $info[$row['id']]['description'] : ''
                        ))
                    );
                    Form::html('</div>');// .info
                }
                Form::closePanel();

            Form::closeCol();
            Form::openCol('col-lg-4');

                Form::openPanel($lang->core['params']);
                    Form::formGroup(
                        $lang->users_group['parent'],
                        $lang->users_group['parent_tip'],
                        Form::select(
                            array(
                                'name' => 'data[parent_id]'
                            ),
                            $parents)
                    );

                    Form::formGroup(
                        $lang->users_group['rang'],
                        $lang->users_group['rang_tip'],
                        Form::input(array(
                            'type'        => 'text',
                            'name'        => 'data[rang]',
                            'required'    => 'required',
                            'placeholder' => $lang->users_group['rang_placeholder'],
                            'data-parsley-required' => 'true',
                            'data-parsley-type'     => 'number',
                            'data-parsley-min'      => $rang[0],
                            'data-parsley-max'      => $rang[1],
                            'value' => isset($data['rang']) ? $data['rang'] : ''
                        ))
                    );

                Form::closePanel();

            Form::closeCol();
        Form::closeRow();
        Form::hidden('action', $action);
    Form::close();