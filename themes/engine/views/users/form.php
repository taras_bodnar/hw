<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 01.06.14 10:46
 */
use \controllers\engine\Form;

defined('SYSPATH') or die();

Form::open("{$form_action}/$id");
Form::openRow();
Form::openCol('col-lg-8');

Form::openPanel($lang->users['title']."<br>".Form::link('Переглянути профіль','',
        array(
            'class'  =>'btn-info',
            'href'  => APPURL.$data['url'],
            'target' => '_blank'
        )));

Form::formGroup(
    $lang->users['name'],
    $lang->users['name_tip'],
    Form::input(array (
        'type' => 'text',
        'name' => "data[name]",
        'required' => 'required',
        'placeholder' => $lang->users['name_placeholder'],
        'data-parsley-required' => 'true',
        'value' => isset($data['name']) ? $data['name'] : ''
    ))
);

Form::formGroup(
    'Прізвище',
    'Прізвище',
    Form::input(array (
        'type' => 'text',
        'name' => "data[surname]",
        'required' => 'required',
        'placeholder' => 'Прізвище',
        'data-parsley-required' => 'true',
        'value' => isset($data['surname']) ? $data['surname'] : ''
    ))
);
Form::formGroup(
    $lang->users['phone'],
    $lang->users['phone_tip'],
    Form::input(array (
        'type' => 'text',
        'name' => "data[phone]",
        'placeholder' => $lang->users['phone_placeholder'],
//                        'data-parsley-type'     => 'number',
        'value' => isset($data['phone']) ? $data['phone'] : ''
    ))
);
Form::formGroup(
    $lang->users['email'],
    $lang->users['email_tip'],
    Form::input(array (
        'type' => 'text',
        'name' => "data[email]",
        'required' => 'required',
        'placeholder' => $lang->users['email_placeholder'],
        'data-parsley-required' => 'true',
        'data-parsley-type' => 'email',
        'value' => isset($data['email']) ? $data['email'] : ''
    ))
);
Form::formGroup(
    $lang->users['password'],
    $lang->users['password_tip'],
    Form::input(array (
        'type' => 'password',
        'name' => "data[password]",
        'data-parsley-required' => $action == 'create' ? 'true' : '',
        'placeholder' => $lang->users['password_placeholder'],
        'value' => ''
    ))
);

Form::formGroup(
    'Нотатки адміністратора',
    'Нотатки адміністратора',
    Form::textarea(array (
        'name' => "data[note]",
        'placeholder' => 'Нотатки',
        'cols'=> 10,
        'rows'=> 10,
        'value' => isset($data['note']) ? $data['note'] : ''
    ))
);

Form::closePanel();

if ( isset($plugins['left']) ) {
    Form::html(implode('', $plugins['left']));
}

Form::closeCol();
Form::openCol('col-lg-4');
Form::openPanel($lang->core['params']);
Form::formGroup(
    $lang->users['group'],
    $lang->users['group_tip'],
    Form::select(
        array (
            'name' => 'data[users_group_id]'
        ),
        $group)
);
Form::formGroup(
    $lang->users['languages'],
    $lang->users['languages_tip'],
    Form::select(
        array (
            'name' => 'data[languages_id]'
        ),
        $languages,
        $data['languages_id'])
);

Form::formGroup(
    'Позиція',
    '',
    Form::input(array (
        'type' => 'text',
        'name' => "data[sort]",
        'placeholder' => 'введіть порядковий номер в порядку зростання [0-9]+',
        'data-parsley-type' => 'number',
        'value' => isset($data['sort']) ? $data['sort'] : ''
    ))
);

Form::formGroup(
    'Бали',
    '',
    Form::input(array (
        'type' => 'text',
        'name' => "",
        'readonly'=>'readonly',
        'data-parsley-type' => 'number',
        'value' => isset($point) ? $point : ''
    ))
);

Form::formGroup(
    'Бали',
    '',
    Form::input(array (
        'type' => 'text',
        'name' => "data[point]",
        'data-parsley-type' => 'number',
        'value' => isset($data['point']) ? $data['point'] : ''
    ))
);



//                Form::closePanel();
//                print_r($plugins['right']);
if ( isset($plugins['right']) ) {
    Form::html(implode('', $plugins['right']));
}
Form::closeCol();
Form::closeRow();
Form::hidden('action', $action);
Form::close();