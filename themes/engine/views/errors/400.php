<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 17.05.14 22:57
 */
 //app/themes/engine/default/assets
if ( !defined('SYSPATH') ) die();
?>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7 lt-ie10"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8 lt-ie10"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9 lt-ie10"> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <base href="<?=$base_url?>" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">


    <title>oyi UI - 400 Bad Request</title>
    <meta name="description" content="">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

    <link rel="stylesheet" href="styles/92bc1fe4.bootstrap.css">


    <!-- oyi CSS: -->
    <link rel="stylesheet" href="styles/aaf5c053.oyi.css">
    <link rel="stylesheet" href="styles/vendor/animate.css">

    <!-- adds CSS media query support to IE8   -->
    <!--[if lt IE 9]>
    <script src="scripts/html5shiv.js"></script>
    <script src="scripts/vendor/respond.min.js"></script>
    <![endif]-->

    <!-- Fonts CSS: -->
    <link rel="stylesheet" href="styles/6227bbe5.font-awesome.css" type="text/css" />
    <link rel="stylesheet" href="styles/40ff7bd7.font-titillium.css" type="text/css" />

    <!-- Common Scripts: -->
    <script src="scripts/jquery.min.js"></script>
    <script src="scripts/vendor/modernizr.js"></script>
    <script src="scripts/vendor/jquery.cookie.js"></script>
</head>

<body class="error-page">
<!--[if lt IE 8]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<section class="wrapper scrollable">
    <div class="panel panel-default panel-block panel-error-block">
        <div class="panel-heading">
            <div>
                <i class="icon-frown"></i>
                <h1>
                    <small>
                        Sorry, there's been an error.
                    </small>
                    Bad request
                    <i class="icon-info-sign uses-tooltip" data-toggle="tooltip" data-placement="top" data-original-title="The request cannot be fulfilled due to bad syntax."></i>
                </h1>
                <div class="error-code">
                    400
                </div>
            </div>
        </div>
        <div class="panel-body">
            <a href="index.html"><i class="icon-home"></i></a>
            <div class="input-group">
                <input type="text" class="form-control">
                <div class="input-group-btn">
                    <button class="btn btn-default btn-search" type="button"><i class="icon-search"></i></button>
                </div>
            </div>
        </div>
    </div>
</section>

<script src="scripts/e1d08589.bootstrap.min.js"></script>

<!-- oyi base scripts: -->
<script src="scripts/9f7a46ed.oyi.js"></script>

</body>
</html>