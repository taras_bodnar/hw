<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 01.06.14 10:46
 */
 use \controllers\engine\Form;

 defined('SYSPATH') or die();

    Form::open("plugins/{$action}" . (isset($id) ? '/'.$id : ''), array('id' => 'pluginsForm'));

    Form::formGroup(
        $lang->plugins['structure'],
        $lang->plugins['structure_tip'],
        Form::select(
            array(
                'name' => 'structure[]',
                'multiple' => 'multiple',
                'placeholder' => $lang->plugins['structure_placeholder'],
                'required' => 'required'
            ),
            $structure
        )
    );
    Form::formGroup(
        $lang->plugins['position'],
        $lang->plugins['position_tip'],
        Form::select(
            array(
                'name' => 'data[position]',
                'required' => 'required'
            ),
            $position
        )
    );

    if(isset($plugin)) {
        Form::hidden('plugin', $plugin);
    }
    Form::hidden('action', $action);
    Form::customSuccessAction("
        var oTable = $('#plugins').dataTable();
        oTable.fnDraw(oTable.fnSettings());
        $('.modal').modal('hide');
    ");
    Form::close();