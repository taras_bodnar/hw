<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 28.08.14 21:24
 */
 defined("SYSPATH") or die();
?>
<!--<pre>--><?php //print_r($themes);?><!--</pre>-->
<div class="themes row">
    <?php foreach($themes as $theme) : ?>
        <div class="col-md-4 theme <?=$theme['current']?>">
            <section class="panel panel-default panel-block">
                <div class="panel-heading text-overflow-hidden">
                    <div class="screenshot">
                        <img src="<?= $theme['urlpath'] . $theme['screenshot']?>" alt="screenchot of <?=$theme['name']?>"/>
                        <div class="description">
                            <div class="author"><?=$lang->themes['author']?>: <?=$theme['author']?></div>
                            <div class="version"><?=$lang->themes['version']?>: <?=$theme['version']?></div>
                            <div class="info"><?=$theme['description']?></div>
                        </div>
                    </div>
                    <h3 class="name"><?=$theme['name']?></h3>
                    <?php if($theme['current'] == '') : ?>
                        <div class="actions">
                            <a onclick="engine.themes.activate('<?=$theme['theme']?>'); return false;"
                               class="btn btn-success"
                               href="javascript:void(0);"
                                ><?=$lang->themes['activate']?></a>
                        </div>
                    <?php endif; ?>
                </div>
            </section>
        </div>
    <?php endforeach; ?>
</div>