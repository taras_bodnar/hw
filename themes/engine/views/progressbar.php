<?php
/**
 * Created by PhpStorm.
 * User: wg
 * Date: 26.06.15
 * Time: 17:45
 */
defined("SYSPATH") or die();
?>

<div class='panel panel-default panel-block' id='alias-box'>
    <div class='list-group'>
        <div class='list-group-item button-demo'>
            <span class="start"></span>/<span class="total"></span>
            <div id='progress' class='progress progress-thin  progress-striped active'>
                <div style='width: 0%;' class='progress-bar progress-bar-success' data-original-title='' title=''></div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).on("ready", function () {
        $("#alias-box").css("display", "none");
    });
</script>