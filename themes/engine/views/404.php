<div class="panel panel-default panel-block panel-error-block">
    <div class="panel-heading">
        <div>
            <i class="icon-frown"></i>
            <h1>
                <small>
                    <?=$lang->e404['title']?>
                </small>
                <?=$lang->e404['desc']?>
            </h1>
            <div class="error-code">
                404
            </div>
        </div>
    </div>
</div>