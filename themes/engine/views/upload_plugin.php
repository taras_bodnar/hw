<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 15.11.14 21:56
 */
use \controllers\engine\Form;

defined("SYSPATH") or die();

Form::open("plugins/process/1",array('enctype' => 'multipart/form-data'), $lang);
Form::openRow();
Form::openCol('col-lg-12');

Form::openPanel($lang->plugins['upload_title']);

Form::formGroup(
    $lang->plugins['upload_extends'],
    $lang->plugins['upload_extends_tip'],
    Form::select(
        array(
        'name'        => "data[type_id]",
        'required'    => 'required'
        ),
        $type_id
    )
);
Form::formGroup(
    $lang->plugins['upload_position'],
    $lang->plugins['upload_position_tip'],
    Form::select(
        array(
        'name'        => "data[position]",
        'required'    => 'required'
        ),
        $position
    )
);
Form::formGroup(
    $lang->plugins['upload_file'],
    $lang->plugins['upload_file_tip'],
    Form::fileInput(
        array(
        'name'        => "file",
        'required'    => 'required'
        )
    )
);
Form::closePanel();

Form::closeCol();
Form::closeRow();
Form::html('<script src="'. $template_url .'scripts/vendor/fileinput.js"></script>');
Form::close();