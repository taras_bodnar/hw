<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 14.07.14 13:14
 */
defined('SYSPATH') or die();
?>
<form
    data-parsley-namespace='data-parsley-enabled'
    data-parsley-validate
    action='auth/login'
    method="post"
    accept-charset="utf-8"
    class="login-form"
    id="form">
    <section class="wrapper scrollable animated fadeInDown">
        <section class="panel panel-default">
            <div class="panel-heading">
                <div>
                    <img src="<?=$template_url?>images/logo.png" alt="engine-logo">
                    <h1>
                        <span class="title">
                            <?=$lang->auth['title']?>
                        </span>
                        <span class="subtitle">
                            <?=$lang->auth['subtitle']?>
                        </span>
                    </h1>
                </div>
            </div>

            <ul class="list-group">
                <li class="list-group-item">
                    <span class="login-text">
                            <?=$lang->auth['login_text']?>
                    </span>
                    <div class="form-group">
                        <label for="email"><?=$lang->auth['email']?></label>
                        <input required="" type="email" class="form-control input-lg" id="email" name="email" placeholder="<?=$lang->auth['email']?>">
                    </div>
                    <div class="form-group">
                        <label for="password"><?=$lang->auth['pass']?></label>
                        <input required="" type="password" class="form-control input-lg" id="password" name="password" placeholder="<?=$lang->auth['pass']?>">
                    </div>
                    <div class="form-group secpic hidden">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <img src='<?=$template_url?>secpic/' alt='<?=$lang->auth['secpic']?>'/>
                            </div>
                            <input class="form-control input-lg" name="secpic" type="text" placeholder="<?=$lang->auth['secpic']?>">
                        </div>
                    </div>
                </li>
            </ul>

            <div class="panel-footer">
                <button type="submit" class="btn btn-lg btn-success form-submit"><?=$lang->auth['btn_login']?></button>
                <br>
                <a class="forgot" href="javascript:;" onclick="toggleForms();"><?=$lang->auth['fp']?></a>
            </div>

        </section>
    </section>
</form>
<form
    data-parsley-namespace='data-parsley-enabled'
    data-parsley-validate
    action='auth/fp'
    method="post"
    accept-charset="utf-8"
    id="form_fp"
    class="fp-form"
    >
    <section class="wrapper hide scrollable animated fadeInDown">
        <section class="panel panel-default">
            <div class="panel-heading">
                <div>
                    <img src="<?=$template_url?>images/logo.png" alt="engine-logo">
                    <h1>
                        <span class="title">
                            <?=$lang->auth['title']?>
                        </span>
                        <span class="subtitle">
                            <?=$lang->auth['subtitle']?>
                        </span>
                    </h1>
                </div>
            </div>

            <ul class="list-group">
                <li class="list-group-item">
                    <span class="login-text">
                            <?=$lang->auth['login_text']?>
                    </span>
                    <div class="form-group">
                        <label for="email"><?=$lang->auth['email']?></label>
                        <input required="" type="email" class="form-control input-lg" id="email" name="email" placeholder="<?=$lang->auth['email']?>">
                    </div>
                </li>
            </ul>

            <div class="panel-footer">
                <button type="submit" class="btn btn-lg btn-success form-submit"><?=$lang->auth['gen']?></button>
                <br>
                <a class="forgot" href="javascript:;" onclick="toggleForms();"><?=$lang->auth['login']?></a>
            </div>

        </section>
    </section>

    <script>
        function toggleForms(){
            $('form:visible:eq(0) .wrapper').addClass('fadeOutUp', function(){
                var $this = $(this);
                setTimeout(function(){
                    $('form')
                        .not($this)
                        .find('.wrapper')
                        .removeClass('hide')
                        .addClass('fadeInDown');
                }, 1000);
                console.log(1);
            });
        }
        $(document).ready(function(){
            $('#form').submit(function(e) {
                var form = $(this);
                e.preventDefault();
                if ( form.parsley('validate') ) {
                    $.ajax({
                        type: "post",
                        url: form.attr('action'),
                        data: form.serialize(), // serializes the form's elements.
                        dataType : 'json',
                        success: function(d)
                        {
                            if(d.s > 0) {
                                window.location.reload(true);
                            } else {
                                if(d.f > 0) {
                                    $('.form-group.secpic').removeClass('hidden');
                                }
                                engine.alert(d.t, d.m, 'error', '.login-text', 'html' )
                            }
                        }
                    });
                }
            });

            $('#form_fp').submit(function(e) {
                var form = $(this);
                e.preventDefault();
                if ( form.parsley('validate') ) {
                    $.ajax({
                        type: "post",
                        url: form.attr('action'),
                        data: form.serialize(), // serializes the form's elements.
                        dataType : 'json',
                        success: function(d)
                        {
                            engine.alert(d.t, d.m, (d.s > 0 ? 'success' : 'error'), '.login-text', 'html' )
                        }
                    });
                }
            });

            $(document).on('ajaxSend', function(){
                $('.form-submit').attr('disabled',true).addClass('loading');
            }).on('ajaxComplete', function(){
                $('.form-submit').removeAttr('disabled').removeClass('loading');
            });
        });
    </script>
</form>