<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 01.06.14 10:46
 */
 use \controllers\engine\Form;

 defined('SYSPATH') or die();

    Form::open("CarYears/process/$id", array('id' => 'CarYearsForm'));
        Form::html('<span class="response"></span>');
        Form::formGroup(
            'Рік випуску',
            '',
            Form::input(array(
                'type'        => 'text',
                'name'        => "data[year]",
                'required'    => 'required',
                'value'       => isset($data['year']) ? $data['year'] : ''
            ))
        );

        if(isset($models_id)){
            Form::hidden('data[models_id]', $models_id);
        }

        Form::hidden('action', $action);

        Form::customSuccessAction("
            if(d.s){
                var oTable = $('#CarYears').dataTable();
                oTable.fnDraw(oTable.fnSettings());
                $('.modal').modal('hide');
            } else{
                $('.response').html(d.m);
            }
        ");
    Form::close();