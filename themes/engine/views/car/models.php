<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 01.06.14 10:46
 */
 use \controllers\engine\Form;

 defined('SYSPATH') or die();

    Form::open("CarModels/process/$id", array('id' => 'CarModelsForm'));
        Form::html('<span class="response"></span>');
        Form::formGroup(
            'Модель',
            '',
            Form::input(array(
                'type'        => 'text',
                'name'        => "data[model]",
                'required'    => 'required',
                'value'       => isset($data['model']) ? $data['model'] : ''
            ))
        );

        if(isset($vendor_id)){
            Form::hidden('data[vendors_id]', $vendor_id);
        }

        Form::hidden('action',$action);

        Form::customSuccessAction("
            if(d.s){
                var oTable = $('#CarModels').dataTable();
                oTable.fnDraw(oTable.fnSettings());
                $('.modal').modal('hide');
            } else{
                $('.response').html(d.m);
            }
        ");
    Form::close();