<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 01.06.14 10:46
 */
 use \controllers\engine\Form;

 defined('SYSPATH') or die();

    Form::open("CarModification/process/$id", array('id' => 'CarModificationForm'));
        Form::html('<span class="response"></span>');
        Form::formGroup(
            'Модифікація',
            '',
            Form::input(array(
                'type'        => 'text',
                'name'        => "data[modification]",
                'required'    => 'required',
                'value'       => isset($data['modification']) ? $data['modification'] : ''
            ))
        );

        if(isset($models_id)){
            Form::hidden('data[years_id]', $models_id);
        }

        Form::hidden('action', $action);

        Form::customSuccessAction("
            if(d.s){
                var oTable = $('#CarModification').dataTable();
                oTable.fnDraw(oTable.fnSettings());
                $('.modal').modal('hide');
            } else{
                $('.response').html(d.m);
            }
        ");
    Form::close();