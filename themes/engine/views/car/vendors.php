<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 01.06.14 10:46
 */
 use \controllers\engine\Form;

 defined('SYSPATH') or die();

    Form::open("CarVendors/process/$id", array('id' => 'CarVendorsForm'));
        Form::html('<span class="response"></span>');
        Form::formGroup(
            'Виробник',
            '',
            Form::input(array(
                'type'        => 'text',
                'name'        => "data[vendor]",
                'required'    => 'required',
                'value'       => isset($data['vendor']) ? $data['vendor'] : ''
            ))
        );

        Form::hidden('action',$action);

        Form::customSuccessAction("
            if(d.s){
                var oTable = $('#CarVendors').dataTable();
                oTable.fnDraw(oTable.fnSettings());
                $('.modal').modal('hide');
            } else{
                $('.response').html(d.m);
            }
        ");
    Form::close();