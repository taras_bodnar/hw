<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 01.06.14 10:32
 */
 
defined('SYSPATH') or die();
?>
<div id="top" class="panel panel-default panel-block panel-title-block">
    <div class="panel-heading">
        <div>
            <?php if(!empty($icon)) :  ?>
            <i class="<?=$icon?>"></i>
            <?php endif; ?>
            <h1>
                <span class="page-title"><?=$title?></span>
                <small>
                    <?=$description?>
                </small>
            </h1>
            <div class="panel-buttons">
                <?php if(isset($buttons) && is_array($buttons)) echo implode(' ', $buttons); ?>
            </div>
        </div>
    </div>
</div>