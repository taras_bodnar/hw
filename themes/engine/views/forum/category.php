<?php
/**
 * Company Otakoyi.com
 * Author: wg
 * Date: 04.12.14 10:27
 */
use \controllers\engine\Form;
defined("SYSPATH") or die();

Form::open('ForumCategories/process/' . $id, array('id' => 'PostsCategoriesForm'));
$hide='';
if(count($languages) > 1) {
    $hide='hide';
    Form::languagesSwitch(
        $lang->core['sel_languages'],
        $lang->core['sel_languages_tip'],
        $languages,
        $lang
    );
}
foreach ($languages as $row) {
    Form::html("<div class=\"language-switch $hide language-{$row['id']}\">");
    Form::formGroup(
        $lang->content['name'],
        $lang->content['name_tip'],
        Form::input(array(
            'type'        => 'text',
            'value'       => isset($info[$row['id']]['name']) ? $info[$row['id']]['name'] : '',
            'name'        => "info[{$row['id']}][name]",
            'id'          => "info_{$row['id']}_name",
            'onChange'    => $autotranslit_alias ?
                    'engine.pages.mkAlias($(\'#PostsCategoriesForm #info_'.$row['id'].'_alias\'),'.$parent_id.','. $row['id'] .',\''. $row['code'] .'\', this.value);' : '',
            'required'    => 'required',
            'placeholder' => $lang->content['name_placeholder'],
        ))
    );

    Form::formGroup(
        $lang->content['alias'],
        $lang->content['alias_tip'],
        Form::input(array(
            'type'        => 'text',
            'value'       => isset($info[$row['id']]['alias']) ? $info[$row['id']]['alias'] : '',
            'name'        => "info[{$row['id']}][alias]",
            'id'          => "info_{$row['id']}_alias",
            'required'    => 'required',
            'placeholder' => $lang->content['alias_placeholder'],
        ))
    );
    Form::formGroup(
        $lang->content['meta_title'],
        $lang->content['meta_title_tip'],
        Form::input(array(
            'type'        => 'text',
            'name'        => "info[{$row['id']}][title]",
            'required'    => 'required',
            'placeholder' => $lang->content['meta_title_placeholder'],
            'data-parsley-required' => 'true',
            'value'       => isset($info[$row['id']]['title']) ? $info[$row['id']]['title'] : '',
            'counter'     => array(
                'counterText' => $lang->core['counter_text'],
                'allowed' => 255,
                'warning' => 25
            )
        ))
    );

        Form::formGroup(
            $lang->content['keywords'],
            $lang->content['keywords_tip'],
            Form::input(array(
                'type'        => 'text',
                'name'        => "info[{$row['id']}][keywords]",
                'placeholder' => $lang->content['keywords_placeholder'],
                'value'       => isset($info[$row['id']]['keywords']) ? $info[$row['id']]['keywords'] : '',
                'counter'     => array(
                    'counterText' => $lang->core['counter_text'],
                    'allowed' => 255,
                    'warning' => 25
                )
            ))
        );

        Form::formGroup(
            $lang->content['desc'],
            $lang->content['desc_tip'],
            Form::textarea(array(
                'name' => "info[{$row['id']}][description]",
                'placeholder' => $lang->content['desc_placeholder'],
                'value'       => isset($info[$row['id']]['description']) ? $info[$row['id']]['description'] : '',
                'counter'     => array(
                    'counterText' => $lang->core['counter_text'],
                    'allowed' => 255,
                    'warning' => 25
                )
            ))
        );


    Form::html('</div>');// .info
}
Form::hidden('data[published]', 1);
Form::hidden('data[parent_id]', $parent_id);
Form::hidden('created', $created);
Form::hidden('redirect_url', '');
Form::customSuccessAction("
    $('#tree').jstree('refresh');
    $('.modal').modal('hide');
");
Form::close();
