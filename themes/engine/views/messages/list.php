<table class="table table-striped">
    <thead>
    <tr>
        <th>#</th>
        <th>Name</th>
        <th>Message</th>
        <th>Date</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($msgs as $k => $msg): ?>
    <tr>
        <th scope="row"><?= ++$k ?></th>
        <td><?= $msg['name'] . ' ' . $msg['surname'] ?></td>
        <td><?= $msg['message'] ?></td>
        <td><?= $msg['createdon'] ?></td>
    </tr>
    <?php endforeach; ?>
    </tbody>
</table>