<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 06.11.14 17:08
 */
use \controllers\engine\Form;

defined("SYSPATH") or die();
Form::open('', array('id' => 'shopFilter'));
    Form::openPanel($lang->products['filter']);

Form::openRow();
Form::openCol('col-lg-4');
Form::formGroup(
    $lang->products['manufacturers'],
    '',
    Form::select(
        array(
            'name' => 'filter[manufacturers_id]',
        ),
        $manufacturers
    )
);
Form::closeCol();
Form::openCol('col-lg-4');

Form::formGroup(
    $lang->products['filter_status'],
    '',
    Form::select(
        array(
            'name' => 'filter[status]',
        ),
        $status
    )
);
Form::closeCol();
Form::openCol('col-lg-4');
Form::formGroup(
    '<div class="clearfix" style="margin-top: 40px;"></div>',
    '',
    Form::button($lang->products['f_reset'], Form::icon('icon-refresh'), array('class' =>'btn-primary', 'id'=>'resetFilter'))
);
Form::closeCol();
Form::closeRow();


Form::closePanel();
Form::close();
