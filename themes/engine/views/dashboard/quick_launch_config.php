<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 02.10.14 21:21
 */
defined("SYSPATH") or die();
use \controllers\engine\Form;

Form::open("dashboard/quickLaunchConfigSave", array('id'=>'quickLaunchConfigForm'), $lang);

Form::formGroup(
    $lang->dashboard['ql_structure_items'],
    $lang->dashboard['ql_structure_items_tip'],
    Form::pickList(
        array(
            'name' => 'items[]'
        ),
        $items
    )
);

Form::customSuccessAction("
    return engine.dashboard.quickLaunchConfigProcess(d);
");
Form::close();