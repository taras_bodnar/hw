<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 02.10.14 21:21
 */
defined("SYSPATH") or die();
?>
<nav class="quick-launch-bar">
    <ul id="quick_launch_items">
        <?php foreach($items as $item) : ?>
        <li id="<?=$item['id']?>">
            <a href="<?=$item['url']?>">
                <i class="<?=$item['icon']?>"></i>
                <span><?=$item['name']?></span>
            </a>
        </li>
        <?php endforeach; ?>
    </ul>
    <a onclick="engine.dashboard.quickLaunchConfig(); return false;" class="add-quick-launch"><i class="icon-plus"></i></a>
</nav>
<script>
    $(function(){
       engine.dashboard.quickLaunchInitSortable();
    });
</script>