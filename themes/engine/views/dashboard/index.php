<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 20.05.14 23:57
 */
 
if ( !defined('SYSPATH') ) die();
?>
<section class="wrapper scrollable">
    <section class="title-bar">
        <div>
            <span><?=$title?></span>
        </div>
    </section>
    <?=$content?>
    <?=$updates?>
    <?=$widgets?>
</section>
