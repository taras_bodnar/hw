<div id="expenses-widget" class="oyi-widget">
    <div class="panel panel-info front">
        <div class="panel-heading">
            <i class="icon-chevron-sign-down"></i>
            <span><?=$lang->dashWigets['disc_space_title']?></span>
        </div>
        <div>
            <div class="form-group">
                <div id="hero-donut" class="graph" style="margin-top: 10px; height: 185px;"></div>
            </div>
        </div>
    </div>
</div>

<script>
    var m = Morris.Donut({
        element: 'hero-donut',
        data: <?=json_encode($data)?>,
        formatter: function (y) { return y + "%" }
    });
</script>