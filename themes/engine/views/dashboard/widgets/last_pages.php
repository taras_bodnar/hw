<div id="messages-widget" class="oyi-widget messages">
    <div class="panel panel-<?=$color?> front">
        <div class="panel-heading">
            <i class="icon-file-text-alt"></i>
            <span><?=$title?></span>
        </div>
        <div>
            <ul class="list-group pending">
               <?php foreach($pages as $page) : ?>
                <li class="list-group-item" onclick="self.location.href='<?=$type?>/edit/<?=$page['id']?>'">
                    <?php if(isset($page['img']) && !empty($page['img'])) : ?>
                    <i><img src="/uploads/content/<?=$page['id']?>/thumbnails/<?=$page['img']?>"></i>
                    <?php endif; ?>
                    <div class="text-holder">
                        <span class="description-text">
                            <?=$page['name']?>
                        </span>
                    </div>
                    <span class="stat-value">
                        <?=$page['editedon']?>
                    </span>
                </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
</div>