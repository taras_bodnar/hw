<div id="expenses-widget" class="oyi-widget">
    <div class="panel panel-info front">
        <div class="panel-heading">
            <i class="icon-chevron-sign-down"></i>
            <span>Статистика замовлень і запитів</span>
        </div>
        <div>
            <div class="form-group">
                <div id="order-donut" class="graph" style="margin-top: 10px; height: 185px;"></div>
            </div>
        </div>
    </div>
</div>

<script>
    var m = Morris.Donut({
        element: 'order-donut',
        data: <?=json_encode($data)?>,
        formatter: function (y) { return y }
    });
</script>