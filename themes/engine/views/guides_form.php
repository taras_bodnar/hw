<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 01.06.14 10:46
 */
 use \controllers\engine\Form;

 defined('SYSPATH') or die();

    Form::open("guides/process/$id");
        Form::openRow();
            Form::openCol('col-lg-12');

                Form::openPanel($lang->guides['title']);

                    $icon = strtr(Form::icon('icon-sort-by-alphabet'),array('"'=>"'"));
                    $icon_spinner = strtr(Form::icon('icon-spinner'),array('"'=>"'"));
                    $lang_id = 0; $lang_code=''; $i=0;
                foreach ($languages as $row) {

                    if($row['front_default'] == 1) {
                        $lang_id = $row['id']; $lang_code = $row['code'];
                    }

                    Form::html("<input type=\"hidden\" name=\"s_languages[{$row['id']}]\" value='{$row['code']}' class='s-languages'>");
                    if($i == 0) {
                        Form::formGroup(
                            '','',
                            Form::button(
                                $lang->core['translate'],
                                $icon,
                                array(
                                    'class'   =>'btn-translate-all btn-info',
                                    'onclick' => 'engine.translator.translateAll('. $lang_id .', \''. $lang_code .'\', this); return false;',
                                    'data-complete-text' => $icon .' '. $lang->core['translate'],
                                    'data-loading-text' => $icon_spinner .' '. $lang->core['begin_translation']
                                )
                            )
                        );
                    }
                    $i++;

                    Form::formGroup(
                        $lang->guides['name'] . ' ['. $row['name'] .']',
                        $lang->guides['name_tip'],
                        Form::textarea(array(
                            'type'        => 'text',
                            'name'        => "info[{$row['id']}][name]",
                            'required'    => 'required',
                            'placeholder' => $lang->guides['name_placeholder'],
                            'data-parsley-required' => 'true',
                            'value'       => isset($info[$row['id']]['name']) ? $info[$row['id']]['name'] : ''
                        ))
                    );
                }

                Form::formGroup(
                    $lang->guides['value'],
                    $lang->guides['value_tip'],
                    Form::input(array(
                        'type'        => 'text',
                        'name'        => "data[value]",
                        'placeholder' => $lang->guides['value_placeholder'],
                        'value'       => isset($data['value']) ? $data['value'] : ''
                    ))
                );
                Form::closePanel();

            Form::closeCol();
        Form::closeRow();
        Form::hidden('action',$action);
    Form::close();