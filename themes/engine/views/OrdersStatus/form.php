<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 01.06.14 10:46
 */
 use \controllers\engine\Form;

 defined('SYSPATH') or die();

    Form::open("ordersStatus/process/$id", array('id' => 'ordersStatusForm'));


    $icon = strtr(Form::icon('icon-sort-by-alphabet'),array('"'=>"'"));
    $icon_spinner = strtr(Form::icon('icon-spinner'),array('"'=>"'"));
    $lang_id = 0; $lang_code=''; $i=0;
    foreach ($languages as $row) {

        if($row['front_default'] == 1) {
            $lang_id = $row['id']; $lang_code = $row['code'];
        }

        Form::html("<input type=\"hidden\" name=\"s_languages[{$row['id']}]\" value='{$row['code']}' class='s-languages'>");
        if($i == 0) {
            Form::formGroup(
                '','',
                Form::button(
                    $lang->core['translate'],
                    $icon,
                    array(
                        'class'   =>'btn-translate-all btn-info',
                        'onclick' => 'engine.translator.translateAll('. $lang_id .', \''. $lang_code .'\', this); return false;',
                        'data-complete-text' => $icon .' '. $lang->core['translate'],
                        'data-loading-text' => $icon_spinner .' '. $lang->core['begin_translation']
                    )
                )
            );
        }
        $i++;

        Form::formGroup(
            $lang->ordersStatus['name'] . ' ['. $row['name'] .']',
            $lang->ordersStatus['name_tip'],
            Form::input(array(
                'type'        => 'text',
                'name'        => "info[{$row['id']}][name]",
                'required'    => 'required',
                'placeholder' => $lang->ordersStatus['name_placeholder'],
                'value'       => isset($info[$row['id']]['name']) ? $info[$row['id']]['name'] : ''
            ))
        );
    }


Form::formGroup(
            $lang->ordersStatus['bg_color'],
            $lang->ordersStatus['bg_color_tip'],
            Form::input(array(
                'type'        => 'color',
                'name'        => "data[bg_color]",
                'required'    => 'required',
                'class'       => 'color {hash:true}',
                'placeholder' => $lang->ordersStatus['bg_color_placeholder'],
                'value'       => isset($data['bg_color']) ? $data['bg_color'] : ''
            ))
        );

        Form::formGroup(
            $lang->ordersStatus['text_color'],
            $lang->ordersStatus['text_color_tip'],
            Form::input(array(
                'type'        => 'color',
                'name'        => "data[text_color]",
                'required'    => 'required',
                'class'       => 'color {hash:true}',
                'placeholder' => $lang->ordersStatus['text_color_placeholder'],
                'value'       => isset($data['text_color']) ? $data['text_color'] : ''
            ))
        );

        Form::hidden('action',$action);

        Form::customSuccessAction("
            var oTable = $('#ordersStatus').dataTable();
            oTable.fnDraw(oTable.fnSettings());
            $('.modal').modal('hide');
        ");
/*
    Form::html('<script src="/themes/engine/scripts/vendor/colorpicker/js/colorpicker.js"></script>');
    Form::html('<link rel="stylesheet" href="/themes/engine/scripts/vendor/colorpicker/css/colorpicker.css">');
    Form::html('
    <script>
        $(\'.color\').ColorPicker({
            onSubmit: function(hsb, hex, rgb, el) {
            $(el).val(hex);
            $(el).ColorPickerHide();
        },
            onBeforeShow: function () {
            $(this).ColorPickerSetColor(this.value);
        }
        })
        .bind(\'keyup\', function(){
            $(this).ColorPickerSetColor(this.value);
        });
        </script>
    ');
*/
    Form::close();