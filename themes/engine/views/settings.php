<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 28.08.14 21:24
 */
use \controllers\engine\Form;
 defined("SYSPATH") or die();

Form::open("settings/update");
    Form::openRow();
        Form::openCol('col-lg-4');
            Form::openPanel($lang->settings['global']);

                Form::formGroup(
                    $data['languages_create_info']['description'],
                    '',
                    Form::input(array(
                        'type'        => 'text',
                        'name'        => "data[languages_create_info]",
                        'required'    => 'required',
                        'value'       => $data['languages_create_info']['value']
                    ))
                );

                Form::formGroup(
                    $data['app_views_path']['description'],
                    '',
                    Form::input(array(
                        'type'        => 'text',
                        'name'        => "data[app_views_path]",
                        'required'    => 'required',
                        'value'       => $data['app_views_path']['value']
                    ))
                );
                Form::formGroup(
                    $data['app_chunks_path']['description'],
                    '',
                    Form::input(array(
                        'type'        => 'text',
                        'name'        => "data[app_chunks_path]",
                        'required'    => 'required',
                        'value'       => $data['app_chunks_path']['value']
                    ))
                );

                Form::formGroup(
                    $data['themes_path']['description'],
                    '',
                    Form::input(array(
                        'type'        => 'text',
                        'name'        => "data[themes_path]",
                        'required'    => 'required',
                        'value'       => $data['themes_path']['value']
                    ))
                );

                Form::formGroup(
                    $data['mod_path']['description'],
                    '',
                    Form::input(array(
                        'type'        => 'text',
                        'name'        => "data[mod_path]",
                        'required'    => 'required',
                        'value'       => $data['mod_path']['value']
                    ))
                );

            Form::closePanel();
        Form::closeCol();
        Form::openCol('col-lg-4');
            Form::openPanel($lang->settings['content']);

                Form::formGroup(
                    $data['autofil_title']['description'],
                    '',
                    Form::input(array(
                        'type'        => 'text',
                        'name'        => "data[autofil_title]",
                        'required'    => 'required',
                        'value'       => $data['autofil_title']['value']
                    ))
                );

                Form::formGroup(
                    $data['autotranslit_alias']['description'],
                    '',
                    Form::input(array(
                        'type'        => 'text',
                        'name'        => "data[autotranslit_alias]",
                        'required'    => 'required',
                        'value'       => $data['autotranslit_alias']['value']
                    ))
                );
                Form::formGroup(
                    $data['editor_language']['description'],
                    '',
                    Form::input(array(
                        'type'        => 'text',
                        'name'        => "data[editor_language]",
                        'required'    => 'required',
                        'value'       => $data['editor_language']['value']
                    ))
                );
                Form::formGroup(
                    $data['editor_bodyId']['description'],
                    '',
                    Form::input(array(
                        'type'        => 'text',
                        'name'        => "data[editor_bodyId]",
                        'required'    => 'required',
                        'value'       => $data['editor_bodyId']['value']
                    ))
                );
                Form::formGroup(
                    $data['editor_bodyClass']['description'],
                    '',
                    Form::input(array(
                        'type'        => 'text',
                        'name'        => "data[editor_bodyClass]",
                        'required'    => 'required',
                        'value'       => $data['editor_bodyClass']['value']
                    ))
                );

                Form::formGroup(
                    $data['editor_contentsCss']['description'],
                    '',
                    Form::input(array(
                        'type'        => 'text',
                        'name'        => "data[editor_contentsCss]",
                        'required'    => 'required',
                        'value'       => $data['editor_contentsCss']['value']
                    ))
                );

            Form::closePanel();
        Form::closeCol();
        Form::openCol('col-lg-4');
            Form::openPanel($lang->settings['images']);

                Form::formGroup(
                    $data['content_images_dir']['description'],
                    '',
                    Form::input(array(
                        'type'        => 'text',
                        'name'        => "data[content_images_dir]",
                        'required'    => 'required',
                        'value'       => $data['content_images_dir']['value']
                    ))
                );
                Form::formGroup(
                    $data['content_images_thumb_dir']['description'],
                    '',
                    Form::input(array(
                        'type'        => 'text',
                        'name'        => "data[content_images_thumb_dir]",
                        'required'    => 'required',
                        'value'       => $data['content_images_thumb_dir']['value']
                    ))
                );
                Form::formGroup(
                    $data['content_images_source_dir']['description'],
                    '',
                    Form::input(array(
                        'type'        => 'text',
                        'name'        => "data[content_images_source_dir]",
                        'required'    => 'required',
                        'value'       => $data['content_images_source_dir']['value']
                    ))
                );
                Form::formGroup(
                    $data['img_source_size']['description'],
                    '',
                    Form::input(array(
                        'type'        => 'text',
                        'name'        => "data[img_source_size]",
                        'required'    => 'required',
                        'value'       => $data['img_source_size']['value']
                    ))
                );
            Form::closePanel();
        Form::closeCol();
    Form::closeRow();
    Form::openRow();
        Form::openCol('col-lg-4');
            Form::openPanel($lang->settings['analytics']);

                Form::formGroup(
                    $data['google_ananytics_id']['description'],
                    '',
                    Form::input(array(
                        'type'        => 'text',
                        'name'        => "data[google_ananytics_id]",
//                        'required'    => 'required',
                        'value'       => $data['google_ananytics_id']['value']
                    ))
                );

                Form::formGroup(
                    $data['google_webmaster_id']['description'],
                    '',
                    Form::input(array(
                        'type'        => 'text',
                        'name'        => "data[google_webmaster_id]",
//                        'required'    => 'required',
                        'value'       => $data['google_webmaster_id']['value']
                    ))
                );

                Form::formGroup(
                    $data['yandex_webmaster_id']['description'],
                    '',
                    Form::input(array(
                        'type'        => 'text',
                        'name'        => "data[yandex_webmaster_id]",
//                        'required'    => 'required',
                        'value'       => $data['yandex_webmaster_id']['value']
                    ))
                );
                Form::formGroup(
                    $data['yandex_metrika']['description'],
                    '',
                    Form::input(array(
                        'type'        => 'text',
                        'name'        => "data[yandex_metrika]",
//                        'required'    => 'required',
                        'value'       => $data['yandex_metrika']['value']
                    ))
                );

            Form::closePanel();
        Form::closeCol();
        Form::openCol('col-lg-4');
            Form::openPanel($lang->settings['email_templates']);

                Form::formGroup(
                    $data['et_header']['description'],
                    '',
                    Form::textarea(array(
                        'type'        => 'text',
                        'name'        => "data[et_header]",
                        'value'       => $data['et_header']['value']
                    ))
                );

                Form::formGroup(
                    $data['et_footer']['description'],
                    '',
                    Form::textarea(array(
                        'type'        => 'text',
                        'name'        => "data[et_footer]",
                        'value'       => $data['et_footer']['value']
                    ))
                );
            Form::closePanel();
        Form::closeCol();
        Form::openCol('col-lg-4');
            Form::openPanel($lang->settings['errors_pages']);

                Form::formGroup(
                    $data['page_404']['description'],
                    '',
                    Form::input(array(
                        'type'        => 'text',
                        'name'        => "data[page_404]",
                        'required'    => 'required',
                        'value'       => $data['page_404']['value']
                    ))
                );
            Form::closePanel();
        Form::closeCol();

    Form::closeRow();
/*
    Form::openRow();
        Form::openCol('col-lg-12');
                $mtitle = $lang->settings['modules'];
                Form::html("
                    <div class=\"panel panel-default panel-block panel-title-block\">
                        <div class=\"panel-heading\">
                            <div>
                                <i class=\"icon-puzzle-piece\"></i>
                                <h1><span class=\"page-title\">{$mtitle}</span></h1>
                            </div>
                        </div>
                    </div>
              ");
        Form::closeCol();

    Form::closeRow();

*/


Form::closeRow();
Form::openRow();
Form::openCol('col-lg-4');
Form::openPanel("Бали");

Form::formGroup(
    'Ціна за Hot аккаунт (14 днів)',
    '',
    Form::input(array(
        'type' => 'text',
        'name' => "data[priceWeek]",
        'required' => 'required',
        'value' => $data['priceWeek']['value']
    ))
);

Form::formGroup(
    'Ціна за Hot аккаунт (30 днів)',
    '',
    Form::input(array(
        'type' => 'text',
        'name' => "data[priceMonth]",
        'required' => 'required',
        'value' => $data['priceMonth']['value']
    ))
);

Form::formGroup(
    'Ціна за Gold аккаунт',
    '',
    Form::input(array(
        'type' => 'text',
        'name' => "data[priceYear]",
        'required' => 'required',
        'value' => $data['priceYear']['value']
    ))
);

Form::formGroup(
    'Ціна за одне місто',
    '',
    Form::input(array(
        'type' => 'text',
        'name' => "data[priceOneC]",
        'required' => 'required',
        'value' => $data['priceOneC']['value']
    ))
);

Form::formGroup(
    'Ціна за два міста',
    '',
    Form::input(array(
        'type' => 'text',
        'name' => "data[priceTwoC]",
        'required' => 'required',
        'value' => $data['priceTwoC']['value']
    ))
);


Form::closePanel();
Form::closeCol();

Form::closeRow();

Form::close();
