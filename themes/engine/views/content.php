<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 31.05.14 19:59
 */
 
defined('SYSPATH') or die();
?>
<?=$sidebar?>
<section class="wrapper <?php if(!empty($sidebar)) echo 'retracted'; else echo 'extended'; ?> scrollable">
    <?=$user_menu?>
    <?=$breadcrumb?>
    <div class="row">
        <div class="col-lg-12">
            <?=$title_block?>
            <div id="notification"></div>
            <?=$content?>
        </div>
    </div>
</section>