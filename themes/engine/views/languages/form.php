<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 01.06.14 10:46
 */
 use \controllers\engine\Form;

 defined('SYSPATH') or die();

    Form::open("languages/process/$id");
        Form::openRow();
            Form::openCol('col-lg-8');

                Form::openPanel($lang->languages['title']);
                    Form::formGroup(
                        $lang->languages['name'],
                        $lang->languages['name_tip'],
                        Form::input(array(
                            'type'        => 'text',
                            'name'        => "data[name]",
                            'required'    => 'required',
                            'placeholder' => $lang->languages['name_placeholder'],
                            'data-parsley-required' => 'true',
                            'value'       => isset($data['name']) ? $data['name'] : ''
                        ))
                    );
                    Form::formGroup(
                        $lang->languages['code'],
                        $lang->languages['code_tip'],
                        Form::input(array(
                            'type'        => 'text',
                            'name'        => "data[code]",
                            'required'    => 'required',
                            'placeholder' => $lang->languages['code_placeholder'],
                            'data-parsley-required' => 'true',
                            'value'       => isset($data['code']) ? $data['code'] : ''
                        ))
                    );

                Form::closePanel();

            Form::closeCol();
            Form::openCol('col-lg-4');
                Form::openPanel($lang->core['params']);

                    Form::formGroup(
                        $lang->languages['front'],
                        $lang->languages['front_tip'],
                        Form::buttonSwitch('data[front]',isset($data['front']) ? $data['front'] : 1),
                        array('class'=>'text-right')
                    );

                    Form::formGroup(
                        $lang->languages['back'],
                        $lang->languages['back_tip'],
                        Form::buttonSwitch('data[back]',isset($data['back']) ? $data['back'] : 0),
                        array('class'=>'text-right')
                    );

                    Form::formGroup(
                        $lang->languages['front_default'],
                        $lang->languages['front_default'],
                        Form::buttonSwitch('data[front_default]',isset($data['front_default']) ? $data['front_default'] : 0),
                        array('class'=>'text-right')
                    );

                    Form::formGroup(
                        $lang->languages['back_default'],
                        $lang->languages['back_default'],
                        Form::buttonSwitch('data[back_default]',isset($data['back_default']) ? $data['back_default'] : 0),
                        array('class'=>'text-right')
                    );


                Form::closePanel();

            Form::closeCol();
        Form::closeRow();
        Form::hidden('action',$action);
    Form::close();