<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 13.11.14 16:35
 */
defined("SYSPATH") or die();
?>
<div class="panel panel-default panel-block" id="translate-box">
    <div class="list-group">
        <div class="list-group-item button-demo" >
            <h4 class="section-title"><?=$lang->languages['au_title']?></h4>
            <?php foreach ($languages as $l): ?>
                <p><?=$lang->languages['au_desc']?> <?=$l['name']?></p>
                <form action="languages/processTranslate/" id="translate" method="post">
                    <ul>
                        <?php foreach ($tables as $k => $t) :
                            if($t == 'components_info' && $l['back'] == 0) continue;
                        ?>
                        <li id="t-<?=$t?>" class="table-to-translate">
                            <?=$t?> <span class="process"></span>
                            <input type="hidden" name="table[]" value="<?=$t?>"/>
                            <div id="progress-<?=$t?>" class="progress progress-thin  progress-striped active">
                                <div style="width: 0%;" class="progress-bar progress-bar-success" data-original-title="" title=""></div>
                            </div>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                    <input type="hidden" name="languages_id" value="<?=$l['id']?>" />
                </form>

            <?php endforeach; ?>
        </div>
    </div>
</div>
<script src="<?=$template_url?>scripts/vendor/jquery.form.js"></script>
<script>
    $(function(){

        $('#translate')
            .ajaxForm({
                beforeSend: function()
                {
                    $('#submit').attr('disabled', true);
                },
                success: function(d)
                {
                    $(d.t).each(function(i,e){
                        translateTable(e.table, e.start, e.total, e.col, e.from_lang, e.to_lang);
                    });
                },
                dataType: 'json'
            })
            .submit();

        function successTranslation(lang)
        {
            $('#translate-box').hide();
            engine.languages.autoGenerateAlias(lang);
        }

        function translateTable(table, start, total, col, from_lang,to_lang)
        {
            if(total == 0 || start >= total) {
                $('#t-' + table).hide();

                if($('.table-to-translate:visible').length == 0){
                    successTranslation(from_lang);

                    return false;
                }
                return false;
            }


            $('#t-' + table).find('.process').html('(<span class="s">'+ start +'</span> : <span class="t">'+ total +'</span>)');

            var percent =  100 / total, done = Math.round( start * percent ) ;
            $("#progress-"+table).find('div').css('width', done + '%');

            if(start < total){

                $.ajax({
                    type: "POST",
                    url: './languages/translate/',
                    data: {
                        table: table,
                        start: start,
                        total: total,
                        col  : col,
                        from_lang: from_lang,
                        to_lang  : to_lang
                    },
                    success: function(d){
                        setTimeout(function(){
                            translateTable(d.table, d.start, d.total, d.col, d.from_lang, d.to_lang);
                        }, 1000);
                    },
                    dataType: 'json'
                });

            }
            return false;
        }
    });
</script>