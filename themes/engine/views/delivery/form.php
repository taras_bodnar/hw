<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 01.06.14 10:46
 */
 use \controllers\engine\Form;

 defined('SYSPATH') or die();

    Form::open("delivery/process/$id", array(), $lang);
        Form::openRow();
            Form::openCol('col-lg-8');

                Form::openPanel($lang->delivery['title']);
                $hide='';
                if(count($languages) > 1) {
                    $hide='hide';
                    Form::languagesSwitch(
                        $lang->core['sel_languages'],
                        $lang->core['sel_languages_tip'],
                        $languages,
                        $lang
                    );
                }
                foreach ($languages as $row) {
                    Form::html("<div class=\"language-switch $hide language-{$row['id']}\">");
                    Form::formGroup(
                        $lang->delivery['name'],
                        $lang->delivery['name_tip'],
                        Form::input(array(
                            'type'        => 'text',
                            'name'        => "info[{$row['id']}][name]",
                            'required'    => 'required',
                            'placeholder' => $lang->delivery['name_placeholder'],
                            'data-parsley-required' => 'true',
                            'value'       => isset($info[$row['id']]['name']) ? $info[$row['id']]['name'] : ''
                        ))
                    );
                    Form::formGroup(
                        $lang->delivery['desc'],
                        $lang->delivery['desc_tip'],
                        Form::editor(
                            array(
                                'name' => "info[{$row['id']}][description]",
                                'placeholder' => $lang->delivery['desc_placeholder'],
                                'value'       => isset($info[$row['id']]['description']) ? $info[$row['id']]['description'] : ''
                            )
                        )
                    );
                    Form::html('</div>');// .info
                }


            Form::closePanel();

            Form::closeCol();

            Form::openCol('col-lg-4');

                Form::openPanel($lang->core['params']);


                Form::formGroup(
                    $lang->delivery['price'],
                    $lang->delivery['price_tip'],
                    Form::input(
                        array(
                            'type'        => 'text',
                            'name'        => "data[price]",
//                            'required'    => 'required',
                            'placeholder' => $lang->delivery['price_placeholder'],
                            'value'       => isset($data['price']) ? $data['price'] : ''
                        )
                    )
                );
                Form::formGroup(
                    $lang->delivery['free_from'],
                    $lang->delivery['free_from_tip'],
                    Form::input(
                        array(
                            'type'        => 'text',
                            'name'        => "data[free_from]",
//                            'required'    => 'required',
                            'placeholder' => $lang->delivery['free_from_placeholder'],
                            'value'       => isset($data['free_from']) ? $data['free_from'] : ''
                        )
                    )
                );

                Form::formGroup(
                    $lang->delivery['published'],
                    $lang->delivery['published'],
                    Form::buttonSwitch('data[published]',isset($data['published']) ? $data['published'] : 0),
                    array('class'=>'text-right')
                );

                Form::closePanel();

                Form::openPanel($lang->delivery['payment']);

                    Form::formGroup(
                        $lang->delivery['payment_items'],
                        $lang->delivery['payment_items_tip'],
                        Form::pickList(
                            array(
                                'name' => 'dp[]',
                                'sort_url' => 'delivery/sort',
                                'nav_id'   => $id
                            ),
                            $payment
                        )
                    );

                Form::closePanel();
            Form::closeCol();
        Form::closeRow();
        Form::hidden('action', $action);

    Form::close();