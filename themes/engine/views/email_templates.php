<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 01.06.14 10:46
 */
 use \controllers\engine\Form;

 defined('SYSPATH') or die();

    Form::open("EmailTemplates/process/$id");

        Form::html("
            <link rel=\"stylesheet\" href=\"{$template_url}scripts/vendor/codemirror/lib/codemirror.css\">
            <script src=\"{$template_url}scripts/vendor/codemirror/lib/codemirror.js\"></script>
            <script src=\"{$template_url}scripts/vendor/codemirror/mode/css.js\"></script>
            <script src=\"{$template_url}scripts/vendor/codemirror/mode/htmlmixed.js\"></script>
        ");

        Form::openRow();
            Form::openCol('col-lg-6');

                Form::openPanel($lang->EmailTemplates['title']);

                Form::formGroup(
                    $lang->EmailTemplates['name'],
                    $lang->EmailTemplates['name_tip'],
                    Form::input(array(
                        'type'        => 'text',
                        'name'        => "data[name]",
                        'required'    => 'required',
                        'placeholder' => $lang->EmailTemplates['name_placeholder'],
                        'value'       => isset($data['name']) ? $data['name'] : ''
                    ))
                );

                Form::formGroup(
                    $lang->EmailTemplates['code'],
                    $lang->EmailTemplates['code_tip'],
                    Form::input(array(
                        'type'        => 'text',
                        'name'        => "data[code]",
                        'data-parsley-pattern' => '[a-zA-Z0-9_]',
                        'required'    => 'required',
                        'placeholder' => $lang->EmailTemplates['code_placeholder'],
                        'value'       => isset($data['code']) ? $data['code'] : ''
                    ))
                );

                Form::formGroup(
                    $lang->EmailTemplates['email'],
                    $lang->EmailTemplates['email_tip'],
                    Form::input(array(
                        'type'        => 'text',
                        'name'        => "data[email]",
                        'required'    => 'required',
                        'placeholder' => $lang->EmailTemplates['email_placeholder'],
                        'value'       => isset($data['email']) ? $data['email'] : ''
                    ))
                );

                Form::closePanel();

            Form::closeCol();

            Form::openCol('col-lg-6');
                Form::openPanel($lang->core['params']);
                    $hide='';
                    if(count($languages) > 1) {
                        $hide='hide';
                        Form::languagesSwitch(
                            $lang->core['sel_languages'],
                            $lang->core['sel_languages_tip'],
                            $languages,
                            $lang
                        );
                    }
                    foreach ($languages as $row) {
                        Form::html("<div class=\"language-switch $hide language-{$row['id']}\">");

                        Form::formGroup(
                            $lang->EmailTemplates['reply_to'],
                            $lang->EmailTemplates['reply_to_tip'],
                            Form::input(array(
                                'type'        => 'text',
                                'name'        => "info[{$row['id']}][reply_to]",
                                'required'    => 'required',
                                'placeholder' => $lang->EmailTemplates['reply_to_placeholder'],
                                'data-parsley-required' => 'true',
                                'value'       => isset($info[$row['id']]['reply_to']) ? $info[$row['id']]['reply_to'] : ''
                            ))
                        );

                        Form::formGroup(
                            $lang->EmailTemplates['subject'],
                            $lang->EmailTemplates['subject_tip'],
                            Form::input(array(
                                'type'        => 'text',
                                'name'        => "info[{$row['id']}][subject]",
                                'required'    => 'required',
                                'placeholder' => $lang->EmailTemplates['subject_placeholder'],
                                'data-parsley-required' => 'true',
                                'value'       => isset($info[$row['id']]['subject']) ? $info[$row['id']]['subject'] : ''
                            ))
                        );

                        Form::formGroup(
                            $lang->EmailTemplates['body'],
                            $lang->EmailTemplates['body_tip'],
                            Form::textarea(array(
                                'name'        => "info[{$row['id']}][body]",
                                'required'    => 'required',
                                'placeholder' => $lang->EmailTemplates['body_placeholder'],
                                'data-parsley-required' => 'true',
                                'value'       => isset($info[$row['id']]['body']) ? $info[$row['id']]['body'] : ''
                            ))
                        );
                        Form::html("
                                    <script>
                                   var cm{$row['id']} = CodeMirror.fromTextArea(document.getElementById('info_{$row['id']}_body'), {
                                                    theme: 'neo',
                                                    mode: 'html',
                                                    styleActiveLine: true,
                                                    lineNumbers: true,
                                                    lineWrapping: true,
                                                    autoCloseTags: true,
                                                    matchBrackets: true
                                                  });

                                        cm{$row['id']}.on(\"change\", function() {
                                        cm{$row['id']}.save();
                                        var c{$row['id']} = cm{$row['id']}.getValue();
                                        $(\"textarea#info_{$row['id']}_body\").html(c{$row['id']});
                                      });
                                    </script>
                                    ");

                        Form::html('</div>');// .info
                    }
                Form::closePanel();
            Form::closeCol();
        Form::closeRow();
        Form::hidden('action',$action);

    Form::close();