<?php
/**
 * Company Otakoyi.com
 * Author: wg
 * Date: 11.01.15 0:53
 */
use \controllers\engine\Form;

defined('SYSPATH') or die();

Form::open("comments/process/$parent_id");
Form::formGroup(
    $lang->comments['message'],
    $lang->comments['message_tip'],
    Form::textarea(array(
        'type'        => 'text',
        'name'        => "data[message]",
        'required'    => 'required',
        'placeholder' => $lang->comments['message_placeholder']
    ))
);
Form::customSuccessAction("
    if(d.s>0){
        var oTable = $('#comments').dataTable();
        oTable.fnDraw(oTable.fnSettings());
        $('.modal').modal('hide');
    }
");
Form::close();