<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 18.10.14 22:39
 */
defined("SYSPATH") or die();
?>
<br>
<div class="row">
    <div class="col-lg-3"></div>
    <div class="col-lg-6">

        <div class="alert alert-dismissable alert-info fade in">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="icon-remove"></i></button>
            <span class="title"><i class="icon-info-sign"></i> Доступне оновлення</span>
            <p>З радістю повідомляємо вас про оновлення системи. Ви можете завантажити його безкоштовно, або відмовитись від нього</p>
            <br/>
            <footer class="panel-footer text-right">
                <button class="btn btn-primary">Встановити</button>
                <button class="btn btn-danger" style="margin-left: 10px;">Ні, все влаштовує</button>
            </footer>
        </div>

    </div>
    <div class="col-lg-3"></div>
</div>
++++++
<pre><?php print_r($data);?></pre>
++++++
