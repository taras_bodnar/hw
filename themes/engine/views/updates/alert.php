<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 18.10.14 22:39
 */
defined("SYSPATH") or die();
?>
<br>
<div class="row">
    <div class="col-lg-3"></div>
    <div class="col-lg-6" id="notifications">

        <div class="alert alert-dismissable alert-info fade in">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="icon-remove"></i></button>
            <span class="title"><i class="icon-info-sign"></i> <?=$lang->updates['title']?></span>
            <p><?=$lang->updates['text']?></p>
            <br/>
            <footer class="panel-footer text-right">
                <button class="btn btn-primary" onclick="engine.updates.get();"><?=$lang->updates['install']?></button>
                <button class="btn btn-danger" style="margin-left: 10px;" onclick="engine.updates.disable();"><?=$lang->updates['disable']?></button>
            </footer>
        </div>

    </div>
    <div class="col-lg-3"></div>
</div>