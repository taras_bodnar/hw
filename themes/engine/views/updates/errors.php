<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 18.10.14 22:39
 */
defined("SYSPATH") or die();
?>
<div class="alert alert-dismissable alert-<?=$e?> fade in">
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="icon-remove"></i></button>
    <span class="title"><i class="icon-info-sign"></i> <?=$t?></span>
    <p><?=$m?></p>
</div>