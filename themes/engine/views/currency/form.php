<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 01.06.14 10:46
 */
 use \controllers\engine\Form;

 defined('SYSPATH') or die();

    Form::open("currency/process/$id", array('id' => 'currencyForm'));

        Form::formGroup(
            $lang->currency['name'],
            $lang->currency['name_tip'],
            Form::input(array(
                'type'        => 'text',
                'name'        => "data[name]",
                'required'    => 'required',
                'placeholder' => $lang->currency['name_placeholder'],
                'value'       => isset($data['name']) ? $data['name'] : ''
            ))
        );

        Form::formGroup(
            $lang->currency['code'],
            $lang->currency['code_tip'],
            Form::input(array(
                'type'        => 'text',
                'name'        => "data[code]",
                'required'    => 'required',
                'placeholder' => $lang->currency['code_placeholder'],
                'value'       => isset($data['code']) ? $data['code'] : ''
            ))
        );

        Form::formGroup(
            $lang->currency['symbol'],
            $lang->currency['symbol_tip'],
            Form::input(array(
                'type'        => 'text',
                'name'        => "data[symbol]",
                'required'    => 'required',
                'placeholder' => $lang->currency['symbol_placeholder'],
                'value'       => isset($data['symbol']) ? $data['symbol'] : ''
            ))
        );
        Form::formGroup(
            $lang->currency['rate'],
            $lang->currency['rate_tip'],
            Form::input(array(
                'type'        => 'text',
                'name'        => "data[rate]",
                'required'    => 'required',
                'placeholder' => $lang->currency['rate_placeholder'],
                'value'       => isset($data['rate']) ? $data['rate'] : ''
            ))
        );

        Form::formGroup(
            $lang->currency['is_main'],
            $lang->currency['is_main'],
            Form::buttonSwitch('data[is_main]', isset($data['is_main']) ? $data['is_main'] : 0)
        );

        Form::hidden('action',$action);

        Form::customSuccessAction("
            var oTable = $('#currency').dataTable();
            oTable.fnDraw(oTable.fnSettings());
            $('.modal').modal('hide');
        ");
    Form::close();