<?php
/**
 * Company Otakoyi.com
 * Author: wg
 * Date: 06.01.15 14:24
 */
defined("SYSPATH") or die();
use \controllers\engine\Form;

Form::open("promoCodes/process/$id", array(), $lang);

Form::formGroup(
    $lang->promo_codes['code'],
    $lang->promo_codes['code_tip'],
    Form::input(array(
        'type'        => 'text',
        'name'        => "data[code]",
        'required'    => 'required',
        'data-parsley-type' => 'alphanum',
        'placeholder' => $lang->promo_codes['code_placeholder'],
        'value'       => isset($data['code']) ? $data['code'] : ''
    ))
);
Form::formGroup(
    $lang->promo_codes['discount'],
    $lang->promo_codes['discount_tip'],
    Form::inputGroup(
        array(
            'type'        => 'text',
            'name'        => "data[discount]",
            'required'    => 'required',
            'data-parsley-type' => 'number',
            'placeholder' => $lang->promo_codes['discount_placeholder'],
            'value'       => isset($data['discount']) ? $data['discount'] : ''
        ),
        '',
        Form::select(
            array(
                'name' => 'data[type]'
            ),
            $type
        )
    )
);
Form::formGroup(
    $lang->promo_codes['minp'],
    $lang->promo_codes['minp_tip'],
    Form::input(array(
        'type'        => 'text',
        'name'        => "data[minp]",
        'placeholder' => $lang->promo_codes['minp_placeholder'],
        'value'       => isset($data['minp']) ? $data['minp'] : ''
    ))
);
Form::formGroup(
    $lang->promo_codes['expire'],
    $lang->promo_codes['expire_tip'],
    Form::input(array(
        'type'        => 'date',
        'name'        => "data[expire]",
        'placeholder' => $lang->promo_codes['expire_placeholder'],
        'value'       => isset($data['expire']) ? $data['expire'] : ''
    ))
);
Form::formGroup(
    $lang->promo_codes['single'],
    $lang->promo_codes['single_tip'],
    Form::buttonSwitch('data[single]',isset($data['single']) ? $data['single'] : 1),
    array('class'=>'text-right')
);

Form::hidden('action', $action);
Form::customSuccessAction("
    if(d.s > 0){
        var oTable = $('#PromoCodes').dataTable();
        oTable.fnDraw(oTable.fnSettings());
        $('.modal').modal('hide');
    }
");
Form::close();
