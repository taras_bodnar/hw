<?php $total=0; ?>
    <div class="table-responsive">
        <table class="table table-bordered table-striped">
            <thead class="">

            <tr>
                <th>#</th>
                <th>Назва</th>
                <th>Ціна</th>
                <th>Кількість</th>
                <th>Сума</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($products as $item) :
                $total += $item['quantity'] * $item['price'];
                ?>
            <tr>
                <td><?=$item['id']?></td>
                <td><?=$item['name']?></td>
                <td><?=round($item['price'],2) ?></td>
                <td><?=$item['quantity']?></td>
                <td><?= round($item['quantity'] * $item['price'],2)?></td>
            </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<br/>
    <div class="row">
        <div class="col-md-4 col-md-offset-8">
            <section class="panel stat stat-info">
                <div class="panel-heading">
                    <div>
                        <h2>Загальна вартість:</h2>
                        <div class="counter counter-small"><?=round($total,2)?> грн.</div>
                    </div>
                </div>
            </section>
            <section class="panel stat stat-info">
                <div class="panel-heading">
                    <div>
                        <h2>Доставка:</h2>
                        <div class="counter counter-small"><?=round($data['delivery_price'],2)?> грн.</div>
                    </div>
                </div>
            </section>
            <section class="panel stat stat-info">
                <div class="panel-heading">
                    <div>
                        <h2>Сума:</h2>
                        <div class="counter counter-small"><?=$data['delivery_price'] + $total ?> грн.</div>
                    </div>
                </div>
            </section>
        </div>
    </div>