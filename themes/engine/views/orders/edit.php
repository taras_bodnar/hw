<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 01.06.14 10:46
 */
use \controllers\engine\Form;

defined('SYSPATH') or die();

Form::open("orders/process/$id");
Form::openRow();
Form::openCol('col-lg-6');

Form::openPanel(
      $lang->orders['info']
    . (isset($data['code']) ? $data['code'] : '')
);

Form::formGroup(
    'Дата замовлення',
    '',
    Form::input(array(
        'type'        => 'text',
        'readonly'    => 'readonly',
        'value'       => isset($data['createdon']) ? $data['createdon'] : ''
    ))
);

Form::formGroup(
    'Сума, грн.',
    '',
    Form::input(array(
        'type'        => 'text',
        'readonly'    => 'readonly',
        'value'       => isset($data['total']) ? $data['total'] : ''
    ))
);
Form::formGroup(
    'Оплачено',
    '',
    Form::buttonSwitch('data[pay]',isset($data['pay']) ? $data['pay'] : 0)
);
if($data['pay'] > 0){

    Form::formGroup(
        'Дата оплати',
        '',
        Form::input(array(
            'type'        => 'text',
            'readonly'    => 'readonly',
            'value'       => isset($data['pay_date']) ? $data['pay_date'] : ''
        ))
    );
}

Form::formGroup(
    'Статус',
    '',
    Form::select(
        array(
            'name'        => 'data[status_id]',
            'type'        => 'text',
            'readonly'    => 'readonly',
            'value'       => isset($data['pay_date']) ? $data['pay_date'] : ''
        ),
        $statuses,
        $data['status_id']
    )
);
Form::formGroup(
    'Коментар',
    'Коментар до замовлення',
    Form::textarea(array(
        'name'        => 'data[comment]',
        'value'       => isset($data['comment']) ? $data['comment'] : ''
    ))
);
if(!empty($status_history)){

    $history=array();
    foreach ($status_history as $s) {
        $history[] = "{$s['createdon']}  {$s['name']} <br>Коментар: <i>{$s['comment']}</i>";
    }

    Form::formGroup(
        'Історія змін:',
        '',
        '<ul><li>'.implode('<li>', $history) . '</ul>'
    );
}
Form::closePanel();
Form::closeCol();
//echo '<pre>'; print_r($data); echo '</pre>';
Form::openCol('col-lg-6');
Form::openPanel(
    $lang->orders['user_info']
 . (isset($data['one_click']) && $data['one_click']==1 ? " <label class='label label-success'>Один клік</label>" : '')
);

    Form::formGroup(
        $lang->orders['user_name'],
        $lang->orders['user_name_tip'],
        Form::input(array(
            'type'        => 'text',
            'readonly'    => 'readonly',
            'value'       => isset($data['user_name']) ? $data['user_name'] : ''
        ))
    );
    Form::formGroup(
        $lang->orders['user_phone'],
        '',
        Form::input(array(
            'type'        => 'text',
            'readonly'    => 'readonly',
            'value'       => isset($data['user_phone']) ? $data['user_phone'] : ''
        ))
    );
    Form::formGroup(
        $lang->orders['user_email'],
        '',
        Form::input(array(
            'type'        => 'text',
            'readonly'    => 'readonly',
            'value'       => isset($data['user_email']) ? $data['user_email'] : ''
        ))
    );

Form::closePanel();
Form::closeCol();
Form::closeRow();
    Form::openRow();
        Form::openCol('col-lg-12');
            Form::openPanel($lang->orders['products']);
                Form::html($products);
            Form::closePanel();
        Form::closeCol();
    Form::closeRow();
Form::hidden('action', $action);
Form::close();