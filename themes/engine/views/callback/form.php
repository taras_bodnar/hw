<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 01.06.14 10:46
 */
 use \controllers\engine\Form;

 defined('SYSPATH') or die();

    Form::open("callback/process/$id", array('id' => 'CallbackForm'));

        Form::formGroup(
            $lang->callback['name'],
            '',
            Form::input(array(
                'type'        => 'text',
                'name'        => "data[name]",
                'required'    => 'required',
                'value'       => isset($data['name']) ? $data['name'] : ''
            ))
        );

        Form::formGroup(
            $lang->callback['phone'],
            '',
            Form::input(array(
                'type'        => 'text',
                'name'        => "data[phone]",
                'required'    => 'required',
                'value'       => isset($data['phone']) ? $data['phone'] : ''
            ))
        );

        Form::formGroup(
            $lang->callback['comment'],
            '',
            Form::textarea(
                array(
                    'name'        => "data[comment]",
                    'required'    => 'required',
                    'value'       => isset($data['comment']) ? $data['comment'] : ''
                )
            )
        );
        Form::formGroup(
            $lang->callback['date'],
            '',
            Form::input(array(
                'type'        => 'text',
                'name'        => "data[date]",
                'required'    => 'required',
                'value'       => isset($data['date']) ? $data['date'] : ''
            ))
        );

        Form::formGroup(
            $lang->callback['processed'],
            $lang->callback['processed'],
            Form::buttonSwitch('data[processed]', isset($data['processed']) ? $data['processed'] : 0)
        );

        Form::hidden('action',$action);

        Form::customSuccessAction("
            var oTable = $('#Callback').dataTable();
            oTable.fnDraw(oTable.fnSettings());
            $('.modal').modal('hide');
        ");
    Form::close();