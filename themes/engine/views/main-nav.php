<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 25.05.14 20:24
 */
 
defined('SYSPATH') or die();
//    \controllers\Engine::dump($nav);
?>
<nav class="main-menu" data-step='2' data-intro='This is the extendable Main Navigation Menu.' data-position='right'>
<ul>
    <li>
        <a href="./" title="<?=$dashInfo['description']?>">
            <i class="<?=$dashInfo['icon']?> nav-icon"></i>
            <span class="nav-text"><?=$dashInfo['name']?></span>
        </a>
    </li>
    <?php foreach($nav as $item) : ?>
    <li <?php if($item['isfolder']) echo 'class="has-subnav"'; ?>>
        <a href="<?=$item['isfolder']?'#':$item['controller']?>" title="<?=$item['description']?>">
            <i class="<?=$item['icon']?> nav-icon"></i>
            <span class="nav-text">
                <?=$item['name']?>
            </span>
            <?php if($item['isfolder']): ?>
            <i class="icon-angle-right"></i>
            <?php endif; ?>
        </a>
        <?php if($item['isfolder']) : ?>
            <ul>
            <?php foreach ($item['children'] as $subnav) :  ?>
                <li>
                    <a class="subnav-text" href="<?=$subnav['controller']?>/index" title="<?=$subnav['description']?>">
                        <?=$subnav['name']?>
                    </a>
                </li>
            <?php endforeach; ?>
            </ul>
        <?php endif ; ?>
    </li>
    <?php endforeach ; ?>
</ul>
<ul class="logout">
    <li>
        <a href="auth/logout">
            <i class="icon-off nav-icon"></i>
                        <span class="nav-text">
                            <?=$lang->core['logout']?>
                        </span>
        </a>
    </li>
</ul>
</nav>