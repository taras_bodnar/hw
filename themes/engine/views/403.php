
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7 lt-ie10"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8 lt-ie10"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9 lt-ie10"> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js"> <!--<![endif]-->
<head>

    <base href="<?=$base_url?>engine/" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>403 Forbinden</title>
    <meta name="description" content="<?=$description?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

    <link rel="stylesheet" href="<?=$template_url?>styles/bootstrap.css">

    <!-- Page-specific Plugin CSS: -->
    <link rel="stylesheet" href="<?=$template_url?>styles/core.css">
    <link rel="stylesheet" href="<?=$template_url?>styles/vendor/animate.css">

    <!-- adds CSS media query support to IE8   -->
    <!--[if lt IE 9]>
    <script src="<?=$template_url?>scripts/html5shiv.js"></script>
    <script src="<?=$template_url?>scripts/vendor/respond.min.js"></script>
    <![endif]-->

    <!-- Fonts CSS: -->
    <link rel="stylesheet" href="<?=$template_url?>styles/6227bbe5.font-awesome.css" type="text/css" />
    <link rel="stylesheet" href="<?=$template_url?>styles/40ff7bd7.font-titillium.css" type="text/css" />

    <!-- Common Scripts: -->
    <script src="<?=$template_url?>scripts/jquery.min.js"></script>
</head>

<body class="<?=$body_class?>">
<script>
    var theme = $.cookie('OyiTheme') || 'default';
    $('body').removeClass (function (index, css) {
        return (css.match (/\btheme-\S+/g) || []).join(' ');
    });
    if (theme !== 'default') $('body').addClass(theme);

</script>
<div class="panel panel-default panel-block panel-error-block">
    <div class="panel-heading">
        <div>
            <i class="icon-frown"></i>
            <h1>
                <small>
                    <?=$lang->e403['title']?>
                </small>
                <?=$lang->e403['desc']?>
            </h1>
            <div class="error-code">
                403
            </div>
        </div>
    </div>
</div>
</body>
</html>