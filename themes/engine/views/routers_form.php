<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 01.06.14 10:46
 */
 use \controllers\engine\Form;

 defined('SYSPATH') or die();

    Form::open("routers/process/$id");
        Form::openRow();
            Form::openCol('col-lg-12');

                Form::openPanel($lang->routers['title']);

                Form::formGroup(
                    $lang->routers['from'],
                    $lang->routers['from_tip'],
                    Form::input(array(
                        'type'        => 'text',
                        'name'        => "data[url_from]",
                        'required'    => 'required',
                        'placeholder' => $lang->routers['from_placeholder'],
                        'value'       => isset($data['url_from']) ? $data['url_from'] : ''
                    ))
                );
                Form::formGroup(
                    $lang->routers['to'],
                    $lang->routers['to_tip'],
                    Form::input(array(
                        'type'        => 'text',
                        'name'        => "data[url_to]",
                        'required'    => 'required',
                        'placeholder' => $lang->routers['to_placeholder'],
                        'value'       => isset($data['url_to']) ? $data['url_to'] : ''
                    ))
                );

                Form::formGroup(
                    $lang->routers['header'],
                    $lang->routers['header'],
                    Form::select(
                        array(
                            'name' => 'data[header]'
                        ),
                        $headers
                    )
                );
                Form::closePanel();

            Form::closeCol();
        Form::closeRow();
        Form::hidden('action',$action);
    Form::close();