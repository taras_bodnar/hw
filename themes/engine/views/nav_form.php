<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 01.06.14 10:46
 */
 use \controllers\engine\Form;

 defined('SYSPATH') or die();

    Form::open("nav/process/$id", array(), $lang);
        Form::openRow();
            Form::openCol('col-lg-6');

                Form::openPanel($lang->nav['title']);

                Form::formGroup(
                    $lang->nav['name'],
                    $lang->nav['name_tip'],
                    Form::input(array(
                        'type'        => 'text',
                        'name'        => "data[name]",
                        'required'    => 'required',
                        'placeholder' => $lang->nav['name_placeholder'],
                        'value'       => isset($data['name']) ? $data['name'] : ''
                    ))
                );

                Form::closePanel();

            Form::closeCol();

            Form::openCol('col-lg-6');
                Form::openPanel($lang->core['params']);

                Form::formGroup(
                    $lang->nav['auto_add_pages'],
                    $lang->nav['auto_add_pages_tip'],
                    Form::buttonSwitch('data[auto_add_pages]',isset($data['auto_add_pages']) ? $data['auto_add_pages'] : 0),
                    array('class'=>'text-right')
                );
                Form::formGroup(
                    $lang->nav['items'],
                    $lang->nav['items_tip'],
                    Form::pickList(
                        array(
                            'name' => 'items[]',
                            'sort_url' => 'nav/sort/',
                            'nav_id'   => $id
                        ),
                        $items
                    )
                );
                Form::closePanel();
            Form::closeCol();
        Form::closeRow();
        Form::hidden('action',$action);

    Form::close();