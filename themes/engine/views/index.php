<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 20.05.14 23:32
 */
 
if ( !defined('SYSPATH') ) die();
?>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7 lt-ie10"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8 lt-ie10"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9 lt-ie10"> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js"> <!--<![endif]-->
<head>

    <base href="<?=$base_url?>engine/" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title><?=$title?></title>
    <meta name="description" content="<?=$description?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

    <link rel="stylesheet" href="<?=$template_url?>styles/bootstrap.css">

    <!-- Page-specific Plugin CSS: -->
    <link rel="stylesheet" href="<?=$template_url?>styles/vendor/jquery.pnotify.default.css">
    <link rel="stylesheet" href="<?=$template_url?>styles/vendor/select2/select2.css">
    <link rel="stylesheet" href="<?=$template_url?>styles/vendor/animate.css">
    <link rel="stylesheet" href="<?=$template_url?>styles/vendor/datatables.css">
    <link rel="stylesheet" href="<?=$template_url?>styles/vendor/jqueryui/jqueryui.css">
    <link rel="stylesheet" href="<?=$template_url?>styles/core.css">
<!--    <link rel="stylesheet" type="text/css" href="--><?//=$template_url?><!--styles/vendor/datapicker/mdp.css">-->
    <link rel="stylesheet" type="text/css" href="<?=$template_url?>styles/vendor/datapicker/prettify.css">
    <script type="text/javascript" src="<?=$template_url?>scripts/vendor/datapicker/prettify.js"></script>
    <script type="text/javascript" src="<?=$template_url?>scripts/vendor/datapicker/lang-css.js"></script>

    <!-- adds CSS media query support to IE8   -->
    <!-- [if lt IE 9]>
    <script src="<?=$template_url?>scripts/vendor/html5shiv.js"></script>
    <script src="<?=$template_url?>scripts/vendor/respond.min.js"></script>
    <![endif]-->

    <!-- Fonts CSS: -->
    <link rel="stylesheet" href="<?=$template_url?>styles/6227bbe5.font-awesome.css" type="text/css" />
    <link rel="stylesheet" href="<?=$template_url?>styles/40ff7bd7.font-titillium.css" type="text/css" />

    <!-- Common Scripts: -->
    <script src="<?=$template_url?>scripts/vendor/jquery.min.js"></script>
    <script src="<?=$template_url?>scripts/vendor/jquery-ui.min.js"></script>
    <script src="<?=$template_url?>scripts/vendor/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?=$template_url?>scripts/vendor/jquery-ui.multidatespicker.js"></script>
    <script src="<?=$template_url?>scripts/vendor/bootstrap-tagsinput.min.js"></script>
    <script src="<?=$template_url?>scripts/vendor/modernizr.js"></script>
    <script src="<?=$template_url?>scripts/vendor/jquery.cookie.js"></script>
    <script src="<?=$template_url?>scripts/vendor/bootstrap-switch.js"></script>
    <script src="<?=$template_url?>scripts/vendor/jquery.form.js"></script>

    <script src="<?=$template_url?>scripts/vendor/select2.min.js"></script>
    <script src="<?=$template_url?>scripts/vendor/jquery.pnotify.min.js"></script>
    <script src="<?=$template_url?>scripts/vendor/jquery.jstree.js"></script>
    <script src="<?=$template_url?>scripts/vendor/parsley.min.js"></script>
    <script src="<?=$template_url?>scripts/vendor/parsley.extend.min.js"></script>

    <script src="<?=$template_url?>scripts/vendor/jquery.dataTables.min.js"></script>
    <script src="<?=$template_url?>scripts/vendor/jquery.maskedinput.min.js"></script>
    <script src="<?=$template_url?>scripts/vendor/datatables.js"></script>
    <script src="<?=$template_url?>scripts/vendor/charCount.js"></script>
    <script src="<?=$template_url?>scripts/vendor/dropzone.min.js"></script>
    <!-- jsTree -->
    <script src="<?=$template_url?>scripts/vendor/jquery.jstree.js"></script>

    <script>
        engine = {}, theme = $.cookie('OyiTheme') || 'default';

        $('body').removeClass (function (index, css) {
            return (css.match (/\btheme-\S+/g) || []).join(' ');
        });

        if (theme !== 'default') $('body').addClass(theme);
        engine.lang = <?=json_encode($lang)?>;
        engine.lang.table = <?=json_encode($lang->table)?>;

        if (!($('body').is('.dashboard-page') || $('body').is('.login-page'))){
            if ($.cookie('oyiSidebar') == 'retracted') {
                $('.wrapper').removeClass('retracted').addClass('extended');
            }
            if ($.cookie('oyiSidebar') == 'extended') {
                $('.wrapper').removeClass('extended').addClass('retracted');
            }
        }
    </script>
    <script src="<?=$template_url?>scripts/core.js"></script>
    <?php if(!empty($js_file)) foreach ($js_file as $k=>$file) : ?>
        <script src="<?=$file?>"></script>
    <?php endforeach ;?>
</head>
<body class="<?=$body_class?>">
    <?=$nav?>
    <?=$body?>
</body>
</html>