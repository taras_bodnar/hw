<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 01.06.14 10:46
 */
 use \controllers\engine\Form;

 defined('SYSPATH') or die();

    Form::open("components/process/$id");
        Form::openRow();
            Form::openCol('col-lg-8');

                Form::openPanel($lang->components['title']);
                $hide='';
                if(count($languages) > 1) {
                    $hide='hide';
                    Form::languagesSwitch(
                        $lang->core['sel_languages'],
                        $lang->core['sel_languages_tip'],
                        $languages,
                        $lang
                    );
                }
                foreach ($languages as $row) {
                    Form::html("<div class=\"language-switch $hide language-{$row['id']}\">");
                    Form::formGroup(
                        $lang->components['name'],
                        $lang->components['name_tip'],
                        Form::input(array(
                            'type'        => 'text',
                            'name'        => "info[{$row['id']}][name]",
                            'required'    => 'required',
                            'placeholder' => $lang->components['name_placeholder'],
                            'data-parsley-required' => 'true',
                            'value'       => isset($info[$row['id']]['name']) ? $info[$row['id']]['name'] : ''
                        ))
                    );
                    Form::formGroup(
                        $lang->components['desc'],
                        $lang->components['desc_tip'],
                        Form::textarea(array(
                            'name' => "info[{$row['id']}][description]",
                            'placeholder' => $lang->components['desc_placeholder'],
                            'value'       => isset($info[$row['id']]['description']) ? $info[$row['id']]['description'] : ''
                        ))
                    );
                    Form::html('</div>');// .info
                }
                Form::closePanel();

            Form::closeCol();
            Form::openCol('col-lg-4');

                Form::openPanel($lang->core['params']);
// todo зробити вибірку контроллерів
                    Form::formGroup(
                        $lang->components['controller'],
                        $lang->components['controller_tip'],
                        Form::select(
                            array(
                                'name' => 'data[controller]'
                            ),
                            $controllers
                        )
                    );
// todo зробити вибірку парентів;

                    Form::formGroup(
                        $lang->components['parent'],
                        $lang->components['parent_tip'],
                        Form::select(
                            array(
                                'name' => 'data[parent_id]'
                            ),
                            $parents
                        )
                    );

                    Form::formGroup(
                        $lang->components['menu_icon'],
                        $lang->components['menu_icon_tip'],
                        Form::input(array(
                            'type' => 'text',
                            'name' => 'data[icon]',
                            'placeholder' => $lang->components['menu_icon_placeholder'],
                            'data-parsley-required' => 'true',
                            'value' => isset($data['icon']) ? $data['icon'] : ''
                        ))
                    );
                    Form::formGroup(
                        $lang->components['rang'],
                        $lang->components['rang_tip'],
                        Form::input(array(
                            'type'        => 'text',
                            'name'        => 'data[rang]',
                            'required'    => 'required',
                            'placeholder' => $lang->components['rang_placeholder'],
                            'data-parsley-required' => 'true',
                            'data-parsley-type'     => 'number',
                            'data-parsley-min'      => 100,
                            'data-parsley-max'      => 999,
                            'value' => isset($data['rang']) ? $data['rang'] : ''
                        ))
                    );
                    Form::formGroup(
                        $lang->components['sort'],
                        $lang->components['sort_tip'],
                        Form::input(array(
                            'type'        => 'text',
                            'name'        => 'data[sort]',
                            'required'    => 'required',
                            'placeholder' => $lang->components['sort_placeholder'],
                            'data-parsley-required' => 'true',
                            'data-parsley-type'     => 'number',
                            'value' => isset($data['sort']) ? $data['sort'] : ''
                        ))
                    );
                    Form::formGroup(
                        $lang->core['published'],
                        '',
                        Form::buttonSwitch('data[published]',isset($data['published']) ? $data['published'] : 1),
                        array('class'=>'text-right')
                    );

                Form::closePanel();

            Form::closeCol();
        Form::closeRow();
        Form::hidden('action',$action);
    Form::close();