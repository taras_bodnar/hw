<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 17.11.14 16:33
 */
use \controllers\engine\Form;
defined("SYSPATH") or die();

Form::open("components/installProcess", array('id'=>'formInstall', 'enctype' => 'multipart/form-data'), $lang);
Form::openRow();
Form::openCol('col-lg-12');

Form::formGroup(
    $lang->components['parent'],
    $lang->components['parent_tip'],
    Form::select(
        array(
            'name' => 'data[parent_id]'
        ),
        $parents)
);

Form::formGroup(
    $lang->components['upload_file'],
    $lang->components['upload_file_tip'],
    Form::fileInput(
        array(
            'name'        => "file",
            'required'    => 'required'
        )
    )
);
Form::closePanel();

Form::closeCol();
Form::closeRow();
Form::customSuccessAction("
    if(d.s){
        var oTable = $('#components').dataTable();
        oTable.fnDraw(oTable.fnSettings());
        $('#tree').jstree('refresh');
        $('.modal').modal('hide');
        $('.modal-backdrop ').remove();
        engine.alert(d.t,d.m,d.e,'#notification', 'html');
        setTimeout(function(){location.reload(true)}, 1500);
    }
");
Form::close();