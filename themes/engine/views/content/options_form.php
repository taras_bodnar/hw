<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 01.06.14 10:46
 */
 use \controllers\engine\Form;

 defined('SYSPATH') or die();

    Form::open("content/options/process/$id",array(),$lang);
        Form::openRow();
            Form::openCol('col-lg-7');

                Form::openPanel($lang->options['title']);

                    $icon = strtr(Form::icon('icon-sort-by-alphabet'),array('"'=>"'"));
                    $icon_spinner = strtr(Form::icon('icon-spinner'),array('"'=>"'"));
                    $lang_id = 0; $lang_code=''; $i=0;
                    $c = count($languages);
                foreach ($languages as $row) {

                    if($row['front_default'] == 1) {
                        $lang_id = $row['id']; $lang_code = $row['code'];
                    }
                    if($c > 1) {
                        Form::html("<input type=\"hidden\" name=\"s_languages[{$row['id']}]\" value='{$row['code']}' class='s-languages'>");
                        if($i == 0) {
                            Form::formGroup(
                                '','',
                                Form::button(
                                    $lang->core['translate'],
                                    $icon,
                                    array(
                                        'class'   =>'btn-translate-all btn-info',
                                        'onclick' => 'engine.translator.translateAll('. $lang_id .', \''. $lang_code .'\', this); return false;',
                                        'data-complete-text' => $icon .' '. $lang->core['translate'],
                                        'data-loading-text' => $icon_spinner .' '. $lang->core['begin_translation']
                                    )
                                )
                            );
                        }
                        $i++;
                    }
                    Form::formGroup(
                        $lang->options['name'] . ' ['. $row['name'] .']',
                        $lang->options['name_tip'],
                        Form::input(array(
                            'type'        => 'text',
                            'name'        => "info[{$row['id']}][name]",
                            'required'    => 'required',
                            'placeholder' => $lang->options['name_placeholder'],
                            'data-parsley-required' => 'true',
                            'value'       => isset($info[$row['id']]['name']) ? $info[$row['id']]['name'] : ''
                        ))
                    );
                }

//            echo '<pre>';    print_r($type);echo '</pre>';

                Form::formGroup(
                    $lang->options['type'],
                    $lang->options['type_tip'],
                    Form::select(
                        array(
                            'name'     => "data[type]",
                            'onchange' => "engine.content.options.getTypeValues({$id}, this.value);"
                        ),
                        $type
                    )
                );

                Form::html("<div id='type_options'></div>");

                Form::closePanel();

            Form::closeCol();

            Form::openCol('col-lg-5');
                Form::openPanel($lang->core['params']);

                    Form::formGroup(
                        $lang->core['published'],
                        '',
                        Form::buttonSwitch('data[published]', isset($data['published']) ? $data['published'] : 1),
                        array('class'=>'text-right')
                    );

                    Form::formGroup(
                        $lang->options['required'],
                        '',
                        Form::buttonSwitch('data[required]', isset($data['required']) ? $data['required'] : 1),
                        array('class'=>'text-right')
                    );
                    Form::formGroup(
                        $lang->options['multilang'],
                        '',
                        Form::buttonSwitch('data[multilang]', isset($data['multilang']) ? $data['multilang'] : 1),
                        array('class'=>'text-right')
                    );

                    Form::formGroup(
                        $lang->options['show_list'],
                        '',
                        Form::buttonSwitch('data[show_list]', isset($data['show_list']) ? $data['show_list'] : 0),
                        array('class'=>'text-right')
                    );
                    Form::formGroup(
                        $lang->options['show_compare'],
                        '',
                        Form::buttonSwitch('data[show_compare]', isset($data['show_compare']) ? $data['show_compare'] : 0),
                        array('class'=>'text-right')
                    );
                    Form::formGroup(
                        $lang->options['show_filter'],
                        '',
                        Form::buttonSwitch('data[show_filter]', isset($data['show_filter']) ? $data['show_filter'] : 0),
                        array('class'=>'text-right')
                    );

                    Form::formGroup(
                        $lang->options['content_options'],
                        $lang->options['content_options_tip'],
                        Form::select(
                            array(
                                'multiple' => 'multiple',
                                'name' => 'content_options[]'
                            ),
                            $categories
                        )
                    );

                    Form::formGroup(
                        $lang->options['extends'],
                        $lang->options['extends_tip'],
                        Form::select(
                            array(
                                'multiple' => 'multiple',
                                'name' => 'content_type_options[]'
                            ),
                            $content_type
                        )
                    );
                Form::closePanel();
            Form::closeCol();

        Form::closeRow();
        Form::hidden('action',$action);
    Form::close();