<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 01.06.14 10:46
 */
use \controllers\engine\Form;

defined('SYSPATH') or die();

// показувати
$display_parent_id = true;

Form::open($form_action, array('enctype' => 'multipart/form-data'), $lang);
Form::openRow();
Form::openCol('col-lg-8');

Form::openPanel($title);
    $hide='';
    if(count($languages) > 1) {
        $hide='hide';
        Form::languagesSwitch(
            $lang->core['sel_languages'],
            $lang->core['sel_languages_tip'],
            $languages,
            $lang
        );
    }
    foreach ($languages as $row) {
        Form::html("<div class=\"language-switch $hide language-{$row['id']}\">");

        Form::formGroup(
            $lang->content['name'],
            $lang->content['name_tip'],
            Form::input(array(
                'type'        => 'text',
                'name'        => "info[{$row['id']}][name]",
                'onkeyUp'     => $autofil_title ?
                        '$(\'#info_'.$row['id'].'_title\').val(this.value).trigger(\'change\')' : '',
                'onChange'     => $autofil_title ?
                        'engine.pages.mkAlias($(\'#info_'.$row['id'].'_alias\'),'.$data['parent_id'].','. $row['id'] .',\''. $row['code'] .'\', this.value);' : '',
                //mkAlias: function(target,id,languages_id,languages_code,text)
                'required'    => 'required',
                'placeholder' => $lang->content['name_placeholder'],
                'data-parsley-required' => 'true',
                'value'       => isset($info[$row['id']]['name']) ? $info[$row['id']]['name'] : '',
                'counter'     => array(
                    'counterText' => $lang->core['counter_text'],
                    'allowed' => 255,
                    'warning' => 25
                )
            ))
        );

        $input = array(
            'type'        => 'text',
            'name'        => "info[{$row['id']}][alias]",
            'placeholder' => $lang->content['alias_placeholder'],
            'value'       => isset($info[$row['id']]['alias']) ? $info[$row['id']]['alias'] : '',
            'counter'     => array(
                'counterText' => $lang->core['counter_text'],
                'allowed' => 255,
                'warning' => 25
            )
        );
        if($id > 1) $input['required'] = 'required';
        Form::formGroup(
            $lang->content['alias'],
            $lang->content['alias_tip'],
            Form::input($input)
        );

        Form::formGroup(
            $lang->content['meta_title'],
            $lang->content['meta_title_tip'],
            Form::input(array(
                'type'        => 'text',
                'name'        => "info[{$row['id']}][title]",
                'required'    => 'required',
                'placeholder' => $lang->content['meta_title_placeholder'],
                'data-parsley-required' => 'true',
                'value'       => isset($info[$row['id']]['title']) ? $info[$row['id']]['title'] : '',
                'counter'     => array(
                    'counterText' => $lang->core['counter_text'],
                    'allowed' => 255,
                    'warning' => 25
                )
            ))
        );

        if(in_array('keywords', $form_fields )) {
            Form::formGroup(
                $lang->content['keywords'],
                $lang->content['keywords_tip'],
                Form::input(array(
                    'type'        => 'text',
                    'name'        => "info[{$row['id']}][keywords]",
                    'placeholder' => $lang->content['keywords_placeholder'],
                    'value'       => isset($info[$row['id']]['keywords']) ? $info[$row['id']]['keywords'] : '',
                    'counter'     => array(
                        'counterText' => $lang->core['counter_text'],
                        'allowed' => 255,
                        'warning' => 25
                    )
                ))
            );
        }

        if(in_array('case_title', $form_fields )) {
            Form::formGroup(
                'Відмінок',
                'Відмінок',
                Form::input(array(
                    'type'        => 'text',
                    'name'        => "info[{$row['id']}][case_title]",
                    'placeholder' => $lang->content['keywords_placeholder'],
                    'value'       => isset($info[$row['id']]['case_title']) ? $info[$row['id']]['case_title'] : '',
                    'counter'     => array(
                        'counterText' => $lang->core['counter_text'],
                        'allowed' => 255,
                        'warning' => 25
                    )
                ))
            );
        }

        if(in_array('description', $form_fields )) {
            Form::formGroup(
                $lang->content['desc'],
                $lang->content['desc_tip'],
                Form::textarea(array(
                    'name' => "info[{$row['id']}][description]",
                    'placeholder' => $lang->content['desc_placeholder'],
                    'value'       => isset($info[$row['id']]['description']) ? $info[$row['id']]['description'] : '',
                    'counter'     => array(
                        'counterText' => $lang->core['counter_text'],
                        'allowed' => 320,
                        'warning' => 25
                    )
                ))
            );
        }

        if(in_array('content', $form_fields )) {
            Form::formGroup(
                $lang->content['content'],
                $lang->content['content_tip'],
                Form::editor(array(
                    'name' => "info[{$row['id']}][content]",
                    'value'       => isset($info[$row['id']]['content']) ? $info[$row['id']]['content'] : ''
                ))
            );
        }

        Form::html('</div>');// .info
    }

Form::closePanel();


    if(isset($plugins['left'])) {
        Form::html(implode('', $plugins['left']));
    }

Form::closeCol();
Form::openCol('col-lg-4');

Form::openPanel($lang->core['options']);
Form::formGroup(
    $lang->core['published'],
    '',
    Form::buttonSwitch('data[published]',isset($data['published']) ? $data['published'] : 1),
    array('class'=>'text-right')
);

if(in_array('templates_id', $form_fields )) {
    Form::formGroup(
        $lang->content['template'],
        $lang->content['template_tip'],
        Form::select(
            array(
                'name' => 'data[templates_id]'
            ),
            $templates
        )
    );
} else {
    Form::hidden('data[templates_id]', $data['templates_id']);
}
Form::formGroup(
    'Зовнішній ключ',
    '',
    Form::input(
        array(
            'name' => 'data[external_id]',
            'value' => isset($data['external_id']) ? $data['external_id'] : ''
        )
    )
);
Form::formGroup(
    'Позиція',
    '',
    Form::input(
        array(
            'name' => 'data[position]',
            'type' => 'number',
            'value' => !empty($data['position']) ? $data['position'] : 0
        )
    )
);
if(in_array('module', $form_fields )) {
    Form::formGroup(
        $lang->content['module'],
        $lang->content['module_tip'],
        Form::input(
            array(
                'name' => 'data[module]',
                'placeholder' => $lang->content['module_placeholder'],
                'value' => isset($data['module']) ? $data['module'] : ''
            )
        )
    );
}

Form::closePanel();

if(in_array('nav_menu', $form_fields )) {
    Form::openPanel($lang->core['menu']);
    $input = '';
    if(empty($nav_menu)){
        $input .= '<div class="alert alert-dismissable alert-warning fade in">
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">
                                          <i class="icon-remove"></i>
                                        </button>
                                        <span class="title"><i class="icon-warning-sign"></i> '. $lang->core['warning'] .'</span>
                                        '. $lang->pages['nav_menu_not_created'] .'
                                    </div>';
    } else {
        foreach ($nav_menu as $item) {
            $input .= "<div class='checkbox'>
                        <label for='nav_menu_{$item['id']}'>
                            {$item['name']}
                            ". Form::checkbox('nav_menu['. $item['id'] .']', in_array($item['id'], $selected_nav_menu)) ."
                        </label>
                   </div>";
        }
    }

    Form::formGroup(
        $lang->pages['nav_menu'],
        $lang->pages['nav_menu_tip'],
        $input
    );
    Form::closePanel();
}

Form::openPanel($lang->core['params']);


if(isset($categories)){
    $display_parent_id=false;
    Form::formGroup(
        $lang->products['categories'],
        $lang->products['categories_tip'],
        Form::select(
            array(
                'name' => 'categories[]',
                'multiple' =>'multiple',
                'required' =>'required'
            ),
            $categories
        )
    );
}

Form::formGroup(
    $lang->core['date'],
    $lang->core['date_tip'],
    Form::input(
        array(
            'name' => 'data[createdon]',
            'required' =>'required',
            'value' => isset($data['createdon']) ? $data['createdon'] : ''
        )
    )
);

if($type == 'product'){

    Form::formGroup(
        $lang->products['price'],
        $lang->products['price_tip'],
        Form::input(
            array(
                'name'        => 'product_options[price]',
                'placeholder' => $lang->products['price_placeholder'],
                'value'       => isset($product_options['price']) ? $product_options['price'] : ''
            )
        )
    );

    Form::formGroup(
        $lang->products['code'],
        $lang->products['code_tip'],
        Form::input(
            array(
                'name'        => 'product_options[code]',
                'placeholder' => $lang->products['code_placeholder'],
                'value'       => isset($product_options['code']) ? $product_options['code'] : ''
            )
        )
    );

    Form::formGroup(
        $lang->products['quantity'],
        $lang->products['quantity_tip'],
        Form::input(
            array(
                'name'        => 'product_options[quantity]',
                'placeholder' => $lang->products['quantity_placeholder'],
                'value'       => isset($product_options['quantity']) ? $product_options['quantity'] : ''
            )
        )
    );

    Form::formGroup(
        $lang->products['availability'],
        '',
        Form::buttonSwitch(
            'product_options[availability]',
            isset($product_options['availability']) ? $product_options['availability'] : 1
        ),
        array('class'=>'text-right')
    );
    Form::formGroup(
        $lang->products['sale'],
        '',
        Form::buttonSwitch('product_options[sale]',isset($product_options['sale']) ? $product_options['sale'] : 0),
        array('class'=>'text-right')
    );
    Form::formGroup(
        $lang->products['hit'],
        '',
        Form::buttonSwitch('product_options[hit]',isset($product_options['hit']) ? $product_options['hit'] : 0),
        array('class'=>'text-right')
    );
    Form::formGroup(
        $lang->products['new'],
        '',
        Form::buttonSwitch('product_options[new]',isset($product_options['new']) ? $product_options['new'] : 0),
        array('class'=>'text-right')
    );
}
    // параметри сторінки по замовчуванню
    if(isset($content_type_options)){
    //        print_r($content_type_options);
        Form::html($content_type_options);
    }
// dynamic options
Form::html("<div id='content_features' data-type='{$type}' data-id='{$id}'>{$features}</div>");

Form::closePanel();

// IMAGES
    if(isset($images) && !empty($images)) {
        Form::html($images);
    }

if(isset($plugins['right'])) {
    Form::html(implode('', $plugins['right']));
}
Form::closeCol();
Form::closeRow();
    if($display_parent_id){
        Form::hidden('data[parent_id]', $parent_id);
    }
    Form::hidden('action',          $action);
    if($created){
      Form::hidden('redirect_url',    $redirect_url);
//      Form::hidden('created',          $created);
    }
    Form::hidden('content_id',      $id);
Form::close();