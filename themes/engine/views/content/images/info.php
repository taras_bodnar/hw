<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 16.07.14 15:59
 */
 
defined('SYSPATH') or die();
use \controllers\engine\Form;

Form::open('content/images/updateInfo/'. $id, array('id'=>'imagesInfo'));
Form::customSuccessAction("$('.modal').modal('hide');");
Form::openRow();
Form::openCol('col-lg-12');

    $icon = strtr(Form::icon('icon-sort-by-alphabet'),array('"'=>"'"));
    $icon_spinner = strtr(Form::icon('icon-spinner'),array('"'=>"'"));
    $lang_id = 0; $lang_code=''; $i=0;
foreach ($languages as $row) {

    if($row['front_default'] == 1) {
        $lang_id = $row['id']; $lang_code = $row['code'];
    }

    Form::html("<input type=\"hidden\" name=\"s_languages[{$row['id']}]\" value='{$row['code']}' class='s-languages'>");
    if($i == 0) {
        Form::formGroup(
            '','',
            Form::button(
                $lang->core['translate'],
                $icon,
                array(
                    'class'   =>'btn-translate-all btn-info',
                    'onclick' => 'engine.translator.translateAll('. $lang_id .', \''. $lang_code .'\', this); return false;',
                    'data-complete-text' => $icon .' '. $lang->core['translate'],
                    'data-loading-text' => $icon_spinner .' '. $lang->core['begin_translation']
                )
            )
        );
    }
    $i++;

    Form::formGroup(
        $lang->images['alt'] . ' ['. $row['name'] .']',
        $lang->images['alt_tip'],
        Form::input(array(
            'type'        => 'text',
            'name'        => "info[{$row['id']}][alt]",
            'required'    => 'required',
            'placeholder' => $lang->images['alt_tip'],
            'data-parsley-required' => 'true',
            'value'       => isset($info[$row['id']]['alt']) ? $info[$row['id']]['alt'] : ''
        ))
    );
}

Form::closeCol();
Form::closeRow();
Form::close();