<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 16.07.14 15:59
 */
 
defined('SYSPATH') or die();
?>
<div class="dz-preview dz-file-preview" <?=((isset($image)) ? 'id="'. $image['id'] .'"' : 'id={imageId}')?>>
    <div class="dz-details">
        <img data-dz-thumbnail src="<?php if(isset($image)) echo $path . $image['name']; else echo $base_url . 'images/blank.png'; ?>"/>
        <div class="overlay">
            <div class="dz-filename text-overflow-hidden"><span data-dz-name></span></div>
            <!-- <div class="dz-size" data-dz-size></div> -->
            <div class="status">
                <a class="dz-error-mark remove-item" href="javascript:;"><i class="icon-remove-sign"></i></a>
                <div class="dz-error-message"><?=$lang->images['error']?> <span data-dz-errormessage></span></div>
            </div>
            <div class="controls clearfix">
                <a
                    onclick="engine.modal.alert('<div style=\'text-align:center\'><img style=\'max-width: 480px;max-height:320px;\' src=\'<?=$path_view .((isset($image)) ? $image['name'] : '{imageName}')?>\'></div>');"
                    href="javascript:;"
                    ><i class="icon-search"></i></a>
                <a onclick="engine.content.images.crop(<?=$content_id?>,<?=((isset($image)) ?  $image['id']  : '{imageId}')?>);"  class="crop-item" href="javascript:;"><i class="icon-crop"></i></a>
                <a onclick="engine.content.images.editInfo(<?=((isset($image)) ?  $image['id']  : '{imageId}')?>);"  class="edit-item" href="javascript:;"><i class="icon-pencil"></i></a>
                <a class="trash-item" href="javascript:;"><i class="icon-trash"></i></a>
            </div>
            <div class="controls confirm-removal clearfix">
                <a onclick="engine.content.images.delete(<?=((isset($image)) ?  $image['id']  : '{imageId}')?>);" class="remove-item" href="javascript:;"><?=$lang->core['btn_yes']?></a>
                <a class="remove-cancel" href="javascript:;"><?=$lang->core['btn_no']?></a>
            </div>
        </div>
    </div>
    <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
</div>