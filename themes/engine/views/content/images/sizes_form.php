<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 01.06.14 10:46
 */
 use \controllers\engine\Form;

 defined('SYSPATH') or die();

    Form::open("content/images/sizes/process/$id", array(), $lang);
        Form::openRow();
            Form::openCol('col-lg-6');

                Form::openPanel($lang->images_sizes['title']);

                Form::formGroup(
                    $lang->images_sizes['name'],
                    $lang->images_sizes['name_tip'],
                    Form::input(array(
                        'type'        => 'text',
                        'name'        => "data[name]",
                        'required'    => 'required',
                        'placeholder' => $lang->images_sizes['name_placeholder'],
                        'value'       => isset($data['name']) ? $data['name'] : ''
                    ))
                );
                Form::formGroup(
                    $lang->images_sizes['width'],
                    $lang->images_sizes['width_tip'],
                    Form::input(array(
                        'type'        => 'text',
                        'name'        => "data[width]",
                        'required'    => 'required',
                        'placeholder' => $lang->images_sizes['width_placeholder'],
                        'value'       => isset($data['width']) ? $data['width'] : ''
                    ))
                );
                Form::formGroup(
                    $lang->images_sizes['height'],
                    $lang->images_sizes['height_tip'],
                    Form::input(array(
                        'type'        => 'text',
                        'name'        => "data[height]",
                        'required'    => 'required',
                        'placeholder' => $lang->images_sizes['height_placeholder'],
                        'value'       => isset($data['height']) ? $data['height'] : ''
                    ))
                );

                Form::closePanel();

            Form::closeCol();

            Form::openCol('col-lg-6');
                Form::openPanel($lang->core['params']);

                Form::formGroup(
                    $lang->images_sizes['crop'],
                    $lang->images_sizes['crop_tip'],
                    Form::buttonSwitch('data[crop]',isset($data['crop']) ? $data['crop'] : 0),
                    array('class'=>'text-right')
                );
                Form::formGroup(
                    $lang->images_sizes['content_type'],
                    $lang->images_sizes['content_type_tip'],
                    Form::pickList(
                        array(
                            'name' => 'content_type[]'
                        ),
                        $content_type
                    )
                );
                Form::closePanel();
            Form::closeCol();
        Form::closeRow();
        Form::hidden('action',$action);

    Form::close();