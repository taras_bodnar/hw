<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 16.07.14 15:59
 */
 
defined('SYSPATH') or die();
?>
<section class="panel panel-default panel-block gallery-uploader">
    <div class="panel-heading text-overflow-hidden">
        <span class="gallery-title">
            <?=$lang->images['g_title']?>
            <i class="icon-info-sign uses-tooltip"
               data-toggle="tooltip"
               data-placement="top"
               data-original-title="<?=$lang->images['g_title_tip']?>"
            ></i>
        </span>
        <a href="javascript:;" class="add insert">
            <i class="icon-plus-sign"></i>
            <span>
                <?=$lang->images['insert_images']?>
            </span>
        </a>
        <a href="javascript:;" class="add finished">
            <i class="icon-check-sign"></i>
            <span>
                <?=$lang->images['finish_upload']?>
            </span>
        </a>
    </div>
    <div class="list-group">
        <div class="list-group-item dropzone-container">
            <div class="form-group">
                <div class="dropzone" id="imageGalleryDropzone" data-target="content/images/upload/<?=$id?>">
                    <div class="dz-message clearfix">
                        <i class="icon-picture"></i>
                        <span><?=$lang->images['message']?></span>
                        <div class="hover">
                            <i class="icon-download"></i>
                            <span><?=$lang->images['drop_files_here']?></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="list-group-item preview-container">
            <div class="form-group">
                <div class="gallery-container"><?php if(isset($images)) echo implode('', $images); ?></div>
            </div>
        </div>
    </div>
</section>
<script>
    $(".gallery-container").sortable({
//        handle: ".dz-file-preview",
        update: function(event, ui) {
            var newOrder = $(this).sortable('toArray').toString();
            $.get('table/sort/content_images/id/sort/'+newOrder);
        }
    });
</script>