<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 16.12.14 11:15
 */
defined("SYSPATH") or die();
?>

<div class="panel panel-default panel-block" id="alias-box">
    <div class="list-group">
        <div class="list-group-item button-demo" >
            <h4 class="section-title"><?=$lang->images_sizes['auto_resize_title']?></h4>
            <p><?=$lang->images_sizes['auto_resize_desc']?></p>
            <div id="progress" class="progress progress-thin  progress-striped active">
                <div style="width: 0%;" class="progress-bar progress-bar-success" data-original-title="" title=""></div>
            </div>
        </div>
    </div>
</div>
<script>
    function resize(sizes_id, total, start)
    {
        if(start >= total) {
            engine.alert(
                engine.lang.images_sizes.auto_resize_success,
                engine.lang.images_sizes.auto_resize_success_desc,
                'success',
                '#notification',
                'html'
            );
            return false;
        }

        var percent =  100 / total, done = Math.round( start * percent ) ;
        $("#progress").find('div').css('width', done + '%');
        /*
        setTimeout(function(){
            start++;
            resize(sizes_id, total, start);
        }, 1500);
        */

        $.ajax({
            type: "POST",
            url: './content/images/sizes/cropProcess/',
            data: {
                start: start,
                sizes_id: sizes_id
            },
            success: function(d){
                if(d > 0){
                    start++;
                    resize(sizes_id, total, start);
                }
            },
            dataType: 'html'
        });

        return false;
    }
    $(function(){
        resize(<?=$sizes_id?>,<?=$total?>, 0);
    });
</script>