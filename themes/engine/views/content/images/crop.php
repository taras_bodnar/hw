<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wg.
 * Date: 16.07.14 15:59
 */
use \controllers\engine\Form;
defined('SYSPATH') or die();
$t = time();

Form::open("content/images/cropProcess/$content_id/$id",array('id'=>'imagesCrop'));
Form::customSuccessAction("$('.modal').modal('hide');");
    Form::openRow();
        Form::openCol('col-lg-12');
            Form::formGroup(
                $lang->images['size'],
                $lang->images['size_tip'],
                Form::select(
                    array(
                        'name' => 'size',
                        'onchange' => 'changeSize(this.value)'
                    ),
                    $sizes
                )
            );
        Form::closeCol();
    Form::closeRow();
Form::hidden('data[x]');
Form::hidden('data[y]');
Form::hidden('data[w]');
Form::hidden('data[h]');
Form::close();
?>

<link rel="stylesheet" href="<?=$template_url?>/scripts/vendor/jcrop/jquery.Jcrop.min.css">
<script src="<?=$template_url?>/scripts/vendor/jcrop/jquery.Jcrop.min.js"></script>
<br>
<img id="imCrop<?=$t?>" src="<?=$img?>" alt="img" style="width: 560px"/>
<script>
    function changeSize(v)
    {
        var a= v.split('x');
//        console.log(a);
        var jCropApi = $('#imCrop<?=$t?>').Jcrop({
            boxWidth: 560,
//            minSize: [a[0], a[1]],
            aspectRatio: a[0]/a[1],
            onSelect: function updateCoords(c)
            {
                $('#data_x').val(c.x);
                $('#data_y').val(c.y);
                $('#data_w').val(c.w);
                $('#data_h').val(c.h);
            }
        });

        jCropApi.focus();
        jCropApi.enable();
    }

    jQuery(function($){
        $('#size').trigger('change');
    });
</script>