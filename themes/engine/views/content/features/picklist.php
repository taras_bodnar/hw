<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 31.07.14 22:38
 */

use \controllers\engine\Form;
defined('SYSPATH') or die();
//echo '<pre>'; print_r($features); echo '</pre>'; return'';
Form::open("content/pickFeatures", array('id'=>'pickContentFeatures'), $lang);

Form::formGroup(
    $lang->features['content_features'],
    $lang->features['content_features_tip'],
    Form::pickList(
        array(
            'id'   => 'pick_content_features',
            'name' => 'content_features[]'
        ),
        $features
    )
);
Form::customSuccessAction("
    if(d.s) {
        $(d.c).insertBefore($('.button-pink-new'));
    }
");
Form::hidden('content_id', $content_id);

Form::close();