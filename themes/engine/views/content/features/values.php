<?php
/**
 * Created by PhpStorm.
 * User: wg
 * Date: 11.03.15
 * Time: 20:47
 */

foreach ($items as $item) : 

        $remove_link = $use_remove_link ?  "
         <a
            title='{$lang->features['remove_content_features']}'
            href='javascript:void(0);'
            onclick='engine.content.features.removeContentFeatures({$content_id},{$item['id']});'
            >
            <i style='color: red' class='icon-remove-circle'></i>
        </a>
        " : '';

        $btn_add_values = $enable_values ?  "
        <a
            title='{$lang->features['new_co_value']}'
            href='javascript:void(0);'
            onclick='engine.content.features.addContentFeaturesValue({$content_id},{$item['id']});'
            >
            <i style='color: blue' class='icon-plus-sign'></i>
        </a>
        " : '';
        ?>
        <div class="content-features form-group" id="co-<?=$item['id']?>">
            <?php if($item['type'] == 'text') : ?>
    <label for="features_<?=$item['id']?>">
        <?=$item['name']?>
        <?=$remove_link?>
    </label>
    <?php if($enable_values) : ?>
        <?php foreach($item['values'] as $v) : ?>
            <input placeholder="<?=$v['placeholder']?>"
                <?=$item['required']?>
                   value="<?=$v['value']?>"
                   type="text"
                   id="features_<?=$item['id']?>"
                   name="features[text][<?=$item['id']?>][<?=$v['languages_id']?>]"
                   class="form-control form-group"
                >
        <?php endforeach ;?>
    <?php endif; ?>

    <?php elseif($item['type'] == 'number') : ?>
        <label for="features_<?=$item['id']?>">
            <?=$item['name']?>
            <?=$remove_link?>
        </label>
        <?php if($enable_values) : ?>
            <input
                <?=$item['required']?>
                   value="<?=$item['values']?>"
                   type="text"
                   id="features_<?=$item['id']?>"
                   name="features[number][<?=$item['id']?>]"
                   class="form-control form-group"
                >
        <?php endif; ?>

<?php elseif($item['type'] == 'radiogroup') : ?>
    <label for="features_<?=$item['id']?>">
        <?=$item['name']?>
        <?=$remove_link?>
        <?=$btn_add_values?>
    </label>
    <?php if($enable_values) : ?>
        <?php if(!empty($item['values'])) foreach ($item['values'] as $v) : ?>
            <div class="radio">
                <label for="features_<?=$v['id']?>"><?=$v['value']?>
                    <input type="radio"
                           id="features_<?=$v['id']?>"
                        <?=$v['checked']?>
                           name="features[radiogroup][<?=$item['id']?>]"
                           value="<?=$v['id']?>"
                        >
                </label>
            </div>
        <?php endforeach ; ?>
    <?php endif; ?>

<?php elseif($item['type'] == 'checkboxgroup') : ?>
    <label for="features_<?=$item['id']?>">
        <?=$item['name']?>
        <?=$remove_link?>
        <?=$btn_add_values?>
    </label>

    <?php if($enable_values) : ?>
        <?php if(!empty($item['values'])) foreach ($item['values'] as $v) : ?>
            <div class="checkbox">
                <label for="features_<?=$item['id']?>_<?=$v['id']?>"><?=$v['value']?>
                    <input
                        type="hidden"
                        name="features[checkboxgroup][<?=$item['id']?>][<?=$v['id']?>]"
                        value="0"
                        >
                    <input
                        type="checkbox"
                        id="features_<?=$item['id']?>_<?=$v['id']?>"
                        <?=$v['checked']?>
                        name="features[checkboxgroup][<?=$item['id']?>][<?=$v['id']?>]"
                        value="1"
                        >
                </label>
            </div>
        <?php endforeach ; ?>
    <?php endif; ?>

<?php elseif($item['type'] == 'select') : ?>
    <label for="features_<?=$item['id']?>">
        <?=$item['name']?>
        <?=$remove_link?>
        <?=$btn_add_values?>
    </label>
    <?php if($enable_values) : ?>
        <br>
        <select  class="features-sel" name="features[select][<?=$item['id']?>]" id="features_<?=$item['id']?>" style="width: 100%" >
            <?php if(!empty($item['values'])) foreach ($item['values'] as $v) : ?>
                <option <?=($v['checked'] == 'checked') ? 'selected' : ''?>  value="<?=$v['id']?>"><?=$v['value']?></option>
            <?php endforeach ; ?>
        </select>
    <?php endif; ?>

<?php elseif( $item['type'] == 'textarea' ) : ?>
    <label for="features_<?=$item['id']?>">
        <?=$item['name']?>
        <?=$remove_link?>
    </label>
    <?php if($enable_values) : ?>
        <?php foreach($item['values'] as $v) : ?>
            <textarea
                placeholder="<?=$v['placeholder']?>"
                id="features_<?=$item['id']?>"
                name="features[text][<?=$item['id']?>][<?=$v['languages_id']?>]"
                <?=$item['required']?>
                class="form-control form-group"
                style="height: 300px"
                ><?=$v['value']?></textarea>
        <?php endforeach ;?>
    <?php endif; ?>

<?php elseif($item['type'] == 'checkbox') : ?>
    <div class="checkbox">
        <label for="features_<?=$item['id']?>"><?=$item['name']?>
            <?=$remove_link?>
            <?php if($enable_values) : ?>
                <input type="hidden" name="features[checkbox][<?=$item['id']?>]" value="0" >
                <input type="checkbox" <?=$item['required']?> name="features[checkbox][<?=$item['id']?>]" value="1" <?=$item['checked']?>>
            <?php endif; ?>
        </label>
    </div>

<?php elseif($item['type'] == 'sm') : ?>
    <label for="features_<?=$item['id']?>">
        <?=$item['name']?>
        <?=$remove_link?>
        <?=$btn_add_values?>
    </label>
    <?php if($enable_values) : ?>
        <br>
        <select class="features-sel" multiple name="features[sm][<?=$item['id']?>][]" id="features_<?=$item['id']?>" style="width: 100%" >
            <?php if(!empty($item['values'])) foreach ($item['values'] as $v) : ?>
                <option <?=($v['checked'] == 'checked') ? 'selected' : ''?> value="<?=$v['id']?>"><?=$v['value']?></option>
            <?php endforeach ; ?>
        </select>
    <?php endif; ?>

<?php endif; ?>
        </div>
<?php endforeach; ?>
<?php if(isset($enable_button) && $enable_button == 1) : ?>
    <!-- ADD CUSTOM OPTIONS -->
    <div class="form-group text-right button-pink-new">
        <button
            onclick="engine.content.features.addModal(<?=$content_id?>);"
            class="btn btn-primary"
            type="button"
            >
            <?=$lang->features['add_popup']; ?>
        </button>
    </div>
    <!-- / ADD CUSTOM OPTIONS -->
<?php endif; ?>
<script>
    $(document).ready(function(){
        try{
            $('.features-sel').select2();
        } catch (r){}
    });
</script>