<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 01.06.14 10:46
 */
 use \controllers\engine\Form;

 defined('SYSPATH') or die();

    Form::open("content/chunks/process/$id");

        Form::openRow();
            Form::openCol('col-lg-8');

                Form::openPanel($lang->chunks['title']);

                Form::formGroup(
                    $lang->chunks['name'],
                    $lang->chunks['name_tip'],
                    Form::input(array(
                        'type'        => 'text',
                        'name'        => "data[name]",
                        'required'    => 'required',
                        'placeholder' => $lang->chunks['name_placeholder'],
                        'data-parsley-required' => 'true',
                        'value'       => isset($data['name']) ? $data['name'] : ''
                    ))
                );

                Form::closePanel();

            Form::closeCol();
            Form::openCol('col-lg-4');
                Form::openPanel($lang->core['params']);

                Form::formGroup(
                    $lang->chunks['path'],
                    $lang->chunks['path_tip'],
                    Form::input(
                        array(
                            'type'        => 'text',
                            'name'        => "data[path]",
                            'required'    => 'required',
                            'placeholder' => $lang->chunks['path_placeholder'],
                            'data-parsley-type' => 'alphanum',
                            'data-parsley-required' => 'true',
                            'value'       => isset($data['path']) ? $data['path'] : ''
                           )
                    )
                );

                Form::closePanel();
            Form::closeCol();

        Form::closeRow();
        Form::openRow();
            Form::openCol('col-lg-12');

                Form::formGroup(
                    $lang->chunks['template'],
                    $lang->chunks['template_tip'],
                    Form::textarea(array(
                        'name'        => "template",
                        'placeholder' => $lang->chunks['template_placeholder'],
//                        'style' => 'height:600px',
                        'value'       => isset($template) ? $template : ''
                    ))
                );
            Form::closeCol();
        Form::closeRow();
        Form::hidden('action',$action);

        // codemirror
        Form::html("
        <link rel=\"stylesheet\" href=\"{$template_url}scripts/vendor/codemirror/lib/codemirror.css\">
        <script src=\"{$template_url}scripts/vendor/codemirror/lib/codemirror.js\"></script>
        <script src=\"{$template_url}scripts/vendor/codemirror/mode/css.js\"></script>
        <script src=\"{$template_url}scripts/vendor/codemirror/mode/php.js\"></script>
        <script src=\"{$template_url}scripts/vendor/codemirror/mode/htmlmixed.js\"></script>
        <script src=\"{$template_url}scripts/vendor/codemirror/mode/sql.js\"></script>
        <script src=\"{$template_url}scripts/vendor/codemirror/mode/javascript.js\"></script>
        <script>
       var cm = CodeMirror.fromTextArea(document.getElementById('template'), {
                        theme: 'neo',
//                        mode: 'htmlmixed',
                        styleActiveLine: true,
                        lineNumbers: true,
                        lineWrapping: true,
                        autoCloseTags: true,
                        matchBrackets: true
                      });

            cm.on(\"change\", function() {
            cm.save();
            var c = cm.getValue();
            $(\"textarea#template\").html(c);
          });
        </script>
        ");

    Form::close();