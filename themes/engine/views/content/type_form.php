<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 01.06.14 10:46
 */
 use \controllers\engine\Form;

 defined('SYSPATH') or die();

    Form::open("content/type/process/$id", array(), $lang);
        Form::openRow();
            Form::openCol('col-lg-6');

                Form::openPanel($lang->content_type['title']);
                Form::formGroup(
                    $lang->content_type['type'],
                    $lang->content_type['type_tip'],
                    Form::input(array(
                        'type'        => 'text',
                        'name'        => "data[type]",
                        'required'    => 'required',
                        'placeholder' => $lang->content_type['type_placeholder'],
                        'data-parsley-type' => 'alphanum',
                        'data-parsley-required' => 'true',
                        'value'       => isset($data['type']) ? $data['type'] : ''
                    ))
                );

                Form::formGroup(
                    $lang->content_type['name'],
                    $lang->content_type['name_tip'],
                    Form::input(array(
                        'type'        => 'text',
                        'name'        => "data[name]",
                        'required'    => 'required',
                        'placeholder' => $lang->content_type['name_placeholder'],
                        'data-parsley-required' => 'true',
                        'value'       => isset($data['name']) ? $data['name'] : ''
                    ))
                );

                Form::closePanel();

            Form::closeCol();

            Form::openCol('col-lg-6');
                Form::openPanel($lang->core['params']);

                Form::formGroup(
                    $lang->content_type['images_sizes'],
                    $lang->content_type['images_sizes_tip'],
                    Form::pickList(
                        array(
                            'name' => 'images_sizes[]'
                        ),
                        $images_sizes
                    )
                );
                Form::closePanel();
            Form::closeCol();
        Form::closeRow();
        Form::hidden('action',$action);
    Form::close();