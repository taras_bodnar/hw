<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 01.06.14 10:46
 */
 use \controllers\engine\Form;

 defined('SYSPATH') or die();

    Form::open("content/templates/process/$id");

        Form::openRow();
            Form::openCol('col-lg-8');

                Form::openPanel($lang->content_templates['title']);

                Form::formGroup(
                    $lang->content_templates['name'],
                    $lang->content_templates['name_tip'],
                    Form::input(array(
                        'type'        => 'text',
                        'name'        => "data[name]",
                        'required'    => 'required',
                        'placeholder' => $lang->content_templates['name_placeholder'],
                        'data-parsley-required' => 'true',
                        'value'       => isset($data['name']) ? $data['name'] : ''
                    ))
                );

                Form::formGroup(
                    $lang->content_templates['desc'],
                    $lang->content_templates['desc_tip'],
                    Form::textarea(array(
                        'name'        => "data[description]",
                        'required'    => 'required',
                        'placeholder' => $lang->content_templates['desc_placeholder'],
                        'data-parsley-required' => 'true',
                        'style'       =>'height:79px',
                        'value'       => isset($data['description']) ? $data['description'] : ''
                    ))
                );
                Form::closePanel();

            Form::closeCol();
            Form::openCol('col-lg-4');
                Form::openPanel($lang->core['params']);

                Form::formGroup(
                    $lang->content_type['type'],
                    $lang->content_type['type_tip'],
                    Form::select(
                        array(
                            'name' => 'data[type_id]'
                        ),
                        $type
                    )
                );

                Form::formGroup(
                    $lang->content_templates['path'],
                    $lang->content_templates['path_tip'],
                    Form::input(array(
                        'type'        => 'text',
                        'name'        => "data[path]",
                        'required'    => 'required',
                        'placeholder' => $lang->content_templates['path_placeholder'],
                        'data-parsley-type' => 'alphanum',
                        'data-parsley-required' => 'true',
                        'value'       => isset($data['path']) ? $data['path'] : ''
                    ))
                );

                Form::formGroup(
                    $lang->content_templates['main'],
                    '',
                    Form::buttonSwitch('data[main]',isset($data['main']) ? $data['main'] : 0),
                    array('class'=>'text-right')
                );

                Form::formGroup(
                    'Наслідувати',
                    'Дочірні сторінки будуть наслідувати шаблон',
                    Form::buttonSwitch('data[follow]',isset($data['follow']) ? $data['follow'] : 0),
                    array('class'=>'text-right')
                );
                Form::closePanel();
            Form::closeCol();

        Form::closeRow();
        Form::openRow();
            Form::openCol('col-lg-12');

                Form::formGroup(
                    $lang->content_templates['template'],
                    $lang->content_templates['template_tip'],
                    Form::textarea(array(
                        'name'        => "template",
                        'placeholder' => $lang->content_templates['template_placeholder'],
                        'style'       => 'height:600px',
                        'value'       => isset($template) ? $template : ''
                    ))
                );
            Form::closeCol();
        Form::closeRow();
        Form::hidden('action',$action);

        // codemirror
        Form::html("
        <link rel=\"stylesheet\" href=\"{$template_url}scripts/vendor/codemirror/lib/codemirror.css\">
        <script src=\"{$template_url}scripts/vendor/codemirror/lib/codemirror.js\"></script>
        <script src=\"{$template_url}scripts/vendor/codemirror/mode/css.js\"></script>
        <script src=\"{$template_url}scripts/vendor/codemirror/mode/php.js\"></script>
        <script src=\"{$template_url}scripts/vendor/codemirror/mode/htmlmixed.js\"></script>
        <script src=\"{$template_url}scripts/vendor/codemirror/mode/sql.js\"></script>
        <script src=\"{$template_url}scripts/vendor/codemirror/mode/javascript.js\"></script>
        <script>
       var cm = CodeMirror.fromTextArea(document.getElementById('template'), {
                        theme: 'neo',
//                        mode: 'htmlmixed',
                        styleActiveLine: true,
                        lineNumbers: true,
                        lineWrapping: true,
                        autoCloseTags: true,
                        matchBrackets: true
                      });

            cm.on(\"change\", function() {
            cm.save();
            var c = cm.getValue();
            $(\"textarea#template\").html(c);
          });
        </script>
        ");

    Form::close();