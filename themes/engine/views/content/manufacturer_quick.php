<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 01.06.14 10:46
 */
use \controllers\engine\Form;

defined('SYSPATH') or die();

Form::open('products/createManufacturersProcess/', array('id' => 'createManufacturerForm'));
$hide='';
if(count($languages) > 1) {
    $hide='hide';
    Form::languagesSwitch(
        $lang->core['sel_languages'],
        $lang->core['sel_languages_tip'],
        $languages,
        $lang
    );
}
foreach ($languages as $row) {
    Form::html("<div class=\"language-switch $hide language-{$row['id']}\">");
    Form::formGroup(
        $lang->content['name'],
        $lang->content['name_tip'],
        Form::input(array(
            'type'        => 'text',
            'name'        => "info[{$row['id']}][name]",
            'id'        => "info_{$row['id']}_name",
            'onChange'     => $autotranslit_alias ?
                    'engine.pages.mkAlias($(\'#createManufacturerForm #info_'.$row['id'].'_alias\'),0,'. $row['id'] .',\''. $row['code'] .'\', this.value);' : '',
            'required'    => 'required',
            'placeholder' => $lang->content['name_placeholder'],
        ))
    );

    Form::formGroup(
        $lang->content['alias'],
        $lang->content['alias_tip'],
        Form::input(array(
            'type'        => 'text',
            'name'        => "info[{$row['id']}][alias]",
            'id'          => "info_{$row['id']}_alias",
            'required'    => 'required',
            'placeholder' => $lang->content['alias_placeholder'],
        ))
    );
    Form::html('</div>');// .info
}
Form::hidden('data[published]', 1);
Form::customSuccessAction("
    $('#product_options_manufacturers_id').select2('destroy').append(d.v).select2();
    $('.modal').modal('hide');
");
Form::close();
