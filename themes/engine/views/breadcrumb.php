<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 01.06.14 10:32
 */
 
defined('SYSPATH') or die();
//asort($items);
//echo '<pre>'; print_r($items); echo '</pre>';
?>
<ol class="breadcrumb breadcrumb-nav">
    <li><a href="."><i class="icon-home"></i></a></li>
    <?php foreach($items as $li) :  ?>
        <li class="active">
            <a class="bread-page-title"
                <?php if(!empty($li['children'])) echo ' href="', $li['controller'], '/index" data-toggle="dropdown" '; ?>
                ><?=$li['name']?></a>
           <?php if(!empty($li['children'])) : ?>
            <ul class="dropdown-menu dropdown-menu-arrow" role="menu">
                <?php foreach($li['children'] as $c) : ?>
                    <li><a href="<?=$c['controller']?>/index"><?=$c['name']?></a> </li>
                <?php endforeach;  ?>
            </ul>
           <?php endif; ?>
        </li>
    <?php endforeach ; ?>
</ol>