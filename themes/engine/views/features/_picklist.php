<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 31.07.14 22:38
 */

use \controllers\engine\Form;
defined('SYSPATH') or die();
//echo '<pre>'; print_r($options); echo '</pre>';
Form::open("content/options/pickContent/$content_id",array('id'=>'pickOptions'),$lang);

Form::formGroup(
    $lang->options['content_options'],
    $lang->options['content_options_tip'],
    Form::pickList(
        array(
            'name' => 'content_options[]'
        ),
        $options
    )
);
Form::customSuccessAction("
    if(d.s) {
        $(d.c).insertBefore($('.button-pink-new'));
    }
");

foreach ($cid as $k=>$v) {
    Form::hidden('cid[]', $v);
}

Form::close();