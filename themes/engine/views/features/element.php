<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 05.08.14 19:01
 */
 
defined('SYSPATH') or die();
?>
<?php if($type == 'radiogroup') : ?>
    <div class="radio">
        <label for="features_<?=$values_id?>">
            <?=$name?>
            <input checked type="radio" value="<?=$values_id?>" name="features[radiogroup][<?=$features_id?>]" id="features_<?=$values_id?>">
        </label>
    </div>
<?php elseif($type == 'checkboxgroup') : ?>
    <div class="checkbox">
        <label for="features_<?=$features_id?>_<?=$values_id?>"><?=$name?>
            <input type="hidden" value="0" name="features[checkboxgroup][<?=$features_id?>][<?=$values_id?>]">
            <input checked type="checkbox" value="1" name="features[checkboxgroup][<?=$features_id?>][<?=$values_id?>]" id="features_<?=$features_id?>_<?=$values_id?>">
        </label>
    </div>
<?php elseif(in_array($type, array('select', 'sm'))) : ?>
    <option selected value="<?=$values_id?>"><?=$name?></option>
<?php endif; ?>