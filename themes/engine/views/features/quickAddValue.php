<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 04.08.14 22:39
 */
 
defined('SYSPATH') or die();

use \controllers\engine\Form;

Form::open('features/quickAddValueProcess', array('id' => 'quickAddValueForm'));

    $icon = strtr(Form::icon('icon-sort-by-alphabet'),array('"'=>"'"));
    $icon_spinner = strtr(Form::icon('icon-spinner'),array('"'=>"'"));
    $lang_id = 0; $lang_code=''; $i=0;

    foreach ($languages as $row) {

        if($row['front_default'] == 1) {
            $lang_id = $row['id']; $lang_code = $row['code'];
        }

        Form::html("<input type=\"hidden\" name=\"s_languages[{$row['id']}]\" value='{$row['code']}' class='s-languages'>");
        if($i == 0) {
            Form::formGroup(
                '','',
                Form::button(
                    $lang->core['translate'],
                    $icon,
                    array(
                        'class'   =>'btn-translate-all btn-info',
                        'onclick' => 'engine.translator.translateNewValue('. $lang_id .', \''. $lang_code .'\', this); return false;',
                        'data-complete-text' => $icon .' '. $lang->core['translate'],
                        'data-loading-text' => $icon_spinner .' '. $lang->core['begin_translation']
                    )
                )
            );
        }
        $i++;

        Form::formGroup(
            $lang->features['value'] . ' ['. $row['name'] .']',
            $lang->features['value_tip'],
            Form::input(array(
                'type'        => 'text',
                'name'        => "value[{$row['id']}]",
                'required'    => 'required',
                'placeholder' => $lang->features['name_placeholder'],
                'data-parsley-required' => 'true'
            ))
        );
    }

    Form::hidden('content_id', $content_id);
    Form::hidden('features_id', $features_id);
    Form::hidden('type', $data['type']);
    Form::customSuccessAction("
        console.log(d);
        if(d.type == 'select' || d.type == 'sm' ){
            $('#features_'+d.features_id).select2('destroy').append(d.v).select2();
        } else {
            $('#co-' + d.features_id).append(d.v);
        }
        $('.modal').modal('hide');
    ");
Form::close();