<?php
/**
 * Created by PhpStorm.
 * User: wg
 * Date: 11.03.15
 * Time: 15:06
 */
defined('SYSPATH') or die();
?>
<div class=" form-group">
    <label>Варіанти</label>
    <div class="row">
        <div class="col-xs-3">
            <a class="btn btn-primary" href="javascript:;" onclick="engine.content.features.createValue(<?=$features_id?>)">
                <i class="icon-plus"></i>
                <?=$lang->core['create']?>
            </a>
        </div>
        <div id="typeValuesCnt" class="col-xs-9 ui-sortable">
            <?=$values?>
        </div>
    </div>
</div>