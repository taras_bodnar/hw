<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 01.06.14 10:46
 */
 use \controllers\engine\Form;

 defined('SYSPATH') or die();

    Form::open("features/process/$id",array(), $lang);
        Form::openRow();
            Form::openCol('col-lg-7');

                Form::openPanel($lang->features['title']);

                    $icon = strtr(Form::icon('icon-sort-by-alphabet'),array('"'=>"'"));
                    $icon_spinner = strtr(Form::icon('icon-spinner'),array('"'=>"'"));
                    $lang_id = 0; $lang_code=''; $i=0;
                    $c = count($languages);
                foreach ($languages as $row) {

                    if($row['front_default'] == 1) {
                        $lang_id = $row['id']; $lang_code = $row['code'];
                    }
                    if($c > 1) {
                        Form::html("<input type=\"hidden\" name=\"s_languages[{$row['id']}]\" value='{$row['code']}' class='s-languages'>");
                        if($i == 0) {
                            Form::formGroup(
                                '','',
                                Form::button(
                                    $lang->core['translate'],
                                    $icon,
                                    array(
                                        'class'   =>'btn-translate-all btn-info',
                                        'onclick' => 'engine.translator.translateAll('. $lang_id .', \''. $lang_code .'\', this); return false;',
                                        'data-complete-text' => $icon .' '. $lang->core['translate'],
                                        'data-loading-text' => $icon_spinner .' '. $lang->core['begin_translation']
                                    )
                                )
                            );
                        }
                        $i++;
                    }
                    Form::formGroup(
                        $lang->features['name'] . ' ['. $row['name'] .']',
                        $lang->features['name_tip'],
                        Form::input(array(
                            'type'        => 'text',
                            'name'        => "info[{$row['id']}][name]",
                            'required'    => 'required',
                            'placeholder' => $lang->features['name_placeholder'],
                            'value'       => isset($info[$row['id']]['name']) ? $info[$row['id']]['name'] : ''
                        ))
                    );
                }

                Form::formGroup(
                    $lang->features['type'],
                    $lang->features['type_tip'],
                    Form::select(
                        array(
                            'name'     => "data[type]",
//                            'onchange' => "engine.content.features.getTypeValues({$id}, this.value);"
                        ),
                        $type
                    )
                );
                Form::formGroup(
                    $lang->features['external_id'],
                    $lang->features['external_id_tip'],
                    Form::input(
                        array(
                            'name'     => "data[external_id]",
                            'required' => 'required',
                            'type'     => 'text',
                            'placeholder' => $lang->features['external_id_placeholder'],
                            'value'    => isset($data['external_id']) ? $data['external_id'] : ''
                        ),
                        $type
                    )
                );

                Form::html("<div id='features_values'>{$features_values}</div>");

                Form::closePanel();

            Form::closeCol();

            Form::openCol('col-lg-5');
                Form::openPanel($lang->core['params']);

                    Form::formGroup(
                        $lang->core['published'],
                        '',
                        Form::buttonSwitch('data[published]', isset($data['published']) ? $data['published'] : 1),
                        array('class'=>'text-right')
                    );

                    Form::formGroup(
                        $lang->features['required'],
                        '',
                        Form::buttonSwitch('data[required]', isset($data['required']) ? $data['required'] : 1),
                        array('class'=>'text-right')
                    );

                    Form::formGroup(
                        $lang->features['show_list'],
                        '',
                        Form::buttonSwitch('data[show_list]', isset($data['show_list']) ? $data['show_list'] : 0),
                        array('class'=>'text-right')
                    );
                    Form::formGroup(
                        $lang->features['show_compare'],
                        '',
                        Form::buttonSwitch('data[show_compare]', isset($data['show_compare']) ? $data['show_compare'] : 0),
                        array('class'=>'text-right')
                    );
                    Form::formGroup(
                        $lang->features['show_filter'],
                        '',
                        Form::buttonSwitch('data[show_filter]', isset($data['show_filter']) ? $data['show_filter'] : 0),
                        array('class'=>'text-right')
                    );

                    Form::formGroup(
                        $lang->features['content_features'],
                        $lang->features['content_features_tip'],
                        Form::select(
                            array(
                                'multiple' => 'multiple',
                                'name' => 'content_features[]'
                            ),
                            $categories
                        )
                    );

                    Form::formGroup(
                        $lang->features['extends'],
                        $lang->features['extends_tip'],
                        Form::select(
                            array(
                                'multiple' => 'multiple',
                                'name' => 'content_type_features[]'
                            ),
                            $content_type
                        )
                    );
                Form::closePanel();
            Form::closeCol();

        Form::closeRow();
        Form::hidden('action',$action);
        Form::hidden('id', $id);
    Form::close();