<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 29.07.14 11:47
 */
 
defined('SYSPATH') or die();

if(!isset($options)) return '';

    $enable_values = isset($enable_values) && $enable_values == 1 ? 1 : 0;
    $use_remove_link = isset($use_remove_link) && $use_remove_link == 0 ? 0 : 1;

    foreach($options as $o) :

        $remove_link = $use_remove_link ?  "
         <a
            title='{$lang->options['remove_content_options']}'
            href='javascript:void(0);'
            onclick='engine.content.options.removeContentOption({$content_id},{$o['id']});'
            >
            <i style='color: red' class='icon-remove-circle'></i>
        </a>
        " : '';

        $btn_add_values = $enable_values ?  "
        <a
            title='{$lang->options['new_co_value']}'
            href='javascript:void(0);'
            onclick='engine.content.options.addContentOptionValue({$content_id},{$o['id']});'
            >
            <i style='color: blue' class='icon-plus-sign'></i>
        </a>
        " : '';
        ?>
        <div class="content-options form-group" id="co-<?=$content_id?>x<?=$o['id']?>">
            <?php if($o['type'] == 'text') : ?>
                <label for="options_<?=$o['id']?>">
                    <?=$o['name']?>
                    <?=$remove_link?>
                </label>
                <?php if($enable_values) : ?>
                    <?php if($o['multilang']) : ?>
                        <input <?=$o['required']?>
                            value="<?=$o['value']?>"
                            type="text"
                            id="options_<?=$o['id']?>"
                            name="options[text][<?=$o['id']?>][0]"
                            class="form-control"
                        >
                    <?php else : ?>
                       <?php foreach($o['value'] as $v) : ?>
                            <input placeholder="<?=$v['placeholder']?>"
                                <?=$o['required']?>
                                   value="<?=$v['value']?>"
                                   type="text"
                                   id="options_<?=$o['id']?>"
                                   name="options[text][<?=$o['id']?>][<?=$v['languages_id']?>]"
                                   class="form-control form-group"
                                >
                        <?php endforeach ;?>
                    <?php endif; ?>
                <?php endif; ?>

            <?php elseif($o['type'] == 'radiogroup') : ?>
                <label for="options_<?=$o['id']?>">
                    <?=$o['name']?>
                    <?=$remove_link?>
                    <?=$btn_add_values?>
                </label>
                <?php if($enable_values) : ?>
                    <?php if(!empty($o['values'])) foreach ($o['values'] as $v) : ?>
                        <div class="radio">
                            <label for="options_<?=$v['id']?>"><?=$v['value']?>
                                <input type="radio"
                                       id="options_<?=$v['id']?>"
                                       <?=$v['checked']?>
                                       name="options[radiogroup][<?=$o['id']?>]"
                                       value="<?=$v['id']?>"
                                >
                            </label>
                        </div>
                    <?php endforeach ; ?>
                <?php endif; ?>

            <?php elseif($o['type'] == 'checkboxgroup') : ?>
                <label for="options_<?=$o['id']?>">
                    <?=$o['name']?>
                    <?=$remove_link?>
                    <?=$btn_add_values?>
                </label>

                <?php if($enable_values) : ?>
                    <?php if(!empty($o['values'])) foreach ($o['values'] as $v) : ?>
                        <div class="checkbox">
                            <label for="options_<?=$o['id']?>_<?=$v['id']?>"><?=$v['value']?>
                                <input
                                    type="hidden"
                                    name="options[checkboxgroup][<?=$o['id']?>][<?=$v['id']?>]"
                                    value="0"
                                >
                                <input
                                    type="checkbox"
                                    id="options_<?=$o['id']?>_<?=$v['id']?>"
                                    <?=$v['checked']?>
                                    name="options[checkboxgroup][<?=$o['id']?>][<?=$v['id']?>]"
                                    value="1"
                                >
                            </label>
                        </div>
                    <?php endforeach ; ?>
                <?php endif; ?>

            <?php elseif($o['type'] == 'select') : ?>
                <label for="options_<?=$o['id']?>">
                    <?=$o['name']?>
                    <?=$remove_link?>
                    <?=$btn_add_values?>
                </label>
            <?php if($enable_values) : ?>
                <br>
                <select name="options[select][<?=$o['id']?>]" id="options_<?=$o['id']?>" style="width: 100%" >
                <?php if(!empty($o['values'])) foreach ($o['values'] as $v) : ?>
                        <option <?=$v['selected']?> value="<?=$v['id']?>"><?=$v['value']?></option>
                <?php endforeach ; ?>
                </select>
            <?php endif; ?>

            <?php elseif($o['type'] == 'editor' || $o['type'] == 'textarea') : ?>
                <label for="options_<?=$o['id']?>">
                    <?=$o['name']?>
                    <?=$remove_link?>
                </label>
                <?php if($enable_values) : ?>
                    <?php if($o['multilang']) : ?>
                        <textarea
                            id="options_<?=$o['id']?>"
                            name="options[text][<?=$o['id']?>][0]"
                            <?=$o['required']?>
                            class="form-control"
                            style="height: 300px"
                            ><?=$o['value']?></textarea>
                    <?php else : ?>
                        <?php foreach($o['value'] as $v) : ?>
                            <textarea
                                placeholder="<?=$v['placeholder']?>"
                                id="options_<?=$o['id']?>"
                                name="options[text][<?=$o['id']?>][<?=$v['languages_id']?>]"
                                <?=$o['required']?>
                                class="form-control form-group"
                                style="height: 300px"
                            ><?=$v['value']?></textarea>
                        <?php endforeach ;?>
                    <?php endif; ?>
                <?php endif; ?>

            <?php elseif($o['type'] == 'checkbox') : ?>
                <div class="checkbox">
                    <label for="options_<?=$o['id']?>"><?=$o['name']?>
                        <?=$remove_link?>
                        <?php if($enable_values) : ?>
                            <input type="hidden" name="options[checkbox][<?=$o['id']?>]" value="0" >
                            <input type="checkbox" <?=$o['required']?> name="options[checkbox][<?=$o['id']?>]" value="1" <?=$o['checked']?>>
                        <?php endif; ?>
                    </label>
                </div>

            <?php elseif($o['type'] == 'sone') : ?>
                <label for="options_<?=$o['id']?>">
                    <?=$o['name']?>
                    <?=$remove_link?>
                    <?=$btn_add_values?>
                </label>
                <?php if($enable_values) : ?>
                    <br>
                    <select name="options[select][<?=$o['id']?>]" id="options_<?=$o['id']?>" style="width: 100%" >
                        <?php if(!empty($o['values'])) foreach ($o['values'] as $v) : ?>
                            <option <?=$v['selected']?> value="<?=$v['id']?>"><?=$v['value']?></option>
                        <?php endforeach ; ?>
                    </select>
                <?php endif; ?>

            <?php elseif($o['type'] == 'smultiple') : ?>
                <label for="options_<?=$o['id']?>">
                    <?=$o['name']?>
                    <?=$remove_link?>
                    <?=$btn_add_values?>
                </label>
                <?php if($enable_values) : ?>
                    <br>
                    <select multiple name="options[smultiple][<?=$o['id']?>][]" id="options_<?=$o['id']?>" style="width: 100%" >
                        <?php if(!empty($o['values'])) foreach ($o['values'] as $v) : ?>
                            <option <?=$v['selected']?> value="<?=$v['id']?>"><?=$v['value']?></option>
                        <?php endforeach ; ?>
                    </select>
                <?php endif; ?>

            <?php endif; ?>
        </div>
<?php endforeach ; ?>
<?php if(isset($enable_button) && $enable_button == 1) : ?>
<!-- ADD CUSTOM OPTIONS -->
    <div class="form-group text-right button-pink-new">
        <button
            onclick="engine.content.options.addModal(<?=$content_id?>);"
            class="btn btn-primary"
            type="button"
            >
            <?php
                if($content_type == 'category') echo $lang->options['add_popup_cat'];
                else echo $lang->options['add_popup'];
            ?>
        </button>
    </div>
<!-- / ADD CUSTOM OPTIONS -->
<?php endif; ?>
<script>
    $(function(){
        $('.content-options select').select2();
    });
</script>