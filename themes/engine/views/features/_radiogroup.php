<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 27.07.14 14:57
 */
 
defined('SYSPATH') or die();
?>
<div class=" form-group">
    <label><?=$lang->options['variables']?></label>
    <div class="row">
        <div class="col-xs-3">
            <a
                onclick="engine.content.options.addTypeValue(<?=$options_id?>)"
                href="javascript:;"
                class="btn btn-primary"
                >
                <i class="icon-plus"></i>
                <?=$lang->core['btn_add']?>
            </a>
        </div>
        <div class="col-xs-9" id="typeValuesCnt"><?=$values?></div>
    </div>
</div>