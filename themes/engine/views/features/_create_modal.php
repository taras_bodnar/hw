<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 05.08.14 23:14
 */

defined('SYSPATH') or die();

use \controllers\engine\Form;

Form::open("content/options/processModal/$id", array('id'=>'modalOptionsForm'));
Form::openRow();
Form::openCol('col-lg-7');

Form::openPanel($lang->options['title']);

$icon = strtr(Form::icon('icon-sort-by-alphabet'),array('"'=>"'"));
$icon_spinner = strtr(Form::icon('icon-spinner'),array('"'=>"'"));
$lang_id = 0; $lang_code=''; $i=0;

foreach ($languages as $row) {

    if($row['front_default'] == 1) {
        $lang_id = $row['id']; $lang_code = $row['code'];
    }

    Form::html("<input type=\"hidden\" name=\"s_languages[{$row['id']}]\" value='{$row['code']}' class='ms-languages'>");
    if($i == 0) {
        Form::formGroup(
            '','',
            Form::button(
                $lang->core['translate'],
                $icon,
                array(
                    'class'   =>'btn-translate-all btn-info',
                    'onclick' => 'engine.translator.translateModalOptionsName('. $lang_id .', \''. $lang_code .'\', this); return false;',
                    'data-complete-text' => $icon .' '. $lang->core['translate'],
                    'data-loading-text' => $icon_spinner .' '. $lang->core['begin_translation']
                )
            )
        );
    }
    $i++;

    Form::formGroup(
        $lang->options['name'] . ' ['. $row['name'] .']',
        $lang->options['name_tip'],
        Form::input(array(
            'type'        => 'text',
            'name'        => "info[{$row['id']}][name]",
//            'id'        => "option_info_{$row['id']}_name",
            'required'    => 'required',
            'placeholder' => $lang->options['name_placeholder'],
            'data-parsley-required' => 'true',
            'value'       => isset($info[$row['id']]['name']) ? $info[$row['id']]['name'] : ''
        ))
    );
}

//            echo '<pre>';    print_r($type);echo '</pre>';

Form::formGroup(
    $lang->options['type'],
    $lang->options['type_tip'],
    Form::select(
        array(
            'name'     => "data[type]",
            'onchange' => "engine.content.options.getTypeValues({$id}, this.value);"
        ),
        $type
    )
);
Form::closePanel();

Form::closeCol();

Form::openCol('col-lg-5');
Form::openPanel($lang->core['params']);

Form::formGroup(
    $lang->core['published'],
    '',
    Form::checkbox('data[published]', isset($data['published']) ? $data['published'] : 1),
    array('class'=>'text-right')
);

Form::formGroup(
    $lang->options['required'],
    '',
    Form::checkbox('data[required]', isset($data['required']) ? $data['required'] : 1),
    array('class'=>'text-right')
);
Form::formGroup(
    $lang->options['multilang'],
    '',
    Form::checkbox('data[multilang]', isset($data['multilang']) ? $data['multilang'] : 1),
    array('class'=>'text-right')
);

Form::formGroup(
    $lang->options['show_list'],
    '',
    Form::checkbox('data[show_list]', isset($data['show_list']) ? $data['show_list'] : 0),
    array('class'=>'text-right')
);
Form::formGroup(
    $lang->options['show_compare'],
    '',
    Form::checkbox('data[show_compare]', isset($data['show_compare']) ? $data['show_compare'] : 0),
    array('class'=>'text-right')
);
Form::formGroup(
    $lang->options['show_filter'],
    '',
    Form::checkbox('data[show_filter]', isset($data['show_filter']) ? $data['show_filter'] : 0),
    array('class'=>'text-right')
);

Form::closePanel();
Form::closeCol();

Form::closeRow();
Form::hidden('action',$action);
Form::hidden('content_id',$content_id);
Form::hidden('content_options[]',$content_id);
Form::customSuccessAction("
    if(d.s){
        $(d.v).insertBefore($('.button-pink-new'));
    }
");
Form::close();