<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 27.07.14 17:04
 */
 
defined('SYSPATH') or die();
?>
<div class="row form-group options-value" id="v-<?=$options_value_id?>">
    <div class="col-xs-3">
        <button class="btn btn-sortable-move" style="cursor: move"><i class="icon-reorder icon-white"></i></button>
        <button
            title="<?=$lang->core['translate']?>"
            data-loading-text="<i class='icon-spinner icon-white'></i>"
            data-complete-text="<i class='icon-sort-by-alphabet icon-white'></i>"
            onclick="translateValue<?=$options_value_id?>(<?=$language['id']?>, '<?=$language['code']?>', this); return false;"
            class="btn btn-info">
            <i class="icon-sort-by-alphabet icon-white"></i>
        </button>
    </div>
    <div class="col-xs-7">
        <?php foreach ($inputs as $i=> $input) : ?>
            <div class="form-group">
                <?=$input?>
            </div>
        <?php endforeach; ?>
    </div>
    <div class="col-xs-2">
        <a
            class="btn btn-danger"
            onclick="engine.content.options.removeTypeValue('<?=$options_value_id?>')"
            href="javascript:void(0);"
        >
        <i class="icon-remove"></i>
        </a>
    </div>
</div>
<script>
    function translateValue<?=$options_value_id?>(id, code, el)
    {
        var btn = $(el);
        btn.button('loading');

        var data =  $(el).parent('div.col-xs-3')
            .next('.col-xs-7')
            .find('[id^=options_value_info_],[id^=new_options_value_info_]')
            .serialize();
        //options_value_info[2]
        data += '&'+ $('.s-languages').serialize();

        $.post('translator/translateOptionsValue/'+code+'/', data ,
            function(d){
                $(d).each(function(i,e){
                    $('#' + e.n).val(e.v).trigger('change');
                });
                btn.button('complete');
                $.pnotify({
                    title: engine.lang.core.info,
                    type: '',
                    history: false,
                    text: engine.lang.core.translation_complete
                });
            },'json');
    }
</script>