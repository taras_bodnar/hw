<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 01.06.14 10:46
 */
 use \controllers\engine\Form;

 defined('SYSPATH') or die();

    Form::open("brands/process/$id", array('class'=>'brandsForm','enctype' => 'multipart/form-data'), $lang);
        Form::openRow();
            Form::openCol('col-lg-8');

                Form::openPanel($lang->brands['title']);

                Form::formGroup(
                    $lang->brands['name'],
                    $lang->brands['name'],
                    Form::input(array(
                        'name'        => "data[name]",
                        'placeholder' => $lang->brands['name_placeholder'],
                        'value'       => isset($data['name']) ? $data['name'] : ''
                    )),
                    array('class'=>'b-data-img')
                );
                Form::formGroup(
    $lang->brands['url'],
    $lang->brands['url'],
    Form::input(array(
        'name'        => "data[url]",
        'placeholder' => $lang->brands['url_placeholder'],
        'value'       => isset($data['url']) ? $data['url'] : ''
    )),
    array('class'=>'b-data-img')
);
                Form::formGroup(
                    $lang->brands['img'],
                    $lang->brands['img_tip'],
                    Form::fileInput(
                        array(
                            'name'  => 'img',
                            'path'  => isset($data['path']) ? $data['path'] : '',
                            'src'   => isset($data['src']) ? $data['src'] : '',
                        )
                    ),
                    array('class'=>'b-data-img')
                );

                Form::closePanel();

            Form::closeCol();
            Form::openCol('col-lg-4');
                Form::openPanel($lang->core['params']);
                Form::formGroup(
                    $lang->core['published'],
                    '',
                    Form::buttonSwitch('data[published]',isset($data['published']) ? $data['published'] : 1),
                    array('class'=>'text-right')
                );
                Form::closePanel();
            Form::closeCol();
        Form::closeRow();
        Form::hidden('action',$action);

    Form::close();