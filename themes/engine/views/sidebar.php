<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 01.06.14 10:19
 */
 
defined('SYSPATH') or die();

?>
<script>
    $(document).ready(function() {
        if($('.sidebar').length){
            if ($.cookie('engineSidebar') == 'retracted') {
                $('.sidebar').removeClass('extended').addClass('retracted');
                $('.wrapper').removeClass('retracted').addClass('extended');
            }
            if ($.cookie('engineSidebar') == 'extended') {
                $('.wrapper').removeClass('extended').addClass('retracted');
                $('.sidebar').removeClass('retracted').addClass('extended');
            }

        } else {
            $('.sidebar').removeClass('retracted').addClass('extended');
        }
    });
</script>
<section class="sidebar extended">
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="clearfix">
                <img src="<?=$template_url?>images/oyi-logo.png" alt="oyi-logo">
                <h5>
                    <span class="title">
                        OYi.Engine
                    </span>
                    <span class="subtitle">
                        Content Management Framework
                    </span>
                </h5>
            </div>
        </div>
        <div class="panel-body">
            <div class="title">
                <?php if(!empty($icon)) : ?>
                <i class="<?=$icon?>"></i>
                <?php endif; ?>
                <span>
                    <?=$title?>
                </span>
                 <?=$button?>
            </div>
            <?php if($search) : ?>
            <div class="input-group">
                <input type="text" class="form-control" onkeyup="$('#tree').jstree('search',this.value); ">
                <div class="input-group-btn">
                    <button class="btn btn-default btn-search" type="button"><i class="icon-search"></i></button>
                </div>
            </div>
            <?php endif; ?>
        </div>
        <div class="panel-body tree-body <?php if(!$search) echo 'no-search'; ?>">
            <div class="scrollable">
                <div id="tree"></div>
            </div>
        </div>
    </div>
    <div class="sidebar-handle">
        <i class="icon-ellipsis-horizontal"></i>
        <i class="icon-ellipsis-vertical"></i>
    </div>
</section>
<script>
engine.sidebar = {
    build: function () {
        // Initiate sidebar events
        engine.sidebar.events();

        // Initiate sidebar retraction on smaller screen sizes
        engine.sidebar.retractOnResize();

        // Sets max-heigh for sidbar menu in mobile mode (needed for CSS transitions)
        engine.sidebar.setSidebarMobHeight();

        // Builds page data for sidebar menu
        engine.sidebar.buildPageData();

        // Check if jstree plugin exists, initiate if true
        !$.jstree || engine.sidebar.jstreeSetup();
    },
    events : function () {
        $(document).on('click', '.sidebar-handle', function(event) {
            event.preventDefault();
            engine.sidebar.toogleSidebar();
        });

    },
    toogleSidebar: function () {
        if($('.sidebar').length == 0) return ;
        $('.sidebar').toggleClass('extended').toggleClass('retracted');
        $('.wrapper').toggleClass('extended').toggleClass('retracted');

        if ($('.sidebar').is('.retracted')){
            $.cookie('engineSidebar', 'retracted', {
                expires: 7,
                path: '/'
            });
        }
        else {
            $.cookie('engineSidebar', 'extended', {
                expires: 7,
                path: '/'
            });
        }
    },
    retractOnResize: function () {

        if(!ltIE9 && !Modernizr.mq('(min-width:' + (screenMd) + 'px)')){
            if ($('.sidebar').is('.extended')){
                engine.sidebar.toogleSidebar();
            }
        }
    },
    jstreeSetup : function () {
//        $.jstree._themes = "styles/vendor/jstree-theme/";

        $("#tree").jstree({
            "json_data" : {
                "ajax" : {
                    "url" : "<?=$ajax_url?>",
                    "data" : function (n) {
                        return { id : n.data ? n.attr("id") : 0 };
                    },
                    'method' : 'POST'
                }
            },
            // the `plugins` array allows you to configure the active plugins on this instance
            "plugins" : [ "themes","json_data","ui","crrm","cookies","dnd","search","types","contextmenu" ],
            // each plugin you have included can have its own config object
            "core" : {
                "animation" : 100
            },
            <?php if($search) :  ?>
            "search" : {
                "case_insensitive" : true,
                    "ajax" : {
                    "url" : "<?=$ajax_url?>",
                        "data" : function (n) {
                        return { id : n.data ? n.attr("id") : 0 };
                    },
                    'method' : 'POST'
                }
            },
            <?php endif; ?>
                <?php if(isset($contextmenu) && !empty($contextmenu)) : ?>
                "contextmenu" : {
                    "items" : function ($node) {
                                return <?=str_replace(
                                    array('\/', '"function','}"}','\"'),
                                    array('/', 'function','}}','"'),
                                    json_encode($contextmenu)
                                )?>;
                    },
                    hide_on_mouseleave: true
                },
            <?php else :  ?>
            "contextmenu" : {},
            <?php endif; ?>
                // set a theme
                "themes" : {
                    "theme" : "oyi"
                }
            })
            .on('click', 'a', function() {
                    var treeLink = $(this).attr("href");
                    if (treeLink !== "#")
                        document.location.href = treeLink;

                    return false;
            })
            .bind("move_node.jstree", function (e, data) {
                data.rslt.o.each(function (i) {
                    var id    = $(this).attr("id"),
                        from  = data.rslt.op.attr("id"),
                        to    = data.rslt.cr === -1 ? 0 : data.rslt.np.attr("id"),
                        table = '<?=$table?>';
                    if(from == 'tree') from=0;
                    $.ajax({
                        async : false,
                        type: 'GET',
                        url: "tree/move/" + table + '/' + id + '/' + from + '/' + to,
                        success : function (r) {
                            if(r > 0){
                                $("#tree").jstree("refresh");
                                if($("table").length){
                                    var oTable = $('table').dataTable();
                                    oTable.fnDraw(oTable.fnSettings());
                                }
                            }

                        }
                    });
                });
            });
    },
    setSidebarMobHeight : function () {
        if(ltIE9 || Modernizr.mq('(min-width:' + (screenXs) + 'px)')){
            $('.sidebar').css('max-height','none');
        }
        else{
            $('.sidebar').css('max-height','none');
            setTimeout(function() {
                var sidebarMaxH = $('.sidebar > .panel').height() + 30 + 'px';
                $('.sidebar').css('max-height',sidebarMaxH);
            }, 200);
        }
    },
    doThislater : function (){
        $('.sidebar .sidebar-handle').on('click', function(){
            $('.panel, .main-content').toggleClass('retracted');
        });

        // APPLY THEME COLOR
        if ($.cookie('themeColor') == 'light') {
            $('body').addClass('light-version');
        }
        if($.cookie('jsTreeMenuNotification')!='true') {
            $.cookie('jsTreeMenuNotification', 'true', {
                expires: 7,
                path: '/'
            });
            $.pnotify({
                title: 'Slide Menu Remembers It\'s State',
                type: 'info',
                text: 'Slide menu will remain closed when you browse other pages, until you open it again.'
            });
        }
    },
    buildPageData : function () {
        var pageTitle = document.title;
        $('.bread-page-title').text(pageTitle);
        $('.preface p').text(pageTitle + ' include: ');
        engine.sidebar.treeJson = {
            'data' : [
                {
                    'data' : {
                        'title' : pageTitle,
                        'attr' : {
                            'href' : '#top',
                            'id' : 'engine-lvl-0'
                        }
                    },
                    'children' : [

                    ]
                }
            ]
        }

        var numSections = $('.section-title').length;
        $('.section-title').each(function(index, el) {
            if ($(this).is('.preface-title')) return;

            var sectionTitle = $.trim($(this).text());
            var sectionId = sectionTitle.replace(/\s+/g, '-').toLowerCase()

            // creates dash-case anchor ID to be used with sidebar links
            $(this).parents('.list-group-item').attr('id', sectionId);

            // Add item to breadcrumb nav
//            $('<li role="presentation"><a role="menuitem" tabindex="-1" href="#' + sectionId + '">' + sectionTitle + '</a></li>').appendTo('.breadcrumb-nav .active .dropdown-menu');


            // creates sidebar link object
            var newLinkObject = {
                'data' : {
                    'title' : sectionTitle,
                    'attr' : { 'href' : '#' + sectionId }
                }
            };
            engine.sidebar.treeJson.data[0].children.push(newLinkObject);

            // Add item to title bar
            if ((index + 1) !== numSections)
                $('.preface p').text($('.preface p').text() + sectionTitle + ', ');
            else
                $('.preface p').text($('.preface p').text().slice(0, -2) + ' and ' + sectionTitle + '.');
        });
    }
}
$(document).ready(function() {
    engine.sidebar.build();
    $('.jstree').delegate('a', 'contextmenu', function(e) {
       var id = $.jstree._focused()._get_node(this).attr('id');
        $.cookie('jstree_select', '#'+id);
        $('.jstree a').removeClass('jstree-clicked');
        $('li#'+id).find('a:first').addClass('jstree-clicked');
    });
});
</script>