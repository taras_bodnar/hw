<?php
/**
 * Company Otakoyi.com
 * Author: wg
 * Date: 12.01.15 19:56
 */
defined("SYSPATH") or die();
if(empty($settings)) return '';
?>
<?php foreach ($settings as $item ) : ?>
<div class="form-group ">
    <label><?=$item['description']?></label>
    <input type="text" id="settings_<?=$item['name']?>" value="<?=$item['value']?>" required="required" name="settings[<?=$item['name']?>]" class="form-control">
</div>
<?php endforeach; ?>