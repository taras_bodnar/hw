<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 01.06.14 10:46
 */
 use \controllers\engine\Form;

 defined('SYSPATH') or die();

    Form::open("payment/process/$id", array(), $lang);
        Form::openRow();
            Form::openCol('col-lg-8');

                Form::openPanel($lang->payment['title']);

                $hide='';
                if(count($languages) > 1) {
                    $hide='hide';
                    Form::languagesSwitch(
                        $lang->core['sel_languages'],
                        $lang->core['sel_languages_tip'],
                        $languages,
                        $lang
                    );
                }
                foreach ($languages as $row) {
                    Form::html("<div class=\"language-switch $hide language-{$row['id']}\">");
                    Form::formGroup(
                        $lang->payment['name'],
                        $lang->payment['name_tip'],
                        Form::input(array(
                            'type'        => 'text',
                            'name'        => "info[{$row['id']}][name]",
                            'required'    => 'required',
                            'placeholder' => $lang->payment['name_placeholder'],
                            'data-parsley-required' => 'true',
                            'value'       => isset($info[$row['id']]['name']) ? $info[$row['id']]['name'] : ''
                        ))
                    );
                    Form::formGroup(
                        $lang->payment['desc'],
                        $lang->payment['desc_tip'],
                        Form::editor(
                            array(
                                'name' => "info[{$row['id']}][description]",
                                'placeholder' => $lang->payment['desc_placeholder'],
                                'value'       => isset($info[$row['id']]['description']) ? $info[$row['id']]['description'] : ''
                            )
                        )
                    );
                    Form::html('</div>');// .info
                }


            Form::closePanel();

            Form::closeCol();

            Form::openCol('col-lg-4');

                Form::openPanel($lang->core['params']);

                Form::formGroup(
                    $lang->payment['published'],
                    $lang->payment['published'],
                    Form::buttonSwitch('data[published]',isset($data['published']) ? $data['published'] : 0),
                    array('class'=>'text-right')
                );

                Form::formGroup(
                    $lang->payment['module'],
                    $lang->payment['module_tip'],
                    Form::select(
                        array(
                            'name' => 'data[module]',
                        ),
                        $modules,
                        isset($data['module']) ? $data['module'] : 0
                    )
                );

                Form::formGroup(
                    $lang->payment['currency'],
                    $lang->payment['currency_tip'],
                    Form::select(
                        array(
                            'name' => 'data[currency_id]',
                        ),
                        $currency,
                        isset($data['currency_id']) ? $data['currency_id'] : 0
                    )
                );
                Form::html('<div id="paymentSettings" data-payment-id="'.$id.'">'.$settings.'</div>');
                Form::closePanel();

                Form::openPanel($lang->payment['payment']);

                Form::formGroup(
                    $lang->payment['delivery_items'],
                    $lang->payment['delivery_items_tip'],
                    Form::pickList(
                        array(
                            'name' => 'dp[]',
                            'sort_url' => 'delivery/sort',
                            'nav_id'   => $id
                        ),
                        $delivery
                    )
                );

                Form::closePanel();
            Form::closeCol();
        Form::closeRow();
        Form::hidden('action',$action);

    Form::close();