<div class="mod-form">
    <div class="modal-wrapper">
        <h2>{$t.callback_title}</h2>
    </div>
    <div class="background-line">
        <div class="modal-wrapper">
            <p class="response">{$t.callback_msg}</p>
        </div>
    </div>
    <div class="modal-wrapper">
        <form method="post" action="ajax/Callback/process" id="CallbackForm">
            <div class="form-row">
                <label for="data_name">{$t.callback_pib} <span>*</span></label>
                <input type="text" required="" name="data[name]" id="data_name">
            </div>
            <div class="form-row">
                <label for="data_phone">{$t.callback_phone} <span>*</span></label>
                <input type="text" required="" name="data[phone]" id="data_phone" >
            </div>
            <div class="form-row">
                <label for="data_comment">{$t.callback_comment}</label>
                <textarea required="" name="data[comment]" id="data_comment"></textarea>
            </div>
            <button type="submit" class="call-btn">{$t.callback_btn}</button>
            <input type="hidden" name="skey" value="{$skey}" />
        </form>
    </div>
</div>