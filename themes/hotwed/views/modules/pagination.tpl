<ul>
    {if $prev}
        <li><a href="{$prev.url}" class="btn-prev animation-left"><i class="icon-arrow-left"></i></a></li>
    {/if}
    {if count($items) > 1}
    {foreach $items as $item}
        {if $item.class=='active'}
            <li class='active'><span>{$item.name}</span></li>
        {else}
            <li><a href="{$item.url}">{$item.name}</a></li>
        {/if}
    {/foreach}
    {/if}
    {if $next}
        <li><a href="{$next.url}" class="btn-next animation-right"><i class="icon-arrow-right"></i></a></li>
    {/if}

</ul>