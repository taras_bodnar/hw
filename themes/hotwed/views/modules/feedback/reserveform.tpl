<h2 class="reserve-form-header">Підтвердіть ваше замовлення</h2>
<form action="ajax/Feedback/reserveTravel" id="reserveTravel" class="reserve-form">

    <input type="hidden" value="{$skey}" name="skey">
    <input type="hidden" value="{$travel_name}" name="data[travel_name]">
    <input type="hidden" value="{$travel_price}" name="data[travel_price]">
    <input type="hidden" value="{$travel_date}" name="data[travel_date]">
    <div class="row input-box">
        <label>{$t.u_email}</label>
        <input required name="data[email]" type="email"/>
    </div>
    <div class="row input-box">
        <label>{$t.u_name}</label>
        <input required name="data[name]" type="text"/>
    </div>
    <div class="row input-box">
        <label>{$t.ui_phone}</label>
        <input required id="phone" name="data[phone]" type="text"/>
    </div>

    <div class="row footer">
        <div class="row reserve-button">
            <button class="btn red">{$t.b_order}</button>
        </div>
    </div>
    <div id="output"></div>
</form>

{literal}
    <script>
        $(document).ready(function(){
            $("#reserveTravel").ajaxForm({
                target: '#output',
                type: 'post',
                dataType: 'json',
                success: function (data) {
                    if (data.s == 0) {
                        $("button.btn").removeAttr("disabled");
                        $("#output").html(data.m);
                    } else {

                        $("#output").html(data.m);
                        $("#go_back").on("click",function(){
                            $(".fancybox-close").click();
                        });
//                        $("#reserveTravel").clear();
                        $.fancybox(data.view, {
                            padding: '0',
                            openMethod:'changeIn',
                            maxWidth: 500,
                            maxHeight: 300,
                            autoDimension: false,
                            helpers : {
                                overlay : {
                                    css : {
                                        'background' : 'rgba(0,0,0,0.7)',
                                        'transition' : 'all 0.35s ease;'
                                    }
                                }
                            },
                            openEffect:'fade',
                            closeEffect:'fade'
                        });
                    }
                },
                beforeSend : function () {
                    $("button.btn").attr("disabled","disabled");
                }
            });
        });
    </script>

{/literal}