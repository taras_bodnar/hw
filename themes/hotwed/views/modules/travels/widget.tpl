<section class="l-slider-news">
    <div class="container">
        <div class="header">
            <h2>{$t.w_news_title}</h2>
            <div class="btn-group">
                <a class="btn-prev animation-left">
                    <i class="icon-arrow-left"></i>
                </a>
                <a class="btn-next animation-right">
                    <i class="icon-arrow-right"></i>
                </a>
            </div>
        </div>
        <div id="slider-news" class="owl-carousel">
            {foreach $items as $item}
            <div class="item">
                <div class="midle">
                    <div class="img">
                        <a href="{$item.id}" title="{$item.title}"><img src="{$item.img.src}" alt=""/></a>
                    </div>
                    <div class="date"><span>{$item.pub_date}</span></div>
                    <div class="title">
                        <a href="{$item.id}" title="{$item.title}">{$item.name}</a>
                    </div>
                    <p>{$item.description}</p>
                </div>
            </div><!-- .item -->
            {/foreach}
        </div>
        <div class="footer">
            <a href="5" class="btn png-red">{$t.b_news_more}</a>
        </div>
    </div>
</section>