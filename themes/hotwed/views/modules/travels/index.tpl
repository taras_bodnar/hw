<section class="l-travel-places">
    {foreach $items as $item}
        <div class="item">
            <div class="item-midle">
                <div class="img">
                    <img src="{$item.img.src}" alt=""/>   {* bground  category image *}
                </div>
                <a href="{$item.id}" title="{$item.title}">  {* link *}
                    <div class="middle">                        {* content *}
                        <div class="table">
                            <div class="table-cell">
                                <div class="title">{$item.name}</div>
                                <div class="number">{$item.t} {$t.count_workers}</div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div><!-- .item -->
    {/foreach}

</section><!-- .l-category-specialists-->