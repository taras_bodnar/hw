<div class="item">
    <div class="midle">
        <div class="img">
            <a href="{$item.id}" title="{$item.title}" ><img src="{$item.img.src}" alt=""/></a>
        </div>
        <div class="date"><span>{$item.dd} {$item.mm} {$item.yy}</span></div>
        <div class="title">
            <a href="{$item.id}" title="{$item.title}" >{$item.name}</a>
        </div>
        <p>{$item.description}</p>
    </div>
</div><!-- .item -->