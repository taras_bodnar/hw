{if $smarty.get.filter}
<div class="frame-check-filter">
    <div class="inside-padd">
        <div class="title">{str_replace('{total}', $total, $t.shop_filter_found)}</div>
        <ul class="list-check-filter">
            {if $smarty.get.filter.minp && $smarty.get.filter.maxp}
                <li class="clear-slider">
                    <button type="button" id="clear-range">
                        <span class="icon_times icon_remove_filter f_l"></span>
                        <span class="name-check-filter">{str_replace(
                            array('{from}', '{to}'),
                            array($smarty.get.filter.minp, $smarty.get.filter.maxp),
                            $t.shop_filter_ft
                            )}</span>
                    </button>
                </li>
            {/if}
            {foreach $selected_features as $item}
                {foreach $item.values as $value}
                    <li class="clear-slider">
                        <button type="button" class="clear-features" value="f-{$item.id}x{$value.id}">
                            <span class="icon_times icon_remove_filter f_l"></span>
                            <span class="name-check-filter">{$item.name}: {$value.value}</span>
                        </button>
                    </li>
                 {/foreach}
            {/foreach}
        </ul>
        <div class="foot-check-filter">
            <button class="btn-reset-filter" onclick='self.location.href="{$page.id}"' type="button">
                <span class="icon_times icon_remove_all_filter f_l"></span>
                <span class="text-el d_l_r_f">Сбросить фильтр</span>
            </button>
        </div>
    </div>
</div>
{/if}

    <form method="get" id="catalogForm">
        <div class="frame-filter p_r">
            <div class="preloader wo-i"></div>
            <div class="frames-checks-sliders">
                <div class="frame-slider" data-rel="sliders.slider1">
                    <div class="title">Цена в гривнах</div>
                    <div class="inside-padd">
                        <div class="slider-cont">
                            <noscript>Джаваскрипт не включен</noscript>
                            <div class="slider" id="slider-range">
                                <a href="#" class="ui-slider-handle left-slider"></a>
                                <a href="#" class="ui-slider-handle right-slider"></a>
                            </div>
                        </div>
                        <div class="form-cost number">
                            <div class="t-a_j">
                                <label>
                                    <input type="text" class="minCost" id='minp' name="filter[minp]" value="{$filter_minp}" />
                                </label>
                                <label>
                                    <input type="text" class="maxCost" id="maxp" name="filter[maxp]" value="{$filter_maxp}" />
                                </label>
                                <div class="btn-def">
                                    <input type="submit" value="ОК" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {if !empty($manufactutrers)}
                <div class="frame-group-checks" data-rel="dropDown">
                    <div class="inside-padd">
                        <div class="title">
                            <span class="f-s_0">
                                <span class="d_b">
                                    <span class="text-el">Производитель</span>
                                </span>
                                <span class="icon-arrow"></span>
                            </span>
                        </div>
                        <div class="filters-content">
                            <ul>
                                {foreach $manufactutrers as $item}
                                <li >
                                    <div class="frame-label" id="m-{$item.id}">
                                        <span class="niceCheck b_n">
                                            <input onchange="submit();" {if $smarty.get.filter.m && in_array($item.id, $smarty.get.filter.m)}checked{/if} name="filter[m][]" value='{$item.id}' type="checkbox"  />
                                        </span>
                                        <div class="name-count">
                                            <span class="text-el">{$item.name}</span>
                                            <span class="count">(0)</span>
                                        </div>
                                    </div>
                                </li>
                                {/foreach}
                            </ul>
                        </div>
                    </div>
                </div>
                {/if}

                {foreach $features as $item}
                <div class="frame-group-checks" data-rel="dropDown">
                    <div class="inside-padd">
                        <div class="title">
                            <span class="f-s_0">
                                <span class="d_b">
                                    <span class="text-el">{$item.name}</span>
                                </span>
                                <span class="icon-arrow"></span>
                            </span>
                        </div>
                        <div class="filters-content">
                            <ul>
                                {foreach $item.values as $value}
                                <li >
                                    <div class="frame-label" id="m-{$item.id}-{$value.id}">
                                        {if in_array($item.type, array('sm','select'))}
                                            <span class="niceCheck b_n">
                                                <input onchange="submit();"
                                                       {if $smarty.get.filter.f[$item.id] && in_array($value.id, $smarty.get.filter.f[$item.id])}checked{/if}
                                                       name="filter[f][{$item.id}][]"
                                                       id="f-{$item.id}x{$value.id}"
                                                       value='{$value.id}' type="checkbox"  />
                                            </span>
                                        {elseif $item.type == 'number' || $item.type == 'text'}
                                            <span class="niceCheck b_n">
                                                <input onchange="submit();"
                                                       {if $smarty.get.filter.f[$item.id] && in_array($value.value, $smarty.get.filter.f[$item.id])}checked{/if}
                                                       name="filter[f][{$item.id}][]" value='{$value.value}' type="checkbox"  />
                                            </span>
                                        {/if}
                                        <div class="name-count">
                                            <span class="text-el">{$value.value}</span>
                                            {*<span class="count">({$value.total})</span>*}
                                        </div>
                                    </div>
                                </li>
                                {/foreach}
                            </ul>
                        </div>
                    </div>
                </div>
                {/foreach}
            </div>
        </div>
    </form>
    {*<script type="text/javascript">*}
        {*function removeFilter(name) {*}

            {*$('#' + name + ' input').parent().nStCheck('checkUnChecked');$(self.catalogForm).submit();*}
            {*return false;*}
        {*}*}

    {*</script>*}
    {literal}
    <script type="text/javascript">
        var preloader = $('.preloader'), frameGroup = $('.frame-group-checks'), t=500, catalogForm = $('#catalogForm');
        $(document).ready(function(){
            preloader.hide();

            frameGroup.each(function(){
                $(this).fadeIn(t);
                t+= 500;
                var title   = $(this).find('.title'),
                    content = $(this).find('.filters-content');
                title.click(function(){
//                    console.log('click');
                    content.slideToggle(300);
                });
                content.find('input:checked').each(function(){
                    $(this).parents('.filters-content').css('display', 'block');
                });
            });

            var sl =$( "#slider-range"),iMinp=$("#minp"),iMaxp=$("#maxp"),minp= 0,maxp=0;
            if(sl.length == 0) return ;

            minp=iMinp.val();
            maxp=iMaxp.val();
            /*if(parseInt(iMinp.data('value')) != 0){
                minp = iMinp.data('value');
            }
            if(parseInt(iMaxp.data('value')) != 0){
                maxp = iMaxp.data('value');
            }*/

//            console.log(minp,maxp);

            sl.slider({
                range: true,
                min: 0.00,
                max: iMaxp.val(),
                values: [minp,maxp],
                slide: function( event, ui ) {
                    iMinp.val( ui.values[ 0 ] );
                    iMaxp.val( ui.values[ 1 ] );
                }
            });
            iMinp.val( sl.slider( "values", 0 ) );
            iMaxp.val( sl.slider( "values", 1 ) );

            $(document).on('change',iMinp, function(){
                var v1=parseInt(iMinp.val());
                var v2=parseInt(iMaxp.val());

                if(v1>v2){
                    v1=v2;
                    iMinp.val(v1);
                }
                sl.slider("values",0,v1);
            });
            $(document).on('change',iMaxp, function(){
                var v1=parseInt(iMinp.val());
                var v2=parseInt(iMaxp.val());
                if(v2<v1){
                    v2=v1;
                }
                sl.slider("values", 1, v2);
            });

            $(document).on('click','#clear-range', function(){
                iMinp.attr('disabled', true);
                iMaxp.attr('disabled', true);
                catalogForm.submit();
            });

            $(document).on('click', '.clear-features', function(){
                $('#' + this.value).removeAttr('checked');
                catalogForm.submit();
            });


        });
    </script>
    {/literal}