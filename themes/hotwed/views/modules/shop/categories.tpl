<div class="art_div">
    {foreach $items as $item}
       <div class="center-ccipu">
            <div class="newspage-countries">
                <a href="{$item.id}" title="{$item.title}">
                    <img border="0" class="img_cat" src="{$item.img.src}">
                </a>
                <h3 class="zah_cat_h2">
                    <a href="{$item.id}" class="zah_cat">{$item.name}</a>
                </h3>
                <div class="pid_catt">
                    {if $item.isfolder}
                    <ul style="list-style-type:none;">
                        {foreach $item.children as $li}
                        <li>
                            <a href="{$li.id}" style="text-decoration:none;" title="{$li.title}" >{$li.name}</a>
                        </li>
                        {/foreach}
                    </ul>
                    {/if}
                </div>
                <div class="readmore11">
                    <a href="{$item.id}" title="{$item.title}">{$t.s_c_all} {$item.name}</a>
                </div>
            </div>
        </div>
    {/foreach}
</div>