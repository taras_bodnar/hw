{if $total > 0}
<div class="frame-inside">
    <div class="container">
        <div class="right-catalog" >
            <div class="f-s_0 title-category">
                <div class="frame-title">
                    <h1 class="d_i"><span class="s-t">{$t.search_h1}</span> <span class="what-search">«{$smarty.get.sq}»</span></h1>
                </div>
                <span class="count">{str_replace('$t', $total, $t.search_found_t)}</span>
            </div>
            <div class="frame-header-category">
                <div class="header-category f-s_0">
                    <div class="inside-padd t-a_j">
                        <!-- Start. Order by block -->
                        <form action="{$page.id}" method="get" class="frame-sort f-s_0 d_i-b">
                            <span class="title">{$t.sorting}</span>
                            <ul class="sort d_i-b v-a_b f-s_0" id="sort">
                                {foreach $sorting as $item}
                                    <li class="{if isset($smarty.get.order) && $smarty.get.order ==$item.id}active{/if} d_i-b v-a_b">
                                        <button class="d_l_1" name="order" value="{$item.id}">{$item.name}</button>
                                    </li>
                                {/foreach}
                            </ul>
                            <input type="hidden" name="sq" value="{$smarty.get.sq}" />
                        </form>
                        <!-- End. Order by block -->
                        <!--        Start. Show products as list or table-->
                        <nav class="frame-catalog-view f-s_0 d_i-b">
                            <span class="title">Вид:</span>
                            <ul class="tabs groups-buttons tabs-list-table f-s_0" data-elchange="#items-catalog-main" data-cookie="listtable">
                                <li class="active  d_i-b v-a_m">
                                    <button type="button" data-href="table" data-title="Таблица" data-rel="tooltip">
                                        <span class="icon_table_cat"></span>
                                    </button>
                                </li>
                                <li class=" d_i-b v-a_m">
                                    <button type="button" data-href="list" data-title="Список" data-rel="tooltip">
                                        <span class="icon_list_cat"></span>
                                    </button>
                                </li>
                            </ul>
                        </nav>
                        <!--        End. Show products as list or table-->
                    </div>
                </div>
            </div>
            <ul class="animateListItems items items-catalog  table" id="items-catalog-main">
                {$products}
            </ul>
            {$pagination}
        </div>

        <div class="left-catalog">
            <div class="frame-category-menu layout-highlight">
                <div class="title-menu-category">
                    <div class="title-default">
                        <div class="title-h3 title">{$t.search_cat}</div>
                    </div>
                </div>
                <div class="inside-padd">
                    <nav>
                        {foreach $categories as $item}
                        <ul class="nav nav-vertical nav-category">
                            <li>
                                <span>{$item.name}</span>
                            </li>
                        </ul>
                        {/foreach}
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
{else}
    <div style="width:100% !important" class="right-catalog">
        <div class="msg layout-highlight layout-highlight-msg">
            <div class="info">
                <span class="icon_info"></span>
                <span class="text-el">{$t.search_nf}</span>
            </div>
        </div>
        <!--Start. Pagination -->
        <!-- End pagination -->
    </div>
{/if}