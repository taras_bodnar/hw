<div class="product-page clearfix">
    <h1>{$page.name}</h1>
    <div class="container-full-width no-padding">
        <div class="wrapper-large">
            <div class="this-pr-box">
                <div class="image-prev">
                    <a href="/uploads/content/{$page.id}/product_large/{$cover_img.src}" class="modal"><img src="/uploads/content/{$page.id}/product_large/{$cover_img.src}"></a>
                    <a href="/uploads/content/{$page.id}/product_large/{$cover_img.src}" class="large-image-link modal">
                        <span>{$t.open_img}</span>
                    </a>
                </div>
                <div class="this-pr-inf">
                    {if $params.hit}
                    <div class="br">
                        <span class="top-sales">{$t.label_hit}</span>
                    </div>
                    {/if}

                    <div class="br">
                        {if $params.availability}
                            <span class="is_available">{$t.in_av}</span>
                        {else}
                            <span class="no_available">{$t.no_av}</span>
                        {/if}
                    </div>
                </div>

            </div>

            <div class="description-box">
                {if $params.availability}
                <form id="toCart" action="ajax/Cart/add" method="post" class="description-head">
                    <p class="calc-pr">{number_format($params.price, 0, ',', ' ')} {$params.symbol}</p>
                    <p class="multiplication"></p>
                    <input type="number" value="4" name="quantity" data-price="{$params.price}" >
                    <p class="equal"></p>
                    <p class="calc-pr-total"><span id="pTotal">{number_format(round(4 * $params.price, 2), 0, ',', ' ')}</span> {$params.symbol}</p>
                    <button class="btn-large-blue">{$t.to_cart}</button>
                    <input type="hidden" name="skey" value="{$skey}" />
                    <input type="hidden" name="variant_id" value="{$params.id}" />
                    {*<a href="/inc/cart-window.html" class="btn-large-blue modal" data-fancybox-type="ajax">{$t.to_cart}</a>*}
                </form>
                {/if}
                <div class="product-tabs">
                    <ul class='etabs'>
                        <li class='tab'><a href="#info"><span>{$t.tab_desc}</span></a></li>
                        <li class='tab'><a href="#feedback"><span>{$t.tab_reviews} ({$comments_total})</span></a></li>
                    </ul>
                    <div id="info">
                        <div class="info-table">
                            <div class="table-row">
                                <div class="tb tb-option">
                                    <span>{$t.code}</span>
                                </div>
                                <div class="tb tb-value">
                                    <span>{$params.code}</span>
                                </div>
                            </div>
                            {foreach $features as $item}
                            <div class="table-row">
                                <div class="tb tb-option">
                                    <span>{$item.name}</span>
                                </div>
                                <div class="tb tb-value">
                                    {if isset($item.value)}
                                        {$item.value}
                                    {else}
                                        {foreach $item.values as $k=>$v}
                                            {if $item.id==7}
                                                <a href="{$params.cid};filter[f][{$item.id}]={$k}" title="{$v}">
                                                    <span class="season {if $k==12}all-seasons{elseif $k==11}summer{elseif $k==10}winter{/if}"></span>
                                                </a>
                                            {elseif $item.id==3}
                                                <a href="{$params.cid};filter[f][{$item.id}][]={$k}">{$v}</a>
                                            {else}
                                                <a href="{$params.cid};filter[f][{$item.id}]={$k}">{$v}</a>
                                            {/if}
                                        {/foreach}
                                    {/if}
                                </div>
                            </div>
                            {/foreach}
                        </div>
                        <div class="right-block cms_content">
                            {$page.content}
                            <blockquote>
                               {$t.pay_delivery}
                            </blockquote>
                        </div>
                        <div class="social">
                            <div class="likes facebook">
                                <a href="#"></a>
                                <span>0</span>
                            </div>
                            <div class="likes vk">
                                <a href="#"></a>
                                <span>+1</span>
                            </div>
                            <div class="likes google">
                                <a href="#"></a>
                                <span>0</span>
                            </div>
                        </div>
                    </div>
                    <div id="feedback">
                        {$comments}
                    </div>
                </div>


            </div>
        </div>
        <div class="right-sidebar">
            <div class="another-options">
                <p>Варіанти з іншими параметрами</p>
                <ul>
                    <li><a href="#">225/51 R16</a></li>
                    <li><a href="#">225/55 R17</a></li>
                    <li><a href="#">225/55 R17</a></li>
                    <li><a href="#">225/55 R17</a></li>
                </ul>
                <ul>
                    <li><a href="#">225/51 R16</a></li>
                    <li><a href="#">225/55 R17</a></li>
                </ul>
            </div>
            <div class="link-to-filter">
                <a href="7" class="d-link">
                    <span></span>
                    <span>{$t.filter_tires_by_params}</span>
                </a>
            </div>
            <div class="link-to-filter">
                <a href="8" class="c-link">
                    <span></span>
                    <span>{$t.filter_disc_by_params}</span>
                </a>
            </div>
            <div class="link-to-filter">
                <a href="26" class="b-link">
                    <span></span>
                    <span>{$t.filter_tires_by_brand}</span>
                </a>
            </div>
            <div class="link-to-filter">
                <a href="27" class="b-link">
                    <span></span>
                    <span>{$t.filter_disc_by_brand}</span>
                </a>
            </div>
        </div>
    </div>
</div>
<section class="main-product-content">
    [[mod:Shop::siblings]]
    [[mod:Shop::viewed]]
</section>