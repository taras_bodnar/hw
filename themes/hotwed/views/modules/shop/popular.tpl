<div class="main-row clearfix has-filter">
    <h1>{$t.tires_popular}</h1>
    <div class="product-content" id="pc1">

        <div class="slider-navigation">
            <a href="7" class="all">{$t.link_all_tires}</a>
            <a class="btn prev"></a>
            <span></span>
            <a class="btn next"></a>
        </div>
        <div class="owl-carousel owl-theme">
            {$items}
        </div>
    </div>
    <aside class="side-filter">
        <h6>{$t.link_brand_tires}:</h6>
        {foreach $vendors as $row}
        <ul class="brand-links">
            {foreach $row as $item}
            <li><a href="7;filter[f][3][]={$item.id}">{$item.name}</a></li>
            {/foreach}
        </ul>
        {/foreach}
        <a href="26">{$t.filter_all_vendors}</a>
    </aside>
</div>