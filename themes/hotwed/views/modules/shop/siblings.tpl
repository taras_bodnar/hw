<div class="main-row clearfix">
    <h2>{$t.siblings_title}</h2>
    <div class="product-content" id="pc4">
        <div class="slider-navigation">
            <a class="btn prev"></a>
            <span></span>
            <a class="btn next" ></a>
        </div>

        <div class="owl-carousel owl-theme">
            {$siblings_items}
        </div>
    </div>
</div>