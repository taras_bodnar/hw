<div class="main-row clearfix">
    <h2>{$t.action_products}</h2>
    <div class="product-content" id="pc3">

        <div class="slider-navigation">
            <a class="btn prev"></a>
            <span></span>
            <a class="btn next" ></a>
        </div>

        <div class="owl-carousel owl-theme">
            {$items}
        </div>
    </div>
</div>