<h2>{$t.product_features}</h2>
<div class="characteristic">
    <div class="product-charac patch-product-view">
        <table border="0" cellpadding="4" cellspacing="0" class="characteristic">
            <tbody>
            {foreach $features as $item}
                <tr>
                    <td>{$item.name}</td>
                    <td>
                        {if isset($item.value)}
                            {$item.value}
                        {else}
                            {implode(', ', $item.values)}
                        {/if}
                    </td>
                </tr>
            {/foreach}
            </tbody>
        </table>
    </div>
</div>