<form class="fl-wrap" id="shopFilter" action="ajax/Shop/displayProducts" method="post">
    <div class="fl-top-box">
        <p>Заводська комплектація:</p>
        <ul class="filt-btn">
            <li>
                <a href="#" class="active">
                    <span>205/60 R16</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <span>205/55 R16</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <span>205/50 R17</span>
                </a>
            </li>
        </ul>
        <p>Варіанти заміни:	</p>
        <ul class="filt-btn">
            <li>
                <a href="#">
                    <span>205/50 R17</span>
                </a>
            </li>
        </ul>
    </div>
    <h6>{if $cat_id == 7}{$t.filter_tires}{else}{$t.filter_disc}{/if}</h6>
    <div class="side-fl-tabs">
        <ul class='etabs'>
            <li class='tab'>
                <a href="#f_features">
                    <span>{$t.sel_by_params}</span>
                </a>
            </li>
            <li class='tab'>
                <a href="#f_car">
                    <span>{$t.sel_by_auto2}</span>
                </a>
            </li>
        </ul>
        <div id="f_features" class="tb-cont {if !isset($smarty.request.filter.car.vendor_id)}active{/if} ">
            {foreach $features as $item}
                {if $item.id == 7}
                    <div class="each-select full-width">
                        <label for="f_{$item.id}">{$item.name}:</label>
                        {foreach $item.values as $v}
                            <input {if $smarty.request.filter.f[$item.id] == $v.id}checked{/if}
                                    onchange="ShopFilter()" type="radio"
                                   class="rad-{if $v.id == 10}winter{/if}{if $v.id== 11}summer{/if}{if $v.id == 12}all-seasons{/if}"
                                   id="f_{$item.id}_{$v.id}"
                                   name="filter[f][{$item.id}]" value="{$v.id}" title="{$v.value}">
                        {/foreach}
                    </div>
                    {elseif $item.id != 3 && $item.id !=9}
                    <div class="each-select">
                        <label for="f_{$item.id}">{$item.name}:</label>
                        <select
                                data-placeholder="{$t.filter_sel_all}"
                                onchange="ShopFilter()"
                                id="f_{$item.id}"
                                name="filter[f][{$item.id}]"
                                >
                            <option value="">{$t.filter_sel_all}</option>
                            {foreach $item.values as $v}
                                <option {if $smarty.request.filter.f[$item.id] == $v.id}selected{/if} value="{$v.id}">{$v.value}</option>
                            {/foreach}
                        </select>
                    </div>
                {/if}
            {/foreach}
        </div>
        <div id="f_car" class="tb-cont {if $smarty.request.filter.car.vendor_id>0}active{/if}">
            <div class="each-select">
                <label for="filter_car_vendor_id">{$t.filter_vendor}:</label>
                <select data-placeholder="{$t.filter_sel_all}" id="filter_car_vendor_id" name="filter[car][vendor_id]">
                    <option value="">{$t.filter_sel_all}</option>
                    {foreach $car_vendors as $v}
                        <option {if $smarty.request.filter.car.vendor_id == $v.id}selected{/if} value="{$v.id}">{$v.vendor}</option>
                    {/foreach}
                </select>
            </div>
            <div class="each-select">
                <label for="filter_car_model_id">{$t.filter_model}</label>
                <select name="filter[car][model_id]" id="filter_car_model_id" disabled></select>
            </div>
            <div class="each-select">
                <label for="filter_car_year_id">{$t.filter_year}</label>
                <select name="filter[car][year_id]" id="filter_car_year_id" disabled></select>
            </div>
            <div class="each-select">
                <label for="filter_car_modification_id">{$t.filter_modification}</label>
                <select data-placeholder="{$t.filter_sel_all}"  onchange="ShopFilter();" name="filter[car][modification_id]" id="filter_car_modification_id" disabled></select>
            </div>
        </div>
    </div>
    <div class="sl-filter">

        <p class="fr-title">{$t.filter_price}</p>
        <p>
            <input {if !isset($smarty.request.filter.minp)}disabled{/if} value="{$filter_minp}" name="filter[minp]" type="text" id="minp" class="min">
            <input {if !isset($smarty.request.filter.maxp)}disabled{/if} value="{$filter_maxp}" name="filter[maxp]" type="text" id="maxp" class="max">
        </p>
        <div class="ui-slider" id="slider-range"></div>

{* Список виробників *}
        <p class="fr-title">{$t.vendor}</p>

        <div class="check-btn">
            {foreach $vendors as $i=>$item}
            <div class="br vendors {if $i > 10}hidden{/if}">
                <label class="has-text">
                    {$item.name}
                    <input
                            onchange="ShopFilter();"
                            type="checkbox"
                            {if isset($smarty.request.filter.f[$vendors_features_id]) && in_array($v.id, $smarty.request.filter.f[$vendors_features_id])}checked{/if}
                            name="filter[f][{$vendors_features_id}][]"
                            value="{$item.id}"
                        >
                </label>
            </div>
            {/foreach}
            {if count($vendors) > 10}
            <a data-hide="{$t.b_hide_vendors}" data-show="{$t.b_show_all}" class="toggle-vendors" href="javascript:void(0);">{$t.b_show_all}</a>
            {/if}
        </div>
{* Список виробників *}

        <button type="submit" class="btn-mid-blue bt">{$t.btn_filter}</button>

        <a class="reset" href="{$cat_id}">{$t.filter_reset}</a>
    </div>
    <input type="hidden" name="skey" value="{$skey}" />
    <input type="hidden" name="cid" value="{$cat_id}" />
    {if $smarty.request.q}
        <input type="hidden" name="q" value="{$smarty.request.q}" />
    {/if}
</form>
{literal}<style>.hidden{display:none}.loading{background: url("/themes/tires/assets/img/loading.gif") no-repeat center center;}</style>{/literal}