<section class="product-is-filter clearfix">
    <div class="wr-cont" id="shopProducts">
       {if !empty($products_list) }
            {$products_list}
        {else}
            {$t.filter_no_results}
        {/if}
    </div>
    <aside class="main-fl-side" id="shopFilterContent">
        {$filter}
    </aside>

    <div class="clearfix"></div>
    <div class="more b-shop-more" style="display: {if $total_left > 0}block{else}none{/if}">
        <a onclick="return false;" href="{$more_uri}" data-category="{$cat_id}"><span>{$t.news_show_more} <span id="total">{$ipp} з {$total_left}</span></span></a>
    </div>

    {*<div id="pagination">{$pagination}</div>*}
</section>
<section class="main-product-content">
    [[mod:Shop::viewed]]
    [[mod:Shop::recommended]]
</section>