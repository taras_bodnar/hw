{if $smarty.request.q}
    <h1>{$t.search_results} "{$smarty.request.q}"</h1>
{else}
    <h1>{$name}</h1>
{/if}
{$panel_sorting}
<div class="wr filt shop-products">
    {$products}
</div>