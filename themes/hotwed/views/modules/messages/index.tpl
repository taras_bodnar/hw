<div class="l-message">
    <div class="container">
        <form class="header" action="" method="get">
            {if $no_read > 0}
            <div class="info">{$t.m_new1} {$no_read} {$t.m_new2}</div>
            {/if}
            <select name="order" onchange="submit();" data-placeholder = "{$t.all}">
                <option value="">{$t.all}</option>
                {foreach $sorting as $item}
                <option {if $smarty.get.order == $item.id}selected{/if} value="{$item.id}">{$item.name}</option>
                {/foreach}
            </select>
        </form>
        <div class="content">{$items}</div>
        {*<div class="footer">*}
            {*<a class="btn icons-png-red icons-around" href="#">*}
                {*<span>Більше повідомлень</span>*}
            {*</a>*}
        {*</div>*}
    </div>
</div>
{if $items == ''}
<section class="l-page-message" style="margin-top: -100px;">
    <div class="container">
        <p>{$t.no_messages}</p>
    </div>
</section><!-- .l-title-page-->
{/if}