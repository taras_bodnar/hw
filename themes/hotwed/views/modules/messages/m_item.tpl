{*{$item.id}*}
<div class="item messages-item {if $item.is_read == 0 && $item.is_out == 0}new{/if}" id="msg-{$item.id}" {* onclick='self.location.href="50;p={$item.user_id}"'*}>
    <div class="item-header">
        <div class="date-user">
            <div class="name">
                {if $item.users_group_id != 3}<a href="45;p={$item.user_id}">{/if}
                    {$item.username}
                {if $item.users_group_id != 3}</a>{/if}
            </div>
            <div class="directions"><a href="{$item.content_id}">{$item.group_name}</a></div>
            <div class="email"><i class="icon-mail"></i><span>{$item.email}</span></div>
        </div>
        <div class="mark">{$t.new}</div>
        <div class="control">
            <a href="#" class="m-star {if $item.important}important{/if} set-important tooltip-top" data-id="{$item.id}"
               title="{$t.set_important}">
                <i class="icon-star_1"></i>
            </a>
            <div class="remove">
                <a href='javascript:;' data-text="{$t.confirm_d_msg}" data-id="{$item.id}" data-y="{$t.b_y}" data-n="{$t.b_n}" class="delete-msg"><i class="icon-clancel_circle"></i></a>
            </div>
        </div>
    </div>
    <div class="item-content"  data-href="50;p={$item.user_id}">
        <div class="info-message">
            <div class="info-mark">
                {if $item.is_out}
                    <span>{$t.u_out}</span>
                    <i class="icon-arrow_big_right"></i>
                    {else}
                    <span>{$t.u_in}</span>
                    <i class="icon-arrow_big"></i>
                {/if}
            </div>
            <ul class="date-time">
                <li>
                    <i class="icon-calendar"></i>
                    <span>{$item.dd}</span>
                </li>
                <li>
                    <i class="icon-clok"></i>
                    <span>{$item.hh}</span>
                </li>
            </ul>
        </div>
        <div class="text">
            <p>{$item.message}</p>
        </div>
    </div>

</div><!-- .item -->