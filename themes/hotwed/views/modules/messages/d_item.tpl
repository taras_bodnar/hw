<div class="item {if $item.is_out}out{/if}">
    <div class="item-midel">
        {if $item.is_out == 0}
        <div class="item-header">
            {if $item.users_group_id != 3}
                <a href="45;p={$item.user_id}" class="name">{$item.username}</a>
                {else}
                <div class="name">{$item.username}</div>
            {/if}
            <div class="phone">
                <i class="icon-phone"></i>
                <span>{$item.phone}</span>
            </div>
            <div class="email">
                <i class="icon-mail"></i>
                <span>{$item.email}</span>
            </div>
        </div>
        {/if}
        <div class="item-content">
            <div class="text">
                <p>{$item.message}</p>
            </div>
        </div>

        <div class="info-message">
            <div class="info-mark">
                <i class="{if $item.is_out}icon-arrow_big{else}icon-arrow_big_right{/if}"></i>
            </div>
            <ul class="date-time">
                <li>
                    <i class="icon-calendar"></i>
                    <span>{$item.dd}</span>
                </li>
                <li>
                    <i class="icon-clok"></i>
                    <span>{$item.hh}</span>
                </li>
            </ul>
        </div>
    </div>
</div><!-- .item -->