<div class="frame-crumbs">
    <div class="crumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
        <div class="container">
            <ul class="items items-crumbs">
                {foreach $items as $i=>$item}
                    {if $i != $tc}
                        <li class="btn-crumb">
                            <a href="{if $item.url}{$item.url}{else}{$item.id}{/if}" title="{$item.title}" typeof="v:Breadcrumb" style="top: -2px;">
                                <span class="text-el">{$item.name}<span class="divider"><span class="icon-two-arr"></span></span></span>
                            </a>
                        </li>
                        {else}
                        <li class="btn-crumb">
                            <a typeof="v:Breadcrumb" class="disabled">
                                <span class="text-el">{$item.name}</span>
                            </a>
                        </li>
                    {/if}
                {/foreach}
            </ul>
        </div>
    </div>
</div>