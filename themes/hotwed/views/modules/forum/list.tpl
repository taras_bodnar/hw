<div class="content">

    {if $items}
        {$items}
    {else}
        {$t.articles_empty}
    {/if}

</div>
{if $t_total>2}
<div class="footer">
    {*<div class="col-3">*}
        {*<a class="btn icons-png-red icons-around b-more-workers" data-p="1" href="vykonavtsi">*}
            {*<span>Завантажити ще </span>*}
        {*</a>*}
    {*</div>*}
    <div class="col-9 m-pagination" id="pagination">
        <ul>
            {$pagination}
        </ul>
    </div>
</div>
{/if}