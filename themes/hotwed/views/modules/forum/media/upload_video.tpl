<div class="pop-up-middle">   
    <form id="uploadVideo" action="ajax/Forum/uploadVideo" method="post">
        <h2>{$t.upload_video_title}</h2> 

        <div class="row input-box">
            <input style="width: 100%;" type="text" name="url"
                   placeholder="напр: https://www.youtube.com/watch?v=pACPVYIldmQ" required/>
            <input type="hidden" name="forum_id" value="{$forum_id}">
        </div>
        <div class="row"><br></div>
        <div class="row footer">
            <div class="row">
                <button class="btn red" type="submit">{$t.b_upload}</button>
            </div>
        </div>
        <input type="hidden" name="skey" value="{$skey}">
        <input type="hidden" name="process" value="1">
    </form>
    {*<script>new Users()</script>*}   
</div> 