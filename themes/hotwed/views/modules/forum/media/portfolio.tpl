<div class="add-photos">
    <div class="container">

        <section class="l-page-portfolio">
            <div class="container">
                <div class="header">
                    {if $c<35}
                        <form id="formUploadImages" class="btn-group" enctype="multipart/form-data" action="ajax/Forum/uploadImages" method="post">
                            <a href="#" class="btn icons-png-red plus-circle b-pf-add-images"><span>{$t.b_add_photo}</span></a>
                            <a href="#" class="btn icons-png-blue plus-circle b-pf-add-video-forum" data-forum_id="{$id}"><span>{$t.b_add_video}</span></a>
                            <input type="file" name="images[]" multiple style="display: none" id="selImg">
                            <input type="hidden" name="skey" value="{$skey}">
                            <input type="hidden" name="forum_id" value="{$id}">
                            <button type="submit" style="display: none" id="bSubmit2"></button>
                        </form>
                        <div class="progress" style="display: none;">
                            <div class="bar"></div>
                            <div class="percent">0%</div>
                        </div>
                        <div id="status"></div>
                        <div class="response"></div>
                    {else}
                        <p>{$t.max_file_portolio}</p>
                    {/if}
                </div>
                <div class="content" id="pf_content">
                    {$items}
                </div>
            </div>
        </section> <!-- .l-title-page-->

        <style>
            .progress { position:relative; width:400px; border: 1px solid #ddd; padding: 1px; border-radius: 3px; }
            .bar { background-color: #B4F5B4; width:0%; height:20px; border-radius: 3px; }
            .percent { position:absolute; display:inline-block; top:3px; left:48%; }
        </style>
    </div>
</div>