{*<pre>{print_r($info)}*}
<section class="l-people-list forum-page">
    <div class="container">
        [[mod:Forum::getCategory]]
            <div class="title">
                {if $article.event==1}
                    <p>{$t.event_take_place} </p>
                {/if}
                {if isset($smarty.session.app.user.id) && $ui.id!=$smarty.session.app.user.id}
                    <div class="btn-group">
                        <a class="follow no-reload {if $info.follow=="follow"}checked{/if}"
                           onclick="Forum.follow({$article.id},{$smarty.session.app.user.id},'follow',$(this))"
                           data-text="{if $info.follow=="follow"}{$t.follow}{else}{$t.following}{/if}"
                           href="">{if $info.follow=="follow"}{$t.following}{else}{$t.follow}{/if}</a>
                        <a class="ignore no-reload {if $info.follow=="ignore"}checked{/if}"
                           onclick="Forum.follow({$article.id},{$smarty.session.app.user.id},'ignore',$(this))"
                           data-text="{if $info.follow=="ignore"}{$t.ignore}{else}{$t.ignoring}{/if}"
                           href="#">{if $info.follow=="ignore"}{$t.ignoring}{else}{$t.ignore}{/if}</a>
                    </div>
                {/if}
            </div>
    </div>
    {if $article.event==1}
        <div class="event-details">

            <div class="container">
                <ul>
                    <li class="col-4">{$t.event_address}: {$info.event_town}, {$info.event_place}</li>
                    <li class="col-4">{$t.event_start}: {$info.start_date} о {$info.hhs}:{$info.iis}</li>
                    <li class="col-4">{$t.event_end}: {$info.end_date} о {$info.hhe}:{$info.iie}</li>
                </ul>
            </div>

        </div>
        <div class="participants">
            <div class="container">
                <div class="title">
                    <p>{$t.event_registered} <span class="participant-count">{count($part)}</span> {$t.people}</p>
                </div>
                <div id="part-items">
                    {foreach $part as $item}
                        <a class="item no-reload" id="participant-{$item.id}" href="#">
                        <span class="photo">
                            <img src="{if !empty($item.avatar)}{$item.avatar}{else}{$template_url}assets/img/avatar.jpg{/if}">
                        </span>
                            <span class="name">{$item.username}</span>
                        </a>
                    {/foreach}
                </div>
                {if $smarty.session.app.user.id}
                    <div class="button">
                        <a class="btn red no-reload takePart" onclick="Forum.participants({$article.id},{$smarty.session.app.user.id})" href="#">
                            {if $takePart>0}
                                {$t.dont_go}
                            {else}
                                {$t.take_part}
                            {/if}
                        </a>
                    </div>
                {/if}
            </div>
        </div>
    {/if}

    <div class="container">
        <div class="content page-inside">
            <div class="forum-item">
                <div class="description">
                    <p>{nl2br($article.content)}</p>
                    {if !empty($img)}
                        <div class="photos-line">
                            {foreach $img as $image}
                                <a class="photo pf-items {if $image.type=='video'} video fancybox.iframe{/if}" rel="group{$image.id}" href="{$image.big}">
                                    <img data-src="{$image.thumb}" src="{$image.thumb}" alt=""/>
                                </a>
                            {/foreach}
                        </div>
                    {/if}
                </div>
                <div class="general">
                    <span class="name">{$ui.name} {$ui.surname}</span>
                    <span class="date">{$article.createdon}</span>
                </div>
            </div>
            [[mod:Forum::comments]]

        </div>
    </div>

</section>


