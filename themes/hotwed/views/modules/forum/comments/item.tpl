{*<pre>{print_r($item)}*}
{function name=renderComment}
    <div class="item item-comment item-{$item.id} comments-item level-{$offset}">
        <div class="name"><a class="no-reload" href="#">{$item.username}</a></div>
        <div class="date">{$item.date}</div>
        <div class="item-content">{nl2br($item.content)}</div>
        {*<pre>{print_r($item.portfolio)}*}

        <div class="forum-item">
            <div class="description">
                <div class="photos-line">
                    {foreach $item.portfolio as $portfolio}
                        <a class="photo pf-items " rel="group" href="uploads/forum_comment/{$portfolio.forum_comment_id}/big_{$portfolio.path}">
                            <img data-src="uploads/forum_comment/{$portfolio.forum_comment_id}/thumb_{$portfolio.path}" src="uploads/forum_comment/{$portfolio.forum_comment_id}/thumb_{$portfolio.path}" alt="">
                        </a>
                    {/foreach}
                </div>
            </div>
        </div>
        {if $offset<2}
            <a class="sub-review" data-forum_id="{$item.forum_id}" data-user_id="{$smarty.session.app.user.id}" href="">{$t.forum_answer}</a>
        {/if}
        <div class="form" style="display:none;">
            <form class="m-pop-up-message pop-up-middle commentsForm" method="post" action="ajax/Forum/createComment">
                <div class="text">
                    <div class="row input-box">
                        <textarea required="" name="data[content]" id="data_message"></textarea>
                        <input type="hidden" name="skey" value="{$skey}"/>
                        <input type="hidden" name="data[forum_id]" value="{$item.forum_id}"/>
                        <input type="hidden" name="data[parent_id]" value="{$item.id}"/>
                        <input type="hidden" class="comment_id" name="data[comment_id]" value=""/>
                    </div>
                    <div class="row">
                        <div class="btn-group">
                            <button type="submit" id="bSubmit" class="btn red">{$t.b_send}</button>
                        </div>
                    </div>
                </div>
            </form>
            <form  class="btn-group formUploadSubCommentImages" enctype="multipart/form-data" action="ajax/Forum/uploadCommentImages" method="post">
                <a href="#" class="btn icons-png-red plus-circle b-pf-add-comment-images"><span>Додати фото</span></a>

                <input type="file" name="images[]" multiple="" style="display: none" id="selImg">
                <input type="hidden" name="skey" value="{$skey}">
                <input type="hidden" name="forum_comment_id" class="comment_id" value="">
                <button type="submit" style="display: none" class="bSubmit2"></button>
            </form>
            <div class="progress" style="display: none;">
                <div class="bar"></div>
                <div class="percent">0%</div>
            </div>
            <div id="status"></div>
            <div class="response"></div>
            <div class="content" id="pf_content">
            </div>
        </div>

        {if $item.isfolder}
            {foreach $item.items as $c}
                {call renderComment item=$c offset=$offset+1}
            {/foreach}
        {/if}
    </div>
    <!-- .item -->
{/function}


{call renderComment item=$item offset=0}
<script src="{$template_url}assets/js/vendor/jquery-1.11.2.min.js"></script>
<script src="{$template_url}assets/js/vendor/jquery-ui.min.js"></script>
<script src="{$template_url}assets/js/vendor/jquery.form.min.js"></script>
<script src="{$template_url}assets/js/vendor/jquery.fancybox.js"></script>
{literal}
    <script>
        var is = true;
        $(document).ready(function () {

                $(".sub-review").on("click",function(e){
                    var element = $(this);
                    if(is) {
                        var forum_id = $(this).data('forum_id');
                        var user_id = $(this).data('user_id');
                        if(user_id>0) {
                            $.ajax({
                                url: 'ajax/Forum/createCommentAuto',
                                type: 'post',
                                data: {
                                    skey: SKEY,
                                    forum_id: forum_id,
                                    user_id: user_id
                                },
                                success: function (d) {
                                    element.next().find('.comment_id').val(d);
                                },
                                beforeSend: function () {
                                    var bar = element.next().find('.bar'),
                                            percent = element.next().find('.percent'),
                                            status = element.next().find('#status'),
                                            formUploadCommentImages = element.next().find('.formUploadSubCommentImages'),
                                            inpSelImg = element.next().find('#selImg'),
                                            progress = element.next().find('.progress');

                                    formUploadCommentImages.ajaxForm({
                                        dataType: 'json',
                                        beforeSend: function () {
//                                            progress.show();
////            status.empty();
//                                            var percentVal = '0%';
//                                            bar.width(percentVal);
//                                            percent.html(percentVal);
                                            element.next().find('.bSubmit2').attr('disabled','disabled');
                                        },
//                                        uploadProgress: function (event, position, total, percentComplete) {
//                                            var percentVal = percentComplete + '%';
//                                            bar.width(percentVal);
//                                            percent.html(percentVal);
//                                            //console.log(percentVal, position, total);
//                                        },
                                        success: function (d) {
//                                            progress.hide();
//                                            var percentVal = '100%';
//                                            bar.width(percentVal);
//                                            percent.html(percentVal);

                                            $(d.images).each(function (i, e) {
                                                $(e).appendTo(element.next().find('#pf_content'));
                                            });
                                            if (d.error) {
                                                myAlert(d.error);
                                            }
//            pfSortable();
                                        }
                                    });

                                    $('.b-pf-add-comment-images').click(function (e) {
                                        e.preventDefault();

                                        inpSelImg.click();
                                    });


                                    inpSelImg.change(function () {

                                        element.next().find('.bSubmit2').click();
                                    });
                                }
                            });
                            is = false;
                            element.next().stop().slideToggle("slow");
                        } else {

                            $('.login-btn').click();
                        }
//                    e.preventDefault();
                    }

                    return false;
                });

            $('.commentsForm').ajaxForm({
                dataType: 'json',
                beforeSend: function () {
                    processing = true;
                    if (!iOnline()) {
                        $('.login-btn').click();
                        return false;
                    }
                },
                success: function (d) {
                    processing = false;
                    $('.commentsForm').trigger('reset');
                    $("div.form").hide();
                    $("#pf_content").html("");

                    if ($("#comments").find(".item-" + d.parent_id).find(".item-comment:first").length) {
                        $("#comments").find(".item-" + d.parent_id).find(".item-comment:first").before(d.item);
                    } else {
                        $("#comments").find(".item-" + d.parent_id).append(d.item);
                    }
                    //myAlert(d.m);
                    if($("#comments").find(".item-comment").hasClass('level-0')){
                        $("#comments").find(".item-comment").addClass('level-1');
                    }else if($("#comments").find(".item").hasClass('level-1')){
                        $("#comments").find(".item-comment").addClass('level-2');
                    } else {
                        $("#comments").find(".item-comment").addClass('level-3');
                    }
                }
            });
        });
    </script>
{/literal}
