<div class="add-photos">
    <div class="container">

        <section class="l-page-portfolio">
            <div class="container">
                <div class="header">
                    {if $c<35}
                        <form id="formUploadCommentImages" class="btn-group" enctype="multipart/form-data" action="ajax/Forum/uploadCommentImages" method="post">
                            <a href="#" class="btn icons-png-red plus-circle b-pf-add-images"><span>{$t.b_add_photo}</span></a>
                            {*<a href="#" class="btn icons-png-blue plus-circle b-pf-add-video-forum" data-forum_comment_id="{$id}"><span>{$t.b_add_video}</span></a>*}
                            <input type="file" name="images[]" multiple style="display: none" id="selCommentImg">
                            <input type="hidden" name="skey" value="{$skey}">
                            <input type="hidden" name="forum_comment_id" value="{$id}">
                            <button type="submit" style="display: none" id="bSubmit2"></button>
                        </form>
                        <div class="progress" style="display: none;">
                            <div class="bar"></div>
                            <div class="percent">0%</div>
                        </div>
                        <div id="status"></div>
                        <div class="response"></div>
                    {else}
                        <p>{$t.max_file_portolio}</p>
                    {/if}
                </div>
                <div class="content" id="pf_content_comment">
                    {$items}
                </div>
            </div>
        </section> <!-- .l-title-page-->

        <style>
            .progress { position:relative; width:400px; border: 1px solid #ddd; padding: 1px; border-radius: 3px; }
            .bar { background-color: #B4F5B4; width:0%; height:20px; border-radius: 3px; }
            .percent { position:absolute; display:inline-block; top:3px; left:48%; }
        </style>
    </div>
</div>
<script src="{$template_url}assets/js/vendor/jquery-1.11.2.min.js"></script>
<script src="{$template_url}assets/js/vendor/jquery-ui.min.js"></script>
<script src="{$template_url}assets/js/vendor/jquery.form.min.js"></script>
<script src="{$template_url}assets/js/vendor/jquery.fancybox.js"></script>
{literal}
<script>
    var bar = $('.bar'),
            percent = $('.percent'),
            status = $('#status'),
            formUploadCommentImages = $('#formUploadCommentImages'),
            inpSelImg = $('#selCommentImg'),
            progress = $('.progress');

    formUploadCommentImages.ajaxForm({
        dataType: 'json',
        beforeSend: function() {
            progress.show();
//            status.empty();
            var percentVal = '0%';
            bar.width(percentVal);
            percent.html(percentVal);
        },
        uploadProgress: function(event, position, total, percentComplete) {
            var percentVal = percentComplete + '%';
            bar.width(percentVal);
            percent.html(percentVal);
            //console.log(percentVal, position, total);
        },
        success: function(d) {
            var percentVal = '100%';
            bar.width(percentVal);
            percent.html(percentVal);
            progress.hide();
            $(d.images).each(function(i,e){
                $(e).appendTo($('#pf_content_comment'));
            });
            if(d.error) {
                myAlert(d.error);
            }
//            pfSortable();
        }
    });

    $('.b-pf-add-images').click(function(e){
        e.preventDefault();

        inpSelImg.click();
    });


    inpSelImg.change(function(){
        $('#bSubmit2').click();
    });

</script>
{/literal}