{*{print_r($comments_total)}*}
<div class="l-user-page">
    <div class="reviews">
        <div class="reviews-header">
            {if $comments_total!=0}
                <h2>{$t.comments_title}<span>({$comments_total})</span></h2>
            {/if}
            {if isset($smarty.session.app.user.id)}
                <a class="btn icons-png-red icons-massage no-reload" id="addForumComment"
                   data-user_id="{$smarty.session.app.user.id}"
                   href="#" data-id="{$article.id}"><span>{$t.comments_btn_forum}</span></a>
            {/if}
        </div>
        <div class="reviews-content" id="comments">
            {$comments_items}
        </div>
        {if $comments_show_more}
            <div class="reviews-footer">
                <a class="btn icons-png-red icons-around forum-comments-more" data-id="{$article.id}" href="javascript:;">
                    <span>{$t.comments_more}</span>
                </a>
            </div>
        {/if}
    </div>
</div>