{*<pre>{print_r($article)}*}
<form class="header" action="">
    <ul class="sorting-list {if $smarty.session.app.user.id && $groupId==2}col-9{/if}">
        <li {if $page.id==206}class="active"{/if}>
            <a href="206">{$t.all_forum_cat}</a>
        </li>
        {foreach $items as $item}
            <li {if $item.id==$page.id || $item.id==$article.cid}class="active"{/if}>
                <a href="{$item.id}">{$item.name}</a>
            </li>
        {/foreach}
        {if isset($smarty.session.app.user.id)}
            <li class="{if $page.id==208}active{/if} highlighted">
                <a href="208">{$t.following}</a>
            </li>
            <li class="{if $page.id==209}active{/if} highlighted">
                <a href="209">{$t.ignoring}</a>
            </li>
            {if $groupId==2}
                <li class="{if $page.id==210}active{/if} highlighted">
                    <a href="210">{$t.my_article}</a>
                </li>
            {/if}
        {/if}
    </ul>
    {*{$groupId}*}
    {if $groupId==2}
        <div class="col-3">
            <a class="btn icons-png-blue" href="212">{$t.new_theme}</a>
        </div>
    {/if}
</form>