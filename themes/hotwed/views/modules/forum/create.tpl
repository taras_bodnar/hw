<section class="l-page-personal-information l-people-list forum-page">
    <div class="container">
        <form class="header" action="" style="margin: -45px 0 35px 0;">
            <ul class="sorting-list">
                {foreach $type as $item}
                    <li {if $item.id==$page.id}class="active"{/if}>
                        <a href="{$item.id}">{$item.name}</a>
                    </li>
                {/foreach}
            </ul>
        </form>
        <div class="row">
            <div class="m-form-box">
                <div class="column-l">
                    <form method="post" id="forumProcess" name="forumProcess" action="ajax/Forum/process/{$id}">
                        <div class="row group-input">
                            <label class="input-label">{$t.article_name}</label>

                            <div class="input">
                                <input name="data[name]" value="{$info.name}" required type="text">
                            </div>
                        </div>
                        {if $page.id!=213}
                        <div class="row group-input">
                            <label class="input-label">{$t.forum_cat}</label>

                            <div class="input">
                                <select name="data[cid]" required>
                                    {foreach $category as $item}
                                        {if $item.id==217}{continue}{/if}
                                        <option value="{$item.id}">{$item.name}</option>
                                    {/foreach}
                                </select>
                            </div>
                        </div>
                        {/if}
                        <div class="row group-input">
                            <label class="input-label">{$t.article_text}</label>

                            <div class="input">
                                <textarea name="data[content]" required>{$info.content}</textarea>
                            </div>
                        </div>
                        {if $page.id==213}
                            <div class="row group-input">
                                <label class="input-label">{$t.article_town}</label>

                                <div class="input">
                                    <select name="data[event_town]" required>
                                        {foreach $town as $item}
                                            <option {if $info.event_town==$item.name}selected{/if} value="{$item.name}">{$item.name}</option>
                                        {/foreach}
                                    </select>
                                </div>
                            </div>
                            <div class="row group-input">
                                <label class="input-label">{$t.event_address}</label>

                                <div class="input">
                                    <input name="data[event_place]" value="{$info.event_place}" required type="text">
                                </div>
                            </div>
                            <div class="row group-input picker-row">
                                <label class="input-label">{$t.event_start}</label>

                                <div class="input date-picker">
                                    <input autocomplete="off" name="start_date" class="forumDatepicker" value="{$info.event_start_date|date_format:"%Y-%m-%d"}"
                                           required type="text">
                                         <span class="add-on">
                                            <i class="icon-calendar"></i>
                                         </span>
                                </div>
                                <div class="input time-picker">
                                    <input autocomplete="off" name="start_time" required data-format="hh:mm"
                                           value="{$info.event_start_date|date_format:"%H:%M"}"
                                           type="text"/>
                                         <span class="add-on">
                                             <i class="icon-clok"></i>
                                         </span>
                                </div>
                            </div>
                            <div class="row group-input picker-row">
                                <label class="input-label">{$t.event_end}</label>

                                <div class="input date-picker">
                                    <input autocomplete="off" name="end_date" class="forumDatepicker" required
                                           value="{$info.event_end_date|date_format:"%Y-%m-%d"}"
                                           type="text">
                                         <span class="add-on">
                                             <i class="icon-calendar"></i>
                                         </span>
                                </div>
                                <div class="input time-picker">
                                    <input autocomplete="off" name="end_time" required data-format="hh:mm"
                                           value="{$info.event_end_date|date_format:"%H:%M"}"
                                           type="text"/>
                                         <span class="add-on">
                                            <i class="icon-clok"></i>
                                         </span>
                                </div>
                            </div>

                            <input type="hidden" name="data[event]" value="1">
                            <input type="hidden" name="data[cid]" value="217">
                        {/if}

                        <div class="row group-input" style="margin-top: 15px;">
                            <label class="input-label">{$t.event_permission}</label>

                            <div class="input">

                                <select name="data[status]">
                                    <option value="publish">{$t.event_perm_all}</option>
                                    <option value="onlyworker">{$t.only_regis}</option>
                                </select>
                            </div>
                        </div>
                        <input type="hidden" name="id" value="{$id}">
                    </form>
                </div>
            </div>
        </div>
    </div>
    {$media}
    <div class="button">
        <button form="forumProcess" id="bSubmit" class="btn red" type="submit">{$t.b_save}</button>
    </div>
</section>