{*<pre>{print_r($item)}*}
<div id="forum-item-{$item.id}" class="forum-item">
        <div class="title">
            <a href="forum/article/{$item.id}">{$item.name}</a>
            {if isset($smarty.session.app.user.id) && $item.user_id!=$smarty.session.app.user.id}
                <div class="btn-group">
                    <a class="follow no-reload {if $item.follow=="follow"}checked{/if}"
                       onclick="Forum.follow({$item.id},{$smarty.session.app.user.id},'follow',$(this))"
                       data-text="{if $item.follow=="follow"}{$t.follow}{else}{$t.following}{/if}"
                       href="">{if $item.follow=="follow"}{$t.following}{else}{$t.follow}{/if}</a>
                    <a class="ignore no-reload {if $item.follow=="ignore"}checked{/if}"
                       onclick="Forum.follow({$item.id},{$smarty.session.app.user.id},'ignore',$(this))"
                       data-text="{if $item.follow=="ignore"}{$t.ignore}{else}{$t.ignoring}{/if}"
                       href="#">{if $item.follow=="ignore"}{$t.ignoring}{else}{$t.ignore}{/if}</a>
                </div>
            {/if}
        </div>
        <div class="description">
            <p>{nl2br($item.content|mb_substr:0:200)}</p>
            {*<pre>{print_r($item.img)}*}
            {if !empty($item.img)}
                <div class="photos-line">
                    {foreach $item.img as $img}
                        <a class="photo pf-items {if $img.type=='video'} video fancybox.iframe{/if}" rel="group{$item.id}" href="{$img.big}" href="#" rel="group123">
                            <img data-src="{$img.thumb}" src="{$img.thumb}" alt=""/>
                        </a>
                    {/foreach}
                </div>
            {/if}
        </div>
        <div class="bt-wrap">
            <div class="general" style="float: left">
                <span class="name"><a class="link-def" href="45;p={$item.user_id}">{$item.username}
                </a></span>
                <span class="date">{$item.date} o {$item.hh}:{$item.ii}</span>
                <span class="views">{$item.t_comment}</span>
            </div>
            {if $page.id==210}
                {*{$item.event}*}
                <div class="edit" style="float: right">
                    <a href="{if $item.event==1}213{else}212{/if};l={$page.languages_id};id={$item.id}" class="btn red">{$t.edit_event}</a>
                    <a href="#" onclick="Forum.deleteArticle({$item.id})" class="btn red no-reload">{$t.delete_event}</a>
                </div>
            {/if}
            <div style="clear: both"></div>
        </div>
    </div>