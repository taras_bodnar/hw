<section class="list-photo">
    <div class="container">
        <div class="add-photo">
            <a href="8" class="btn icons-png-red plus-circle add-photo-to-portfolio"
               data-user_group_id="{$groupId}"
               data-user_id="{$smarty.session.app.user.id}"
               data-text="{$t.be_costumer}"
            >
                <span>{$t.addPhotoVideo}</span>
            </a>
        </div>
        <div class="inner-list" id="photo-day-list">
            {$items}
        </div>
        <div class="footer">
            {*{if $total>0}*}
                {*<div class="col-3">*}
                    {*<a class="btn icons-png-red icons-around b-more-images" data-p="1" href="vykonavtsi">*}
                        {*<span>{$t.b_catalog_more}</span>*}
                    {*</a>*}
                {*</div>*}
            {*{/if}*}
            <div class="col-9 m-pagination" id="pagination">
                {$pagination}
            </div>

        </div>
    </div>
</section>
