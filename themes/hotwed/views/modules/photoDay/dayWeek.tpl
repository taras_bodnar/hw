<section class="l-bl-day">
    <div class="container">
        <div class="row">
            <div class="big-bl">
                {*<pre>{print_r($photoOfDay)}</pre>*}
                {if !empty($photoOfDay)}
                    <div class="item wrap-photo">
                        <h2>
                            <img src="{$template_url}/assets/img/icons/photo.svg" alt="">
                            Фото дня
                        </h2>

                        <div class="item-midle">
                            <div class="img-label tipsy" original-title="{$t.photoDay}"></div>

                            <div class="img cut-height" style="background-image: url('{$photoOfDay.big}')">
                                {*<img src="" alt="">*}
                            </div>
                            <a href="photo/{$photoOfDay.id}" class="wrap-link" title="">
                                <div class="middle">
                                    <div class="table">
                                        <div class="table-cell">
                                            <div class="title">{$photoOfDay.username}</div>
                                            <div class="info">
                                                <span class="like tipsy" original-title = "{$t.like_quantity}">{$photoOfDay.t_like}</span>
                                                <span class="comment tipsy" original-title = "{$t.qunatity_like}">{$photoOfDay.t_comments}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                {/if}
                {*<pre>{print_r($videoOfWeek)}*}
                {if !empty($videoOfWeek)}
                    <div class="item wrap-video">
                        <h2>
                            <img src="{$template_url}/assets/img/icons/video.svg" alt="">
                            {$t.VideoWeek}
                        </h2>
                        <div class="item-midle">
                            <div class="img-label tipsy" original-title="{$t.VideoWeek}"></div>

                            <div class="item-image">
                                <a class="pf-items  video fancybox.iframe" href="{$videoOfWeek.big}">
                                            <span>
                                                 <img class="" alt="" src="{$videoOfWeek.thumb}" style="display: block;">
                                            </span>
                                </a>
                            </div>
                        </div>
                    </div>
                {/if}
            </div>
            {*<pre>{print_r($more)}*}
            {if !empty($more)}
                <div class="day-slider">
                    <span class="a-prev arrow"></span>
                    <div  id="day-slider">
                        {foreach $more as $item}
                            <a href="photo/{$item.id}">
                                <div class="item" style = "background: url('{$item.big}')">
                                </div>
                            </a>
                        {/foreach}
                    </div>
                    <span class="a-next arrow"></span>
                </div>
            {/if}
        </div>
    </div>

</section>