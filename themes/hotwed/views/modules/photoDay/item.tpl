{*<pre>{print_r($item)}*}
<div class="h-item-img">
    <div class="item-midle">
        {*<div class="img-label tipsy" original-title="Фото дня"></div>*}
        <a href="{$item.big}" rel="group" class="full pf-items {if $item.type=='video'}  fancybox.iframe{/if}"></a>
        <div class="img">
            <img src="{$item.thumb}" alt="">
        </div>
        <a href="photo/{$item.id}" class="wrap-link" title="">
            <div class="middle">
                <div class="table">
                    <div class="table-cell">
                        <div class="title">{$item.username}</div>
                        <div class="info">
                            <span class="like tipsy" original-title="{$t.like_quantity}">{$item.like_t}</span>
                            <span class="comment tipsy" original-title="{$t.qunatity_like}">{$item.comment_t}</span>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
</div>
{literal}
    <script>
        $(document).ready(function(){

            var wp = false;
            $('.b-more-images').click(function(e){
                //console.log($_GET.filter);
                var $this = $(this);
                e.preventDefault();
                if(wp) return false;
                wp=true;
                $.ajax({
                    url: 'ajax/PhotoDay/more',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        p: $this.data('p'),
                        id: window.page.id,
                        order: $_GET.order,
                        filter: $_GET,
                        skey: SKEY
                    },
                    success: function(d){
                        if(d.t == 0 || d == '') $this.hide();
                        $(d.items).each(function(i,e){
                            $('#photo-day-list').append(e);
                        });
                        //$('#catalogWorkers').append(d.items);
                        $('#pagination').html(d.pagination);
                        $this.data('p', d.p);

                        doLazy();
                    }
                });
            });
        });
    </script>
{/literal}