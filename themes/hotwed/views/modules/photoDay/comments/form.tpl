<form class="m-pop-up-message pop-up-middle commentsForm" method="post" action="ajax/PhotoDay/createComment" id="commentsForm">
    <div class="text">
        <h2>{$t.comments_info}</h2>
        <div class="row input-box">
            <textarea required="" name="data[content]" id="data_message"></textarea>
            <input type="hidden" name="skey" value="{$skey}" />
            <input type="hidden" name="data[users_uploads_id]" value="{$id}" />
        </div>
        <div class="row">
            <div class="btn-group">
                <button type="submit" id="bSubmit" class="btn red">{$t.b_send}</button>
            </div>
        </div>
    </div>
</form>
