<section class="reviews">

    <div class="container">
        <div class="reviews-header">
            {if $comments_total!=0}
                <h2>{$t.comments_title}<span>({$comments_total})</span></h2>
            {/if}
            {*{if $smarty.session.app.user.users_group_id==3 || !isset($smarty.session.app.user.id)}*}
                <a class="btn icons-png-red icons-massage" id="addImgComment" data-user_id="{$smarty.session.app.user.id}" href="#" data-id="{$photo.id}">
                    <span>{$t.comments_btn}</span>
                </a>
            {*{/if}*}
        </div>
        <div class="reviews-content" id="img-comments">
            {$comments_items}
        </div>
        {if $comments_show_more}
            <div class="reviews-footer">
                <a class="btn icons-png-red icons-around img-comments-more" data-id="{$photo.id}" href="javascript:;">
                    <span>{$t.comments_more}</span>
                </a>
            </div>
        {/if}
    </div>
</section>