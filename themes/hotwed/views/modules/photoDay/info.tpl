{*<pre>{print_r($photo)}*}
<section class="l-title l-title-user-page">
    <div class="row">
        <div class="container">
            <h1><a style="color: #fff;" href="45;p={$ui.id}">{$ui.name} {$ui.surname}</a></h1>
            <div class="directions">
                <a href="{$ui.content_id}">{$ui.group_name}</a>
            </div>
            <div class="town">
                {foreach $ui.city as $city}
                    <a href="3;filter[city_id]={$city.id}">
                        <span>{$city.name}</span>
                    </a>
                {/foreach}
            </div>
        </div>
    </div>
</section><!-- .l-title-page-->
{*<pre>{print_r($prevNext)}*}
<section class="photo-slider">
    <div class="container">
        <div class="s-photo-day day-info">
            <div class="wrap-item">
                <div class="item">
                    {if ($prevNext.nextid)}
                        <a href="photo/{$prevNext.nextid}">
                    <span class="a-prev arrow">
                    </span>
                        </a>
                    {/if}

                    {if $photo.day_check==1}
                        <div class="img-label tipsy" original-title="{$t.best_photo_for} {$photo.photo_day}"></div>
                    {/if}
                    {if $photo.type=='video'}
                        <iframe src="{$photo.big_path}" frameborder="0"></iframe>
                        {else}
                        <div class="photo-container-big-new" style="background-image: url('{$big_path}')"></div>
                        {*<img src="" alt="">*}
                    {/if}

                    {if ($prevNext.previd)}
                        <a href="photo/{$prevNext.previd}">
                            <span class="a-next arrow"></span>
                        </a>
                    {/if}
                </div>
            </div>
        </div>
    </div>
</section>

<section class="photo-share">
    <div class="container">
        <div class="row">
            <div class="share42init" data-top1="150" data-top2="20" data-margin="0">
            </div>
            {if $photo.day_check!=1}
                <div data-user="{$photo.id}" {if $already_like>0}original-title="{$t.you_already_like}"{/if}
                     data-client = "{$smarty.session.app.user.id}" class="{if $already_like>0}tipsy{/if} likes photo_like">
                    {$t.ilike}
                    <span class="count">{$like_t}</span>
                </div>
            {/if}
        </div>
    </div>
</section>