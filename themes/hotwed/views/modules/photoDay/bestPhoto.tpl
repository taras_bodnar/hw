{*<pre>{print_r($best)}*}
{if !empty($best)}
<section class="best-photo">
    <div class="container">
        <div class="s-best-photo">
            <h2>{$t.best_work}</h2>
            <div class="btn-group">
                <a class="btn-prev animation-left">
                    <i class="icon-arrow-left"></i>
                </a>
                <a class="btn-next animation-right">
                    <i class="icon-arrow-right"></i>
                </a>
            </div>
            <div id="s-best-photo">
                {foreach $best as $item}
                    <div class="h-item-img">
                        {foreach $item as $img}
                            <div class="item-midle">
                                <a href="photo/{$img.id}" class="full"></a>
                                <div class="img">
                                    <img src="{$img.big}" alt="">
                                </div>
                                <a href="photo/{$img.id}" class="wrap-link" title="">
                                    <div class="middle">
                                        <div class="table">
                                            <div class="table-cell">
                                                <div class="title">{$img.username}</div>
                                                <div class="info">
                                                    <span class="like tipsy" original-title="{$t.like_quantity}">{$img.t_like}</span>
                                                    <span class="comment tipsy" original-title="{$t.qunatity_like}">{$img.t_comments}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        {/foreach}
                    </div>
                {/foreach}
            </div>
        </div>
    </div>
</section>
{/if}