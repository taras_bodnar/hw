
<div class="item wl-item" id="wl-{$item.id}">
    <div class="item-header">
        <div class="name"><a href="45;p={$item.id}">{$item.name} {$item.surname}</a></div>
        <div class="town">
            <a href="3;filter[city_id]={$item.city_id}">
                <span>{$item.city}</span>
            </a>
        </div>
        <div class="remove-list">
            <a href="#" class="wl-rm-item" data-id="{$item.id}"><span>{$t.wl_rm}</span><i class="icon-clancel_circle"></i></a>
        </div>
    </div>
    <div class="item-content">
        <div class="row">
            <div class="author">
                <div class="row">
                    <div class="column-l">
                        <div class="img-autor">
                            <a href="45;p={$item.id}">
                                <img src="{$item.avatar}" alt=""/>
                            </a>
                        </div>
                        <div class="number-comments">
                            <span>{$item.comments}</span><i class="icon-massage"></i>
                        </div>
                    </div>
                    <div class="column-r">
                        <div class="directions"><a href="{$item.content_id}">{$item.group_name}</a></div>
                        <div class="price-hour"><span>{$t.price_per_h}</span>{$item.price_per_hour} $</div>
                        <div class="price-day"><span>{$t.price_per_d}</span>{$item.price_per_day} $</div>
                    </div>
                </div>
            </div>
            <div class="portfolio">
                {foreach $item.img as $img}
                    <div class="img">
                        <a class="pf-items {if $img.type=='video'} video fancybox.iframe{/if}" rel="group{$item.id}" href="{$img.big}">
                            <img src="{$img.thumb}" alt=""/>
                        </a>
                    </div><!-- .img -->
                {/foreach}
            </div>
        </div>
        <div class="row write-message">
            <a href="50;p={$item.id}" class="btn icons-png-red icons-open_mail"><span>{$t.write_msg}</span></a>
        </div>
    </div>
</div><!-- .item -->