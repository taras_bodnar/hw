<section class="l-people-list l-favorite-performers">
    <div class="container">
        <form class="header" action="">
            <select name="order" onchange="submit();">
                {foreach $sorting as $item}
                    <option {if isset($smarty.get.order) && $smarty.get.order == $item.id}selected{/if} value="{$item.id}">{$item.name}</option>
                {/foreach}
            </select>
        </form>
        <div class="content" id="catalog">
            {$items}
        </div>
        <div class="footer">
            <div class="col-3">
                {if $total>0}
                    <a class="btn icons-png-red icons-around b-more-wl-workers" data-p="{$start}" href="{$root_id}">
                        <span>{$t.b_catalog_more}</span>
                    </a>
                {/if}
            </div>
            <div class="col-9 m-pagination" id="pagination">
                {$pagination}
            </div>
        </div>
    </div>
</section>