<div class="reviews">
    <div class="reviews-header">
        {if $comments_total!=0}
            <h2>{$t.comments_title}<span>({$comments_total})</span></h2>
        {/if}
        {if $smarty.session.app.user.users_group_id==3 || !isset($smarty.session.app.user.id)}
            <a class="btn icons-png-red icons-massage" id="addComment" data-user_id="{$smarty.session.app.user.id}" href="#" data-id="{$workers_id}"><span>{$t.comments_btn}</span></a>
        {/if}
    </div>
    <div class="reviews-content" id="comments">
        {$comments_items}
    </div>
    {if $comments_show_more}
        <div class="reviews-footer">
            <a data-user_id="{$workers_id}" class="btn icons-png-red icons-around comments-more" href="javascript:;">
                <span>{$t.comments_more}</span>
            </a>
        </div>
    {/if}
</div>
{*<pre>{print_r($smarty.session)}*}
{*<div class="reviews">*}
{*<div class="reviews-header">*}
{*<h2>{$t.comments_title}<span>({$comments_total})</span></h2>*}
{*{if ! $is_worker}*}
{*<a class="btn icons-png-red icons-massage" id="addComment" href="#" data-id="{$workers_id}"><span>{$t.comments_btn}</span></a>*}
{*{/if}*}
{*</div>*}
{*<div class="reviews-content" id="comments">*}
{*{$comments_items}*}
{*</div>*}
{*{if $comments_show_more}*}
{*<div class="reviews-footer">*}
{*<a class="btn icons-png-red icons-around comments-more" href="javascript:;">*}
{*<span>{$t.comments_more}</span>*}
{*</a>*}
{*</div>*}
{*{/if}*}
{*</div>*}