<form class="m-pop-up-message pop-up-middle" method="post" action="ajax/Comments/create" id="commentsForm">
    <div class="text">
        <h2>{$t.comments_info}</h2>
        <div class="row input-box">              
            <textarea required="" name="data[message]" id="data_message"></textarea>
            <input type="hidden" name="skey" value="{$skey}" />
            <input type="hidden" name="data[workers_id]" value="{$workers_id}" />
        </div> 
        <div class="row">
            <div class="btn-group">
                <button type="submit" id="bSubmit" class="btn red">{$t.b_send}</button>
            </div>
        </div>
    </div>
</form> 