<a {if !empty( $items )} href="46" {/if}class="hover-layer">
    <span class="product-number">{$total}</span>
    <span class="cart-tl">{$t.my_cart}</span>
    <span class="total-price">{number_format($total_price, 0, ',', ' ')} грн</span>
</a>
{if !empty( $items )}
<div class="cart-dropdown">
    <div class="drop-container">
        <h4>{$t.in_cart}</h4>
        {foreach $items as $item}
            <div class="cart-pr-box">
                <div class="pr-photo">
                    <img src="{$item.img.src}">
                </div>
                <div class="pr-info">
                    <a href="{$item.products_id}">{$item.name}</a>
                </div>
                <div class="pr-number">
                    <span>{$item.qtt} шт.</span>
                </div>
                <div class="pr-price">
                    <span>{number_format($item.price * $item.quantity, 0, ',', ' ')} {$item.symbol}</span>
                </div>
            </div>
        {/foreach}
    </div>
    <div class="drop-footer">
        <a href="#" class="btn-small-blue b-show-cart">{$t.link_cart}</a>
        <span class="drop-tot-price">{number_format($total_price, 0, ',', ' ')} грн</span>
    </div>
</div>
{/if}