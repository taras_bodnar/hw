<select class="currency select" id="changeCurrency">
    {foreach $items as $item}
    <option {if $curency_id == $item.id} selected {/if} class="{$item.symbol}" value="{$item.id}">{$item.name}</option>
    {/foreach}
</select>