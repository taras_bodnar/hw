{*<pre>{print_r($items)}*}
{if !empty($items)}
<section class="l-resort">
    <h2>{$t.travel_for_you}</h2>
    <div class="items-container">
        {foreach $items as $item}
            <div class="item">
                <div class="item-midle">
                    <div class="img">
                        <img src="{$item.img.src}" alt="{$item.img.alt}" width="321" height="207"/>
                    </div>
                    <a href="{$item.parent_id}" title="{$item.title}" class="item-link no-click">
                        <div class="middle">
                            <div class="table">
                                <div class="table-cell">
                                    <div class="title">{$item.name}</div>
                                    <div class="number">{$item.description}</div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="tour-info">
                    <div class="tour-dates">
                        <span>{$item.date.from} - {$item.date.from}</span>
                    </div>
                    <div class="actual-price">
                        <span>$ <span class="tour-price">{$item.travel_price|round}</span></span>
                    </div>
                    {if $item.travel_old_price|round!=0}
                        <div class="old-price">
                            <span>$ <span class="tour-price">{$item.travel_old_price|round}</span></span>
                        </div>
                    {/if}
                </div>
            </div>
            <!-- .item -->
        {/foreach}
    </div>
</section>
{/if}