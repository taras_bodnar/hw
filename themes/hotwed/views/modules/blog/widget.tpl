
<section class="l-good-know">
    <div class="container">
        <div class="header">
            <h2>{if $blog_type == 'news'}{$t.news_widget_title}{else}{$t.blog_widget_title}{/if}</h2>
            <div class="btn-group">
                <a class="btn-prev animation-left">
                    <i class="icon-arrow-left"></i>
                </a>
                <a class="btn-next animation-right">
                    <i class="icon-arrow-right"></i>
                </a>
            </div>
        </div>
        <div id="good-know" class="owl-carousel">
            {foreach $items as $item}
            <div class="item">
                <div class="midle">
                    <a href="{$item.id}" title="{$item.title}">
                        <div class="title">
                            <span>{$item.name}</span>
                        </div>
                        <p>{$item.description}</p>
                    </a>
                </div>
            </div><!-- .item -->
            {/foreach}
        </div>
        <div class="footer">
            <a href="4" class="btn png-red">{$t.blog_more}</a>
        </div>
    </div>
</section><!-- .l-good-know -->