<div class="m-module m-left-menu">
    <div class="m-content">
        <ul class="level-1">
            {foreach $categories as $item}
            <li><a href="{$item.id}" title="{$item.title}">{{$item.name}}</a></li>
            {/foreach}
        </ul>
    </div>
</div><!-- .m-module -->