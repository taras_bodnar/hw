<section class="l-information-list">
    <div class="container">
        <div class="header">
            <label for="blogCat">{$t.blog_category}</label>
            <select id="blogCat">
                <option data-href="{$root_id}">{$t.all}</option>
                {foreach $categories as $item}
                <option {if $page.id==$item.id}selected{/if} data-href="{$item.id}">{$item.name}</option>
                {/foreach}
            </select>
        </div>
        <div class="content" id="blogItems">{$items}</div>
        <div class="footer">
            <div class="col-3">
                {if $total>0}
                <a class="btn icons-png-red icons-around b-more-posts" data-p="{$start}" href="#">
                    <span>{$t.blog_more}</span>
                </a>
                {/if}
            </div>
            <div class="col-9 m-pagination" id="pagination">
                {$pagination}
            </div>
        </div>
    </div>
</section>