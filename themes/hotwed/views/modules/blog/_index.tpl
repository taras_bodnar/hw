<div class="m-blog">
    <div class="m-container">
        <div class="m-content">
            {foreach $items as $item}
                <div class="item">
                    <h2><a href="{$item.id}" title="{$item.title}">{$item.name}</a></h2>
                    <div class="date">{$item.dd} {$item.mm} {$item.yy}</div>
                    <div class="item-content">
                        {if !empty($item.img.src)}<img src="{$item.img.src}" alt="{$item.img.alt}"/>{/if}
                        <p>{$item.description}</p>
                    </div>
                    {if !empty($item.tags)}
                        <div class="m-tags">
                            {foreach $item.tags as $tag}
                                <a href="{$root_id}&tag={$tag.alias}"><i class="sprite i-09"></i><span>{$tag.name}</span></a>
                            {/foreach}
                        </div>
                    {/if}
                </div><!-- .item -->
            {/foreach}
            {* циферки пагінації *}
            {$pages}
            {* циферки пагінації *}
            {* опис категорії*}
            {$page.content}
            {* опис категорії*}
        </div><!-- .m-content -->
    </div>
    <div class="m-sidebar">
        <ul>
            {foreach $categories as $item}
            <li><a {if $item.id==$page.id}class="active"{/if} href="{$item.id}" title="{$item.title}" >{$item.name}</a></li>
            {/foreach}
        </ul>
    </div>
</div>