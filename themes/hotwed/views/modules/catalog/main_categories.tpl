
<section class="l-category-specialists">
    {foreach $items as $item}
        <div class="item">
            <div class="item-midle">
                <div class="img">
                    <img src="{$item.img.src}" alt="" style="width: inherit;"/>
                </div>
                <a href="{$item.id}" title="{$item.title}">
                    <div class="middle">
                        <div class="table">
                            <div class="table-cell">
                                <div class="title">{$item.name}</div>
                                {if $item.id!=63}
                                    <div class="number">{$item.t} {$t.count_workers}</div>
                                {/if}
                            </div>
                        </div>
                    </div>
                </a></div>
        </div><!-- .item -->
    {/foreach}
    <div class="item">
        <div class="item-midle">
            <div class="img">
                <img src="{if $show_all_image.src}{$show_all_image.src}{else}http://hotwed.com.ua/uploads/content/64/wg/restorany-36.jpg{/if}" alt="" style="width: inherit;"/>
            </div>
            <a href="3" title="{$t.show_all_catalog}">
                <div class="middle">
                    <div class="table">
                        <div class="table-cell">
                            <div class="title" style="font-size: 42px;">{$t.show_all_catalog}</div>
                            {if $item.id!=63}
                                <div class="number">{$items_total} {$t.categories_workers}</div>
                            {/if}
                        </div>
                    </div>
                </div>
            </a></div>
    </div><!-- .item -->

</section><!-- .l-category-specialists-->