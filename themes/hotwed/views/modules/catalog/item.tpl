{*<!--Тарас, додаєш st-hot або st-gold до item -->*}
<div class="item  {if $item.hot==1}st-hot{elseif $item.gold==1}st-gold {elseif $item.point>=1000}st-rec{/if}">
    <div class="item-header">
        <div class="name"><a href="45;p={$item.id}{if $smarty.get.filter.df};filter[df]={$smarty.get.filter.df}{/if}"><h3>{$item.name} {$item.surname}</h3></a></div>
        <div class="town">
            {foreach $item.city as $city}
                <a href="3;filter[city_id]={$city.id}{if $smarty.get.filter.df}&filter[df]={$smarty.get.filter.df}{/if}">
                    <span>{$city.name}</span>
                </a>
            {/foreach}
        </div>
        {*<span><pre>{print_r($item)}</span>*}
        <!--new-->
        <div class="st-info">
            {if $item.hot==1}
                <div class="status">
                    Hot Пропозиція
                </div>
                {elseif $item.gold==1}
                <div class="status">
                    Gold Аккаунт
                </div>
                {elseif $item.point>=1000}
                <div class="status">
                    HotWed Рекомендує
                </div>
            {/if}
            {if $item.hot!=1 && $item.gold!=1 && $item.point<1000 }
            <div class="diamonds tipsy" original-title="{$t.scores}">
                {$item.point}
            </div>
            {/if}
            <div class="views tipsy" original-title="{$t.count_views}">
                {$item.views}
            </div>
        </div>
    </div>
    <div class="item-content">
        <div class="author">
            <div class="row">
                <div class="column-l">
                    <div class="img-autor">
                        <a href="45;p={$item.id}{if $smarty.get.filter.df};filter[df]={$smarty.get.filter.df}{/if}">
                            <img src="{if $item.avatar == ''}/themes/hotwed/assets/img/avatar.jpg{else}{$item.avatar}{/if}" alt=""/>
                        </a>
                    </div>
                    {if $item.comments>0}
                    <div class="number-comments">
                        <span>{$item.comments}</span><i class="icon-massage"></i>
                    </div>
                    {/if}
                </div>
                <div class="column-r">
                    <div class="directions"><a href="{$item.content_id}{if $smarty.get.filter.df};filter[df]={$smarty.get.filter.df}{/if}">{$item.group_name}</a></div>

                        {if $item.content_id == 64}
                            <div class="price-day"><span>{$t.item_per_place}</span><abbr title="{$t.sprice}{$item.sppd}%">{$item.price_per_day}{$item.symbol}</abbr></div>
                        {elseif $item.content_id == 42 || $item.content_id == 146 || $item.content_id == 145|| $item.content_id == 144 || $item.content_id == 84 || $item.content_id == 83 || $item.content_id == 140  }
                            <div class="price-day"><span>{$t.item_per_service}</span><abbr title="{$t.sprice}{$item.sppd}%">{$item.price_per_day}{$item.symbol}</abbr></div>
                        {else}
                            <div class="price-day"><span>{$t.price_per_d}</span><abbr title="{$t.sprice}{$item.sppd}%">{$item.price_per_day}{$item.symbol}</abbr></div>
                        {/if}

                        {if $item.price_per_hour > 0}
                            <div class="price-hour"><span>{$t.price_per_h}</span><abbr title="{$t.sprice}{$item.spph}%">{$item.price_per_hour}{$item.symbol}</div>
                        {/if}

                </div>
            </div>
        </div>
        <div class="portfolio">
            {*<pre>{print_r($item.img)}*}
            {foreach $item.img as $img}
            <div class="img">

                <a class="pf-items {if $img.type=='video'} video fancybox.iframe{/if}" rel="group{$item.id}" href="{$img.big}">
                    {if $img.day_check==1}
                        <div class="img-label tipsy" original-title="Фото дня"></div>
                    {/if}
                    <span>
                         <img class="lazy" data-src="{$img.thumb}" src="" alt=""/>
                    </span>
                </a>
            </div><!-- .img -->
            {/foreach}
        </div>
    </div>
</div><!-- .item -->