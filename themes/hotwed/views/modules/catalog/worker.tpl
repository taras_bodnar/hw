<main class="l-main  {if $user.gold==1}st-gold{elseif $user.hot==1}st-hot{/if}" role="main">
    <section class="l-title l-title-user-page">
        <div class="row">
            <div class="container">
                <h1>{$user.name} {$user.surname}</h1>
                <div class="directions">
                    <a href="{$user.content_id}">{$user.group_name}</a>
                </div>
                <div class="town">
                    {foreach $user.city as $city}
                        <a href="3;filter[city_id]={$city.id}">
                            <span>{$city.name}</span>
                        </a>
                    {/foreach}
                </div>
            </div>
        </div>
    </section><!-- .l-title-page-->

    <section class="l-user-page">
        <div class="container">
            <div class="sidebar">
                <!--myroslav-->
                <div class="l-account">
                    {if $user.gold==1}
                        <div class="gold">Gold Аккаунт</div>
                    {/if}

                    {if $user.point>=1000}
                        <div class="rec">HotWed рекомендує</div>
                    {/if}
                    {if $user.hot==1}
                    <div class="hot">Hot Пропозиція</div>
                    {/if}
                    {if $user.hot!=1 && $user.gold!=1 && $user.point<1000}
                        <div class="numb tipsy" original-title="{$t.scores}">{$points}</div>
                    {/if}
                    <div class="progress" data-progress="{if $user.gold!=1 && $user.point<1000}{$pointsDel|round}{else}100{/if}">
                        <span></span>
                    </div>
                </div>
                <!--end-->
                <div class="img">
                    <img src="{if $user.avatar == ''}/themes/hotwed/assets/img/avatar.jpg{else}{$user.avatar}{/if}" alt="{$user.name} {$user.surname}" />
                </div>

                <div class="info-line">
                    <a id="like" style="cursor: pointer;" data-user="{$user.id}" data-client="{$smarty.session.app.user.id}" original-title="{$t.abuse}" class="likes tipsy">
                        {$like}
                    </a>
                    <div class="views tipsy" original-title="{$t.count_views}">{$views}</div>
                </div>
                {*{if $is_online}*}
                {*<pre>{$user.id}*}
                {if $user.id!=$smarty.session.app.user.id}
                <a class="btn icons-png-red icons-open_mail b-write-message" href="50;p={$user.id}"><span>{$t.b_write_dialog}</span></a>
                {/if}
                {if !$is_worker}
                    <a class="btn icons-png-red icons-heart b-wl {$user.on_wishlist} "
                       href="47" data-id="{$user.id}" data-in="{$t.wl_in}"><span>{if $user.on_wishlist == 'in'}{$t.wl_in}{else}{$t.wl_add}{/if}</span></a>
                {/if}
                <table>
                    {if $user.phone != ''}
                    <tr>
                        <td>{$t.phone}</td>
                        <td title="{$user.phone}" style="cursor: help">{$user.phone}</td>
                    </tr>
                    {/if}
                    {if $user.site != ''}
                    <tr>
                        <td>{$t.ui_site}:</td>
                        <td><a rel="nofollow" target="_blank" href="http://{$user.site}">{$user.site}</a></td>
                    </tr>
                    {/if}
                    {if $user.blog != ''}
                    <tr>
                        <td>{$t.ui_blog}:</td>
                        <td><a rel="nofollow" target="_blank" href="http://{$user.blog}">{$user.blog}</a></td>
                    </tr>
                    {/if}
                    {if $user.facebook != ''}
                    <tr>
                        <td><img src="{$template_url}assets/img/icons/svg/facebook.svg"></td>
                        <td><a rel="nofollow" target="_blank" href="http://{$user.facebook}">{$user.facebook}</a></td>
                    </tr>
                    {/if}
                    {if $user.vk != ''}
                    <tr>
                        <td><img src="{$template_url}assets/img/icons/svg/vk.svg"></td>
                        <td><a rel="nofollow" target="_blank" href="http://{$user.vk}">{$user.vk}</a></td>
                    </tr>
                    {/if}
                    {if $user.instagram != ''}
                    <tr>
                        <td><img src="{$template_url}assets/img/icons/svg/instagramm.svg"></td>
                        <td><a rel="nofollow" target="_blank" href="http://{$user.instagram}">{$user.instagram}</a></td>
                    </tr>
                    {/if}
                    {*<pre>{print_r()}*}
                </table>
                {if $user.info != ''}
                <div class="description">
                    {$user.info}
                </div>
                {/if}
            </div>
            <div class="content">
                <div class="row">
                    <div class="reserve" id="formContent">
                        {$order_form}
                    </div>
                    <div class="portfolio">
                        {if !empty($user.images)}
                        <div class="header">
                            <h2>{$t.portfolio}</h2>
                            <div class="btn-group">
                                <a class="btn-prev animation-left">
                                    <i class="icon-arrow-left"></i>
                                </a>
                                <a class="btn-next animation-right">
                                    <i class="icon-arrow-right"></i>
                                </a>
                            </div>
                        </div>
                        <div id="portfolio-carusel" class="owl-carousel" style="width: 100%">
                            {*<pre>{print_r($user.images)}*}
                            {foreach $user.images as $item}
                            <div class="item">
                                {foreach $item as $img}

                                <div class="item-image">
                                    <a class="pf-items {if $img.type=='video'} video fancybox.iframe{/if}" rel="group" href="{$img.big}">
                                        {if $img.day_check==1}
                                            <div class="img-label tipsy" original-title="Фото дня"></div>
                                        {/if}
                                        <span>
                                             <img class="lazy" data-src="{$img.thumb}" alt=""/>
                                        </span>
                                    </a>
                                </div><!-- .item-image -->
                                {/foreach}
                            </div><!-- .item -->
                            {/foreach}
                        </div>
                        {/if}

                        {$comments}
                    </div>
                </div>
            </div>
        </div>
    </section>
    {if $user.gold!=1 && $user.hot!=1}
        <h2 class="another-performers-h2">{$t.another_performers}</h2>
        [[mod:Catalog::mainGallery]]
    {/if}
</main><!-- .l-main -->