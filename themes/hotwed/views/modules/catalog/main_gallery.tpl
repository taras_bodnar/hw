
<section class="l-gallery-professionals">
    {foreach $items as $item}
    <div class="item">
        <div class="img {if $item.type=='video'}video{/if}">
            <span>
                 <img data-src="{$item.img}" class="lazy" alt=""/>
            </span>
        </div>
        <a href="45;p={$item.users_id}">
            <div class="middle">
                <div class="table">
                    <div class="table-cell">
                        <div class="title">{$item.pib}</div>
                        <div class="direction">{$item.ug_name}</div>
                    </div>
                </div>
            </div>
        </a>
    </div><!-- .item -->
    {/foreach}
</section><!-- .l-gallery-professionals -->