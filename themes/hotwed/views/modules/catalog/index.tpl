{*<pre>{print_r($seo)}*}
<main class="l-main" role="main">
    <section class="l-title hide-onload">
        <div class="row">
            <div class="container">
                <h1>{if empty($seo)}{if empty($page.case_title)}{$t.catalog_title}{else}{$page.case_title}{/if}{else}{$seo.name}{/if}</h1>
               [[mod:Catalog::filter]]
            </div>
        </div>
    </section><!-- .l-title-page-->

    <section class="l-people-list hide-onload">
        <div class="container">
            <form class="header" action="">
                <ul class="sorting-list">
                    {foreach $sorting as $k=>$item}
                        <li {if isset($smarty.get.order) && $smarty.get.order == $item.id}class="active"{elseif !isset($smarty.get.order) && $k == 0}class="active" {/if}>
                            {*<a href="{$page.id};l={$page.languages_id};order={$item.id}&{$sUrl}">*}
                                {*{$item.name}*}
                            {*</a>*}
                            <a href="{$smarty.server.PATH_INFO}?order={$item.id}&{$sUrl}">
                                {$item.name}
                            </a>
                        </li>
                    {/foreach}
                </ul>
                {*<div class="col-9">*}
                    {*{if $smarty.get.q}*}
                    {*<h5>{str_replace('{total}', $t_total, $t.search_total)}</h5>*}
                    {*{/if}*}
                    {*&nbsp; *}
 {**}
                {*</div>*}
                {*<div class="col-3">*}
                    {*<select name="order" onchange="submit();"> *}
                        {*{foreach $sorting as $item}*}
                            {*<option {if isset($smarty.get.order) && $smarty.get.order == $item.id}selected{/if} value="{$item.id}">{$item.name}</option>*}
                        {*{/foreach}*}
                    {*</select>*}
                {*</div>*}
                {if isset($smarty.get.filter.df) && $smarty.get.filter.df != ''}
                    <input type="hidden" name="filter[df]" value="{$smarty.get.filter.df}">
                {/if}
                {if isset($smarty.get.filter.city_id) && $smarty.get.filter.city_id != ''}
                    <input type="hidden" name="filter[city_id]" value="{$smarty.get.filter.city_id}">
                {/if}
                {if isset($smarty.get.q) && $smarty.get.q != ''}
                    <input type="hidden" name="q" value="{$smarty.get.q}">
                {/if}
            </form>
            <div class="content hide-onload" id="catalogWorkers">{$items}</div>
            <div class="footer">
                <div class="col-3">
                    {if $total>0}
                        {*<pre>{print_r($smarty.get)}</pre>*}
                    <a class="btn icons-png-red icons-around b-more-workers"
                       data-city_id="{if isset($smarty.get.filter.city_id) && !empty($smarty.get.filter.city_id)}{$smarty.get.filter.city_id}{else}0{/if}" data-p="{$start}" href="{$root_id}">
                        <span>{$t.b_catalog_more}</span>
                    </a>
                    {/if}
                </div>
                <div class="col-9 m-pagination hide-onload" id="pagination">
                    {$pagination}
                </div>
            </div>
            [[mod:Catalog::seo]]
        </div>
    </section>


</main><!-- .l-main -->
{*<style>*}
    {*.hide-onload{*}
        {*display: none;*}
    {*}*}
{*</style>*}
{*{literal}*}
{*<script>*}

    {*window.onload = function () {*}
    {*var items = $.parseHTML( '{/literal}{$items}{literal}');*}
        {*$('#catalogWorkers').html(items);*}
        {*$('.hide-onload').show()*}
    {*}*}
{*</script>*}
{*{/literal}*}