<form method="get" action="{if $page.parent_id!=3}3{else}{$page.id}{/if}" id="catalogFilter">
    <div class="m-filter-people">
        <div class="filter-content">
            <div class="filter-border">
                <div class="col-4">
                    <div class="title">{$t.catalog_f_date}</div>
                    <div class="date" id="datepickerBox">
                        <div class="bg-input" >
                            <input type="text" required name="filter[df]" id="datepicker" readonly value="{$smarty.get.filter.df}">
                            <i class="icon-calendar_2"></i>
                        </div>
                    </div>
                </div><!-- .col-4 -->
                <div class="col-4 front-spec-input">
                    <div class="title">{$t.catalog_f_ug}</div>
                    <select data-placeholder="{$t.artists}" id="filter_gid">
                        <option value="" data-href="3"></option>
                        {foreach $group as $item}
                            <option data-href="{$item.content_id}" {if $page.id == $item.content_id}selected{/if} value="{$item.id}">{$item.name}</option>
                        {/foreach}
                    </select>

                </div><!-- .col-4 -->
                <div class="col-4 front-city-input" style="display: block!important;">
                    <div class="title">{$t.catalog_f_city}</div>
                    <select data-placeholder="{$t.cities}" id="filter_city_id">
                        <option value=""></option>
                        {foreach $city as $item}
                            <option {if $smarty.get.filter.city_id == $item.id}selected{/if} data-href="{$item.value}" value="{$item.id}">{$item.name}</option>
                        {/foreach}
                    </select>
                </div><!-- .col-4 -->
            </div>
        </div>
        <div class="footer">
            <div class="row">
                <button onclick="submitFilter();return false;" class="btn red" type="submit">{$t.b_filter}</button>
                {if $page.id != 1 && $smarty.get.filter}
                    <a  href="3" class="reset"><span>{$t.b_filter_reset}</span><i class="icon-clancel_circle"></i></a>
                {/if}
            </div>
            <input type="hidden" name="order" value="{$smarty.get.order}">
        </div>
    </div><!-- .m-filter-people -->
</form>