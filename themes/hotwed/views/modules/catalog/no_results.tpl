<main role="main" class="l-main" style="padding-bottom: 203px;">
    <section class="l-title">
        <div class="row">
            <h1>{$t.no_results_title}</h1>
        </div>
    </section><!-- .l-title-page-->

    <section class="l-page-message">
        <div class="container">
            {$t.no_results_content}
        </div>
    </section><!-- .l-title-page-->

</main>