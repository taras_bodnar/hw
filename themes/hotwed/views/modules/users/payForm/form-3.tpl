<div class="w-modal st-hot" style="width: 460px;">
    <div class="c-wrap">
        <div class="f-content">
            <p class="title">Отримати Додаткові міста!</p>
            <p>Текст</p>
        </div>
    </div>
    <form id="hotProcess" action="ajax/Orders/moreCities" method="POST">
        <div class="inline-input">
            <div class="f-content">
                <div class="input-box">
                    <label>Телефон</label>
                    <div class="in-wr">
                        <input name="data[phone]" required type="text">
                    </div>
                </div>
            </div>
        </div>
        <div class="f-content">
            <div class="f-check-wrap">
                {if $count<1}
                    <label>
                        <input class="price" type="radio" name="data[price]" value="{$priceOneC}">
                        <span class="radio-btn"></span>
                        <span class="label"> Отримати ще<strong> 1 місто</strong></span>
                    </label>
                    <label>
                        <input class="price" type="radio" name="data[price]" value="{$priceTwoC}">
                        <span class="radio-btn"></span>
                        <span class="label">Отримати ще <strong> 2 міста</strong></span>
                    </label>
                    {else}
                    <label>
                        <input class="price" type="radio" name="data[price]" value="{$priceOneC}">
                        <span class="radio-btn"></span>
                        <span class="label"> Отримати ще<strong> 1 місто</strong></span>
                    </label>
                {/if}
            </div>
            <div class="total">
                <p>Сума до оплати: <span>0 грн</span></p>
            </div>
            <div class="submit">
                <button type="submit" class="btn red">Оплатити</button>
                {*<div class="link">*}
                    {*<a href="#">{$t.ab_more}</a>*}
                {*</div>*}
            </div>
        </div>
    </form>
</div>

<script>
    $(document).ready(function(){
        $('.price').on('change', function(){
            if($(this).is(':checked') && $(this)!=='undefined'){
                $(".total").find('span').text($(this).attr('value')+' грн');
            }
        });
    });
    var bSubmit = $('.btn');
    $("#hotProcess").ajaxForm({
        dataType: 'json',
        data: {
            skey: SKEY
        },
        beforeSend: function(){
//            bSubmit.attr('disabled', true);
        },
        success: function(d)
        {
//            bSubmit.removeAttr('disabled');
            if(d.s==1){
//                $("#hotProcess").remove();
//                $(".f-content").html(d.payMethod.checkout);
//                location.href = d.payMethod.checkout;
                location.href = d.href;
            } else {
                $(d.m).insertAfter(".btn");
            }
        }
    });

</script>