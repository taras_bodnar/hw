<div class="w-modal st-hot" style="width: 460px;">
    <div class="c-wrap">
        <div class="f-content">
            {$t.hot_form_title}
        </div>
    </div>
    <form id="hotProcess" action="ajax/Orders/hotProcess" method="POST">
        <div class="inline-input">
            <div class="f-content">
                <div class="input-box">
                    <label>{$t.gold_ui_phone}</label>
                    <div class="in-wr">
                        <input name="data[phone]" required type="text">
                    </div>
                </div>
            </div>
        </div>
        <div class="f-content">
            <div class="f-check-wrap">
                <label>
                    <input class="price" type="radio" name="data[price]" value="{$priceWeek}">
                    <span class="radio-btn"></span>
                    <span class="label">{$t.get_status_on} <strong>15 {$t.days}</strong></span>
                </label>
                <label>
                    <input class="price" type="radio" name="data[price]" value="{$priceMonth}">
                    <span class="radio-btn"></span>
                    <span class="label">{$t.get_status_on} <strong>30 {$t.days}</strong></span>
                </label>
            </div>
            <div class="total">
                <p>{$t.amount} <span>0 {$t.uah}</span></p>
            </div>
            <div class="submit">
                <button type="submit" class="btn red">{$t.submit_request}</button>
                {*<div class="link">*}
                    {*<a href="#">{$t.ab_more}</a>*}
                {*</div>*}
            </div>
        </div>
    </form>
</div>

<script>
    $(document).ready(function(){
        $('.price').on('change', function(){
            if($(this).is(':checked') && $(this)!=='undefined'){
                $(".total").find('span').text($(this).attr('value')+' грн');
            }
        });
    });
    var bSubmit = $('.btn');
    $("#hotProcess").ajaxForm({
        dataType: 'json',
        data: {
            skey: SKEY
        },
        beforeSend: function(){
//            bSubmit.attr('disabled', true);
        },
        success: function(d)
        {
//            bSubmit.removeAttr('disabled');
            if(d.s==1){
//                $("#hotProcess").remove();
//                $(".f-content").html(d.payMethod.checkout);
//                location.href = d.payMethod.checkout;
                location.href = d.href;
            } else {
                $(d.m).insertAfter(".btn");
            }
        }
    });

</script>