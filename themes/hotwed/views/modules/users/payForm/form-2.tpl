<div class="w-modal st-gold" style="width: 460px;">
    <div class="c-wrap">
        <div class="f-content">
            {$t.gold_form_title}
        </div>
    </div>
    <form id="goldProcess" action="ajax/Orders/goldProcess" method="POST">
        <div class="inline-input">
            <div class="f-content">
                <div class="input-box">
                    <label>{$t.gold_ui_phone}</label>
                    <div class="in-wr">
                        <input name="data[phone]" required type="text">
                    </div>
                </div>
            </div>
        </div>
        <div class="f-content">
            <div class="total">
                <p>{$t.amount} <span>{$priceYear} {$t.uah}</span></p>
            </div>
            <div class="submit">
                <button type="submit" class="btn red">{$t.submit_request}</button>
                {*<div class="link">*}
                    {*<a href="#">{$t.ab_more}</a>*}
                {*</div>*}
            </div>
        </div>
    </form>
</div>

<script>
    var bSubmit = $('.btn');
    $("#goldProcess").ajaxForm({
        dataType: 'json',
        data: {
            skey: SKEY
        },
        beforeSend: function(){

//            bSubmit.attr('disabled', true);
        },
        success: function(d)
        {
//            bSubmit.removeAttr('disabled');
            if(d.s==1){
//                $("#hotProcess").remove();
//                $(".f-content").html(d.payMethod.checkout);
//                location.href = d.payMethod.checkout;
                location.href = d.href;

            } else {
                displayErrors(d.m, d.inp);
            }
        }
    });

</script>