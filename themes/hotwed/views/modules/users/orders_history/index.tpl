<section class="l-history-bookings">
    <div class="container">
        <div class="content">
            {if $items_new != ''}
            <div class="upcoming-reservations">
                <div class="reservations-header">
                    {$t.oh_2}
                </div>
                <div class="upcoming-content">
                    {$items_new}
                </div>
            </div>
            {/if}
            {if $items != ''}
            <div class="past-reservations">
                <div class="reservations-header">
                    {$t.oh_1}
                </div>
                <div class="upcoming-content" id="orders">
                    {$items}
                </div>
            </div>
            {/if}
        </div>
        <div class="footer">
            <div class="col-3">
                {if $total>0}
                    <a class="btn icons-png-red icons-around b-more-orders-oh" data-p="{$start}" href="{$root_id}">
                        <span>{$t.b_more_orders}</span>
                    </a>
                {/if}
            </div>
            <div class="col-9 m-pagination" id="pagination">
                {$pagination}
            </div>
        </div>
    </div>
</section>