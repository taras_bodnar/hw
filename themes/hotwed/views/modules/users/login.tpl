<div class="response"></div>
<form id="usersLogin" action="ajax/Users/login" method="post">
    <div class="row input-box">
        <label for="data_email">{$t.u_email}</label>
        <input type="email" name="data[email]" required id="data_email"/>
    </div>
    <div class="row input-box">
        <label for="data_password">{$t.u_password}</label>
        <input type="password" name="data[password]" required id="data_password" />   
    </div> 
    <div class="row input-message">
        <p>{$t.forgot} <a href="ajax/Users/fp" class="pop-up" >{$t.link_forgot}</a></p>
    </div>
    <div class="row footer">
        <div class="row">        
            <button class="btn red" type="submit">{$t.b_logn}</button>
        </div>
    </div>
    <input type="hidden" name="skey" value="{$skey}" >
    <input type="hidden" name="process" value="1" >

    <div class="row footer">
        <div class="row">
            <p>{$t.oauth_title}</p>
            <a href="{$oauth_facebook_url}"><img src="/themes/hotwed/assets/img/icons/icon-facebook.png"></a>
            <a href="{$oauth_google_url}"><img src="/themes/hotwed/assets/img/icons/g-plus.png"></a>
            {*<a href="{$oauth_vk_url}"><img src="/themes/hotwed/assets/img/icons/icon-vk.png"></a>*}
        </div>
    </div>
</form>