<section class="l-calendar">
    <div class="container">
        <div id="calendar"></div>
    </div>
</section><!-- .l-calendar -->
<div class="container">
    <form method="post" action="ajax/Calendar/oDate" id="oDate" class="set-disabled-days">
        <p style="text-align: center;">{$t.manual_s_day_title}</p>
        <div class="m-form-box">
            <div class="row group-input">
                <label for="data_surname" class="input-label">{$t.odate_label}</label>
                <div class="input" id="dpbox">
                    <input type="text" readonly id="ddf" name="df" required placeholder="{$t.from}" >
                    <input type="text" readonly id="ddt" name="dt" required placeholder="{$t.to}" >
                </div>
            </div>
            <div class="row group-input">
                <label for="data_surname" class="input-label">{$t.odate_comment}</label>
                <div class="input">
                    <input type="text" name="comment" required >
                </div>
            </div>
            <div class="row send">
                <button class="btn red" id="bSubmit" type="submit">{$t.b_save}</button>
            </div>
        </div>
        <input type="hidden" name="skey" value="{$skey}">
        <div class="row">
            <div class="response"></div>
        </div>
    </form>
</div>