<div class="m-pop-up-message pop-up-middle"> 

<form id="usersFp" action="ajax/Users/fp" method="post">
    <h2>{$t.password_recovery}</h2>   
    <div class="row input-box">
        <label for="data_email">{$t.u_email}</label>
        <input type="email" name="data[email]" required id="data_email"/>
    </div>  
    <div class="row footer">        
        <button class="btn red" type="submit">{$t.u_fp_btn}</button>       
    </div>
    <input type="hidden" name="skey" value="{$skey}" >
    <input type="hidden" name="process" value="1" >
    <div class="row">
        <div class="response"></div>
    </div> 
</form>
<script>new Users()</script> 
</div>