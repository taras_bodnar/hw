<div class="pop-up-middle" style="width: 500px;">
    <h2>{$t.change_password}</h2>
    <form id="changePasswordForm" method="post" action="ajax/Users/changePassword">
        <div class="row input-box">
            <label for="data_password">{$t.u_password}</label>
            <input type="password" name="data[password]" id="data_password" required />
        </div>
        <div class="row input-box">
            <label for="data_r_password">{$t.u_re_password}</label>
            <input type="password" name="data[r_password]" id="data_r_password" required />
        </div>
    <div class="response"></div>
        <div class="row footer" style="padding: 0">
            <div class="row">
                <button class="btn red" id="bSubmitForm" type="submit" style="display: block; margin: 0 auto;">{$t.b_send}</button>
            </div>
        </div>
        <input type="hidden" name="skey" value="{$skey}" >
    </form>
</div>