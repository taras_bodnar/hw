<div class="mod-form">
    <div class="modal-wrapper">
        <h2>{$t.login_title}</h2>
    </div>
    <div class="modal-wrapper">
        <div class="form-tl">
            <p class="info">{$t.auth_info}</p>
        </div>
        <form method="post" action="ajax/Users/authProcess" id="authForm">
            <div class="form-row">
                <label for="data_password">{$t.auth_password}</label>
                <input required name="password" type="text" id="data_password">
            </div>
            <button type="submit" class="call-btn">{$t.auth_btn}</button>
            <input type="hidden" name="skey" value="{$skey}" />
            <input type="hidden" name="mode" value="{$smarty.post.mode}" />
            <input type="hidden" name="email" value="{$smarty.post.email}" />
            <input type="hidden" name="phone" value="{$smarty.post.phone}" />
        </form>
    </div>
</div>