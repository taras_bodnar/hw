<div class="l-personal-information">
    {*<pre>{print_r($ui)}</pre>*}
    <div class="container">
        <form method="post" action="ajax/Users/profile" id="usersProfile">

            <div class="m-form-box">
                <div class="row group-input">
                    <label for="data_surname" class="input-label">{$t.ui_surname}</label>
                    <div class="input tooltip-right" title="{$t.profile_pib_info}">
                        <input type="text" data-error="{$t.e_user_surname}" name="data[surname]" id="data_surname" required value="{$ui.surname}">
                    </div>
                </div>
                <div class="row group-input">
                    <label for="data_name" class="input-label">{$t.ui_name}</label>
                    <div class="input">
                        <input class="m-tooltip" data-error="{$t.e_user_surname}" name="data[name]" id="data_name" required type="text" value="{$ui.name}" >
                    </div>
                </div>
                <div class="row group-input">
                    <label for="data_email" class="input-label">{$t.ui_email}</label>
                    <div class="input">
                        <input type="email" data-error="{$t.user_login_e_bad_email}" name="data[email]" required id="data_email" value="{$ui.email}">
                    </div>
                </div>
                <div class="row group-input">
                    <label for="data_url" class="input-label">{$t.ui_url}</label>

                    <div class="input">
                        <input type="text" data-error="{$t.e_user_url}" name="data[url]" required
                               id="data_url" value="{$ui.url}">
                    </div>
                </div>
                <div class="row group-input">
                    <label for="data_phone" class="input-label">{$t.ui_phone}</label>
                    <div class="input">
                        <input type="text" name="data[phone]" required id="data_phone" value="{$ui.phone}">
                    </div>
                </div>

                <div class="row group-input">
                    <label class="input-label" for="languages_id">{$t.email_notification_lang}</label>
                    <div class="input">
                        <select class="tooltip-right" id="data_languages_id" name="data[languages_id]">
                            {foreach $languages as $lang}
                                <option value="{$lang.id}" {if $ui.languages_id == $lang.id}selected{/if}>{$lang.name}</option>
                            {/foreach}
                        </select>
                    </div>
                </div>

                <div class="row group-input">
                    <div class="input">
                        <button type="button" class="btn white-green" id="changePassword" style="width: 100%; margin: 20px 0">{$t.change_password}</button>
                    </div>
                </div>
{*
                <div class="row group-input">
                    <label for="data_phone" class="input-label">Facebook ID</label>
                    <div class="input tooltip-right" title="{$t.facebook_id}">
                        <input type="text" name="oauth[facebook]"  id="oauth_facebook" value="{$ui.oauth.facebook}">
                    </div>
                </div>
                <div class="row group-input">
                    <label for="data_phone" class="input-label">VK ID</label>
                    <div class="input tooltip-right" title="{$t.vk_id}">
                        <input type="text" name="oauth[vk]"  id="oauth_vk" value="{$ui.oauth.vk}">
                    </div>
                </div>
                <div class="row group-input">
                    <label for="data_phone" class="input-label">Google+ ID</label>
                    <div class="input tooltip-right" title="{$t.google_id}">
                        <input type="text" name="oauth[google]"  id="oauth_google" value="{$ui.oauth.google}">
                    </div>
                </div>
*}
                <div class="row send">
                    <button class="btn red" type="submit">{$t.b_save}</button>
                </div>
            </div>
            <input type="hidden" name="skey" value="{$skey}">
            <input type="hidden" name="process" value="1">
            <input type="hidden" name="gid" value="{$ui.users_group_id}">
            <div class="row">
                <div class="response"></div>
            </div>
        </form>
        {$rm_profile}
    </div>
</div>