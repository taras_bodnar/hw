<div class="item {if $item.is_read == 0}new{/if}">
    <div class="column-l">
        <div class="item-header">
            <div class="date-user">
                <div class="name">
                    {if $item.notify == 'worker'}
                        <span>{$item.username}</span>
                    {else}
                        <a href="#">{$item.username}</a>    
                    {/if}
                    
                </div>
                <div class="phone">
                    <i class="icon-phone"></i>
                    <span>{$item.phone}</span>
                </div>
                <div class="email"><i class="icon-mail"></i><span>{$item.email}</span></div>
            </div>
            <div class="mark">{$t.new}</div>
        </div>
        <div class="item-content">
            <div class="text">
                <p>{$item.message}</p>
            </div>
        </div>
    </div>
    <div class="column-r">
        <div class="control">
            <ul class="date-time">
                <li>
                    <i class="icon-calendar"></i>
                    <span>{$item.dd}</span>
                </li>
                <li>
                    <i class="icon-clok"></i>
                    <span>{$item.hh}</span>
                </li>
            </ul>
            {if $item.more != ''}
            <a href="{$item.more}" class="review"><span>{$t.more}</span><i class="icon-arrow-right"></i></a>
            {/if}
        </div>
    </div>
</div><!-- .item -->