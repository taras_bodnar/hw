<div class="l-message {($type == 'worker' )? 'l-page-request' : 'l-request'}">
    <div class="container">
        {*<div class="header">*}
            {*<select>*}
                {*<option value="Підтвердженні">Підтвердженні</option>*}
                {*<option value="Більше коментарів">Більше коментарів</option>*}
                {*<option value="Спочатку дешевші">Спочатку дешевші</option>*}
                {*<option value="Спочатку дорожчі">Спочатку дорожчі</option>*}
            {*</select>*}
        {*</div>*}
        <div class="content" id="orders">
            {$items}
        </div>
        <div class="footer">
            <div class="col-3">
                {if $total>0}
                    <a class="btn icons-png-red icons-around b-more-orders" data-p="{$start}" href="{$root_id}">
                        <span>{$t.b_more_orders}</span>
                    </a>
                {/if}
            </div>
            <div class="col-9 m-pagination" id="pagination">
                {$pagination}
            </div>
        </div>
    </div>
</div>