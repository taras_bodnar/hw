<section class="l-page-message">
    <div class="container">

        {if $smarty.session.app.user.users_group_id == 3}
            <p>{$t.users_orders_no_res_clients}</p>
            <a href="3" class="btn red">{$t.find_specialist}</a>
        {else}
            <p>{$t.users_orders_no_res}</p>
            <a href="7" class="btn red">{$t.to_profile}</a>
        {/if}

    </div>
</section><!-- .l-title-page-->
