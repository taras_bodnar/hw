<div class="l-page-personal-information">
    <div class="container">
        {*<pre>{print_r($ui)}</pre>*}

        <div class="row">
            <div class="m-form-box">
                <div class="column-l">
                    <form method="post" id="usersProfile" action="ajax/Users/profile">
                        <div class="row group-input">
                            <label for="data_surname" class="input-label">{$t.ui_surname}</label>

                            <div class="input tooltip-right" title="{$t.profile_pib_info}">
                                <input type="text" data-error="{$t.e_user_surname}" name="data[surname]"
                                       id="data_surname" required value="{$ui.surname}">
                            </div>
                        </div>
                        <div class="row group-input">
                            <label for="data_name" class="input-label">{$t.ui_name}</label>

                            <div class="input">
                                <input class="m-tooltip" data-error="{$t.e_user_surname}" name="data[name]"
                                       id="data_name" required type="text" value="{$ui.name}">
                            </div>
                        </div>
                        <div class="row group-input">
                            <label for="data_email" class="input-label">{$t.ui_email}</label>

                            <div class="input">
                                <input type="email" data-error="{$t.user_login_e_bad_email}" name="data[email]" required
                                       id="data_email" value="{$ui.email}">
                            </div>
                        </div>
                        <div class="row group-input">
                            <label for="data_url" class="input-label">{$t.ui_url}</label>

                            <div class="input">
                                <input type="text" data-error="{$t.e_user_url}" name="data[url]" required
                                       id="data_url" value="{$ui.url}" onkeyup="this.value=this.value.replace(/[^\w\s?-]/g,'-'); this.value=this.value.replace('--','-'); this.value=this.value.replace(' ','-')">
                            </div>
                        </div>

                        <div class="row group-input">
                            <label for="data_phone" class="input-label">{$t.ui_phone}</label>

                            <div class="input">
                                <input type="text" name="data[phone]" required id="data_phone" value="{$ui.phone}">
                            </div>
                        </div>
                        <div class="row group-input input-spec-field">
                        <div class="row group-input input-spec-field">
                            <label class="input-label " for="users_group_id">{$t.ui_group}</label>

                            <div class="input ">
                                <select name="data[users_group_id]" id="users_group_ids" required>
                                    {*<option value=""></option>*}
                                    {foreach $group as $item}
                                        <option {if $ui.users_group_id == $item.id}selected{/if} value="{$item.id}">{$item.name}</option>
                                    {/foreach}
                                </select>
                            </div>
                        </div>
                        {*<div class="row group-input input-spec-field">*}
                        {*<label class="input-label" for="data_city_id">{$t.ui_city}</label>*}
                        {*<div class="input">*}
                        {*<select id="data_city_id" data-placeholder="{$t.cities}" name="data[city_id][]" >*}
                        {*<option value=""></option>*}
                        {*{foreach $city as $item}*}
                        {*<option {if $ui.city_id == $item.id}selected{/if} value="{$item.id}">{$item.name}</option>*}
                        {*{/foreach}*}
                        {*</select>*}
                        {*</div>*}
                        {*</div>*}
                        {*{$pay_city}*}
                        {*<pre>{print_r($pay_city)}*}
                        {for $foo = 0 to $pay_city}
                            {*<pre>{print_r($city)}*}
                            <div class="row group-input input-spec-field">
                                <label {if $city[$foo]["main_city"]}style="font-weight: bold;"{/if} class="input-label" for="data_city_id_{$foo}">{$t.ui_city}</label>

                                <div class="input">
                                    <select id="data_city_id_{$foo}" data-placeholder="{$t.ui_city}" name="data[city_id][]" required>
                                        <option value=""></option>
                                        {foreach $city as $item}
                                            {if in_array($item.id, $selected) && $item.id != $selected[$foo]}
                                                {continue}
                                            {/if}
                                            <option {if $item.id == $selected[$foo]}selected{/if} value="{$item.id}">{$item.name}</option>
                                        {/foreach}
                                    </select>
                                </div>
                            </div>
                        {/for}
                        {if $ui.gold!=1}
                            {*<a href="#" class="getMoreCities">Оплатити ще місто</a>*}
                        {/if}

                        <div class="row group-input input-currency-field">
                            <label class="input-label" for="languages_id">{$t.email_notification_lang}</label>
                            <div class="input">
                                <select class="tooltip-right" id="data_languages_ids" name="data[languages_id]">
                                    {foreach $languages as $lang}
                                        <option value="{$lang.id}" {if $ui.languages_id == $lang.id}selected{/if}>{$lang.name}</option>
                                    {/foreach}
                                </select>
                            </div>
                        </div>

                        <div class="row group-input input-currency-field">
                            <label class="input-label" for="currency_id">{$t.sel_currency}</label>

                            <div class="input">
                                <select class="tooltip-right" data-placeholder="{$t.currency}"
                                        title="{$t.i_price_per_day}" id="data_currency_ids" name="data[currency_id]">
                                    <option value=""></option>
                                    {foreach $currency as $item}
                                        <option {if $ui.currency_id == $item.id}selected{/if}
                                                value="{$item.id}">{$item.code} ({$item.symbol})
                                        </option>
                                    {/foreach}
                                </select>
                            </div>
                        </div>
                        <div class="row" id="default-prices">
                            <div class="row group-input">
                                <label class="input-label" for="price_per_hour">{$t.price_per_hour}</label>

                                <div class="input">
                                    <input type="text" id="price_per_hour" class="tooltip-right"
                                           title="{$t.i_price_per_day}" name="data[price_per_hour]"
                                           value="{$ui.price_per_hour}" required>
                                </div>
                            </div>
                            <div class="row group-input">
                                <label class="input-label" for="price_per_day">{$t.price_per_day}</label>

                                <div class="input">
                                    <input type="text" id="price_per_day" name="data[price_per_day]"
                                           value="{$ui.price_per_day}" required>
                                </div>
                            </div>
                        </div>
                        {*<div class="row group-input" id="p_per_service" style="display: none;">*}
                            {*<label class="input-label" for="price_per_service">{$t.p_per_service}</label>*}

                            {*<div class="input">*}
                                {*<input type="text" id="price_per_service" name="data[price_per_day]"*}
                                       {*value="{$ui.price_per_day}" required>*}
                            {*</div>*}
                        {*</div>*}
                        {*<div class="row group-input" id="p_per_place" style="display: none;">*}
                            {*<label class="input-label" for="price_per_place">{$t.p_per_place}</label>*}

                            {*<div class="input">*}
                                {*<input type="text" id="price_per_place" name="data[price_per_day]"*}
                                       {*value="{$ui.price_per_day}" required>*}
                            {*</div>*}
                        {*</div>*}

                        <div class="row group-input">
                            <label class="input-label">{$t.discount_per_week}</label>

                            <div class="input">
                                {foreach $discounts as $item}
                                    <label><input {if $ui.discount_per_week == $item.name}checked{/if}
                                                  name="data[discount_per_week]" type="radio"
                                                  value="{$item.name}"><span>{$item.name}</span></label>
                                {/foreach}
                            </div>
                        </div>
                        <div class="row group-input">
                            <label class="input-label">{$t.discount_per_month}</label>

                            <div class="input">
                                {foreach $discounts as $item}
                                    <label><input {if $ui.discount_per_month == $item.name}checked{/if}
                                                  name="data[discount_per_month]" type="radio"
                                                  value="{$item.name}"><span>{$item.name}</span></label>
                                {/foreach}
                            </div>
                        </div>
                        <div class="row group-input">
                            <label class="input-label">{$t.discount_per_2_month}</label>

                            <div class="input">
                                {foreach $discounts as $item}
                                    <label><input {if $ui.discount_per_2_month == $item.name}checked{/if}
                                                  name="data[discount_per_2_month]" type="radio"
                                                  value="{$item.name}"><span>{$item.name}</span></label>
                                {/foreach}
                            </div>
                        </div>

                        {*
                                                <div class="row group-input">
                                                    <label for="data_phone" class="input-label">Facebook ID</label>
                                                    <div class="input tooltip-right" title="{$t.facebook_id}">
                                                        <input type="text" name="oauth[facebook]"  id="oauth_facebook" value="{$ui.oauth.facebook}">
                                                    </div>
                                                </div>
                                                <div class="row group-input">
                                                    <label for="data_phone" class="input-label">VK ID</label>
                                                    <div class="input tooltip-right" title="{$t.vk_id}">
                                                        <input type="text" name="oauth[vk]"  id="oauth_vk" value="{$ui.oauth.vk}">
                                                    </div>
                                                </div>
                                                <div class="row group-input">
                                                    <label for="data_phone" class="input-label">Google+ ID</label>
                                                    <div class="input tooltip-right" title="{$t.google_id}">
                                                        <input type="text" name="oauth[google]"  id="oauth_google" value="{$ui.oauth.google}">
                                                    </div>
                                                </div>
                        *}
                        <div class="row group-input">
                            <label class="input-label" for="data_site">{$t.ui_site}</label>

                            <div class="input">
                                <input type="text" id="data_site" name="data[site]" value="{$ui.site}">
                            </div>
                        </div>
                        <div class="row group-input">
                            <label class="input-label" for="data_blog">{$t.ui_blog}</label>

                            <div class="input">
                                <input type="text" id="data_blog" name="data[blog]" value="{$ui.blog}">
                            </div>
                        </div>
                        <div class="row group-input">
                            <label class="input-label" for="data_facebook">Facebook</label>

                            <div class="input">
                                <input type="text" id="data_facebook" name="data[facebook]" value="{$ui.facebook}">
                            </div>
                        </div>
                        <div class="row group-input">
                            <label class="input-label" for="data_vk">Vk</label>

                            <div class="input">
                                <input type="text" id="data_vk" name="data[vk]" value="{$ui.vk}">
                            </div>
                        </div>
                        <div class="row group-input">
                            <label class="input-label" for="data_instagram">Instagram</label>

                            <div class="input">
                                <input type="text" name="data[instagram]" id="data_instagram" value="{$ui.instagram}">
                            </div>
                        </div>
                        <div class="row group-input">
                            <label class="input-label" for="data_info">{$t.ui_info}</label>

                            <div class="input">
                                <textarea id="data_info" name="data[info]">{$ui.info}</textarea>
                            </div>
                        </div>

                        <div class="row send">
                            <button class="btn red" name="bSubmit" id="bSubmit" type="submit">{$t.b_save}</button>
                        </div>

                        <input type="hidden" name="skey" value="{$skey}">
                        <input type="hidden" name="process" value="1">
                        <input type="hidden" name="gid" value="{$ui.users_group_id}">
                    </form>
                </div>
                <div class="row">
                    <div class="response"></div>
                </div>
            </div>
            <div class="column-r">
                <form method="post" enctype="multipart/form-data" name="usersAvatar" action="ajax/Users/avatar"
                      id="usersAvatar">
                    <div class="m-upload-photo">
                        <input type="file" name="avatar" id="data_avatar" data-img="{$ui.avatar}"
                               onchange="document.usersAvatar.bbSubmit.click();"/>
                    </div>
                    <input type="hidden" name="skey" value="{$skey}">
                    <button class="btn red" style="display: none;" name="bbSubmit" id="bbSubmit"
                            type="submit"></button>
                </form>
                <button type="button" class="btn red" id="changePassword" style="width: 212px; margin: 20px 0">{$t.change_password}</button>
            </div>
        </div>

    </div>
    {*<pre>{print_r($ui)}*}
    <div class="profile-bottom">
        {*{if $ui.hot!=1 || $ui.gold!=1}*}
            {*{if $ui.gold!=1}*}
                <div class="item hot">
                    <div class="image">
                        <img src="{$template_url}assets/img/new-1.jpg" alt="">
                    </div>
                    <div class="item-content">
                        <div class="table">
                            <div class="table-cell">
                                <div class="middle">
                                    {if $ui.hot!=1}
                                        <h3>{$t.hot_title}</h3>
                                        <a href="javascript:;" data-type="hot" class="btn green getHotPropovse">
                                            <span class="btn-i"></span>
                                            {$t.hot_btn}
                                        </a>
                                        <div class="link">
                                            <a href="javascript:;">{$t.ab_more}</a>
                                        </div>
                                    {else}
                                        <h3>{str_replace('{title}','Hot',$t.your_account_paid)}</h3>
                                        <p>{$t.paid_account_expire} {$dateDiff} {$t.days}</p>
                                    {/if}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            {*{/if}*}
            {*{if $ui.hot!=1}*}
                <div class="item gold">
                    <div class="image">
                        <img src="{$template_url}assets/img/new-2.jpg" alt="">
                    </div>
                    <div class="item-content">
                        <div class="table">
                            <div class="table-cell">
                                <div class="middle">
                                    {if $ui.gold!=1}
                                        <h3>{$t.gold_title}</h3>
                                        <a href="javascript:;" data-type="gold" class="btn green getHotPropovse">
                                            <span class="btn-i"></span>
                                            {$t.gold_btn}
                                        </a>
                                    {else}
                                        <h3>{str_replace('{title}','Gold',$t.your_account_paid)}</h3>
                                        <p>{$t.paid_account_expire} {$dateDiffGold} {$t.days}</p>
                                        <div class="link">
                                            <a href="javascript:;" data-type="gold" class="getHotPropovse">{$t.b_send_request}</a>
                                        </div>
                                    {/if}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            {*{/if}*}

        {*{/if}*}
        <div class="remove-profile">
            <div class="container">
                <h3>{$t.del_profile_title}</h3>
                <a class="remove" id="aDelProfile" href="#">{$t.realy_delete_account}</a>
                {$rm_profile}
            </div>
        </div>
    </div>
</div>
