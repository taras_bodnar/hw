{*<pre>{print_r($items)}</pre>*}
<ul class="lang">
    {foreach $items as $item}
        <li>
            <a class="{if $id == $item.id }active{/if}" href="{$page.id};l={$item.id};">{strtoupper($item.code)}</a>
        </li>
    {/foreach}
</ul>