<div class="frame-menu-main horizontal-menu">
    <div class="container">
        <div class="menu-main menu-row-category">
            <nav>
                <table>
                    <tbody>
                    <tr>
                        {foreach $items as $item}
                        <td>
                            <div class="frame-item-menu {if $page.id==$item.id}active{/if}">
                                <div class="frame-title {if $item.isfolder}is-sub{/if}">
                                    <a href="{$item.id}" title="{$item.title}" class="title {if $page.id==$item.id}active{/if}">
                                        <span class="helper"></span>
                                        <span class="text-el">{$item.name}</span>
                                    </a>
                                </div>
                                {if $item.isfolder && isset($item.children)}
                                    <div class="frame-drop-menu left-drop">
                                        <ul class="items">
                                            <li class="column_0">
                                                <a href="{$item.id}" class="title-category-l1 ">
                                                    <span class="text-el">{$t.cm_see_all}</span>
                                                </a>
                                            </li>
                                            {foreach $item.children as $c0}
                                                <li class="column_0">
                                                    <a href="{$c0.id}" title="{$c0.title}" class="title-category-l1 {if $c0.isfolder}is-sub{/if} {if $page.id==$c0.id}active{/if}">
                                                        <span class="text-el">{$c0.name}</span>
                                                    </a>
                                                    {if $c0.isfolder && isset($c0.children)}
                                                        <div class="frame-l2">
                                                            <ul class="items">
                                                                {foreach $c0.children as $c1}
                                                                <li class="column2_0" style="display: block;">
                                                                    <a  href="{$c1.id}"  title="{$c1.title}">{$c1.name}</a>
                                                                </li>
                                                                {/foreach}
                                                            </ul>
                                                        </div>
                                                    {/if}
                                                </li>
                                            {/foreach}
                                        </ul>
                                    </div>
                                {/if}
                            </div>
                        </td>
                        {/foreach}
                    </tr>
                    </tbody>
                </table>
            </nav>
        </div>
    </div>
</div>