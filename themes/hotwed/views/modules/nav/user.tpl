<ul>
    {foreach $items as $i=>$item}
        <li{if $i == 0} class="home"{/if}>
            <a class="nav-item-{$item.id}" href="{$item.id}" title="{$item.title}">{$item.name}<span class="tn"></span></a>
        </li>
    {/foreach}
</ul>