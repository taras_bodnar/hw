<ul class="menu">
    {foreach $items as $i=>$item}
        <li{if $i == 0} class="home"{/if}><a href="{$item.id}" title="{$item.title}">{$item.name}</a></li>
    {/foreach}
</ul>