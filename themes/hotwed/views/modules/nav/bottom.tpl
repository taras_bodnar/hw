<ul>
    {foreach $items as $i=>$item}
        <li{if $id == $item.id} class="active"{/if}><a href="{$item.id}" title="{$item.title}">{$item.name}</a></li>
    {/foreach}
</ul>