<div class="m-module m-left-menu">
    <div class="m-content">
        <ul class="level-1" id="rules">
            {foreach $categories as $item}
            <li {if $page.id==$item.id}class="active"{/if}><a href="{$item.id}" title="{$item.title}">{{$item.name}}</a>
                {if $item.isfolder}
                    <ul class="level-2" style="display: none;">
                        {foreach $item.children as $child}
                            <li {if $page.id==$child.id}class="active"{/if} ><a href="{$child.id}" title="{$child.title}">{$child.name}</a>
                                {if $child.isfolder}
                                    <ul class="level-3" style="display: none;">
                                        {foreach $child.children as $cc}
                                            <li {if $page.id==$cc.id}class="active"{/if}><a href="{$cc.id}" title="{$cc.title}">{$cc.name}</a></li>
                                        {/foreach}
                                    </ul>
                                {/if}
                            </li>
                        {/foreach}
                    </ul>
                {/if}
            </li>
            {/foreach}
        </ul>
    </div>
</div><!-- .m-module -->