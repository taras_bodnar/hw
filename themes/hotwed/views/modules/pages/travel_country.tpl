<div class="items-container">
    {foreach $items as $item}
    <div class="item">
        <div class="item-midle">
            <div class="img">
                <img src="{$item.img.src}" alt="{$item.img.alt}"/>
            </div>
            <a href="{$item.id}" title="{$item.title}">
                <div class="middle">
                    <div class="table">
                        <div class="table-cell">
                            <div class="title">{$item.name}</div>
                            <div class="number">{$item.count} {$item.plural}</div>
                        </div>
                    </div>
                </div>
            </a></div>
    </div><!-- .item -->
    {/foreach}
</div>

