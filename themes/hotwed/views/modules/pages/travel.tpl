{*<pre>{print_r($items)}*}
<div class="items-container">
{foreach $items as $item}
    <div class="item">
        <div class="item-midle">
            <div class="img">
                <img src="{$item.img.src}" alt="{$item.img.alt}" width="321" height="207"/>
            </div>
            <a href="javascript:void(0);" title="{$item.title}"  class="item-link no-click">
                <div class="middle">
                    <div class="table">
                        <div class="table-cell">
                            <div class="title">{$item.name}</div>
                            <div class="number">{$item.description}</div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="tour-info">
            <div class="tour-dates">
                <span>{$item.date.from} - {$item.date.to}</span>
            </div>
            <div class="actual-price">
                <span>$ <span class="tour-price">{$item.travel_price|round}</span></span>
            </div>
            {if $item.travel_old_price|round!=0}
            <div class="old-price">
                <span>$ <span class="tour-price">{$item.travel_old_price|round}</span></span>
            </div>
            {/if}

            <input type="hidden" id="skey" name="skey" value="{$skey}">
            <input type="hidden" id="travel_name" name="travel_name" value="{$item.name}">
            <input type="hidden" id="travel_price" name="travel_price" value="{$item.travel_price}">
            <input type="hidden" id="travel_date" name="travel_date" value="{$item.date.from} - {$item.date.to}">
            <button class="reserve">
                {$t.reserve}
            </button>
        </div>
    </div><!-- .item -->
{/foreach}
</div>