<section class="l-news">
    <div class="container">
        <div class="list-news" id="newsContent">
            {*{$items}*}
            {foreach $items as $item}
                <div class="item">
                    <div class="midle">
                        <div class="img">
                            <a href="{$item.id}" title="{$item.title}" ><img src="{$item.img.src}" alt=""/></a>
                        </div>
                        <div class="date"><span>{$item.pub_date}</span></div>
                        <div class="title">
                            <a href="{$item.id}" title="{$item.title}" >{$item.name}</a>
                        </div>
                        <p>{$item.description}</p>
                    </div>
                </div><!-- .item -->
            {/foreach}
        </div>
        {*<div class="footer">*}
            {*<div class="col-3">*}
                {*{if $total>0}*}
                    {*<a class="btn icons-png-red icons-around b-more-news" data-p="{$start}" href="#">*}
                        {*<span>{$t.b_news_more}</span>*}
                    {*</a>*}
                {*{/if}*}
            {*</div>*}
            {*<div class="col-9 m-pagination" id="pagination">*}
                {*{$pagination}*}
            {*</div>*}
        {*</div>*}
    </div>
</section><!-- .l-title-page-->

