<div class="feedback pop-up-middle"> 
    <h2>{$t.fb_intro}</h2>
    <div class="response"></div>
    <form id="feedback" method="post" action="ajax/Feedback/process">
        <div class="row input-box">
            <label for="data_name">{$t.u_name}</label>
            <input type="text" name="data[name]" required id="data_name"/>
        </div>
        <div class="row input-box">
            <label for="data_email">{$t.u_email}</label>
            <input type="email" name="data[email]" required id="data_email"/>
        </div>
        <div class="row input-box">
            <label for="data_message">{$t.fb_message}</label>
            <textarea required name="data[message]" id="data_message"></textarea>
        </div>      
        <div class="row footer">
            <div class="row">
                <button class="btn red" id="bSubmit" type="submit">{$t.b_send}</button>
            </div>
        </div>
        <input type="hidden" name="skey" value="{$skey}" >
    </form>
    <script>new Users()</script>
</div>