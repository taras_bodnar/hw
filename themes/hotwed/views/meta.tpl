<!-- (c) Developed by Otakoyi.com | http://www.otakoyi.com/ -->
<!-- (c) Powered by OYi.Engine | http://www.engine.otakoyi.com/ -->
<base href="{$appurl}" />
{if !isset($article)}
    <title>{if empty($meta)}{$page.title}{if $page.author}: {$page.author} {/if}{if $page.tag} / {$t.tag_title}: {$page.tag} {/if}{else}{$meta.title}{/if}</title>
    <meta name="keywords" content="{if empty($meta)}{$page.keywords}{if $page.author} : {$page.author} {/if}{if $page.tag} / {$t.tag_title}: {$page.tag} {/if}{else}{$meta.keywords}{/if}"/>
    <meta name="description" content="{if empty($meta)}{$page.description}{if $page.author} : {$page.author} {/if}{if $page.tag} / {$t.tag_title}: {$page.tag} {/if}{else}{$meta.description}{/if}"/>
{else}
    <title>{$article.name}</title>
    <meta name="keywords" content="{if empty($meta)}{$article.content|mb_substr:0:200}{else}{$meta.keywords}{/if}"/>
    <meta name="description" content="{if empty($meta)}{$article.content|mb_substr:0:200}{else}{$meta.description}{/if}"/>
{/if}
<meta name="generator" content="OYi.Engine 6">

<meta name="Reply-to" content="support@otakoyi.com" />

{if $page.canonical}
    <link href="{if !empty($smarty.server.HTTPS)}https://{else}http://{/if}{$smarty.server.HTTP_HOST}/{$page.canonical}" rel="canonical">
    {*<link rel="canonical" href="{$page.canonical}">*}
{/if}

<meta name="document-state"           content="Dynamic" />
<meta name="document-rights"          content="Public Domain" />
<meta name="distribution"             content="Global" />
{*<pre>{print_r($photo)}*}
{if isset($photo)}
    <meta itemprop="name" content="{$page.name} {$photo.username} {if $photo.photo_day!=0}{$photo.photo_day|date_format:"%d-%m-%Y"}{/if}">
    <meta property="og:title" content="{$page.title}">
    <meta property="og:description" content="{$photo.username} {if $photo.photo_day!=0}{$photo.photo_day|date_format:"%d-%m-%Y"}{/if}">
    <meta property="og:url" content="http://{$smarty.server.SERVER_NAME}/photo/{$photo.id}">
    <meta property="og:image" content="http://{$smarty.server.SERVER_NAME}{$photo.big_path}" />
{elseif !isset($article)}
    <meta itemprop="name" content="{$page.name}{if $page.author}: {$page.author} {/if}{if $page.tag} / {$t.tag_title}: {$page.tag} {/if}">
    <meta itemprop="description" content="{$page.description}{if $page.author}: {$page.author} {/if}{if $page.tag} / {$t.tag_title}: {$page.tag} {/if}">
    <meta property="og:title" content="{$page.title}{if $page.author}: {$page.author} {/if}{if $page.tag} / {$t.tag_title}: {$page.tag} {/if}">
    <meta property="og:type" content="Page">
    <meta property="og:url" content="http://{$smarty.server.SERVER_NAME}/{$page.url}{if $page.id==45}/{$userId}{/if}">
    <meta property="og:description" content="{$page.description}{if $page.author}: {$page.author} {/if}{if $page.tag} / {$t.tag_title}: {$page.tag} {/if}">
{else}
    <meta itemprop="name" content="{$article.name}">
    <meta itemprop="description" content="{$article.content|mb_substr:0:200}}">
    <meta property="og:title" content="{$article.title}">
    <meta property="og:type" content="Forum">
    <meta property="og:url" content="http://{$smarty.server.SERVER_NAME}/forum/article/{$article.id}">
    <meta property="og:description" content="{$article.content|mb_substr:0:200}">
    <meta property="og:image" content="{$template_url}assets/img/images/logo(200x200).png">
{/if}


