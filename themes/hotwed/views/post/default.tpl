{*
 * Company Otakoyi.com.
 * Author Демоверсія OYi.Engine
 * Date: 17.03.2015
 * Time: 14:09
 * Name: Default
 * Description: Шаблон статті по замовчуванню
 *}

[[chunk:head]]
<div class="l-wrapper">
    [[chunk:header]]
    <main class="l-main" role="main">
        <section class="l-title">
            <div class="row">
                <h1>{$page.name}</h1>
            </div>
        </section><!-- .l-title-page-->

        <section class="l-page">
            <div class="container">
                <div class="cms_content">
                    {$page.content}
                </div>
                <div class="date">
                    [[mod:Blog::postDate]]
                </div>

                <script type="text/javascript">(function() {
                        if (window.pluso)if (typeof window.pluso.start == "function") return;
                        if (window.ifpluso==undefined) { window.ifpluso = 1;
                            var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
                            s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
                            s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://share.pluso.ru/pluso-like.js';
                            var h=d[g]('body')[0];
                            h.appendChild(s);
                        }})();</script>
                <div class="pluso" data-background="#ebebeb" data-options="medium,square,line,horizontal,counter,theme=04" data-services="vkontakte,facebook,twitter,google"></div>
            </div>

        </section><!-- .l-title-page-->
        [[mod:Blog::relatedPost]]
    </main><!-- .l-main -->
</div><!-- .l-wrapper -->
[[chunk:footer]]