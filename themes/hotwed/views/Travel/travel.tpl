{*
 * Company Otakoyi.com.
 * Author Taras
 * Date: 17.09.2015
 * Time: 12:17
 * Name: Список Країн подорожей
 * Description: Список Країн подорожей
 *}
[[chunk:head]]
<div class="l-wrapper">
    [[chunk:header]]


    <main class="l-main" role="main">
        <section class="l-title">
            <div class="row">
                <div class="container"><h1>{$page.name}</h1></div>
            </div>
        </section><!-- .l-title-page-->

        <section class="l-resort">
            [[mod:Pages::getTravelCountry({$page.id},travel)]]

            <h3>{$t.chooseothertravel}</h3>

            {*<form method="post" id="tour-filter" class="tour-filter">*}
                {*<table>*}
                    {*<tr>*}
                        {*<td>*}
                            {*<label>*}
                                {*<span>Тип тура:</span>*}
                                {*<select class="tour-type">*}
                                    {*<option>Пакетні</option>*}
                                    {*<option>Не пакетні</option>*}
                                {*</select>*}
                            {*</label>*}
                        {*</td>*}
                        {*<td>*}

                            {*<label>*}
                                {*<span>Ночи</span>*}
                                {*от*}
                                {*<select>*}
                                    {*<option>6</option>*}
                                    {*<option>7</option>*}
                                    {*<option>8</option>*}
                                {*</select>*}
                            {*</label>*}

                            {*<label>*}
                                {*до*}
                                {*<select>*}
                                    {*<option>10</option>*}
                                    {*<option>11</option>*}
                                    {*<option>12</option>*}
                                {*</select>*}
                            {*</label>*}
                        {*</td>*}
                    {*</tr>*}
                    {*<tr>*}
                        {*<td>*}
                            {*<label>*}
                                {*<span>Страна:</span>*}
                                {*<select class="tour-country">*}
                                    {*<option>Турция</option>*}
                                    {*<option>Азербайджан</option>*}
                                {*</select>*}
                            {*</label>*}
                        {*</td>*}
                        {*<td>*}

                            {*<label>*}
                                {*<span>Дата вылета</span>*}
                                {*с*}
                                {*<select>*}
                                    {*<option>11.10.2015</option>*}
                                    {*<option>11.10.2015</option>*}
                                    {*<option>11.10.2015</option>*}

                                {*</select>*}
                            {*</label>*}
                            {*<label>*}
                                {*по*}
                                {*<select>*}
                                    {*<option>11.10.2015</option>*}
                                    {*<option>11.10.2015</option>*}
                                    {*<option>11.10.2015</option>*}
                                {*</select>*}
                            {*</label>*}
                        {*</td>*}
                    {*</tr>*}
                    {*<tr>*}
                        {*<td>*}

                            {*<label>*}
                                {*<span>Отель</span>*}
                                {*<input type="checkbox" form="tour-filter"/>2*}
                            {*</label>*}
                            {*<label>*}
                                {*<input type="checkbox" form="tour-filter"/>3*}
                            {*</label>*}
                            {*<label>*}
                                {*<input type="checkbox" form="tour-filter"/>4*}
                            {*</label>*}
                            {*<label>*}
                                {*<input type="checkbox" form="tour-filter"/>5*}
                            {*</label>*}
                        {*</td>*}
                        {*<td>*}
                            {*<label>*}
                                {*<span>Вылет из</span>*}
                                {*<select class="tour-flight-start">*}
                                    {*<option>Одеса</option>*}
                                    {*<option>Не Одеса</option>*}
                                    {*<option>Одеса</option>*}
                                {*</select>*}
                            {*</label>*}
                        {*</td>*}
                    {*</tr>*}
                    {*<tr>*}
                        {*<td>*}

                            {*<label>*}
                                {*<span>Цена</span>*}
                                {*от*}
                                {*<input type="text" class="tour-price-range" />*}
                            {*</label>*}
                            {*<label>*}
                                {*до*}
                                {*<input type="text" class="tour-price-range" />*}
                            {*</label>*}
                            {*<label>*}
                                {*<select>*}
                                    {*<option>USD</option>*}
                                {*</select>*}
                            {*</label>*}
                        {*</td>*}
                        {*<td>*}
                            {*<input type="submit" class="tour-filter-submit" form="tour-filter" value="Найти"/>*}
                        {*</td>*}
                    {*</tr>*}
                    {*<tr>*}
                        {*<td>*}
                            {*<label>*}
                                {*<input type="checkbox" form="tour-filter" class="flight-included"/>*}
                                {*<span>Туры с включенным перелетом</span>*}
                            {*</label>*}
                        {*</td>*}
                    {*</tr>*}
                {*</table>*}
            {*</form>*}

            {*<div id="tour_search_module" class="another-travels-form"></div>*}
            {*<h4>Або оберіть іншу подорож</h4>*}
            <iframe src="html/travelForm.html" class="another-travels-form"></iframe>

            {*<script src="http://module.ittour.com.ua/tour_search.jsx?id=8D7023G4480565817313N870&ver=2&type=2970"></script>*}

            <article class="resort-description">
              {$page.content}
            </article>
        </section><!-- .l-category-specialists-->

    </main><!-- .l-main -->
</div><!-- .l-wrapper -->

[[chunk:footer]]