{*
 * Company Otakoyi.com.
 * Author Тарас
 * Date: 26.04.2016
 * Time: 09:05
 * Name: Список фоток
 * Description: Список фоток
 *}
[[chunk:head]]
<div class="l-wrapper">
    [[chunk:header]]
    <main class="l-main photo-page" role="main">

        <section class="l-title">
            <div class="row">
                <div class="container">
                    <h1>{$page.name}</h1>
                </div>
            </div>
        </section>

       [[mod:PhotoDay::get]]

        {$page.module}

        {if !empty($page.content)}
            <section class="l-page">
                <div class="container">
                    <div class="cms_content">
                        {$page.content}
                    </div>
                </div>
            </section><!-- .l-title-page-->
        {/if}
        [[mod:Blog::widget]]
    </main><!-- .l-main -->
</div><!-- .l-wrapper -->
[[chunk:footer]]