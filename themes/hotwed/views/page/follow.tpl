{*
 * Company Otakoyi.com.
 * Author Taras
 * Date: 20.04.2016
 * Time: 16:35
 * Name: Слідкую/Ігнорую
 * Description: Слідкую/Ігнорую
 *}
[[chunk:head]]
<div class="l-wrapper">
    [[chunk:header]]
    <main class="l-main" role="main">
        <section class="l-title">
            <div class="row">
                <h1>{$page.name}</h1>
            </div>
        </section><!-- .l-title-page-->


        <section class="l-people-list forum-page">
            <div class="container">
                [[mod:Forum::getCategory]]
                [[mod:Forum::getArticles]]


            </div>
        </section>
    </main><!-- .l-main -->
</div><!-- .l-wrapper -->
[[chunk:footer]]