{*
 * Company Otakoyi.com.
 * Author Тарас
 * Date: 26.04.2016
 * Time: 17:57
 * Name: Фото
 * Description: Фото
 *}
{*<pre>{print_r($photo)}*}
[[chunk:head]]
<div class="l-wrapper">
    [[chunk:header]]
    <main class="l-main photo-page" role="main">

        [[mod:PhotoDay::info]]
        [[mod:PhotoDay::getComments]]


        [[mod:PhotoDay::bestPhoto]]

    </main><!-- .l-main -->
</div><!-- .l-wrapper -->
[[chunk:footer]]