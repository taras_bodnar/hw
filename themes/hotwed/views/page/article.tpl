{*
 * Company Otakoyi.com.
 * Author Taras
 * Date: 20.04.2016
 * Time: 10:44
 * Name: article
 * Description: article
 *}
[[chunk:head]]
<div class="l-wrapper">
    [[chunk:header]]
    <main class="l-main" role="main">
        <section class="l-title">
            <div class="row">
                <h1>{$article.name}</h1>
            </div>
        </section><!-- .l-title-page-->


        [[mod:Forum::article]]
        {*{if !empty($article.content)}*}
            {*<section class="l-page">*}
                {*<div class="container">*}
                    {*<div class="cms_content">*}
                        {*{$article.content}*}
                    {*</div>*}
                {*</div>*}
            {*</section><!-- .l-title-page-->*}
        {*{/if}*}
        {*[[mod:Blog::widget]]*}
    </main><!-- .l-main -->
</div><!-- .l-wrapper -->
[[chunk:footer]]