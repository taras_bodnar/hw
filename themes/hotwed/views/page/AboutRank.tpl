{*
 * Company Otakoyi.com.
 * Author Administrator
 * Date: 10.02.2018
 * Time: 03:07
 * Name: About Rating
 * Description: About Rating
 *}
{*
 * Author Олег
 * Date: 10.02.2018
 * Time: 12:21
 * Name: AboutRang
 *}

<div class="l-wrapper">
        
    <main class="l-main" role="main">
        <section class="l-title">
            <div class="row">
                <h1>{$page.name}</h1>
            </div>
        </section><!-- .l-title-page-->

        {$page.module}

        {if !empty($page.content)}
        <section class="l-page">
            <div class="container">
                <div class="cms_content">
                    {$page.content}
                </div>
            </div>
        </section><!-- .l-title-page-->

    </main><!-- .l-main -->
</div><!-- .l-wrapper -->

