{*
 * Company Otakoyi.com.
 * Author Володимир
 * Date: 10.07.2015
 * Time: 14:11
 * Name: Контакти
 * Description: Контакти
 *}

[[chunk:head]]
<div class="wrapper">
    <main>
        [[chunk:header]]
        <section class="contacts">
            <div class="container-fixed-width has-padding">
                <h1>{$page.name}</h1>
                <div class="contact-box">
                    {$page.content}
                    [[mod:Feedback]]
                </div>
                <div class="side-contact">
                    <h2>{$t.contacts_time}</h2>
                    <div class="bg-box">
                        {$t.contacts_phones}
                    </div>
                    <div class="link-to-filter">
                        <a href="7" class="d-link">
                            <span></span>
                            <span>{$t.filter_tires_by_params}</span>
                        </a>
                    </div>
                    <div class="link-to-filter">
                        <a href="8" class="t-link">
                            <span></span>
                            <span>{$t.filter_disc_by_params}</span>
                        </a>
                    </div>
                </div>
            </div>
        </section>
        {$page.module}
    </main>
</div>
[[chunk:footer]]
</body>
</html>