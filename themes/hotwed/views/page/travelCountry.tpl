{*
 * Company Otakoyi.com.
 * Author Taras
 * Date: 17.09.2015
 * Time: 12:17
 * Name: Список Країн подорожей
 * Description: Список Країн подорожей
 *}
[[chunk:head]]
<div class="l-wrapper">
    [[chunk:header]]


    <main class="l-main" role="main">
        <section class="l-title">
            <div class="row">
                <div class="container"><h1>{$page.name}</h1></div>
            </div>
        </section><!-- .l-title-page-->

        <section class="l-travel-places">
            [[mod:Pages::getTravelCountry({$page.id},travel_country)]]
        </section><!-- .l-category-specialists-->

    </main><!-- .l-main -->
</div><!-- .l-wrapper -->

[[chunk:footer]]