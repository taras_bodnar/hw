{*
 * Company Otakoyi.com.
 * Author Taras
 * Date: 14.03.2016
 * Time: 15:47
 * Name: Нова тема
 * Description: Нова тема
 *}
[[chunk:head]]
<div class="l-wrapper">
    [[chunk:header]]
    <main class="l-main" role="main">
        <section class="l-title">
            <div class="row">
                <h1>{$page.name}</h1>
            </div>
        </section><!-- .l-title-page-->

        {if $smarty.get.id}
            [[mod:Forum::edit({$smarty.get.id})]]
            {else}
             [[mod:Forum::create]]
        {/if}




        {*{$page.module}*}

        <!-- .l-title-page-->
        {*<section class="l-page">*}
            {*<div class="container">*}
                {*<div class="wrapper">*}
                    {*<div class="create-filter">*}
                        {*<div class="create-filter-row">*}
                            {*<div class="label"><h2>Свторюєм </h2></div>*}
                            {*<div class="right-col">*}
                                {*<ul class="create-filter-buttons">*}
                                    {*<li class="topic-type-1">*}
                                        {*<button type="button" class="switcher-button first-child active" data-topic="1">*}
                                            {*разговор*}
                                        {*</button>*}
                                    {*</li>*}
                                    {*<li class="topic-type-3">*}
                                        {*<button type="button" class="switcher-button" data-topic="3">Подія</button>*}
                                    {*</li>*}
                                {*</ul>*}
                            {*</div>*}
                        {*</div>*}
                        {*<form id="forum_topic_add_form" name="forum_topic_add_form" action="/forum/all/add/"*}
                              {*method="post" enctype="multipart/form-data" onsubmit="return forum_topic.validateForm();">*}
                            {*<input type="hidden" value="2" name="in_forum_topic" id="in_forum_topic">*}

                            {*<div class="create-filter-row">*}
                                {*<div class="brd input-xxxlarge">*}
                                    {*<input name="forum_topic_name" type="text" value="" maxlength="120"*}
                                           {*placeholder="Название темы">*}
                                {*</div>*}
                            {*</div>*}
                            {*<div id="meet_cnt" class="hidden">*}
                                {*<div class="create-filter-row ind">*}
                                    {*<div class="label">Событие</div>*}
                                    {*<div class="right-col">*}
                                        {*<div class="line-form">*}
                                            {*<div class="cusel width" id="cuselFrame-meet_type" style="width:129px"*}
                                                 {*tabindex="0">*}
                                                {*<div class="cuselFrameRight"></div>*}
                                                {*<div class="cuselText">Мастер-класс</div>*}
                                                {*<div class="cusel-scroll-wrap"*}
                                                     {*style="height: 270px; display: none; visibility: visible;">*}
                                                    {*<div class="cusel-scroll-pane" id="cusel-scroll-meet_type"*}
                                                         {*style="height: 270px;"><span val="1" class="cuselActive">Мастер-класс</span><span*}
                                                                {*val="2">Семинар</span><span val="3">Вебинар</span><span*}
                                                                {*val="4">Выставка</span><span*}
                                                                {*val="5">Фестиваль</span><span val="6">Форум</span><span*}
                                                                {*val="7">Лекция</span><span val="8">Конкурс</span><span*}
                                                                {*val="9">Акция</span><span val="10">Встреча</span><span*}
                                                                {*val="11">Поездка</span></div>*}
                                                {*</div>*}
                                                {*<input type="hidden" id="meet_type" name="meet_type" value="1"></div>*}
                                        {*</div>*}
                                    {*</div>*}
                                {*</div>*}
                                {*<div class="create-filter-row ind z-2">*}
                                    {*<div class="label">Город</div>*}
                                    {*<div class="right-col">*}
                                        {*<div class="brd">*}
                                            {*<input name="" id="autocomplete_live" type="text" value=""*}
                                                   {*placeholder="Начните вводить название города..." autocomplete="off">*}
                                        {*</div>*}
                                        {*<input type="hidden" id="autocomplete_city_live_input" value=""*}
                                               {*name="forum_topic_city">*}
                                        {*<input type="hidden" id="autocomplete_country_live_input" value=""*}
                                               {*name="forum_topic_country">*}
                                    {*</div>*}
                                {*</div>*}
                                {*<div class="create-filter-row">*}
                                    {*<div class="label">Адрес</div>*}
                                    {*<div class="right-col">*}
                                        {*<div class="brd">*}
                                            {*<input type="text" name="forum_topic_place" value="" maxlength="128">*}
                                        {*</div>*}
                                    {*</div>*}
                                {*</div>*}
                                {*<div class="create-filter-row high">*}
                                    {*<div class="label">Начало</div>*}
                                    {*<div class="right-col">*}
                                        {*<div class="b-datepicker">*}
                                            {*<div class="datepicker-button" id="datepicker-button-2">*}
                                                {*<span class="ins-text">Выбрать</span>*}
                                                {*<input class="datepicker hasDatepicker" name="" type="hidden" value=""*}
                                                       {*id="date">*}
                                                {*<button type="button" class="ui-datepicker-trigger">...</button>*}
                                            {*</div>*}
                                        {*</div>*}
                                        {*<div class="movable-indicator day">*}
                                            {*<div class="grad first">Утро</div>*}
                                            {*<div class="grad sec">День</div>*}
                                            {*<div class="grad four">Вечер</div>*}
                                            {*<div class="grad last">Ночь</div>*}
                                            {*<div class="indicator ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all"*}
                                                 {*id="time" aria-disabled="false">*}
                                                {*<div class="ui-slider-range ui-widget-header ui-corner-all ui-slider-range-min"*}
                                                     {*style="width: 59.2593%;"></div>*}
                                                {*<a class="ui-slider-handle ui-state-default ui-corner-all" href="#"*}
                                                   {*style="left: 59.2593%;"><span*}
                                                            {*class="indicator-amount"><i></i><strong>16:40</strong></span></a>*}
                                            {*</div>*}
                                        {*</div>*}
                                    {*</div>*}
                                {*</div>*}
                                {*<div class="create-filter-row high">*}
                                    {*<div class="label">Окончание</div>*}
                                    {*<div class="right-col">*}
                                        {*<div class="b-datepicker">*}
                                            {*<div class="datepicker-button" id="datepicker-button-2">*}
                                                {*<span class="ins-text">Выбрать</span>*}
                                                {*<input class="datepicker hasDatepicker" name="" type="text" value=""*}
                                                       {*id="datepicker">*}

                                            {*</div>*}
                                        {*</div>*}
                                        {*<div class="movable-indicator day">*}
                                            {*<div class="grad first">Утро</div>*}
                                            {*<div class="grad sec">День</div>*}
                                            {*<div class="grad four">Вечер</div>*}
                                            {*<div class="grad last">Ночь</div>*}
                                            {*<div class="indicator ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all"*}
                                                 {*id="time_to" aria-disabled="false">*}
                                                {*<div class="ui-slider-range ui-widget-header ui-corner-all ui-slider-range-min"*}
                                                     {*style="width: 81.4815%;"></div>*}
                                                {*<a class="ui-slider-handle ui-state-default ui-corner-all" href="#"*}
                                                   {*style="left: 81.4815%;"><span*}
                                                            {*class="indicator-amount"><i></i><strong>20:40</strong></span></a>*}
                                            {*</div>*}
                                        {*</div>*}
                                    {*</div>*}
                                {*</div>*}
                            {*</div>*}
                            {*<div class="create-filter-descrip ">*}
                                {*<div class="brd-texta">*}
                                    {*<textarea name="forum_topic_text" maxlength="10000" data-autosize-on="true"*}
                                              {*style="overflow: hidden; word-wrap: break-word; height: 127px;"></textarea>*}
                                {*</div>*}
                            {*</div>*}
                            {*<div class="b-uploader">*}
                                {*<p class="text-p">Можно загрузить <span id="photo_counter_span">35</span> фотографий*}
                                    {*(jpg, png, gif)*}
                                {*</p>*}

                                {*<div class="forumFile" id="forumFile">*}

                                {*</div>*}
                            {*</div>*}

                            {*<div class="upload-title">*}

                            {*</div>*}


                            {*<div class="uploading-photos" id="photos_cnt">*}
                                {*<div class="uploading-photos-promt" id="explaining-help" style="display: none;">*}
                    {*<span>*}
                        {*<i class="ico"></i>*}
                        {*Для управления порядком фотографий — перетягивайте их мышкой.*}
                    {*</span>*}
                                {*</div>*}
                                {*<div class="uploading-photos-list">*}
                                    {*<ul class="uploading-photos-list ui-sortable"></ul>*}
                                {*</div>*}
                            {*</div>*}
                            {*<input type="hidden" id="forum_topic_minute_to" name="forum_topic_minute_to" maxlength="2"*}
                                   {*value="40">*}

                            {*<div class="add-new-video">*}
                                {*<p>Добавить YouTube или Vimeo видео</p>*}

                                {*<div class="video-list">*}
                                    {*<ul>*}
                                        {*<li class="list-item hidden">*}
                                            {*<div class="delete">*}
                                                {*<button class="default-button" onclick="video.remove();" type="button">*}
                                                    {*Удалить*}
                                                {*</button>*}
                                            {*</div>*}
                                            {*<div class="play-ico"></div>*}
                                            {*<div class="opacity"></div>*}
                                            {*<div class="video">*}
                                                {*<img alt="" src="">*}
                                            {*</div>*}
                                        {*</li>*}
                                        {*<li class="list-item">*}
                                            {*<div class="upload-video">*}
                                                {*<div class="brd">*}
                                                    {*<input placeholder="Ссылка на видео"*}
                                                           {*id="forum_topic_video_user_url">*}
                                                {*</div>*}
                                                {*<button class="default-button" type="button"*}
                                                        {*onclick="video.get($('#forum_topic_video_user_url').val());">*}
                                                    {*Загрузить*}
                                                {*</button>*}
                                            {*</div>*}
                                        {*</li>*}
                                    {*</ul>*}
                                {*</div>*}
                                {*<ul class="buttons-group clr">*}
                                    {*<li><span>Сделать тему видимой</span></li>*}
                                    {*<li>*}
                                        {*<button class="switcher-button first-child active" type="button">всім</button>*}
                                    {*</li>*}
                                    {*<li>*}
                                        {*<button class="switcher-button" type="button">тільки виконавцям</button>*}
                                    {*</li>*}
                                {*</ul>*}
                                {*<button type="button" class="default-button-large fixed"*}
                                        {*onclick="$('#forum_topic_add_form').submit();">Готово*}
                                {*</button>*}
                            {*</div>*}
                        {*</form>*}
                    {*</div>*}
                {*</div>*}
            {*</div>*}
        {*</section>*}
        {*[[mod:Blog::widget]]*}
    </main><!-- .l-main -->
</div><!-- .l-wrapper -->
[[chunk:footer]]

<script>
    var video = {
        youtubeCode: null,
        vimeoCode: null,
        type: null,
        img: null,
        url: null,
        get: function(url) {
            this.url = url;
            if (this.isYoutube()) {
                this.type = 1;
            }
            if (this.isVimeo()) {
                this.type = 2;
            }
            switch (this.type) {
                case 1:
                    this.img = 'http://img.youtube.com/vi/' + this.youtubeCode + '/mqdefault.jpg';
                    $('.video-list img').attr('src', this.img);
                    $('.video-list img').parent().parent().removeClass('hidden');
                    $('.video-list li').eq(1).hide();
                    $('#forum_topic_video').remove();
                    $('.video-list li').eq(1).append('<input type="hidden" id="forum_topic_video" name="forum_topic_video" class="video-field">');
                    $('#forum_topic_video').val(this.url);
                    break;
                case 2:
                    var self = this;
                    $.getJSON("http://vimeo.com/api/v2/video/" + this.vimeoCode + ".json", function(data) {
                        self.img = data[0].thumbnail_medium;
                        $('.video-list img').attr('src', self.img);
                        $('.video-list img').parent().parent().removeClass('hidden');
                        $('.video-list li').eq(1).hide();
                        $('#forum_topic_video').remove();
                        $('.video-list li').eq(1).append('<input type="hidden" id="forum_topic_video" name="forum_topic_video" class="video-field">');
                        $('#forum_topic_video').val(self.url);
                    });
                    break;
                default:
                    break;
            }
        },
        remove: function() {
            this.url = null;
            this.type = null;
            this.vimeoCode = null;
            this.youtubeCode = null;
            this.img = null;
            $('.video-list img').parent().parent().addClass('hidden');
            $('.video-list img').attr('src', '');
            $('#forum_topic_edit_video_delete').val(1);
            $('#forum_topic_video').remove();
            $('#forum_topic_video_user_url').val('');
            $('.video-list li').eq(1).show();
        },
        isYoutube: function() {
            var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
            var match = this.url.match(regExp);
            if (match&&match[7].length==11){
                this.youtubeCode = match[7];
                return true;
            } else{
                return false;
            }
        },
        isVimeo: function() {
            var regExpStandart = /http(s)?:\/\/(www\.)?vimeo.com\/(\d+)(\/)?(#.*)?/;
            var matchStandart = this.url.match(regExpStandart);
            var regExpChannel = /http(s)?:\/\/(www\.)?vimeo.com\/channels\/(\w+)\/(\d+)(\/)?(#.*)?/;
            var matchChannel = this.url.match(regExpChannel);
            if (matchStandart) {
                this.vimeoCode = matchStandart[3];
                return true;
            } else if(matchChannel) {
                this.vimeoCode = matchChannel[4];
                return true;
            } else {
                return false;
            }
        }
    };
</script>