{*
 * Company Otakoyi.com.
 * Author Володимир
 * Date: 16.07.2015
 * Time: 10:51
 * Name: Новини
 * Description: Список новин
 *}{*
 * Company Otakoyi.com.
 * Author Будяк
 * Date: 09.06.2015
 * Time: 12:21
 * Name: main
 * Description: main
 *}
{*
 * Company Otakoyi.com.
 * Author Volodymyr
 * Date: 09.06.2015
 * Time: 12:21
 * Name: main
 * Description: main
 *}
[[chunk:head]]
<div class="l-wrapper">
    [[chunk:header]]
    <main class="l-main" role="main">
        <section class="l-title">
            <div class="row">
                <div class="container"><h1>{$page.name}</h1></div>
            </div>
        </section><!-- .l-title-page-->

        {*{$page.module}*}
        [[mod:News::getNews]]

        {*<section class="l-page">*}
            {*<div class="container">*}
                {*<div class="cms_content">*}
                    {*{$page.content}*}
                {*</div>*}
                {*{if $page.isfolder == 0}*}
                {*<div class="date">[[mod:News::pubDate]]</div>*}
                {*{/if}*}

                {*<script type="text/javascript">(function() {*}
                        {*if (window.pluso)if (typeof window.pluso.start == "function") return;*}
                        {*if (window.ifpluso==undefined) { window.ifpluso = 1;*}
                            {*var d = document, s = d.createElement('script'), g = 'getElementsByTagName';*}
                            {*s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;*}
                            {*s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://share.pluso.ru/pluso-like.js';*}
                            {*var h=d[g]('body')[0];*}
                            {*h.appendChild(s);*}
                        {*}})();</script>*}
                {*<div class="pluso" data-background="#ebebeb" data-options="medium,square,line,horizontal,counter,theme=04" data-services="vkontakte,facebook,twitter,google"></div>*}
            {*</div>*}
        {*</section><!-- .l-title-page-->*}
        {*[[mod:Blog::relatedPost]]*}

    </main><!-- .l-main -->
</div><!-- .l-wrapper -->
[[chunk:footer]]
