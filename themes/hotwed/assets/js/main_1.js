var $_GET = {}, lazyInstance;
function buildQuery(href)
{
    href = href || window.location.search;
    var parts = href.substr(1).split("&");
    for (var i = 0; i < parts.length; i++) {
        var temp = parts[i].split("=");

        if(temp[0] == '' || temp[0] == 'undefined') continue;
        $_GET[decodeURIComponent(temp[0])] = decodeURIComponent(temp[1]);
    }


    //console.log(temp[0].replace('[','.').substr(0,temp[0].length - 1 ));
}
var App = {
    screenH : $(window).height(),
    screenW : $(window).width(),
    h100 : $('.h100'),
    owl : $('.owl-carousel'),
    lFooter : $(".l-footer"),
    lMain : $(".l-main"),
    backTop: $(".m-scroll-top"),
    hotwedPreloader : $('#hotwed-preloader'),
    scrollTop: 0,
    initFast : function(){
        this.height100();
        App.preloaderAnimation();
    },
    init : function(){
        this.footerHeight();
        this.searchPanel();
        this.menu.init();
        this.formStyler();
        this.scrollUp();
        this.goNext.init();

        App.loadPage();


        if($("#filter_city_id, #filter_gid, #data_city_id, #users_group_id, #data_currency_id").length){

            var elementNoSearch = $("#filter_city_id, #filter_gid, #data_city_id, #users_group_id, #data_currency_id");
            //var element = $("#filter_city_id, #filter_gid, #data_city_id, #users_group_id, #data_currency_id");

            var elementWithSearch = $('#filter_city_id');

            var placeholder = $(elementNoSearch).data("placeholder");

            $("#filter_gid, #data_city_id, #users_group_id, #data_currency_id").select2({
                minimumResultsForSearch: Infinity,
                placeholder: placeholder

            });

            elementWithSearch.select2({

            });
        }

        function progress(){
            var el = $('.l-account .progress');
            var pl = el.attr('data-progress');
            el.find('span').css('width', pl + '%')
        }
        if($('.l-account').length){
            progress();
            console.log('adbasdbasfb afdbabsfbsdf')
        }
        //$('.l-account progress')

        $('.authorised').on('click', function(){
            $('.m-sm-cabinet').addClass('open');
            $('body').addClass('overflow-hidden');
            return false;
        });
        $('.close-m-cabinet').on('click', function(){
            $('.m-sm-cabinet').removeClass('open');
            $('body').removeClass('overflow-hidden');
            return false;
        });
        
        if( $("#datepicker,.datepicker").length){
            this.date.init();
        }

        this.hBlock.init();


        if( $(".tabs").length){
            this.tabs.init();
        }

        if( $(".pop-up, .pop-up-2").length){
            this.popUp.init();
        }

        if( $(".m-tooltip").length){
            this.tooltipstered.init();
        }

        if(this.owl.length){
            this.carusel.caruselDestroy();
        }
        this.works.init();

        if($('#calendar').length){
            this.calendar.init();
        }
        buildQuery();
        //Dropzone.autoDiscover = false;  
        //$(".zone-upload-photo").dropzone({
        //    url: "assets/img",  
        //    maxFilesize: 1,
        //    dictRemoveFile: false
        //});       

        $(".m-scroll-top").on('click', function (){
            $('html,body').animate( { scrollTop: 0 }, 1100 );
        });

        $('#blogCat').change(function(){
            var h = $(this).find('option:selected').data('href');
            self.location.href=h;
        });

        $('.b-more-posts').click(function(e){
            e.preventDefault();
            var $this = $(this);
            $.ajax({
                url: 'ajax/Blog/more',
                type: 'post',
                dataType: 'json',
                data: {
                    p: $this.data('p'),
                    id: window.page.id,
                    skey: SKEY
                },
                success: function(d){
                    $('#blogItems').append(d.items);
                    $('#pagination').html(d.pagination);
                    $this.data('p', d.p);
                    if(d.t == 0) $this.hide();
                }
            });
        });

        $('.b-more-news').click(function(e){
            e.preventDefault();
            var $this = $(this);
            $.ajax({
                url: 'ajax/News/more',
                type: 'post',
                dataType: 'json',
                data: {
                    p: $this.data('p'),
                    id: window.page.id,
                    skey: SKEY
                },
                success: function(d){
                    $('#newsContent').append(d.items);
                    $('#pagination').html(d.pagination);
                    $this.data('p', d.p);
                    if(d.t == 0) $this.hide();
                }
            });
        });

        $('.b-more-notify').click(function(e){
            e.preventDefault();
            var $this = $(this);
            $.ajax({
                url: 'ajax/UsersNotifications/more',
                type: 'post',
                dataType: 'json',
                data: {
                    p: $this.data('p'),
                    //id: window.page.id,
                    skey: SKEY
                },
                success: function(d){
                    $('#notifications').append(d.items);
                    $('#pagination').html(d.pagination);
                    $this.data('p', d.p);
                    if(d.t == 0) $this.hide();
                }
            });
        });

        var wp = false;
        $('.b-more-workers').click(function(e){
            //console.log($_GET.filter);
            var $this = $(this);
            e.preventDefault();
            if(wp) return false;
            wp=true;
            $.ajax({
                url: 'ajax/Catalog/more',
                type: 'post',
                dataType: 'json',
                data: {
                    p: $this.data('p'),
                    id: window.page.id,
                    order: $_GET.order,
                    filter: $_GET,
                    skey: SKEY
                },
                success: function(d){
                    if(d.t == 0 || d == '') $this.hide();
                    $(d.items).each(function(i,e){
                        $('#catalogWorkers').append(e.html);
                    });
                    //$('#catalogWorkers').append(d.items);
                    $('#pagination').html(d.pagination);
                    $this.data('p', d.p);

                    doLazy();
                }
            });
        });


        $('.nav-item-13').click(function(e){
            e.preventDefault();
            $.ajax({
                url: 'ajax/Users/logout',
                data: {
                    skey : SKEY
                },
                type: 'post',
                success: function(d) {
                    if(d){
                        self.location.href='/';
                    }
                }
            });
        });

        //
        var wl = new Wishlist(); wl.init();
        var o = new Orders(); o.init();
        //console.log('i online? ',I_ONLINE);
        if(I_ONLINE){
            getNotifications();
        }
        $('.b-write-message').click(function(e){
            if(I_ONLINE){
                return true;
            }

            $('.login-btn').click();
            e.preventDefault();
        });
    },
    resize : function(){
        this.screenH = $(window).height();
        this.screenW = $(window).width();
        this.height100();
        this.carusel.caruselDestroy();
        this.footerHeight();
        this.hBlock.init();
        this.works.init();
    },
    height100 : function ()
    {
        this.h100.css('height', App.screenH);
    },
    loadPage : function ()
    {
            App.hotwedPreloader.fadeOut(1000,function ()
            {
                setTimeout(function(){
                    App.hotwedPreloader.remove();
                }, 300);
            });
        $('.l-wrapper').fadeTo('slow', 1);
    },
    preloaderAnimation : function()
    {
        var wraper = App.hotwedPreloader,
            heart = wraper.find(".heart"),
            i1 = wraper.find('.i-1'),
            i2 = wraper.find('.i-2'),
            i3 = wraper.find('.i-3'),
            tl = new TimelineMax({repeat:1}),
            speed = 0.5;
            
            
            
        function long(){
            tl.from(i1, 0.8, { left:"-=200px", top:"-=200px", alpha:0, scale:1.8, ease:Power1.easeInOut})
                .from(i2, 0.8, { left:"+=200px", top:"-=200px", alpha:0, scale:1.8, ease:Power1.easeInOut}, 0)
                .from(i3, 0.8, {top:"-=30px", rotation:"-40deg", alpha:0, scale:1.8, ease:Back.easeOut}, 0.8)
                .from(heart, 1, {scaleX:1.2, scaleY:1.2, ease:Elastic.easeOut, duration:1, repeat:-1, repeatDelay:speed}, 1);
        }

        long();

        function small(){
            tl.to(heart, 1, {scaleX:1.2, scaleY:1.2, ease:Elastic.easeOut, duration:1, repeat:-1, repeatDelay:speed })
        }

    },
    works : {
        video: $(".video"),
        init : function(){
            if(this.video.length){
                this.go();
            }
        },
        go : function(){
            return;
            this.video.each(function(){
                var $this = $(this);
                setTimeout(function(){
                    $this.css('height', $this.width());
                }, 300)

            });
        }
    },
    goNext : {
        go : $(".l-header-home").find('.arrow'),
        h : 0,
        init : function(){
            App.goNext.go.on('click', function(){
                App.goNext.h = App.lMain.find('section').offset().top;
                $('html,body').animate({scrollTop: App.goNext.h}, 1100);
            });
        }
    },
    hBlock : {
        b : $("#blogItems").find('.item'),
        init : function(){
            if(App.hBlock.b.length){
                App.hBlock.go();
            }
        },
        go : function(){
            var maxHeight = -1;
            setTimeout(function(){
                App.hBlock.b.each(function() {
                    maxHeight = maxHeight > $(this).outerHeight() ? maxHeight : $(this).outerHeight();
                });

                App.hBlock.b.each(function() {
                    $(this).height(maxHeight);
                });

                //console.log(maxHeight);

                if(App.screenW > 767){
                    App.hBlock.b.css('height', maxHeight);
                } else {
                    App.hBlock.b.css('height', 'auto');
                }
            }, 100);
        }
    },
    tooltipstered : {
        tooltipError :  $('.tooltip-error'),
        tooltipTop :  $('.tooltip-top'),
        tooltipRight : $('.tooltip-right'),
        init : function ()
        {
            if(this.tooltipError.length) {
                this.tooltipError.tooltipster({
                    position: 'right',
                    animation: 'grow',
                    theme: 'tooltipster-error'
                });
                //this.tooltipError.tooltipster('show');
            }

            if(this.tooltipRight.length){
                this.tooltipRight.tooltipster({
                    position: 'right',
                    animation: 'grow',
                    theme: 'tooltipster-info'
                });
                //this.tooltipRight.tooltipster('show');
            }

            if(this.tooltipTop.length){
                this.tooltipTop.tooltipster({
                    position: 'top',
                    animation: 'grow',
                    theme: 'tooltipster-arrow-top'
                });
            }
        }
    },
    date : {
        datepicker : $( "#datepicker,.datepicker" ),
        datepickerBox : $('#datepickerBox'),
        init : function ()
        {
            App.date.datepicker.datepicker({
                dateFormat: 'dd/mm/yy',
                minDate: 0,
                defaultDate: 0,
                beforeShow:function(input, inst){
                    App.date.datepickerBox.append(inst.dpDiv);
                    inst.dpDiv.hide();
                    setTimeout(function () {
                        inst.dpDiv.css({
                            top: 64,
                            left: 0
                        });
                    }, 0);
                }
            });
        }
    },
    carusel : {
        owl1 : $("#slider-news"),
        owl2 : $("#good-know"),
        owl3 : $("#portfolio-carusel"),
        init : function ()
        {
            function carusel(obj, option)
            {
                obj.owlCarousel(option);

                obj.prev().find(".btn-next").click(function(){
                    obj.trigger('owl.next');
                });
                obj.prev().find(".btn-prev").click(function(){
                    obj.trigger('owl.prev');
                });
            }

            var option1 = {
                items: 3,
                itemsDesktop : [1200,3],
                itemsDesktopSmall : [992,2],
                itemsTablet: [768,2],
                itemsMobile : false,
                responsive : true,
                lazyLoad : true,
                responsiveRefreshRate: 200,
                afterAction: function(){
                    if ( this.itemsAmount > this.visibleItems.length ) {
                        $('.btn-next').show();
                        $('.btn-prev').show();

                    } else {
                        $('.btn-next').hide();
                        $('.btn-prev').hide();
                    }
                }
            };

            var option2 = {
                items: 1,
                itemsDesktop : [1200,1],
                itemsDesktopSmall : [992,1],
                itemsTablet: [768,1],
                itemsMobile : [320,1],
                responsive : true,
                lazyLoad : true,
                responsiveRefreshRate: 200,
                afterInit : function ()
                {
                    App.carusel.owl3.find(".item-image").find('a').fancybox({
                        maxWidth	: "80%",
                        maxHeight	: "80%",
                        fitToView	: false,
                        autoSize	: false,
                        closeClick	: false,
                        margin: [0, 0, 0, 0],
                        padding     : 0,
                        closeBtn    : true,
                        openEffect	: 'elastic',
                        closeEffect	: 'elastic',
                        wrapCSS     : "pop-up-style",
                        helpers: {
                            overlay: {
                                locked: false
                            }
                        }
                    });
                },
                afterAction: function(){
                    if ( this.itemsAmount > this.visibleItems.length ) {
                        $('.btn-next').show();
                        $('.btn-prev').show();

                    } else {
                        $('.btn-next').hide();
                        $('.btn-prev').hide();
                    }
                }

            };

            carusel(this.owl1, option1);
            carusel(this.owl2, option1);
            carusel(this.owl3, option2);

        },
        caruselDestroy : function ()
        {
            if(App.screenW < 768) {
                if(typeof App.carusel.owl1.data('owlCarousel') != 'undefined') {
                    App.carusel.owl1.data('owlCarousel').destroy();
                }

                if(typeof App.carusel.owl2.data('owlCarousel') != 'undefined') {
                    App.carusel.owl2.data('owlCarousel').destroy();
                }

                if(this.owl3.length){
                    App.carusel.init();
                }

            } else {
                App.carusel.init();
            }
        }
    },
    formStyler : function ()
    
    // #users_group_id,
    //var selectInit = $('select').not('#data_city_id,  #data_currency_id, #filter_gid, #filter_city_id ');
    {
        var selectInit = $('select').not('#data_city_id, #data_currency_id, #users_group_id, #filter_gid, #filter_city_id');
        //var inputInit = $('input[type="radio"]').not('#selImg');

        //$('input:not(#selImg), select:not(#data_city_id),  select').styler({
        if(selectInit.length) {
            $(selectInit).styler({
                selectSmartPositioning : false,
                selectPlaceholder : '',
                singleSelectzIndex : 'auto',
                fileBrowse: '<span class="btn red icons-image"><span>'+lang.fileBrowse+'</span></span>',
                filePlaceholder: '<div class="table"><div class="table-cell"><i class="icon-plus_circle"></i><div class="upload-title">'+lang.filePlaceholder+'</div></div></div>'
            });
        }


        //if($('input:not(#selImg)').length){
        //    $('input:not(#selImg)').styler({
        //        selectSmartPositioning : false,
        //        selectPlaceholder : '',
        //        singleSelectzIndex : 'auto',
        //        fileBrowse: '<span class="btn red icons-image"><span>'+lang.fileBrowse+'</span></span>',
        //        filePlaceholder: '<div class="table"><div class="table-cell"><i class="icon-plus_circle"></i><div class="upload-title">'+lang.filePlaceholder+'</div></div></div>'
        //    });
        //}    


       
            $('input:not(#selImg)').styler({
                selectSmartPositioning : false,
                selectPlaceholder : '',
                singleSelectzIndex : 'auto',
                fileBrowse: '<span class="btn red icons-image"><span>'+lang.fileBrowse+'</span></span>',
                filePlaceholder: '<div class="table"><div class="table-cell"><i class="icon-plus_circle"></i><div class="upload-title">'+lang.filePlaceholder+'</div></div></div>'
            });
      


        // styler for all inputs

    },
    footerHeight : function ()
    {
        /**
         * Прижимаємо футер до низу екрану - адаптивність
         */
        var a = this.lFooter.innerHeight();
        this.lMain.css({
            "padding-bottom" : a
        });
        this.lFooter.css({
            "margin-top" : -a
        });
    },
    searchPanel : function ()
    {
        $(".m-button-search").on('click', function(event){
            event.preventDefault();
            $("#search-panel").addClass("open");
            $("html").addClass("html-fixed");

        });
        $(".close-search-panel").on('click', function(event){
            event.preventDefault();
            $("#search-panel").removeClass("open");
            $("html").removeClass("html-fixed");
        });
    },
    scrollUp: function () {
        if (App.scrollTop > 300 && App.backTop.hasClass("default")) {
            App.backTop
                .removeClass("default")
                .addClass("hatch");
        } else if (App.scrollTop <= 300 && App.backTop.hasClass("hatch")) {
            App.backTop
                .removeClass("hatch")
                .addClass("default");
        }
    },
    calendar : {
        init : function ()
        {
            $('#calendar').fullCalendar({
                lang: window.page.langCode,
                events: {
                    url: '/ajax/Calendar/getEvents',
                    type: 'POST',
                    data: {
                        skey: SKEY
                    },
                    error: function() {
                        alert('there was an error while fetching events!');
                    }
                    //color: 'yellow',   // a non-ajax option
                    //textColor: 'black' // a non-ajax option
                },
                eventRender: function(event, element) {
                    element.qtip({
                        content: event.description
                    });
                },
                dayClick: function(date, jsEvent, view) {
                    var day = $(this);
                    //alert('Clicked on: ' + date.format());
                    //$(this).css('background-color', '#3a87ad');
                    $.ajax({
                        url: 'ajax/Calendar/dayClick',
                        dataType:'json',
                        data:{
                            date: date.format(),
                            skey: SKEY
                        },
                        type:'post',
                        success: function(d)
                        {
                            var event = {
                                title:     lang.is_disabled,
                                allDay:    true,
                                start:     date.format(),
                                className: 'i-disabled'
                            };
                            if(d.s == 1){
                                $('#calendar').fullCalendar( 'renderEvent', event, true)
                            } else if(d.s == -1){
                                $('#calendar').fullCalendar('removeEvents', function (calEvent) {
                                    if(calEvent.start._i == date.format()){
                                        return true;
                                    }
                                });
                            }

                        }
                    });
                }
            });
            $( "#ddf" ).datepicker({
                dateFormat: 'yy-mm-dd',
                defaultDate: 0,
                //changeMonth: true,
                numberOfMonths: 3,
                onClose: function( selectedDate ) {
                    $( "#ddt" ).datepicker( "option", "minDate", selectedDate );
                },
                beforeShow:function(input, inst){
                    $('#dpbox').append(inst.dpDiv);
                    inst.dpDiv.hide();
                    setTimeout(function () {
                        inst.dpDiv.css({
                            top: 64,
                            left: 0
                        });
                    }, 0);
                }
            });
            $( "#ddt" ).datepicker({
                dateFormat: 'yy-mm-dd',
                defaultDate: 0,
                //changeMonth: true,
                numberOfMonths: 3,
                onClose: function( selectedDate ) {
                    $( "#ddf" ).datepicker( "option", "maxDate", selectedDate );
                },
                beforeShow:function(input, inst){
                    $('#dpbox').append(inst.dpDiv);
                    inst.dpDiv.hide();
                    setTimeout(function () {
                        inst.dpDiv.css({
                            top: 64,
                            left: 0
                        });
                    }, 0);
                }
            });
            /*
             $('#mydate').datepicker({
             dateFormat: 'yy-mm-dd',
             minDate: 0,
             beforeShow:function(input, inst){
             $('#dpbox').append(inst.dpDiv);
             inst.dpDiv.hide();
             setTimeout(function () {
             inst.dpDiv.css({
             top: 64,
             left: 0
             });
             }, 0);
             }
             });*/
            var bSubmit = $('#bSubmit');
            $('#oDate').ajaxForm({
                beforeSend: function(){
                    bSubmit.attr('disabled', true);
                },
                success: function(d)
                {
                    bSubmit.removeAttr('disabled');
                    if(d){
                        $('#oDate').resetForm();
                        $('#calendar').fullCalendar( 'refetchEvents' );
                    }
                }
            });
        }
    },
    menu : {
        MainMenu : $(".main-menu"),
        mMainMenu : $(".m-main-menu"),
        buttonMainMenu : $(".button-main-menu"),
        closeMainMenu : $(".close-main-menu"),
        init : function ()
        {
            this.buttonMainMenu.on('click', function ()
            {
                App.menu.MainMenu.addClass('open');
                $("body").addClass('overflow-hidden');
                TweenMax.to(App.menu.MainMenu, 0.5, {left:"0px", ease:Power1.easeOut});
            });
            this.closeMainMenu.on('click', function ()
            {
                App.menu.MainMenu.removeClass('open');
                $("body").removeClass('overflow-hidden');
                TweenMax.to(App.menu.MainMenu, 0.5, {left:"-100%", ease:Power1.easeOut});
            });
            this.MainMenu.on('click', '.dropdown', function ()
            {
                $(this).toggleClass('open').find('.level-2').slideToggle();
                return false;
            });
        }
    },
    tabs : {
        init : function ()
        {
            $('ul.tabs__caption').on('click', 'li:not(.active)', function() {
                $(this)
                    .addClass('active').siblings().removeClass('active')
                    .closest('div.tabs').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
            });
        }
    },
    popUp : {
        init : function ()
        {

            $(".pop-up-2").click(function(){
                var href= $(this).attr('href');
                $.get(href, function(d){
                    $.fancybox({
                        content     : d,
                        maxWidth	: 600,
                        autoSize	: true,
                        padding     : 0,
                        closeBtn    : true,
                        margin: [0, 0, 0, 0],
                        wrapCSS     : "pop-up-style"
                    });
                });
                return false;
            });
        }
    }
};


function Users()
{
    function close()
    {
        $.fancybox.close();
    }

    function displayErrors(msg, inp)
    {
        if(typeof inp != 'undefined'){
            $('input.error').removeClass('error');
            $(inp).each(function(i,e){
                $(e).addClass('error');
            });
        }
        $('.response:visible').removeClass('success').addClass('error').html(msg);
    }

    function displaySuccess(msg)
    {
        $('.response:visible').removeClass('error').addClass('success').html(msg);
    }

    var bSubmit = $('.b-submit');

    $('#usersRegister').ajaxForm({
        dataType: 'json',
        beforeSend: function(){
            bSubmit.attr('disabled', true);
        },
        success: function(d)
        {
            bSubmit.removeAttr('disabled');
            if(d.s){
                if(d.r != ''){
                    self.location.href = d.r;
                } else{
                    window.location.reload(true);
                }
            } else {
                displayErrors(d.m, d.inp);
            }
        }
    });

    $('#usersLogin').ajaxForm({
        dataType: 'json',
        beforeSend: function(){
            bSubmit.attr('disabled', true);
        },
        success: function(d)
        {
            bSubmit.removeAttr('disabled');
            if(d.s){
                //if(d.r != ''){
                //    self.location.href = d.r;
                //}
                window.location.reload(true);
            } else {
                displayErrors(d.m, d.inp);
            }
        }
    });
    $('#usersFp').ajaxForm({
        dataType: 'json',
        beforeSend: function(){
            bSubmit.attr('disabled', true);
        },
        success: function(d)
        {
            bSubmit.removeAttr('disabled');
            if(d.s){
                displaySuccess(d.m);
                $('#usersFp').remove();
            } else {
                displayErrors(d.m, d.inp);
            }
        }
    });

    $("#data_phone").mask("+38(099) 999 99 99");
    if($('#data_avatar').length){
        var img =$('#data_avatar').data('img');
        if(img != ''){

            setTimeout(function(){
                $('.jq-file__name')
                    .css('background', 'url('+img+') no-repeat center center')
                    .find('.table')
                    .hide();
            }, 300);
        }
    }

    $('#usersProfile').ajaxForm({
        dataType: 'json',
        beforeSend: function(){
            bSubmit.attr('disabled', true);
            return validateInput('#data_surname', ['required', {'minlength' : 3}])
                && validateInput('#data_name', ['required', {'minlength' : 3}])
                && validateInput('#data_email', ['required', 'email']);
        },
        success: function(d)
        {
            bSubmit.removeAttr('disabled');
            if(d.s){
                displaySuccess(d.sm);
                $('#usersFp').remove();
                if(d.im != ''){
                    $('.jq-file__name').css('background', 'url("'+ d.im +'") no-repeat center center');
                }
            } else {
                $(d.e).each(function(i,e){
                    displayErrorInput(e.i, e.m);
                });
            }
        }
    });

    $('#usersAvatar').ajaxForm({
        dataType: 'json',
        beforeSend: function(){
            bSubmit.attr('disabled', true);
        },
        success: function(d)
        {
            if(d.im != ''){
                $('.jq-file__name').css('background', 'url("'+ d.im +'") no-repeat center center');
            }
        }
    });

    $('#deleteProfile').ajaxForm({
        dataType: 'json',
        beforeSend: function(){
            $('#bSubmit2').attr('disabled', true);
        },
        success: function(d)
        {
            $('#bSubmit2').removeAttr('disabled');
            if(d.s){
                $('#dpFc').html(d.m);
            }
        }
    });
    $('#users_group_id').change(function(){
        var dp = $('#default-prices'), pps = $('#p_per_service'), ppl = $('#p_per_place');
        var v  = this.value;
        //console.log(v);
        switch (v){
            case '8':
                dp.hide().find('input').attr('disabled', true);
                ppl.hide().find('input').attr('disabled', true);

                // price per service
                pps.show().find('input').removeAttr('disabled');

                break;
            case '13':
                dp.hide().find('input').attr('disabled', true);
                ppl.hide().find('input').attr('disabled', true);

                // price per service
                pps.show().find('input').removeAttr('disabled');

                break;
            case '14':
                dp.hide().find('input').attr('disabled', true);
                ppl.hide().find('input').attr('disabled', true);

                // price per service
                pps.show().find('input').removeAttr('disabled');

                break;
            case '16':
                dp.hide().find('input').attr('disabled', true);
                ppl.hide().find('input').attr('disabled', true);

                // price per service
                pps.show().find('input').removeAttr('disabled');

                break;
            case '17':
                dp.hide().find('input').attr('disabled', true);
                ppl.hide().find('input').attr('disabled', true);

                // price per service
                pps.show().find('input').removeAttr('disabled');

                break;
            case '18':
                dp.hide().find('input').attr('disabled', true);
                ppl.hide().find('input').attr('disabled', true);

                // price per service
                pps.show().find('input').removeAttr('disabled');

                break;
            case '19':
                dp.hide().find('input').attr('disabled', true);
                ppl.hide().find('input').attr('disabled', true);

                // price per service
                pps.show().find('input').removeAttr('disabled');

                break;
            case '12':
                dp.hide().find('input').attr('disabled', true);
                pps.hide().find('input').attr('disabled', true);

                // price per place
                ppl.show().find('input').removeAttr('disabled');

                break;
            default:
                dp.show().find('input').removeAttr('disabled');
                pps.hide().find('input').attr('disabled', true);
                ppl.hide().find('input').attr('disabled', true);
                break;
        }
    }).trigger('change');

    //$('#users_group_id').styler();
    $('#selectGroup').ajaxForm({
        beforeSend: function(){
            bSubmit.attr('disabled', true);
        },
        success: function(d)
        {
            bSubmit.removeAttr('disabled');
            if(d>0){
                location.reload(true);
            }
        }
    });

    var bar = $('.bar'),
        percent = $('.percent'),
        status = $('#status'),
        formUploadImages = $('#formUploadImages'),
        inpSelImg = $('#selImg'),
        progress = $('.progress')
        ;

    formUploadImages.ajaxForm({
        dataType: 'json',
        beforeSend: function() {
            progress.show();
            status.empty();
            var percentVal = '0%';
            bar.width(percentVal)
            percent.html(percentVal);
        },
        uploadProgress: function(event, position, total, percentComplete) {
            var percentVal = percentComplete + '%';
            bar.width(percentVal)
            percent.html(percentVal);
            //console.log(percentVal, position, total);
        },
        success: function(d) {
            var percentVal = '100%';
            bar.width(percentVal)
            percent.html(percentVal);
            progress.hide();
            $(d.images).each(function(i,e){
                $(e).appendTo($('#pf_content'));
            });
            pfSortable();
        }
    });

    $('.b-pf-add-images').click(function(e){
        e.preventDefault();

        inpSelImg.click();
    });

    $('.b-pf-add-video').click(function(e){
        e.preventDefault();
        $.ajax({
            url: 'ajax/Users/uploadVideo',
            type: 'post',
            data:{
                skey: SKEY
            },
            success: function(d)
            {
                $.fancybox({
                    content:d,
                    fitToView	: false,
                    padding     : 0,
                    margin: [0, 0, 0, 0],
                    wrapCSS     : "pop-up-style",
                    closeBtn    : true,
                    openEffect	: 'elastic',
                    closeEffect	: 'elastic',
                    helpers: {
                        overlay: {
                            locked: false
                        }
                    }
                });
                $('#uploadVideo').ajaxForm({
                    dataType: 'json',
                    success: function(d) {
                        if(d.s){
                            close();
                            $(d.images).each(function(i,e){
                                $(e).appendTo($('#pf_content'));
                            });
                            pfSortable();
                        } else{
                            displayErrors(d.m);
                        }
                    }
                });

            }
        });

    });

    inpSelImg.change(function(){
        $('#bSubmit2').click();
    });


    $('.pf-items').fancybox({
        maxWidth	: "80%",
        maxHeight	: "80%",
        fitToView	: false,
        margin: [0, 0, 0, 0],
        wrapCSS     : "pop-up-style",
        padding     : 0,
        closeBtn    : true,
        openEffect	: 'elastic',
        closeEffect	: 'elastic',
        helpers: {
            overlay: {
                locked: false
            }
        }
    });

    pfSortable();

    $(document).on('click', '.rm-pf-item', function(e){
        e.preventDefault();
        var item_id = $(this).attr('id');
        $.ajax({
            url: 'ajax/Users/rmPortfolioItem',
            type: 'post',
            data:{
                id: item_id,
                skey: SKEY
            },
            success: function(d)
            {
                if(d>0){
                    $('#pf-item-' + item_id).hide(function(){$(this).remove();});
                }
            }
        });
    });

    function pfSortable()
    {
        $("#pf_content").sortable({
            update: function (event, ui) {
                /*$("#pf_content").find('.item').each(function(i,e){
                 if(i<5){
                 $(this).addClass('on-main');
                 }else {
                 $(this).removeClass('on-main');
                 }
                 });
                 */
                var order = $(this).sortable('toArray').toString();
                $.ajax({
                    url: 'ajax/Users/portfolioSort',
                    type: 'post',
                    data:{
                        order: order,
                        skey: SKEY
                    },
                    success: function(d)
                    {
                        displaySuccess(d);
                    }
                });
            }
        });
    }
}
function displayErrorInput(input, msg)
{
    var  $this    = $(input),
        rowEl    =$this.parents('.group-input:first'),
        parentEl = $this.parent('.input');

    rowEl.removeClass('success').addClass('error');
    //$this.addClass('tooltip-error').title(msg);
    parentEl.find('i').remove();
    $('<i></i>').addClass('icon-clancel_circle').appendTo(parentEl);
    $this.tooltipster({
        position: 'right',
        animation: 'grow',
        theme: 'tooltipster-error',
        content: msg,
        multiple: true
    });
    $this.tooltipster('show');
}

function validateInput(input, rules)
{
    //console.log('validateInput:' , input);
    var s = true,
        $this = $(input),
        val=$this.val(),
        parentEl = $this.parent('.input'),
        rowEl=$this.parents('.group-input:first'),
    //nameReg    = /^[A-Za-zА-Я]+$/,
    // numberReg  =  /^[0-9]+$/,
        emailReg   = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/,
        phoneReg   = /^[(]{0,1}[0-9]{3}[)]{0,1}[-\s\.]{0,1}[0-9]{3}[-\s\.]{0,1}[0-9]{4}$/
        ;

    function displayError()
    {
        //console.log('call: displayError');
        var msg = $this.data('error');
        rowEl.removeClass('success').addClass('error');
        //$this.addClass('tooltip-error').title(msg);
        parentEl.find('i').remove();
        $('<i></i>').addClass('icon-clancel_circle').appendTo(parentEl);
        //$this.tooltipster('destroy');
        $this.tooltipster({
            position: 'right',
            animation: 'grow',
            theme: 'tooltipster-error',
            content: msg,
            multiple: true
        });
        $this.tooltipster('show');
    }

    function displaySuccess()
    {
        //console.log('call: displaySuccess');
        //var msg = $this.data('success');
        rowEl.removeClass('error').addClass('success');
        parentEl.find('i').remove();
        $('<i></i>').addClass('icon-accept').appendTo(parentEl);
        //$this.tooltipster('disable');
        //$this.tooltipster({
        //    position: 'right',
        //    animation: 'grow',
        //    theme: 'tooltipster-success',
        //    content: msg,
        //    multiple: true
        //});
        //$this.tooltipster('show');
    }

    rules.forEach(function(k, v){
        //console.log('rules: ', k ,' - ', v, typeof k);
        if(k !== null && typeof k === 'object'){
            Object.keys(k).forEach(function(a) {
                var b = k[a];
                //console.log(a,b);
                switch (a){
                    case 'minlength':
                        if(val.length < b){
                            s = false;
                            displayError();
                        } else{
                            displaySuccess();
                        }
                        break;
                }
            });
            return;
        }
        switch (k){
            case 'required':
                //console.log('case: required');
                if(val == ''){
                    s=false;
                    displayError();
                }
                break;
            case 'email':
                //console.log('case: email');
                if(! emailReg.test(val)){
                    s=false;
                    displayError();
                }
                break;
            case 'phone':
                //console.log('case: phone');

                break;
        }
    });

    $this.data('status', s);

    return s;
}

App.initFast();

$(window).on('load',function(){
    App.init();

    $(document).on('click','.pop-up',function(e){
        e.preventDefault();
        $.fancybox.close();
        var href= $(this).attr('href');

        $.ajax({
            type: 'post',
            url : href,
            data: {
                skey: SKEY
            },
            success: function(d)
            {
                $.fancybox({
                    content     : d,
                    maxWidth	: 600,
                    width		: '100%',
                    height		: '100%',
                    autoSize	: true,
                    padding     : 0,
                    closeBtn    : true,
                    margin: [0, 0, 0, 0],
                    wrapCSS     : "pop-up-style",
                    helpers: {
                        overlay: {
                            locked: true
                        }
                    },
                    afterShow : function ()
                    {
                        App.tabs.init();
                        App.formStyler();
                    }
                });
            }
        });
    });

    var u = new Users();
    var c = new Catalog();
    c.init();
    new Messages().init();
    new WorkersOrders().init();
    var processing =false;
    $('.b-more-orders-oh').click(function(e){
        e.preventDefault();
        if(processing) return false;
        processing = true;
        var $this = $(this);
        $.ajax({
            url: 'ajax/OrdersHistory/more',
            type: 'post',
            dataType: 'json',
            data: {
                p: $this.data('p'),
                skey: SKEY
            },
            success: function(d){
                $('#orders').append(d.items);
                $('#pagination').html(d.pagination);
                $this.data('p', d.p);
                if(d.t == 0) $this.hide();
                processing = false;
            }
        });
    });

    $('.comments-more').click(function(e){
        e.preventDefault();
        if(processing) return false;
        processing = true;
        var $this = $(this);
        $.ajax({
            url: 'ajax/Comments/more',
            type: 'post',
            dataType: 'json',
            data: {
                p: $('.comments-item').length + 1,
                skey: SKEY
            },
            success: function(d){
                $('#comments').append(d.items);
                if(d.t == 0) $this.hide();
                processing = false;
            }
        });
    });
    $('#addComment').click(function(e){
        e.preventDefault();
        if(processing) return false;
        processing = true;
        var $this = $(this);
        var user_id = $this.data('user_id');
        if(user_id) {
            $.ajax({
                url: 'ajax/Comments/form',
                type: 'post',
                data: {
                    workers_id: $this.data('id'),
                    skey: SKEY
                },
                success: function(d){
                    var bSubmit = $('#bSubmit')
                    $.fancybox({
                        content: d,
                        fitToView	: false,
                        padding     : 0,
                        margin: [0, 0, 0, 0],
                        wrapCSS     : "pop-up-style",
                        closeBtn    : true,
                        openEffect	: 'elastic',
                        closeEffect	: 'elastic',
                        helpers: {
                            overlay: {
                                locked: false
                            }
                        }
                    });
                    processing = false;

                    $('#commentsForm').ajaxForm({
                        dataType: 'json',
                        beforeSend: function(){
                            processing = true;
                            bSubmit.attr('disabled', true);
                            if(! iOnline()){
                                $('.login-btn').click();
                                return false;
                            }
                        },
                        success: function(d)
                        {
                            processing = false;
                            bSubmit.removeAttr('disabled');
                            myAlert(d.m);
                        }
                    });

                }
            });
        } else {
            $('.login-btn').click();
            return ;
        }

    });

    $('.b-feedback').click(function(e){
        e.preventDefault();
        if(processing) return false;
        processing = true;
        var $this = $(this);
        $.ajax({
            url: 'ajax/Feedback/index',
            type: 'post',
            data: {
                skey: SKEY
            },
            success: function(d){
                var bSubmit = $('#bSubmit')
                $.fancybox({
                    content: d,
                    fitToView	: false,
                    padding     : 0,
                    maxWidth	: 600,
                    width       : '100%',
                    margin: [0, 0, 0, 0],
                    closeBtn    : true,
                    autoSize	: true,
                    openEffect	: 'elastic',
                    closeEffect	: 'elastic',
                    wrapCSS     : "pop-up-style",
                    helpers: {
                        overlay: {
                            locked: false
                        }
                    }
                });
                processing = false;

                $('#feedback').ajaxForm({
                    dataType: 'json',
                    beforeSend: function(){
                        processing = true;
                        bSubmit.attr('disabled', true);
                    },
                    success: function(d)
                    {
                        processing = false;
                        bSubmit.removeAttr('disabled');
                        myAlert(d.m);
                    }
                });

            }
        });
    });

});

function doLazy()
{
    lazyInstance = $("img.lazy").lazy({
        chainable: false,
        bind: "event",
        afterLoad: function(element) {
            element.parent().css({
                height: element.width(),
                background: '#000'
            });
            element.removeClass('lazy');
        }
    });
}
//$(document).ready(function() {
//    var onlyOne=true;
//    $("#data_city_id").select2({
//    });
//    doLazy();
//
//
//});

$(document).ready(function() {
    var onlyOne=true;
    //$("#data_city_id, #users_group_id, #data_currency_id, #filter_city_id").select2({
    //});
    //
    //$("#users_group_id").select2({
    //});
    //
    //$("#data_currency_id").select2({
    //});

    //$("#filter_gid").select2({
    //});

    //$("#filter_city_id").select2({
    //});
    Pay.init();

    doLazy();
    //tooltip
    $('.tipsy').tipsy({gravity: 's'});

});



function Catalog()
{
    var catalogFilter = $('#catalogFilter'), filter_gid = $('#filter_gid');
    return {
        init: function()
        {
            $(document).on('change',filter_gid, function(){
                var action = $(this).find('option:selected').data('href');
                    console.log(action);
                    catalogFilter.attr('action', action);
            });
            //filter_gid.change(function(){
            //    var action = $(this).find('option:selected').data('href');
            //    //console.log(action);
            //    catalogFilter.attr('action', action);
            //});
        }
    };
}

function Wishlist()
{
    var btn = $('.b-wl'), moreWorkers = $('.b-more-wl-workers');
    function add(id)
    {
        return $.ajax({
            url: 'ajax/Wishlist/add',
            type: 'post',
            data: {
                id: id,
                skey: SKEY
            },
            async: false
        }).responseText;
    }



    function showNoResults()
    {
        var t = $.ajax({
            url: 'ajax/Wishlist/noResults',
            type: 'post',
            data: {
                skey: SKEY
            },
            async: false
        }).responseText;

        $('.l-people-list ').replaceWith( t );
    }

    return {
        init: function()
        {
            btn.click(function(e){
                e.preventDefault();
                var $this = $(this);
                if($this.hasClass('in')){
                    self.location.href=$this.attr('href');
                    return;
                }

                var id = $this.data('id');
                if( !iOnline()){
                    $('.login-btn').click();
                    return ;
                }
                if(add(id)){
                    $this.addClass('in').find('span').text($this.data('in'));
                }
            });

            moreWorkers.click(function(e){
                e.preventDefault();
                var $this = $(this);
                $.ajax({
                    url: 'ajax/Wishlist/more',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        p: $this.data('p'),
                        id: window.page.id,
                        order: $_GET.order,
                        filter: $_GET.filter,
                        skey: SKEY
                    },
                    success: function(d){
                        $('#catalog').append(d.items);
                        $('#pagination').html(d.pagination);
                        $this.data('p', d.p);
                        if(d.t == 0) $this.hide();
                    }
                });
            });

            $('.wl-rm-item').click(function(e){
                e.preventDefault();
                var $this = $(this);
                $.ajax({
                    url: 'ajax/Wishlist/rm',
                    type: 'post',
                    data: {
                        id: $this.data('id'),
                        skey: SKEY
                    },
                    success: function(d){
                        if(d>0){
                            $('#wl-' + $this.data('id')).slideUp(300, function(){
                                $(this).remove();
                                if($('.wl-item').length == 0){
                                    showNoResults();
                                }
                            });
                        }
                    }
                });
            });
        }
    };
}
function Orders()
{
    var
        bSubmit = $('#bSubmit'),
        formContent = $('#formContent'),
        worker_id = $("#worker_id").val(),
        sDate= 0,
        dates = []; //

    function getOrderedDates()
    {
        if(typeof worker_id == 'undefined') return ;
        var jqXHR = $.ajax({
            type: 'post',
            url :  'ajax/Orders/getOrderedRange',
            data: {
                id: worker_id,
                skey: SKEY
            },
            async: false
        });
        dates = jqXHR.responseText.split(',');
        //console.log(dates);
    }

    function calc(date)
    {
        $.ajax({
            url: 'ajax/Orders/calc',
            type: 'post',
            data: {
                date: date,
                worker_id: worker_id,
                skey: SKEY
            },
            success: function(d)
            {
                formContent.html(d);
                initDatepicker();
                $('.period').styler();
                initForm();
            }
        });
    }

    function dateParse(input) {
        var parts = input.split('/');
        return new Date(parts[2], parts[1]-1, parts[0]);
    }

    function initDatepicker()
    {
        $('#odate').datepicker({
            dateFormat: 'dd/mm/yy',
            minDate: 0,
            defaultDate: sDate,
            onSelect: function ( dateText, inst ) {
                //var cur = (new Date(inst.selectedYear, inst.selectedMonth, inst.selectedDay)).getTime();
                sDate = new Date(inst.selectedYear, inst.selectedMonth, inst.selectedDay);
                calc(dateText);
            },
            beforeShowDay: function(date)
            {
                var day = $.datepicker.formatDate('yy-mm-dd', date), cls = '', status;

                status = jQuery.inArray(day, dates) !==-1 ? false : true;
                if(status == false){
                    cls += ' ordered ';
                }
                return [status, cls];
            }
        });

    }

    function initForm()
    {
        $('#order').ajaxForm({
            dataType: 'json',
            beforeSend: function(){
                bSubmit.attr('disabled', true);
                if(! iOnline()){
                    $('.login-btn').click();
                    return false;
                }
            },
            success: function(d)
            {
                bSubmit.removeAttr('disabled');
                if(d.s){
                    self.location.href= d.r;
                } else{
                    myAlert(d.e);
                }
            }
        });
    }


    return {
        init: function()
        {
            //console.log('Orders.init();');

            getOrderedDates();

            if(typeof $_GET['filter[df]'] != 'undefined'){
                sDate = dateParse($_GET['filter[df]']);
            }

            initDatepicker();
            initForm();

        }
    };
}

function WorkersOrders()
{
    function changeStatus(id, action)
    {
        $.ajax({
            url: 'ajax/UsersOrders/changeStatus',
            type: 'post',
            data: {
                id:id,
                action: action,
                skey: SKEY
            },
            success: function(d){
                $('#o-item-'+id).replaceWith(d);
            }
        });
    }
    return {
        init: function()
        {
            //console.log('WorkersOrders.init();');
            $(document).on('click', '.b-change-status', function(e)
            {
                e.preventDefault();
                var id = $(this).data('id'), action = $(this).data('action');
                changeStatus(id, action);
            });

            $('.b-more-orders').click(function(e){
                e.preventDefault();
                var $this = $(this);
                $.ajax({
                    url: 'ajax/usersOrders/more',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        p: $this.data('p'),
                        skey: SKEY
                    },
                    success: function(d){
                        $('#orders').append(d.items);
                        $('#pagination').html(d.pagination);
                        $this.data('p', d.p);
                        if(d.t == 0) $this.hide();
                    }
                });
            });
        }
    };
}

function Messages()
{
    var bSubmit = $('#bSubmit'), form = $('#sendMessage'), processing=false;

    function del(id)
    {
        $.ajax({
            url: 'ajax/Messages/delete',
            type: 'post',
            data: {
                id: id,
                skey: SKEY
            },
            success: function(d)
            {
                if(d>0){
                    $('#msg-'+id).remove();
                } else{
                    alert('ERROR: ' + d);
                }

            }
        });
    }

    function setImportant(id, status)
    {
        return $.ajax({
            url: 'ajax/Messages/setImportant',
            type: 'post',
            data: {
                id:     id,
                status: status,
                skey:   SKEY
            },
            async: false
        }).responseText;
    }

    function check(p)
    {
        $.ajax({
            url: 'ajax/Messages/check',
            type: 'post',
            data: {
                p: p,
                skey: SKEY
            },
            success: function(d)
            {
                if(d != ''){
                    $('#messages').append(d);
                }
                setTimeout(function(){
                    check(p);
                }, 10000);
            }
        });
    }
    return {
        init: function()
        {
            //console.log('Messages.init()');

            if($('#messages').length){
                var p = $('#to_id').val();
                check(p);
            }


            $('.messages-item .item-content').click(function(){
                var h = $(this).data('href');
                self.location.href= h;
            });
            $('#data_message').keydown(function (e) {

                if (e.ctrlKey && e.keyCode == 13) {
                    // Ctrl-Enter pressed
                    bSubmit.click();
                }
            });
            form.ajaxForm({
                dataType: 'json',
                beforeSend: function(){
                    if(processing) return false;
                    processing=true;
                    bSubmit.attr('disabled', true);
                },
                success: function(d)
                {
                    processing=false;

                    bSubmit.removeAttr('disabled');
                    if(d.s){
                        $('#messages').append(d.m)
                    }

                    form.resetForm();
                }
            });

            $(document).on('click', '.delete-msg', function(e){
                e.preventDefault();

                var el = $(this),m = el.data('text'), by = el.data('y'), bn = el.data('n'), id = el.data('id');

                var c = $('<div class="m-pop-up-message">\
                            <div class="text">\
                            <div class="row">\
                                <p>'+m+'</p>\
                            </div>\
                                <div class="row">\
                                    <div class="btn-group">\
                                        <a class="btn red b-c-yes" href="#">'+by+'</a>\
                                        <a class="btn green b-c-no" href="#">'+bn+'</a>\
                                    </div>\
                                </div>\
                            </div>\
                        </div>');

                $.fancybox({
                    content: c,
                    fitToView	: false,
                    padding     : 0,
                    margin: [0, 0, 0, 0],
                    wrapCSS     : "pop-up-style",
                    closeBtn    : true,
                    openEffect	: 'elastic',
                    closeEffect	: 'elastic',
                    helpers: {
                        overlay: {
                            locked: false
                        }
                    }
                });

                $('.b-c-yes').click(function(e){
                    e.preventDefault();
                    del(id);
                    $.fancybox.close();
                });
                $('.b-c-no').click(function(e){
                    e.preventDefault();
                    $.fancybox.close();
                });

            });

            $('.set-important')
                .tooltipster({
                    position: 'top',
                    animation: 'grow',
                    theme: 'tooltipster-arrow-top',
                    trigger: 'hover'
                })
                .click(function(e){

                    e.preventDefault();
                    var status = 0, id = $(this).data('id');
                    if($(this).hasClass('important')){
                        status = 0;
                    } else{
                        status = 1;
                    }

                    if(setImportant(id, status)){
                        $(this).toggleClass('important');
                    }
                });
        }
    };
}
var resizeId;
$(window).on('resize',function() {
    clearTimeout(resizeId);
    resizeId = setTimeout(doneResizing, 500);
    function doneResizing(){
        App.resize();
    }
});

$(window).scroll(function() {
    App.scrollUp();
    App.scrollTop = $(window).scrollTop();
});

function myAlert(msg)
{
    var d = $('<div class="m-pop-up-message">\
                <div class="text">\
                    <p>'+msg+'</p>\
                </div>\
            </div>');
    $.fancybox({
        content:d,
        fitToView	: false,
        padding     : 0,
        margin: [0, 0, 0, 0],
        wrapCSS     : "pop-up-style",
        closeBtn    : true,
        openEffect	: 'elastic',
        closeEffect	: 'elastic',
        helpers: {
            overlay: {
                locked: false
            }
        }
    });
}
function _myConfirm(msg)
{
    var status = false;

    var c = $('<div class="m-pop-up-message">\
                            <div class="text">\
                            <div class="row">\
                                <p>'+msg+'</p>\
                            </div>\
                                <div class="row">\
                                    <div class="btn-group">\
                                        <a class="btn red" id="b-c-yes" href="#">Так,хочу</a>\
                                        <a class="btn green" id="b-c-no" href="#">Ні, повернутись назад</a>\
                                    </div>\
                                </div>\
                            </div>\
                        </div>');

    $.fancybox({
        content:c,
        fitToView	: false,
        padding     : 0,
        margin: [0, 0, 0, 0],
        closeBtn    : true,
        wrapCSS     : "pop-up-style",
        openEffect	: 'elastic',
        closeEffect	: 'elastic',
        helpers: {
            overlay: {
                locked: false
            }
        }
    });
    $('#b-c-yes').click(function(){
        status = true;
        $.fancybox.close();
    });
    $('#b-c-no').click(function(){
        $.fancybox.close();
    });

    return status;
}
function iOnline()
{
    return $.ajax({
            url: 'ajax/Users/iOnline',
            type: 'post',
            data: {
                skey: SKEY
            },
            async: false
        }).responseText != '';
}

function getNotifications()
{
    $.ajax({
        url: 'ajax/UsersNotifications/check',
        type: 'post',
        dataType:'json',
        data: {
            skey: SKEY
        },
        success: function(d)
        {
            if(d != ''){
                // total
                if(d.t > 0){
                    $('.b-register').find('span.tn').text(d.t).addClass('on');
                } else{
                    $('.b-register').find('span.tn').text('').removeClass('on');
                }
                if(d.m > 0){
                    $('.nav-item-11').find('span.tn').text(d.m).addClass('on');
                } else{
                    $('.nav-item-11').find('span.tn').text('').removeClass('on');
                }
                if(d.o > 0){
                    $('.nav-item-10').find('span.tn').text(d.o).addClass('on');
                } else{
                    $('.nav-item-10').find('span.tn').text('').removeClass('on');
                }

            }
            setTimeout(function(){
                getNotifications();
            }, 30000);
        }
    });
}

$(document).ready(function(){
    Like.init();
    $("button.reserve").on('click',function(e){
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: 'ajax/Feedback/getForm/',
            data : ({
                'skey':$("#skey").val(),
                'travel_name':$("#travel_name").val(),
                'travel_price':$("#travel_price").val(),
                'travel_date':$("#travel_date").val()
            }),
            success: function (data) {

                $.fancybox(data, {
                    padding: [20,20,20,20],
                    openMethod:'changeIn',
                    helpers : {
                        overlay : {
                            css : {
                                'background' : 'rgba(0,0,0,0.7)',
                                'transition' : 'all 0.35s ease;'
                            }
                        }
                    },
                    openEffect:'fade',
                    closeEffect:'fade',
                    beforeShow: function()
                    {
                        $('input, select').styler();

                        if($('#phone').length){
                            $("#phone").mask("+38(999) 999-99-99");
                        }
                    }
                });
            }
        });
    });

    if($("a.no-click").length) {
        //console.log("sss");
        $("a.no-click").on("click",function(e){
            e.preventDefault();
        });
    }
    (function() {
        if (window.pluso)if (typeof window.pluso.start == "function") return;
        if (window.ifpluso==undefined) { window.ifpluso = 1;
            var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
            s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
            s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://share.pluso.ru/pluso-like.js';
            var h=d[g]('body')[0];
            h.appendChild(s);
        }})();

    //Delete Profile

    $("#aDelProfile").on('click',function(e){
        e.preventDefault();
        var form = $(this).next('form');
        form.stop().slideToggle(200);
    });
});

function browserDetectNav(chrAfterPoint)
{
    var UA=window.navigator.userAgent,       // содержит переданный браузером юзерагент
    //--------------------------------------------------------------------------------
    OperaB = /Opera[ \/]+\w+\.\w+/i,     //
    OperaV = /Version[ \/]+\w+\.\w+/i,   //
    FirefoxB = /Firefox\/\w+\.\w+/i,     // шаблоны для распарсивания юзерагента
    ChromeB = /Chrome\/\w+\.\w+/i,       //
    SafariB = /Version\/\w+\.\w+/i,      //
    IEB = /MSIE *\d+\.\w+/i,             //
    SafariV = /Safari\/\w+\.\w+/i,       //
    //--------------------------------------------------------------------------------
    browser = new Array(),               //массив с данными о браузере
    browserSplit = /[ \/\.]/i,           //шаблон для разбивки данных о браузере из строки
    OperaV = UA.match(OperaV),
    Firefox = UA.match(FirefoxB),
    Chrome = UA.match(ChromeB),

    Safari = UA.match(SafariB),
    SafariV = UA.match(SafariV),
    IE = UA.match(IEB),
    Opera = UA.match(OperaB);

    //----- Opera ----
    if ((!Opera=="")&(!OperaV=="")) browser[0]=OperaV[0].replace(/Version/, "Opera")
else if (!Opera=="") browser[0]=Opera[0]
else
    //----- IE -----
    if (!IE=="") browser[0] = IE[0]
    else
    //----- Firefox ----
    if (!Firefox=="") browser[0]=Firefox[0]
else
    //----- Chrom ----
    if (!Chrome=="") browser[0] = Chrome[0]
    else
    //----- Safari ----
    if ((!Safari=="")&&(!SafariV=="")) browser[0] = Safari[0].replace("Version", "Safari");
    //------------ Разбивка версии -----------

    var outputData;                                      // возвращаемый функцией массив значений

    // [0] - имя браузера, [1] - целая часть версии

    // [2] - дробная часть версии

    if (browser[0] != null) outputData = browser[0].split(browserSplit);

    if ((chrAfterPoint==null)&&(outputData != null)){

        chrAfterPoint=outputData[2].length;

        outputData[2] = outputData[2].substring(0, chrAfterPoint); // берем нужное ко-во знаков

        return(outputData);

    } else return(false);
}

var Like = {
  init : function(){
      this.click();
  },
    click : function(){
        $("#like").on("click",function(){
           var user_id = $(this).data('user');
           var client_id = $(this).data('client');
            var trigger = $(this);
            if(client_id=="") {
                $(".login-btn").click();

            } else {
                $.ajax({
                    url: 'ajax/Users/like',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        user_id:user_id,
                        client_id: client_id,
                        skey: SKEY
                    },
                    success: function(d){
                        console.log(trigger);
                        trigger.text(d.c);

                    }
                })
            }
        });
    }
};

var Pay = {
    init : function(){
        this.getHotPropovse();
        this.getMoreCities();
    },
    getHotPropovse :function() {
        $(".getHotPropovse").on('click',function(e){
            e.preventDefault();
            var type = $(this).data('type');
            $.ajax({
                url: 'ajax/Users/getPayForm',
                type: 'post',
                dataType: 'json',
                data: {
                    type: type,
                    skey: SKEY
                },
                success: function(d){
                    $.fancybox(d.view, {
                        padding: '100',
                        openMethod:'changeIn',
                        helpers : {
                            overlay : {
                                css : {
                                    'background' : 'rgba(0,0,0,0.7)',
                                    'transition' : 'all 0.35s ease;',
                                    'padding' : '0 10px 10px 0 '
                                }
                            }
                        },
                        width         : 940,
                        height        : 400,
                        autoScale     : false,
                        openEffect:'fade',
                        closeEffect:'fade',
                        beforeShow: function()
                        {
                            $('input').styler();
                        }
                    });
                }
            })
        });
    },
    getMoreCities :function() {
        $(".getMoreCities").on('click',function(e){
            e.preventDefault();
            $.ajax({
                url: 'ajax/Users/getMoreCities',
                type: 'post',
                dataType: 'json',
                data: {
                    skey: SKEY
                },
                success: function(d){
                    $.fancybox(d.view, {
                        padding: '100',
                        openMethod:'changeIn',
                        helpers : {
                            overlay : {
                                css : {
                                    'background' : 'rgba(0,0,0,0.7)',
                                    'transition' : 'all 0.35s ease;',
                                    'padding' : '0 10px 10px 0 '
                                }
                            }
                        },
                        width         : 940,
                        height        : 400,
                        autoScale     : false,
                        openEffect:'fade',
                        closeEffect:'fade',
                        beforeShow: function()
                        {
                            $('input').styler();
                        }
                    });
                }
            })
        });
    }
};

//$(document).ready(function(){
//    //console.log();
//    $(".sorting-list a").each(function(i,e){
//      var href = $(this).attr('href');
//       $(this).attr('href',location.href+href);
//    });
//});

$(window).on('load', function(){
    var $loader = $('#page-loader'),
        $spinner = $loader.find('.spinner');
    //$spinner.delay(1000).fadeOut ();
    //$loader.delay (1350).fadeOut ('slow');
    $spinner.delay(1000).fadeOut ();
    $loader.delay (500).fadeOut ('slow');

    //$("#fileuploader").uploadFile({
    //    showPreview:true
    //});

    if($('.forumDatepicker').length){
        $('.forumDatepicker').datepicker({ dateFormat: 'yy-mm-dd' });
        //$('.ui-datepicker-inline').hide();
    }

    $('.icon-calendar').on('click', function(){
        $(this).parents('.date-picker').find('input').focus();
    });


    $('.time-picker').datetimepicker({
        maskInput: true,           // disables the text input mask
        pickDate: false,            // disables the date picker
        pickTime: true
    });

    $('#m-page-slider').owlCarousel({
        navigation : false,
        pagination: true,
        slideSpeed : 300,
        paginationSpeed : 400,
        singleItem: true
        //afterInit: function(){
        //    this.$userItems.each(function(){
        //        $(this).css({
        //            'backgroundImage': 'url('+$(this).find('img').attr('src')+')'
        //        });
        //        console.log($(this), $(this).find('img').attr('src'))
        //    });
        //}
    });

});