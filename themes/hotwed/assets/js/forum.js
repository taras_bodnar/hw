/**
 * Created by taras on 20.04.16.
 */
var Forum = {
  follow: function(id,uid,status,e) {
      if(e.hasClass('checked')) {
          e.removeClass('checked');
          var text = e.text();
          e.text(e.data('text'));
          e.attr('data-text',text);
      }
      $.ajax({
          url: 'ajax/Forum/following',
          type: 'post',
          dataType: 'json',
          data: {
              id: id,
              uid: uid,
              status: status,
              skey: SKEY
          },
          success: function(d){
            if(d>0) {
                e.addClass('checked');
                 text = e.text();
                e.text(e.attr('data-text'));
                e.attr('data-text',text);
            } else {
                e.removeClass('checked');
                 text = e.text();
                e.text(e.data('text'));
                e.attr('data-text',text);
            }
          }
      });
  },
  participants: function(id,uid) {
      if(uid) {
          $.ajax({
              url: 'ajax/Forum/getPart',
              type: 'post',
              data: {
                  uid: uid,
                  id: id,
                  skey: SKEY
              },
              success: function(d){
                  var obj = $.parseJSON(d);
                  var count = $(".participant-count").text();
                  count = parseInt(count);
                  //console.log(obj.ui.id);
                  if(obj.s==1) {
                      //cancel
                      if($("#participant-"+obj.ui.id).length) {
                          $("#participant-"+obj.ui.id).remove();
                      }
                      $(".participant-count").text(count-1);
                      $("a.takePart").text("Прийняти участь");
                  } else {
                      //takePart
                      var html = '<a class="item no-reload" id="participant-'+ obj.ui.id+'" href="#">';
                      html += '<span class="photo">';
                      if(obj.ui.avatar) {
                          html += ' <img src="'+obj.ui.avatar+'">';
                      } else {
                          html += ' <img src="/themes/hotwed/assets/img/avatar.jpg">';
                      }
                      html += '</span>';
                      if(obj.ui.surname) {
                          html += ' <span class="name">' + obj.ui.name + ' ' + obj.ui.surname + '</span>';
                      } else {
                          html += ' <span class="name">' + obj.ui.name +'</span>';
                      }
                      html += '</a>';
                      $("a.takePart").text("Не піду");
                      $(".participant-count").text(count+1);
                      $("#part-items").append(html);
                  }

              }
          });
      } else{
          $('.login-btn').click();
      }
  },
    deleteArticle: function(id){
        $.ajax({
            url: 'ajax/Forum/delete',
            type: 'post',
            data: {
                id: id,
                skey: SKEY
            },
            success: function(d){
                $("#forum-item-"+id).slideUp(1000, function() {
                    $("#forum-item-"+id).remove();
                });
            }
        })
    }
};

var ImgLike = {
    init : function(){
        this.click();
    },
    click : function(){
        $(".photo_like").on("click",function(){
            var user_id = $(this).data('user');
            var client_id = $(this).data('client');
            var trigger = $(this).find('.count');
            if(client_id=="") {
                $(".login-btn").click();

            } else {
                $.ajax({
                    url: 'ajax/PhotoDay/like',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        user_id:user_id,
                        client_id: client_id,
                        skey: SKEY
                    },
                    success: function(d){
                        trigger.text(d.c);

                    }
                })
            }
        });
    }
};

$(document).ready(function(){
    $( "#forumDatepicker" ).datepicker();

    $("ul.lang").find('a').on('click',function(e){
        var href = $(this).data('href');
        if(href){
            location.href = href;
            e.preventDefault();
        }

    });

    $(".add-photo-to-portfolio").on('click',function(e){
        var user_id = $(this).data('user_id');
        var group_id = $(this).data('user_group_id');
        var text = $(this).data('text');
        if(!user_id){
            $('.login-btn').click();
            e.preventDefault();
        }

        if(group_id && group_id!=2){
            myAlert(text);
            e.preventDefault();
        }
    });

    ImgLike.init();

    var processing =false;

    $('#addImgComment').click(function(e){
        e.preventDefault();
        if(processing) return false;
        processing = true;
        var $this = $(this);
        var user_id = $this.data('user_id');
        if(user_id) {
            $.ajax({
                url: 'ajax/PhotoDay/Commentform',
                type: 'post',
                data: {
                    uid: user_id,
                    id: $this.data('id'),
                    skey: SKEY
                },
                success: function(d){
                    var bSubmit = $('#bSubmit')
                    $.fancybox({
                        content: d,
                        fitToView	: false,
                        padding     : 0,
                        margin: [0, 0, 0, 0],
                        wrapCSS     : "pop-up-style",
                        closeBtn    : true,
                        openEffect	: 'elastic',
                        closeEffect	: 'elastic',
                        helpers: {
                            overlay: {
                                locked: false
                            }
                        }
                    });
                    processing = false;

                    $('#commentsForm').ajaxForm({
                        dataType: 'json',
                        beforeSend: function(){
                            processing = true;
                            bSubmit.attr('disabled', true);
                            if(! iOnline()){
                                $('.login-btn').click();
                                return false;
                            }
                        },
                        success: function(d)
                        {
                            processing = false;
                            bSubmit.removeAttr('disabled');
                            //$(".item-container").find(".fb-number").append(data.view);
                            if($("#img-comments").find(".item:first").length) {
                                $("#img-comments").find(".item:first").prepend(d.item);
                            } else {
                                $("#img-comments").prepend(d.item);
                            }
                            $('#commentsForm').ajaxForm();
                            $.fancybox.close();
                            //$(".fancybox-close").click();
                            //myAlert(d.m);
                        }
                    });

                }
            });
        } else {
            $('.login-btn').click();
            return ;
        }

    });

    $('#addForumComment').click(function(e){
        e.preventDefault();
        if(processing) return false;
        processing = true;
        var $this = $(this);
        var user_id = $this.data('user_id');
        if(user_id) {
            $.ajax({
                url: 'ajax/Forum/form',
                type: 'post',
                data: {
                    uid: user_id,
                    id: $this.data('id'),
                    skey: SKEY
                },
                success: function(d){
                    var bSubmit = $('#bSubmit')
                    $.fancybox({
                        content: d,
                        fitToView	: false,
                        padding     : 0,
                        margin: [0, 0, 0, 0],
                        wrapCSS     : "pop-up-style",
                        closeBtn    : true,
                        openEffect	: 'elastic',
                        closeEffect	: 'elastic',
                        helpers: {
                            overlay: {
                                locked: false
                            }
                        }
                    });
                    processing = false;

                    $('#commentsForm').ajaxForm({
                        dataType: 'json',
                        beforeSend: function(){
                            processing = true;
                            bSubmit.attr('disabled', true);
                            if(! iOnline()){
                                $('.login-btn').click();
                                return false;
                            }
                        },
                        success: function(d)
                        {
                            processing = false;
                            bSubmit.removeAttr('disabled');
                            //$(".item-container").find(".fb-number").append(data.view);
                            if($("#comments").find(".item:first").length) {
                                $("#comments").find(".item:first").prepend(d.item);
                            } else {
                                $("#comments").prepend(d.item);
                            }
                            $('#commentsForm').ajaxForm();
                            //$.fancybox.close();
                            //$(".fancybox-close").click();
                            myAlert(d.m);
                        }
                    });

                }
            });
        } else {
            $('.login-btn').click();
            return ;
        }

    });

    $('.forum-comments-more').click(function(e){
        e.preventDefault();
        if(processing) return false;
        processing = true;
        var $this = $(this);
        $.ajax({
            url: 'ajax/Forum/moreComments',
            type: 'post',
            dataType: 'json',
            data: {
                //p: $('.comments-item').length + 1,
                p: $('.comments-item.level-0').length ,
                skey: SKEY,
                id: $(this).data('id')
            },
            success: function(d){
                $('#comments').append(d.items);
                if(d.t == 0) $this.hide();
                processing = false;
            }
        });
    });

    $('.img-comments-more').click(function(e){
        e.preventDefault();
        if(processing) return false;
        processing = true;
        var $this = $(this);
        $.ajax({
            url: 'ajax/PhotoDay/moreComments',
            type: 'post',
            dataType: 'json',
            data: {
                //p: $('.comments-item').length + 1,
                p: $('.comments-item').length ,
                skey: SKEY,
                id: $(this).data('id')
            },
            success: function(d){
                $('#img-comments').append(d.items);
                if(d.t == 0) $this.hide();
                processing = false;
            }
        });
    });

    $('.commentsForm').ajaxForm({
        dataType: 'json',
        beforeSend: function(){
            processing = true;
            if(! iOnline()){
                $('.login-btn').click();
                return false;
            }
        },
        success: function(d)
        {
            processing = false;
            $('.commentsForm').trigger('reset');
            $("div.form").hide();
            $("#pf_content").html("");

            if($("#comments").find(".item-"+ d.parent_id).find(".item-comment:first").length) {
                //console.log($("#comments").find(".item-"+ d.parent_id).find(".item:first"));
                $("#comments").find(".item-"+ d.parent_id).find(".item-comment:first").before(d.item);
            } else {
                $("#comments").find(".item-"+ d.parent_id).append(d.item);
            }
            //myAlert(d.m);
            if($("#comments").find(".item-comment").hasClass('level-0')){
                $("#comments").find(".item-comment").addClass('level-1');
            }else if($("#comments").find(".item").hasClass('level-1')){
                $("#comments").find(".item-comment").addClass('level-2');
            } else {
                $("#comments").find(".item-comment").addClass('level-3');
            }

        }
    });

    var bSubmit = $('#bSubmit');
    $("#forumProcess").ajaxForm({
        dataType: 'json',
        data: {
            skey: SKEY
        },
        beforeSend: function(){

            bSubmit.attr('disabled', true);
            if(! iOnline()){
                $('.login-btn').click();
                return false;
            }
        },
        success: function(d)
        {
            bSubmit.removeAttr('disabled');
            location.href = d.r;
        }
    });


    $('.no-reload').on('click',function(e){
        e.preventDefault();
    });


    $('.b-pf-add-video-forum').click(function(e){
        e.preventDefault();
        var forum_id = $(this).data('forum_id');
        $.ajax({
            url: 'ajax/Forum/uploadVideo',
            type: 'post',
            data:{
                skey: SKEY,
                forum_id:forum_id
            },
            success: function(d)
            {
                $.fancybox({
                    content:d,
                    fitToView	: false,
                    padding     : 0,
                    margin: [0, 0, 0, 0],
                    wrapCSS     : "pop-up-style",
                    closeBtn    : true,
                    openEffect	: 'elastic',
                    closeEffect	: 'elastic',
                    helpers: {
                        overlay: {
                            locked: false
                        }
                    }
                });
                $('#uploadVideo').ajaxForm({
                    dataType: 'json',
                    success: function(d) {
                        if(d.s){
                            $.fancybox.close();
                            $(d.images).each(function(i,e){
                                $(e).appendTo($('#pf_content'));
                            });

                        } else{
                            console.log("error");
                        }
                    }
                });

            }
        });

    });

    $(document).on('click', '.rm-pf-item-forum', function(e){
        e.preventDefault();
        var item_id = $(this).attr('id');
        $.ajax({
            url: 'ajax/Forum/rmMediaItem',
            type: 'post',
            data:{
                id: item_id,
                skey: SKEY
            },
            success: function(d)
            {
                if(d>0){
                    $('#pf-item-' + item_id).hide(function(){$(this).remove();});
                }
            }
        });
    });

    var wp = false;
    $('.b-more-images').click(function(e){
        //console.log($_GET.filter);
        var $this = $(this);
        e.preventDefault();
        if(wp) return false;
        wp=true;
        $.ajax({
            url: 'ajax/PhotoDay/more',
            type: 'post',
            dataType: 'json',
            data: {
                p: $this.data('p'),
                id: window.page.id,
                order: $_GET.order,
                filter: $_GET,
                skey: SKEY
            },
            success: function(d){
                if(d.t == 0 || d == '') $this.hide();
                $(d.items).each(function(i,e){
                    $('#photo-day-list').append(e);
                });
                //$('#catalogWorkers').append(d.items);
                $('#pagination').html(d.pagination);
                $this.data('p', d.p);

                doLazy();
            }
        });
    });
});