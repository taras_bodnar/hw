/**
 * Created by taras on 14.03.16.
 */
$(document).ready(function(){
    $('.create-filter-buttons li').each(function() {
        $(this).click(function() {
            $('.create-filter-buttons li button').removeClass('active');
            $(this).find('button').addClass('active');
            $('#in_forum_topic').val($(this).find('button').data('topic'));
            if (parseInt($(this).find('button').data('topic')) == 3) {
                $('#meet_cnt').removeClass('hidden');
            } else {
                if (!$('#meet_cnt').hasClass('hidden')) {
                    $('#meet_cnt').addClass('hidden');
                }
            }
        });
    });
    var topicTypeId = '0';
    $('.create-filter-buttons').find('.topic-type-' + topicTypeId).click();
    if ($('.create-filter-buttons').find('.topic-type-' + topicTypeId).length == 0) {
        $('.create-filter-buttons').find('.topic-type-' + 2).click();
    }


    $('.add-new-video li button.switcher-button').click(function() {
        $('.add-new-video li button.switcher-button').removeClass('active');
        $(this).addClass('active');
        if ($(this).hasClass('first-child')) {
            $('#forum_topic_add_form #forum_topic_only_for_users').remove();
        } else {
            $('#forum_topic_add_form #forum_topic_only_for_users').remove();
            $('#forum_topic_add_form').append('<input type="hidden" name="forum_topic_only_for_users" id="forum_topic_only_for_users" value="1">');
        }
    });
    if ($('#forum_topic_only_for_users').length > 0) {
        $('.add-new-video ul.buttons-group li:eq(2) button').click();
    }
    if ($("#date").datepicker('getDate') != null) {
        $("#date-to").datepicker("option", "minDate", new Date($("#date").datepicker('getDate')));
        $("#date").datepicker("option", "minDate", new Date($("#date").datepicker('getDate')));
    }

    $("#forumFile").uploadFile({
        dataplaceholder: "{$t.choose_file}",
        url: "ajax/AjaxForm/uploadFiles",
        multiple: true,
        fileName: "myfile",
//            returnType: "json",
        formData: {
            "services_id": $("#services_id").val()
        },
    showDelete: true,
        onSuccess: function (files, data, xhr) {
        //console.log(files,data);
        $("#temp").val(files);
    },
    deleteCallback: function (data, pd) {
        var id = $.parseJSON(data);
        console.log(id.data);
        $.post("ajax/AjaxForm/deleteFile", {op: "delete", id: id.data},
            function (resp, textStatus, jqXHR) {
                //  console.log("ok");
            });

        // pd.statusbar.hide();
    }

});
});