{*
 * Company Otakoyi.com.
 * Author Ð’Ð¾Ð»Ð¾Ð´Ð¸Ð¼Ð¸Ñ€
 * Date: 22.01.2015
 * Time: 00:08
 * Name: header
 *}
{if $page.id == 1}
    <header class="l-header-home">
        <div class="layer-1 h100">
            <div class="video-container">
                {*<video id="video" loop autoplay>*}
                    {*<source src="{$template_url}assets/video/hotved.mp4" type="video/mp4">*}
                    {*<source src="{$template_url}assets/video/hotved.webm" type="video/webm">*}
                    {*<source src="{$template_url}assets/video/hotved.ogv" type="video/ogv">*}
                {*</video>*}
            </div>
        </div>
        <div class="layer-2">
            <div class="h-100">
                <div class="border">
                    <div class="row top">
                        <div class="m-lang col-4">
                            [[mod:Nav::languages]]
                            <!--myroslav-->
                            <div class="cabinet-sm">

                                {if $smarty.session.app.user.id}
                                    <!--коли залогінений-->
                                    <a class="log-in authorised" href="#">
                                        <i class="icon-user"></i>
                                    </a>
                                {else}
                                    <!--коли не залогінений-->
                                    <a href="ajax/Users/signUp" class="log-in pop-up">
                                        <i class="icon-login"></i>
                                    </a>
                                {/if}
                            </div>
                            <!--end-->
                            {*<div class="m-button-search">*}
                            {*<i class="icon-search"></i>*}
                            {*</div>*}
                        </div>
                        <div class="logo col-4">
                            <img src="{$template_url}assets/img/images/logo.png" alt=""/>
                        </div>
                        {if $smarty.session.app.user.id}
                            <div class="login col-4">
                                <div class="m-login logged">
                                    <div class="m-login logged">
                                        <a href="7" class="login-btn b-register" style="font-family: 'FiraSansLight'">
                                            <span class="tn"></span>
                                            <span>{$t.u_account}</span>
                                        </a>
                                        <a href="{if $smarty.session.app.user.users_group_id == 3}7{else}{$smarty.session.app.user.url}{/if}" class="login-btn b-register">
                                            <i class="icon-user"></i>
                                        </a>
                                        <div class="user-menu">
                                            [[mod:Nav::user]]
                                        </div>
                                    </div>

                                    <div class="user-menu">
                                        [[mod:Nav::user]]
                                    </div>
                                </div>
                                <div class="menu-xs">
                                    [[mod:Nav::main]]
                                </div>
                            </div>
                        {else}
                            <div class="login col-4">
                                <div class="menu-xs">
                                    [[mod:Nav::main]]
                                </div>
                                <a href="ajax/Users/signUp" class="btn png-red text-white pop-up login-btn" style="font-family: 'FiraSansLight'">{$t.b_login_reg}</a>
                                {*<a href="ajax/Users/signUp" class="btn-icon pop-up"><i class="icon-login"></i></a>*}
                            </div>
                        {/if}
                    </div>
                    <div class="row advanced-search">
                        <div class="container">
                            <div class="advanced-search-title">{$t.main_filter_title}</div>
                            [[mod:Catalog::filter]]
                            <div class="arrow">
                                <i class="icon-arrow"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="container bottom">
                    [[mod:Nav::top]]
                    <div class="m-button-search">
                        <i class="icon-search"></i>
                    </div>
                </div>
            </div>
        </div>

    </header><!-- .header-->
{else}
    <!--пхідні сторінки-->
    <header class="l-header secondary-page">
        <div class="h-1">
            [[mod:Nav::main]]
        </div>
        <div class="h-2">
            <a href="1">
                <img src="{$template_url}assets/img/images/color-logo.png" alt=""/>
            </a>
        </div>
        <div class="h-3">
            {if $smarty.session.app.user.id}
                <div class="m-login logged">
                    <a href="7" class="login-btn b-register" style="font-family: 'FiraSansLight'">
                        <span class="tn"></span>
                        <span>{$smarty.session.app.user.name} {$smarty.session.app.user.surname}</span>
                    </a>
                    <a href="{if $smarty.session.app.user.users_group_id == 3}7{else}{$smarty.session.app.languages.code}/{$smarty.session.app.user.url}{/if}" class="login-btn b-register">
                        <i class="icon-user"></i>
                    </a>
                    <div class="user-menu" style="width: 220px;">
                        [[mod:Nav::user]]
                    </div>
                </div>
                <!--myroslav-->
                <div class="cabinet-sm">
                    <a class="log-in authorised" href="#">
                        <i class="icon-user"></i>
                    </a>
                </div>
                <!--end-->
            {else}
                <div class="cabinet-sm">
                    <a href="ajax/Users/signUp" class="log-in pop-up login-btn">
                        <i class="icon-login"></i>
                    </a>
                </div>
                <a href="ajax/Users/signUp" class="log-in pop-up login-btn log-lg">
                    <i class="icon-login"></i>
                    <span>{$t.b_login_reg}</span>
                </a>
            {/if}
            <div class="m-button-search" id="m-search">
            <i class="icon-search"></i>
            </div>
        </div>

    </header><!-- .header-->
    [[mod:Breadcrumb]]
{/if}