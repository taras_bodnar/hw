{*
 * Company Otakoyi.com.
 * Author Ð’Ð¾Ð»Ð¾Ð´Ð¸Ð¼Ð¸Ñ€
 * Date: 22.01.2015
 * Time: 00:08
 * Name: header
 *}
{if $page.id == 1}
    <header class="l-header-home">
        <div class="layer-1 h100">
            <div class="video-container">
                <video id="video" loop autoplay>
                    <source src="{$template_url}assets/video/hotved.mp4" type="video/mp4">
                    <source src="{$template_url}assets/video/hotved.webm" type="video/webm">
                    <source src="{$template_url}assets/video/hotved.ogv" type="video/ogv">
                </video>
            </div>
        </div>
        <div class="layer-2">
            <div class="h-100">
                <div class="border">
                    <div class="row top">
                        <div class="m-lang col-4">
                            [[mod:Nav::languages]]
                            <!--myroslav-->
                            <div class="cabinet-sm">
                                <!--коли не залогінений-->
                                <a class="log-in" href="#">
                                    <i class="icon-login"></i>
                                </a>
                                <!--коли залогінений-->
                                {*<a class="log-in authorised" href="#">*}
                                    {*<i class="icon-user"></i>*}
                                {*</a>*}
                            </div>
                            <!--end-->
                            {*<div class="m-button-search">*}
                                {*<i class="icon-search"></i>*}
                            {*</div>*}
                        </div>
                        <div class="logo col-4">
                            <img src="{$template_url}assets/img/images/logo.png" alt=""/>
                        </div>
                        {if $smarty.session.app.user.id}
                            <div class="login col-4">
                                <div class="m-login logged">
                                    <a href="7" class="login-btn b-register"><span>{$t.u_account}</span><i class="icon-user"></i></a>
                                    <div class="user-menu">
                                        [[mod:Nav::user]]
                                    </div>
                                </div>
                                <div class="menu-xs">
                                    [[mod:Nav::main]]
                                </div>
                            </div>
                        {else}
                            <div class="login col-4">
                                <a href="ajax/Users/signUp" class="btn png-red text-white pop-up">{$t.b_login_reg}</a>
                                <a href="ajax/Users/signUp" class="btn-icon pop-up"><i class="icon-login"></i></a>
                            </div>
                        {/if}
                    </div>
                    <div class="row advanced-search">
                        <div class="container">
                            <div class="advanced-search-title">{$t.main_filter_title}</div>
                            [[mod:Catalog::filter]]
                            <div class="arrow">
                                <i class="icon-arrow"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="container bottom">
                    [[mod:Nav::top]]
                    <div class="m-button-search">
                        <i class="icon-search"></i>
                    </div>
                </div>
            </div>
        </div>

    </header><!-- .header-->
{else}
    <header class="l-header">
        <div class="h-1">
            [[mod:Nav::main]]
        </div>
        <div class="h-2">
            <a href="1">
                <img src="{$template_url}assets/img/images/color-logo.png" alt=""/>
            </a>
        </div>
        <div class="h-3">
            {if $smarty.session.app.user.id}
                <div class="m-login logged">
                    <a href="7" class="login-btn b-register"><span class="tn"></span><span>{$smarty.session.app.user.name} {$smarty.session.app.user.surname}</span><i class="icon-user"></i></a>
                    <div class="user-menu">
                        [[mod:Nav::user]]
                    </div>
                </div>
            {else}
                <div class="m-login logged">
                    <a href="ajax/Users/signUp" class="login-btn pop-up"><span>{$t.b_login_reg}</span><i class="icon-user"></i></a>
                </div>
            {/if}
            <div class="m-button-search">
                <i class="icon-search"></i>
            </div>
        </div>
    </header><!-- .header-->
{/if}

