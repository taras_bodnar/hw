<!DOCTYPE html>
<html>
<head>
    [[app:metatags]]
    <meta charset="utf-8"/>

    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link href="{$template_url}assets/img/favicon.ico" rel="shortcut icon" type="image/x-icon"/>

    <script src="{$template_url}assets/js/vendor/modernizr.custom.18042.js"></script>

    <!-- css -->
    <link href="{$template_url}assets/css/vendor/preloader.css" rel="stylesheet">
    <link href="{$template_url}assets/css/vendor/owl.carousel.css" rel="stylesheet">
    <link href="{$template_url}assets/css/vendor/jquery.fancybox.css" rel="stylesheet">
    <link href="{$template_url}assets/css/vendor/select2.min.css" rel="stylesheet"/>

    <link href="{$template_url}assets/css/vendor/fullcalendar.min.css" rel="stylesheet">
    <link href="{$template_url}assets/css/vendor/tipsy.css" rel="stylesheet">
    <link href="{$template_url}assets/css/vendor/jquery.qtip.min.css" rel="stylesheet">
    <link href="{$template_url}assets/css/vendor/uploadfile.css" rel="stylesheet">
    {*<link href="{$template_url}assets/css/style.v662.css" rel="stylesheet">*}
    <link href="{$template_url}assets/css/style.css" rel="stylesheet">
    {*<link href="{$template_url}assets/css/custom.min.css" rel="stylesheet">*}
    {*<link href="{if !empty($smarty.server.HTTPS)}https://{else}http://{/if}{$smarty.server.HTTP_HOST}/{$page.canonical}" rel="canonical">*}

    {literal}
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                        (i[r].q = i[r].q || []).push(arguments)
                    }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-113820093-1', 'auto');
            ga('send', 'pageview');

        </script>
    {/literal}
</head>
<body {if $page.templates_id==25}class="forum"{/if}>
{if $page.id==1 || ($page.id==222 && $photo.day_check==1)}
    <div id="page-loader">
        <span class="spinner"></span>
    </div>
{/if}
