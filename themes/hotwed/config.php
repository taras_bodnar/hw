<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 28.08.14 7:56
 */
 defined("SYSPATH") or die();

return array(
    'name'        => 'hotwed',
    'author'      => 'wmgodyak',
    'version'     => '1.0.0',
    'screenshot'  => 'screenshot.jpg',
    'type'        => 'frontend',
    'description' => 'Тема для HotWed'
);
