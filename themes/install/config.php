<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 28.08.14 7:56
 */
 defined("SYSPATH") or die();

return array(
    'name'        => 'Інсталяція',
    'author'      => 'wg',
    'version'     => '1.0.0',
    'screenshot'  => 'screenshot.png',
    'type'        => 'backend',
    'description' => ''
);