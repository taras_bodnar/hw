<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 16.10.14 19:03
 */

defined("SYSPATH") or die();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?=$title?></title>
    <link rel="stylesheet" href="/themes/install/css/style.css"/>
</head>
<body>
    <div class="install">

        <form method="post">

            <div class="panel panel-default panel-block">
                <div class="list-group">
                    <div class="list-group-item" id="register">
                        <div class="logo"><a href="http://otakoyi.com"><img src="/themes/install/images/oyi.jpg" alt=""/></a></div>
                        <h2 class="text-center">Вітаємо в системі OYi.Engine 6</h2>
                        <?=$content?>
                    </div>
                </div>
            </div>
        </form>
    </div>
</body>
</html>