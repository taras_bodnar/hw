<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 16.10.14 19:03
 */

defined("SYSPATH") or die();
?>
    <fieldset>
        <legend>Ліцензія</legend>
    </fieldset>
    <?php if(! empty($error)) : ?>
        <div class="alert alert-dismissable alert-danger fade in">
            <?=implode('<br>', $error);?>
        </div>
    <?php endif; ?>
    <p>Введіть тут ліцензійний ключ або використайте цей безкоштовний</p>

    <div class="form-group">
        <textarea class="form-control " name="key" required="" id="key"><?=$key?></textarea>
    </div>
    <input type="hidden" name="step" value="5"/>

    <footer class="panel-footer text-right">
        <button onclick="this.disabled=true;" class="btn btn-success" type="submit">Наступний крок</button>
    </footer>