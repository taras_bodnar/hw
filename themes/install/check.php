<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 16.10.14 19:03
 */

defined("SYSPATH") or die();
?>
    <fieldset>
        <legend>Неможливо встановити систему.</legend>
    </fieldset>

    <?php if(!empty($error)) : ?>
        <div class="alert alert-dismissable alert-danger fade in">
            <?=implode('<br>', $error);?>
        </div>
    <?php endif; ?>
