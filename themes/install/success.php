<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 16.10.14 19:03
 */

defined("SYSPATH") or die();
?>
    <div class="alert alert-dismissable alert-info fade in">
        Вітаємо! Систему OYi.Engine встановлено. <br/>
        Дякуємо за використання ліцензійної версії OYi.Engine.
    </div>
    <footer class="panel-footer text-right">
        <a class="btn btn-success" href="/engine">Панель адміністратора</a>
        <a class="btn btn-link" href="/">На сайт</a>
    </footer>