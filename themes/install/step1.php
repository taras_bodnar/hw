<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 16.10.14 19:03
 */

defined("SYSPATH") or die();
?>
    <fieldset>
        <legend>Налаштування БД</legend>
    </fieldset>
    <?php if(empty($error)) : ?>
        <div class="alert alert-dismissable alert-success fade in">
            Перед інсталяцією системи створіть БД.
        </div>
    <?php else: ?>
        <div class="alert alert-dismissable alert-danger fade in">
            <?=implode('<br>', $error);?>
        </div>
    <?php endif; ?>
    <p>Введіть тут інформацію про підключення до бази даних.</p>

    <div class="form-group">
        <label>Назва бази даних <span class="text-danger">*</span></label>
        <input type="text" required="" name="data[name]" placeholder="введіть назву БД" class="form-control ">
    </div>

    <div class="form-group">
        <label>Ім’я користувача <span class="text-danger">*</span></label>
        <input type="text" required="" name="data[user]" value="root" class="form-control ">
    </div>

    <div class="form-group">
        <label>Пароль <span class="text-danger">*</span></label>
        <input type="password" required="" name="data[pass]" placeholder="pasword" class="form-control ">
    </div>

    <div class="form-group">
        <label>Сервер баз даних <span class="text-danger">*</span></label>
        <input type="text" required="" name="data[host]" value="localhost" class="form-control ">
    </div>
    <input type="hidden" name="step" value="1"/>

    <footer class="panel-footer text-right">
        <button class="btn btn-success" type="submit">Наступний крок</button>
    </footer>