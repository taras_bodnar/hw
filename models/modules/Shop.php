<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 13.10.14 21:44
 */

namespace models\modules;

use models\App;

defined("SYSPATH") or die();

/**
 * Class Shop
 * @package models\modules
 */
class Shop extends App{

    private $limit = '';
    private $where = array();
    private $join  = array();
    private $order_by = "abs(c.id) asc";

    private $currency_id=0;
    private $debug = 0;

    /**
     * @param $status
     * @return $this
     */
    public function debug($status=1)
    {
        $this->debug = $status;
        return $this;
    }

    /**
     * @param $id
     * @return $this
     */
    public function setCurrencyId($id)
    {
        $this->currency_id = $id;

        return $this;
    }

    /**
     * @return array|mixed
     */
    public function getProducts()
    {
        $where = empty($this->where) ? '' : implode(' ', $this->where);
        $join = empty($this->join) ? '' : implode(' ', $this->join);

        return $this->db->select("
            select DISTINCT c.id,
            i.title, i.name, i.description,
            po.id as variant_id, po.availability, po.price, po.hit, po.new, po.sale,
            cu.symbol
            from products_options po
            join currency cu on cu.is_main=1
            join content c on c.id = po.products_id and c.auto=0 and c.published = 1
            {$join}
            join content_info i on i.content_id = c.id and i.languages_id = '{$this->languages_id}'
            where po.is_variant=0 {$where}
            order by {$this->order_by}
            {$this->limit}
            ", $this->debug)->all();
    }


    /**
     * @return array|mixed
     */
    public function getTotalProducts()
    {
        $where = empty($this->where) ? '' : implode(' ', $this->where);
        $join = empty($this->join) ? '' : implode(' ', $this->join);

        if($this->currency_id == 0){
            $this->currency_id = $this->db->select("select id from currency where is_main=1")->row('id');
        }

        return $this->db->select("
            select count(po.id) as total,
            MIN(IF(cu.id=cr.id, po.price, po.price / cr.rate )) as minp,
            MAX(IF(cu.id=cr.id, po.price, po.price / cr.rate )) as maxp
            from products_options po
            join currency cu on cu.is_main=1
            join currency cr on cr.id= {$this->currency_id}
            join content c on c.id = po.products_id and c.auto=0 and c.published = 1
            {$join}
            join content_info i on i.content_id = c.id and i.languages_id = {$this->languages_id}
            where po.is_variant=0 {$where}
            ", $this->debug)->row();
    }

    /**
     * @param $start
     * @param $num
     * @return $this
     */
    public function setLimit($start, $num)
    {
        $this->limit = " limit $start, $num";
        return $this;
    }

    /**
     * @param $w
     * @param string $prefix
     * @return $this
     */
    public function setWhere($w, $prefix='and')
    {
        $this->where[] = ' '. $prefix . ' '. $w;
        return $this;
    }

    /**
     * @param $join
     * @return $this
     */
    public function setJoin($join)
    {
        $this->join[] = $join;
        return $this;
    }

    /**
     * @return $this
     */
    public function clearJoin()
    {
        $this->join = array();
        return $this;
    }

    /**
     * @return $this
     */
    public function clearWhere()
    {
        $this->where = array();
        return $this;
    }

    /**
     * @param $ob
     * @return $this
     */
    public function setOrderBy($ob)
    {
        $this->order_by = $ob;
        return $this;
    }


    /**
     * new Products
     * @return array|mixed
     */
    public function newProducts()
    {
        return $this->db->select("
            select p.products_id as id, p.price, i.title, i.name
            from products_options p
            join content c on c.id=p.products_id and c.published = 1
            join content_info i on i.content_id = p.products_id and i.languages_id = {$this->languages_id}
            where p.new = 1
            order by abs(c.id) desc
            ")->all();
    }
    /**
     * new Products
     * @return array|mixed
     */
    public function hitProducts()
    {
        return $this->db->select("
            select p.products_id as id, p.price, i.title, i.name
            from products_options p
            join content c on c.id=p.products_id and c.published = 1
            join content_info i on i.content_id = p.products_id and i.languages_id = {$this->languages_id}
            where p.new = 1
            order by rand()
            limit 1
            ")->row();
    }

    /**
     * @param int $parent_id
     * @param string $limit
     * @return array|mixed
     */
    public function getCategories($parent_id = 0, $limit ='')
    {
        if($limit != '') {
            $limit = " limit {$limit}";
        }
        return $this->db->select("
            select c.id, i.title, i.name, c.isfolder
            from content c
            join content_type ct on ct.type='category' and ct.id=c.type_id
            join content_info i on i.content_id = c.id and i.languages_id = '{$this->languages_id}'
            where c.parent_id='{$parent_id}' and c.auto=0 and c.published = 1
            order by abs(c.id) asc
            {$limit}
            ")->all();
    }

    /**
     * @return array|mixed
     */
    public function getManufacturers()
    {
        return $this->db->select("
            select c.id, i.title, i.name
            from content c
            join content_type ct on ct.type='manufacturer' and ct.id=c.type_id
            join content_info i on i.content_id = c.id and i.languages_id = '{$this->languages_id}'
            where c.auto=0 and c.published = 1
            order by abs(i.name) asc
            ")->all();
    }

    public function totalCategories($parent_id)
    {
        return $this->db->select("select count(c.id) as t
        from content c, content_type ct
        where c.parent_id='{$parent_id}' and ct.type='category' and c.type_id=ct.id and c.auto=0 and c.published=1
        ")->row('t');
    }

    /**
     * @param $product_id
     * @return array|mixed
     */
    public function getProductOptions($product_id)
    {
        return $this->db->select("
              select po.*, cu.symbol
              from products_options po
              join currency cu on cu.is_main=1
              where po.products_id='{$product_id}' and po.is_variant=0 limit 1
        ")->row();
    }

    /**
     * @param $pid
     * @return array|mixed
     */
    public function getCategoryID($pid)
    {
        return $this->db->select("select cid from products_categories pc where pc.pid={$pid}")->row('cid');
    }

    /**
     * @param $product_id
     * @return array|mixed
     */
    public function getVariants($product_id)
    {
        return $this->db->select("
            select po.id,po.name, IF(cu.id=cr.id, po.price, po.price / cr.rate ) as price
            from products_options po
            join currency cu on cu.is_main=1
            join currency cr on cr.id= {$this->currency_id}
            where po.products_id={$product_id} and po.is_variant=1
            order by abs(po.sort) asc
        ")->all();
    }

    /**
     * @param string $key
     * @return array|mixed
     */
    public function mainCurrency($key='*')
    {
        return $this->db->select("select {$key} from currency where is_main=1")->row($key);
    }

    /**
     * @param $id
     * @param string $key
     * @return array|mixed
     */
    public function currency($id,$key='*')
    {
        return $this->db->select("select {$key} from currency where id={$id}")->row($key);
    }

    public function getProductsCategory($pid)
    {
        return $this->db->select("select cid from products_categories where pid={$pid} limit 1")->row('cid');
    }

    /**
     * @param $content_id
     * @return array|mixed
     */
    public function filterGetFeatures($content_id, $pids)
    {
        $features = $this->db->select("
             select f.id, f.type, fi.name
             from features f
             join content_features cf on cf.features_id = f.id and cf.content_id = '{$content_id}'
             join features_info fi on fi.features_id = f.id and fi.languages_id = '{$this->languages_id}'
             where f.show_filter and f.auto=0 and f.published=1
        ")->all();

        foreach($features as $k=> $item){
            $join = '';
            if(!empty($pids)){
                $join = "join features_content_values fcv on
                fcv.features_id='{$item['id']}'
                and fcv.features_values_id=v.id
                and fcv.content_id in (". implode(',', $pids) .")
                ";
            }

            switch($item['type']){
                case 'select':
                case 'sm':
                    $features[$k]['values'] = $this->db->select("
                     select DISTINCT v.id,i.value
                     from features_values v
                     {$join}
                     join features_values_info i on i.values_id = v.id and i.languages_id = '{$this->languages_id}'
                     where v.features_id = '{$item['id']}'
                     order by abs(v.sort) asc")->all();
                break;
                case 'number':
                    $features[$k]['values'] = $this->db->select("
                     select DISTINCT value
                     from features_content_values
                     where features_id = '{$item['id']}'
                     ")->all();
                    break;

                case 'text':
                    $features[$k]['values'] = $this->db->select("
                     select DISTINCT value
                     from features_content_values
                     where features_id = '{$item['id']}' and languages_id={$this->languages_id} and value <> ''
                     ")->all();
                    break;
            }
        }

        return $features;
    }

    /**
     * @param $id
     * @return array|mixed
     */
    public function getProductFeatures($id)
    {
        $features = $this->db->select("
             select DISTINCT f.id, f.type, fi.name, cf.value
             from features f
             join features_content_values cf on cf.features_id = f.id and cf.content_id = '{$id}'
             join features_info fi on fi.features_id = f.id and fi.languages_id = '{$this->languages_id}'
             where f.auto=0 and f.published=1
             order by fi.name asc
        ")->all();

        foreach($features as $k=> $item){
            switch($item['type']){
                case 'select':
                case 'sm':
                    $v = $this->db->select("
                     select v.id,i.value
                     from features_values v
                     join features_content_values cf on cf.features_id = {$item['id']} and cf.content_id = '{$id}'
                     and cf.features_values_id = v.id
                     join features_values_info i on i.values_id = v.id and i.languages_id = '{$this->languages_id}'
                     where v.features_id = '{$item['id']}'
                     order by abs(v.sort) asc")->all();
                foreach ($v as $vv) {
                    $features[$k]['values'][$vv['id']] = $vv['value'];
                }

                break;
                case 'number':
                    $features[$k]['value'] = $this->db->select("
                     select value from features_content_values
                      where features_id = '{$item['id']}' and content_id={$id} and languages_id=0
                     ")->row('value');
                    if(empty($features[$k]['value'])){ unset($features[$k]); }
                    break;

                case 'text':
                case 'textarea':
                $features[$k]['value'] = $this->db->select("
                     select value from features_content_values
                      where features_id = '{$item['id']}' and content_id={$id} and languages_id='{$this->languages_id}'
                     ")->row('value');
                if(empty($features[$k]['value'])){ unset($features[$k]); }
                    break;
            }
        }
        return $features;
    }

    /**
     * @param $id
     * @return array|mixed
     */
    public function getProductDescription($id)
    {
        return $this->db->select("
                select content
                from content_info
                where content_id={$id} and languages_id={$this->languages_id} limit 1")
            ->row('content');
    }

    public function getProductsRelated($products_id)
    {

        if($this->currency_id == 0){
            $this->currency_id = $this->db->select("select id from currency where is_main=1")->row('id');
        }

        return $this->db->select("
            select c.id, i.title, i.name, i.description, po.availability, cr.symbol,
            IF(cu.id=cr.id, po.price, po.price / cr.rate ) as price
            from products_related pr
            join products_options po on po.products_id=pr.related_id and po.is_variant=0
            join currency cu on cu.is_main=1
            join currency cr on cr.id= {$this->currency_id}
            join content c on c.id = po.products_id and c.auto=0 and c.published = 1
            join content_info i on i.content_id = c.id and i.languages_id = {$this->languages_id}
            where pr.products_id={$products_id}
            order by abs(pr.sort) asc, i.name asc
            ")->all();
    }

    /**
     * @param $in
     * @return array|mixed
     */
    public function getProductsViewed($in)
    {
        $in = implode(',', $in);
        if($this->currency_id == 0){
            $this->currency_id = $this->db->select("select id from currency where is_main=1")->row('id');
        }

        return $this->db->select("
            select c.id, i.title, i.name, i.description, po.availability, cr.symbol,
            IF(cu.id=cr.id, po.price, po.price / cr.rate ) as price
            from products_options po
            join currency cu on cu.is_main=1
            join currency cr on cr.id= {$this->currency_id}
            join content c on c.id = po.products_id and c.auto=0 and c.published = 1
            join content_info i on i.content_id = c.id and i.languages_id = {$this->languages_id}
            where po.products_id in ({$in}) and po.is_variant=0
            order by i.name asc
            limit 5
            ")->all();
    }

    public function setViews($products_id)
    {
        $now = date('Y-m-d');
        $r = $this->db->select("
                select id, views
                from products_views
                where products_id = {$products_id} and date='{$now}' limit 1
              ")
            ->row();
        if(empty($r)){
            $this->db->insert('products_views', array('products_id' => $products_id, 'views' => 1, 'date' => date('Y-m-d')));
        } else{
           $this->db->update('products_views', array('views' => ++ $r['views']), "  id = {$r['id']} limit 1");
        }

    }

    public function getPopular($in)
    {
        $in = implode(',', $in);
        if($this->currency_id == 0){
            $this->currency_id = $this->db->select("select id from currency where is_main=1")->row('id');
        }

        return $this->db->select("
            select c.id, i.title, i.name, i.description, po.availability, cr.symbol,
            IF(cu.id=cr.id, po.price, po.price / cr.rate ) as price
            from products_options po
            join currency cu on cu.is_main=1
            join currency cr on cr.id= {$this->currency_id}
            join content c on c.id = po.products_id and c.auto=0 and c.published = 1
            join content_info i on i.content_id = c.id and i.languages_id = {$this->languages_id}
            where po.products_id in ({$in}) and po.is_variant=0
            order by i.name asc
            limit 5
            ")->all();
    }

    /**
     * @param $content_id
     * @return array|mixed
     */
    public function makeAlias($content_id)
    {
        return $this->db->select("
            select IF(ld.id={$this->languages_id}, i.alias, CONCAT(l.code, '/' ,i.alias) ) as alias
            from content_info i
            left join languages ld on ld.front_default = 1
            left join languages l on l.id = {$this->languages_id}
            where i.content_id = '{$content_id}' and i.languages_id = '{$this->languages_id}'
            limit 1
        ")->row('alias');
    }

    /**
     * @param $in
     * @return array|mixed
     */

    public function searchGetCategories($in)
    {
        $in = implode(',', $in);

        return $this->db->select("
            select DISTINCT c.id, i.title, i.name
            from content c
            join products_categories pc on pc.pid in ($in) and pc.cid=c.id
            join content_info i on i.content_id = c.id and i.languages_id = '{$this->languages_id}'
            where c.auto=0 and c.published = 1
            order by i.name
            ")->all();
    }

    /**
     * @param $id
     * @param string $limit
     * @return array|mixed
     */
    public function getVendors($id, $limit='', $pids=array())
    {
        if(!empty($limit)){
            $limit = " limit 0, {$limit}";
        }
        $join = '';
        if(!empty($pids)){
            $join = "join features_content_values fcv on
                fcv.features_id=fv.features_id
                and fcv.features_values_id=fv.id
                and fcv.content_id in (". implode(',', $pids) .")
                ";
        }
        return $this->db->select("
            select DISTINCT fv.id, i.value as name
            from features_values fv
            {$join}
            join features_values_info i on i.values_id=fv.id and i.languages_id='{$this->languages_id}'
            where fv.features_id='{$id}'
            order by name asc
            {$limit}
        ")->all();
    }

    /**
     * @param $id
     * @return array|mixed
     */
    public function getVendorsLetters($id)
    {
        return $this->db->select("
            select DISTINCT LEFT(i.value, 1) as letter
            from features_values fv
            join features_values_info i on i.values_id=fv.id and i.languages_id='{$this->languages_id}'
            where fv.features_id='{$id}'
            order by letter asc
        ")->all();
    }

    /**
     * @param $id
     * @param $letter
     * @return array|mixed
     */
    public function getVendorsLettersItems($id, $letter)
    {
        return $this->db->select("
            select DISTINCT i.value, fv.id
            from features_values fv
            join features_values_info i on i.values_id=fv.id and i.languages_id='{$this->languages_id}'
            where fv.features_id='{$id}' and i.value like '{$letter}%'
            order by i.value asc
        ")->all();
    }

    /**
     * @param $content_id
     * @return string
     */
    public function getSeason($content_id)
    {
        $r = $this->db->select("
                 select features_values_id
                 from features_content_values
                 where content_id={$content_id} and features_id=7
            ")
            ->row('features_values_id');
        switch($r){
            case 10:
                $type = 'winter';
                break;
            case 11:
                $type = 'summer';
                break;
            case 12:
                $type = 'all-seasons';
                break;
            default:
                $type = 'all-seasons';
                break;
        }

        return $type;
    }

    /**
     * @param $id
     * @return array|mixed
     */
    public function getVendorName($id)
    {
        return $this->db->select("select vendor from car_vendors where id='{$id}' limit 1")->row('vendor');
    }

    /**
     * @param $id
     * @return array|mixed
     */
    public function getModelName($id)
    {
        return $this->db->select("select model from car_models where id='{$id}' limit 1")->row('model');
    }

    /**
     * @param $id
     * @return array|mixed
     */
    public function getYearName($id)
    {
        return $this->db->select("select `year` from car_years where id='{$id}' limit 1")->row('year');
    }

    /**
     * @param $id
     * @return array|mixed
     */
    public function getModificationName($id)
    {
        return $this->db->select("select `modification` from car_modification where id='{$id}' limit 1")->row('modification');
    }
} 