<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 13.10.14 21:44
 */

namespace models\modules;

use models\App;

defined("SYSPATH") or die();

/**
 * Class Catalog
 * @package models\modules
 */
class Catalog extends App{

    private $debug = 0;

    private $where = '';
    private $join =  '';
    private $order = 'order by abs(u.hot) desc,u.hot_date desc,abs(u.gold) desc,u.gold_date desc,(u.point+u.mark) desc,u.name COLLATE  utf8_unicode_ci ASC ';
    private $limit = '';
    private $key = '';

    private $currency_id;

    /**
     * @param $status
     * @return $this
     */
    public function debug($status=1)
    {
        $this->debug = $status;
        return $this;
    }

    /**
     * @param $q
     * @return $this
     */
    public function where($q)
    {
        if(!empty($this->where) && strpos($q, 'and') === false){
            $this->where .= ' and ';
        }
        $this->where .= $q;

        return $this;
    }

    /**
     * @param $q
     * @return $this
     */
    public function join($q)
    {
        $this->join .= $q;

        return $this;
    }

    /**
     * @param $o
     * @return $this
     */
    public function order($o)
    {
        $this->order = " order by "  . $o;

        return $this;
    }

    /**
     * @param $start
     * @param $num
     */
    public function limit($start, $num)
    {
        $this->limit = " limit $start, $num";
    }

    public function key($query)
    {
        $this->key = $query;

        return $this;
    }
    /**
     * @return array|mixed
     */
    public function getWorkersTotal()
    {
        $w = !empty($this->where) ? " where {$this->where}" : '';
        return $this->db->select("
            select count( u.id ) as t
            from users u
            {$this->join}
            join users_group_info ugi on ugi.users_group_id=u.users_group_id and ugi.languages_id = {$this->languages_id}
            join pages_workers_group pwg on pwg.users_group_id=u.users_group_id
            {$w}
            ", $this->debug)->row('t');
    }

    public function getWorkersGroupsID()
    {
        $r = $this->db->select("select id from users_group where parent_id=2")->all();
        $res = array();
        foreach ($r as $row) {
            $res[] = $row['id'];
        }
        return $res;
    }
    /**
     * @return array|mixed
     */
    public function getWorkers()
    {

        $this->currency_id = $this->db->select("select id from currency where is_main=1")->row('id');

        $w = !empty($this->where) ? " where {$this->where}" : '';
        return $this->db->select("
            select u.id, u.name, u.surname, u.avatar, u.city_id, u.price_per_hour, u.price_per_day, u.url,

             ugi.name as group_name, pwg.content_id,
             cu.symbol {$this->key},
             if(cr.id!=u.currency_id,u.price_per_day/cu.rate,u.price_per_day) as new_price,
             u.hot,u.gold
            from users u
            {$this->join}
            left join currency cu on cu.id=u.currency_id
             join currency cr on cr.id= {$this->currency_id}
            join users_group_info ugi on ugi.users_group_id=u.users_group_id and ugi.languages_id = {$this->languages_id}
            join pages_workers_group pwg on pwg.users_group_id=u.users_group_id

            {$w}
            group by u.id
            {$this->order}
            {$this->limit}
            ",$this->debug)->all();
    }

    public function getAllWorkers()
    {
        return $this->db->select("
                Select * from users
        ")->all();
    }

    /**
     * @param $id
     * @return array|mixed
     */
    public function getWorkerInfo($id, $users_id = 0)
    {
//        $this->stat($id);
        return $this->db->select("
            select u.*,
             ugi.name as group_name, pwg.content_id,
             IF(uwl.id>0, 'in' ,'') as on_wishlist
            from users u
            {$this->join}
            join users_group_info ugi on ugi.users_group_id=u.users_group_id and ugi.languages_id = {$this->languages_id}
            join pages_workers_group pwg on pwg.users_group_id=u.users_group_id
            left join users_wishlist uwl on uwl.users_id = {$users_id} and uwl.workers_id=u.id
            where u.id = '{$id}'
            ", $this->debug)->row();
    }

    /**
     * @param $users_id
     */
    public function stat($users_id)
    {
        if(isset($_SESSION['stat_view'][$users_id])) return;

        $now = date('Y-m-d');
        $r = $this->db->select("select id,views from users_stat where users_id = {$users_id} and date= '{$now}'")->row();
        if(empty($r)){
            $this->db->insert('users_stat', array('users_id' => $users_id, 'views' => 1, 'date' => $now));
        } else{
            $id = $r['id']; $views = $r['views']; $views ++;
            $this->db->update('users_stat', array('views' => $views), " id = {$id} limit 1");
        }
        @$_SESSION['stat_view'][$users_id] = 1;
    }

    public function getViews($id)
    {
        return $this->db->select("Select count(views) as views from users_stat where users_id = {$id} ")->row("views");
    }

    /**
     * @param $content_id
     * @return array|mixed
     */
    public function workersGroupID($content_id)
    {
        return $this->db->select("select users_group_id from pages_workers_group where content_id={$content_id}")
            ->row('users_group_id');
    }

    public function getUploads($users_id, $limit = 0)
    {
        $limit = $limit>0 ? " limit {$limit}" : '';
        return $this->db->select("select * from users_uploads where users_id = {$users_id} order by abs(id) desc,abs(sort) asc {$limit}")->all();
    }


    /**
     * @param $parent_id
     * @return array|mixed
     */
    public function getItems($parent_id)
    {
        return $this->db->select("
            select c.id, i.title, i.name,c.isfolder, i.description
            from content c
            join content_info i on i.content_id = c.id and i.languages_id = {$this->languages_id}
            where c.parent_id={$parent_id} and c.published = 1 and c.auto=0
            order by abs(c.sort) asc, c.id asc
            ")->all();
    }

    public function categoriesOnMain($parent_id)
    {
        return $this->db->select("
            select c.id, i.title, i.name,c.isfolder, i.description
            from content c
            join content_info i on i.content_id = c.id and i.languages_id = {$this->languages_id}
            where c.parent_id={$parent_id} and c.published = 1 and c.auto=0 and c.position > 0
            order by abs(c.position) asc, abs(c.sort) asc, c.id asc
            ")->all();
    }

    public function getWorkersCount($content_id)
    {
        return $this->db->select("
            select count(u.id) as t
            from users u
            join pages_workers_group g on g.content_id='{$content_id}' and u.users_group_id=g.users_group_id
        ")->row('t');
    }

    public function getUsersGroup($parent_id)
    {
        return $this->db->select("
              select g.id, i.name, pwg.content_id
              from users_group g
              join users_group_info i on i.users_group_id=g.id and i.languages_id = '{$this->languages_id}'
              join pages_workers_group pwg on pwg.users_group_id=g.id
              where g.parent_id = '{$parent_id}'
              order by abs(g.sort) asc
        ")->all();
    }
//    public function getMainGalleryItems()
//    {
//        return $this->db->select("select uu.file,uu.type,uu.users_id,uu.provider,uu.thumb,CONCAT(u.name,' ',u.surname) as pib, ugi.name as ug_name
//                                  from users u
//                                  join users_uploads uu on u.id=uu.users_id
//                                  join users_group ug on ug.id=u.users_group_id
//                                  join users_group_info ugi on ugi.users_group_id=ug.id and ugi.languages_id={$this->languages_id}
//                                  where u.hot = 1 or u.gold = 1
//                                  GROUP by u.id
//                                  order by rand()
//                                  limit 12
//                                  ")->all();
//    }

    public function getMainGalleryItems()
    {
        $limit = 12;
        $items = $this->db->select("select uu.file,uu.type,uu.users_id,uu.provider,uu.thumb,CONCAT(u.name,' ',u.surname) as pib, ugi.name as ug_name
                                  from users_uploads uu
                                  join users u on u.id=uu.users_id
                                  join users_group ug on ug.id=u.users_group_id
                                  join users_group_info ugi on ugi.users_group_id=ug.id and ugi.languages_id={$this->languages_id}
                                  where u.hot = 1 or u.gold = 1
                                  group by u.id
                                  order by rand()
                                  limit {$limit}
                                  ")->all();

        if (count($items) >= $limit) return $items;

        $new_limit = $limit - count($items);

        $new_items = $this->db->select("
                  select uu.file,uu.type,uu.users_id,uu.provider,uu.thumb,CONCAT(u.name,' ',u.surname) as pib, ugi.name as ug_name
                  from users_uploads uu
                  join users u on u.id=uu.users_id
                  join users_group ug on ug.id=u.users_group_id
                  join users_group_info ugi on ugi.users_group_id=ug.id and ugi.languages_id={$this->languages_id}
                  group by u.id
                  order by rand()
                  limit {$new_limit}
          ")->all();

        $items = array_merge($items, $new_items);
        shuffle($items);
        return $items;
    }

    /**
     * @param $workers_id
     * @return array|mixed
     */
    public function getCommentsTotal($workers_id)
    {
        return $this->db->select("select count(id) as t
               from comments
               where workers_id = '{$workers_id}' and status = 'approved'
               ")
            ->row('t');
    }

    /**
     * @param $id
     * @return array|mixed
     */
    public function getTemplatesID($id)
    {
        return $this->data('content', $id, 'templates_id');
    }

    public function updateMark($id,$point)
    {
        return $this->db->update("users",array(
            'mark'=>$point
        )," id={$id} limit 1");
    }

    public function getSeo($service_id, $city_id)
    {
        return $this->db->select("
                                  SELECT ci.name, ci.title, ci.description, ci.keywords, ci.content FROM content c
                                  JOIN content_info ci ON ci.content_id = c.id AND ci.languages_id={$this->languages_id}
                                  JOIN service_city sc ON sc.content_id = c.id
                                  WHERE sc.service_id = $service_id AND sc.city_id = $city_id        
                                  ")->row();
    }

    /**
     * @deprecated
     * @param $url
     * @return mixed
     */
    public function getIdByUrl($url)
    {
        return $this->db->select("
            SELECT id FROM users where url = '{$url}' limit 1       
        ")->row('id');
    }
    public function getUserIdByUrl($url)
    {
        return $this->db->select("
            SELECT id FROM users where url = '{$url}' limit 1       
        ")->row('id');
    }

    public function getUrlById($id)
    {
        return $this->db->select("
            select url from users where id = '{$id}' limit 1
        ")->row('url');
    }
} 