<?php
/**
 * Created by PhpStorm.
 * User: taras
 * Date: 09.03.16
 * Time: 17:28
 */

namespace models\modules;


use models\App;

class Forum extends App
{
    private $where = array();
    private $join = array();
    private $limit = '';
    private $key = '';


    public function getTotalArticles()
    {
        $w = $this->getWhere();

        $join = $this->getJoin();
        return $this->db->select("
                Select count(f.id) as t from forum f

                {$join}
                where  f.auto=0 {$w}
        ")->row("t");
    }

    public function getCategory()
    {
        return $this->db->select("
                Select c.id,ci.name,ci.title
                from content c
                join content_info ci on c.id=ci.content_id and ci.languages_id={$this->languages_id}
                where c.type_id=11 and c.auto=0 and c.published=1
                order by abs(c.sort) asc
        ")->all();
    }

    public function getArticles()
    {
        $w = $this->getWhere();
        $join = $this->getJoin();
        if(isset($this->key) && !empty($this->key)) {
            $this->key = ",".$this->key;
        }
        return $this->db->select("
                Select f.*, CONCAT_WS( ' ' ,u.name, u.surname) as username,u.id as user_id,
                  DATE_FORMAT(f.createdon, '%d') as dd,
                    DATE_FORMAT(f.createdon, '%m') as mm,
                    DATE_FORMAT(f.createdon, '%Y') as yy,
                    DATE_FORMAT(f.createdon, '%H') as hh,
                    DATE_FORMAT(f.createdon, '%i') as ii,
                    DATE_FORMAT(f.createdon, '%s') as ss,
                    (Select date from forum_comment where forum_id=f.id order by date desc limit 1) as lastCommentDate
                    {$this->key}
                 from forum f
                join users u on u.id = f.owner_id

                {$join}
                where f.auto=0 {$w}
                group by f.id
                ORDER by lastCommentDate desc,f.createdon desc
                 {$this->limit}
        ")->all();
    }

    public function getPermision($id,$uid)
    {
        return $this->db->select("Select id from forum where id={$id} and owner_id={$uid} limit 1")->row("id");
    }

    public function createAuto($uid,$cid)
    {
        $this->db->delete("forum", " auto=1 and owner_id={$uid}");
        return $this->db->insert("forum",array(
            'owner_id'=>$uid,
            'cid'=>$cid,
            'createdon'=>date('Y-m-d H:i:s')
        ));
    }

    public function getParticipants($id)
    {
        return $this->db->select("
               Select u.id,CONCAT_WS( ' ' ,u.name, u.surname) as username,u.avatar from forum_event_users fu
               join users u on u.id=fu.users_id
               where fu.forum_id = {$id}
        ")->all();
    }

    public function takePart($article_id,$user_id)
    {
        $id = $this->db->select("Select id from forum_event_users
                                where forum_id={$article_id} and users_id={$user_id} limit 1")->row("id");

        if($id>0) {
            $s = 1;
            $this->db->delete("forum_event_users","id={$id}");
        } else {
            $s = 2;
            $this->db->insert("forum_event_users",array(
                'forum_id'=>$article_id,
                'users_id'=>$user_id
            ));
        }

        return $s;
    }

    public function getUserPart($article_id,$user_id)
    {
        return $this->db->select("Select id from forum_event_users
                                where forum_id={$article_id} and users_id={$user_id} limit 1")->row("id");
    }

    public function update($id,$data)
    {
        return $this->db->update("forum",$data,"id={$id}");
    }

    public function getInfo($id)
    {
        $where = "";
        if(isset($_SESSION['app']['user']['id'])) {
            $where = "and ff.users_id={$_SESSION['app']['user']['id']}";
        }

        return $this->db->select("Select f.id,f.name,f.content,f.event_start_date,f.event_end_date,f.event_town,f.event_place,f.event,f.owner_id,
                    f.cid,
                    DATE_FORMAT(createdon, '%d') as dd,
                    DATE_FORMAT(createdon, '%m') as mm,
                    DATE_FORMAT(createdon, '%Y') as yy,
                    DATE_FORMAT(createdon, '%H') as hh,
                    DATE_FORMAT(createdon, '%i') as ii,
                    DATE_FORMAT(createdon, '%s') as ss,
                    DATE_FORMAT(event_start_date, '%d') as dds,
                    DATE_FORMAT(event_start_date, '%m') as mms,
                    DATE_FORMAT(event_start_date, '%Y') as yys,
                    DATE_FORMAT(event_start_date, '%H') as hhs,
                    DATE_FORMAT(event_start_date, '%i') as iis,
                    DATE_FORMAT(event_start_date, '%s') as sss,
                    DATE_FORMAT(event_end_date, '%d') as dde,
                    DATE_FORMAT(event_end_date, '%m') as mme,
                    DATE_FORMAT(event_end_date, '%Y') as yye,
                    DATE_FORMAT(event_end_date, '%H') as hhe,
                    DATE_FORMAT(event_end_date, '%i') as iie,
                    DATE_FORMAT(event_end_date, '%s') as sse,
                     ff.status as follow
              from forum f
                left join forum_follow ff on ff.forum_id=f.id {$where}
               where f.id={$id} limit 1")->row();
    }

    public function getParentGroupId($id)
    {
        return $this->db->select("Select parent_id from users_group where id={$id} limit 1")->row("parent_id");
    }

    public function getIdByHandle($id)
    {
        return $this->db->select("Select alias from forum where id={$id} limit 1")->row();
    }

    public function getTotalComment($id)
    {
        $w = $this->getWhere();
        return $this->db->select("Select count(id) as t from forum_comment where forum_id={$id} and auto=0 {$w}")->row("t");
    }

    public function delete($id)
    {
        return $this->db->delete("forum","id={$id} limit 1");
    }

    public function following($id,$uid,$status)
    {
        $s = $this->db->select("
            Select id from forum_follow where forum_id={$id} and users_id={$uid} and status='{$status}' limit 1
        ")->row("id");

        if($s>0) {
            $this->db->delete("forum_follow"," id={$s}");
            $s = 0;
        } else {
            $s = $this->db->insert("forum_follow",array(
                'forum_id'=>$id,
                'users_id'=>$uid,
                'status'=>$status
            ));
        }

        return $s;

    }

    public function getComments($id, $parent_id=0)
    {
        $res = array();
        $r = $this->db->select("
            select c.*, DATE_FORMAT(c.date, '%d.%m.%Y') as pubdate, CONCAT_WS( ' ' ,u.name, u.surname) as username
            from forum_comment c
            join users u on u.id = c.users_id
            where c.forum_id = {$id}  and parent_id={$parent_id} and auto=0
            order by abs(c.id) desc
             {$this->limit}
        ")->all();
        foreach ( $r as $row) {
            if($row['isfolder']){
                $row['items'] = $this->getComments($id, $row['id']);
            }
            $row['portfolio'] = $this->getCommentMedia($row['id']);
            $res[] = $row;
        }

        return $res;
    }

    public function getLastComment($id)
    {
        return $this->db->select("
                Select c.*, DATE_FORMAT(c.date, '%d.%m.%Y') as pubdate, CONCAT_WS( ' ' ,u.name, u.surname) as username
                 from forum_comment c
                 join users u on u.id = c.users_id
                  where c.id = {$id}  limit 1
                ")->row();
    }

    public function getMedia($id)
    {
        return $this->db->select("select * from forum_photo where forum_id = {$id} order by abs(sort) asc")->all();
    }

    public function getMediaInfo($id)
    {
        return $this->db->select("select * from forum_photo where id = {$id} limit 1")->row();
    }



    public function createMedia($forum_id, $file, $type = 'img', $provider = '', $thumb = '')
    {
        $sort = $this->db->select("select count(id) as t from forum_photo where forum_id={$forum_id}")->row('t');
        $sort ++;
        return $this->db->insert(
            'forum_photo',
            array (
                'forum_id' => $forum_id,
                'path'     => $file,
                'type'     => $type,
                'sort'     => $sort,
                'provider' => $provider,
                'thumb'    => $thumb
            )
        );
    }

    public function updateMedia($id)
    {
        return $this->db->update("forum_photo",array(
            'auto'=>0
        ),"forum_id={$id}");
    }

    public function getUploads($id, $limit = 0)
    {
        $limit = $limit>0 ? " limit {$limit}" : '';
        return $this->db->select("select * from forum_photo where forum_id = {$id} order by abs(sort) asc {$limit}")->all();
    }

    public function createComment($data)
    {
        $id = $data['comment_id'];
        unset($data['comment_id']);
        unset($data['forum_id']);
        $data['auto'] = 0;
        return $this->db->update("forum_comment",$data,"id={$id}");
        return $this->db->insert('forum_comment', $data);
    }

    public function createCommentAuto($id,$uid)
    {
        $this->db->delete("forum_comment", " auto=1 and users_id={$uid} and forum_id={$id}");
        return $this->db->insert('forum_comment',array(
            'forum_id'=>$id,
            'users_id'=>$uid
        ));
    }

    public function getCommentMedia($id)
    {
        return $this->db->select("select * from forum_comment_photo where forum_comment_id = {$id} order by abs(sort) asc")->all();
    }

    public function createCommentMedia($forum_comment_id, $file, $type = 'img', $provider = '', $thumb = '')
    {
        $sort = $this->db->select("select count(id) as t from forum_comment_photo where forum_comment_id={$forum_comment_id}")->row('t');
        $sort ++;
        return $this->db->insert(
            'forum_comment_photo',
            array (
                'forum_comment_id' => $forum_comment_id,
                'path'     => $file,
                'type'     => $type,
                'sort'     => $sort,
                'provider' => $provider,
                'thumb'    => $thumb
            )
        );
    }

    /**
     * @param $q
     * @return $this
     */
    public function setWhere($q)
    {
        $this->where[] = $q;

        return $this;
    }
    /**
     * @return string
     */
    private function getWhere()
    {
        $w = '';
        if(!empty($this->where)){
            $w = implode(' ', $this->where);
        }
        return $w;
    }

    /**
     * @param $o
     * @return $this
     */
    public function order($o)
    {
        $this->order = " order by "  . $o;

        return $this;
    }

    /**
     * @param $start
     * @param $num
     */
    public function limit($start, $num)
    {
        $this->limit = " limit $start, $num";

        return $this;
    }

    public function key($query)
    {
        $this->key = $query;

        return $this;
    }

    /**
     * @param $q
     * @return $this
     */
    public function setJoin($join)
    {
        $this->join[] = $join;

        return $this;
    }

    /**
     * @return string
     */
    private function getJoin()
    {
        $o = '';
        if(!empty($this->join)){
            $o = implode(' ', $this->join);
        }
        return $o;
    }

    public function clearWhere()
    {
        $this->where = array();
        return $this;
    }


}