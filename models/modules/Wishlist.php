<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 13.10.14 21:44
 */

namespace models\modules;

use models\App;

defined("SYSPATH") or die();

/**
 * Class Wishlist
 * @package models\modules
 */
class Wishlist extends App{

    private $debug = 0;


    /**
     * @param $status
     * @return $this
     */
    public function debug($status=1)
    {
        $this->debug = $status;
        return $this;
    }

    /**
     * @param $users_id
     * @param $workers_id
     * @return bool|string
     */
    public function create($users_id, $workers_id)
    {
        return $this->db->insert("users_wishlist", array('users_id' => $users_id, 'workers_id' => $workers_id));
    }

    /**
     * @param $users_id
     * @param $workers_id
     * @return int
     */
    public function delete($users_id, $workers_id)
    {
        return $this->db->delete("users_wishlist", " users_id = {$users_id} and workers_id = {$workers_id} limit 1 ");
    }
} 