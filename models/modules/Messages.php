<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 13.10.14 21:44
 */

namespace models\modules;

use models\App;

defined("SYSPATH") or die();

/**
 * Class Messages
 * @package models\modules
 */
class Messages extends App{

    private $debug = 0;
    private $where = '';
    private $limit = '';

    /**
     * @param $status
     * @return $this
     */
    public function debug($status=1)
    {
        $this->debug = $status;
        return $this;
    }

    /**
     * @param $q
     * @return $this
     */
    public function where($q)
    {
        $this->where .= $q;

        return $this;
    }

    public function limit($s, $n)
    {
        $this->limit = " limit $s, $n";
        return $this;
    }

    /**
     * @param $from_id
     * @param $to_id
     * @param $message
     * @return bool|string
     */
    public function create($from_id, $to_id, $message)
    {
//        $dialogs_id = $this->getDialogsId($from_id, $to_id);

        return $this->db->insert(
            "messages",
            array(
                'from_id'    => $from_id,
                'to_id'      => $to_id,
//                'dialogs_id' => $dialogs_id,
                'message'    => $message
            )
        );
    }

    /**
     * @param $from_id
     * @param $to_id
     * @return array|mixed
     */
    public function get($from_id, $to_id)
    {
        return $this->db->select("
            select m.*,
              DATE_FORMAT(m.createdon, '%d.%m.%Y') as dd,
              DATE_FORMAT(m.createdon, '%H:%i') as hh,
              CONCAT(u.name, ' ', u.surname) as username, u.email, u.phone, u.users_group_id, u.id as user_id,
              IF(m.from_id = {$from_id}, 1 , 0) as is_out
            from messages m
            join users u on u.id = '{$to_id}'
            where (from_id = '{$from_id}' and to_id = '{$to_id}') or (from_id = '{$to_id}' and to_id = '{$from_id}')
             {$this->where}
            order by m.id asc
            {$this->limit}
            ", $this->debug)->all();
    }

    public function getNew($from_id, $to_id)
    {
        return $this->db->select("
            select m.*,
              DATE_FORMAT(m.createdon, '%d.%m.%Y') as dd,
              DATE_FORMAT(m.createdon, '%H:%i') as hh,
              CONCAT(u.name, ' ', u.surname) as username, u.email, u.phone,
              IF(m.from_id = {$from_id}, 1 , 0) as is_out
            from messages m
            join users u on u.id = '{$to_id}'
            where (m.from_id = '{$to_id}' and m.to_id = '{$from_id}') and m.is_read = 0
            order by m.id asc
            limit 1
            ")->all();
    }

    public function getItem($id)
    {
        return $this->db->select("
            select m.*,
              DATE_FORMAT(m.createdon, '%d.%m.%Y') as dd,
              DATE_FORMAT(m.createdon, '%H:%i') as hh,
              CONCAT(u.name, ' ', u.surname) as username, u.email, u.phone
            from messages m
            join users u on u.id = m.to_id
            where m.id={$id}
            ", $this->debug)->row();
    }

    public function setRead($id)
    {
        return $this->db->update('messages',array('is_read' => 1, 'read_date' => date('Y-m-d H:i:s')), " id = {$id} limit 1");
    }

    /**
     * @param $users_id
     * @return array|mixed
     */
    public function getDialogs($users_id)
    {
        return $this->db->select("
            select * from (
            select
            m.*,
              DATE_FORMAT(m.createdon, '%d.%m.%Y') as dd,
              DATE_FORMAT(m.createdon, '%H:%i') as hh,
              CONCAT(u.name, ' ', u.surname) as username, u.email, u.phone, u.id as user_id, u.users_group_id,
              IF(m.from_id = {$users_id}, 1 , 0) as is_out,
              pw.content_id, ci.name as group_name
            from messages m
            join users u on u.id = IF(m.from_id = {$users_id}, m.to_id, m.from_id)
            left join pages_workers_group pw on pw.users_group_id = u.users_group_id
            left join users_group_info ci on ci.users_group_id=u.users_group_id and ci.languages_id = {$this->languages_id}
            where {$users_id} in (m.from_id, m.to_id) {$this->where}
              order by abs(m.id) desc
            limit 50
            ) as t group by user_id
            order by t.createdon desc
            ", $this->debug)->all();


        return $this->db->select("
            select DISTINCT m.from_id, m.*,
              DATE_FORMAT(m.createdon, '%d.%m.%Y') as dd,
              DATE_FORMAT(m.createdon, '%H:%i') as hh,
              CONCAT(u.name, ' ', u.surname) as username, u.email, u.phone, u.id as user_id,
              IF(m.from_id = {$users_id}, 1 , 0) as is_out
            from messages m
            join users u on u.id = IF(m.from_id = {$users_id}, m.to_id, m.from_id)
            where {$users_id} in (m.from_id, m.to_id)
            order by m.createdon desc
            ", $this->debug)->all();
    }

    public function getTotalNewMessages($users_id)
    {
        return $this->db->select("
            select count(id) as t
            from messages
            where {$users_id} = to_id and is_read = 0
            ", $this->debug)->row('t');
    }

    /**
     * @param $id
     * @param $user_id
     * @return int
     */
    public function delete($id, $user_id)
    {
        return $this->db->delete("messages", " id = {$id} and {$user_id} in (from_id, to_id) limit 1");
    }

    /**
     * @param $id
     * @param $status
     * @param $user_id
     * @return bool
     */
    public function setImportant($id, $status, $user_id)
    {
        return $this->db->update("messages", array('important' => $status) , " id = {$id} and {$user_id} in (from_id, to_id) limit 1");
    }
} 