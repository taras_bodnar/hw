<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 25.11.14 14:39
 */

namespace models\modules;

use models\App;

defined("SYSPATH") or die();

/**
 * Class Comments
 * @package models\modules
 */
class Comments extends App {

    public $workers_id = 0;
    private $limit = '';

    public function __construct($workers_id)
    {
        parent::__construct();

        $this->workers_id = $workers_id;
    }


    /**
     * @return array|mixed
     */
    public function get()
    {
        return $this->db->select("
            select c.*, DATE_FORMAT(c.createdon, '%d.%m.%Y') as pubdate, CONCAT_WS( ' ' ,u.name, u.surname) as username
            from comments c
            join users u on u.id = c.users_id
            where c.workers_id = {$this->workers_id} and c.status = 'approved'
            order by abs(c.id) desc
            {$this->limit}
        ")->all();
    }

    /**
     * @return array|mixed
     */
    public function getTotal()
    {
        return $this->db->select("
            select count(c.id) as t
            from comments c
            join users u on u.id = c.users_id
            where c.workers_id = {$this->workers_id} and c.status = 'approved'
        ")->row('t');
    }

    /**
     * @param $s
     * @param $n
     * @return $this
     */
    public function limit($s, $n)
    {
        $this->limit = " limit $s, $n";
        return $this;
    }

    /**
     * @param $data
     * @return bool|string
     */
    public function create($data)
    {
        return $this->db->insert('comments', $data);
    }
} 