<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 13.10.14 21:44
 */

namespace models\modules;

use models\App;

defined("SYSPATH") or die();

/**
 * Class UsersUploads
 * @package models\modules
 */
class UsersUploads extends App{

    private $debug = 0;


    /**
     * @param $status
     * @return $this
     */
    public function debug($status=1)
    {
        $this->debug = $status;
        return $this;
    }

    /**
     * @param $users_id
     * @param $file
     * @param string $type
     * @return bool|string
     */
    public function create($users_id, $file, $type = 'img', $provider = '', $thumb = '')
    {
        $sort = $this->db->select("select count(id) as t from users_uploads where users_id={$users_id}")->row('t');
        $sort ++;
        return $this->db->insert(
            'users_uploads',
            array (
                'users_id' => $users_id,
                'file'     => $file,
                'type'     => $type,
                'sort'     => $sort,
                'provider' => $provider,
                'thumb'    => $thumb
            )
        );
    }

    public function exist($file,$type = 'img',$provider = null)
    {
        $w = !empty($provider)?" and provider='{$provider}'":"";
        return $this->db->select("Select count(id) as t from users_uploads 
                    where file='{$file}' and type='{$type}' {$w} ")->row("t");
    }

    /**
     * @param $users_id
     * @return array|mixed
     */
    public function getItems($users_id)
    {
        return $this->db->select("select * from users_uploads where users_id = {$users_id} order by abs(sort) asc")->all();
    }

    public function getInfo($id, $users_id)
    {
        return $this->db->select("select file, type from users_uploads where id = {$id} and users_id = {$users_id}")->row();
    }

    /**
     * @param $users_id
     * @param $order
     * @return int
     */
    public function sort($users_id, $order)
    {
        $o = explode(',', $order);
        foreach ($o as $sort=> $id) {
            $id = str_replace('pf-item-', '', $id);
            $id = (int)$id;
            $this->db->update('users_uploads', array('sort'=> $sort), " id='{$id}' and users_id='{$users_id}' limit 1");
        }
        return 1;
    }
} 