<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 25.11.14 14:39
 */

namespace models\modules;

use models\App;

defined("SYSPATH") or die();

/**
 * Class Delivery
 * @package models\modules
 */
class Delivery extends App {
    /**
     * @return array|mixed
     */
    public function get()
    {
        return $this->db->select("
            select d.id, d.price, d.free_from, di.name, di.description
            from delivery d
            join delivery_info di on di.delivery_id=d.id and di.languages_id={$this->languages_id}
            where d.published=1
            order by abs(d.sort) asc, di.name asc
            ")->all();
    }
} 