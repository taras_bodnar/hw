<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 13.10.14 21:44
 */

namespace models\modules;

use models\App;

defined("SYSPATH") or die();

/**
 * Class UsersOnline
 * @package models\modules
 */
class UsersOnline extends App {

    private static $instance;

    public static function instance()
    {
        if(! self::$instance){
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * @param $users_id
     */
    public function set($users_id)
    {
        $id = $this->db->select("select id from users_online where users_id = {$users_id} limit 1")->row('id');
        if(empty($id)){
            $this->db->insert('users_online', array('users_id' => $users_id));
        } else{
            $this->db->update('users_online', array('lastvisit' => date('Y-m-d H:i:s')), " id ={$id}");
        }
    }

    /**
     * @param $users_id
     * @return bool
     */
    public function isOnline($users_id)
    {
        return $this->db->select("select id from users_online where users_id = {$users_id} limit 1")->row('id') > 0;
    }

    /**
     * @param $users_id
     * @return int
     */
    public function setOffline($users_id)
    {
        return $this->db->delete('users_online', "users_id = {$users_id} limit 1");
    }

    /**
     * clear offline users
     * @return int
     */
    public function clearOffline()
    {
        return $this->db->delete('users_online', " lastvisit < SUBTIME(NOW(),'0 0:10:0')");
    }
 }