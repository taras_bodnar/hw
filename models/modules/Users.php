<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 13.10.14 21:44
 */

namespace models\modules;

use models\App;

defined("SYSPATH") or die();

/**
 * Class Users
 * @package models\modules
 */
class Users extends App{

    protected $debug = 0;
    private $users_group_id = 4;

    protected $where = '';
    protected $join =  '';
    protected $order = '';
    protected $limit = '';


    /**
     * @param $status
     * @return $this
     */
    public function debug($status=1)
    {
        $this->debug = $status;
        return $this;
    }

    /**
     * @param $q
     * @return $this
     */
    public function where($q)
    {
        $this->where .= $q;

        return $this;
    }

    public function clearWhere()
    {
        $this->where = '';

        return $this;
    }

    /**
     * @param $q
     * @return $this
     */
    public function join($q)
    {
        $this->join .= $q;

        return $this;
    }

    /**
     * @param $o
     * @return $this
     */
    public function order($o)
    {
        $this->order = " order by "  . $o;

        return $this;
    }

    /**
     * @param $start
     * @param $num
     */
    public function limit($start, $num)
    {
        $this->limit = " limit $start, $num";
    }

    /**
     * @param $data
     * @return bool|string
     */
    public function create($data)
    {
        if(isset($data['provider'])){

            $provider = $data['provider'];
            $social_id = $data['social_id'];
        }

        unset($data['provider'], $data['social_id']);

        $data['createdon'] = date('Y-m-d H:i:s');
        $data['password'] = $this->encodePassword($data['password']);

        if( !isset($data['users_group_id'])){
            $data['users_group_id'] = $this->users_group_id;
        }
        if( !isset($data['languages_id'])){
            $data['languages_id'] = $this->languages_id;
        }

        $data['sort'] = time();

        $this->db->beginTransaction();

        $users_id =  $this->db->insert('users', $data);
        $sid=1;
        if(isset($provider) && isset($social_id)){
            $sid = $this->addUsersProvider($users_id, $provider, $social_id);
        }

        if($users_id>0 && $sid > 0){
            $this->db->commit();
        } else {
            $this->db->rollBack();
            return 0;
        }

        return $users_id;
    }

    /**
     * @param $users_id
     * @param $provider
     * @param $social_id
     * @param bool|false $ignore
     * @return bool|string
     */
    public function addUsersProvider($users_id, $provider, $social_id, $ignore = false)
    {
        if($social_id == '') return 0;
        return $this->db->insert(
            'users_oauth',
            array(
                'users_id' => $users_id,
                'provider' => $provider,
                'social_id' => $social_id
            ),
            0,
            $ignore
        );
    }
    /**
     * @param $id
     * @param $data
     * @return bool
     */
    public function update($id, $data)
    {
        $data['editedon'] = date('Y-m-d H:i:s');
        if(isset($data['users_group_id']) && !empty($data['users_group_id'])) {
            $_SESSION['app']['user']['users_group_id'] = $data['users_group_id'];
        }
        return $this->db->update('users', $data, " id = '{$id}' limit 1");
    }

    public function insertCities($uid,$cities)
    {

      $id = $this->db->delete("users_cities","users_id={$uid}");

        if($id) {
            foreach ( $cities as  $k=>$item) {
                if (empty($item)) continue;
                $id = $this->db->insert("users_cities",
                    array(
                        'users_id'=>$uid,
                        'guides_id'=>$item,
                        'main'=>$k==0?1:0
                    )
                    );
            }
        }

        return $id;
    }

    public function getUserCityMain($uid)
    {
        return $this->db->select("Select guides_id from users_cities 
          where users_id={$uid} and main = 1
        ")->row("guides_id");
    }

    /**
     * @param $parent_id
     * @return array|mixed
     */
    public function getUsersGroup($parent_id)
    {
        return $this->db->select("
              select g.id, i.name, i.description
              from users_group g, users_group_info i
              where g.parent_id = '{$parent_id}' and i.users_group_id=g.id and i.languages_id = '{$this->languages_id}'
              order by abs(g.sort) asc
        ")->all();
    }

    /**
     * @param $id
     * @param string $key
     * @return array|mixed
     */
    public function getInfoById($id, $key='*')
    {
        return $this->db->select("
          select u.*, g.rang
          from users u
          join users_group g on g.id=u.users_group_id
          where u.id = '{$id}'
          limit 1")->row($key);
    }

    public function info($id, $key='*')
    {
        return $this->db->select("
          select {$key} from users where id = '{$id}' limit 1")->row($key);
    }

    /**
     * @param $email
     * @return array|mixed
     */
    public function getInfoByEmail($email)
    {
        return $this->db->select("
          select u.*, g.rang
          from users u
          join users_group g on g.id=u.users_group_id
          where u.email = '{$email}'
          limit 1")->row();
    }
    /**
     * @param $phone
     * @return array|mixed
     */
    public function getInfoByPhone($phone)
    {
        return $this->db->select("
          select u.*, g.rang
          from users u
          join users_group g on g.id=u.users_group_id
          where u.phone = '{$phone}'
          limit 1")->row();
    }

    /**
     * @param $email
     * @param $phone
     * @return array|mixed
     */
    public function getInfoByPhoneOrEmail($phone, $email)
    {
        return $this->db->select("
          select u.*, g.rang
          from users u
          join users_group g on g.id=u.users_group_id
          where u.phone = '{$phone}' or u.email = '{$email}'
          limit 1")->row();
    }
    /**
     * @param null $key
     * @return mixed
     */
    public function getSessionData($key = null)
    {
        if($key){
            if(isset($_SESSION['app']['user'][$key])) return $_SESSION['app']['user'][$key];
        }

        return isset($_SESSION['app']['user']) ? $_SESSION['app']['user'] : null;
    }

    /**
     * Check the password against a hash generated by the generate_hash
     * @param $password
     * @param $hash
     * @return bool
     */
    public function checkPassword($password, $hash)
    {
        return crypt($password, $hash)==$hash;
    }

    /*
 * Generate a secure hash for a given password. The cost is passed
 * to the blowfish algorithm. Check the PHP manual page for crypt to
 * find more information about this setting.
 */
    public function encodePassword($password, $cost=11){
        /* To generate the salt, first generate enough random bytes. Because
         * base64 returns one character for each 6 bits, the we should generate
         * at least 22*6/8=16.5 bytes, so we generate 17. Then we get the first
         * 22 base64 characters
         */
        $salt=substr(base64_encode(openssl_random_pseudo_bytes(17)),0,22);
        /* As blowfish takes a salt with the alphabet ./A-Za-z0-9 we have to
         * replace any '+' in the base64 string with '.'. We don't have to do
         * anything about the '=', as this only occurs when the b64 string is
         * padded, which is always after the first 22 characters.
         */
        $salt=str_replace("+",".",$salt);
        /* Next, create a string that will be passed to crypt, containing all
         * of the settings, separated by dollar signs
         */
        $param='$'.implode('$',array(
                "2y", //select the most secure version of blowfish (>=PHP 5.3.7)
                str_pad($cost,2,"0",STR_PAD_LEFT), //add the cost in two digits
                $salt //add the salt
            ));

        //now do the actual hashing
        return crypt($password, $param);
    }


    /**
     * generate random password
     * @param int $number
     * @return string
     */
    public function generatePassword($number=6) {
        $arr = array(
            'A','B','C','D','E','F',
            'G','H','I','J','K','L',
            'M','N','O','P','R','S',
            'T','U','V','X','Y','Z',
            '1','2','3','4','5','6',
            '7','8','9','0'
        );

        $pass = "";
        for($i = 0; $i < $number; $i++)
        {
            $index = rand(0, count($arr) - 1);
            $pass .= $arr[$index];
        }
        return $pass;
    }

    /**
     * @param $id
     * @param $skey
     * @return bool
     */
    public function setSkey($id, $skey)
    {
        return $this->db->update('users', array('skey'=> $skey), "id={$id} limit 1");
    }

    /**
     * @param $id
     * @param $password
     * @return bool
     */
    public function setPassword($id, $password)
    {
        return $this->db->update('users', array('password'=> $this->encodePassword($password)), "id={$id} limit 1");
    }

    /**
     * @param $id
     * @param $email
     * @return array|mixed
     */
    public function isUniqueEmail($id, $email)
    {
        $id = $this->db->select("select id from users where email = '{$email}' and id <> {$id} limit 1")->row('id') ;
        return empty($id);
    }
    /**
     * @param $id
     * @param $phone
     * @return array|mixed
     */
    public function isUniquePhone($id, $phone)
    {
        $id = $this->db->select("select id from users where phone = '{$phone}' and id <> {$id} limit 1")->row('id') ;
        return empty($id);
    }

    /**
     * @param $social_id
     * @param $provider
     * @param $email
     * @return array|mixed
     */
    public function getInfoByProvider($social_id, $provider, $email)
    {
        return $this->db->select("  select u.*, ug.rang
                                    from users u
                                    join users_group ug on ug.id=u.users_group_id
                                    where u.social_id='{$social_id}' and u.provider='{$provider}' and email = '{$email}'
                                  limit 1")->row();
    }

    /**
     * @param $provider
     * @param $social_id
     * @return array|mixed
     */
    public function getUsersIDByProvider($provider, $social_id)
    {
        return $this->db
            ->select("
              select users_id from users_oauth where provider = '{$provider}' and social_id = '{$social_id}' limit 1
            ")
            ->row('users_id');
    }

    /**
     * @param $users_id
     * @return array
     */
    public function getOauthInfo($users_id)
    {
        $r = $this->db->select("select provider, social_id from users_oauth where users_id = '{$users_id}' and social_id <> ''")->all();
        $res = array();
        foreach ($r as $row) {
            $res[$row['provider']] = $row['social_id'];
        }

        return $res;
    }

    /**
     * @param $users_id
     * @return int
     */
    public function saveOauthInfo($users_id)
    {
        if(!isset($_POST['oauth'])) return 0;

        foreach ($_POST['oauth'] as $provider => $social_id) {
            $this->addUsersProvider($users_id, $provider, $social_id, true);
        }
        return 1;
    }

    /**
     * @param $id
     * @param $users_group_id
     * @return bool
     */
    public function selectGroup($id, $users_group_id)
    {
        return $this->db->update('users', array('users_group_id' => $users_group_id), " id = {$id} limit 1");
    }

    public function validateLike($uid,$cid)
    {
        return $this->db->select("
                Select count(id) as c from users_like
                where users_id={$uid} and client_id={$cid}
                limit 1
        ")->row("c");
    }

    public function addLike($uid,$cid)
    {
        $id = $this->db->insert("users_like",
            array(
                'users_id'=>$uid,
                'client_id'=>$cid,
            )
            );

        if ($id>0) {
            $id = $this->getTotalLike($uid);
        }

        return $id;
    }

    public function getTotalLike($uid)
    {
      return  $this->db->select("
                Select count(id) as c from users_like
                where users_id={$uid}
        ")->row("c");
    }

    public function getPrice($id){
        return $this->db->select("
                Select if(price_per_hour>0 or price_per_day>0,1,0) as status from users
                where id={$id} limit 1
        ")->row("status");
    }

    public function getDiscount($id){
        return $this->db->select("
                Select if(discount_per_week>0 or discount_per_month>0 or discount_per_2_month>0,1,0) as status from users
                where id={$id} limit 1
        ")->row("status");
    }

    public function getPortfolio($id)
    {
        return $this->db->select("
                Select count(id) as s from users_uploads
                where users_id = {$id}
        ")->row("s");
    }

    public function getConfirmOrders($id)
    {
        return $this->db->select("
                SELECT count(id) as s FROM  users_notifications
                where workers_id={$id} and section='orders' and message='CONFIRMED_ORDERS'
        ")->row("s");
    }

    public function getQuantityInfo($id)
    {
        return $this->db->select("Select * from img_quantity where users_id={$id}
            and date > DATE_SUB(CURDATE(), INTERVAL 1 WEEK) limit 1")->row();
    }

    public function insertImgQuantity($uid)
    {
        return $this->db->insert("img_quantity",array(
            'users_id'=>$uid,
            'quantity'=>1,
            'date'=>date("Y-m-d H:i:s")
        ));
    }

    public function checkingUrl($url, $id = null)
    {
        $where = $id ? " and id <> '$id' " : "";

        $is_url_users = $this->db->select("Select * from users where url='{$url}' {$where}")->row('url');
        $is_url_pages = $this->db->select("Select * from content_info where alias='{$url}'")->row('alias');

        if ($is_url_users || $is_url_pages){
            return false;
        } else {
            return true;
        }
    }

    public function getInfoBySkey($skey)
    {
        return $this->db->select("select id,confirm from users where skey='{$skey}' limit 1")->row();
    }

}