<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 13.10.14 21:44
 */

namespace models\modules;

use models\App;

defined("SYSPATH") or die();

/**
 * Class Cart
 * @package models\modules
 */
class Cart extends App{

    private $debug = 0;

    /**
     * @param $status
     * @return $this
     */
    public function debug($status=1)
    {
        $this->debug = $status;
        return $this;
    }

    /**
     * @param $id
     * @return array|mixed
     */
    public function getInfo($id)
    {
        return $this->db->select("
            select po.*, cr.symbol, i.title, i.name, i.description
            from products_options po
            join currency cr on cr.is_main =1
            join content_info i on i.content_id = po.products_id and i.languages_id = {$this->languages_id}
            where po.id = '{$id}'
            limit 1
            ", $this->debug)->row();
    }
    /**
     * @return array|mixed
     */
    public function getProducts()
    {
        return $this->db->select("
            select c.id, i.title, i.name, i.description, po.availability,po.price
            from products_options po
            join content c on c.id = po.products_id and c.auto=0 and c.published = 1
            join content_info i on i.content_id = c.id and i.languages_id = {$this->languages_id}
            where po.is_variant=0
            ", $this->debug)->all();
    }
} 