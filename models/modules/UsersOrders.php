<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 13.10.14 21:44
 */

namespace models\modules;

use controllers\modules\UsersNotifications as UN;
use models\App;

defined("SYSPATH") or die();

/**
 * Class Users
 * @package models\modules
 */
class UsersOrders extends Users {
    /**
     * @var string тип користувувача worker| user
     */
    private $user_type = '';

    public function setUserType($type)
    {
        $this->user_type = $type;

        return $this;
    }

    /**
     * @param $customer_id
     * @return int
     */
    public function getOrdersTotal($customer_id)
    {
        if($this->user_type == 'worker'){
            $where = "o.workers_id='{$customer_id}'";
        }elseif($this->user_type == 'user'){
            $where = "o.users_id='{$customer_id}'";
        }else{
            return 0;
        }
        return $this->db->select("
            select COUNT(o.id) as t
            from orders o
            where {$where}
            {$this->where}
        ")->row('t');
    }
    /**
     * @param $customer_id
     * @return array|mixed
     */
    public function getOrders($customer_id=0)
    {
        if($this->user_type == 'worker'){
            $where = "o.workers_id='{$customer_id}'";
        }elseif($this->user_type == 'user'){
            $where = "o.users_id='{$customer_id}'";
        }else{
            return array();
        }
        return $this->db->select("
            select o.*, DATE_FORMAT(o.createdon, '%d.%m.%Y') as dd,
            DATE_FORMAT(o.createdon, '%H:%i') as hh,
            odate as eDate,
            DATE_FORMAT(o.odate, '%d.%m.%Y') as odate,
            CONCAT_WS( ' ',u.name, u.surname) as username, u.phone,u.email, u.avatar, u.city_id, u.id as users_id,
            '{$this->user_type}' as type,
             ci.name as city,
             ugi.name as group_name, pwg.content_id
            from orders o
            join users u on u.id = ". (($this->user_type == 'worker') ? 'o.users_id' : 'o.workers_id') ."
            join users_group_info ugi on ugi.users_group_id=u.users_group_id and ugi.languages_id = {$this->languages_id}
            left join pages_workers_group pwg on pwg.users_group_id=u.users_group_id
            left join guides_info ci on ci.guides_id = u.city_id and ci.languages_id = {$this->languages_id}
            where {$where}
             {$this->where}
            order by o.id desc
            {$this->limit}
        ", $this->debug)->all();
    }

    public function getOrdersItem($id)
    {
        return $this->db->select("
            select o.*, DATE_FORMAT(o.createdon, '%d.%m.%Y') as dd,
            DATE_FORMAT(o.createdon, '%H:%i') as hh,
            DATE_FORMAT(o.odate, '%d.%m.%Y') as odate,
            CONCAT(u.name, ' ', u.surname) as username, u.phone,u.email, u.avatar, u.city_id,
            '{$this->user_type}' as type,
             ci.name as city,
             ugi.name as group_name, pwg.content_id
            from orders o
            join users u on u.id = ". (($this->user_type == 'worker') ? 'o.users_id' : 'o.workers_id') ."
            join users_group_info ugi on ugi.users_group_id=u.users_group_id and ugi.languages_id = {$this->languages_id}
            left join pages_workers_group pwg on pwg.users_group_id=u.users_group_id
            left join guides_info ci on ci.guides_id = u.city_id and ci.languages_id = {$this->languages_id}
            where o.id={$id}
        ")->row();
    }

    public function changeStatus($id, $status, $users_id = null, $workers_id = null)
    {
        $and = '';
        if($users_id){
            $and = "and users_id = {$users_id}";
        }
        if($workers_id){
            $and = "and workers_id = {$workers_id}";
        }

        $s = $this->db->update("orders", array('status' => $status), " id = {$id} {$and} limit 1");

        if($status == 'canceled' && $s > 0){
            $o = $this->db->select("select * from orders where id = {$id} limit 1")->row();
            $notify_id = UsersNotifications::instance()->cancelOrders($o['workers_id'], $o['users_id'], $o['id']);
            UN::sendNotificationMessage($notify_id);
        } elseif($status == 'dismiss' && $s > 0){
            $o = $this->db->select("select * from orders where id = {$id} limit 1")->row();
            $notify_id = UsersNotifications::instance()->dismissOrders($o['workers_id'], $o['users_id'], $o['id']);
            UN::sendNotificationMessage($notify_id);
        } elseif($status == 'confirmed' && $s > 0){
            $o = $this->db->select("select * from orders where id = {$id} limit 1")->row();
            $notify_id = UsersNotifications::instance()->confirmedOrders($o['workers_id'], $o['users_id'], $o['id']);
            UN::sendNotificationMessage($notify_id);
        }
        return $s;
    }

    /**
     * @param $users_id
     * @param $date
     * @param $comment
     * @return bool|string
     */
    public function setOdate($users_id, $date, $comment)
    {
        return $this->db->insert
        (
            'users_odates',
            array(
                'users_id' => $users_id,
                'odate' => $date,
                'comment' => $comment
            ),
            0,
            1
        );
    }

    /**
     * @param $users_id
     * @param $date
     * @return array|mixed
     */
    public function getOdateID($users_id, $date)
    {
        return $this->db
            ->select("select id from users_odates where users_id = {$users_id} and odate = '{$date}' limit 1")
            ->row('id');
    }

    /**
     * @param $id
     * @return int
     */
    public function removeODate($id)
    {
        return $this->db->delete('users_odates', " id={$id} limit 1");
    }

    /**
     * @param $users_id
     * @param $df
     * @param $dt
     * @return array|mixed
     */
    public function getOdates($users_id, $df, $dt)
    {
        return $this->db->select("select odate,comment from users_odates where users_id = {$users_id} and odate BETWEEN '{$df}' and '{$dt}'")->all();
    }
 }