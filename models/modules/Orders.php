<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 13.10.14 21:44
 */

namespace models\modules;

use controllers\modules\UsersNotifications as UN;
use models\App;

defined("SYSPATH") or die();

/**
 * Class Orders
 * @package models\modules
 */
class Orders extends App{

    private $debug = 0;

    private $where = array();

    /**
     * @param $status
     * @return $this
     */
    public function debug($status=1)
    {
        $this->debug = $status;
        return $this;
    }

    /**
     * @param $data
     * @return bool|string
     */
    public function create($data)
    {
        $s =  $this->db->insert('orders', $data);
        if($s>0){
            // notification
            $notify_id = UsersNotifications::instance()->newOrders($data['workers_id'], $data['users_id'], $s);
            UN::sendNotificationMessage($notify_id);
        }
        return $s;
    }

    /**
     * @param $id
     * @return array|mixed
     */
    public function getUserInfo($id)
    {
        return $this->db->select("
            select u.*, cu.symbol
            from users u
            join currency cu on cu.id=u.currency_id
            where u.id = {$id} limit 1
          ")->row();
    }

    /**
     * @param $workers_id
     * @param $oDate
     * @return bool
     */
    public function isOrdered($workers_id, $oDate)
    {
        return $this->db
            ->select("select id from orders where odate = '{$oDate}' and workers_id = {$workers_id} limit 1")
            ->row('id')>0;
    }

    /**
     * @param $workers_id
     * @return array|mixed
     */
    public function getOrderedRange($workers_id)
    {
        $now = date('Y-m-d');
        return $this->db
            ->select("
                select odate from orders where workers_id={$workers_id} and odate >= '{$now}'
                union
                select odate from users_odates where users_id = {$workers_id} and odate >= '{$now}'
              ")
            ->all();
    }

    public function createPay($data)
    {
        $this->db->delete("ordersAccount"," users_id={$data['users_id']} and pay=0");
        $data['createdon'] = date('Y-m-d H:i:s');
        $oid =  $this->db->insert("ordersAccount",$data);

        $code = date("ymd-{$oid}");
        $this->db->update('ordersAccount', array('code' => $code), " id= {$oid} limit 1");

        return $oid;
    }


    public function getOrderInfo($id)
    {
        return $this->db->select("
          Select o.*,p.module from ordersAccount o
          join payment p on o.payment_id=p.id
          where o.id='{$id}' limit 1
          ")->row();
    }

    public function isPay($id)
    {
        $w = $this->getWhere();
        return $this->db->select("
                Select id from ordersAccount
                WHERE users_id={$id} and pay=1 and status_id=1
                and type not in ('oneC','twoC') {$w}
                limit 1
                ")->row("id");
    }

    public function confirmPayment($id)
    {
        $s = $this->db->update("ordersAccount",array(
            'pay'=>1,
            'status_id'=>1,
            'pay_date'=>date('Y-m-d H:i:s'),
//            'end_date'=>$endDate->format('Y-m-d H:i:s'),
        )," code='{$id}'");

        if($s) {
           $info = $this->db->select("
                        select * from ordersAccount
                        where code = '{$id}' limit 1
                        ")->row();

            switch($info['type']) {
                case 'week':
                    $ts = strtotime('+14 days');
                    $endDate = date('Y-m-d H:i:s',$ts);
                    $s = $this->db->update("users",
                        array(
                            'hot'=>1,
                            'hot_date'=>$endDate
                        )," id={$info['users_id']}"
                    );
                    break;
                case 'month':
                    $ts = strtotime('+30 days');
                    $endDate = date('Y-m-d H:i:s',$ts);
                    $s = $this->db->update("users",
                        array(
                            'hot'=>1,
                            'hot_date'=>$endDate
                        )," id={$info['users_id']}"
                        );
                    break;
                case 'year':
                    $ts = strtotime('+1 year');
                    $endDate = date('Y-m-d H:i:s',$ts);
                    $s = $this->db->update("users",
                        array(
                            'gold'=>1,
                            'gold_date'=>$endDate
                        )," id={$info['users_id']}"
                    );
                    break;
                default :
//                    $s = $this->db->update("users",
//                        array(
//                            'hot'=>1,
//                            'hot_date'=>$info['pay_date']
//                        )," id={$info['users_id']}"
//                    );
                    $ts = strtotime('+1 year');
                    $endDate = date('Y-m-d H:i:s',$ts);
                    break;
            }
            $s = $this->db->update("ordersAccount",array(
                'end_date'=>$endDate,
            )," code='{$id}'");
        }

        return $s;
    }

    public function getPayCities($id)
    {
        return $this->db->select("select count(city_index) as c from ordersAccount
              where users_id={$id} and pay=1 and type in ('oneC','twoC')")->row("c");
    }

    public function getTotalCity($id)
    {
        return $this->db->select("Select count(id) as t from users_cities 
                  where users_id={$id}")->row("t");
    }

    /**
     * @param $q
     * @return $this
     */
    public function setWhere($q)
    {
        $this->where[] = $q;

        return $this;
    }

    /**
     * @return string
     */
    private function getWhere()
    {
        $w = '';
        if(!empty($this->where)){
            $w = implode(' ', $this->where);
        }
        return $w;
    }
} 