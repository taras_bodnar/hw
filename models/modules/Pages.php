<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 25.11.14 14:39
 */

namespace models\modules;

use models\App;

defined("SYSPATH") or die();

/**
 * Class Pages
 * @package models\modules
 */
class Pages extends App {

    private $join = array();
    private $where = array();
    private $limit = '';

    /**
     * @param $id
     * @param string $key
     * @return array|mixed
     */
    public function getInfo($id, $key='*')
    {
        return $this->db->select("
           select $key
           from content_info
           where content_id={$id} and languages_id={$this->languages_id}
           limit 1")->row($key);
    }

    /**
     * get results
     * @return array|mixed
     */
    public function _getPosts()
    {
        $w = $this->getWhere();
        $join = $this->getJoin();

        return $this->db->select("
            select c.id, i.title, i.name, i.description,
            DATE_FORMAT(c.createdon, '%d') as dd,
            DATE_FORMAT(c.createdon, '%m') as mm,
            DATE_FORMAT(c.createdon, '%Y') as yy
            from content c
            join content_type ct on ct.type='{$this->type_post}' and ct.id=c.type_id
            {$join}
            join content_info i on i.content_id = c.id and i.languages_id = {$this->languages_id}
            where c.published = 1 and c.auto=0 {$w}
            order by c.createdon desc
            {$this->limit}
            ")->all();
    }

    /**
     * @return array|mixed
     */
    public function getItems($parent_id)
    {
        $w = $this->getWhere();
        return $this->db->select("
            select c.id, i.title, i.name,c.isfolder, i.description,c.travel_date,c.travel_price,c.travel_old_price
            from content c
            join content_info i on i.content_id = c.id and i.languages_id = {$this->languages_id}
            where c.parent_id={$parent_id} and c.published = 1 and c.auto=0 {$w}
            order by abs(c.sort) asc, c.id asc
            ")->all();
    }



    public function getTotal($id)
    {
        return $this->db->select("
              Select count(c.id) as c from content c
                join content_info ci on ci.content_id=c.id and ci.languages_id=1
                where parent_id={$id} and auto=0 and published=1
        ")->row("c");
    }

    public function getTravelbyDate($date)
    {
        $w = $this->getWhere();
        return $this->db->select("
            select c.id, i.title, i.name,c.isfolder, i.description,c.travel_date,c.travel_price,
            c.travel_old_price,td.start_date,end_date,c.parent_id
            from content c
            join content_info i on i.content_id = c.id and i.languages_id = {$this->languages_id}
            left join travel_date td on c.id=td.content_id
            where c.published = 1 and c.auto=0 and DATE(start_date) BETWEEN '{$date} + INTERVAL 3 DAY ' AND end_date
            and start_date!='$date'
              {$w}
            order by abs(td.start_date) asc, c.id asc
            ")->all();
    }

    /**
     * @param $q
     * @return $this
     */
    public function setWhere($q)
    {
        $this->where[] = $q;

        return $this;
    }

    /**
     * @return string
     */
    private function getWhere()
    {
        $w = '';
        if(!empty($this->where)){
            $w = implode(' ', $this->where);
        }
        return $w;
    }
    /**
     * @param $q
     * @return $this
     */
    public function setJoin($join)
    {
        $this->join[] = $join;

        return $this;
    }

    /**
     * @return string
     */
    private function getJoin()
    {
        $o = '';
        if(!empty($this->join)){
            $o = implode(' ', $this->join);
        }
        return $o;
    }

    /**
     * @param $start
     * @param $num
     * @return $this
     */
    public function setLimit($start, $num)
    {
        $this->limit = "limit {$start}, {$num}";

        return $this;
    }

} 