<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 25.11.14 14:39
 */

namespace models\modules;

use models\App;

defined("SYSPATH") or die();

/**
 * Class Notify
 * @package models\modules
 */
class Notify extends App {

    /**
     * @param $id
     * @return array|mixed
     */
    public function getOrdersInfo($id)
    {
        $res = $this->db->select("
          select o.*, di.name as delivery_name, pi.name as payment_name
          from orders o
          left join delivery_info di on di.delivery_id = o.delivery_id and di.languages_id='{$this->languages_id}'
          left join payment_info pi on pi.payment_id = o.payment_id and pi.languages_id='{$this->languages_id}'
          where o.id = {$id} limit 1
        ")->row();
        $res['products'] = $this->db->select("select * from orders_products where orders_id = {$id}")->all();

        $total = 0;
        foreach($res['products'] as $item){
            $total += $item['price'] * $item['quantity'];
        }
        $res['total'] = round($total, 2);
        return  $res;
    }

    public function orderAccountInfo($id)
    {
        return $this->db->select("
                    select u.id,u.name,u.surname,o.type,o.price,o.pay_date,u.email from ordersAccount o
                    join users u on u.id=o.users_id
                    where o.code = '{$id}' limit 1
        ")->row();
    }
} 