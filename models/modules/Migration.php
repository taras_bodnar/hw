<?php
/**
 * Created by PhpStorm.
 * User: taras
 * Date: 17.09.15
 * Time: 10:05
 */

namespace models\modules;


use models\App;

class Migration extends App
{

    public function getCities()
    {
        return $this->db->select("
               SELECT id, city_id
                FROM users
                WHERE city_id <> ''
        ")->all();
    }

    public function insertCities($uid,$gid)
    {
        $this->db->delete("users_cities"," users_id=$uid");
        return $this->db->insert("users_cities",
                array(
                    'users_id'=>$uid,
                    'guides_id'=>$gid,
                    'main'=>1
                )
            );
    }
}