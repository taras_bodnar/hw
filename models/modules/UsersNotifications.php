<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 13.10.14 21:44
 */

namespace models\modules;

use models\App;

defined("SYSPATH") or die();

/**
 * Class Users
 * @package models\modules
 */
class UsersNotifications extends Users {
    /**
     * @var string тип користувувача worker| user
     */
    private $user_type = '';
    private static $instance;

    public static function instance()
    {
        if(! self::$instance){
            self::$instance = new self;
        }

        return self::$instance;
    }



    public function setUserType($type)
    {
        $this->user_type = $type;

        return $this;
    }

    /**
     * @param $customer_id
     * @return int
     */
    public function getNotificationTotal($customer_id, $w = '')
    {
        if($this->user_type == 'worker'){
            $where = "workers_id='{$customer_id}' and notify = 'worker' ";
        }elseif($this->user_type == 'user'){
            $where = "users_id='{$customer_id}' and notify = 'user' ";
        }else {
            return 0;
        }
        if(!empty($w)){
            $where .= $w;
        }
        return $this->db->select("
            select COUNT(id) as t
            from users_notifications
            where {$where}
        ")->row('t');
    }
    /**
     * @param $customer_id
     * @return array|mixed
     */
    public function getNotifications($customer_id=0)
    {
        if($this->user_type == 'worker'){
            $where = "n.workers_id='{$customer_id}'";
        }elseif($this->user_type == 'user'){
            $where = "n.users_id='{$customer_id}'";
        }else{
            return array();
        }
        return $this->db->select("
            select n.*,
            DATE_FORMAT(n.createdon, '%d.%m.%Y') as dd,
            DATE_FORMAT(n.createdon, '%H:%i') as hh,
            CONCAT(u.name, ' ', u.surname) as username, u.phone,u.email, u.id as users_id
            from users_notifications n
            join users u on u.id = ". (($this->user_type == 'worker') ? 'n.users_id' : 'n.workers_id') ."
            where {$where}
             {$this->where}
            order by n.id desc
            {$this->limit}
        ", $this->debug)->all();
    }

    public function maskAsRead($id)
    {
        return $this->db->update('users_notifications', array('is_read' => 1), " id = {$id} limit 1");
    }

    /**
     * @param $item_id
     * @param $section
     * @param $notify
     * @return bool
     */
    public function setIsRead($item_id, $section, $notify = '')
    {
        $and = $notify == '' ? '' : " and notify='{$notify}' ";
        return $this->db->update(
            'users_notifications',
            array('is_read' => 1),
            " item_id = {$item_id} and section = '{$section}' {$and} limit 1"
        );
    }


    /**
     * @param $workers_id
     * @param $users_id
     * @param $message
     * @param $more
     * @param $section
     * @param $item_id
     * @param string $notify
     * @return bool|string
     */
    public function add($workers_id, $users_id, $message, $more, $section, $item_id, $notify = 'all')
    {
        return $this->db->insert(
            "users_notifications",
            array(
                'workers_id' => $workers_id,
                'users_id'   => $users_id,
                'message'    => $message,
                'more'       => $more,
                'notify'     => $notify,
                'section'    => $section,
                'item_id'    => $item_id
            )
        );
    }

    /**
     * @param $workers_id
     * @param $users_id
     * @param $comments_id
     * @return bool|string
     */
    public function newComment($workers_id, $users_id, $comments_id)
    {
        return $this->add($workers_id, $users_id, 'NEW_COMMENT', '12;#comment-' . $comments_id, 'comments', $comments_id);
    }

    /**
     * @param $workers_id
     * @param $users_id
     * @param $orders_id
     * @return bool|string
     */
    public function newOrders($workers_id, $users_id, $orders_id)
    {
        return $this->add($workers_id, $users_id, 'NEW_ORDERS', '10;#o-item-' . $orders_id, 'orders', $orders_id, 'worker');
    }

    /**
     * @param $workers_id
     * @param $users_id
     * @param $orders_id
     * @return bool|string
     */
    public function cancelOrders($workers_id, $users_id, $orders_id)
    {
        return $this->add($workers_id, $users_id, 'CANCEL_ORDERS', '10;#o-item-' . $orders_id, 'orders', $orders_id, 'worker');
    }
    /**
     * @param $workers_id
     * @param $users_id
     * @param $orders_id
     * @return bool|string
     */
    public function dismissOrders($workers_id, $users_id, $orders_id)
    {
        return $this->add($workers_id, $users_id, 'DISMISS_ORDERS', '10;#o-item-' . $orders_id, 'orders', $orders_id, 'user');
    }
    /**
     * @param $workers_id
     * @param $users_id
     * @param $orders_id
     * @return bool|string
     */
    public function confirmedOrders($workers_id, $users_id, $orders_id)
    {
        return $this->add($workers_id, $users_id, 'CONFIRMED_ORDERS', '10;#o-item-' . $orders_id, 'orders', $orders_id, 'user');
    }

    /**
     * @param $workers_id
     * @param $users_id
     * @param $message
     * @param $notify
     * @return bool|string
     */
    public function newMessage($workers_id, $users_id, $message, $messages_id, $notify)
    {
        return $this->add($workers_id, $users_id, 'NEW_MESSAGE ' . $message, '50;p=' . $workers_id,  'messages', $messages_id, $notify);
    }

    /**
     * @param $id
     * @return array|mixed
     */
    public function getFullInfo($id)
    {
        return $this->db->select("
            select n.*, u.name,u.surname, u.email, CONCAT(s.name, ' ', s.surname) as sender_name
            from users_notifications n
            join users u on u.id = IF(n.notify ='user', n.users_id, n.workers_id)
            join users s on s.id = IF(n.notify ='user', n.workers_id, n.users_id)
            where n.id = {$id} limit 1
        ")->row();
    }
}