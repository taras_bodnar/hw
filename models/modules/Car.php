<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 13.10.14 21:44
 */

namespace models\modules;

use models\App;

defined("SYSPATH") or die();

/**
 * Class Car
 * @package models\modules
 */
class Car extends App{

    private $debug = 0;

    /**
     * @param $status
     * @return $this
     */
    public function debug($status=1)
    {
        $this->debug = $status;
        return $this;
    }

    /**
     * @return array|mixed
     */
    public function getVendors()
    {
        return $this->db->select("select * from car_vendors order by vendor asc")->all();
    }

    /**
     * @param $vendor_id
     * @return array|mixed
     */
    public function getModels($vendor_id)
    {
        return $this->db->select("select * from car_models where vendors_id = '{$vendor_id}' order by model asc ")->all();
    }

    /**
     * @param $model_id
     * @return array|mixed
     */
    public function getYears($model_id)
    {
        return $this->db->select("select * from car_years where models_id = '{$model_id}' order by year asc ")->all();
    }

    /**
     * @param $year_id
     * @return array|mixed
     */
    public function getModification($year_id)
    {
        return $this->db->select("select * from car_modification where years_id = '{$year_id}' order by modification asc ")->all();
    }
} 