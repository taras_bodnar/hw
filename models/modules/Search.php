<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 25.11.14 14:39
 */

namespace models\modules;

use models\App;

defined("SYSPATH") or die();

class Search extends App {

    private $where = array();
    private $limit = '';

    /**
     * @param $id
     * @return array|mixed
     */
    public function getAliasById($id)
    {
        return $this->db->select("select alias
                                  from content_info
                                  where content_id = '{$id}' and languages_id = {$this->languages_id} limit 1
                                  ")->row('alias');
    }

    /**
     * get results
     * @return array|mixed
     */
    public function get()
    {
        $w = $this->getWhere();

        return $this->db->select("
            select c.id, i.title, i.name, i.description
            from content c
            join content_info i on i.content_id = c.id and i.languages_id = {$this->languages_id}
            where c.published = 1 and c.auto=0 {$w}
            order by c.createdon desc
            {$this->limit}
            ")->all();
    }

    /**
     * total results
     * @return array|mixed
     */
    public function total()
    {
        $w = $this->getWhere();
        return $this->db->select("
             select count(c.id) as t
            from content c
            join content_info i on i.content_id = c.id and i.languages_id = {$this->languages_id}
            where c.published = 1 and c.auto=0 {$w}
        ")->row('t');
    }

    /**
     * @param $q
     * @return $this
     */
    public function setWhere($q)
    {
        $this->where[] = $q;

        return $this;
    }

    /**
     * @param $start
     * @param $num
     * @return $this
     */
    public function setLimit($start, $num)
    {
        $this->limit = "limit {$start}, {$num}";

        return $this;
    }

    /**
     * @return string
     */
    private function getWhere()
    {
        $w = '';
        if(!empty($this->where)){
            $w = implode(' ', $this->where);
        }
        return $w;
    }

} 