<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 25.11.14 14:39
 */

namespace models\modules;

use models\App;

defined("SYSPATH") or die();

/**
 * Class Blog
 * @package models\modules
 */
class Blog extends App {

    private $join = array();
    private $where = array();
    private $limit = '';
    public $type_post= 'post';
    private $type_category= 'PostsCategories';

    public function clearLimit()
    {
        $this->limit='';
        return $this;
    }

    /**
     * get results
     * @return array|mixed
     */
    public function getPosts()
    {
        $w = $this->getWhere();
        $join = $this->getJoin();

        return $this->db->select("
            select c.id, i.title, i.name, i.description,
            DATE_FORMAT(c.createdon, '%d') as dd,
            DATE_FORMAT(c.createdon, '%m') as mm,
            DATE_FORMAT(c.createdon, '%Y') as yy,
            DATE_FORMAT(c.createdon, '%h') as hh,
            DATE_FORMAT(c.createdon, '%i') as ii,
            DATE_FORMAT(c.createdon, '%s') as ss
            from content c
            join content_type ct on ct.type='{$this->type_post}' and ct.id=c.type_id
            {$join}
            join content_info i on i.content_id = c.id and i.languages_id = {$this->languages_id}
            where c.published = 1 and c.auto=0 {$w}
            order by c.createdon desc
            {$this->limit}
            ")->all();
    }

    /**
     * total results
     * @return array|mixed
     */
    public function getTotal()
    {
        $w = $this->getWhere();
        $join = $this->getJoin();

        return $this->db->select("
            select count(c.id) as t
            from content c
            join content_type ct on ct.type='{$this->type_post}' and ct.id=c.type_id
            {$join}
            join content_info i on i.content_id = c.id and i.languages_id = {$this->languages_id}
            where c.published = 1 and c.auto=0 {$w}
        ")->row('t');
    }

    /**
     * @return array|mixed
     */
    public function getCategories()
    {
        return $this->db->select("
            select c.id, i.title, i.name, i.description
            from content c
            join content_type ct on ct.type='{$this->type_category}' and ct.id=c.type_id
            join content_info i on i.content_id = c.id and i.languages_id = {$this->languages_id}
            where c.published = 1 and c.auto=0
            order by abs(c.sort) asc, c.id asc
            {$this->limit}
            ")->all();
    }

    public function getTags($content_id)
    {
        return $this->db->select("
          select t.alias, t.name
          from tags t
          join tags_content tc on tc.content_id={$content_id} and tc.tags_id=t.id
          where t.languages_id='{$this->languages_id}'
        ")->all();
    }

    public function getRelatedPosts($products_id)
    {
        return $this->db->select("
            select c.id, i.title, i.name
            from posts_related pr
            join content c on c.id = pr.related_id and c.auto=0 and c.published = 1
            join content_info i on i.content_id = c.id and i.languages_id = {$this->languages_id}
            where pr.products_id={$products_id}
            order by abs(pr.sort) asc, i.name asc
            ")->all();
    }

    /**
     * @param $q
     * @return $this
     */
    public function setWhere($q)
    {
        $this->where[] = $q;

        return $this;
    }

    /**
     * @return string
     */
    private function getWhere()
    {
        $w = '';
        if(!empty($this->where)){
            $w = implode(' ', $this->where);
        }
        return $w;
    }
    /**
     * @param $q
     * @return $this
     */
    public function setJoin($join)
    {
        $this->join[] = $join;

        return $this;
    }

    /**
     * @return string
     */
    private function getJoin()
    {
        $o = '';
        if(!empty($this->join)){
            $o = implode(' ', $this->join);
        }
        return $o;
    }

    /**
     * @param $start
     * @param $num
     * @return $this
     */
    public function setLimit($start, $num)
    {
        $this->limit = "limit {$start}, {$num}";

        return $this;
    }

} 