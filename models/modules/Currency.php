<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 25.11.14 14:39
 */

namespace models\modules;

use models\App;

defined("SYSPATH") or die();

/**
 * Class Currency
 * @package models\modules
 */
class Currency extends App {
    /**
     * @return array|mixed
     */
    public final function get()
    {
        return $this->db->select("select * from currency")->all();
    }
} 