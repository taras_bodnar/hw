<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 25.11.14 14:39
 */

namespace models\modules;

use models\App;

defined("SYSPATH") or die();

/**
 * Class Blog
 * @package models\modules
 */
class News extends App {

    private $join = array();
    private $where = array();
    private $limit = '';

    public function clearLimit()
    {
        $this->limit='';
        return $this;
    }

    /**
     * @param $parent_id
     * @return array|mixed
     */
    public function get($parent_id)
    {
        $join = $this->getJoin();

        return $this->db->select("
            select c.id, i.title, i.name, i.description,
            DATE_FORMAT(c.createdon, '%d') as dd,
            DATE_FORMAT(c.createdon, '%m') as mm,
            DATE_FORMAT(c.createdon, '%Y') as yy,
            DATE_FORMAT(c.createdon, '%h') as hh,
            DATE_FORMAT(c.createdon, '%i') as ii,
            DATE_FORMAT(c.createdon, '%s') as ss
            from content c
            join content_type ct on ct.type='page' and ct.id=c.type_id
            {$join}
            join content_info i on i.content_id = c.id and i.languages_id = {$this->languages_id}
            where c.parent_id={$parent_id} and c.published = 1 and c.auto=0
            order by c.createdon desc
            {$this->limit}
            ")->all();
    }

    public function getNews()
    {
        $join = $this->getJoin();

        return $this->db->select("
            select c.id, i.title, i.name, i.description,
            DATE_FORMAT(c.createdon, '%d') as dd,
            DATE_FORMAT(c.createdon, '%m') as mm,
            DATE_FORMAT(c.createdon, '%Y') as yy,
            DATE_FORMAT(c.createdon, '%h') as hh,
            DATE_FORMAT(c.createdon, '%i') as ii,
            DATE_FORMAT(c.createdon, '%s') as ss,
            pw.content_id,pw.users_group_id
            from content c
            join content_type ct on ct.type='news' and ct.id=c.type_id
            {$join}
            join content_info i on i.content_id = c.id and i.languages_id = {$this->languages_id}
            where c.published = 1 and c.auto=0
            order by c.createdon desc
            {$this->limit}
            ")->all();
    }

    /**
     * @param $parent_id
     * @return array|mixed
     */
    public function getTotal($parent_id)
    {
        $join = $this->getJoin();

        return $this->db->select("
            select count(c.id) as t
            from content c
            join content_type ct on ct.type='page' and ct.id=c.type_id
            {$join}
            join content_info i on i.content_id = c.id and i.languages_id = {$this->languages_id}
            where c.parent_id={$parent_id} and  c.published = 1 and c.auto=0
        ")->row('t');
    }

    public function getTags($content_id)
    {
        return $this->db->select("
          select t.alias, t.name
          from tags t
          join tags_content tc on tc.content_id={$content_id} and tc.tags_id=t.id
          where t.languages_id='{$this->languages_id}'
        ")->all();
    }

    /**
     * @param $q
     * @return $this
     */
    public function setJoin($join)
    {
        $this->join[] = $join;

        return $this;
    }

    /**
     * @return string
     */
    private function getJoin()
    {
        $o = '';
        if(!empty($this->join)){
            $o = implode(' ', $this->join);
        }
        return $o;
    }

    /**
     * @param $start
     * @param $num
     * @return $this
     */
    public function setLimit($start, $num)
    {
        $this->limit = "limit {$start}, {$num}";

        return $this;
    }

} 