<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 13.10.14 21:44
 */

namespace models\modules;

use models\App;

defined("SYSPATH") or die();

/**
 * Class Breadcrumb
 * @package models\modules
 */
class Breadcrumb extends App{

    private $items = array();
    public function current($id)
    {
        return $this->db->select("
            select c.id, c.parent_id, i.name, i.title,c.type_id
            from content c
            join content_info i on i.content_id = c.id and i.languages_id = {$this->languages_id}
            where c.id={$id} limit 1
        ")->row();
    }
    /**
     * parents pages
     * @param $id
     * @return array|mixed
     */
    public function parents($id)
    {
        $r = $this->db->select("
            select c.id, c.parent_id, i.name, i.title
            from content c
            join content_info i on i.content_id = c.id and i.languages_id = {$this->languages_id}
            where c.id={$id}
            limit 1
        ")->row();
//        echo "<pre>";print_r($this->items);
//        $this->items = array_merge($this->items, $r);
//        if($r['parent_id'] > 0) {
//            $this->parents($r['parent_id']);
//        }
//        echo "<pre>";print_r($this->items);
        return $r;
    }
    public function category($id, $type)
    {
        switch ($type) {
            case 5:
                $table = 'products_categories';
                break;
            case 8:
                $table = 'posts_categories';
                break;
            default: return false;
        }
        return $this->db->select("
            select c.id, c.parent_id, i.name, i.title
            from {$table} pc
            join content c on c.id = pc.cid
            join content_info i on i.content_id = c.id and i.languages_id = {$this->languages_id}
            where pc.pid = {$id} and c.published = 1
            ORDER BY abs( c.parent_id ) DESC
            limit 1
        ")->row();
    }
    /**
     * main page
     * @return array|mixed
     */
    public function home()
    {
        return $this->db->select("
            select c.id,i.name,i.title
            from content c
            join content_info i on i.content_id = c.id and i.languages_id = {$this->languages_id}
            where c.id=1 limit 1
        ")->row();
    }
    public function getUrl($id)
    {
        return $this->db->select("select alias
                                  from content_info
                                  where content_id = '{$id}' and languages_id = {$this->languages_id}
                                  limit 1
                                  ")->row('alias');
    }

    public function getWorkersGroup($groupID)
    {
        return $this->db->select("Select content_id from pages_workers_group
                where users_group_id={$groupID} limit 1")->row("content_id");
    }
} 