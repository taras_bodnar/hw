<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 25.11.14 14:39
 */

namespace models\modules;

use models\App;

defined("SYSPATH") or die();

class Sitemap extends App {

    /**
     * @param $parent_id
     * @param $isfolder
     * @return array|mixed
     */
    public function getChildren($parent_id, $isfolder)
    {
        $and = $isfolder == 1 ? " and c.isfolder=1" : '';
        return $this->db->select("
             select c.id, c.isfolder, i.name ,i.title
            from content c
            join content_type ct on ct.type='page' and ct.id=c.type_id
            join content_info i on i.content_id = c.id and i.languages_id = {$this->languages_id}
            where c.parent_id={$parent_id} {$and} and c.published=1 and c.auto=0
            order by abs(c.sort) asc, c.id asc
        ")->all();
    }
} 