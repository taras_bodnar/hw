<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 25.11.14 14:39
 */

namespace models\modules;

use models\App;

defined("SYSPATH") or die();

/**
 * Class Payment
 * @package models\modules
 */
class Payment extends App {

    private $module;
    private $id;
    private $settings;

    /**
     * Встановлення вибраного модуля
     * @param $module
     * @return $this
     * @throws \Exception
     */
    public function setActiveModule($module)
    {
        $r = $this->db->select("select id,settings from payment where module = '{$module}' and published=1 limit 1")->row();
        if(empty($r['id'])){
            throw new \Exception('Не можу активувати модуль ' . $module);
        }

        $this->id       = $r['id'];
        $this->module   = $module;
        $this->settings = unserialize($r['settings']);

        return $this;
    }

    /**
     * @param int $delivery_id
     * @return array|mixed
     */
    public function getMethods($delivery_id=0)
    {
        $join = '';
        if($delivery_id>0){
            $join = "join delivery_payment dp on dp.delivery_id={$delivery_id} and dp.payment_id=p.id";
        }
        return $this->db->select("
            select p.id, p.module, p.settings, pi.name, pi.description
            from payment p
            {$join}
            join payment_info pi on pi.payment_id=p.id and pi.languages_id={$this->languages_id}
            where p.published=1
            order by abs(p.sort) asc
            ")->all();
    }

    /**
     * get Settings
     * @return mixed
     */
    public function getSettings()
    {
        return $this->settings;
    }

    public function getInfo()
    {
        return $this->db->select("Select p.id,i.name from payment p
                  join payment_info i on i.payment_id=p.id and i.languages_id=1
                  order by abs(p.sort) asc
        ")->all();
    }
} 