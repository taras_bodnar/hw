<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 15.07.14 18:44
 */

namespace models;

use controllers\core\Model;

defined('SYSPATH') or die();

/**
 * Class Settings
 * @package models
 */
class Settings extends Model{
    public function __construct()
    {
        parent::__construct();

    }
    public function create($data, $info){}
    public function update($id, $data, $info){}

    /**
     * @return array|mixed
     */
    public function load()
    {
        return $this->db->select("select name, value from settings where autoload=1")->all();
    }

    /**
     * @param $name
     * @param $value
     * @param null $description
     * @param int $autoload
     * @return bool|string
     */
    public function add($name, $value, $description = NULL, $autoload = 1)
    {
        return $this->db->insert(
            'settings',
            array(
                'name'        => $name,
                'value'       => $value,
                'description' => $description,
                'autoload'    => $autoload
            )
        );
    }

    /**
     * @param $name
     * @param $value
     * @return bool|void
     */
    public function set($name, $value)
    {
        $id = $this->db->select("select id from settings where name = '$name' limit 1")->row('id');

        if(empty($id)){
            return $this->add($name, $value);
        }

        return $this->db->update('settings', array('value' => $value), " name='{$name}' limit 1");
    }

    /**
     * @param $name
     * @return bool
     */
    public function delete($name)
    {
        return $this->db->delete("settings", " name='{$name}' limit 1");
    }
}