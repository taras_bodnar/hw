<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 06.10.14 21:49
 */

namespace models;

use controllers\core\Model;

defined("SYSPATH") or die();

/**
 * Class App
 * @package models
 */
class App extends Model {

    public function __construct()
    {
        parent::__construct();

        $this->languages_id = \controllers\App::langugesId();
        $this->languages_code = \controllers\App::langugesCode();
    }

} 