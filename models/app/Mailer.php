<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 22.10.14 16:44
 */

namespace models\app;

use models\App;
use controllers\core\Settings;

defined("SYSPATH") or die();

class Mailer extends App{

    public function getTemplate($code)
    {
        $template= $this->db->select("
            select t.email,i.reply_to,i.subject,i.body
            from email_templates t
            join email_templates_info i on i.templates_id=t.id and i.languages_id='{$this->languages_id}'
            where t.code = '{$code}'
        ")->row();

        $header = Settings::instance()->get('et_header');
        $footer = Settings::instance()->get('et_footer');

        $template['body'] = $header . $template['body'] . $footer;

        return $template;
    }
} 