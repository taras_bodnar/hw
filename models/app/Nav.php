<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 06.10.14 21:43
 */

namespace models\app;

use models\App;

defined("SYSPATH") or die();

class Nav extends App{

    /**
     * return menu items by nav_menu_id
     * @param $menu_id
     * @return array|mixed
     */
    public function menuItems($menu_id)
    {
        return $this->db->select("
            select c.id, c.isfolder, i.name ,i.title
            from content c
            join nav_menu_items n on n.nav_menu_id = '{$menu_id}' and  n.content_id=c.id
            join content_info i on i.content_id = c.id and i.languages_id = {$this->languages_id}
            where c.published=1 and c.auto=0
            order by abs(n.sort) asc
            ")->all();
    }

    /**
     * @param $parent_id
     * @return array|mixed
     */
    public function getChildren($parent_id)
    {
        return $this->db->select("
            select c.id, c.isfolder, i.name ,i.title
            from content c
            join content_info i on i.content_id = c.id and i.languages_id = {$this->languages_id}
            where c.parent_id={$parent_id} and c.published=1 and c.auto=0
            order by abs(c.sort) asc
        ")->all();
    }

    /**
     * get languages
     * @return array|mixed
     */
    public function languages()
    {
        return $this->db->select("
              select *
              from languages
              where front=1
              order by front_default desc, id asc
            ")->all();
    }

    public function languageData($id, $key='*')
    {
        return $this->db->select("select {$key} from languages where id = '{$id}' limit 1")->row($key);
    }
} 