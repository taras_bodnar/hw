<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 18.09.14 21:48
 */

namespace models\app;
use controllers\core\Model;

defined("SYSPATH") or die();

/**
 * Class Translations
 * @package models\app
 */
 class Translations extends Model{
    private static $_data;

     public function __construct()
    {
        parent::__construct();

    }

     /**
      * встановлює мову по замовчуванню
      * @param $languages_id
      * @return $this
      */
     public function setLanguagesId($languages_id)
     {
         $this->languages_id = $languages_id;
         return $this;
     }
    /**
     * витгує всі переклади
     * @return array $_data
     */
    public function get()
    {
        $r= $this->db->select("
                    select t.code,i.value
                    from translations t
                    join translations_info i on i.translations_id = t.id and i.languages_id={$this->languages_id}
                ")
            ->all();

        foreach ($r as $row) {
            self::$_data[$row['code']] = $row['value'];
        }

        return self::$_data;
    }


     public function create($data, $info){}
     public function update($id, $data, $info){}
} 