<?php
/**
 * Created by PhpStorm.
 * User: wg
 * Date: 17.03.15
 * Time: 15:54
 */

namespace models\app;

use models\App;

class Features extends App {

    private $where = array();
    private $join=array();

    public function get()
    {
        $where = empty($this->where) ? '' : implode(' ', $this->where);
        if(!empty($where)){
            $where .= ' and ';
        }
        $join = empty($this->join) ? '' : implode(' ', $this->join);

        $features = $this->db->select("
          select f.id, f.type, i.name, f.required
          from features f
          {$join}
          join features_info i on i.features_id=f.id and i.languages_id='{$this->languages_id}'
          where {$where} f.auto=0 and f.published=1
         ")->all();
        foreach ($features as $k=>$row) {
            $features[$k]['values'] = $this->getValues($row['id'], $row['type']);
        }
        return $features;
    }

    /**
     * @param $id
     * @return array|mixed
     */
    public function getType($id){return $this->db->select("select type from features where id={$id} limit 1")->row('type');}
    /**
     * @param $id
     * @return array|mixed
     */
    public function getItem($id, $values = false)
    {
        $r = $this->db->select("
        select f.id, f.type, i.name
        from features f
        join features_info i on i.features_id=f.id and i.languages_id='{$this->languages_id}'
        where f.id='{$id}' and f.auto=0 and f.published=1
        ")->row();
        if(empty($r)) return array();
        if($values){
            $r['values'] = $this->getValues($r['id'], $r['type']);
        }

        return $r;
    }

    private function getValues($features_id, $type)
    {
        if($type == 'number'){
            $values= $this->db->select("
            select MAX(value) as value
            from features_content_values
            where features_id={$features_id} and languages_id = 0
            ")->row('value');
        } else {
            $values =  $this->db->select("
            select fv.id,i.value
            from features_values fv
            join features_values_info i on i.values_id=fv.id and i.languages_id='{$this->languages_id}'
            where fv.features_id={$features_id}
            order by abs(i.value) asc
        ")->all();
        }

        return $values;
    }

    /**
     * @param $content_id
     * @param $features_id
     * @param $type
     * @return array
     */
    public function getContentValues($content_id, $features_id, $type='text')
    {
        $res=array();
        // декілька значень
        if(in_array($type, array('sm','checkboxgroup'))){

            $v = $this->db->select("
                    select fvi.value,fcv.features_values_id as id
                    from features_content_values fcv
                    join features_values_info fvi on fvi.values_id=fcv.features_values_id and fvi.languages_id={$this->languages_id}
                    where fcv.features_id='{$features_id}' and fcv.content_id = '{$content_id}'  and fcv.languages_id = 0
                ")->all();
            foreach ($v as $rrr) {
                $res['values'][$rrr['id']] = $rrr['value'];
            }

            // одне з багатьох
        } elseif(in_array($type, array('select','radiogroup'))){

            $res['value'] = $this->db->select("
                    select fvi.value
                    from features_content_values fcv
                    join features_values_info fvi on fvi.values_id=fcv.features_values_id and fvi.languages_id={$this->languages_id}
                    where fcv.features_id='{$features_id}' and fcv.content_id = '{$content_id}'  and fcv.languages_id = 0
                    limit 1
                ")->row('value');

            // одне значення
        } elseif($type == 'number') {

            $res['value'] = $this->db->select("
                select fcv.value
                from  features_content_values fcv
                where fcv.features_id='{$features_id}' and fcv.content_id = '{$content_id}' and fcv.languages_id = 0
                limit 1
            ")->row('value');
        }
         else {

            $res['value'] = $this->db->select("
                select fcv.value
                from features_content_values fcv
                where fcv.features_id='{$features_id}' and fcv.content_id = '{$content_id}' and fcv.languages_id='{$this->languages_id}'
                limit 1
            ")->row('value');
        }

        return $res;
    }


    public function getContentFeatures($content_id, $features_id = 0)
    {
        $where = $features_id > 0 ? " and f.id = '{$features_id}'" : '';
        $features = $this->db->select("select f.id,i.name, f.type, f.hide
                                  from features f
                                  join features_info i on i.features_id=f.id and i.languages_id='{$this->languages_id}'
                                  where f.auto = 0 {$where} and f.published=1
                                  ")->all();
        $res = array();
        foreach ($features as $k=>$row) {
            $res[$row['id']] = $row;
            // декілька значень
            if(in_array($row['type'], array('sm','checkboxgroup'))){

                $v = $this->db->select("
                    select fvi.value,fcv.features_values_id as id
                    from features_content_values fcv
                    join features_values_info fvi on fvi.values_id=fcv.features_values_id and fvi.languages_id={$this->languages_id}
                    where fcv.features_id='{$row['id']}' and fcv.content_id = '{$content_id}'  and fcv.languages_id = 0
                ")->all();
                foreach ($v as $rrr) {
                    $res[$row['id']]['values'][$rrr['id']] = $rrr['value'];
                }

            // одне з багатьох
            } elseif(in_array($row['type'], array('select','radiogroup'))){

                $res[$row['id']]['value'] = $this->db->select("
                    select fvi.value
                    from features_content_values fcv
                    join features_values_info fvi on fvi.values_id=fcv.features_values_id and fvi.languages_id={$this->languages_id}
                    where fcv.features_id='{$row['id']}' and fcv.content_id = '{$content_id}'  and fcv.languages_id = 0
                    limit 1
                ")->row('value');

            // одне значення
            } else {
                if($row['type'] == 'number'){
                    $res[$row['id']]['value'] = $this->db->select("
                        select value
                        from features_content_values
                        where features_id='{$row['id']}' and content_id = '{$content_id}' and languages_id = 0
                        limit 1
            ")->row('value');
                } else {
                    $res[$row['id']]['value'] = $this->db->select("
                        select value
                        from features_content_values
                        where features_id='{$row['id']}' and content_id = '{$content_id}' and languages_id='{$this->languages_id}'
                        limit 1
            ")->row('value');
                }
            }
        }
        return $res;
    }

    /**
     * @param $where
     * @return $this
     */
    public function where($where)
    {
        $this->where[] = $where;
        return $this;
    }

    /**
     * @param $join
     * @return $this
     */
    public function join($join)
    {
        $this->join[] = $join;
        return $this;
    }

    /**
     * @return $this
     */
    public function onFilter()
    {
        $this->where[] = ' f.show_filter';
        return $this;
    }
}
