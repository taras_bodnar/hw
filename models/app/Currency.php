<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 10.12.14 15:11
 */

namespace models\app;

use models\App;

defined("SYSPATH") or die();

class Currency extends App{
    /**
     * @return array|mixed
     */
    public function get()
    {
        return $this->db->select("select * from currency order by is_main desc,id asc")->all();
    }

    /**
     * get data from default currency
     * @param string $key
     * @return array|mixed
     */
    public function getDefault($key='*')
    {
        return $this->db->select("select {$key} from currency where is_main=1 limit 1")->row($key);
    }
} 