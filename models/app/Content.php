<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 18.09.14 19:55
 */

namespace models\app;

defined("SYSPATH") or die();

class Content extends \models\engine\Content {

    public function __construct()
    {
        parent::__construct();
        $this->languages_id = \controllers\App::langugesId();
        $this->languages_code = \controllers\App::langugesCode();
    }

    /**
     * витягує ІД сторінки по аліасу
     * @param $alias
     * @param $languages_id
     * @return array|mixed
     */
    public function getIdByAlias($alias, $languages_id)
    {
        return $this->db->select("select content_id as id
                                  from content_info
                                  where alias like '{$alias}' and languages_id = {$languages_id}
                                  limit 1
                                  ")->row('id');
    }

    /**
     * вертає аліас для парсера
     * @param $id
     * @param $languages_id
     * @return array|mixed
     */
    public function getAliasById($id, $languages_id)
    {
        return $this->db->select("select alias
                                  from content_info
                                  where content_id = {$id} and languages_id = {$languages_id}
                                  limit 1
                                  ")->row('alias');
    }

    /**
     * @param $id
     * @return array|mixed
     */
    public function makeUrl($id)
    {
        return $this->db->select("select IF(l.id=ld.id, alias, CONCAT(l.code, '/', alias) ) as alias
                                  from content_info
                                  join languages ld on ld.front_default = 1
                                  join languages l on l.id='{$this->languages_id}'
                                  where content_id = '{$id}' and languages_id = '{$this->languages_id}'
                                  limit 1
                                  ")->row('alias');
    }
    public function getTagName($tag)
    {
        return $this->db->select("select name from tags where alias = '{$tag}'")->row('name');
    }

    public function getAuthor($id)
    {
        return $this->db->select("select CONCAT(name, ' ' , surname) as name from users where id='{$id}'")->row('name');
    }
    /**
     * вертає інформацію просторінку
     * @param $id
     * @param $languages_id
     * @return array|mixed
     */
    public function getData($id, $languages_id)
    {
        return $this->db->select("select c.*,
                                  DATE_FORMAT(c.createdon, '%d') as dd,
                                  DATE_FORMAT(c.createdon, '%m') as mm,
                                  DATE_FORMAT(c.createdon, '%Y') as yy,
                                  i.title,i.name,i.keywords,i.case_title,i.description,i.content, i.languages_id,
                                   CONCAT(ct.type , '/' , t.path) as template,
                                   l.code as languages_code, l.name as languages_name
                                  from content c
                                  join content_type ct on ct.id=c.type_id
                                  join content_templates t on t.id=c.templates_id
                                  join languages l on l.id='{$languages_id}'
                                  join content_info i on i.content_id=c.id and i.languages_id = {$languages_id}
                                  where c.id={$id} and c.published=1 and c.auto=0
                                  limit 1
        ")->row();
    }

    /**
     * @param $id
     * @param string $key
     * @return array|mixed
     */
    public function getInfo($id, $key='*')
    {
        return $this->db->select("
            select {$key}
            from content_info
            where content_id = '{$id}' and languages_id = '{$this->languages_id}'
            limit 1")
            ->row($key);
    }
    public function getRoute($url)
    {
        return $this->db->select("select url_to as url, header from routers where url_from='{$url}' limit 1")->row();
    }

    /**
     * @param $users_id
     * @return array|mixed
     */
    public function getWorkerCategory($users_id)
    {
        return $this->db
            ->select("
                  select i.name
                  from users u
                  join users_group_info i on i.users_group_id = u.users_group_id and i.languages_id='{$this->languages_id}'
                  where u.id='{$users_id}'
                  limit 1
               ")
            ->row('name');
    }

    /**
     * @param $users_id
     * @return array|mixed
     */
    public function getWorkerCity($users_id)
    {
        return $this->db
            ->select("
                select i.name
                from users u
                join guides_info i on i.guides_id = u.city_id and i.languages_id='{$this->languages_id}'
                where u.id={$users_id} limit 1")
            ->row('name');
    }
}