<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 18.09.14 19:55
 */

namespace models\app;

defined("SYSPATH") or die();

use models\App;

/**
 * Class Guides
 * @package models\app
 */
class Guides extends App{

    private $limit = '';
    private $where = array();
    /**
     * @param $parent_id
     * @return array|mixed
     */
    public function get($parent_id)
    {
        return $this->db->select("
            select g.id, g.value, i.name
            from guides g,guides_info i
            where g.parent_id={$parent_id} and i.guides_id=g.id and i.languages_id={$this->languages_id}
            order by sort desc, g.id asc, i.name
        ")->all();
    }

    /**
     * @param $parent_id
     * @return array|mixed
     */
    public function getCities($parent_id,$uid)
    {
        $w = $this->getWhere();
        return $this->db->select("
            select g.id, g.value, i.name, if(uc.guides_id>0,'selected','') as selected,
            uc.main as main_city
            from guides g
            join guides_info i on i.guides_id=g.id and i.languages_id={$this->languages_id}
            left join users_cities uc on g.id=uc.guides_id and uc.users_id={$uid}
            where g.parent_id={$parent_id} {$w}
            order by uc.main desc, i.name asc
        ")->all();
    }

    /**
     * @param $parent_id
     * @return array|mixed
     */
    public function getFilter($parent_id)
    {
        return $this->db->select("
            Select '' as value , 'Всі міста' as name
            union
            select g.id as value, i.name
            from guides g,guides_info i
            where g.parent_id={$parent_id} and i.guides_id=g.id and i.languages_id={$this->languages_id}
            order by value asc
        ")->all();
    }

    public function getUserCities($id)
    {
        $gold_account = $this->db->select("select gold from users where id = '{$id}' limit 1")->row('gold');

        $w = !$gold_account ? "and uc.main = 1" : "";

        return $this->db->select("
            select g.id,i.name, uc.main
            from guides g
            join guides_info i on i.guides_id=g.id and i.languages_id={$this->languages_id}
            join users_cities uc on g.id=uc.guides_id and uc.users_id={$id}
            where g.parent_id=9 {$w}
        ")->all();
    }

    public function getIdByAlias($alias)
    {
        return $this->db->select("Select id from guides where value='{$alias}' limit 1")->row("id");
    }

    public function getNameByAlias($alias)
    {
        return $this->db->select("Select i.name from guides g
            join guides_info i on g.id=i.guides_id and i.languages_id={$this->languages_id}
            where value='{$alias}' limit 1")->row("name");
    }

    public function getNameByID($id)
    {
        return $this->db->select("Select i.name from guides g
            join guides_info i on g.id=i.guides_id and i.languages_id={$this->languages_id}
            where g.id='{$id}' limit 1")->row("name");
    }

    public function getValue($id)
    {
        return $this->db->select("Select value from guides where id={$id} limit 1")->row("value");
    }

    public function setLimit($start, $num)
    {
        $this->limit = "limit {$start}, {$num}";

        return $this;
    }

    /**
     * @param $q
     * @return $this
     */
    public function setWhere($q)
    {
        $this->where[] = $q;

        return $this;
    }

    /**
     * @return string
     */
    private function getWhere()
    {
        $w = '';
        if(!empty($this->where)){
            $w = implode(' ', $this->where);
        }
        return $w;
    }
}