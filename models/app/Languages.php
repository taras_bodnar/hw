<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 18.09.14 20:19
 */

namespace models\app;
defined("SYSPATH") or die();

class Languages extends \models\engine\Languages{

    /**
     * повертає всю інформацію або конкретне поле по активній мові
     * @param string $key
     * @return array|mixed
     */
    public function getDefault($key='*')
    {
        return $this->db->select("select {$key} from languages where front_default = 1")->row($key);
    }

    /**
     * get languages id by code
     * @param $code
     * @return array|mixed
     */
    public function getIdByCode($code)
    {
        return $this->db->select("select id from languages where code = '{$code}' limit 1")->row('id');
    }

    /**
     * @param $code
     * @return array|mixed
     */
    public function getDataByCode($code)
    {
        return $this->db->select("select id,code,name from languages where code = '{$code}' limit 1")->row();
    }

    /**
     * @param $id
     * @param string $key
     * @return array|mixed
     */
    public function getData($id, $key='*')
    {
        return $this->db->select("select {$key} from languages where id = '{$id}' limit 1")->row($key);
    }

    /**
     * get all languages
     * @return array
     */
    public function get()
    {
        return $this->db->select("select *
                                  from languages
                                  where front=1
                                  order by front_default desc, id asc")->all();
    }

} 