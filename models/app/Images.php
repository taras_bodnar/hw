<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 12.10.14 21:08
 */

namespace models\app;

use models\App;

defined("SYSPATH") or die();

class Images extends App{

    /**
     * get content image
     * @param $content_id
     * @param null $size
     * @return array|mixed
     */
    public function cover($content_id, $size = null)
    {
        $r = $this->db->select("select i.name as src, ii.alt
                                  from content_images i
                                  left join content_images_info ii on
                                                                    ii.images_id = i.id and
                                                                    ii.languages_id = '{$this->languages_id}'
                                  where i.content_id = '{$content_id}'
                                  order by abs(i.sort) asc
                                  limit 1
                                  ")->row();
        if($size && !empty($r)){
            $r['src'] = HOTWEDURL . "uploads/content/{$content_id}/{$size}/{$r['src']}";
        }
        return $r;
    }

    /**
     * get content images
     * @param $content_id
     * @param null $size
     * @param int $start
     * @param int $num
     * @return array
     */
    public function get($content_id, $size = null, $start=0, $num = 1000)
    {
        $r = $this->db->select("select i.name as src, ii.alt
                                  from content_images i
                                  left join content_images_info ii on
                                                                    ii.images_id = i.id and
                                                                    ii.languages_id = {$this->languages_id}
                                  where i.content_id = {$content_id}
                                  order by abs(i.sort) asc
                                  limit {$start}, {$num}
                                  ")->all();
        $res = array();
        foreach ($r as $row) {
            if($size){
                $row['src'] = HOTWEDURL."uploads/content/{$content_id}/{$size}/{$row['src']}";
            }
            $res[] = $row;
        }

        return $res;
    }

    /**
     * format images path
     * @param $content_id
     * @param $size
     * @return string
     */
    public function path($content_id, $size)
    {
        return HOTWEDURL."/uploads/content/{$content_id}/$size/";
    }

} 