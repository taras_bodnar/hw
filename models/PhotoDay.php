<?php
/**
 * Created by PhpStorm.
 * User: taras
 * Date: 26.04.16
 * Time: 15:11
 */

namespace models;


class PhotoDay extends App
{
    private $limit = '';
    private $where = array();

    public function getTotal()
    {
        $w = $this->getWhere();
        return $this->db->select("
                Select count(uu.id) as t from users_uploads uu
                where uu.createdon > DATE_SUB(CURDATE(), INTERVAL 1 WEEK) {$w}")->row("t");
    }

    public function getItems()
    {
        $w = $this->getWhere();
        return $this->db->select("
                Select uu.*,u.id as users_id, CONCAT_WS( ' ' ,u.name, u.surname) as username from users_uploads uu
                join users u on u.id=uu.users_id
                where uu.createdon > DATE_SUB(CURDATE(), INTERVAL 1 WEEK) and day_check!=1 and u.users_group_id in(5,9) {$w}
                order by abs(uu.id) desc
                {$this->limit}
                ")->all();
    }

    public function getInfo($id)
    {
        return $this->db->select("select uu.*,CONCAT_WS( ' ' ,u.name, u.surname) as username from users_uploads uu
          join users u on u.id=uu.users_id
          where uu.id={$id} limit 1")->row();
    }

    public function getTotalComment($id)
    {
        return $this->db->select("
                Select count(id) as t from img_comments where users_uploads_id={$id}
        ")->row("t");
    }

    public function getTotalLike($id)
    {
        return $this->db->select("
                Select count(id) as t from img_like where users_uploads_id={$id}
        ")->row("t");
    }

    public function validateLike($pid, $cid)
    {
        return $this->db->select("
                Select count(id) as c from img_like
                where users_uploads_id={$pid} and users_id={$cid}
                limit 1
        ")->row("c");
    }

    public function getComments($id)
    {
        return $this->db->select("
            select c.*, DATE_FORMAT(c.date, '%d.%m.%Y') as pubdate, CONCAT_WS( ' ' ,u.name, u.surname) as username
            from img_comments c
            join users u on u.id = c.users_id
            where c.users_uploads_id = {$id}
            order by abs(c.id) desc
             {$this->limit}
        ")->all();
    }

    public function createComment($data)
    {
        return $this->db->insert('img_comments', $data);
    }

    public function getLastComment($id)
    {
        return $this->db->select("
                Select c.*, DATE_FORMAT(c.date, '%d.%m.%Y') as pubdate, CONCAT_WS( ' ' ,u.name, u.surname) as username
                 from img_comments c
                 join users u on u.id = c.users_id
                  where c.id = {$id}  limit 1
                ")->row();
    }


    public function addLike($pid, $cid)
    {
        $id = $this->db->insert("img_like",
            array(
                'users_id' => $cid,
                'users_uploads_id' => $pid,
            )
        );

        if ($id > 0) {
            $id = $this->getTotalLike($pid);
        }

        return $id;
    }

    public function getPhotoVideoOfDay($type)
    {
        $w = $this->getWhere();
        $res = $this->db->select("
                SELECT uu.*,CONCAT_WS( ' ' ,u.name, u.surname) as username FROM  users_uploads uu
                join users u on u.id = uu.users_id
                WHERE  uu.type='{$type}' and uu.day_check=1 {$w}
        ")->row();

        return $res;
    }


    public function getPhotoVideoMore($type)
    {
        $w = $this->getWhere();
        return $this->db->select("
                SELECT uu.*,CONCAT_WS( ' ' ,u.name, u.surname) as username FROM `img_like` il
                join users_uploads uu on uu.id=il.users_uploads_id
                join users u on u.id = uu.users_id
                WHERE  uu.type='{$type}' and uu.day_check!=1 and uu.createdon > DATE_SUB(CURDATE(), INTERVAL 1 WEEK) {$w}
                group by il.users_uploads_id order by count(il.id) desc
                {$this->limit}
        ")->all();
    }

    public function getPrevNextId($id,$type)
    {
        return $this->db->select("SELECT
                  (
                    SELECT id
                    FROM users_uploads WHERE id < {$id}
                    and type='{$type}'
                   ORDER BY id desc LIMIT 1
                  ) AS previd,
                  (
                    SELECT id
                    FROM users_uploads WHERE id > {$id}
                   and type='{$type}'
                    ORDER BY id asc LIMIT 1
                  ) AS nextid
  ")->row();
    }

    public function getNextId($id)
    {
        return $this->db->select("
            (SELECT c.id FROM content c
                 WHERE c.id > {$id} and c.id!={$id} and c.id>{$id} and c.type_id=8 and c.published=1 and c.auto=0
                 ORDER BY c.id ASC LIMIT 1)
                UNION (SELECT c.id FROM content c
                       WHERE c.id = (SELECT MIN(c.id) FROM content) and c.id>{$id} and c.id!={$id}  and c.type_id=8
                       and c.published=1 and c.auto=0)
                LIMIT 1
        ")->row("id");
    }

    /**
     * @param $q
     * @return $this
     */
    public function setWhere($q)
    {
        $this->where[] = $q;

        return $this;
    }

    /**
     * @return string
     */
    private function getWhere()
    {
        $w = '';
        if (!empty($this->where)) {
            $w = implode(' ', $this->where);
        }
        return $w;
    }


    public function clearWhere()
    {
        $this->where = array();
        return $this;
    }

    public function limit($start, $num)
    {
        $this->limit = " limit $start, $num";

        return $this;
    }
}