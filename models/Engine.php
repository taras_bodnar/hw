<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 06.10.14 21:49
 */

namespace models;

use controllers\core\Model;

defined("SYSPATH") or die();

abstract class Engine extends Model {

    abstract function create($data, $info);
    abstract function update($id, $data, $info);

} 