<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 05.07.14 20:34
 */

namespace models\engine;
use models\Engine;

defined('SYSPATH') or die();

class Themes extends Engine {

    public function __construct()
    {
        parent::__construct();
    }


    /**
     * insert data to table structure
     * @param $data
     * @param $info
     * @return int insert ID
     */
    public function create($data, $info=array())
    {
//        return $this->createRow('content_type', $data);
    }

    /**
     * @param $id
     * @param $data
     * @param $info
     * @return bool|string
     */
    public function update($id, $data, $info=array()){
//        return $this->updateRow('content_type', $id, $data);
    }

    /**
     * delete item from structure
     * @param $id
     * @return int
     */
    public function delete($id)
    {
//        return $this->deleteRow('content_type', $id);
    }
} 