<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 02.10.14 20:59
 */

namespace models\engine;

use models\Engine;

defined("SYSPATH") or die();

class Dashboard extends Engine{

    private $where = array();
    public function create($data,$info){}
    public function update($id,$data,$info){}

    /**
     * get engine name
     * @return array|mixed
     */
    public function engineName()
    {
        return $this->db->select("
                    select name
                    from components_info
                    where components_id = 1 and languages_id={$this->languages_id}
                    limit 1
               ")->row('name');
    }

    /**
     * вибрані розділи швидкого доступу
     * @return array|mixed
     */
    public function quickLaunchItems()
    {
        return $this->db->select("select s.id,s.icon,REPLACE(s.controller,'controllers/engine/','') as url,i.name
                                from components s
                                join dashboard_quick_launch q on q.components_id=s.id
                                join components_info i on i.components_id=s.id and i.languages_id = {$this->languages_id}
                                where s.published=1
                                order by abs(q.sort) asc
                                limit 6
                                ")->all();
    }

    /**
     * список елементів структури
     * @return array|mixed
     */
    public function structureItems()
    {
        return $this->db->select("select s.id,i.name, IF(q.id > 0, 'selected' , '') as selected
                                from components s
                                left join dashboard_quick_launch q on q.components_id=s.id
                                join components_info i on i.components_id=s.id and i.languages_id = {$this->languages_id}
                                where s.published=1 and s.id <> 1
                                order by abs(s.id) asc
                                ")->all();
    }

    /**
     * delete all items
     * @return int
     */
    public function clearQuickLaunchItems()
    {
        return $this->db->delete("dashboard_quick_launch");
    }

    /**
     * @param $components_id
     * @param $sort
     * @return bool|string
     */
    public function quickLaunchAddItem($components_id, $sort)
    {
        return $this->db->insert("dashboard_quick_launch", array('components_id'=>$components_id, 'sort'=>$sort));
    }

    public function quickLaunchSort($components_id, $i)
    {
        return $this->db->update("dashboard_quick_launch", array('sort'=>$i), " components_id={$components_id} limit 1");
    }

    public function getLastPages($type='page', $img=false)
    {
        $select = $img ? ',im.name as img' : '';
        $join = $img ? "left join content_images im on im.content_id = c.id and im.sort = 1 " : '';
        return $this->db->select("select c.id, c.editedon, i.name {$select}
                                  from content c
                                  join content_info i on i.content_id=c.id and i.languages_id={$this->languages_id}
                                  join content_type t on t.id=c.type_id and t.type='{$type}'
                                  {$join}
                                  where c.auto=0
                                  order by c.editedon desc
                                  limit 4
                                  ")->all();
    }

    public function getTotalOrders()
    {
        return $this->db->select("select COUNT(id) as c from orders where status in ('pending','confirmed')")->row('c');
    }

    public function getOrders($status)
    {
        $w = $this->getWhere();
        return $this->db->select("
                Select count(id) as c from orders
                where status='{$status}' {$w}
        ")->row('c');
    }

    /**
     * @param $q
     * @return $this
     */
    public function setWhere($q)
    {
        $this->where[] = $q;

        return $this;
    }

    /**
     * @return string
     */
    private function getWhere()
    {
        $w = '';
        if(!empty($this->where)){
            $w = implode(' ', $this->where);
        }
        return $w;
    }
}