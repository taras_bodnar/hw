<?php
/**
 * Company Otakoyi.com
 * Author: wg
 * Date: 04.01.15 21:31
 */

namespace models\engine\plugins;

use models\Engine;

defined("SYSPATH") or die();

class ProductsRelated extends Engine {

    public function create($data, $info=array())
    {
        return $this->db->insert('products_related', $data);
    }

    public function update($id, $data, $info=array())
    {
        return $this->db->update('products_related', $data, "id={$id} limit 1");
    }

    public function delete($id)
    {
        return $this->db->delete('products_related', "id={$id} limit 1");
    }
    public function sort($id, $sort)
    {
        return $this->db->update('products_related', array('sort'=>$sort), " id='{$id}' limit 1");
    }
    /**
     * @param $products_id
     * @return array|mixed
     */
    public function get($products_id)
    {
        return $this->db->select("
        select pr.id,c.id as pid,i.name as name, CONCAT('/uploads/content/', c.id , '/thumbnails/', ci.name) as img
        from products_related pr
        join content c on c.id=pr.related_id and c.published=1
        join content_type ct on ct.type='product' and ct.id = c.type_id
        join content_info i on i.content_id=c.id and i.languages_id={$this->languages_id}
        left join content_images ci on ci.content_id=c.id and ci.sort=1
        where pr.products_id={$products_id}
        order by abs(pr.sort) asc
        ")->all();
    }

    /**
     * @param $products_id
     * @param $related_id
     * @return bool|string
     */
    public function add($products_id, $related_id)
    {
        return $this->db->insert('products_related', array('products_id'=>$products_id,'related_id'=>$related_id));
    }

    public function getProducts($products_id, $where='')
    {
        return $this->db->select("
        select c.id,i.name as text, CONCAT('/uploads/content/', c.id , '/thumbnails/', ci.name) as img
        from content c
        join content_type ct on ct.type='product' and ct.id = c.type_id
        join content_info i on i.content_id=c.id and i.languages_id={$this->languages_id}
        left join content_images ci on ci.content_id=c.id and ci.sort=1
        where c.auto =0
          {$where}
          and c.published=1
          and c.id <> {$products_id}
          and c.id not in (
            select related_id from products_related where products_id={$products_id}
          )
        ")->all();
    }
}