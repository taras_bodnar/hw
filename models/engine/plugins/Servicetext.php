<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 17.03.17
 * Time: 12:31
 */

namespace models\engine\plugins;

use controllers\core\Model;

class Servicetext extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function save($service_id, $city_id, $content_id)
    {
        $this->db->delete('service_city', "content_id = $content_id");

        $this->db->insert('service_city', array(
            'service_id' => $service_id,
            'city_id' => $city_id,
            'content_id' => $content_id,
        ));
    }

    public function getService($id = null)
    {
        return $this->db->select("
                                  SELECT DISTINCT c.id,ci.name,if(s.id>0,'selected','') as selected FROM content c
                                  JOIN content_info ci ON ci.content_id = c.id AND ci.languages_id={$this->languages_id}
                                  left join service_city s on s.service_id = c.id and s.content_id={$id}
                                  WHERE c.parent_id = 3
        ")->all();
    }

    public function getCity($id = null)
    {
        return $this->db->select("
                                  SELECT DISTINCT g.id,gi.name,if(s.id>0,'selected','') as selected FROM guides g
                                  JOIN guides_info gi ON gi.guides_id = g.id AND gi.languages_id={$this->languages_id}
                                  left join service_city s on s.city_id = g.id and s.content_id={$id}
                                  WHERE g.parent_id = 9
        ")->all();
    }

    protected function install()
    {
        // TODO: Implement install() method.
    }

    protected function uninstall()
    {
        // TODO: Implement uninstall() method.
    }
}