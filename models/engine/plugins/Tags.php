<?php
/**
 * Company Otakoyi.com
 * Author: wg
 * Date: 24.12.14 16:14
 */

namespace models\engine\plugins;

use controllers\engine\Content;
use models\Engine;

defined("SYSPATH") or die();

class Tags extends Engine {
    /**
     * insert data to table structure
     * @param $data
     * @param $info
     * @return int insert ID
     */
    public function create($data, $info=array())
    {
        return $this->createRow('tags', $data);
    }

    /**
     * @param $id
     * @param $data
     * @param $info
     * @return bool|string
     */
    public function update($id, $data, $info=array()){
        return $this->updateRow('tags', $id, $data);
    }

    public function delete($id)
    {
        return $this->db->delete("tags", " id = '{$id}' limit 1");
    }

    /**
     * @param $content_id
     * @return int
     */
    public function clear($content_id)
    {
        return $this->db->delete("tags_content", " content_id = {$content_id}");
    }

    /**
     * @param $content_id
     * @param $languages_id
     * @return array|mixed
     */
    public function get($content_id, $languages_id)
    {
        return $this->db->select("
            select t.name
            from tags_content tc
            join tags t on t.id=tc.tags_id and t.languages_id={$languages_id}
            where tc.content_id ={$content_id}
            ")->all();
    }

    /***
     * @param $content_id
     * @param $languages_id
     * @param array $tags
     */
    public function set($content_id, $languages_id, array $tags)
    {
        foreach ($tags as $k=>$name) {
            $name= trim($name);
            if(empty($name)) continue;

            $tags_id = $this->getId($languages_id, $name);
            $lcode = $this->db->select("select code from languages where id='{$languages_id}' limit 1")->row('code');

            if(empty($tags_id)){
                $alias = Content::translit($name, $lcode);
                $tags_id = $this->create(array('name'=> $name, 'languages_id'=>$languages_id, 'alias' => $alias));
            }

            $cid = $this->db->select("
                         select id
                         from tags_content
                         where content_id = {$content_id} and tags_id ={$tags_id} limit 1")->row('id');

            if(empty($cid)){
                $this->db->insert('tags_content', array('tags_id'=>$tags_id,'content_id'=>$content_id));
            }
        }
    }

    /**
     * @param $languages_id
     * @param $name
     * @return array|mixed
     */
    private function getId($languages_id, $name)
    {
        return $this->db->select("select id from tags where name = '$name' and languages_id={$languages_id} limit 1")->row('id');
    }

    /**
     * @param $content_id
     * @param $languages_id
     * @param $name
     * @return int
     */
    public function remove($content_id, $languages_id, $name)
    {
        $tags_id = $this->getId($languages_id, $name);
        if(empty($tags_id)) return 0;

        $t =$this->total($tags_id);

        if($t>1){
            $this->db->delete("tags_content", " tags_id={$tags_id} and content_id={$content_id} limit 1");
        } else{
            $this->delete($tags_id);
        }
        return 1;
    }

    /**
     * @param $tags_id
     * @return array|mixed
     */
    private function total($tags_id)
    {
        return $this->db->select("select count(id) as t from tags_content where tags_id={$tags_id}")->row('t');
    }
}