<?php
/**
 * Company Otakoyi.com
 * Author: wg
 * Date: 24.12.14 16:14
 */

namespace models\engine\plugins;

use controllers\engine\Content;
use models\Engine;

defined("SYSPATH") or die();

class UsersSort extends Engine {

    public function create($data, $info){}
    public function update($id,$data, $info){}

    /**
     * @param $parent_id
     * @return array|mixed
     */
    public function getCities($parent_id,$id)
    {
        return $this->db->select("
            select g.id, g.value, i.name, if(us.id>0,'selected','') as selected
            from guides g
            join guides_info i on i.guides_id=g.id and i.languages_id={$this->languages_id}
            join users u on u.city_id=g.id and u.id={$id}
            left join users_sort us on us.guides_id=g.id and us.users_id={$id}
            where g.parent_id={$parent_id}
            order by i.name asc
        ")->all();
    }

    public function insert($id,$city,$sort)
    {
        $this->db->delete("users_sort","users_id={$id}");
        return $this->db->insert("users_sort",
            array(
                'users_id'=>$id,
                'guides_id'=>$city,
                'sort'=>$sort
            )
            );
    }

    public function getSort($id)
    {
        return $this->db->select("
                Select sort from users_sort where users_id={$id} limit 1
        ")->row("sort");
    }
}