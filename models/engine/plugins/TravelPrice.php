<?php
/**
 * Company Otakoyi.com
 * Author: wg
 * Date: 24.12.14 16:14
 */

namespace models\engine\plugins;

use controllers\engine\Content;
use models\Engine;

defined("SYSPATH") or die();

class TravelPrice extends Engine {

    public function create($data, $info){}
    public function update($id,$data, $info){}

    public function getParentType($id)
    {
        return $this->db->select("
                SELECT c1.templates_id
                FROM content c
                JOIN content c1 ON c1.id = c.parent_id
                WHERE c.id ={$id}
        ")->row("templates_id");
    }

    public function addPrice($travel_price,$travel_old_price,$id)
    {
        return $this->db->update("content",
                array(
                    'travel_price'=>$travel_price,
                    'travel_old_price'=>$travel_old_price,
                ),"id={$id}"
            );
    }

    public function getPrice($id)
    {
        return $this->db->select("
                Select travel_price,travel_old_price from content where id={$id} limit 1
        ")->row();
    }
}