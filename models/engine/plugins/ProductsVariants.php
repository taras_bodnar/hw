<?php
/**
 * Company Otakoyi.com
 * Author: wg
 * Date: 04.01.15 21:31
 */

namespace models\engine\plugins;

use models\Engine;

defined("SYSPATH") or die();

class ProductsVariants extends Engine {

    public function create($data, $info=array())
    {
        $data['is_variant']=1;
        return $this->db->insert('products_options', $data);
    }

    public function update($id, $data, $info=array())
    {
        return $this->db->update('products_options', $data, "id={$id} limit 1");
    }

    public function delete($id)
    {
        return $this->db->delete('products_options', "id={$id} limit 1");
    }

    /**
     * @param $products_id
     * @return array|mixed
     */
    public function get($products_id)
    {
        return $this->db->select("select * from products_options where products_id={$products_id} and is_variant=1 order by abs(sort) asc")->all();
    }
}