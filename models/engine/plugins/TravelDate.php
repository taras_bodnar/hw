<?php
/**
 * Company Otakoyi.com
 * Author: wg
 * Date: 24.12.14 16:14
 */

namespace models\engine\plugins;

use controllers\engine\Content;
use models\Engine;

defined("SYSPATH") or die();

class TravelDate extends Engine {

    public function create($data, $info){}
    public function update($id,$data, $info){}

    public function getParentType($id)
    {
        return $this->db->select("
                SELECT c1.templates_id
                FROM content c
                JOIN content c1 ON c1.id = c.parent_id
                WHERE c.id ={$id}
        ")->row("templates_id");
    }

    public function addDate($data,$id)
    {
        $date = explode(',',$data);
        $this->db->delete("travel_date","content_id={$id}");
        $this->db->insert("travel_date",
            array(
                'content_id'=>$id,
                'start_date'=>$date[0],
                'end_date'=>$date[1],
            )
            );
        return $this->db->update("content",
                array(
                    'travel_date'=>$data
                ),"id={$id}"
            );
    }

    public function getDate($id)
    {
        return $this->db->select("
                Select travel_date from content where id={$id} limit 1
        ")->row("travel_date");
    }
}