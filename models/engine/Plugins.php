<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 26.06.14 13:54
 */

namespace models\engine;
use models\Engine;


defined('SYSPATH') or die();

/**
 * Class plugins
 * @package models\engine
 */
class Plugins extends Engine {

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * insert data to table structure
     * @param $data
     * @param $info
     * @return int insert ID
     */
    public function create($data, $info=array())
    {
        return $this->createRow('plugins', $data);
    }

    /**
     * @param $id
     * @param $data
     * @param $info
     * @return bool|string
     */
    public function update($id, $data, $info=array()){
        return $this->updateRow('plugins', $id, $data);
    }

    /**
     * delete item from structure
     * @param $name
     * @return int
     */
    public function delete($name)
    {
      return $this->db->delete("plugins", " controller = '{$name}' limit 1");
    }

    /**
     * position of plugin
     * @return array
     */
    public function getPosition()
    {
        return $this->db->enumValues('plugins', 'position');
    }

    /**
     * @param $data
     * @param $structure
     * @return bool|string
     */
    public function install($data, $structure)
    {
        $this->db->beginTransaction(); $ps = 0;
        $id = $this->db->insert('plugins', $data);
        if($id > 0) {
            foreach ($structure as $k=>$components_id) {
                $ps += $this->db->insert('plugins_structure', array('plugins_id' => $id, 'components_id' => $components_id));
            }
        }
        if($id > 0 && $ps > 0){
            $this->db->commit();
        } else {
            $this->db->rollBack();
            $id=0;
        }

        return $id ;
    }

    /**
     * @param $id
     * @param $data
     * @param $structure
     * @return bool
     */
    public function updateData($id, $data, $structure)
    {
        $s = $this->db->update('plugins', $data, "id = {$id}");
        $s2=0;
        if($id > 0) {
            $this->db->delete('plugins_structure', "plugins_id={$id}");
            foreach ($structure as $k=>$components_id) {
                $s2 = $this->db->insert('plugins_structure', array('plugins_id' => $id, 'components_id' => $components_id));
            }
        }

        return $s > 0 || $s2 > 0 ;
    }

    public function uninstall($id)
    {
        return $this->db->delete('plugins', "id={$id} limit 1");
    }
    /**
     * get settings of controller
     * @param $name
     * @return array|mixed
     */
    public function getSettings($name){
        return $this->db->select("select settings from plugins where controller = '{$name}' limit 1")->row('settings');
    }

    /**
     * @param $controller
     * @param $data
     * @return bool
     */
    public function updateSettings($controller, $data){
        $settings = $this->db->select("
            select settings
            from plugins
            where controller = '{$controller}' limit 1
        ")->row('settings');
        $settings = unserialize($settings);

        foreach ($settings as $k=>$row) {
            if(isset($data[$row['name']])){
                $settings[$k]['value'] = $data[$row['name']];
            }
        }

        $settings = serialize($settings);

        return $this->db->update("plugins", array('settings'=>$settings), " controller = '{$controller}' limit 1");
    }

    /**
     * @param $id
     * @param string $key
     * @return array|mixed
     */
    public function getData($id, $key='*')
    {
        return $this->db->select("select {$key} from plugins where id = '{$id}' limit 1")->row($key);
    }

    /**
     * check if is installed plugin
     * @param $controller
     * @return bool
     */
    public function isInstalled($controller)
    {
        $controller = addslashes($controller);
        return $this->db->select("select id from plugins where controller = '{$controller}' limit 1")
            ->row('id');
    }

    /**
     * @param int $plugins_id
     * @return array|mixed
     */
    public function getStructure($plugins_id=0)
    {
        $join = ''; $sel='';
        if($plugins_id>0){
            $sel = ",IF(ps.id>0,'selected','') as selected";
            $join = "left join plugins_structure ps on ps.plugins_id={$plugins_id} and ps.components_id=s.id";
        }
        return $this->db->select("
            select s.id,i.name {$sel}
            from components s
            {$join}
            join components_info i on i.components_id=s.id and i.languages_id={$this->languages_id}
            where s.id<>1 and s.isfolder <> 1 and s.published=1
            order by i.name asc
        ")->all();
    }

    /**
     * @param $controller
     * @return $this
     */
    public function getStructurePlugins($controller)
    {
        return $this->db->select("
            select p.controller,p.position
            from components s
            join plugins_structure ps on ps.components_id=s.id
            join plugins p on p.id=ps.plugins_id
            where s.controller = '{$controller}'
        ")->all();
    }
}