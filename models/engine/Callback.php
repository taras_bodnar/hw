<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 26.06.14 13:54
 */

namespace models\engine;
use models\Engine;


defined('SYSPATH') or die();

/**
 * Class Callback
 * @package models\engine
 */
class Callback extends Engine {
    /**
     * insert data to table structure
     * @param $data
     * @param $info
     * @return int insert ID
     */
    public function create($data, $info=array())
    {
        return $this->createRow('callbacks', $data);
    }

    /**
     * @param $id
     * @param $data
     * @param $info
     * @return bool|string
     */
    public function update($id, $data, $info=array()){
        return $this->updateRow('callbacks', $id, $data);
    }

    public function delete($id)
    {
        return $this->db->delete("callbacks", "id={$id} limit 1");
    }

    /**
     * @param $id
     * @return array|mixed
     */
    public function getData($id)
    {
        return $this->db->select("
            select *
            from callbacks
            where id={$id}
        ")->row();
    }

}