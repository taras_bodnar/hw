<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 19.10.14 0:35
 */

namespace models\engine;

use models\Engine;

defined("SYSPATH") or die();

class Update extends Engine{

    public function create($data, $info){}
    public function update($id, $data, $info){}

    public function execSQL($data)
    {
        foreach ($data as $k=>$q) {
            $this->db->exec($q);
        }

        return 1;
    }

    /**
     * @param $version
     * @return bool
     */
    public function setVersion($version)
    {
        return $this->db->update("settings", array('value' => $version), "name = 'version' limit 1" );
    }
}