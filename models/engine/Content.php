<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 11.05.14 10:53
 */
namespace models\engine;

use models\Engine;
use controllers\core\DB;


if ( !defined('SYSPATH') ) die();

class Content extends Engine {

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * create page
     * @param $parent_id
     * @param $type_id
     * @param $owner_id
     * @param $templates_id
     * @return bool|string
     */
    public function autoCreate($parent_id, $type_id, $owner_id, $templates_id)
    {
        $this->db->delete("content", " auto=1 and owner_id={$owner_id}");
        return $this->db->insert(
            "content",
            array(
                'parent_id'    => (int)$parent_id,
                'type_id'      =>$type_id,
                'owner_id'     =>$owner_id,
                'templates_id' =>$templates_id,
                'published'    =>1,
                'createdon'    => date('Y-m-d H:i:s')
            )
        );
    }

    /**
     * insert data to table content
     * @param $data
     * @param $info
     * @return int insert ID
     */
    public function create($data, $info)
    {
        $this->db->beginTransaction();
        $r=0;
        $id = $this->db->insert("content", $data);

        if($id > 0) {
            foreach ($info as $languages_id =>$a) {
                if($this->db->insert("content_info",array(
                    'content_id' => $id,
                    'languages_id' => $languages_id,
                    'name'         => $info[$languages_id]['name'],
                    'alias'       => $info[$languages_id]['alias'],
                    'title'        => isset($info[$languages_id]['title']) ? $info[$languages_id]['title'] : $info[$languages_id]['name'],
                    'keywords'        => isset($info[$languages_id]['keywords']) ? $info[$languages_id]['keywords'] : '',
                    'case_title'        => isset($info[$languages_id]['case_title']) ? $info[$languages_id]['case_title'] : '',
                    'description'        => isset($info[$languages_id]['description']) ? $info[$languages_id]['description'] : '',
                    'content'        => isset($info[$languages_id]['content']) ? $info[$languages_id]['content'] : '',
                ))){
                    $r++;
                } else{
                    $this->error(DB::getErrorMessage());
                }
            }
        } else {
            $this->error(DB::getErrorMessage());
        }
        if($r > 0) {
            $this->db->commit();
        } else {
            $this->db->rollBack();
        }

        return $r > 0 ? $id : 0;
    }

    /**
     * @param $id
     * @param $data
     * @param $info
     * @return bool|string
     */
    public function update($id, $data, $info){
        $this->db->beginTransaction();
        $data['editedon'] = date('Y-m-d H:i:s');

        $r = $this->db->update("content", $data, "id={$id} limit 1");

        foreach ($info as $languages_id =>$a) {
            $aid = $this->db->select("select id
                                    from content_info
                                    where content_id=$id and languages_id=$languages_id
                                    limit 1
                                    ")->row('id');
            if(empty($aid)){
                $r += $this->db->insert(
                    "content_info",
                    array(
                        'content_id'=> $id,
                        'languages_id'=> $languages_id,
                        'name'        => $info[$languages_id]['name'],
                        'alias'       => $info[$languages_id]['alias'],

                        'title'        => isset($info[$languages_id]['title']) ? $info[$languages_id]['title'] : $info[$languages_id]['name'],
                        'keywords'     => isset($info[$languages_id]['keywords']) ? $info[$languages_id]['keywords'] : '',
                        'case_title'     => isset($info[$languages_id]['case_title']) ? $info[$languages_id]['case_title'] : '',
                        'description'  => isset($info[$languages_id]['description']) ? $info[$languages_id]['description'] : '',
                        'content'      => isset($info[$languages_id]['content']) ? $info[$languages_id]['content'] : '',

                    )
                );
            } else {
                $r += $this->db->update(
                    "content_info",
                    array(
                        'name'        => $info[$languages_id]['name'],
                        'alias'       => $info[$languages_id]['alias'],
                        'title'        => isset($info[$languages_id]['title']) ? $info[$languages_id]['title'] : $info[$languages_id]['name'],
                        'keywords'     => isset($info[$languages_id]['keywords']) ? $info[$languages_id]['keywords'] : '',
                        'case_title'     => isset($info[$languages_id]['case_title']) ? $info[$languages_id]['case_title'] : '',
                        'description'  => isset($info[$languages_id]['description']) ? $info[$languages_id]['description'] : '',
                        'content'      => isset($info[$languages_id]['content']) ? $info[$languages_id]['content'] : '',
                    )
                    ," id = {$aid} limit 1");
            }
        }

        if($r > 0) {
            $this->db->commit();
        } else {
            $this->db->rollBack();
        }

        return $r;
    }

    /**
     * delete item from content
     * @param $id
     * @return int
     */
    public function delete($id)
    {
        $r = $this->db->select("select id from content where parent_id={$id}")->all();
        if(!empty($r)) foreach ($r as $row) {
            $this->delete($row['id']);
        }

        $this->db->beginTransaction();
        $parent_id = $this->db->select("select parent_id from content where id={$id} limit 1")
            ->row('parent_id');
        if(
            $this->db->delete("content_info", " content_id = {$id}") &&
            $this->db->delete("content", " id = {$id} limit 1")
        ){
            if($parent_id > 0) {
                $t = $this->db->select("select count(id) as t from content where parent_id={$parent_id}")
                    ->row('t');
                if($t == 0) {
                    $this->db->update("content", array('isfolder' =>0) , "id = {$parent_id}");
                }
            }
            $this->db->commit();
        } else {
            $this->db->rollBack();
        }

        return 1;
    }
    public function info($id)
    {
        $columns = $this->db->showColumns('content_info');
//        echo '<pre>'; print_r($columns);echo '</pre>';
        $r = $this->db->select("
                                SELECT *
                                FROM content_info where content_id = {$id}
                                ")->all();
        $res=array();
        foreach ($r as $row) {
            foreach ($columns as $col) {
                $res[$row['languages_id']][$col['Field']] = $row[$col['Field']];
            }
        }
        return $res;
    }
    
    /**
     * switch pub status
     * @param $id
     * @param $published
     * @return bool
     */
    public function pub($id, $published)
    {
        return $this->db->update("content", array('published'=>$published) , " id = {$id} limit 1");
    }

    /**
     * change isfolder status
     * @param $id
     * @return bool
     */
    public function toggleFolder($id)
    {
        $isFolder = ($this->hasChildren($id)) ? 1 : 0;
        return $this->db->update("content", array('isfolder' => $isFolder) , " id = {$id} limit 1");
    }

    /**
     * check children count
     * @param $parent_id
     * @return bool
     */
    public function hasChildren($parent_id)
    {
        return $this->db->select("select count(id) as t from content where parent_id ={$parent_id}")->row('t') > 0;
    }

    /**
     * @param $parent_id
     * @return array|mixed
     */
    public function getChildren($parent_id)
    {
        return $this->db->select("select id from content where parent_id ={$parent_id}")->all();
    }

    /**
     * @param $parent_id
     * @param $type_id
     * @return array|mixed
     */
    public function tree($parent_id, $type_id)
    {
        return $this->db->select("select s.id,s.isfolder,i.name
                                from content s,content_info i
                                where s.type_id = {$type_id}
                                and s.parent_id = {$parent_id}
                                and i.content_id=s.id
                                and i.languages_id = {$this->languages_id}
                                order by abs(sort) asc")->all();
    }

    public function templates($type = 'page', $selected = 0)
    {
        return $this->db->select("
                    select c.id, c.name,c.id as value, IF(c.id=$selected, 'selected', '') as selected
                    from content_templates c
                    join content_type ct on ct.type = '{$type}' and ct.id = c.type_id
                    order by c.main desc, c.name asc
        ")->all();
    }

    /**
     * get default tepmplates id
     * @param string $type
     * @param $parent_id
     * @return array|mixed
     */
    public function defaultTemplatesId($type = 'page', $parent_id)
    {
        if($type == 'page'){
            // пеервірка на наслідування
            $r = $this->db->select("select ct.id, ct.follow from content_templates ct, content c where c.id={$parent_id} and c.templates_id=ct.id   ")->row();
            if($r['follow']){
                return $r['id'];
            }
        }
        return $this->db->select("
                    select c.id
                    from content_templates c
                    join content_type ct on ct.type = '{$type}' and ct.id = c.type_id
                    where c.main=1
        ")->row('id');
    }

    /**
     * @param $type
     * @return array|mixed
     */
    public function getTypeId($type)
    {
        return $this->db->select("
                    select id
                    from content_type
                    where type = '{$type}'
                    limit 1
        ")->row('id');
    }

    /**
     * витягую інформацію про тип сторінки
     * @param $content_id
     * @return array|mixed
     */
    public function getContentType($content_id)
    {
        return $this->db->select("
                    select t.type
                    from content_type t
                    join content c on c.id = {$content_id} and c.type_id=t.id
                    limit 1
        ")->row('type');
    }

    public function parentAlias($id,$languages_id)
    {
        return $this->db->select("select i.alias
                                  from content_info i
                                  where i.content_id={$id} and i.languages_id = {$languages_id}
                                  limit 1")->row('alias');

    }

    /**
     * @param $parent_id
     * @return array|mixed
     */
    public function categories($parent_id)
    {
        return $this->db->select("
            select c.id, i.name, c.isfolder
            from content c
            join content_type ct on ct.type='category'
            join content_info i on i.content_id=c.id and i.languages_id = {$this->languages_id}
            where c.parent_id={$parent_id} and c.type_id=ct.id
            order by abs(c.sort) asc, c.id asc
        ")->all();
    }



    /**
     * products manufacturers
     * @return array|mixed
     */
    public function manufacturers()
    {
        return $this->db->select("
            select c.id, i.name
            from content c
            join content_type ct on ct.type='manufacturer'
            join content_info i on i.content_id=c.id and i.languages_id = {$this->languages_id}
            where c.type_id=ct.id
            order by abs(c.sort) asc, c.id asc
        ")->all();
    }

    /**
     * витягує меню в якому позначка автоматично додавати елементи першого рівня
     * @return array|mixed
     */
    public function getNavMenus()
    {
        return $this->db->select("
            select m.id,m.name
            from nav_menu m
            order by m.name asc
            ")->all();
    }

    /**
     * видалити з меню
     * @param $content_id
     * @param $menu_id
     * @return int
     */
    public function deleteNavMenus($content_id, $menu_id)
    {
        return $this->db->delete("nav_menu_items", "content_id={$content_id} and nav_menu_id={$menu_id} limit 1");
    }

    /**
     * витягує відмічені пункти меню + якщо в мені стоґть позначка вато додавати сторінки, то
     * автоматино додає ці елементи як позначені. Тільки при створенні сторінки
     * @param $content_id
     * @param int $auto
     * @return array
     */
    public function selectedNavMenu($content_id, $auto=0)
    {
        $res=array();
        if($auto){
            $r = $this->db->select("select id from nav_menu where auto_add_pages = 1")->all();
            foreach ($r as $row) {
                $res[] = $row['id'];
            }
        }
        $r = $this->db->select("select nav_menu_id as id from nav_menu_items  where content_id={$content_id}")->all();

        foreach ($r as $row) {
            $res[] = $row['id'];
        }

        return $res;
    }

    /**
     * додати пункт меню
     * @param $nav_menu_id
     * @param $content_id
     * @return bool|string
     */
    public function addMenuItem($nav_menu_id,$content_id)
    {
        $sort = $this->db->select("select max(sort) as s from nav_menu_items where content_id = {$content_id}")->row('s');

        return $this->db->insert(
            "nav_menu_items",
            array(
                'nav_menu_id' => $nav_menu_id,
                'content_id'  => $content_id,
                'sort'        => ++ $sort
            )
        );
    }

    /**
     * get Row info
     * @param $content_id
     * @param $languages_id
     * @param string $key
     * @return array|mixed
     */
    public function getRowInfo($content_id, $languages_id, $key='*')
    {
        return $this->db->select("
        select {$key}
        from content_info
        where content_id={$content_id} and languages_id={$languages_id} limit 1")->row($key);
    }

    /**
     * @param $content_id
     * @param $alias
     * @return bool
     */
    public function updateAlias($content_id,$alias)
    {
        return $this->db->update(
            'content_info',
            array(
                'alias'=> $alias
            ),
            "content_id = {$content_id} limit 1"
        );
    }


    // features | розширені параметри

    public function getFeatures($content_id, $type, $in='')
    {
        $cid = $content_id;
        if(!empty($in)){
            $in = " and f.id in($in)";
        }

        if($type == 'product'){
            // витягну параметри які привязані до категорії
            $r = $this->db->select("select cid from products_categories where pid='{$content_id}' limit 1")->row('cid');
            if(!empty($r)){
                $cid=$r;
            }
        }
        $r = $this->db->select("
            select distinct f.id, IF(f.required, 'required','') required, i.name, f.type
            from features f
            join content_features cf on cf.content_id={$cid} and cf.features_id=f.id
            join features_info i on i.features_id=f.id and i.languages_id = {$this->languages_id}
            where f.auto = 0 {$in} and f.published=1 and f.extends=0
            order by abs(cf.sort) asc
        ")->all();

//        echo '<pre>'; print_r($r); echo '</pre>';

        if(in_array($type, array('category'))) return $r;

        $res = array();
        foreach ($r as $row) {
            switch($row['type']){
                case 'text' :
                case 'textarea' :
                case 'editor' :
                    $row['values'] =  $this->db->select("
                                    select l.id as languages_id, fcv.value, l.name as placeholder
                                    from languages l
                                    left join features_content_values fcv on fcv.content_id   = {$content_id}
                                                                         and fcv.features_id   = {$row['id']}
                                                                         and fcv.languages_id = l.id
                                    where l.front=1
                                    order by l.front_default desc
                                ")->all();
                break;
                case 'number':
                     $row['values'] =  $this->db->select("
                                    select value
                                    from features_content_values
                                    where content_id  = '{$content_id}'
                                         and features_id  = '{$row['id']}'
                                         and languages_id = 0
                                    limit 1
                                ")->row('value');
                    break;
                case 'checkbox':
                    $row['checked'] = $this->db->select("
                                    select value
                                    from features_content_values
                                    where content_id={$content_id} and features_id={$row['id']} and languages_id=0
                                ")->row('value') == 1 ? 'checked' : '';
                    break;
                case 'radiogroup':
                case 'checkboxgroup':
                case 'select':
                case 'sm':
                    $row['values'] = $this->db->select("
                        select v.id,i.value,IF(fcv.id > 0, 'checked', '') as checked
                        from features_values v
                        join features_values_info i on i.values_id=v.id and i.languages_id={$this->languages_id}
                        left join features_content_values fcv on fcv.features_id={$row['id']}
                                                             and fcv.content_id={$content_id}
                                                             and fcv.features_values_id=v.id
                                                             and fcv.languages_id=0
                        where v.features_id = {$row['id']}
                        order by abs(v.sort) asc
                        ")->all();
                    break;
            }
            $res[] = $row;
        }

        return $res;
    }

    public function getFeaturesValues($features_id)
    {

    }

    public function saveFeatures($content_id)
    {
        if(! isset($_POST['features'])) return 0;
//        echo '<pre>';print_r($_POST['features']);
        foreach ($_POST['features'] as $type => $a) {
            switch($type){
                case 'text':
                    foreach ($a as $features_id => $aa) {
                        foreach ($aa as $languages_id => $value) {
                            $id = $this->db->select("
                            select id from features_content_values
                            where content_id={$content_id}
                              and features_id='{$features_id}'
                              and languages_id='{$languages_id}'
                            ")->row('id');
                            if(empty($id)){
                                $this->db->insert(
                                    'features_content_values',
                                    array(
                                        'content_id'   => $content_id,
                                        'features_id'  => $features_id,
                                        'languages_id' => $languages_id,
                                        'value'        => $value
                                    )
                                );
                            }else {
                                $this->db->update('features_content_values', array('value'=>$value), " id={$id} limit 1");
                            }
                        }

                    }

                    break;
                case 'number':
                    foreach ($a as $features_id => $value) {
                        $id = $this->db->select("
                        select id from features_content_values
                        where content_id   = {$content_id}
                          and features_id  = '{$features_id}'
                          and languages_id = 0
                        ")->row('id');
                        if(empty($id)){
                            $this->db->insert(
                                'features_content_values',
                                array(
                                    'content_id'   => $content_id,
                                    'features_id'  => $features_id,
                                    'languages_id' => 0,
                                    'value'        => $value
                                )
                            );
                        }else {
                            $this->db->update('features_content_values', array('value'=>$value), " id={$id} limit 1");
                        }
                    }
                    break;
                case 'select':
                    foreach($a as $features_id => $features_values_id){
                        $this->db->delete(
                            'features_content_values',
                            "content_id={$content_id} and features_id='{$features_id}'
                            ");
                            $this->db->insert(
                                'features_content_values',
                                array(
                                    'content_id'         => $content_id,
                                    'features_id'        => $features_id,
                                    'features_values_id' => $features_values_id
                                )
                            );
                    }
                    break;
                case 'checkbox':
                    foreach($a as $features_id => $checked){
                        $id = $this->db->select("
                            select id from features_content_values
                            where content_id={$content_id}
                              and features_id='{$features_id}'
                            ")->row('id');
                        if(empty($id)){
                            if($checked == 1){
                                $this->db->insert(
                                    'features_content_values',
                                    array(
                                        'content_id'   => $content_id,
                                        'features_id'  => $features_id,
                                        'value'        => $checked
                                    )
                                );
                            }
                        } else if($id > 0 && $checked == 0) {
                            $this->db->delete('features_content_values', " id={$id} limit 1");
                        }
                    }
                    break;
                case 'sm':
                    foreach ($a as $features_id => $values) {
                        $this->db->delete(
                            'features_content_values',
                            "content_id={$content_id} and features_id='{$features_id}'
                            ");
                        foreach ($values as $k=>$features_values_id) {
                            $this->db->insert(
                                'features_content_values',
                                array(
                                    'content_id'         => $content_id,
                                    'features_id'        => $features_id,
                                    'features_values_id' => $features_values_id
                                )
                            );
                        }

                    }
                    break;
                case 'checkboxgroup':
                    foreach ($a as $features_id => $values) {
                        $this->db->delete(
                            'features_content_values',
                            "content_id={$content_id} and features_id='{$features_id}'
                            ");
                        foreach ($values as $features_values_id => $checked) {
                            if($checked == 1){
                                $this->db->insert(
                                    'features_content_values',
                                    array(
                                        'content_id'         => $content_id,
                                        'features_id'        => $features_id,
                                        'features_values_id' => $features_values_id
                                    )
                                );
                            }
                        }

                    }
                    break;
                case 'radiogroup':
                    foreach ($a as $features_id => $features_values_id) {
                        $id = $this->db->select("
                            select id from features_content_values
                            where content_id  = {$content_id}
                              and features_id = '{$features_id}'
                              and features_values_id='{$features_values_id}'
                            ")->row('id');
                        if(empty($id)){
                            $this->db->insert(
                                'features_content_values',
                                array(
                                    'content_id'         => $content_id,
                                    'features_id'        => $features_id,
                                    'features_values_id' => $features_values_id
                                )
                            );
                        } else {
                            $this->db->update(
                                'features_content_values',
                                array(
                                    'features_values_id' => $features_values_id
                                ),
                                " id={$id} limit 1"
                            );
                        }
                    }

                    break;
            }
        }

    }

    public function sortFeatures($content_id, $features_id, $sort)
    {
        return $this->db->update(
            'content_features',
            array('sort'=>$sort),
            " content_id={$content_id} and features_id='{$features_id}' limit 1"
        );
    }

    /**
     * @param $content_id
     * @param $features_id
     * @return int
     */
    public function removeFeatures($content_id, $features_id)
    {
        if($this->db->delete('content_features', " content_id={$content_id} and features_id='{$features_id}' limit 1")){
            $this->db->delete('features_content_values', " content_id={$content_id} and features_id='{$features_id}'");
        }
        return 1;
    }
    public function pickFeatures($content_id)
    {
        $type = $this->getContentType($content_id);

        if($type == 'product'){
            // витягну параметри які привязані до категорії
            $r = $this->db->select("select cid from products_categories where pid='{$content_id}' limit 1")->row('cid');
            if(!empty($r)){
                $content_id = $r;
            }
        }
        $in = array();
        $r = $this->db->select("select features_id from content_features where content_id={$content_id}")->all();
        foreach ($r as $row) {
            $in[] = $row['features_id'];
        }
        $out=array();

        foreach ($_POST['content_features'] as $k=>$features_id) {
            if(!in_array($features_id, $in)){
               $this->db->insert('content_features', array('content_id'=>$content_id, 'features_id' => $features_id));
               $out[] = $features_id;
            }
        }

        return $out;
    }

    public function getFeaturesPicklist($content_id)
    {
        $type = $this->getContentType($content_id);

        if($type == 'product'){
            // витягну параметри які привязані до категорії
            $r = $this->db->select("select cid from products_categories where pid='{$content_id}' limit 1")->row('cid');
            if(!empty($r)){
                $content_id = $r;
            }
        }

        return $this->db->select("
          select f.id,i.name,if(cf.id>0, 'selected', '') as selected
          from features f
          join features_info i on i.features_id=f.id and i.languages_id = {$this->languages_id}
          left join content_features cf on cf.content_id={$content_id} and cf.features_id=f.id
          where f.published=1 and f.auto=0
        ")->all();
    }

    /**
     * @param $pid
     * @return array|mixed
     */
    public function getProductsCategory($pid)
    {
        return $this->db->select("select cid from products_categories where pid='{$pid}' limit 1")->row('cid');
    }
}