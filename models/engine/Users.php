<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 26.06.14 13:54
 */

namespace models\engine;
use models\Engine;
 
use controllers\core\DB;

defined('SYSPATH') or die();

class Users extends Engine {
    private $rang = array(100,999);

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param $min
     * @param $max
     * @return $this
     */
    public function setRang($min,$max)
    {
        $this->rang = array($min, $max);
        return $this;
    }

    /**
     * insert data to table structure
     * @param $data
     * @param $info
     * @return int insert ID
     */
    public function create($data, $info=array())
    {
        $this->db->beginTransaction();
        $id = $this->db->insert("users", $data);

        if($id > 0) {
            $this->db->commit();
        } else {
            $this->error(DB::getErrorMessage());
            $this->db->rollBack();
        }

        return $id;
    }

    /**
     * @param $id
     * @param $data
     * @param $info
     * @return bool|string
     */
    public function update($id, $data, $info=array()){
        return $this->updateRow('users', $id, $data);
    }

    /**
     * delete item from structure
     * @param $id
     * @return int
     */
    public function delete($id)
    {
        $this->db->beginTransaction();

        if($this->db->delete("users", " id = {$id} limit 1")){
            $this->db->commit();
        } else {
            $this->db->rollBack();
        }

        return 1;
    }


    /**
     * parents list
     * @param int $selected_id
     * @return array|mixed
     */
    public function group($selected_id = 0,$uid=0)
    {
        return $this->db->select("SELECT s.id as value, i.name, IF(u.id>{$selected_id}, 'selected', '') as selected
                                FROM users_group s
                                JOIN users_group_info i ON
                                i.users_group_id = s.id AND i.languages_id = {$this->languages_id}
                                 left join users u on s.id=u.users_group_id and u.id={$uid}
                                WHERE s.rang between {$this->rang[0]} and {$this->rang[1]}
                                ORDER BY ABS(s.id) ASC
                                ")->all();
    }

    /**
     * users languages
     * @param int $selected_id
     * @return array|mixed
     */
    public function languages($selected_id = 0)
    {
        return $this->db->select("SELECT s.id as value, s.name, IF(s.id={$selected_id}, 'selected', '') as selected
                                FROM languages s
                                ORDER BY ABS(s.id) ASC
                                ")->all();
    }
    /**
     * change isfolder status
     * @param $id
     * @return bool
     */
    public function toggleFolder($id)
    {
        $isFolder = ($this->hasChildren($id)) ? 1 : 0;
        return $this->db->update("users_group", array('isfolder' => $isFolder) , " id = {$id} limit 1");
    }

    /**
     * check children count
     * @param $parent_id
     * @return bool
     */
    public function hasChildren($parent_id)
    {
        return $this->db->select("select count(id) as t from users_group where parent_id ={$parent_id}")->row('t') > 0;
    }


    /**
     * users group tree
     * @param $parent_id
     * @return array
     */
    public function tree($parent_id)
    {
        return $this->db->select("select s.id,i.name, (select count(t.id) from users_group t where t.parent_id =s.id) as isfolder
                                from users_group s,users_group_info i
                                where s.parent_id = {$parent_id} and s.rang between {$this->rang[0]} and {$this->rang[1]} and i.users_group_id=s.id and i.languages_id = {$this->languages_id}
                                order by abs(s.sort) asc")->all();
    }
} 