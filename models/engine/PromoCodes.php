<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 05.07.14 20:34
 */

namespace models\engine;
use models\Engine;

defined('SYSPATH') or die();

class PromoCodes extends Engine {

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * insert data to table structure
     * @param $data
     * @param $info
     * @return int insert ID
     */
    public function create($data, $info=array())
    {
        return $this->createRow('promo_codes', $data);
    }

    /**
     * @param $id
     * @param $data
     * @param $info
     * @return bool|string
     */
    public function update($id, $data, $info=array()){
        return $this->updateRow('promo_codes', $id, $data);
    }

    /**
     * delete item from structure
     * @param $id
     * @return int
     */
    public function delete($id)
    {
        return $this->deleteRow('promo_codes', $id);
    }

    public function getData($id)
    {
        return $this->db->select("select * from promo_codes where id={$id}")->row();
    }
    /**
     * get headers
     * @return array
     */
    public function getType()
    {
        return $this->db->enumValues('promo_codes','type');
    }

    /**
     * @return array|mixed
     */
    public function getDefaultCurrency()
    {
        return $this->db->select("select id,symbol from currency where is_main=1")->row();
    }
} 