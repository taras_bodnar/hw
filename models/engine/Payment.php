<?php
/**
 * Company Otakoyi.com
 * Author: wg
 * Date: 11.01.15 0:05
 */

namespace models\engine;

use controllers\core\DB;
use models\Engine;

defined("SYSPATH") or die();

class Payment extends Engine {

    public function create($data, $info)
    {
        $this->db->beginTransaction();
        $r=0;
        $id = $this->db->insert("payment", $data);

        if($id > 0) {
            foreach ($info as $languages_id =>$a) {
                if($this->db->insert("payment_info",array(
                    'payment_id' => $id,
                    'languages_id' => $languages_id,
                    'name'         => $info[$languages_id]['name'],
                    'description'  => $info[$languages_id]['description']
                ))){
                    $r++;
                } else{
                    $this->error(DB::getErrorMessage());
                }
            }
        } else {
            $this->error(DB::getErrorMessage());
        }
        if($r > 0) {
            $this->db->commit();
        } else {
            $this->db->rollBack();
        }

        return $r;
    }

    /**
     * @param $id
     * @param $data
     * @param $info
     * @return bool|string
     */
    public function update($id, $data, $info){
        $this->db->beginTransaction();
        $r = $this->db->update("payment", $data, "id={$id} limit 1");

        foreach ($info as $languages_id =>$a) {
            $aid = $this->db->select("
                      select id
                      from payment_info
                      where payment_id=$id and languages_id=$languages_id
                      limit 1
                     ")->row('id');
            if(empty($aid)){
                $r += $this->db->insert(
                    "payment_info",
                    array(
                        'payment_id'=> $id,
                        'languages_id'=> $languages_id,
                        'name'        => $info[$languages_id]['name'],
                        'description' => $info[$languages_id]['description']
                    )
                );
            } else {
                $r += $this->db->update(
                    "payment_info",
                    array(
                        'name'        => $info[$languages_id]['name'],
                        'description' => $info[$languages_id]['description']
                    )
                    ," id = {$aid} limit 1");
            }
        }

        if($r > 0) {
            $this->db->commit();
        } else {
            $this->db->rollBack();
        }

        return $r;
    }

    public function delete($id)
    {
        return $this->db->delete("payment", " id = '{$id}' limit 1");
    }

    public function pub($id,$pub)
    {
        return $this->db->update('payment', array('published'=>$pub), "id=$id limit 1");
    }

    /**
     * @param $content_id
     * @return array|mixed
     */
    public function get($content_id)
    {
        return $this->db->select("select *, DATE_FORMAT(createdon, '%d.%m.%y') as dt, DATE_FORMAT(createdon, '%h:%i') as hi
                                  from payment
                                  where content_id={$content_id}
                                  order by id desc
                                  ")->all();
    }

    /**
     * @param $id
     * @return array|mixed
     */
    public function params($id)
    {
        return $this->data('payment', $id);
    }

    /**
     * @param $id
     * @return array
     */
    public function info($id)
    {
        $r = $this->db->select("
                                SELECT *
                                FROM payment_info where payment_id = {$id}
                                ")->all();
        $res=array();
        foreach ($r as $row) {
            $res[$row['languages_id']]['name'] = $row['name'];
            $res[$row['languages_id']]['description'] = $row['description'];
        }
        return $res;
    }

    /**
     * @param $payment_id
     * @return array|mixed
     */
    public function getDelivery($payment_id)
    {
        return $this->db->select("
        select d.id, i.name, IF(dp.id > 0, 'selected', '') as selected
        from delivery d
        join delivery_info i on i.delivery_id=d.id and i.languages_id= {$this->languages_id}
        left join delivery_payment dp on dp.payment_id={$payment_id} and dp.delivery_id=d.id
        order by abs(dp.sort) asc, d.id asc
        ")->all();
    }

    /**
     * @param $payment_id
     * @param $data
     */
    public function setDelivery($payment_id, $data)
    {
        $this->db->delete("delivery_payment", " payment_id={$payment_id}");
        foreach ($data as $k=>$delivery_id) {
            $this->db->insert(
                "delivery_payment",
                array(
                    'delivery_id' => $delivery_id,
                    'payment_id'  => $payment_id,
                    'sort'        => $k
                )
            );
        }

    }
    /**
     * @param $delivery_id
     * @param $payment_id
     * @param $sort
     * @return bool
     */
    public function updateSort($payment_id, $delivery_id, $sort)
    {
        return $this->db->update(
            'delivery_payment',
            array('sort'=> $sort),
            " delivery_id={$delivery_id} and payment_id={$payment_id} limit 1"
        );
    }

    /**
     * @return array|mixed
     */
    public function getCurrency()
    {
        return $this->db->select("select id, name from currency order by is_main desc, name asc")->all();
    }

    /**
     * @param $id
     * @return array|mixed
     */
    public function getSettings($id)
    {
        return $this->db->select("select settings from payment where id={$id}")->row('settings');
    }

} 