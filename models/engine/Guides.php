<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 26.06.14 13:54
 */

namespace models\engine;
use models\Engine;
use controllers\core\DB;


defined('SYSPATH') or die();

class Guides extends Engine {

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * insert data to table structure
     * @param $data
     * @param $info
     * @return int insert ID
     */
    public function create($data, $info)
    {
        $this->db->beginTransaction();
        $r=0;
        $id = $this->db->insert("guides", $data);

        if($id > 0) {
            foreach ($info as $languages_id =>$a) {
                if($this->db->insert("guides_info",array(
                    'guides_id' => $id,
                    'languages_id' => $languages_id,
                    'name'         => $info[$languages_id]['name']
                ))){
                    $r++;
                } else{
                    $this->error(DB::getErrorMessage());
                }
            }
        } else {
            $this->error(DB::getErrorMessage());
        }
        if($r > 0) {
            $this->db->commit();
        } else {
            $this->db->rollBack();
        }

        return $r;
    }

    /**
     * @param $id
     * @param $data
     * @param $info
     * @return bool|string
     */
    public function update($id, $data, $info){
        $this->db->beginTransaction();

        $r = $this->db->update("guides", $data, "id={$id} limit 1");

        foreach ($info as $languages_id =>$a) {
            $aid = $this->db->select("select id
                                    from guides_info
                                    where guides_id=$id and languages_id=$languages_id
                                    limit 1
                                    ")->row('id');
            if(empty($aid)){
                $r += $this->db->insert(
                    "guides_info",
                    array(
                        'guides_id'=> $id,
                        'languages_id'=> $languages_id,
                        'name'        => $info[$languages_id]['name']
                    )
                );
            } else {
                $r += $this->db->update(
                    "guides_info",
                    array(
                        'name'        => $info[$languages_id]['name']
                    )
                    ," id = {$aid} limit 1");
            }
        }

        if($r > 0) {
            $this->db->commit();
        } else {
            $this->db->rollBack();
        }

        return $r;
    }

    /**
     * delete item from structure
     * @param $id
     * @return int
     */
    public function delete($id)
    {
        $r = $this->db->select("select id from guides where parent_id={$id}")->all();
        if(!empty($r)) foreach ($r as $row) {
            $this->delete($row['id']);
        }

        $this->db->beginTransaction();

        if(
            $this->db->delete("guides_info", " guides_id = {$id}") &&
            $this->db->delete("guides", " id = {$id} limit 1")
        ){
            $this->db->commit();
        } else {
            $this->db->rollBack();
        }

        return 1;
    }

    /**
     * @param $id
     * @return array
     */
    public function info($id)
    {
        $r = $this->db->select("SELECT * FROM guides_info where guides_id = {$id}")->all();
        $res=array();
        foreach ($r as $row) {
            $res[$row['languages_id']]['name'] = $row['name'];
        }
        return $res;
    }
} 