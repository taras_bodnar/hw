<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 26.06.14 13:54
 */

namespace models\engine;
use models\Engine;
use controllers\core\DB;


defined('SYSPATH') or die();

class Currency extends Engine {

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * insert data to table currency
     * @param $data
     * @param $info
     * @return int insert ID
     */
    public function create($data, $info=array())
    {
        $id = $this->db->insert("currency", $data);
        $this->error(DB::getErrorMessage());
        return $id;
    }

    /**
     * @param $id
     * @param $data
     * @param $info
     * @return bool|string
     */
    public function update($id, $data, $info=array()){
        $r = $this->db->update("currency", $data, "id={$id} limit 1");
        return $r;
    }

    /**
     * @param $id
     * @param $s
     * @return bool
     */
    public function changeMain($id, $s)
    {
        $this->db->update("currency", array('is_main' => 0), " id <> '{$id}'");
        return $this->db->update("currency", array('is_main' => $s), " id='{$id}' limit 1");
    }

    /**
     * delete item from currency
     * @param $id
     * @return int
     */
    public function delete($id)
    {
        return $this->db->delete('currency', "id={$id} limit 1");
    }

    /**
     * @return array|mixed
     */
    public function get()
    {
        return $this->db->select("select * from currency order by is_main desc,id asc")->all();
    }

    /**
     * @param $id
     * @param $rate
     * @return bool
     */
    public function updateRate($id, $rate)
    {
        return $this->db->update("currency", array('rate' => $rate), " id='{$id}' limit 1");
    }
} 