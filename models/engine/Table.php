<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 11.05.14 10:53
 */
namespace models\engine;
use models\Engine;
use controllers\core\DB;


if ( !defined('SYSPATH') ) die();

/**
 * Class Table working vs datatables
 * @package models\engine
 */
class Table extends Engine {
    private $debug = false;

    public function __construct()
    {
        parent::__construct();
    }

    public function debug($status)
    {
        $this->debug = $status;
        return $this;
    }

    /**
     * insert data to table structure
     * @param $data
     * @param $info
     * @return int insert ID
     */
    public function create($data, $info)
    {
    }

    /**
     * @param $id
     * @param $data
     * @param $info
     * @return bool|string
     */
    public function update($id, $data, $info)
    {
    }

    /**
     * @param $id
     * @return int
     */
    public function delete($id)
    {
    }

    /**
     * sortable function
     * @param $table table name
     * @param $pk string primary key
     * @param $col string sorting column
     * @param $i int sorting index
     * @param $id int column index
     * @return bool
     */
    public function sort($table, $pk, $col, $i, $id)
    {
        return $this->db->update($table, array($col=>$i), " $pk =$id limit 1", $this->debug);
    }

    /**
     * @param array $columns list of columns : array('col1','col2')
     * @param string $table
     * @param array $join list of joins
     * @param string $where
     * @param $order
     * @param $limit
     * @return array|mixed
     */
    public function rows(array $columns, $table, array $join, $where ='', $order, $limit)
    {
        $columns = implode(', ', $columns);

        if(empty($join)) {
            $join='';
        } else {
            $join = implode('
            ', $join );
        }

        $r = $this->db->select(" SELECT SQL_CALC_FOUND_ROWS
                                   {$columns}
                                   FROM {$table}
                                   {$join}
                                   {$where}
                                   {$order}
                                   {$limit}
                                ", $this->debug)->all();
        if(!$r){
            echo DB::getErrorMessage();
        }
        return $r;
    }
    public function foundRows()
    {
        return $this->db->select("SELECT FOUND_ROWS() as total", $this->debug)->row('total');
    }

    /**
     * @param $table
     * @param array $join
     * @param string $where
     * @return array|mixed
     */
    public function total($table, array $join, $where ='')
    {
        if(empty($join)) {
            $join='';
        } else {
            $join = implode('
            ', $join );
        }
        return $this->db->select("
        SELECT COUNT(*) as total
        from {$table}
        {$join}
        {$where}
        ", $this->debug)->row('total');
    }
}