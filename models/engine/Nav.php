<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 26.06.14 13:54
 */

namespace models\engine;
use models\Engine;

defined('SYSPATH') or die();

class Nav extends Engine {

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * insert data to table structure
     * @param $data
     * @param $info
     * @return int insert ID
     */
    public function create($data, $info=array())
    {
        return $this->createRow('nav_menu', $data);
    }

    /**
     * @param $id
     * @param $data
     * @param $info
     * @return bool|string
     */
    public function update($id, $data, $info=array()){
        return $this->updateRow('nav_menu', $id, $data);
    }

    /**
     * delete item from structure
     * @param $id
     * @return int
     */
    public function delete($id)
    {
        return $this->deleteRow('nav_menu', $id);
    }

    /**
     * виводить масив сторінок
     * @param int $nav_menu_id
     * @return array|mixed
     */
    public function getPages($nav_menu_id = 0)
    {
       return $this->db->select("
               select c.id,CONCAT('#', c.id ,' ', i.name) as name,IF(m.id > 0 , 'selected' , '') as selected,
                      t.type
               from content c
               left join nav_menu_items m on m.nav_menu_id={$nav_menu_id} and m.content_id=c.id
               join content_type t on t.type in ('page','category')
               join content_info i on i.content_id=c.id and i.languages_id={$this->languages_id}
               where c.published=1 and c.auto=0 and c.type_id=t.id
               order by abs(m.sort) asc
        ")->all();
    }

    public function addItem($nav_menu_id, $content_id, $sort)
    {
        return $this->createRow('nav_menu_items', array(
            'nav_menu_id' => $nav_menu_id,
            'content_id'  => $content_id,
            'sort'        => $sort
        ));
    }
    /**
     * очищу вміст меню
     * @param $nav_menu_id
     * @return int
     */
    public function deleteItems($nav_menu_id)
    {
        return $this->db->delete("nav_menu_items", " nav_menu_id={$nav_menu_id}");
    }

    public function sort($nav_id,$content_id, $i)
    {
        return $this->db->update(
            "nav_menu_items",
            array('sort'=>$i),
            "nav_menu_id=$nav_id and content_id = $content_id"
        );
    }
}