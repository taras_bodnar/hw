<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 05.07.14 20:34
 */

namespace models\engine;
use models\Engine;

defined('SYSPATH') or die();

class Settings extends Engine {

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * get settings
     * @return array|mixed
     */
    public function get()
    {
        return $this->db->select("select * from settings order by id asc")->all();
    }

    public function set($name, $value)
    {
        return $this->db->update("settings", array('value' => $value), " name = '{$name}' limit 1");
    }


    /**
     * insert data to table structure
     * @param $data
     * @param $info
     * @return int insert ID
     */
    public function create($data, $info=array())
    {
//        return $this->createRow('tbl', $data);
    }

    /**
     * @param $id
     * @param $data
     * @param $info
     * @return bool|string
     */
    public function update($id, $data, $info=array()){
//        return $this->updateRow('tbl', $id, $data);
    }

} 