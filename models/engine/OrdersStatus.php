<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 26.06.14 13:54
 */

namespace models\engine;
use models\Engine;
use controllers\core\DB;


defined('SYSPATH') or die();

/**
 * Class OrdersStatus
 * @package models\engine
 */
class OrdersStatus extends Engine {

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param $data
     * @param $info
     * @return int
     */
    public function create($data, $info)
    {
        $this->db->beginTransaction();
        $r=0;
        $id = $this->db->insert("orders_status", $data);

        if($id > 0) {
            foreach ($info as $languages_id =>$a) {
                if($this->db->insert("orders_status_info",array(
                    'orders_status_id' => $id,
                    'languages_id'     => $languages_id,
                    'name'             => $info[$languages_id]['name']
                ))){
                    $r++;
                } else{
                    $this->error(DB::getErrorMessage());
                }
            }
        } else {
            $this->error(DB::getErrorMessage());
        }
        if($r > 0) {
            $this->db->commit();
        } else {
            $this->db->rollBack();
        }

        return $r;
    }

    /**
     * @param $id
     * @param $data
     * @param $info
     * @return bool|string
     */
    public function update($id, $data, $info){

        $this->db->beginTransaction();
        $r = $this->db->update("orders_status", $data, "id={$id} limit 1");

        foreach ($info as $languages_id =>$a) {
            $aid = $this->db->select("
                      select id
                      from orders_status_info
                      where orders_status_id=$id and languages_id=$languages_id
                      limit 1
                     ")->row('id');
            if(empty($aid)){
                $r += $this->db->insert(
                    "orders_status_info",
                    array(
                        'orders_status_id'=> $id,
                        'languages_id'    => $languages_id,
                        'name'            => $info[$languages_id]['name']
                    )
                );
            } else {
                $r += $this->db->update(
                    "orders_status_info",
                    array(
                        'name'        => $info[$languages_id]['name']
                    )
                    ," id = {$aid} limit 1");
            }
        }

        if($r > 0) {
            $this->db->commit();
        } else {
            $this->db->rollBack();
        }

        return $r;
    }

    /**
     * delete item from OrdersStatus
     * @param $id
     * @return int
     */
    public function delete($id)
    {
        return $this->db->delete('orders_status', "id={$id} limit 1");
    }

    /**
     * @return array|mixed
     */
    public function get()
    {
        return $this->db->select("select * from orders_status order by is_main desc,id asc")->all();
    }

    /**
     * @param $id
     * @return array
     */
    public function getInfo($id)
    {
        $r = $this->db->select("SELECT languages_id, name FROM orders_status_info where orders_status_id = {$id}")->all();
        $res=array();
        foreach ($r as $row) {
            $res[$row['languages_id']]['name'] = $row['name'];
        }
        return $res;
    }
} 