<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 26.06.14 13:54
 */

namespace models\engine;
use models\Engine;
use controllers\core\DB;


defined('SYSPATH') or die();

class Letters extends Engine {

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * insert data to table structure
     * @param $data
     * @param $info
     * @return int insert ID
     */
    public function create($data, $info=array())
    {
        $l = $this->db->insert("letters",$data);
        if ($l>0) {
            foreach ($info as $item) {
              if(!empty($item)) {
                  $this->db->insert("letters_emails",
                      array(
                          'email'=>$item,
                          'letters_id'=>$l
                      )
                      );
              }
            }
        }

        return $l>0;
    }

    /**
     * @param $id
     * @param $data
     * @param $info
     * @return bool|string
     */
    public function update($id, $data, $info=array())
    {

            $l = $this->db->update("letters",$data," id={$id}");

        if($l) {
            $this->db->delete("letters_emails"," letters_id={$id}");
            foreach ($info as $item) {
                if(!empty($item)) {
                    $this->db->insert("letters_emails",
                        array(
                            'email'=>$item,
                            'letters_id'=>$id
                        )
                  );
                }
            }
        }

        return $l>0;
    }


    /**
     * delete item from structure
     * @param $id
     * @return int
     */
    public function delete($id)
    {
        return   $this->db->delete("letters", " id = {$id} limit 1");
    }


    public function getEmails()
    {
        $w = $this->getWhere();
        $h = $this->getHaving();
        $join = $this->getJoin();
        $h = !empty($h)?"HAVING {$h}":"";
        return $this->db->select("
              select u.id,u.email as name from users u
              {$join}
              where u.id>0 {$w}
              GROUP BY u.id
              {$h}
              ORDER BY u.id desc
              ")->all();
    }

    public function getClientGroup()
    {
        return $this->db->select("
              select ug.id,ugi.name from users_group ug
               join users_group_info ugi on ug.id=ugi.users_group_id and languages_id = {$this->languages_id}
                where ug.id<>1
               "
        )->all();
    }

    public function getEmailsByGroupID($id)
    {
        return $this->db->select("
              select id,email as name from users where users_group_id in ({$id})"
        )->all();
    }

    public function getEmailEdit($id)
    {
        return $this->db->select("
              select email as name from letters_emails where letters_id={$id}"
        )->all();
    }

    public function getTotalEmail($id)
    {
        return $this->db->select("
                Select count(id) as t from letters_emails where letters_id = {$id}
        ")->row("t");
    }

    public function getData($id)
    {
        return $this->db->select("
                Select * from letters where id={$id}
        ")->row();
    }

    public function getEmailSend($id,$start)
    {
        return $this->db->select("
              select id,email as name from letters_emails where letters_id={$id}
              limit {$start},1
              "
        )->row();
    }

    public function updateSendStatus($id)
    {
        return $this->db->update("letters_emails",
            array('status'=>1)," id={$id}"
        );
    }

    public function updateLstatus($id)
    {
        return $this->db->update("letters",
            array('status'=>1)," id={$id}"
        );
    }

    public function getProjects()
    {
        return $this->db->select("select c.id,i.name,'projects' as type from content c
          join content_info i on i.content_id=c.id and i.languages_id={$this->languages_id}
          where c.published=1 and c.type_id in(12,14)
        ")->all();
    }

    public function getEmailsByProjectsID($id)
    {
        return $this->db->select("
              select u.id,email as name from users u
               join projects_users p on p.users_id=u.id
               where p.content_id in ({$id})"
        )->all();
    }

    public function getSubscribes()
    {
        return $this->db->select("select id,email as name from users where subscription=1")->all();
    }

    private $join = array();
    private $where = array();
    private $having = array();
    private $limit = '';

    /**
     * @param $q
     * @return $this
     */
    public function setHaving($q)
    {
        $this->having[] = $q;

        return $this;
    }

    /**
     * @return string
     */
    private function getHaving()
    {
        $w = '';
        if(!empty($this->having)){
            $w = implode(' ', $this->having);
        }
        return $w;
    }

    /**
     * @param $q
     * @return $this
     */
    public function setWhere($q)
    {
        $this->where[] = $q;

        return $this;
    }

    /**
     * @return string
     */
    private function getWhere()
    {
        $w = '';
        if(!empty($this->where)){
            $w = implode(' ', $this->where);
        }
        return $w;
    }
    /**
     * @param $q
     * @return $this
     */
    public function setJoin($join)
    {
        $this->join[] = $join;

        return $this;
    }

    /**
     * @return string
     */
    private function getJoin()
    {
        $o = '';
        if(!empty($this->join)){
            $o = implode(' ', $this->join);
        }
        return $o;
    }

    /**
     * @param $start
     * @param $num
     * @return $this
     */
    public function setLimit($start, $num)
    {
        $this->limit = "limit {$start}, {$num}";

        return $this;
    }

}