<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 05.07.14 20:34
 */

namespace models\engine\content;

use models\Engine;

defined('SYSPATH') or die();

class Chunks extends Engine {

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * insert data to table structure
     * @param $data
     * @param $info
     * @return int insert ID
     */
    public function create($data, $info=array())
    {
        return $this->createRow('chunks', $data);
    }

    /**
     * @param $id
     * @param $data
     * @param $info
     * @return bool|string
     */
    public function update($id, $data, $info=array()){
        return $this->updateRow('chunks', $id, $data);
    }

    /**
     * delete item from structure
     * @param $id
     * @return int
     */
    public function delete($id)
    {
        return $this->deleteRow('chunks', $id);
    }

    public function filePath($id)
    {
        return $this->db->select("select path from chunks where id=$id limit 1")->row('path');
    }
} 