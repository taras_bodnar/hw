<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 26.06.14 13:54
 */

namespace models\engine\content\images;

use models\Engine;

defined('SYSPATH') or die();

class Sizes extends Engine {

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * insert data to table structure
     * @param $data
     * @param $info
     * @return int insert ID
     */
    public function create($data, $info=array())
    {
        return $this->createRow('images_sizes', $data);
    }

    /**
     * @param $id
     * @param $data
     * @param $info
     * @return bool|string
     */
    public function update($id, $data, $info=array()){
        return $this->updateRow('images_sizes', $id, $data);
    }

    /**
     * delete item from structure
     * @param $id
     * @return int
     */
    public function delete($id)
    {
        return $this->deleteRow('images_sizes', $id);
    }

    public function clearSelectedType($images_sizes_id)
    {
        return $this->db->delete("content_type_images","images_sizes_id={$images_sizes_id}");
    }
    public function addSelectedType($id,$type_id)
    {
        return $this->db->insert("content_type_images", array(
            'content_type_id' => $type_id,
            'images_sizes_id' =>$id
        ));
    }
    public function getSelectedType($images_sizes_id)
    {
        return $this->db->select("select content_type_id as id
                                  from content_type_images
                                  where images_sizes_id={$images_sizes_id}
                                  ")->all();
    }

    public function getTypes()
    {
        return $this->db->select("select id,CONCAT(name , ' (' ,type, ')') as name
                                  from content_type
                                  order by name asc
                                  ")->all();
    }

    /**
     * @param $id
     * @return array|mixed
     */
    public function getTotalPages($id)
    {
        return $this->db->select("
            select count(c.id) as t
            from content c
            join content_type_images cti on cti.images_sizes_id={$id} and cti.content_type_id=c.type_id
        ")->row('t');
    }

    /**
     * @param $sizes_id
     * @param int $start
     * @return array|mixed
     */
    public function getPageImages($sizes_id, $start=0)
    {
        $r =  $this->db->select("
            select c.id
            from content c
            join content_type_images cti on cti.images_sizes_id={$sizes_id} and cti.content_type_id=c.type_id
            order by abs(c.id) asc limit {$start}, 1
        ")->row();

        if(empty($r)) return $r;

        $r['images'] = $this->db->select("
                            select name as src
                            from content_images where content_id={$r['id']}
                        ")->all();

        return $r;
    }

    /**
     * return size name
     * @param $id
     * @return array|mixed
     */
    public function getSizeData($id)
    {
        return $this->db->select("select name,width,height from images_sizes where id = {$id} limit 1")->row();
    }
}