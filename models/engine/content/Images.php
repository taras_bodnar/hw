<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 26.06.14 13:54
 */

namespace models\engine\content;

use models\Engine;

defined('SYSPATH') or die();

class Images extends Engine {

    public function __construct()
    {
        parent::__construct();
    }
    /**
     * get content image
     * @param $content_id
     * @param null $size
     * @return array|mixed
     */
    public function cover($content_id, $size = null)
    {
        $r = $this->db->select("select i.name as src, ii.alt
                                  from content_images i
                                  left join content_images_info ii on
                                                                    ii.images_id = i.id and
                                                                    ii.languages_id = '{$this->languages_id}'
                                  where i.content_id = '{$content_id}'
                                  order by abs(i.sort) asc
                                  limit 1
                                  ")->row();
        if($size && !empty($r)){
            $r['src'] = APPURL . "uploads/content/{$content_id}/{$size}/{$r['src']}";
        }
        return $r;
    }

    /**
     * insert data to table
     * @param $data
     * @param $info
     * @return int insert ID
     */
    public function create($data, $info=array())
    {
        return $this->createRow('content_images', $data);
    }

    /**
     * @param $id
     * @param $data
     * @param $info
     * @return bool|string
     */
    public function update($id, $data, $info=array()){
        $r=0;
        foreach ($data as $languages_id =>$field) {

            $aid = $this->db->select("select id
                                    from content_images_info
                                    where images_id={$id} and languages_id=$languages_id
                                    limit 1
                                    ")->row('id');
            if(empty($aid)){
                $r += $this->db->insert(
                    "content_images_info",
                    array(
                        'images_id'=> $id,
                        'languages_id'=> $languages_id,
                        'alt'        => $data[$languages_id]['alt']
                    )
                );
            } else {
                $r += $this->db->update(
                    "content_images_info",
                    array(
                        'alt'        => $data[$languages_id]['alt']
                    )
                    ," id = {$aid} limit 1");
            }
        }

        return $r;
    }

    /**
     * delete item from structure
     * @param $id
     * @return int
     */
    public function delete($id)
    {
        return $this->deleteRow('content_images', $id);
    }

    public function getSizes($content_id)
    {
        return $this->db->select("select s.*,s.id as value
                                  from images_sizes s
                                  join content c on c.id={$content_id}
                                  join content_type_images t on t.images_sizes_id=s.id and c.type_id = t.content_type_id
                                ")->all();
    }

    public function imgName($content_id)
    {
        return $this->db->select("select name
                                from content_info
                                where content_id={$content_id} and languages_id = {$this->languages_id}
                                limit 1")->row('name');
    }
    public function imgId()
    {
        return $this->db->select("select max(id) as id from content_images")->row('id');
    }

    /**
     * get max sort index
     * @param $content_id
     * @return array|mixed
     */
    public function maxSort($content_id)
    {
        return $this->db->select("select max(sort) as sort from content_images where content_id = {$content_id}")
                        ->row('sort');
    }

    /**
     * get thumbnail path
     * @return array|mixed
     */
    public function thumbPath()
    {
        return $this->db->select("
            SELECT name
            FROM images_sizes
            WHERE width <>0
            AND height <>0
            ORDER BY width ASC , height ASC
            LIMIT 1
            ")->row('name');
    }

    /**
     * preview size
     * @param $content_id
     * @return array|mixed
     */
    public function previewSize($content_id)
    {
        return $this->db->select("select s.name
                                  from images_sizes s
                                  join content c on c.id={$content_id}
                                  join content_type_images t on t.images_sizes_id=s.id and c.type_id = t.content_type_id
                                  where s.width <> 0
                                    AND s.height <> 0
                                    ORDER BY s.width DESC , s.height DESC
                                    LIMIT 1
                                ")->row('name');
    }

    /**
     * @param $content_id
     * @return array|mixed
     */
    public function get($content_id)
    {
        return $this->db->select("select id, name
                                  from content_images
                                  where content_id = $content_id
                                  order by abs(sort) asc
        ")
        ->all();
    }

    public function imageData($id, $key='*')
    {
        return $this->data('content_images', $id, $key);
    }

    /**
     * @param $id
     * @return array
     */
    public function getAlt($id)
    {
        return $this->fulInfo('content_images_info', 'images_id', $id);
    }


    public function cropSizes($content_id)
    {
        return $this->db->select("select
                                  CONCAT(s.name, ' [', s.width, 'px X ', s.height,'px]') as name,
                                  CONCAT(s.width, 'x', s.height) as value
                                  from images_sizes s
                                  join content c on c.id={$content_id}
                                  join content_type_images t on t.images_sizes_id=s.id and c.type_id = t.content_type_id
                                ")->all();
    }

    /**
     * after crop select name of size to create save path
     * @param $w
     * @param $h
     * @return array|mixed
     */
    public function getNameBySize($w, $h)
    {
        return $this->db->select("select name from images_sizes where width={$w} and height = {$h} limit 1")
            ->row('name');
    }
} 