<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 05.07.14 20:34
 */

namespace models\engine\content;

use models\Engine;

defined('SYSPATH') or die();

class Type extends Engine {

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * insert data to table structure
     * @param $data
     * @param $info
     * @return int insert ID
     */
    public function create($data, $info=array())
    {
        return $this->createRow('content_type', $data);
    }

    /**
     * @param $id
     * @param $data
     * @param $info
     * @return bool|string
     */
    public function update($id, $data, $info=array()){
        return $this->updateRow('content_type', $id, $data);
    }

    /**
     * delete item from structure
     * @param $id
     * @return int
     */
    public function delete($id)
    {
        return $this->deleteRow('content_type', $id);
    }

    /**
     * @param $id
     * @return array|mixed
     */
    public function getType($id)
    {
        return $this->db->select("select type
                                  from content_type
                                  where id = '{$id}'
                                  order by id asc")->row('type');
    }

    public function updateSizes($id,$data)
    {
//        return $this->db->insert('content_type_images')
    }

    /**
     * get All images_sizes
     * @return array|mixed
     */
    public function get()
    {
        return $this->db->select("select id, name, id as value
                                  from images_sizes
                                  order by id asc")->all();
    }


    public function clearSelectedSizes($content_type_id)
    {
        return $this->db->delete("content_type_images","content_type_id={$content_type_id}");
    }

    public function addSelectedSize($id,$sizes_id)
    {
        return $this->db->insert("content_type_images", array(
            'images_sizes_id' => $sizes_id,
            'content_type_id' =>$id
        ));
    }

    public function getSelectedSizes($content_type_id)
    {
        return $this->db->select("select images_sizes_id as id
                                  from content_type_images
                                  where content_type_id={$content_type_id}
                                  ")->all();
    }
} 