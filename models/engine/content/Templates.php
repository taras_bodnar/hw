<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 05.07.14 20:34
 */

namespace models\engine\content;

use models\Engine;

defined('SYSPATH') or die();

class Templates extends Engine {

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * insert data to table structure
     * @param $data
     * @param $info
     * @return int insert ID
     */
    public function create($data, $info=array())
    {
        return $this->createRow('content_templates', $data);
    }

    /**
     * @param $id
     * @param $data
     * @param $info
     * @return bool|string
     */
    public function update($id, $data, $info=array()){
        return $this->updateRow('content_templates', $id, $data);
    }

    /**
     * delete item from structure
     * @param $id
     * @return int
     */
    public function delete($id)
    {
        return $this->deleteRow('content_templates', $id);
    }

    /**
     * content type
     * @param int $selected
     * @return array|mixed
     */
    public function types( $selected = 0)
    {
        return $this->db->select("select id,id as value,name, IF($selected = id, 'selected','' ) as selected
                                  from content_type
                                  order by id asc
                                ")->all();
    }

    /**
     * get content type
     * @param $id
     * @return array|mixed
     */
    public function getType($id)
    {
        return $this->db->select("SELECT type from content_type where id=$id limit 1")->row('type');
    }
    public function changMain($id, $type_id)
    {
        $r = $this->db->update("content_templates", array('main' => 0), " id <> $id and type_id={$type_id} ");
        if($r > 0){
            $this->db->update("content_templates", array('main' => 1), " id = $id and type_id={$type_id}  limit 1");
        }
        return $r;
    }
} 