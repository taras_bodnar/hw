<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 26.06.14 13:54
 */

namespace models\engine\users;

use models\Engine;
use controllers\core\DB;

defined('SYSPATH') or die();

class Group extends Engine {

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * insert data to table structure
     * @param $data
     * @param $info
     * @return int insert ID
     */
    public function create($data, $info)
    {
        $this->db->beginTransaction();
        $r=0;
        $id = $this->db->insert("users_group", $data);

        if($id > 0) {
            foreach ($info as $languages_id =>$a) {
                if($this->db->insert("users_group_info",array(
                    'users_group_id' => $id,
                    'languages_id' => $languages_id,
                    'name'         => $info[$languages_id]['name'],
                    'description'  => $info[$languages_id]['description']
                ))){
                    $r++;
                } else{
                    $this->error(DB::getErrorMessage());
                }
            }
        } else {
            $this->error(DB::getErrorMessage());
        }
        if($r > 0) {
            $this->db->commit();
        } else {
            $this->db->rollBack();
        }

        return $r;
    }

    /**
     * @param $id
     * @param $data
     * @param $info
     * @return bool|string
     */
    public function update($id, $data, $info){
        $this->db->beginTransaction();

        $r = $this->db->update("users_group", $data, "id={$id} limit 1");

        foreach ($info as $languages_id =>$a) {
            $aid = $this->db->select("select id
                                    from users_group_info
                                    where users_group_id=$id and languages_id=$languages_id
                                    limit 1
                                    ")->row('id');
            if(empty($aid)){
                $r += $this->db->insert(
                    "users_group_info",
                    array(
                        'users_group_id'=> $id,
                        'languages_id'=> $languages_id,
                        'name'        => $info[$languages_id]['name'],
                        'description' => $info[$languages_id]['description']
                    )
                );
            } else {
                $r += $this->db->update(
                    "users_group_info",
                    array(
                        'name'        => $info[$languages_id]['name'],
                        'description' => $info[$languages_id]['description']
                    )
                    ," id = {$aid} limit 1");
            }
        }

        if($r > 0) {
            $this->db->commit();
        } else {
            $this->db->rollBack();
        }

        return $r;
    }

    /**
     * delete item from structure
     * @param $id
     * @return int
     */
    public function delete($id)
    {
        $r = $this->db->select("select id from users_group where parent_id={$id}")->all();
        if(!empty($r)) foreach ($r as $row) {
            $this->delete($row['id']);
        }

        $this->db->beginTransaction();

        if(
            $this->db->delete("users_group_info", " users_group_id = {$id}") &&
            $this->db->delete("users_group", " id = {$id} limit 1")
        ){
            $this->db->commit();
        } else {
            $this->db->rollBack();
        }

        return 1;
    }

    /**
     * @param $id
     * @return array
     */
    public function info($id)
    {
        $r = $this->db->select("SELECT * FROM users_group_info where users_group_id = {$id}")->all();
        $res=array();
        foreach ($r as $row) {
            $res[$row['languages_id']]['name'] = $row['name'];
            $res[$row['languages_id']]['description'] = $row['description'];
        }
        return $res;
    }

    /**
     * @param int $selected_id
     * @param int $item_id
     * @param $min
     * @param $max
     * @return array|mixed
     */
    public function parents($selected_id = 0, $item_id = 0, $min, $max)
    {
        return $this->db->select("SELECT s.id as value, i.name, IF(s.id={$selected_id}, 'selected', '') as selected
                                FROM users_group s
                                JOIN users_group_info i ON
                                i.users_group_id = s.id
                                AND i.languages_id = {$this->languages_id}
                                WHERE s.id <> $item_id and s.rang between {$min} and {$max}
                                ORDER BY ABS(s.id) ASC
                                ")->all();
    }

    /**
     * change isfolder status
     * @param $id
     * @return bool
     */
    public function toggleFolder($id)
    {
        $isFolder = ($this->hasChildren($id)) ? 1 : 0;
        return $this->db->update("users_group", array('isfolder' => $isFolder) , " id = {$id} limit 1");
    }

    /**
     * check children count
     * @param $parent_id
     * @return bool
     */
    public function hasChildren($parent_id)
    {
        return $this->db->select("select count(id) as t from users_group where parent_id ={$parent_id}")->row('t') > 0;
    }

    /**
     * @param $parent_id
     * @param $min
     * @param $max
     * @return array|mixed
     */
    public function tree($parent_id,$min,$max)
    {
        return $this->db->select("select s.id,i.name, (select count(t.id) from users_group t where t.parent_id =s.id) as isfolder
                                from users_group s,users_group_info i
                                where s.parent_id = {$parent_id} and s.rang between {$min} and {$max} and i.users_group_id=s.id and i.languages_id = {$this->languages_id}
                                order by abs(s.sort) asc")->all();
    }
} 