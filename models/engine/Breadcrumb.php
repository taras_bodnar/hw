<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 26.06.14 13:54
 */

namespace models\engine;
use models\Engine;


defined('SYSPATH') or die();

class Breadcrumb extends Engine {

    private $items = array();

    public function __construct()
    {
        parent::__construct();
    }

    public function controllerId($nc)
    {
        $nc = strtr($nc, '\\', '/');
        return $this->db->select("
                                select id
                                from components
                                where controller like '{$nc}'
                                limit 1
                               ")->row('id');
    }

    public function crumbs($id)
    {
        $r = $this->db->select("
                                select s.id,
                                s.parent_id,
                                s.isfolder,
                                SUBSTRING_INDEX(s.controller, 'engine/', -1) as controller,
                                i.name
                                from components s,
                                components_info i
                                where s.id = {$id} and i.components_id = s.id
                                and i.languages_id = {$this->languages_id}
                                limit 1
                               ")->row();

        if($r['id'] == 1)  return $this->items;

        $this->items[] = array(
            'controller'=>$r['controller'],
            'name'=>$r['name'],
            'children' => $r['isfolder'] ? $this->children($r['id']) : null
        );

        if($r['parent_id'] != 0) $this->crumbs($r['parent_id']);

        return $this->items;
    }

    private function children($parent_id)
    {
        return $this->db->select("
                                select s.id,
                                s.parent_id,
                                s.isfolder,
                                SUBSTRING_INDEX(s.controller, 'engine/', -1) as controller,
                                i.name
                                from components s,
                                components_info i
                                where s.parent_id = {$parent_id} and i.components_id = s.id
                                and i.languages_id = {$this->languages_id}
                                order by ABS(s.sort) asc, ABS(s.id) asc
                                ")->all();
    }





    /**
     * insert data to table structure
     * @param $data
     * @param $info
     * @return int insert ID
     */
    public function create($data, $info=array()){}

    /**
     * @param $id
     * @param $data
     * @param $info
     * @return bool|string
     */
    public function update($id, $data, $info=array()){}

    /**
     * delete item from structure
     * @param $id
     * @return int
     */
    public function delete($id){}
} 