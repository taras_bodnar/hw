<?php
/**
 * Company Otakoyi.com
 * Author: wg
 * Date: 11.01.15 0:05
 */

namespace models\engine;

use models\Engine;
use models\modules\UsersNotifications;

defined("SYSPATH") or die();

class Comments extends Engine {

    public function create($data, $info=array())
    {
        return $this->createRow('comments', $data);
    }

    /**
     * @param $id
     * @param $data
     * @param $info
     * @return bool|string
     */
    public function update($id, $data, $info=array()){
        return $this->updateRow('comments', $id, $data);
    }


    public function delete($id)
    {
        return $this->db->delete("comments", " id = '{$id}' limit 1");
    }

    /**
     * @param $id
     * @param $pub
     * @return bool
     */
    public function pub($id, $pub)
    {
        $s = $this->db->update('comments', array('status'=> 'approved'), "id=$id limit 1");
        if($s > 0 && $pub){
            $c  =$this->db->select("select * from comments where id = {$id} limit 1")->row();
            UsersNotifications::instance()->newComment($c['workers_id'], $c['users_id'], $c['id']);
        }
        return $s;
    }

    /**
     * @param $content_id
     * @return array|mixed
     */
    public function get($content_id)
    {
        return $this->db->select("select *, DATE_FORMAT(createdon, '%d.%m.%y') as dt, DATE_FORMAT(createdon, '%h:%i') as hi
                                  from comments
                                  where content_id={$content_id}
                                  order by id desc
                                  ")->all();
    }

    /**
     * @param $id
     * @return array|mixed
     */
    public function getData($id)
    {
        return $this->data('comments', $id);
    }
} 