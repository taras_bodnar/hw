<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 05.07.14 20:34
 */

namespace models\engine\products;

use models\Engine;

defined('SYSPATH') or die();

class Options extends Engine {

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * insert data to table structure
     * @param $data
     * @param $info
     * @return int insert ID
     */
    public function create($data, $info=array())
    {
        return $this->createRow('products_options', $data);
    }

    /**
     * @param $id
     * @param $data
     * @param $info
     * @return bool|string
     */
    public function update($id, $data, $info=array()){
        $aid = $this->db->select("select id from products_options where products_id = {$id} and is_variant = 0 limit 1")->row('id');
        if($aid > 0) {
            return $this->updateRow('products_options', $aid, $data);
        } else {
            $data['products_id'] = $id;
            return $this->createRow('products_options', $data);
        }
    }

    /**
     * delete item from structure
     * @param $id
     * @return int
     */
    public function delete($id)
    {
        return $this->db->delete('products_options', " product_id = {$id}");
    }

    /**
     * get product data
     * @param $product_id
     * @return array|mixed
     */
    public function getData($product_id)
    {
        return $this->db->select("select * from products_options where products_id ={$product_id} limit 1")->row();
    }


    /**
     * switch status by col
     * @param $col
     * @param $id
     * @param $status
     * @return bool
     */
    public function toggleStatus($col, $id, $status)
    {
        return $this->db->update("products_options", array($col => $status) , " products_id = {$id} limit 1");
    }

    /**
     * @param $id
     * @param $price
     * @return bool
     */
    public function updatePrice($id, $price)
    {
        return $this->db->update("products_options", array('price' => $price) , " products_id = {$id} limit 1");
    }
} 