<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 05.07.14 20:34
 */

namespace models\engine\products;

use models\Engine;

defined('SYSPATH') or die();

class Categories extends Engine {

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * insert data to table structure
     * @param $data
     * @param $info
     * @return int insert ID
     */
    public function create($data, $info=array())
    {
        return $this->createRow('products_categories', $data);
    }

    /**
     * @param $id
     * @param $data
     * @param $info
     * @return bool|string
     */
    public function update($id, $data, $info=array()){
        return $this->updateRow('products_categories', $id, $data);
    }

    /**
     * delete item from structure
     * @param $id
     * @return int
     */
    public function delete($id)
    {
        return $this->db->delete('products_categories', " pid = {$id}");
    }

    /**
     * get selected categories
     * @param $pid
     * @return array
     */
    public function get($pid)
    {
        $r = $this->db->select("select cid from products_categories where pid={$pid}")->all();
        $res = array();
        foreach ($r as $row) {
            $res[] = $row['cid'];
        }

        return $res;
    }
} 