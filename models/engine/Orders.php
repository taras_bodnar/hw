<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 26.06.14 13:54
 */

namespace models\engine;
use models\Engine;


defined('SYSPATH') or die();

/**
 * Class orders
 * @package models\engine
 */
class Orders extends Engine {
    private $where=array();
    private $order_by = 'order by o.id desc';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param $where
     * @param string $prefix
     * @return $this
     */
    public function setWhere($where, $prefix = 'and')
    {
        $this->where[] = $prefix . ' ' . $where;
        return $this;
    }

    /**
     * @param $ob
     * @param string $dir
     * @return $this
     */
    public function setOrderBy($ob, $dir='asc')
    {
        if(!empty($ob)){
            $this->order_by = " order by {$ob} $dir";
        }

        return $this;
    }

    /**
     * @return string
     */
    private function getWhere()
    {
        return implode(' ', $this->where);
    }

    /**
     * insert data to table structure
     * @param $data
     * @param $info
     * @return int insert ID
     */
    public function create($data, $info=array())
    {
        return $this->createRow('orders', $data);
    }

    /**
     * @param $id
     * @param $data
     * @param $info
     * @return bool|string
     */
    public function update($id, $data, $info=array()){
        return $this->updateRow('orders', $id, $data);
    }

    public function delete($id)
    {
        return $this->db->delete("orders", "id={$id} and pay = 0 limit 1");
    }

    /**
     * @param $id
     * @return array|mixed
     */
    public function getData($id)
    {
        return $this->db->select("
            select o.*, DATE_FORMAT(o.createdon, '%d.%m.%Y %H:%i:%s') as createdon
            from orders o
            where o.id={$id}
        ")->row();
    }

    public function getStatuses()
    {
        return $this->db->select("
            select s.id,i.name
            from orders_status s
            join orders_status_info i on i.orders_status_id=s.id and i.languages_id = {$this->languages_id}
             order by abs(s.id) asc
            ")->all();
    }

    public function getProducts($orders_id)
    {
        return $this->db->select("select * from orders_products where orders_id = {$orders_id}")->all();
    }

    public function sumTotal($orders_id)
    {
        return $this->db->select("select SUM(quantity * price) as t from orders_products where orders_id = {$orders_id}")->row('t');
    }

    /**
     * @param $orders_id
     * @param $status_id
     * @param $comment
     */
    public function saveStatusHistory($orders_id, $status_id, $comment)
    {
        $id = $this->db->select("
                select id
                from orders_status_history
                where orders_id={$orders_id} and orders_status_id = {$status_id}
                limit 1")->row('id');
        if(empty($id)){
            $this->db->insert(
                'orders_status_history',
                array(
                    'orders_id' => $orders_id,
                    'orders_status_id' => $status_id,
                    'comment'   => $comment,
                    'createdon' => date('Y-m-d H:i:s')
                )
            );
        }
    }

    /**
     * @param $orders_id
     * @return array|mixed
     */
    public function getStatusHistory($orders_id)
    {
        return $this->db->select("
            select h.comment,h.createdon,i.name
            from orders_status_history h
            left join orders_status_info i on i.orders_status_id=h.orders_status_id and i.languages_id={$this->languages_id}
            where h.orders_id = {$orders_id}
            order by h.id asc
        ")->all();
    }

    /**
     * @param $content_id
     * @return array|mixed
     */
    public function img($content_id)
    {
        return $this->db->select("
          select name from content_images where content_id='{$content_id}' order by abs(sort) asc limit 1
        ")->row('name');
    }
}