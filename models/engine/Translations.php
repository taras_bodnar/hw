<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 26.06.14 13:54
 */

namespace models\engine;
use models\Engine;
use controllers\core\DB;


defined('SYSPATH') or die();

class Translations extends Engine {

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * insert data to table structure
     * @param $data
     * @param $info
     * @return int insert ID
     */
    public function create($data, $info)
    {
        $this->db->beginTransaction();
        $r=0;
        $id = $this->db->insert("translations", $data);

        if($id > 0) {
            foreach ($info as $languages_id =>$a) {
                if($this->db->insert("translations_info",array(
                    'translations_id' => $id,
                    'languages_id' => $languages_id,
                    'value'         => $info[$languages_id]['value']
                ))){
                    $r++;
                } else{
                    $this->error(DB::getErrorMessage());
                }
            }
        } else {
            $this->error(DB::getErrorMessage());
        }
        if($r > 0) {
            $this->db->commit();
        } else {
            $this->db->rollBack();
        }

        return $r;
    }

    /**
     * @param $id
     * @param $data
     * @param $info
     * @return bool|string
     */
    public function update($id, $data, $info){
        $this->db->beginTransaction();

        $r = $this->db->update("translations", $data, "id={$id} limit 1");

        foreach ($info as $languages_id =>$a) {
            $aid = $this->db->select("select id
                                    from translations_info
                                    where translations_id=$id and languages_id=$languages_id
                                    limit 1
                                    ")->row('id');
            if(empty($aid)){
                $r += $this->db->insert(
                    "translations_info",
                    array(
                        'translations_id'=> $id,
                        'languages_id'=> $languages_id,
                        'value'        => $info[$languages_id]['value']
                    )
                );
            } else {
                $r += $this->db->update(
                    "translations_info",
                    array(
                        'value'        => $info[$languages_id]['value']
                    )
                    ," id = {$aid} limit 1");
            }
        }

        if($r > 0) {
            $this->db->commit();
        } else {
            $this->db->rollBack();
        }

        return $r;
    }

    /**
     * delete item from structure
     * @param $id
     * @return int
     */
    public function delete($id)
    {
        $this->db->beginTransaction();

        if(
            $this->db->delete("translations_info", " translations_id = {$id}") &&
            $this->db->delete("translations", " id = {$id} limit 1")
        ){
            $this->db->commit();
        } else {
            $this->db->rollBack();
        }

        return 1;
    }

    /**
     * @param $id
     * @return array
     */
    public function info($id)
    {
        $r = $this->db->select("SELECT * FROM translations_info where translations_id = {$id}")->all();
        $res=array();
        foreach ($r as $row) {
            $res[$row['languages_id']]['value'] = $row['value'];
        }
        return $res;
    }

    /**
     * витгує всі переклади
     * @param $languages_id
     * @return array|mixed
     */
    public function getTranslations($languages_id)
    {
        $r= $this->db->select("
                    select t.code,i.value
                    from translations t
                    join translations_info i on i.translations_id = t.id and i.languages_id={$languages_id}
                ")
            ->all();
        $res=array();
        foreach ($r as $row) {
            $res[$row['code']] = $row['value'];
        }
        return $res;
    }
} 