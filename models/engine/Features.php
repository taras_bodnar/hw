<?php
/**
 * Created by PhpStorm.
 * User: wg
 * Date: 11.03.15
 * Time: 10:43
 */

namespace models\engine;

defined('SYSPATH') or die();

use controllers\core\DB;
use models\Engine;

class Features extends Engine {
    /**
     * create auto index
     * @param $owner_id
     * @return bool|string
     */
    public function createAuto($owner_id)
    {
        $this->db->delete("features", " auto=1 and owner_id={$owner_id}");
        return $this->db->insert("features", array('owner_id' => $owner_id, 'published'=>1));
    }

    /**
     * insert data to table structure
     * @param $data
     * @param $info
     * @return int insert ID
     */
    public function create($data, $info)
    {
        $this->db->beginTransaction();
        $r=0;
        $id = $this->db->insert("features", $data);

        if($id > 0) {
            foreach ($info as $languages_id =>$a) {
                if($this->db->insert("features_info",array(
                    'features_id'   => $id,
                    'languages_id' => $languages_id,
                    'name'         => $info[$languages_id]['name']
                ))){
                    $r++;
                } else{
                    $this->error(DB::getErrorMessage());
                }
            }
        } else {
            $this->error(DB::getErrorMessage());
        }
        if($r > 0) {
            $this->db->commit();
        } else {
            $this->db->rollBack();
        }

        return $id;
    }

    /**
     * @param $id
     * @param $data
     * @param $info
     * @return bool|string
     */
    public function update($id, $data, $info){
        $this->db->beginTransaction();

        $r = $this->db->update("features", $data, "id={$id} limit 1");

        foreach ($info as $languages_id =>$a) {
            $aid = $this->db->select("select id
                                      from features_info
                                      where features_id=$id and languages_id=$languages_id
                                      limit 1
                                    ")->row('id');
            if(empty($aid)){
                $r += $this->db->insert(
                    "features_info",
                    array(
                        'features_id'  => $id,
                        'languages_id'=> $languages_id,
                        'name'        => $info[$languages_id]['name']
                    )
                );
            } else {
                $r += $this->db->update(
                    "features_info",
                    array(
                        'name'        => $info[$languages_id]['name']
                    )
                    ," id = {$aid} limit 1");
            }
        }

        if($r > 0) {
            $this->db->commit();
        } else {
            $this->db->rollBack();
        }

        return $r;
    }


    /**
     * delete item from structure
     * @param $id
     * @return int
     */
    public function delete($id)
    {
        return $this->deleteRow('features', $id);
    }
    /**
     * switch pub status
     * @param $id
     * @param $published
     * @return bool
     */
    public function pub($id, $published)
    {
        return $this->db->update("features", array('published'=>$published) , " id = {$id} limit 1");
    }

    /**
     * switch status
     * @param $id
     * @param $col
     * @param $published
     * @return bool
     */
    public function show($id, $col, $published)
    {
        return $this->db->update("features", array($col=>$published) , " id = {$id} limit 1");
    }


    public function getType($selected='')
    {
        $r = $this->db->enumValues('features','type');
        $res=array();
        foreach($r as $k=>$v){
            $res[] = array(
                'id' => $v,
                'name' => $v,
                'selected' => $v == $selected ? 'selected' : '',
            );
        }

        return $res;
    }

    /**
     * @param $parent_id
     * @return array|mixed
     */
    public function categories($parent_id)
    {
        return $this->db->select("
            select c.id, i.name, c.isfolder
            from content c
            join content_type ct on ct.type in ( 'page', 'category' )
            join content_info i on i.content_id=c.id and i.languages_id = {$this->languages_id}
            where c.parent_id={$parent_id} and c.type_id=ct.id
            order by  c.id asc
        ")->all();
    }

    /**
     * @param $features_id
     * @return array
     */
    public function getSelectedCategories($features_id)
    {
        $r = $this->db->select("select content_id as id from content_features where features_id = {$features_id}")->all();
        $res=array();
        foreach ($r as $row) {
            $res[] = $row['id'];
        }

        return $res;
    }

    /**
     * @param $features_id
     * @return array|mixed
     */
    public function getContentType($features_id)
    {
        return $this->db->select("
            select ct.id,ct.type,CONCAT(ct.name,' (',ct.type, ')') as name, IF(cto.id > 0, 'selected' , '') as selected
            from content_type ct
            left join content_type_features cto on cto.content_type_id=ct.id and cto.features_id={$features_id}
        ")->all();
    }

    // привязта параметрів до сторінок
    /**
     * @param $features_id
     * @return int
     */
    public function deleteAllContentFeatures($features_id)
    {
        $this->db->update('features', array('extends' => 0) , " id = {$features_id} limit 1");
        return $this->db->delete('content_features', "features_id=$features_id");
    }

    /**
     * @param $features_id
     * @param $content_id
     * @return bool|string
     */
    public function createContentFeatures($features_id, $content_id)
    {
        return $this->db->insert("content_features", array('features_id'=>$features_id, "content_id" => $content_id));
    }
    
//    .. привязка парамтетрів до типу контенту

    /**
     * @param $features_id
     * @return int
     */
    public function deleteAllContentTypeFeatures($features_id)
    {
        return $this->db->delete("content_type_features", "features_id=$features_id");
    }

    /**
     * @param $features_id
     * @param $content_type_id
     * @return bool|string
     */
    public function createContentTypeFeatures($features_id, $content_type_id)
    {
        $s = $this->db->insert(
            "content_type_features",
            array(
                'features_id'     => $features_id,
                "content_type_id" => $content_type_id
            )
        );
        if($s > 0) {
            $this->db->update('features', array('extends' => 1) , " id = {$features_id} limit 1");
        }
        return $s;
    }


    // Значення властивостей (select > options ..)

    /**
     * @param $features_id
     * @return array
     */
    public function createValue($features_id)
    {
        return $this->db->insert("features_values", array('features_id'=> $features_id));
    }

    /**
     * @param $values
     * @return int
     */
    public function updateValuesInfo($values)
    {
        foreach ($values as $values_id=>$a) {
            foreach ($a as $languages_id=>$value) {
                $id = $this->db->select("
                     select id from features_values_info
                     where values_id={$values_id} and languages_id = {$languages_id} limit 1
                ")->row('id');
                if($id > 0){
                    $this->db->update(
                        'features_values_info',
                        array('value' => $value),
                        " id={$id} limit 1"
                    );
                } else {
                    $this->db->insert(
                        "features_values_info",
                        array(
                            'values_id'    => $values_id,
                            'languages_id' => $languages_id,
                            'value'        => $value
                        )
                    );
                }
            }
        }

        return 1;
    }

    /**
     * @param $values_id
     * @param $languages_id
     * @param $value
     * @return bool|string
     */
    public function createValuesInfo($values_id, $languages_id, $value)
    {
       return $this->db->insert(
            "features_values_info",
            array(
                'values_id'    => $values_id,
                'languages_id' => $languages_id,
                'value'        => $value
            )
        );
    }

    /**
     * @param $id
     * @return int
     */
    public function removeValue($id)
    {
        return $this->db->delete("features_values", "id = '{$id}' limit 1");
    }

    public function getValues($features_id)
    {
        $r = $this->db->select("
            select id
            from features_values
            where features_id={$features_id}
            order by abs(sort) asc
        ")->all();
        $res = array();
        foreach ($r as $row) {
            $res[$row['id']] = $this->db->select("
              select l.id as languages_id,l.name as placeholder, i.value
              from languages l
              left join features_values_info i on i.values_id={$row['id']} and i.languages_id=l.id
              where l.front = 1
              ")->all();
        }

        return $res;
    }
}