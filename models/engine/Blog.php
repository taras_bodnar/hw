<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 05.07.14 20:34
 */

namespace models\engine;
use models\Engine;

defined('SYSPATH') or die();

class Blog extends Engine {

    /**
     * insert data to table structure
     * @param $data
     * @param $info
     * @return int insert ID
     */
    public function create($data, $info=array())
    {
    }

    /**
     * @param $id
     * @param $data
     * @param $info
     * @return bool|string
     */
    public function update($id, $data, $info=array())
    {
    }

    /**
     * delete item from structure
     * @param $id
     * @return int
     */
    public function delete($id)
    {
    }

    public function getCategories($post_id)
    {
        return $this->db->select("select id,cid from posts_categories where pid='{$post_id}'")->all();
    }
    public function deleteCategories($post_id, $cid)
    {
        return $this->db->delete('posts_categories', " pid={$post_id} and cid={$cid} limit 1");
    }
    public function setCategories($post_id, $cid)
    {
        return $this->db->insert('posts_categories', array('pid'=>$post_id, 'cid' => $cid));
    }
} 