<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 11.05.14 10:53
 */
namespace models\engine;
use models\Engine;

if ( !defined('SYSPATH') ) die();

/**
 * Class Table working vs datatables
 * @package models\engine
 */
class Tree extends Engine {

    const TBL_PK        = 'id';
    const TBL_IS_FOLDER = 'isfolder';
    const TBL_PARENT_ID = 'parent_id';

    public function __construct()
    {
        parent::__construct();
    }


    /**
     * @param $data
     * @param $info
     * @return int insert ID
     */
    public function create($data, $info)
    {
    }

    /**
     * @param $id
     * @param $data
     * @param $info
     * @return bool|string
     */
    public function update($id, $data, $info)
    {
    }

    /**
     * @param $id
     * @return int
     */
    public function delete($id)
    {
    }

    /**
     * move item to new parent
     * @param $table
     * @param $id
     * @param $parent_id
     * @return bool
     */
    public function moveToParent($table, $id, $parent_id)
    {
        $r = $this->db->update($table, array(self::TBL_PARENT_ID => $parent_id), " id={$id}");
        if($r > 0 && $parent_id > 0){
            $this->db->update($table, array(self::TBL_IS_FOLDER => 1) , "id = {$parent_id} limit 1");
        }

        return $r;
    }

    /**
     * check if prev parent has child if not set isFolder 0
     * @param $table
     * @param $parent_id
     */
    public function checkIsFolderPrevParent($table, $parent_id)
    {
        $pk = self::TBL_PK;
        $r= $this->db->select("select count({$pk}) as t from {$table} where parent_id = {$parent_id}")->row('t');
        if($r == 0) {
            $this->db->update($table, array(self::TBL_IS_FOLDER => 0) , "id = {$parent_id} limit 1");
        }
    }

}