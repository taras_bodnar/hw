<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 26.06.14 13:54
 */

namespace models\engine;
use models\Engine;

defined('SYSPATH') or die();

class Auth extends Engine {

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * insert data to table structure
     * @param $data
     * @param $info
     * @return int insert ID
     */
    public function create($data, $info=array()){}

    /**
     * @param $id
     * @param $data
     * @param $info
     * @return bool|string
     */
    public function update($id, $data, $info=array()){}

    /**
     * check if is user online
     * @param $id
     * @param $sid
     * @return $this
     */
    public function isOnline($id,$sid)
    {
        return $this->db->select("select id
                                  from users
                                  where id = '{$id}' and sessid = '{$sid}'
                                  limit 1
                                  ")->row('id') > 0;
    }

    /**
     * user info by email
     * @param $email
     * @return array|mixed
     */
    public function userDataByEmail($email)
    {
        return $this->db->select("
                                select u.*, g.rang,l.code
                                from users u, users_group g, languages l
                                where u.email like '{$email}' and u.users_group_id = g.id and l.id=u.languages_id
                                ")->row();
    }

    /**
     * update session
     * @param $id
     * @return bool
     */
    public function updateSession($id)
    {
        return $this->db->update('users', array('sessid'=>session_id()), " id = '{$id}' limit 1");
    }
    public function logout($id)
    {
        return $this->db->update('users', array('sessid'=>''), " id = '{$id}' limit 1");
    }

    /**
     * update user password
     * @param $id
     * @param $password
     * @return bool
     */
    public function updatePassword($id, $password)
    {
        return $this->db->update('users', array('password'=>$password), " id = '{$id}' limit 1");
    }

} 