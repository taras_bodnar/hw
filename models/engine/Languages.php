<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 21.05.14 22:24
 */

namespace models\engine;
use models\Engine;

use controllers\core\DB;


if ( !defined('SYSPATH') ) die();

class Languages extends Engine
{
    public function __construct()
    {
        parent::__construct();
    }

    function create($data, $info=array())
    {
        $this->db->beginTransaction();
        $id = $this->db->insert("languages", $data);

        if($id > 0) {
            $this->db->commit();
        } else {
            $this->error(DB::getErrorMessage());
            $this->db->rollBack();
        }

        return $id;
    }
    function update($id,$data, $info=array())
    {
        return $this->updateRow('languages', $id, $data);
    }
    function delete($id)
    {
        return $this->deleteRow('languages', $id);
    }

    /**
     * change default language
     * @param $id
     * @param $col
     * @return bool
     */
    public function changeDefault($id, $col)
    {
        $r = $this->db->update("languages", array($col=>0), "{$col} = 1 and id <> {$id}");
        if($r > 0){
            $this->db->update("languages", array($col=>1), " id = {$id} limit 1");
        }
        return $r;
    }

    /**
     * повертає всю інформацію або конкретне поле по активній мові
     * @param string $key
     * @return array|mixed
     */
    public function getBackendDefault($key='*')
    {
        return $this->db->select("select {$key} from languages where back_default = 1")->row($key);
    }

    /**
     * повертає всю інформацію або конкретне поле по активній мові
     * @param string $key
     * @return array|mixed
     */
    public function getFrontendDefault($key='*')
    {
        return $this->db->select("select {$key} from languages where front_default = 1")->row($key);
    }
    /**
     * get all languages front or back
     * @param int $back
     * @param int $front
     * @return array
     */
    public function all($front= null, $back = null)
    {
        $where = ''; $order_by='';
        if($back) {
            $where .= "and back = $back";
            $order_by = 'back_default desc,';
        }
        if($front) {
            $where .= "and front = $front";
            $order_by = 'front_default desc,';
        }
        return $this->db->select("select *, id as value
                                  from languages
                                  where 1 {$where}
                                  order by {$order_by} id asc")->all();
    }

    /**
     * @param $id
     * @param string $key
     * @return array|mixed
     */
    public function rowData($id, $key='*')
    {
        return $this->db->select("select {$key} from languages where id = '{$id}' limit 1")->row($key);
    }

    /**
     * витягує список таблиць з суфіксом info
     * @return array|mixed
     */
    public function getInfoTables()
    {
        return $this->db->select("SHOW TABLES LIKE '%_info'")->all();
    }

    /**
     * витягує список коонок таблиці з детальною інформацією про них
     * @param $table
     * @return array|mixed
     */
    public function describe($table)
    {
        return $this->db->select("DESCRIBE {$table}")->all();
    }

    public function getInfoRecords($table, $columns, $languages_id)
    {
        $col = implode(',', $columns);
        return $this->db->select("select {$col} from {$table} where languages_id = {$languages_id}")->all();
    }

    public function getNotTranslatedLanguage()
    {
        return $this->db->select("
            select l.name,l.id,i.name as c_name,l.back
            from languages l
            left join content_info i on i.languages_id=l.id
            group by l.id
            order by l.id desc
        ")->all();
    }

    /**
     * @param $table
     * @param $languages_id
     * @return array|mixed
     */
    public function getTotalTableRecords($table, $languages_id)
    {
        return $this->db->select("select count(*) as t from {$table} where languages_id = {$languages_id}")->row('t');
    }

    public function getTableRow($table, $languages_id, $start)
    {
        return $this->db->select("select * from {$table} where languages_id = {$languages_id} order by id asc limit {$start}, 1 ")->row();
    }

    public function insertTranslatedData($table, $iv, $debug=0)
    {
        return $this->db->insert($table, $iv,$debug);
    }

    /**
     * @param $languages_id
     * @param $start
     * @return array|mixed
     */
    public function getAliasInfo($languages_id,$start)
    {
        return $this->db->select("
        SELECT i.id,c.parent_id,i.content_id ,i.name,i.alias
        FROM content c
        join content_info i on i.content_id = c.id and i.languages_id= '{$languages_id}'
        order by c.id asc limit {$start},1")->row();
    }

    /**
     * @param $id
     * @param $alias
     * @return bool
     */
    public function updateAlias($id, $alias)
    {
        return $this->db->update('content_info', array('alias'=> $alias), "id = '{$id}' limit 1");
    }
}