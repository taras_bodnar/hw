<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 26.06.14 13:54
 */

namespace models\engine;
use models\Engine;
use controllers\core\DB;


defined('SYSPATH') or die();

class EmailTemplates extends Engine {

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * insert data to table structure
     * @param $data
     * @param $info
     * @return int insert ID
     */
    public function create($data, $info=array())
    {
        $this->db->beginTransaction();
        $r=0;
        $id = $this->db->insert("email_templates", $data);

        if($id > 0) {
            foreach ($info as $languages_id =>$a) {
                if(
                    $this->db->insert(
                        "email_templates_info",
                        array(
                            'templates_id' => $id,
                            'languages_id' => $languages_id,
                            'reply_to'        => $info[$languages_id]['reply_to'],
                            'subject'        => $info[$languages_id]['subject'],
                            'body'        => $info[$languages_id]['body']
                        )
                    )
                )
                {
                    $r++;
                } else{
                    $this->error(DB::getErrorMessage());
                }
            }
        } else {
            $this->error(DB::getErrorMessage());
        }
        if($r > 0) {
            $this->db->commit();
        } else {
            $this->db->rollBack();
        }

        return $r;
    }

    /**
     * @param $id
     * @param $data
     * @param $info
     * @return bool|string
     */
    public function update($id, $data, $info=array()){
        $this->db->beginTransaction();

        $r = $this->db->update("email_templates", $data, "id={$id} limit 1");

        foreach ($info as $languages_id =>$a) {
            $aid = $this->db->select("select id
                                    from email_templates_info
                                    where templates_id={$id} and languages_id={$languages_id}
                                    limit 1
                                    ")->row('id');
            if(empty($aid)){
                $r += $this->db->insert(
                    "email_templates_info",
                    array(
                        'templates_id'=> $id,
                        'languages_id'=> $languages_id,
                        'reply_to'    => $info[$languages_id]['reply_to'],
                        'subject'     => $info[$languages_id]['subject'],
                        'body'        => $info[$languages_id]['body']
                    )
                );
            } else {
                $r += $this->db->update(
                    "email_templates_info",
                    array(
                        'reply_to' => $info[$languages_id]['reply_to'],
                        'subject'  => $info[$languages_id]['subject'],
                        'body'     => $info[$languages_id]['body']
                    )
                    ," id = {$aid} limit 1");
            }
        }

        if($r > 0) {
            $this->db->commit();
        } else {
            $this->db->rollBack();
        }

        return $r;

    }


    /**
     * delete item from structure
     * @param $id
     * @return int
     */
    public function delete($id)
    {
        $this->db->beginTransaction();

        if(
            $this->db->delete("email_templates_info", " templates_id = {$id}") &&
            $this->db->delete("email_templates", " id = {$id} limit 1")
        ){
            $this->db->commit();
        } else {
            $this->db->rollBack();
        }

        return 1;
    }

    /**
     * @param $id
     * @return array
     */
    public function info($id)
    {
        $r = $this->db->select("SELECT * FROM email_templates_info where templates_id = {$id}")->all();
        $res=array();
        foreach ($r as $row) {
            $res[$row['languages_id']]['reply_to'] = $row['reply_to'];
            $res[$row['languages_id']]['subject'] = $row['subject'];
            $res[$row['languages_id']]['body'] = $row['body'];
        }
        return $res;
    }
}