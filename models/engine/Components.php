<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 11.05.14 10:53
 */
namespace models\engine;
use models\Engine;

use controllers\core\DB;


if ( !defined('SYSPATH') ) die();

class Components extends Engine {

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * return formatted controller
     * @param $c
     * @param $m
     * @return mixed
     */
    public function formatController($c, $m='')
    {
        if($m == 'in') {
            $c = str_replace('\\', '/', $c);
        } else {
            $c = str_replace('/', '\\', $c);
        }

        return $c;
    }

    /**
     * @param $id
     * @param string $key
     * @return array|mixed
     */
    public function getData($id, $key='*')
    {
        return $this->db->select("select {$key} from components where id={$id} limit 1")->row($key);
    }

    /**
     * insert data to table structure
     * @param $data
     * @param $info
     * @return int insert ID
     */
    public function create($data, $info)
    {
        $this->db->beginTransaction();
        $r=0;
        $data['controller'] = $this->formatController($data['controller'] , 'in');
        $id = $this->db->insert("components", $data);

        if($id > 0) {
            foreach ($info as $languages_id =>$a) {
                if($this->db->insert("components_info",array(
                    'components_id' => $id,
                    'languages_id' => $languages_id,
                    'name'         => $info[$languages_id]['name'],
                    'description'  => $info[$languages_id]['description']
                ))){
                    $r++;
                } else{
                    $this->error(DB::getErrorMessage());
                }
            }
        } else {
            $this->error(DB::getErrorMessage());
        }
        if($r > 0) {
            $this->db->commit();
        } else {
            $this->db->rollBack();
        }

        return $r;
    }

    /**
     * @param $id
     * @param $data
     * @param $info
     * @return bool|string
     */
    public function update($id, $data, $info){
        $this->db->beginTransaction();

        $data['controller'] = $this->formatController($data['controller'] , 'in');

        $r = $this->db->update("components", $data, "id={$id} limit 1");

        foreach ($info as $languages_id =>$a) {
            $aid = $this->db->select("select id
                                    from components_info
                                    where components_id=$id and languages_id=$languages_id
                                    limit 1
                                    ")->row('id');
            if(empty($aid)){
                $r += $this->db->insert(
                    "components_info",
                    array(
                        'components_id'=> $id,
                        'languages_id'=> $languages_id,
                        'name'        => $info[$languages_id]['name'],
                        'description' => $info[$languages_id]['description']
                    )
                );
            } else {
                $r += $this->db->update(
                    "components_info",
                    array(
                        'name'        => $info[$languages_id]['name'],
                        'description' => $info[$languages_id]['description']
                    )
                    ," id = {$aid} limit 1");
            }
        }

        if($r > 0) {
            $this->db->commit();
        } else {
            $this->db->rollBack();
        }

        return $r;
    }

    /**
     * delete item from structure
     * @param $id
     * @return int
     */
    public function delete($id)
    {
        $r = $this->db->select("select id from components where parent_id={$id}")->all();
        if(!empty($r)) foreach ($r as $row) {
            $this->delete($row['id']);
        }

        $this->db->beginTransaction();

        if(
            $this->db->delete("components_info", " components_id = {$id}") &&
            $this->db->delete("components", " id = {$id} limit 1")
        ){
            $this->db->commit();
        } else {
            $this->db->rollBack();
        }

        return 1;
    }
    public function info($id)
    {
        $r = $this->db->select("
                                SELECT *
                                FROM components_info where components_id = {$id}
                                ")->all();
        $res=array();
        foreach ($r as $row) {
            $res[$row['languages_id']]['name'] = $row['name'];
            $res[$row['languages_id']]['description'] = $row['description'];
        }
        return $res;
    }
    public function getControllerData($controller)
    {
        $controller = str_replace('\\','/',$controller);
        return $this->db->select("
                                SELECT s.rang,i.name,i.description,s.icon
                                FROM components s
                                JOIN components_info i ON
                                     i.components_id = s.id
                                     AND i.languages_id = {$this->languages_id}
                                WHERE s.controller like '{$controller}'
                                 and s.published = 1
                                ")->row();
    }

    public function itemInfo($id)
    {
        return $this->db->select("
                                SELECT s.rang,i.name,s.icon,i.description
                                FROM components s
                                JOIN components_info i ON
                                     i.components_id = s.id
                                     AND i.languages_id = {$this->languages_id}
                                WHERE s.id={$id}
                                ")->row();
    }

    /**
     * @param int $parent_id
     * @param int $rang
     * @return array
     */
    public function nav($parent_id=1, $rang=100){
        $res=array();
        $r = $this->db->select(" SELECT s.id, SUBSTRING_INDEX(s.controller, 'engine/', -1) as controller,
                                        s.icon, s.rang, i.name, s.isfolder,i.description
                                FROM components s
                                JOIN components_info i ON
                                i.components_id = s.id
                                AND i.languages_id = {$this->languages_id}
                                WHERE s.parent_id = '{$parent_id}' and s.rang <= {$rang} and s.published=1
                                ORDER BY abs(s.sort) asc
                                ")->all();
        foreach ($r as $row) {
            // clear namespace


            if($row['isfolder']) {
                $row['children'] = $this->nav($row['id'], $rang);
            }
            $res[] = $row;
        }

        return $res;
    }

    /**
     * parents list
     * @param int $selected_id
     * @param int $item_id block cycle
     * @return array|mixed
     */
    public function parents($selected_id = 0, $item_id = 0)
    {
        return $this->db->select("SELECT s.id as value, i.name, IF(s.id={$selected_id}, 'selected', '') as selected
                                FROM components s
                                JOIN components_info i ON
                                i.components_id = s.id
                                AND i.languages_id = {$this->languages_id}
                                WHERE s.published=1 and s.id <> $item_id
                                ORDER BY ABS(s.id) ASC
                                ")->all();
    }

    public function table()
    {
        try{
            return $this->db->select("select s.id,i.name, s.controller, s.rang, s.published
                                  from components s
                                  join components_info i on i.components_id=s.id and i.languages_id={$this->languages_id}
                                  order by s.id asc
                                  ")->all();
        } catch(\PDOException $e){
            echo $e->getMessage();
        }
    }

    /**
     * switch pub status
     * @param $id
     * @param $published
     * @return bool
     */
    public function pub($id, $published)
    {
        return $this->db->update("components", array('published'=>$published) , " id = {$id} limit 1");
    }

    /**
     * change isfolder status
     * @param $id
     * @return bool
     */
    public function toggleFolder($id)
    {
        $isFolder = ($this->hasChildren($id)) ? 1 : 0;
        return $this->db->update("components", array('isfolder' => $isFolder) , " id = {$id} limit 1");
    }

    /**
     * check children count
     * @param $parent_id
     * @return bool
     */
    public function hasChildren($parent_id)
    {
        return $this->db->select("select count(id) as t from components where parent_id ={$parent_id}")->row('t') > 0;
    }

    /**
     * structure tree
     * @param $parent_id
     * @return array
     */
    public function tree($parent_id)
    {
        return $this->db->select("select s.id,s.isfolder,s.controller,i.name
                                from components s,components_info i
                                where s.parent_id = {$parent_id} and i.components_id=s.id and i.languages_id = {$this->languages_id}
                                order by abs(sort) asc")->all();
    }

    public function installComponent($data, $info)
    {
        $this->db->beginTransaction();
        $id = $this->db->insert('components', $data);
        if($id > 0) {
            $info['components_id'] = $id;
            $ivd = $this->db->insert('components_info', $info);
        }

        if($id && $ivd) {
            $this->db->commit();
        } else {
            $this->db->rollBack();
        }

        return $id;
    }
}