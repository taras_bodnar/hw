<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 26.06.14 13:54
 */

namespace models\engine;
use models\Engine;
use controllers\core\DB;


defined('SYSPATH') or die();

/**
 * Class Modules
 * @package models\engine
 */
class Modules extends Engine {
    /**
     * insert data to table structure
     * @param $data
     * @param $info
     * @return int insert ID
     */
    public function create($data, $info=array())
    {
        return $this->db->insert('modules', $data);
    }

    /**
     * @param $id
     * @param $data
     * @param $info
     * @return bool|string
     */
    public function update($id, $data, $info=array()){
        return $this->db->update('modules', $data, " id={$id} limit 1");
    }

    /**
     * delete item from modules
     * @param $id
     * @return int
     */
    public function delete($id)
    {
      return $this->db->delete("modules", " id = '{$id}' limit 1");
    }

    /**
     * @param $q
     */
    public function exec($q)
    {
        $this->db->customExec($q);
    }
    /**
     * @param $name
     * @return array|mixed
     */
    public function isInstalled($name)
    {
        return $this->db->select("select id from modules where module = '{$name}' limit 1")->row('id');
    }

    public function getInstalled()
    {
        return $this->db->select("select id, module from modules")->all();
    }

    public function createTranslations($code, $info)
    {
//        $this->db->beginTransaction();
        $r=0;
        $id = $this->db->insert("translations", array('code' => $code));

        if($id > 0) {
            foreach ($info as $languages_id => $value) {
                if($this->db->insert("translations_info",array(
                    'translations_id' => $id,
                    'languages_id'    => $languages_id,
                    'value'           => $value
                ))){
                    $r++;
                } else{
                    $this->error(DB::getErrorMessage());
                }
            }
        } else {
            $this->error(DB::getErrorMessage());
        }
        /*
        if($r > 0) {
            $this->db->commit();
        } else {
            $this->db->rollBack();
        }*/

        return $r;
    }

    /**
     * @param $translations
     * @return int
     */
    public function deleteTranslations($translations)
    {
        foreach ($translations as $n=>$v) {
            if(!$this->db->delete("translations", " code = '{$n}' limit 1")){
                return 0;
            }
        }

        return 1;
    }

    /**
     * @return bool
     */
    public function beginTransaction()
    {
        return $this->db->beginTransaction();
    }

    /**
     * @return bool
     */
    public function commit()
    {
        return $this->db->commit();
    }

    /**
     * @return bool
     */
    public function rollBack()
    {
        return $this->db->rollBack();
    }

    /**
     * @param $t
     * @return int
     */
    public function rmTranslations($t)
    {
        return $this->db->delete('translations', " code in ({$t})");
    }

    /**
     * get settings of module
     * @param $name
     * @return array|mixed
     */
    public function getSettings($name){
        return $this->db->select("select settings from modules where module = '{$name}' limit 1")->row('settings');
    }

    /**
     * @param $id
     * @param $data
     * @return bool
     */
    public function updateSettings($id, $data){
        $settings = $this->db->select("
            select settings
            from modules
            where id = '{$id}' limit 1
        ")->row('settings');
        $settings = unserialize($settings);

        foreach ($settings as $k=>$row) {
            if(isset($data[$row['name']])){
                $settings[$k]['value'] = $data[$row['name']];
            }
        }

        $settings = serialize($settings);

        return $this->db->update("modules", array('settings'=>$settings), " id = '{$id}' limit 1");
    }

    /**
     * @param $id
     * @param string $key
     * @return array|mixed
     */
    public function getData($id, $key='*')
    {
        return $this->db->select("select {$key} from modules where id={$id} limit 1")->row($key);
    }
}