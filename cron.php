<?php
/**
 * Created by PhpStorm.
 * User: taras
 * Date: 25.04.17
 * Time: 16:20
 */
include_once("vendor/DB.php");
class Cron
{

    private $db;

    public function __construct()
    {
        $this->db = DB::instance();
    }

    public function index()
    {
//        $this->db->delete('users_online', " lastvisit < SUBTIME(NOW(),'0 0:10:0')");
//        $this->averagePrices();

        $this->formatMark();
        $this->photoDay();
        $this->videoDay();
        $this->deleteOrderAccount();
//        $this->test();
    }

    public function formatMark()
    {
        $this->log("formatMark ".date("Y-m-d H:i:s"));

        $items = $this->db->select("
                Select * from users
        ")->all();

        foreach ($items as $item) {
            $this->db->update("users", array(
                'mark' => $this->points($item['id'])
            ), " id={$item['id']} limit 1");
        }
    }

    public function points($id)
    {
        $point = $this->db->select("
          select u.*, g.rang
          from users u
          join users_group g on g.id=u.users_group_id
          where u.id = '{$id}'
          limit 1")->row('point');;

        //Витягую вартість за послугу

        $s = $this->db->select("
                Select if(price_per_hour>0 or price_per_day>0,1,0) as status from users
                where id={$id} limit 1
        ")->row("status");;

        $point = $s == 1 ? $point + 5 : $point;

        //Витягую знижки
        $s = $this->db->select("
                Select if(discount_per_week>0 or discount_per_month>0 or discount_per_2_month>0,1,0) as status from users
                where id={$id} limit 1
        ")->row("status");
        $point = $s == 1 ? $point + 10 : $point;

        //Витягую портфоліо
        $s = $this->db->select("
                Select count(id) as s from users_uploads
                where users_id = {$id}
        ")->row("s");
        $point = $s > 0 ? $point + 10 : $point;

        //Витягую коментарі
        $s = $this->db->select("select count(id) as t
               from comments
               where workers_id = '{$id}' and status = 'approved'
               ")
            ->row('t');
        $point = $s > 0 ? $point + (10 * $s) : $point;

        //Витягую лайки
        $s = $this->db->select("
                Select count(id) as c from users_like
                where users_id={$id}
        ")->row("c");;
        $point = $s > 0 ? $point + (1 * $s) : $point;

        //витягую Підтверджені замовлення
        $s = $this->db->select("
                SELECT count(id) as s FROM  users_notifications
                where workers_id={$id} and section='orders' and message='CONFIRMED_ORDERS'
        ")->row("s");

        $point = $s > 0 ? $point + (20 * $s) : $point;


        return $point;
    }

    public function deleteOrderAccount()
    {
        $orders = $this->db->select("select * from ordersAccount
          where status_id=1 and UNIX_TIMESTAMP(end_date)<=UNIX_TIMESTAMP(NOW()) ")->all();

        foreach ($orders as $order) {
            if ($order['type'] == 'week' || $order['type'] == 'month') {
                $this->db->update('users', array(
                    'hot' => 0,
                    'hot_date' => 0
                ), " id={$order['users_id']}");
            } elseif ($order['type'] == 'year') {
                $this->db->update('users', array(
                    'gold' => 0,
                    'gold_date' => 0
                ), " id={$order['users_id']}");
            } elseif ($order['type'] == 'oneC') {
                $this->db->delete("users_cities", " users_id={$order['users_id']} and main=0 limit {$order['city_index']}");
            } elseif ($order['type'] == 'twoC') {
                $this->db->delete("users_cities", " users_id={$order['users_id']} and main=0 limit {$order['city_index']}");
            }
            $this->db->update("ordersAccount",array(
                'status_id' => 0
            )," id = {$order['id']} limit 1");
        }
    }

    public function photoDay()
    {
        $this->log("photoDay ".date("Y-m-d H:i:s"));
//        if(date('H:i') != '08:00') return false;
//        return 1;
        $res = $this->db->select("
                SELECT uu.*,CONCAT_WS( ' ' ,u.name, u.surname) as username FROM `img_like` il
                join users_uploads uu on uu.id=il.users_uploads_id
                join users u on u.id = uu.users_id
                WHERE  uu.type='img'  and uu.createdon > DATE_SUB(CURDATE(), INTERVAL 1 WEEK)  and uu.day_check!=1
                group by il.users_uploads_id order by count(il.id) desc limit 1
        ")->row();

        if (!empty($res)) {
            $this->db->update("users_uploads", array(
                "photo_day" => date("Y-m-d", strtotime(' -1 day')),
                "day_check" => 1
            ),
                "id={$res['id']} limit 1");
        }

        return 1;
    }

    public function videoDay()
    {
        $this->log("VideoDay ".date("Y-m-d H:i:s"));

        $res = $this->db->select("
                SELECT uu.*,CONCAT_WS( ' ' ,u.name, u.surname) as username FROM `img_like` il
                join users_uploads uu on uu.id=il.users_uploads_id
                join users u on u.id = uu.users_id
                WHERE  uu.type='video'  and uu.createdon > DATE_SUB(CURDATE(), INTERVAL 1 WEEK)  and uu.day_check!=1
                group by il.users_uploads_id order by count(il.id) desc limit 1
        ")->row();

        if (!empty($res)) {

            $this->db->update("users_uploads", array(
                "photo_day" => date("Y-m-d", strtotime(' -1 day')),
                "day_check" => 1
            ),
                "id={$res['id']} limit 1");
        }


        return 1;
    }

    public function test()
    {
        echo "OK";
    }

    /**
     * @param $msg
     * @return int
     */
    private function log($msg)
    {
        $msg = date('Y-m-d H:i:s') . '    ' . $msg ."\r\n";
        return file_put_contents('logs/cron-'. date('Ymd') .'.log', $msg, FILE_APPEND);
    }
}

$cron = new Cron();
$cron->index();