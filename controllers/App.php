<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 06.05.14 22:34
 */
namespace controllers;

use controllers\core\Controller;
use controllers\core\Load;
use controllers\core\Request;
use controllers\core\Settings;
use controllers\core\Template;
use models\app\Content;
use models\app\Images;
use models\app\Languages;
use models\modules\Catalog;

if (!defined("SYSPATH")) die();

class App extends Controller
{
    /**
     * all translations
     * @var array|null|\stdClass
     */
    protected $translation = array();
    protected static $t;
    /**
     * languages model
     * @var array
     */
    protected $languages = array();

    /**
     * @var int default language ID
     */
    protected $languages_id = 0;
    protected $languages_code = null;

    /**
     * pageID
     * @var int
     */
    protected $id = 1;

    protected $error = array();

    private static $sLoad;

    /**
     * smarty object
     * @var
     */
    protected $template;

    public function __construct()
    {
        parent::__construct();

        $this->languages = $this->load->model('app\Languages');
        $this->images = new Images();

        if (!$this->request->app_init) {
            $this->request->mode = "app"; // engine | app
            $this->request->app_init = true;
            self::$sLoad = $this->load;
        }

        $this->template = Template::instance();

        if (empty($this->translation)) {
            $this->languages_id = $this->languages->getDefault('id');
            $this->setTranslation();
        }


    }

    public function index()
    {
        $params = Request::instance()->param();
        ## визначаєм мову

        if (empty($params['lang']) && $this->request->isGet()) {
            // треба витягнути мову по замовчуванню
            $languages_id = $this->languages->getDefault('id');
        } elseif (isset($params['lang'])) {
            $languages_id = $this->languages->getIdByCode($params['lang']);
        } else {
            $languages_id = self::langugesId();
        }


        // немає в списку доступних , нах
        if (empty($languages_id)) {
            $this->redirect404();
        }

        // задаю мову для всього сайту
        $this->languages_id = $languages_id;

        // визначу сторінку
        $alias = !isset($params['alias']) ? '' : $params['alias'];

        // обрізаю останній слеш
        if (substr($alias, -1, 1) == '/') {
            $alias = substr($alias, 0, -1);
            $this->redirect($alias, 301);
        }

        $mc = new Content();

        // init routes
        $r = $mc->getRoute($alias);
        if (!empty($r)) {
            header($r['header']);
            header("Location: {$r['url']}");
            die();
        }
        if(!empty($alias)){
            // перевірити чи даний урл є урл виконавця
            $cat = new Catalog();
            $users_id = $cat->getUserIdByUrl($alias);
            if(!empty($users_id)){
                $id = 45;
            } else {
                $id = $mc->getIdByAlias($alias, $languages_id);
            }
        } else {
            $id = $mc->getIdByAlias($alias, $languages_id);
        }

        // невірний ІД
        if (!$id) {
            $this->redirect404();
        }

        // витягну дані про сторінку
        $page = $mc->getData($id, $languages_id);

        if (!$page) {
            $this->redirect404();
        }

        // cover img
        $page['image'] = $this->images->cover($page['id']);

        // записую в реквест ід сторінки і мову
        $this->request->set('id', $page['id']);
        $this->request->set('languages_id', $page['languages_id']);
        $this->request->set('page', $page);

        // записую ід мови в сесію
        $l_data = $this->languages->getData($languages_id);
        $lcode = $l_data['code'];


        self::langugesId($languages_id);
        self::langugesCode($lcode);

        $this->languages_id = $languages_id;
        $this->languages_code = $lcode;

        // вантажу переклади
        $this->setTranslation();

        // зображення для сторінки
        $page['image'] = $this->images->cover($id);


        // для блогу мітки і автор
//        $this->dump($params);
        if (isset($params['tag'])) {
            $page['tag'] = $mc->getTagName($params['tag']);
        }

        if ($page['id'] == 45) {
            $this->request->param('p', $users_id);
//            $users_id = $params['p'];
            if (!empty($users_id)) {
                $page['author'] = $mc->getAuthor($users_id);
                $page['author'] .= ' ' . $mc->getWorkerCategory($users_id);
                $page['author'] .= ' ' . $mc->getWorkerCity($users_id);
            }
        }

        $page['url'] = $mc->makeUrl($page['id']);

//        if (isset($params['p'])) {
//            $page['canonical'] = $page['url'];
//        }

        if (isset($params['handle'])) {
            $page['canonical'] = $params['handle'];
        } elseif($page['url'] == 'vykonavets'){ //для сторінок з конкретним виконавцем, тому що $page['id'] цих сторінок завжди 45, але url різний
            $page['canonical'] = $params['alias'];
        } elseif($page['url'] == 'ru/ispolnitel'){ //для сторінок з конкретним виконавцем, тому що $page['id'] цих сторінок завжди 45, але url різний
            $page['canonical'] = 'ru/' . $params['alias'];
        } else {
            $page['canonical'] = $page['url'];
        }

        // підключення модуля якщо є

        if (!empty($page['module'])) {
            $page['module'] = "[[mod:{$page['module']}]]";
        }
        // вміст шаблону
//        $this->template->assign($page);
        $this->template->assign('current_languages', $l_data);

        $this->template->assign('page', $page);
        $this->template->assign('appurl', APPURL);
        $this->template->assign('id', $page['id']);

        $tpl = $this->template->fetch($page['template']);

        // парсим контент
        $tpl = $this->parse($tpl);
        // id > url
        $tpl = $this->makeFriendlyUrl($tpl);

        $this->request->content('body', $tpl);

    }

    private function setTranslation()
    {
        $mt = $this->load->model('app\Translations');
        // для зручності виклику
        $this->translation = $mt->setLanguagesId(self::langugesId())->get();
        // доступ для шаблонів
        $this->template->assign('t', $this->translation);
        self::$t = $this->translation;
    }

    /**
     * @param $id
     * @return array|mixed
     */
    protected function mkUrl($id)
    {
        $mc = new Content();
        return $mc->makeUrl($id);
    }

    /**
     * languages ID
     * @param null $id
     * @return null
     */
    public static function langugesId($id = null)
    {
        if (!isset($_SESSION['app'])) {
            $_SESSION['app'] = array();
            $_SESSION['app']['languages'] = array();
            $l = new Languages();
            $_SESSION['app']['languages']['id'] = $l->getDefault('id');
        }
        if ($id) {
            $_SESSION['app']['languages']['id'] = $id;
        }

        return isset($_SESSION['app']['languages']['id']) ? $_SESSION['app']['languages']['id'] : null;
    }

    /**
     * languages Code
     * @param null $code
     * @return null
     */
    public static function langugesCode($code = null)
    {
        if (!isset($_SESSION['app'])) {
            $_SESSION['app'] = array();
            $_SESSION['app']['languages'] = array();
        }
        if ($code) {
            $_SESSION['app']['languages']['code'] = $code;
        }

        return isset($_SESSION['app']['languages']['code']) ? $_SESSION['app']['languages']['code'] : null;
    }

    /**
     * @param $k
     * @param null $v
     * @return mixed
     */
    public static function session($k, $v = null)
    {
        if (!isset($_SESSION['app'])) {
            $_SESSION['app'] = array();
        }
        if ($v) {
            $_SESSION['app'][$k] = $v;
        }

        return isset($_SESSION['app'][$k]) ? $_SESSION['app'][$k] : null;
    }

    /**
     * парсить контент
     *
     * модуль <br/>
     * [{mod:Shop}]
     *
     * модуль, метод
     * [{mod:Shop::hello}]
     *
     * модуль, метод, параметри
     * [{mod:Shop::hello(7,3,6)}]
     *
     * виклик чанку
     * [{chunk:test}]
     *
     * @param $ds string document source
     * @return mixed
     */
    public function parse($ds)
    {
        $self = $this;
        $t = $this->translation;
        $themes_path = Settings::instance()->get('themes_path');

        $current = Settings::instance()->get('app_theme_current');
        $chunks_path = Settings::instance()->get('app_chunks_path');
        $chunks_path = $themes_path . $current . '/' . $chunks_path;

        $template_url = APPURL . $themes_path . $current . '/';
        $base_url = APPURL;

        $ds = preg_replace_callback(
            '@\[\[([app|mod|chunk]+):([a-zA-Z_0-9\-]+)?(::[a-zA-Z_0-9]+)??(\([0-9a-zA-z,]+\))?\]\]@s',
            function ($data) use ($self, $t, $template_url, $base_url, $chunks_path) {
                $self->template = Template::instance();
                $self->load = Load::instance();
                $out = '';
                $app = new \controllers\App();
                switch ($data[1]) {
                    case 'mod': // модуль
                        $module = '';
                        $action = '';
                        $params = array();
                        if (isset($data[2]) && !empty($data[2])) {
                            $module = $data[2];
                        }
                        if (isset($data[3]) && !empty($data[3])) {
                            $action = ltrim($data[3], '::');
                        }
                        if (isset($data[4]) && !empty($data[4])) {
                            $params = explode(',', trim($data[4], '()'));
                        }

                        $out = $self->load->module($module, $action, $params);
                        $out = $app->parse($out);
                        break;
                    case 'chunk': // чанк
                        $out = $self->template->fetch($chunks_path . $data[2]);
                        $out = $app->parse($out);
                        break;
                    case 'app': // виклик функції метатегів
                        if (isset($data[2])) {
                            if ($data[2] == 'metatags') {
                                $out = $self->metatags();
                            } else {
                                $out = '<b>Error.</b> invalid parse: ' . $data[2];
                            }
                        }
                        break;
                    default:
                        return 'invalid parse, use pattenrn:
                          @\[{([mod|chunk]+):([a-zA-Z_0-9]+)?(::[a-zA-Z_0-9]+)?}\]@s';
                        break;
                }
                return $out;
            },
            $ds
        );
        return $ds;
    }

    /**
     * @param $ds
     * @return mixed
     * @throws core\Exceptions
     */
    protected function makeFriendlyUrl($ds)
    {
        $mc = $this->load->model('app\Content');
        $current_languages_id = $this->request->get('languages_id');
        if (!$current_languages_id) {
            $current_languages_id = self::langugesId();
        }
//        мову по замовчуванню
        $_languages_id = $this->languages->getDefault('id');

        // список мов
        $_languages = $this->languages->get();
        $languages = array();
        foreach ($_languages as $row) {
            $languages[$row['id']] = $row['code'];
        }

        $ds = preg_replace_callback(
            '/(href|action)=\"([^\"]*)\"/siU',
            function ($data) use ($mc, $_languages_id, $languages, $current_languages_id) {
//                echo '<pre>';print_r($data);echo '</pre>';
                $id = 0;
                $languages_id = 0;
                $p = 0;
                $tag = '';
                $out = '';
                $qs = array();
                $action = $data[1];
                // пошук по системних параметрах
                if (strpos($data[2], ';') !== false) {
                    // пошук по системних параметрах
                    $a = explode(';', $data[2]);
//                    $this->dump($a);
//                    $qs = end($a);
//                    $qs = '?'. ltrim($qs, '&');
                    if (isset($a[1])) {
                        foreach ($a as $i => $row) {
                            if ($i == 0) {
                                $id = (int)$row;
                            } else {
                                if (strpos($row, 'l=') !== false) {
                                    $languages_id = ltrim($row, 'l=');
                                    $languages_id = rtrim($languages_id, ';');
                                    continue;
                                } elseif (strpos($row, 'p=') !== false) {
                                    $p = ltrim($row, 'p=');
                                    $p = rtrim($p, ';');
                                    continue;
                                } elseif (strpos($row, 'tag=') !== false) {
                                    $tag = ltrim($row, 'tag=');
                                    $tag = rtrim($tag, ';');
                                    continue;
                                } elseif (!empty($row)) {
                                    $qs[] = $row;
                                }
                            }
                        }
                    }
                } else if (preg_match('@^([0-9]+)$@', $data[2], $mathes)) {
                    $id = (int)$mathes[1];
                } else {
                    return $action . '="' . $data[2] . '"';
                }
//echo 'RES: ID:', $id, ' L:', $languages_id, ' P: ', $p;
                // якщо вказана мова
                if ($languages_id > 0) {
                    // якщо не мова по замовчванню то присвою префік

                    if ($languages_id != $_languages_id) {
                        $out .= $languages[$languages_id] . '/';
                    }


                } else {

                    // використаю мову по замовчуванню
                    if ($_languages_id != $current_languages_id) {
                        $out .= $languages[$current_languages_id] . '/';
                    }

                    $languages_id = $current_languages_id;

                }

                if ($id > 0) {
//                    echo $id, '++<br>';
                    if ($id == 1) {
                        $alias = '/';
                    } else if ($id == 45 && $p > 0) {
//                        $alias = $out . $mc->getAliasById($id, $languages_id);
                        $mCatalog = new \models\modules\Catalog();
                        $alias = $out . '/' . $mCatalog->getUrlById($p);
                        if(!empty($qs)){
                            $alias .= '?'. implode('&', $qs);
                        }

                        $alias = str_replace('//','/', $alias);
                        $result = 'href="'.$alias.'"';
                        return $result;
                    } else {
                        $alias = $mc->getAliasById($id, $languages_id);
                    }

                    $out .= $alias;
                }
                if ($p > 0) {
                    $out .= '/' . $p;
                }

                if ($tag != '') {
                    $out .= '/tag/' . $tag;
                }
//echo'---<---
                if (!empty($qs)) {
                    $out .= '?' . implode('&', $qs);
                }

                $out = str_replace('//', '/', $out);
                $result = $action . '="' . $out . '"';
                return $result;
            },
            $ds
        );

        return $ds;
    }


    public function metatags()
    {
        $service_id = $this->request->get('id');

        $filter = isset($_GET['filter'])?$_GET['filter']:'';
        $city_id = isset($filter['city_id']) && !empty($filter['city_id'])?$filter['city_id']:'';

        $meta = array();

        if (isset($city_id) && !empty($city_id)) {
            $model = new \models\modules\Catalog();
            $meta = $model->getSeo($service_id, $city_id);
        }

//$this->dump($meta);
        Template::instance()->assign(array(
            'userId' => $this->request->get('p'),
            'meta' => $meta
        ));

        return Template::instance()->fetch('meta.tpl');
    }

    /**
     * 404 redirect
     * // todo підставити 404 сторінку
     */
    public function redirect404()
    {
//        throw new \Exception('404');
        $id = Settings::instance()->get('page_404');

        $mc = $this->load->model('app\Content');
        $alias = $mc->getAliasById($id, $this->languages_id);

        $url = APPURL . $alias;
        header("HTTP/1.0 404 Not Found");
        header("Location: {$url}");
        die();
    }

    /**
     * реиректить на урл з використаннням заголовків
     * @param $url
     * @param $header
     */
    protected function redirect($url, $header = '')
    {
        $url = APPURL . $url;

        switch ($header) {
            case 301:
                header('HTTP/1.1 301 Moved Permanently');
                break;
            default:
                break;
        }

        header("Location: {$url}");
        die();
    }
}