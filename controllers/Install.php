<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 04.09.14 23:40
 */

namespace controllers;

use controllers\core\Controller;

defined("SYSPATH") or die();

/**
 * Class Module
 * @package controllers
 */
class Install extends Controller {

    private $title = 'Інсталяція системи OYi.Engine 6.';
    private $content='';

    public function __construct()
    {
        parent::__construct();
        $this->request->mode = 'install';
    }

    /**
     * @return mixed|string
     */
    public function index()
    {
        if($this->request->isPost()){

            if(isset($_POST['step'])) {
                if($_POST['step'] == 0) {
                    $this->step0();
                }elseif($_POST['step'] == 1) {
                    $this->step1();
                }elseif($_POST['step'] == 2) {
                    $this->step2Process();
                }elseif($_POST['step'] == 5) {
                    $this->success();
                }
            }
        }

        if($this->check()){
            $this->content = $this->view('install/terms');
        }

        $this->output();
    }

    private function step0()
    {
        $this->content = $this->view('install/step1');
        $this->output();
    }

    private function check()
    {
        if (version_compare(phpversion(), '5.3.0', '<') == true) {
            $this->error[] = 'OYI.ENgine працює на php версії не нижче 5.3';
        }

        $cpath = SYSPATH . 'config.sample';
        if(!is_writable($cpath)) {
            $this->error[] = 'Файл конфігу не доступний для запису. <br>
                              Змініть права на файл ' . $cpath . ' на 0777, а після інсталяції на 0644';
        }

        if(!is_writable(DOCROOT . 'uploads/content')) {
            $this->error[] = 'Папка завантажень не доступна для запису <br>
                              Змініть права на файл /uploads/content на 0775';
        }

        if(!is_writable(DOCROOT . 'uploads/images')) {
            $this->error[] = 'Папка завантажень не доступна для запису <br>
                              Змініть права на файл /uploads/images на 0775';
        }

        $this->content = $this->view('install/check', array('error' => $this->error));

        return empty($this->error);
    }

    private function step1()
    {
        $conf = $_POST['data']; $db=null;
        try{
            $db = new \PDO("mysql:host={$conf['host']};dbname={$conf['name']}",$conf['user'],$conf['pass']);
            $db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $db->exec("SET NAMES utf8");
        }
        catch(\PDOException $e) {
            $this->error[] = $e->getMessage();
        }
        if($db) {
            $_SESSION['inst']['db'] = $conf;
            try{
                // імпортую БД
                $file = file_get_contents(SYSPATH . '/db.sql');
                $data = explode(';',$file);
                foreach ($data as $k=>$query) {
                    if(empty($query)) continue;
//                    echo $query, '<br>------<br>';
                    $db->exec($query);
                }
            }
            catch(\PDOException $e) {
                $this->error[] = 'Import error: ' . $e->getMessage() ;
            }
        }

        if(empty($this->error)) {
            $this->step2();
        }

        $this->content = $this->view(
            'install/step1',
            array(
                'error' => $this->error
            )
        );

        $this->output();
    }
    private function step2()
    {
        $this->content = $this->view(
            'install/step2',
            array(
                'error' => $this->error
            )
        );

        $this->output();
    }

    private function step2Process()
    {
        $conf = $_SESSION['inst']['db'];
        $data = $_POST['data'];
        $pass = crypt($data['pass']);

        $now = date('Y-m-d H:i:s');

        try{
            $db = new \PDO("mysql:host={$conf['host']};dbname={$conf['name']}",$conf['user'],$conf['pass']);
            $db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $db->exec("SET NAMES utf8");
        }
        catch(\PDOException $e) {
            $this->error[] = $e->getMessage();
        }

        if(empty($this->error)){
            $languages = array(
                'uk' => 'Українська',
                'ru' => 'Русский',
                'en' => 'English',
                'pl' => 'Polska',
                'de' => 'Deutch',
            );
            try{
                $lang = $languages[$data['language']];
                $db->exec("update languages set `code`='{$data['language']}', `name`='{$lang}' where id=1 limit 1");
            } catch(\PDOException $e) {
                $this->error[] = 'E1:' . $e->getMessage();
            }
        }
        if(empty($this->error)){
            try{
                $db->exec("
                    insert into users (users_group_id, languages_id,name,email,password,createdon)
                    values (1, 1, '{$data['name']}','{$data['email']}', '{$pass}','{$now}') ");
            } catch(\PDOException $e) {
                $this->error[] = 'E2:' . $e->getMessage();
            }
        }
        if(empty($this->error)){
            try{
                $db->exec("INSERT INTO users_group_info (id, users_group_id, languages_id, name, description) VALUES (NULL, '1', '1', 'Admins', NULL)");
            } catch(\PDOException $e) {
                $this->error[] = 'E2UG:' . $e->getMessage();
            }
        }
        if(empty($this->error)){
            try{
                $db->exec("
                insert into content
                    (type_id, owner_id, templates_id, published, auto, createdon)
                values
                    (1, 1, 1, 1, 0, '{$now}')
                ");
            } catch(\PDOException $e) {
                $this->error[] = $e->getMessage();
            }
        }
        if(empty($this->error)){
            try{
                $db->exec("insert into content_info (content_id,languages_id,name,title) values (1,1,'{$data['name']}','{$data['name']}')");
            } catch(\PDOException $e) {
                $this->error[] = $e->getMessage();
            }
        }

        try{
            $c_sample = SYSPATH . 'config.sample';
            $cpath = SYSPATH . 'config.php';
            // запишу конфіг
            $config = file_get_contents($c_sample);

            $config = str_replace
            (
                array(
                    '%host%','%db%','%user%','%pass%'
                ),
                array(
                    $conf['host'], $conf['name'], $conf['user'], $conf['pass']
                ),
                $config
            );

            $h = fopen($cpath,'w+');

            if (fwrite($h, $config) === FALSE) {
                $this->error[] = 'Неможу записати конфіг';
            }

        }
        catch(\Exception $e) {
            $this->error[] = $e->getMessage();
        }

        if(empty($this->error)){

            $this->success();
        } else {

            $this->content = $this->view(
                'install/step2',
                array(
                    'error' => $this->error
                )
            );

            $this->output();
        }
    }

    private function success($expire='')
    {
        $this->content = $this->view('install/success', array('expire'=>$expire));

        $this->output();
    }

    private function output()
    {
        echo $this->view(
            'install/index',
            array(
                'title' => $this->title,
                'content' => $this->content
            )
        );
        die();
    }

    /**
     * @param $name
     * @param array $vars
     * @return string
     */
    private function view($name, array $vars = null)
    {
        $file = $_SERVER['DOCUMENT_ROOT'] . '/themes/'. $name .'.php';

        if(is_readable($file)){

            ob_start();

            if(isset($vars)){
                extract($vars);
            }

            require($file);

            return ob_get_clean();
        }
        die("View: <b>{$file}</b> issues");
    }
}