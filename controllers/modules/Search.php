<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 27.09.14 12:31
 */

namespace controllers\modules;
use controllers\Module;

defined("SYSPATH") or die();

/**
 * Class Search
 * @name OYi.Search
 * @description Search module
 * Class Search
 * @package modules
 * @author wmgodyak mailto:wmgodyak@gmail.com
 * @version 1.0
 * @copyright &copy; 2014 Otakoyi.com
 */

class Search extends Module {

    private $m;
    /**
     * search result page id
     * @var int
     */
    private $resultID = 23;

    public function __construct()
    {
        parent::__construct();

        $this->m = $this->load->model('modules\Search');

    }

    public function index(){}

    /**
     * @return string
     */
    public function form()
    {
        return $this->template->fetch('modules/search/form');
    }

    /**
     * результати пошуку
     * @return string
     */
    public function results()
    {
        if(!isset($_GET['sq'])) return '';

        $q = trim($_GET['sq']); $t=$this->translation;
        $e = array(); $m = array();
        if(strlen($q) <3){
            $e[] = $t['search_min_word_len'];
        }
        if(empty($e)){
            $uq = explode(' ', $q);

            for($i=0;$i<count($uq);$i++){
                if(empty($uq[$i])) continue;
                $this->m->setWhere( " and i.title like '%". $uq[$i] ."%'" );
            }

            $url = $this->request->get('id');
            $total = $this->m->total();
            if($total == 0 ){
                $e[] = $t['search_not_found'] ;
            } else {
                $paginator = new Paginator($total, 10, $url . ';', 7);

                $m = str_replace('{t}', $total, $t['search_found']);

                $limit = $paginator->getLimit();
                $this->m->setLimit($limit['start'], $limit['num']);

                $results = $this->m->get();
//                $this->dump($items);
                $this->template->assign('m', $m);
                $this->template->assign('results', $results);
                $this->template->assign('pages', $paginator->getPages());
            }
        }
        $this->template->assign('e', implode('<br>', $e));

        return $this->template->fetch('modules/search/results');
    }


    public function install(){}
    public function uninstall(){}
}