<?php
/**
 * Company Otakoyi.com
 * Author: wg
 * Date: 02.01.15 16:22
 */

namespace controllers\modules;

use controllers\Module;

defined("SYSPATH") or die();

/**
 * Class Currency
 * @name Currency
 * @description Currency module
 * Class Currency
 * @package modules
 * @author wmgodyak mailto:wmgodyak@gmail.com
 * @version 1.0
 * @copyright &copy; 2014 Otakoyi.com
 */
class Currency extends Module {

    private $m;

    public function __construct()
    {
        parent::__construct();

        $this->m = $this->load->model('modules\Currency');
    }
    public function index(){}

    public function nav()
    {
        $this->template->assign('items', $this->m->get());
        $this->template->assign('curency_id', isset($_SESSION['app']['currency']['id']) ? $_SESSION['app']['currency']['id'] : 0);
        return $this->template->fetch('modules/currency/nav');
    }

    public function change()
    {
        if(isset($_POST['id']) && !empty($_POST['id'])){
            $_SESSION['app']['currency']=array();
            $_SESSION['app']['currency']['id'] = $_POST['id'];
        }
        return 1;
    }

    public function install()
    {
    }

    public function uninstall()
    {
    }
}