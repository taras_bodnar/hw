<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 27.09.14 12:31
 */

namespace controllers\modules;
use controllers\Module;
use models\app\Content;
use models\app\Features;
use models\app\Guides;
use models\app\Images;
use models\modules\Car;

defined("SYSPATH") or die();

/**
 * Class Shop
 * @name OYi.Shop
 * @description Shop module
 * Class Shop
 * @package modules
 * @author wmgodyak mailto:wmgodyak@gmail.com
 * @version 1.0
 * @copyright &copy; 2014 Otakoyi.com
 */

class Shop extends Module {

    private $model;
    private $images;
    private $features;
    private $guides;
    private $car;

    private $title;
    private $name;
    private $uri;
    private $sel_parts;
    private $cat_id;

    private $total;
    private $minp=0;
    private $maxp=0;
    private $items_per_page=7;
    private $products=array();
    private $pids = array();

    private $total_left = 0;
    private $more_uri = '';
    private $p=0;
    private $rq;

    public function __construct()
    {
        parent::__construct();

        $this->model    = new \models\modules\Shop();
        $this->images   = new Images();
        $this->features = new Features();
        $this->guides   = new Guides();
        $this->car      = new Car();

        // фільтрування
        $sort = isset($_REQUEST['sort']) ? (int)$_REQUEST['sort'] : null;
        $order = isset($_REQUEST['order']) ? $_REQUEST['order'] : null;
        $order = $order == 'asc' ? 'desc' : 'asc';

        if($sort){
//            $this->model->debug();
            switch($sort){
                case 6: // новинки
                    $this->model->setOrderBy(" id $order ");
                    break;
                case 7: // price
                    $this->model->setOrderBy(" price $order ");
                    break;
                case 2: // name
                    $this->model->setOrderBy(" name $order ");
                    break;
                case 8: // availability
                    $this->model->setOrderBy(" availability desc ");
                    break;
                default:
                    $this->model->setOrderBy(" c.id desc ");
                    break;
            }
        }

        $request = $_REQUEST;
        $cid = isset($request['cid']) ? (int)$request['cid'] : null;
        if($this->request->isPost()){
            if($cid){
                $c   = new Content();
                $page = $c->getData($cid, $this->languages_id);

                $this->name  = $page['name'];
                $this->title = $page['title'];
                $this->uri   = $cid . ';';
            }
        } else {
            $page = $this->request->param('page');
            $this->name  = $page['name'];
            $this->title = $page['title'];
            $cid = $page['id'];
        }

        $this->cat_id = $cid;
        $this->more_uri = $cid;

        if(isset($_POST['more_uri'])){
            $this->items_per_page=7;
            $a = explode('/', $_POST['more_uri']);
            if(isset($a[1])){
                $this->p = (int)$a[1];
            }
//            $this->p++;
        }

        unset($request['skey'], $request['cid']);
        if(isset($request['sort'])) unset($request['sort']);
        if(isset($request['order'])) unset($request['order']);
        if(isset($request['more_uri'])) unset($request['more_uri']);

        $this->rq = http_build_query($request);
        $this->uri .= $this->rq;

        $this->uri = 'href="'. $this->uri .'"';
        $this->uri = $this->makeFriendlyUrl($this->uri);
        $this->uri = str_replace(array('href="','"'), array(), $this->uri);

        if(isset($_REQUEST['q'])){

            $sq = trim(strip_tags($_REQUEST['q']));
            if(strlen($sq) > 2){

                $q = explode(' ', $sq); $sql = array();
                foreach ($q as $k=>$v) {
                    $v = trim($v);
                    $v = strip_tags($v);

                    $sql[] = " i.name like '%$v%' ";
                }

                // задаю критерії пошуку
                if(!empty($sql)){
                    $this->model->setWhere(" (". implode(' and ', $sql) .") ");
                }
            }
        }

        $filter = isset($_REQUEST['filter']) ? $_REQUEST['filter'] : null;

        if($filter){

            if(isset($filter['minp']) && isset($filter['maxp']) && $filter['maxp'] > 0){
                $minp = (int)$filter['minp'];
                $maxp = (int)$filter['maxp'];

                $this->model->setWhere(" price between {$minp} and {$maxp}");

                $this->sel_parts[] = "{$minp} - {$maxp} грн.";
            }

            if(isset($filter['car']['vendor_id']) && $filter['car']['vendor_id'] > 0){
                $vendors_id = (int)$filter['car']['vendor_id'];
                $model_id   = (int)$filter['car']['model_id'];
                $year_id    = (int)$filter['car']['year_id'];
                $mod_id     = (int)$filter['car']['modification_id'];
                if($vendors_id > 0 && $model_id > 0 && $year_id > 0 && $mod_id > 0){

                    $this->sel_parts[] = $this->model->getVendorName($vendors_id);
                    $this->sel_parts[] = $this->model->getModelName($model_id);
                    $this->sel_parts[] = $this->model->getYearName($year_id);
                    $this->sel_parts[] = $this->model->getModificationName($mod_id);


                    $this->model->setJoin("join products_cars car on
                                            car.vendors_id          = '{$vendors_id}'
                                            and car.models_id       = '{$model_id}'
                                            and car.years_id        = '{$year_id}'
                                            and car.modification_id = '{$mod_id}'
                                            and car.products_id=po.products_id
                        ");
                }
            }

            $selected_features = array();

            if(isset($filter['f'])) {
                foreach ($filter['f'] as $features_id => $a) {
                    $features = $this->features->getItem($features_id, true);
//                    $this->dump($features);

                    foreach ($features['values'] as $k=>$v) {
                        if(is_array($a) && in_array($v['id'], $a)){
                            $this->sel_parts[] = $v['value'];
//                            unset($features['values'][$k]);
                        } elseif($a == $v['id']){
                            $this->sel_parts[] = $v['value'];
//                            $this->sel_parts[] = $features['name'] . ': ' . $v['value'];
                        }
                    }
//                    $selected_features[] = $features;

                    $type = $features['type'];
//                    echo $type, '|';
                    if($type == 'sm' || $type == 'select'){
                        if(is_array($a)){
                            $in = "fcv{$features_id}.features_values_id in (". implode(',', $a) .")";
                        }else {
                            $a = (int)$a;
                            $in = "fcv{$features_id}.features_values_id = '{$a}'";
                        }
//                        $this->dump($a); continue;
                        if(empty($a)) continue;
                        $this->model->setJoin("
                            join features_content_values fcv{$features_id} on
                            {$in}
                            and fcv{$features_id}.features_id='{$features_id}'
                            and fcv{$features_id}.content_id=po.products_id
                        ");

                    } elseif($type == 'text'){
                        foreach ($a as $k=>$v) {
                            $a[$k] = "'$v'";
                        }
                        $in = implode(',',$a);

                        $this->model->setJoin("
                        join features_content_values fcv{$features_id} on
                        fcv{$features_id}.value in({$in})
                        and fcv{$features_id}.features_id='{$features_id}'
                        and fcv{$features_id}.content_id=po.products_id
                        and fcv{$features_id}.languages_id = {$this->languages_id}
                    ");
                    } elseif($type == 'number'){
                        foreach ($a as $k=>$v) {
                            $a[$k] = "'$v'";
                        }
                        $in = implode(',',$a);
                        $this->model->setJoin("
                        join features_content_values fcv{$features_id} on
                        fcv{$features_id}.value in({$in})
                        and fcv{$features_id}.features_id='{$features_id}'
                        and fcv{$features_id}.content_id=po.products_id
                        and fcv{$features_id}.languages_id = 0
                    ");
                    }
                }
//                $this->dump($selected_features);
                $this->template->assign('selected_features', $selected_features);
            }
        }

        if(!empty($this->sel_parts)){
            $this->name  .= ': '. implode(' / ', $this->sel_parts);
            $this->title .= ': '. implode(' / ', $this->sel_parts);
        }

        $this->template->assign('cat_id', $this->cat_id);
    }

    public function index()
    {
        $page = $this->request->param('page');
        if(!$page && $this->cat_id){
            return $this->displayProducts($this->cat_id);
        }

        if($page['isfolder']){ // містить підкатегорії
            return $this->displayCategories($page['id']);
        } else{ // поазую товари
            return $this->displayProducts($page['id']);
        }
    }

    /**
     * @return string
     */
    public function mainFilter()
    {
        // шини
        $tires = array();
        // ширина
        $tires['width']   = $this->features->getItem(4, true);
        // профіль
        $tires['height']  = $this->features->getItem(5, true);
        // діаметр
        $tires['diametr'] = $this->features->getItem(6, true);
        // сезонність
        $tires['season']  = $this->features->getItem(7, true);
        // бренд
        $tires['vendor']  = $this->features->getItem(3, true);

        // диски
        $disc = array();
        // ширина
        $disc['width']   = $this->features->getItem(11, true);
        // тип
        $disc['type']    = $this->features->getItem(12, true);
        // діаметр
        $disc['diametr'] = $this->features->getItem(10, true);
        // сезонність
        $disc['season']  = $this->features->getItem(7, true);
        // бренд
        $disc['vendor']  = $this->features->getItem(9, true);
        // rcd
        $disc['pcd']     = $this->features->getItem(13, true);
        // et
        $disc['et']      = $this->features->getItem(14, true);

//        $this->dump($tires);die();
        $car_vendors = $this->car->getVendors();

        $this->template->assign('tires', $tires);
        $this->template->assign('disc', $disc);
        $this->template->assign('car_vendors', $car_vendors);
        return $this->template->fetch('modules/shop/main_filter.tpl');
    }

    /**
     * @return string
     */
    public function filterGetModels()
    {
        $vendor_id = (int)$_POST['vendor_id'];
        $out = '';
        foreach ($this->car->getModels($vendor_id) as $item) {
            $selected = isset($_POST['model_id']) && $_POST['model_id'] == $item['id'] ? 'selected' : '';
            $out .= "<option {$selected} value='{$item['id']}'>{$item['model']}</option>";
        }

        return $out;
    }

    /**
     * @return string
     */
    public function filterGetYears()
    {
        $model_id = (int)$_POST['model_id'];

        $out = '';
        foreach ($this->car->getYears($model_id) as $item) {
            $selected = isset($_POST['year_id']) && $_POST['year_id'] == $item['id'] ? 'selected' : '';
            $out .= "<option {$selected} value='{$item['id']}'>{$item['year']}</option>";
        }

        return $out;
    }

    /**
     * @return string
     */
    public function filterGetModification()
    {
        $year_id = (int)$_POST['year_id'];

        $out = '<option value="">'. $this->translation['filter_sel_all'] .'</option>';
        foreach ($this->car->getModification($year_id) as $item) {
            $selected = isset($_POST['modification_id']) && $_POST['modification_id'] == $item['id'] ? 'selected' : '';
            $out .= "<option {$selected} value='{$item['id']}'>{$item['modification']}</option>";
        }

        return $out;
    }

    public function vendors()
    {
        $features_id = $this->request->get('id') == 26 ? 3 : 9;
        $cat_id = $this->request->get('id') == 26 ? 7 : 8;
        $letters = $this->model->getVendorsLetters($features_id);

        // розбиваю на блоки по 6 колонок
        $letters = array_chunk($letters, 6);

        foreach ($letters as $i=>$row) {
            foreach ($row as $k=>$item) {
                $letters[$i][$k]['items'] = $this->model->getVendorsLettersItems($features_id, $item['letter']);
            }
        }

//        $this->dump($letters);

        $this->template->assign('features_id', $features_id);
        $this->template->assign('cat_id', $cat_id);
        $this->template->assign('letters', $letters);
        return $this->template->fetch('modules/shop/vendors');
    }

    public function searchForm()
    {
//        $page = $this->request->get('page');
         $action_id=7;

        $this->template->assign('action_id', $action_id);
        return $this->template->fetch('modules/shop/search_form');
    }

    /**
     * @return string
     */
    public function search()
    {
        if(!isset($_REQUEST['q'])) return '';
        $res = array();
        $sq = trim(strip_tags($_REQUEST['q']));

        if(strlen($sq) > 2 && $this->request->isXhr()){
            // ватсновлюю ліміт
            $this->model->setLimit(0, 10);

            foreach ($this->model->getProducts() as $i=>$row) {
                $row['img'] = $this->images->cover($row['id'], 'thumbnails');
                $row['url'] = $this->model->makeAlias($row['id']);
                $row['price'] = round($row['price'], 2);

                $res[] = $row;
            }

        }
        return json_encode(array('source' => $res));
    }

 /**
     * @param $parent_id
     * @return string
     */
    private function displayCategories($parent_id)
    {
        $items = $this->model->getCategories($parent_id);
        foreach($items as $k=>$item){
            $items[$k]['img'] = $this->images->cover($item['id'], 'cat');
            if($item['isfolder']){
                $items[$k]['children'] = $this->model->getCategories($item['id'], 5);
            }
        }

        $this->template->assign('items', $items);
        return $this->template->fetch('modules/shop/categories');
    }
    /**
     * @param $categories_id
     * @return string
     */
    public function displayProducts($categories_id=0)
    {
        $active_tab = isset($_REQUEST['filter']['car']['vendor_id']) && $_REQUEST['filter']['car']['vendor_id'] > 0 ? 2 : 1;

        if($categories_id == 0 && $this->request->isPost()){
            $categories_id = (int)$_POST['cid'];
        }

        // привязка до категорії
        $this->model->setJoin("join products_categories pc on pc.cid={$categories_id} and pc.pid=po.products_id");

        // загальна кількість товарів
        $total = $this->model->getTotalProducts();
        $this->total = $total['total'];
        $this->minp  = $total['minp'];
        $this->maxp  = $total['maxp'];

        if($this->total > 0){
            // пагінація
//            $paginator = new Paginator($this->total, $this->items_per_page, $categories_id.';');
//            $limit = $paginator->getLimit();
//
            // ватсновлюю ліміт
            $this->model->setLimit($this->p * $this->items_per_page, $this->items_per_page);
//            $this->model->setLimit($limit['start'], $limit['num']);

            foreach ($this->model->getProducts() as $item) {
                $this->products[] = $this->item($item, 'item2');
                $this->pids[]     = $item['id'];
            }

//            if($this->total>$this->items_per_page){
//                $this->template->assign('pagination', $paginator->getPages());
//            }
        }

        $this->p++;
        $this->more_uri .= ";p=$this->p";

        $this->more_uri = 'href="'. $this->more_uri .'"';
        $this->more_uri = $this->makeFriendlyUrl($this->more_uri);
        $this->more_uri = str_replace(array('href="','"'), array(), $this->more_uri);
        if(!empty($this->rq)){
            $this->more_uri .= '?'.$this->rq;
        }

        $this->total_left = $this->total - $this->p * $this->items_per_page;
        if($this->total_left < 0) $this->total_left = 0;
        $ipp = $this->items_per_page > $this->total_left ? $this->total_left : $this->items_per_page;
        $this->template->assign('ipp', $ipp);

        if($this->request->isPost()){

            return json_encode(array(
                'items' => $this->products,
                't'     => $this->total,
                'c'     => $this->productsList(false),
                'f'     => $this->filter($categories_id),
                'tab'   => (int)$active_tab,
                'uri'   => $this->uri,
                'title' => $this->title,
                'name'  => $this->name,
                'no_res' => $this->translation['filter_no_results'],
                'rq'    => $_REQUEST,
                'tl'    => $this->total_left,
                'more_uri' => $this->more_uri,
                'ipp'   => $ipp
            ));
        } else {
//            $this->p++;

            $this->template->assign('more_uri',    $this->more_uri);
            $this->template->assign('total_left',    $this->total_left);
            $this->template->assign('products_list', $this->productsList());
            $this->filter($categories_id);
            return $this->template->fetch('modules/shop/products');
        }

    }

    /**
     * @param bool $render_items
     * @return string
     */
    private function productsList($render_items=true)
    {
        // сортування
        $this->sortingPanel();

//        if($this->request->isPost()){
//            $c = new Content();
//            $this->template->assign('page', $c->getData($cid, $this->languages_id));
//        }

        $this->template->assign('name' , $this->name);
        $this->template->assign('total', $this->total);
        $this->template->assign('products', $render_items ? implode('', $this->products) : '');

        return $this->template->fetch('modules/shop/products_list');
    }

    /**
     *
     */
    private function sortingPanel()
    {
        $this->template->assign('uri', $this->uri);
        $this->template->assign('items',         $this->guides->get(1));
        $this->template->assign('panel_sorting', $this->makeFriendlyUrl($this->template->fetch('modules/shop/sorting')));
    }

    /**
     * @param $categories_id
     * @return string
     */
    private function filter($categories_id)
    {
        $filter = isset($_REQUEST['filter']) ? $_REQUEST['filter'] : null;
        $pIds = array();
        if($filter){
            $pIds= $this->pids;
        }


        // список виробників
        $features_id = $categories_id == 7 ? 3 : 9;
        $features_list_id = $categories_id == 7 ? 26 : 27;
        $vendors = $this->model->getVendors($features_id,'', $pIds);

        $this->template->assign('vendors_features_id', $features_id);
        $this->template->assign('features_list_id', $features_list_id);
        $this->template->assign('vendors', $vendors);

        $car_vendors = $this->car->getVendors();

        $this->template->assign('car_vendors', $car_vendors);

        $features = $this->model->filterGetFeatures($categories_id, $pIds);

        $this->template->assign('features', $features);
//        $this->dump($features);die();

        $this->template->assign('filter_minp', round($this->minp));
        $this->template->assign('filter_maxp', round($this->maxp));

        $filter = $this->makeFriendlyUrl($this->template->fetch('modules/shop/filter'));
        $this->template->assign('filter', $filter);

        return $filter;
    }

    /**
     * @return string
     */
    public function product()
    {
        $id = $this->request->get('id', 'int');

        // images
        $images = $this->images->get($id);
        $this->template->assign('cover_img', $this->images->cover($id));
        $this->template->assign('images', $images);

        // products_options
        $params = $this->model->getProductOptions($id);
//        $this->dump($params);
        $params['price'] = round($params['price'], 2);
        $params['cid']   = $this->model->getCategoryID($params['products_id']);

        $this->template->assign('params', $params);

        // варіанти товару
//        $variants = $this->model->getVariants($id);
//        $this->template->assign('variants', $variants);

        // features
        $features =  $this->model->getProductFeatures($id);
        $this->template->assign('features', $features);


        // схожі товари
//        $this->template->assign('related', $this->related($id));

        // переглянуті
//        $this->template->assign('viewed', $this->viewed());

//        $this->dump($features);die();

        // comments / reviews
        $c = new Comments($id);
        $this->template->assign('comments_total', $c->getTotal());
        $this->template->assign('comments',       $c->get());
        $this->template->assign('comments_form',  $c->form());

        // статистика переглядів
        $this->model->setViews($id);
        // template
        return $this->template->fetch('modules/shop/product');
    }


    /**
     * @param $id
     * @return string
     */
    public function mainActions()
    {
        $this->model->setLimit(0, 30);
        $this->model->setWhere(' po.sale = 1 ');
        $items='';

        foreach ($this->model->getProducts() as $k=>$item) {
            $items  .= $this->item($item);
        }

        $this->template->assign('items', $items);
        return $this->template->fetch('modules/shop/main_action');
    }


    /**
     * @return string
     */
    public function popularTires()
    {
        $this->model->setLimit(0, 30);
        $this->model->setWhere(' po.hit = 1 ');
        $this->model->setJoin(" join products_categories pc on pc.cid=7 and pc.pid=c.id");
        $items='';

        foreach ($this->model->getProducts() as $k=>$item) {
            $items  .= $this->item($item);
        }
        // vendors

        $vendors = $this->model->getVendors(3, 20);
        $vendors = array_chunk($vendors, 10);

        $this->template->assign('vendors', $vendors);
        $this->template->assign('items', $items);

        return $this->template->fetch('modules/shop/popular');
    }
    /**
     * @return string
     */
    public function popularDisc()
    {
        $this->model->setLimit(0, 30);
        $this->model->setWhere(' po.hit = 1 ');
        $this->model->setJoin(" join products_categories pc on pc.cid=8 and pc.pid=c.id");
        $items='';

        foreach ($this->model->getProducts() as $k=>$item) {
            $item['img'] = $this->images->cover($item['id'], 'product');
            $items  .= $this->item($item);
        }
        // vendors

        $vendors = $this->model->getVendors(9, 20);
        $vendors = array_chunk($vendors, 10);

        $this->template->assign('vendors', $vendors);
        $this->template->assign('items',   $items);

        return $this->template->fetch('modules/shop/popular_disc');
    }


    /**
     * @param $item
     * @return string
     */
    private function item($item, $template='item')
    {
        $s = $this->model->getSeason($item['id']);
        if(!empty($s)){
            $item['season'] = $s;
        }

        $item['img'] = $this->images->cover($item['id'], 'product');
        $this->template->assign('item', $item);
        return $this->makeFriendlyUrl($this->template->fetch('modules/shop/' . $template));
    }

    public function viewed()
    {
        if(! isset($_COOKIE['viewed'])) return '';
        $id  = $this->request->get('id');
        $in = json_decode($_COOKIE['viewed']);
        if(empty($in)) return '';

        $in = array_reverse($in);
//        $this->dump($in);
        $in = array_slice($in, 0, 30);
        $this->model->setLimit(0, 30);
        $this->model->setWhere(' c.id in ('. implode(',', $in) .') and c.id <> ' . $id);

        $items='';

//        $this->model->debug();
        foreach ($this->model->getProducts() as $k=>$item) {
            $items .= $this->item($item);
        }

        if(empty($items)) return '';

        $this->template->assign('viewed_items', $items);
        return $this->template->fetch('modules/shop/viewed');
    }

    /**
     * @return string
     */
    public function siblings()
    {
        // ід товару
        $content_id = $this->request->get('id');
        // витягну його властивості
        $this->model->clearJoin()->clearWhere();
        $features = $this->model->getProductFeatures($content_id);
//        $this->dump($features);
        foreach ($features as $item) {
            if(
                $item['id'] == 9   || // бренд
                $item['id'] == 3   ||  // бренд
                $item['id'] == 16  ||
                $item['id'] == 15  ||
                $item['id'] == 13  ||
                $item['id'] == 14  ||
                $item['id'] == 7
            ) continue;

            foreach ($item['values'] as $k=>$v) {
                $this->model->setJoin(" join features_content_values fcv{$k} on
                        fcv{$k}.features_id={$item['id']}
                    and fcv{$k}.features_values_id = {$k}
                    and fcv{$k}.content_id = c.id
                    and fcv{$k}.content_id <> {$content_id}
                     ");
            }
        }

//        $this->model->debug();
        $this->model->setLimit(0, 30);

        $items='';
        foreach ($this->model->getProducts() as $k=>$item) {
            $items  .= $this->item($item);
        }
        if(empty($items)) return '';

        $this->template->assign('siblings_items', $items);
        return $this->template->fetch('modules/shop/siblings');
    }
    /**
     * @return string
     */
    public function recommended()
    {
//        $this->model->debug();
        $this->model->clearJoin()->clearWhere();
        $this->model->setLimit(0, 30);
        $this->model->setWhere(' po.hit = 1 ');
        $items='';

        foreach ($this->model->getProducts() as $k=>$item) {
            $items  .= $this->item($item);
        }
        if(empty($items)) return '';

        $this->template->assign('items', $items);
        return $this->template->fetch('modules/shop/recommended');
    }

    public function install(){}
    public function uninstall(){}
}