<?php
/**
 * Created by PhpStorm.
 * User: taras
 * Date: 26.04.16
 * Time: 15:10
 */

namespace controllers\modules;


use controllers\App;
use controllers\Module;
use models\app\Content;
use models\app\Guides;
use models\app\Mailer;
use models\modules\Catalog as CatalogModel;
use models\modules\Forum;
use models\modules\Users as UsersModel;
class PhotoDay extends Module
{
    private $root_id = 221;
    private $items_per_page = 100;
    private $item_pp = 5;
    private $avatars = false;

    public function __construct()
    {
        parent::__construct();
        $this->m = new \models\PhotoDay();
        $this->u = new UsersModel();
        $this->catalog = new CatalogModel();
        $this->mGuides = new Guides();
        $this->mMailer = new Mailer();
        $this->forum = new Forum();
    }

    public function install()
    {
        // TODO: Implement install() method.
    }

    public function uninstall()
    {
        // TODO: Implement uninstall() method.
    }

    public function index()
    {
        $lang = $this->request->param('lang');
        if(empty($lang) && $this->request->isGet()) {
            // треба витягнути мову по замовчуванню
            $languages_id = $this->languages->getDefault('id');
        } elseif(isset($lang)) {
            $languages_id = $this->languages->getIdByCode($lang);
        } else {
            $languages_id = self::langugesId();
        }

        // немає в списку доступних , нах
        if(empty($languages_id)) $this->redirect404();

        // задаю мову для всього сайту
        $this->languages_id = $languages_id;

        $id = $this->request->param('handle');

        if (empty($id))  $this->redirect404();

        $info = $this->m->getInfo($id);

        if(empty($info)) $this->redirect404();

        $_GET['article'] = $id;
        $_POST['article'] = $id;
        $hash_id = self::hashID($info['users_id']);

        if($info['type']=='video'){
            $v = self::makeVideoUrl($info['file'], $info['provider'], $info['thumb']);
            $info['big_path'] = $v['big'];
        } else {
            $info['big_path'] = '/uploads/portfolio/' . $hash_id .'/' . 'big_'. $info['file'];
        }
//        $this->dump($info);die;
        $this->request->set('photo', $info);
        $this->template->assign('photo', $info);
        $lang_id = self::langugesId();
        $c = new Content();
        $alias = $c->getAliasById(222,  $this->languages_id);
        $this->request->set('alias', $alias);
        $app = new App();
        $app->index();
    }

    public function get()
    {
        $id = $this->request->get('id');

        if($id==221) {
            $this->m->setWhere(" and uu.type='img'");
        } else {
            $this->m->setWhere(" and uu.type='video'");
        }



        $total = $this->m->getTotal();
//        die($total);
        $paginator = new Paginator($total, $this->items_per_page, $id . ';');
        $limit = $paginator->getLimit();
        $this->m->limit($limit['start'], $limit['num']);
        $this->template->assign('pagination', $paginator->getPages());


        $items = $this->getItems();
//        $items = array();
//        $this->dump($items);
        $this->template->assign('root_id', $id);
        $this->template->assign('items', implode('', $items));

        $start = $this->request->get('p');
        if(!$start) $start=1;
        $total = $total - $start*$this->items_per_page;
        if($total<0) $total=0;
        $this->template->assign('start', $start);
        $this->template->assign('total', $total);

        if(isset($_SESSION['app']['user']['users_group_id'])) {
            $uid = $_SESSION['app']['user']['users_group_id'];
            $groupId = $this->forum->getParentGroupId($uid);
            $this->template->assign("groupId",$groupId);
        }

        return $this->template->fetch('modules/photoDay/list');
    }

    private function getItems()
    {
        $res = array();

        foreach ($this->m->getItems() as $k=>$img) {
            $item = array ();
            $hash_id = self::hashID($img['users_id']);
            if ( isset($img['type']) && $img['type'] == 'video' ) {
                if ( !isset($img['provider']) ) continue;
                $v = self::makeVideoUrl($img['file'], $img['provider'], $img['thumb']);
//                    $this->dump($v);
                $item['big'] = $v['big'];
                $item['thumb'] = $v['thumb'];
//                    $item['img'][$k]['big']   = "http://www.youtube.com/embed/{$img['file']}";
//                    $item['img'][$k]['thumb'] = "http://img.youtube.com/vi/{$img['file']}/0.jpg";
                $item['type'] = 'video';
            } else {
//                if(!file_exists(DOCROOT.'/uploads/portfolio/' . $hash_id .'/' . 'thumb_'. $img['file'])) continue;
                $item['big']   = '/uploads/portfolio/' . $hash_id .'/' . 'big_'. $img['file'];
                $item['thumb'] = '/uploads/portfolio/' . $hash_id .'/' . 'thumb_'. $img['file'];
            }
            $item['id'] = $img['id'];
            $item['username'] = $img['username'];
            $item['users_id'] = $img['users_id'];
            $item['comment_t'] = $this->m->getTotalComment($img['id']);
            $item['like_t'] = $this->m->getTotalLike($img['id']);
//            $this->dump($item);
            $this->template->assign('item', $item);
            $res[] = $this->template->fetch('modules/photoDay/item');
        }
//        $this->dump($res);
        return $res;
    }

    public function more()
    {
//        $this->dump($_POST['filter']['city_id']);die();
//        die();
        $id = (int)$_POST['id'];
        $start = isset($_POST['p']) ? (int)$_POST['p'] : 0;
        $start++;
        $this->request->param('p', $start);

        if ( empty($id) ) die();

        $total = $this->m->getTotal();

        $paginator = new Paginator($total, $this->items_per_page, $id . ';');
        $limit = $paginator->getLimit();
        $this->m->limit($limit['start'], $limit['num']);
        $this->template->assign('pagination', $paginator->getPages());

        $items = array ();
        foreach ( $this->getItems() as $item ) {
            $items[] = $this->makeFriendlyUrl($item);
        }


        // показати ще
        $total = $total - $start * $this->items_per_page;
        if ( $total < 0 ) $total = 0;

        return json_encode(array (
            't' => $total,
            'items' => $items,
            'pagination' => $this->makeFriendlyUrl($paginator->getPages()),
            'p' => $start
        ));
    }

    public function info()
    {
        $photo = $this->request->get('photo');

        $hash_id = self::hashID($photo['users_id']);
        $this->template->assign('big_path','/uploads/portfolio/' . $hash_id .'/' . 'big_'. $photo['file']);

        $user = $this->catalog->getWorkerInfo($photo['users_id']);
        $user['city'] =$this->mGuides->getUserCities($photo['users_id']);
        $this->template->assign('ui',$user);

        $like_t = $this->m->getTotalLike($photo['id']);
        $already_like = 0;
        if(isset($_SESSION['app']['user']['id'])) {
            $already_like = $this->m->validateLike($photo['id'],$_SESSION['app']['user']['id']);
        }

        $prevNext = $this->m->getPrevNextId($photo['id'],$photo['type']);

        $this->template->assign(array(
            'like_t'=>$like_t,
            'already_like'=>$already_like,
            'prevNext'=>$prevNext
            ));

        return $this->template->fetch('modules/photoDay/info');
    }

    public function like()
    {
        $user_id = $_POST['user_id'];
        $client_id = $_POST['client_id'];

        $c = $this->m->validateLike($user_id,$client_id);
        if ($c>0) return 1;

        $id = $this->m->addLike($user_id,$client_id);

        return json_encode(array('c'=>$id));
    }

    public function getComments()
    {
        $photo = $this->request->get('photo');
        $total = $this->m->getTotalComment($photo['id']);
        $workers_id = $this->request->get('p', 'i');

        $items  = '';
        foreach ($this->m->limit(0, $this->item_pp)->getComments($photo['id']) as $item) {
            $items .= $this->itemComment($item);
        }
        $left = $total -  $this->item_pp;
        if($left < 0 ) $left = 0;
        $this->template->assign('comments_show_more', $left>0);
        $this->template->assign('comments_total', $total);
        $this->template->assign('comments_items', $items);
        $this->template->assign('workers_id', $workers_id);
        $this->template->assign('workers_id', $workers_id);
        $this->template->assign('is_worker', \controllers\modules\Users::isWorker());
        return $this->template->fetch('modules/photoDay/comments/items');
    }

    private function itemComment($item)
    {
        if($this->avatars){
            $item['icon'] = 'http://www.gravatar.com/avatar/' . md5( strtolower( trim( $item['email'] ) ) ) .'?s=80&d=mm&r=g';
        }

        $this->template->assign('item', $item);
        return $this->template->fetch('modules/photoDay/comments/item');
    }

    public function Commentform()
    {
        $uid = $this->request->post('uid', 'i');
        $id = $this->request->post('id', 'i');
        if(!$uid) return 'Invalid contentID';

        $this->template->assign('uid', $uid);
        $this->template->assign('id', $id);
        return $this->template->fetch('modules/photoDay/comments/form');
    }

    public function createComment()
    {
        include_once DOCROOT . "/vendor/phpmailer/PHPMailer.php";
        $s=0; $e=array(); $item = '';

        $users_id = \controllers\modules\Users::data('id');

        $data = $_POST['data'];
        $data['ip'] = $_SERVER['REMOTE_ADDR'];
        $data['content'] = htmlspecialchars(strip_tags($data['content']));


        if( ! $users_id){
            $e[] = $this->translation['comments_not_login'];
        } elseif(empty($data['content'])){
            $e[] = $this->translation['comments_empty_msg'];
        } else {
            $data['users_id'] = $users_id;
            $data['date'] = date("Y-m-d H:i:s");

            $s = $this->m->createComment($data);
            $e[] = $this->translation['fcomments_success'];

            $template = $this->mMailer->getTemplate('new_img_comment');

            $mail = new \PHPMailer;

            $owner_id = $this->m->getInfo($data['users_uploads_id']);

            $ui = $this->u->getInfoById($owner_id['users_id']);

            if(!empty($ui['email'])) {
                $mail->setFrom('no-reply@hotwed.com.ua', $template['reply_to']);
                $mail->addAddress($ui['email'], $ui['name']);
                $mail->addReplyTo($ui['email'], $ui['name']);

                $this->template->assign("photo_id",$data['users_uploads_id']);
                $mail->Body = $this->template->fetch('string:' . $template['body']);
                $mail->Body = $this->makeFriendlyUrl($mail->Body);
                $mail->isHTML(true);

                $mail->Subject = $template['subject'];

                $mail->send();
            }
            $lastComment = $this->m->getLastComment($s);

            $item = $this->itemComment($lastComment);
        }

        return json_encode(array('s'=>$s, 'm' => implode('<br>', $e),'item'=>$item));
    }

    public function getPhotoVideo()
    {

        $this->m->clearWhere()->setWhere(" and date(uu.photo_day) > DATE_SUB(CURDATE(), INTERVAL 1 WEEK)");

        $videoOfWeek = $this->m->getPhotoVideoOfDay('video');
        if(!empty($videoOfWeek)) {
            $v = self::makeVideoUrl($videoOfWeek['file'], $videoOfWeek['provider'], $videoOfWeek['thumb']);
            $videoOfWeek['big'] = $v['big'];
            $videoOfWeek['thumb'] = $v['thumb'];

            $this->template->assign('videoOfWeek',$videoOfWeek);
        }

        $this->m->clearWhere()->setWhere(" and date(uu.photo_day) = DATE_SUB(CURDATE(), INTERVAL 1 DAY)");

        $photoOfDay = $this->m->getPhotoVideoOfDay('img');

        if(!empty($photoOfDay)) {

            $hash_id = self::hashID($photoOfDay['users_id']);
            $photoOfDay['big']   = '/uploads/portfolio/' . $hash_id .'/' . 'big_'. $photoOfDay['file'];
            $photoOfDay['t_comments']   =  $this->m->getTotalComment($photoOfDay['id']);
            $photoOfDay['t_like']   =  $this->m->getTotalLike($photoOfDay['id']);

            $this->template->assign('photoOfDay',$photoOfDay);
        }

        $this->m->limit(0,20);
        $this->m->clearWhere()->setWhere(" and uu.createdon > DATE_SUB(CURDATE(), INTERVAL 1 WEEK)");
        $more = $this->m->getPhotoVideoMore('img');
//        $this->dump($more);
        if(!empty($more)) {
            foreach($more as $k=>$item) {
                $hash_id = self::hashID($item['users_id']);
                $more[$k]['big']   = '/uploads/portfolio/' . $hash_id .'/' . 'thumb_'. $item['file'];
            }
//            $this->dump($more);
            $this->template->assign('more',$more);
        }

        return $this->template->fetch("modules/photoDay/dayWeek");
    }

    public function bestPhoto()
    {
        $photo = $this->request->get('photo');
        $this->m->limit(0,20);
        $this->m->clearWhere()->setWhere(" and uu.createdon > DATE_SUB(CURDATE(), INTERVAL 1 week) and uu.id!={$photo['id']}");
        $more = $this->m->getPhotoVideoMore('img');
//        $this->dump($more);
        if(!empty($more)) {
            foreach($more as $k=>$item) {
                $hash_id = self::hashID($item['users_id']);
                $more[$k]['big']   = '/uploads/portfolio/' . $hash_id .'/' . 'thumb_'. $item['file'];
                $more[$k]['t_comments']   =  $this->m->getTotalComment($item['id']);
                $more[$k]['t_like']   =  $this->m->getTotalLike($item['id']);
            }
//            $this->dump($more);
            $this->template->assign('best',array_chunk($more,2));
        }

        return $this->template->fetch("modules/photoDay/bestPhoto");
    }

    public function moreComments()
    {
        $start = $this->request->post('p', 'i');

        $total = $this->m->getTotalComment($_POST['id']);

        $items  = '';
        foreach ($this->m->limit($start, $this->item_pp)->getComments($_POST['id']) as $item) {
            $items .= $this->itemComment($item);
        }
        $left = $total -  $this->item_pp - $start;
        if($left < 0 ) $left = 0;
        return json_encode(array(
            't'     => $left,
            'items' => $items
        ));
    }

    /**
     * @param $id
     * @return string
     */
    public static function hashID($id)
    {
        return md5('n'. $id . 'x');
    }

    /**
     * @param $file
     * @param $provider
     * @return array
     */
    public static function makeVideoUrl($file, $provider, $thumb = '')
    {
        $item = array();
        switch($provider){
            case 'youtube':

                $item['big']   = "http://www.youtube.com/embed/{$file}";
                $item['thumb'] = "http://img.youtube.com/vi/{$file}/hqdefault.jpg";
                break;
            case 'vimeo':
                //https://developer.vimeo.com/player/embedding
                $item['big']   = "//player.vimeo.com/video/{$file}";
                $item['thumb'] = $thumb;
                /*
                $thumb = @file_get_contents("https://vimeo.com/api/v2/video/{$file}.json");
                if($thumb){
                    $a = json_decode($thumb, true);
//                    $this->dump($a);
                    $item['thumb'] = $a[0]['thumbnail_medium'];
                }
                */
                break;
        }

        return $item;
    }
}