<?php
/**
 * Company Otakoyi.com
 * Author: wg
 * Date: 02.01.15 16:22
 */

namespace controllers\modules;

use controllers\core\Settings;
use controllers\Module;
use models\app\Currency;
use models\app\Guides;
use models\modules\Forum;
use models\modules\Orders as OrdersT;
use models\modules\UsersOnline;
use models\modules\UsersUploads;
use SocialAuther\SocialAuther;

defined("SYSPATH") or die();

include_once DOCROOT . "vendor/phpmailer/PHPMailer.php";
include_once DOCROOT . "vendor/phpmailer/SMTP.php";
include_once DOCROOT  ."vendor/acimage/AcImage.php";
//require_once DOCROOT . 'vendor/SocialAuther/autoload.php';

/**
 * Class Users
 * @name Users
 * @description Users module
 * Class Users
 * @package modules
 * @author wmgodyak mailto:wmgodyak@gmail.com
 * @version 1.0
 * @copyright &copy; 2014 Otakoyi.com
 */
class Users extends Module {

    protected $mUsers;
    protected $mOrders;
    private $adapters=array();

    public function __construct()
    {
        parent::__construct();

        $this->mUsers = new \models\modules\Users();
        $this->mOrders = new OrdersT();
        $this->forum = new Forum();

        $this->oAuth();
    }

    private function oAuth()
    {
        $s = Settings::instance();

        $mc = $this->load->model('app\Content');

        $url = $mc->getAliasById( 7, $this->languages_id );
//        $succes_url = $mc->getAliasById( 7, $this->languages_id );
        $adapterConfigs = array(
            'facebook' => array(
                'client_id'     => $s->get('facebook_app_id'),
                'client_secret' => $s->get('facebook_app_secret'),
                'redirect_uri'  => APPURL . $url. '?provider=facebook'
            ),
            'google' => array(
                'client_id'     => $s->get('google_client_id'),
                'client_secret' => $s->get('google_client_secret'),
                'redirect_uri'  => APPURL . $url. '?provider=google'
            ),
            'vk' => array(
                'client_id'     => $s->get('vk_app_id'),
                'client_secret' => $s->get('vk_app_secret'),
                'redirect_uri'  => APPURL . $url. '?provider=vk'
            )
        );

        foreach ($adapterConfigs as $adapter => $settings) {
            $class = 'vendor\SocialAuther\Adapter\\' . ucfirst($adapter);
            $this->adapters[$adapter] = new $class($settings);
            $this->template->assign('oauth_' . $adapter . '_url', $this->adapters[$adapter]->getAuthUrl());
        }
//        $this->dump($_SESSION);die();
        //  якщо успішна авторизація
        if (isset($_GET['provider']) && array_key_exists($_GET['provider'], $this->adapters)) {

            $auther = new \vendor\SocialAuther\SocialAuther($this->adapters[$_GET['provider']]);
//            $auther = new SocialAuther($this->adapters[$_GET['provider']]);

            if ($auther->authenticate()) {
                $data = array();
                $data['provider'] = $auther->getProvider();
                $data['social_id'] =$auther->getSocialId();
                $data['name'] = $auther->getName();
                $data['email'] = $auther->getEmail();
                $data['avatar'] = $auther->getAvatar();

                $data['users_group_id'] = 4;
                $data['languages_id'] = $this->languages_id;
                $data['password'] = '';

                if(empty($data['provider']) || empty($data['social_id'])) die();
                // розділяю імя на імя і прізвище

                    if(empty($data['email'])){
                        $data['email'] = '';
                    }
//                $this->dump($data);
                // перевірка чи чувак присутній в базі якщо є, авторизую

                $users_id = $this->mUsers->getUsersIDByProvider($data['provider'], $data['social_id']);
                if(!empty($users_id)){

                    $info = $this->mUsers->getInfoById($users_id);
                    $c = $this->mUsers->getInfoByEmail($data['email']);
                    if(!empty($data['email']) && empty($c)){
                        $this->mUsers->update($users_id,array(
                            'email'=>$data['email']
                        ));
                    }

                    $info['provider']  = $data['provider'];
                    $info['social_id'] = $data['social_id'];

                    if(!empty($info)){
                        $this->auth($info);
                        $this->redirect($url);
                    }
                }

//                $info = $this->mUsers->getInfoByProvider($data['social_id'], $data['provider'], $data['email']);

                if(strpos($data['name'], ' ') != FALSE){
                    $a = explode(' ', $data['name']);
                    $data['name'] = $a[0];
                    $data['surname'] = $a[1];
                }

                if(!empty($data['email'])){
                    // спробую витягнути інформацію по email
                    $info = $this->mUsers->getInfoByEmail($data['email']);

                    if(!empty($info)){
                        if($this->mUsers->addUsersProvider($info['id'], $data['provider'], $data['social_id']) > 0){
                            $info['provider']  = $data['provider'];
                            $info['social_id'] = $data['social_id'];

                            $this->auth($info);
                            $this->redirect($url);
                        }
                        die('Could to login by social ID');
                    }
                }

                // реєструю чувачка
                $profile_url = urlencode(trim(strip_tags(htmlspecialchars($data['name'].$data['surname'].$data['social_id']))));
                $data['url'] = $this->valedateUrl($profile_url);

                $id = $this->mUsers->create($data);
                if(empty($id)) die('Could to register user');

                $data['id'] = $id;
                $this->auth($data);
                $this->redirect($url);
            }
        }
    }

    public function index()
    {
//        $this->dump($_SESSION); die();
        if(! self::data('id')) $this->redirect('/');

//        $this->template->assign('ui', self::data());
//        $this->template->assign('orders', $this->myOrders());
//        return $this->template->fetch('modules/users/profile');
    }

    /**
     * @return string
     */
    public function profile()
    {
        if(! self::data('id')) $this->redirect('/');

        if($this->request->post('process')){
            $s=0; $e = array(); $im = '';
            $data = $this->request->post('data'); $id = self::data('id');
            if(isset($data['city_id'])) {
                $cities = $data['city_id'];
                unset($data['city_id']);
            }
            foreach ($data as $k=>$v) $data[$k] = trim($v);

            if(!$data) {
                die('WRONG_DATA');
            } elseif(empty($cities[0]) && $_SESSION['app']['user']['users_group_id'] != 3) {
                $e[] = array(
                    'i' =>'#data_city_id_0',
                    'm' => 'Виберіть місто'
                );
            } elseif(empty($data['name'])){
                $e[] = array(
                    'i' =>'#data_name',
                    'm' => $this->translation['e_user_name']
                );
            } elseif(empty($data['surname'])){
                $e[] = array(
                    'i' =>'#data_surname',
                    'm' => $this->translation['e_user_surname']
                );
            } elseif(empty($data['phone'])){
                $e[] = array(
                    'i' =>'#data_phone',
                    'm' => $this->translation['e_user_phone']
                );
            } elseif(! \PHPMailer::validateAddress($data['email'])){
                $e[] = array(
                    'i' =>'#data_email',
                    'm' => $this->translation['user_login_e_bad_email']
                );
            }  elseif(! $this->mUsers->isUniqueEmail($id, $data['email'])){
                $e[] = array(
                    'i' =>'#data_email',
                    'm' => $this->translation['ui_e_unique_email']
                );
            } else {
                if(!empty($data['password'])) {
                    if (!$data['r_password']) {
                        $e[] = array(
                            'i' => '#r_password',
                            'm' => 'Повторіть пароль'
                        );
                    }
                    if ($data['password'] != $data['r_password']) {
                        $e[] = array(
                            'i' => '#r_password',
                            'm' => 'Паролі не співпадають'
                        );
                    }
                    $data['password'] = $this->mUsers->encodePassword($data['password']);
                } else {
                    unset($data['password']);
                }

                if(empty($e)) {
                    unset($data['r_password']);
                    /*
                                    if(isset($_FILES['avatar']) && $_FILES['avatar']['error'] == 0){

                                        // видалю попереднє зображення
                                        $avatar = $this->mUsers->getInfoById($id, 'avatar');
                                        if(!empty($avatar)){
                                            @unlink($avatar);
                                        }
                    //                    $this->dump($_FILES['avatar']);
                                        $image_tmp = $_FILES['avatar']['tmp_name'];
                                        $upload_dir = DOCROOT . 'uploads/avatars/';
                                        $ext = '.' . pathinfo($_FILES['avatar']['name'], PATHINFO_EXTENSION);

                                        $image_name = md5(self::data('id') . 'x' . time()) . $ext;

                                        $img = \AcImage::createImage($image_tmp);
                                        \AcImage::setRewrite(true);
                                        \AcImage::setQuality(100);
                                        $img->cropCenter('1pr', '1pr');
                                        $img->resize(262, 262);
                                        $img->save($upload_dir . $image_name);

                                        $im =  '/uploads/avatars/' . $image_name;
                                        $data['avatar'] = $im;
                                    }
                    */

                    if(isset($cities)) {
                        $this->mUsers->insertCities(self::data('id'),$cities);
                    }

                    $url = urlencode(trim(strip_tags(htmlspecialchars($data['url']))));
                    $url = $this->valedateUrl($url);

                    if ($this->mUsers->checkingUrl($url, $id)){
                        $s =  $this->mUsers->update($id, $data);
                    } else {
                        $data['url'] = $url . '-' . $id;
                        $s =  $this->mUsers->update($id, $data);
                    }

                    $s += $this->mUsers->saveOauthInfo($id);
                }

            }

            return json_encode(array(
                's'   => $s > 0,
                'e'   => $e,
                'sm'  => $this->translation['ui_update_success'],
                'im'  => $im
            ));
        }

        $ui = $this->mUsers->getInfoById(self::data('id'));
        $ui['oauth'] = $this->mUsers->getOauthInfo(self::data('id'));

        if(isset($ui['users_group_id']) && $ui['users_group_id'] == 4){

            $this->template->assign('group', $this->mUsers->getUsersGroup(2));
            return $this->template->fetch('modules/users/select_group');
        }
        if(isset($ui['users_group_id']) && $ui['users_group_id'] != 3){
            $pay_city = 0;
//            $pay_city = $this->mOrders->getTotalCity(self::data('id'))-1;

            if ($ui['gold'] == 1) {
                $pay_city = $this->mOrders->getTotalCity(self::data('id'))-1;
            }


            if ($pay_city<0) $pay_city = 0;

            $pay_city = !empty($ui['gold']) ? 2: $pay_city;

            $g = new Guides();
            $this->template->assign('discounts', $g->get(13));
            $array = array();
//            $notIn = array();
//            for($i=0;$i<=$pay_city;$i++) {
//                if(!empty($notIn)) {
//                    $g->setWhere(" and g.id not in (".implode(',',$notIn).")");
//                }
//
//
//                $this->template->assign('city_'.$i,$city);
////                $this->dump($city);
//            }
            $city = $g->getCities(9, self::data('id'));
            foreach($city as $k => $item) {
                if($item['selected'] == "selected") {
                    $array[] = $item['id'];
                }

                if ($ui['gold'] != 1 && $item['main_city'] != 1) {
                    $city[$k]['selected'] = '';
                }
            }

            $dateToHot = $ui['hot_date']!=0?$ui['hot_date']:0;
            if($dateToHot!=0)  {
                $this->template->assign('dateDiff',$this->get_duration(date('Y-m-d'),date('Y-m-d',strtotime($dateToHot))));
            }

            $dateToGold = $ui['gold_date']!=0?$ui['gold_date']:0;
            if($dateToGold!=0)  {
                $this->template->assign('dateDiffGold',$this->get_duration(date('Y-m-d'),date('Y-m-d',strtotime($dateToGold))));
            }
//        $this->dump($city);
            $this->template->assign('city', $city);
            $this->template->assign('selected', $array);

            // currency
            $currency = new Currency();
            $this->template->assign('currency', $currency->get());
            $this->template->assign('group', $this->mUsers->getUsersGroup(2));
            $this->template->assign('pay_city',$pay_city);
        }

        $this->template->assign('languages', $this->languages->get());

        $template = isset($ui['users_group_id']) && $ui['users_group_id'] == 3 ? 'modules/users/profile' : 'modules/users/worker_profile';
        $this->template->assign('ui', $ui);
        $this->template->assign('rm_profile', $this->deleteProfile());
       

        return $this->template->fetch($template);
    }

    public function get_duration ($date_from, $date_till) {
        $date_from = explode('-', $date_from);
        $date_till = explode('-', $date_till);

        $time_from = mktime(0, 0, 0, $date_from[1], $date_from[2], $date_from[0]);
        $time_till = mktime(0, 0, 0, $date_till[1], $date_till[2], $date_till[0]);

        $diff = ($time_till - $time_from)/60/60/24;

        return $diff;
    }

    public function avatar()
    {
        if(! self::data('id')) $this->redirect('/');
        $im = '';
        $id = self::data('id');
        if(isset($_FILES['avatar']) && $_FILES['avatar']['error'] == 0){

            // видалю попереднє зображення
            $avatar = $this->mUsers->getInfoById($id, 'avatar');
            if(!empty($avatar)){
                @unlink($avatar);
            }
//                    $this->dump($_FILES['avatar']);
            $image_tmp = $_FILES['avatar']['tmp_name'];
            $upload_dir = DOCROOT . 'uploads/avatars/';
            $ext = '.' . pathinfo($_FILES['avatar']['name'], PATHINFO_EXTENSION);

            $image_name = md5(self::data('id') . 'x' . time()) . $ext;

            $img = \AcImage::createImage($image_tmp);
            \AcImage::setRewrite(true);
            \AcImage::setQuality(100);
            $img->cropCenter('1pr', '1pr');
            $img->resize(262, 262);
            $img->save($upload_dir . $image_name);

            $im =  '/uploads/avatars/' . $image_name;

            $this->mUsers->update($id, array('avatar' => $im));
        }

        return json_encode(array(
            'sm'  => $this->translation['ui_update_success'],
            'im'  => $im
        ));
    }


    public function selectGroup()
    {
        if(! self::data('id')) return 0 ;
        if(!isset($_POST['gid'])) return 0;

        $gid = (int) $_POST['gid'];
        self::data('users_group_id', $gid);
        return $this->mUsers->selectGroup(self::data('id'), $gid);
    }

    /**
     * @return string
     */
    public function signUp()
    {
//        $this->oAuth();
        $this->template->assign('groups', $this->mUsers->getUsersGroup(4));
        $this->template->assign('login', $this->login());
        $this->template->assign('register', $this->register());
        return $this->template->fetch('modules/users/signup');
    }

    public function login()
    {
        if($this->request->post('process')){
            $data = $_POST['data'];
            $status = false;
            $inp = array();
            $msg = array();

            if(! \PHPMailer::validateAddress($data['email'])){
                $msg[] = $this->translation['user_login_e_bad_email'];
                $inp[] = '#data_email';
            } {
                $info  = $this->mUsers->getInfoByEmail($data['email']);
                if(empty($info)){
                    $msg[] = $this->translation['u_e_bad_lp'];
                    $inp[] = '#data_email';
                } else{
                    if(empty($info['confirm'])) {
                        $msg[] = $this->translation['email_not_confirm'];
                        $inp[] = '#data_email';
                    } else {
                        if(!$this->mUsers->checkPassword($data['password'], $info['password']) || empty($data['password'])){
                            $msg[] = $this->translation['u_e_bad_lp'];
                            $inp[] = '#data_password';
                        } else{
                            $this->auth($info);
                            $status = true;
                        }
                    }
                }
            }

            return json_encode(array(
                's'   => $status,
                'm'   => implode('<br>', $msg),
                'inp' => $inp,
                'r'   => $this->mkUrl(37)
            ));
        }
        return $this->template->fetch('modules/users/login');
    }

    /**
     * @return string
     */
    public function register()
    {
        if($this->request->post('process')){
            $data = $_POST['data'];
            $status = false;
            $inp = array();
            $msg = array();

            if(empty($data['name'])){
                $msg =  $this->translation['u_e_empty_name'];
                $inp = '#data_name';
            } else if(! \PHPMailer::validateAddress($data['email'])){
                $msg[] = $this->translation['user_login_e_bad_email'];
                $inp[] = '#data_email2';
            } else if($data['password']  != $data['confirm_password']){
                $msg[] = $this->translation['u_e_confirm_passw'];
                $inp[] = '#data_password2';
                $inp[] = '#data_confirm_password';
            } else if(!isset($data['users_group_id'])){
                $msg[] = $this->translation['u_e_ug'];
                $inp[] = '#confirm';
            } /*else if(!isset($_POST['confirm'])){
                $msg[] = $this->translation['u_e_confirm'];
                $inp[] = '#confirm';
            } */else {
                $info  = $this->mUsers->getInfoByEmail($data['email']);
                if(!empty($info)){
                    $msg[] = $this->translation['u_e_is_registered'];
                    $inp[] = '#data_email2';
                } else {
                    unset($data['confirm_password']);
                    $data['url'] = uniqid();

                    $data['skey'] = md5(serialize([
                        $data['name'], $data['email'], $data['password']
                    ]));

                    $data['confirm'] = 0;

                    $id = $this->mUsers->create($data);

                    if($id > 0){

                        $url = explode('@', $data['email']);
                        $url = urlencode(trim(strip_tags(htmlspecialchars($url[0]))));
                        $url = $this->valedateUrl($url);

                        if ($this->mUsers->checkingUrl($url)){
                            $data['url'] = $url;
                            $this->mUsers->update($id, array(
                                'url' => $data['url']
                            ));
                        } else {
                            $data['url'] = $url . '-' . $id;
                            $this->mUsers->update($id, array(
                                'url' => $data['url']
                            ));
                        }


                        $data['id'] = $id;
//                        $this->auth($data);
                        $this->sendConfirmMessage($data);
                        $status = true;
                    }
                }
            }

            return json_encode(array(
                's'   => $status,
                'm'   => implode('<br>', $msg),
                'inp' => $inp,
                'r'   => $this->mkUrl(37)
            ));
        }
        return $this->template->fetch('modules/users/register');
    }

    private function sendConfirmMessage($data)
    {
        $mail = new \PHPMailer;

        $data['url'] = APPURL.'ajax/users/confirm/'.$data['skey'];

        $mail->setFrom('no-reply@' . $_SERVER['HTTP_HOST']);
        $mail->addAddress($data['email'], $data['name']);
        $mail->isHTML(true);

        // надішлю повідомлення
        $mt = $this->load->model('app\Mailer');
        $template = $mt->getTemplate('confirmation');

        $mail->Subject = $template['subject'];

//        $mail->IsSMTP();
//        $mail->SMTPSecure = 'ssl';
//        $mail->Host       = 'smtp.gmail.com';
//        $mail->Port       = 465;
//        $mail->Username   = 'gstuffpro@gmail.com';
//        $mail->Password   = 'Admin1241';
//        $mail->SMTPAuth   = true;
        
        $mail->Body = preg_replace_callback(
            '({[a-z-_]+})',
            function ($matches) use ($data) {
                $matches[0] = str_replace(array('{','}'), array(), $matches[0]);
                return isset($data[$matches[0]]) ? $data[$matches[0]] : $matches[0];
            },
            $template['body']
        );

        return $mail->send();
    }

    public function confirm($skey = 0)
    {
        if (empty($skey)) die("empty skey");

        $info = $this->mUsers->getInfoBySkey($skey);

        if ($info['id']) {
            $mc = $this->load->model('app\Content');
            $url = $mc->getAliasById( 7, $this->languages_id );
            $data = $this->mUsers->getInfoById($info['id']);
            if (empty($info['confirm'])) {

                $this->mUsers->update($info['id'], [
                    'confirm' => 1,
                    'skey' => NULL
                ]);

                $this->auth($data);
                $this->redirect($url);
            } else {
                $this->auth($data);
                $this->redirect($url);
            }
        } else {
            $this->redirect('/');
        }
    }



    private function valedateUrl($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }

    private function sendRegMessage($data)
    {
        $mail = new \PHPMailer;

        $mail->setFrom('no-reply@' . $_SERVER['HTTP_HOST']);
        $mail->addAddress($data['email'], $data['name']);
        $mail->isHTML(true);

        // надішлю повідомлення
        $mt = $this->load->model('app\Mailer');
        $template = $mt->getTemplate('reg_msg');

        $mail->Subject = $template['subject'];
        $mail->Body = preg_replace_callback(
            '({[a-z-_]+})',
            function ($matches) use ($data) {
                $matches[0] = str_replace(array('{','}'), array(), $matches[0]);
                return isset($data[$matches[0]]) ? $data[$matches[0]] : $matches[0];
            },
            $template['body']
        );

        return $mail->send();
    }

    public function fp()
    {
        if($this->request->post('process')){
            $data = $_POST['data'];
            $status = false;
            $inp = array();
            $msg = array();

            if(! \PHPMailer::validateAddress($data['email'])){
                $msg[] = $this->translation['user_login_e_bad_email'];
                $inp[] = '#data_email';
            } {
                $info  = $this->mUsers->getInfoByEmail($data['email']);
                if(empty($info)){
                    $msg[] = $this->translation['u_e_bad_lp'];
                    $inp[] = '#data_email';
                } else{

                    // згенерую пароль
                    $psw = $this->mUsers->generatePassword();

                    // запишу в базу
                    $this->mUsers->setPassword($info['id'], $psw);
                    $status = $this->sendPasswordByEmail($info['email'], $psw);
                    $msg[] = $this->translation['fp_success'];
                }
            }

            return json_encode(array(
                's'   => $status,
                'm'   => implode('<br>', $msg),
                'inp' => $inp
            ));
        }
        return $this->template->fetch('modules/users/fp');
    }


    /**
     * @param $email
     * @param $psw
     * @return bool
     * @throws \Exception
     * @throws \controllers\core\Exceptions
     * @throws \phpmailerException
     */
    private function sendPasswordByEmail($email, $psw)
    {
        $mail = new \PHPMailer;

        $mail->setFrom('no-reply@' . $_SERVER['HTTP_HOST']);
        $mail->addAddress($email);
        $mail->isHTML(true);

        // надішлю повідомлення
        $mt = $this->load->model('app\Mailer');
        $template = $mt->getTemplate('tmp_passw');

        $mail->Subject = $template['subject'];
        $mail->Body    = $template['body'];
        $data = array(
            'password' => $psw
        );
        $mail->Body = preg_replace_callback(
            '({[a-z-_]+})',
            function ($matches) use ($data) {
                $matches[0] = str_replace(array('{','}'), array(), $matches[0]);
                return isset($data[$matches[0]]) ? $data[$matches[0]] : $matches[0];
            },
            $template['body']
        );

       return $mail->send();
    }


    public function iOnline()
    {
        return self::data('id') > 0;
    }


    /**
     * @param $data
     * @return $this
     */
    private function auth($data)
    {
        if(isset($data['remember_me']) && $data['remember_me'] == 1) {
            // генерую хешкод
            $key = $this->mkSkey($data['id']);
            $this->mUsers->setSkey($data['id'], $key);

            setcookie('rmus',$key,time() + 60 * 24 * 31,'/' );
        }

        if(!empty($data['id'])) {
            $this->mUsers->update($data['id'],array(
                'last_login' => date("Y-m-d H:i:s")
            ));
        }

        foreach ($data as $k=>$v) {
            self::data($k, $v);
        }

        return $this;
    }

    /**
     * @param $id
     * @return string
     */
    private function mkSkey($id)
    {
        return md5(md5($id . date('ymdhis')) . $id);
    }

    /**
     * @param $k
     * @param null $v
     * @return null
     */
    public static function data($k=null, $v=null)
    {
        if(! isset($_SESSION['app']['user'])) $_SESSION['app']['user'] = array();

        if($v){
            $_SESSION['app']['user'][$k] = $v;
        } else {
            if(! $k){
                return $_SESSION['app']['user'];
            }
            return isset($_SESSION['app']['user'][$k]) ? $_SESSION['app']['user'][$k] : null;
        }
    }

    /**
     * check if is Online
     * @return bool
     */
    public static function isOnline()
    {
        return self::data('id') > 0;
    }

    /**
     * @return int
     */
    public function logout()
    {
        $id = self::data('id');
        if($id){
            $mu = new UsersOnline();
            $mu->setOffline($id);
        }
        unset($_SESSION['app']['user']);
        setcookie('rmus','',time() - 1,'/' );
        return 1;
    }

    public function getPayForm()
    {
        $type = $_POST['type'];

        $view = $type=="hot"?1:2;
        
        $users_id = (int)$_SESSION['app']['user']['id'];

        if($type == 'hot') {
            $this->mOrders->setWhere(" and (type='week' or type='month')");
        }

        if($type == 'gold') {
            $this->mOrders->setWhere(" and type='year'");
        }

//        $is_pay = $this->mOrders->isPay($users_id);
        $is_pay = false;

        if($is_pay) {
            $view = $this->template->fetch("modules/users/payForm/already-pay");
        } else {
            $this->template->assign(array(
               'priceWeek'=>Settings::instance()->get('priceWeek'),
               'priceMonth'=>Settings::instance()->get('priceMonth'),
               'priceYear'=>Settings::instance()->get('priceYear')
            ));
            $view = $this->template->fetch("modules/users/payForm/form-".$view);
        }


        return json_encode(array('view'=>$view));

    }

    public function getMoreCities()
    {
        $users_id = (int)$_SESSION['app']['user']['id'];

        $s = 0;
        //витягуємо кількість оплачених міст
        $count = $this->mOrders->getPayCities($users_id);

        $view = '';
        if($count>=2) {
            $view = $this->template->fetch("modules/users/payForm/already-cities-pay");
        }else {
            $s = 1;
            $this->template->assign(array(
                'count'=>$count,
                'priceOneC'=>Settings::instance()->get('priceOneC'),
                'priceTwoC'=>Settings::instance()->get('priceTwoC')
            ));
            $view = $this->template->fetch("modules/users/payForm/form-3");
        }

        return json_encode(array('view'=>$view,'s'=>$s));
    }

    public function install()
    {
    }

    public function uninstall()
    {
    }

    //////////////////// інші методи для сайту

    public function portfolio()
    {
        if(! self::data('id')) $this->redirect('/');
        $uid = $_SESSION['app']['user']['users_group_id'];
        $groupId = $this->forum->getParentGroupId($uid);

        if($groupId!=2){
            $this->redirect('/');
        }
        $uid = self::data('id');
        $UP = new UsersUploads();
        $items = '';
        $c = count($UP->getItems($uid));
        foreach ($UP->getItems($uid) as $item) {
            $items .= $this->renderItem($item);
        }

        $this->template->assign('items', $items)->assign('c',$c);
        return $this->template->fetch('modules/users/portfolio');
    }

    public function uploadImages()
    {
        $UP = new UsersUploads();
        $uid = self::data('id');
        $gold = self::data('gold');
        $max_file_cout = ($gold) ? 7 : 3;
        $c = count($UP->getItems($uid));

        if($c>2) {
            //к-сть завантажень за останній тиждень
            $quantity_info = $this->mUsers->getQuantityInfo($uid);
            if(!empty($quantity_info)) {
                if ($quantity_info['quantity'] > $max_file_cout) {
                    $res['error'] = $this->translation['upload_to_much_files_' . $max_file_cout];
                    return json_encode($res);
                }
            }
        }

//        if($c>50) return 1;
        $uid = self::data('id');
        if(!isset($_FILES['images']) || !$uid ) return '';

        $allowed_type = array("image/jpeg","image/png");
        $res = array();
        $UP = new UsersUploads();

        $dir = '/uploads/portfolio/' . self::hashID($uid) . '/';
        $upload_dir = DOCROOT . $dir;
        if(!is_dir($upload_dir)) mkdir($upload_dir, 0775);


        foreach ($_FILES['images']['name'] as $k=>$name) {
            // check allowed type
            $c = count($UP->getItems($uid));
            if($c>2) {
                $quantity_info = $this->mUsers->getQuantityInfo($uid);
                if (!empty($quantity_info)) {
                    if ($quantity_info['quantity'] < $max_file_cout) {
                        //додаємо ще одне завантаження
                        $this->mUsers->updateRow('img_quantity', $quantity_info['id'], array(
                            'quantity' => $quantity_info['quantity'] + 1
                        ));
                    } else {
                        $res['error'] = $this->translation['upload_to_much_files_' . $max_file_cout];
                        return json_encode($res);
                    }
                } else {
                    $this->mUsers->insertImgQuantity($uid);
                }
            }
            if(! in_array($_FILES['images']['type'][$k], $allowed_type)) continue;

            $image_tmp = $_FILES['images']['tmp_name'][$k];

            $ext = '.' . pathinfo($name, PATHINFO_EXTENSION);
            $image_name = md5(microtime(true)) . $ext;

            // resize to 1600x1200
            $img = \AcImage::createImage($image_tmp);
            \AcImage::setRewrite(true);
            \AcImage::setQuality(70);
//            $img->resize(1600, 1200);
            $img->resizeByWidth(1000);
            $img->save($upload_dir . 'big_'.$image_name);

            // resize to 162x162
            $img = \AcImage::createImage($image_tmp);
            \AcImage::setRewrite(true);
            \AcImage::setQuality(100);
            $img->cropCenter('1pr', '1pr');
            $img->thumbnail(360, 360);
            $img->save($upload_dir . 'thumb_'.$image_name);

            $id = $UP->create($uid, $image_name);
            if($id > 0){
                $res['images'][] = $this->renderItem(
                    array(
                        'id'   => $id,
                        'file' => $image_name
                    )
                );
            }
        }

        return json_encode($res);
    }

    public function imgQuantity()
    {

    }

    public function rmPortfolioItem()
    {
        $uid = self::data('id');
        $id = $this->request->post('id');

        if(!$uid || ! $id) return 0;

        $UP = new UsersUploads();
        $info = $UP->getInfo($id, $uid);

        if(empty($info)) return 0;

        if($info['type'] = 'img'){
            $dir = '/uploads/portfolio/' . self::hashID($uid) . '/';
            $upload_dir = DOCROOT . $dir;
            @unlink($upload_dir . 'big_'.$info['file']);
            @unlink($upload_dir . 'thumb_'.$info['file']);
        }

        return $UP->deleteRow('users_uploads', $id);
    }

    public function portfolioSort()
    {
        $order = $this->request->post('order');
        $uid = self::data('id');

        if(!$uid || ! $order) return 0;


        $UP = new UsersUploads();

        if($UP->sort($uid, $order)){
            return $this->translation['sort_success'];
        }
    }


    /**
     * @param $item
     * @return string
     */
    private function renderItem($item)
    {
        $hash_id = self::hashID(self::data('id'));
        if(isset($item['type']) && $item['type'] == 'video'){
//            $item['big']   = "http://www.youtube.com/embed/{$item['file']}";
            $item['thumb'] = "http://img.youtube.com/vi/{$item['file']}/default.jpg";
            $v = self::makeVideoUrl($item['file'], $item['provider'], $item['thumb']);
            $item['big'] = $v['big'];
            $item['thumb'] = $v['thumb'];
        } else{
            $item['big']   = '/uploads/portfolio/' . $hash_id .'/' . 'big_'. $item['file'];
            $item['thumb'] = '/uploads/portfolio/' . $hash_id .'/' . 'thumb_'. $item['file'];
        }
        $this->template->assign('item', $item);
        return $this->template->fetch('modules/users/portfolio_item');
    }

    /**
     * @param $file
     * @param $provider
     * @return array
     */
    public static function makeVideoUrl($file, $provider, $thumb = '')
    {
        $item = array();
        switch($provider){
            case 'youtube':

                $item['big']   = "https://www.youtube.com/embed/{$file}";
                $item['thumb'] = "http://img.youtube.com/vi/{$file}/default.jpg";
                break;
            case 'vimeo':
                //https://developer.vimeo.com/player/embedding
                $item['big']   = "//player.vimeo.com/video/{$file}";
                $item['thumb'] = $thumb;
                /*
                $thumb = @file_get_contents("https://vimeo.com/api/v2/video/{$file}.json");
                if($thumb){
                    $a = json_decode($thumb, true);
//                    $this->dump($a);
                    $item['thumb'] = $a[0]['thumbnail_medium'];
                }
                */
                break;
        }

        return $item;
    }



    public function uploadVideo()
    {
        if($this->request->post('process') == 1){
            $uid = self::data('id');
            $url = $this->request->post('url');

            if(!$uid || ! $url) return 0;

            if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match)) {
                $video_id = $match[1];

                $UP = new UsersUploads();
                $exist = $UP->exist($video_id,'video','youtube');

                if(!empty($exist)) {

                    return json_encode(array(
                        'm' => $this->translation['video_exist'],
                        's' => false
                    ));
                }

                $id = $UP->create($uid, $video_id, 'video', 'youtube');

                return json_encode(array(
                    'images' =>  $this->renderItem(
                        array(
                            'id'    => $id,
                            'file'  => $video_id,
                            'type'  => 'video',
                            'provider' => 'youtube'
                        )
                    ),
                    's' => 1
                ));
            } elseif (preg_match("/(?:https?:\/\/)?(?:www\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|)(\d+)(?:$|\/|\?)/", $url, $id)) {
                $video_id = $id[3];
                //https://vimeo.com/137591399
                if($video_id > 0){
                    $thumb = '';
                    $u = @file_get_contents("https://vimeo.com/api/v2/video/{$video_id}.json");
                    if($u){
                        $a = json_decode($u, true);
//                    $this->dump($a);
                        $thumb = $a[0]['thumbnail_medium'];
                    }


                    $UP = new UsersUploads();
                    $exist = $UP->exist($video_id,'video','vimeo');

                    if(!empty($exist)) {

                        return json_encode(array(
                            'm' => $this->translation['video_exist'],
                            's' => false
                        ));
                    }

                    $id = $UP->create($uid, $video_id, 'video', 'vimeo', $thumb);
                    return json_encode(array(
                        'images' =>  $this->renderItem(
                            array(
                                'id'    => $id,
                                'file'  => $video_id,
                                'type'  => 'video',
                                'provider' => 'vimeo',
                                'thumb'    => $thumb
                            )
                        ),
                        's' => 1
                    ));
                }
            }

            return '';
        }
        return $this->template->fetch('modules/users/upload_video');
    }

    public function deleteProfile()
    {
        if(! $this->request->isXhr()){
            return $this->template->fetch('modules/users/rm_profile');
        }
        $s = 0; $e = array(); $user = self::data();
        if(! $user){ die();}
        $comment = $this->request->post('comment', 'str');
        if(!$comment){
            $e[] = $this->translation['e_req_comment'];
        } else{
            $user['comment'] = $comment;
            $mail = new \PHPMailer;

            $mt = $this->load->model('app\Mailer');
            $template = $mt->getTemplate('remove_profile');

            $mail->setFrom('no-reply@' . $_SERVER['HTTP_HOST']);
            $mail->addAddress($template['email']);
            $mail->isHTML(true);

            $mail->Subject = $template['subject'];
            $this->template->assign('user', $user);
            $mail->Body    = $this->template->fetch('string:' . $template['body']);

            if(! $mail->send()){
                $e[] = 'MESSAGE_SEND_ERROR';
            } else {
                $s=1;
                $e[] = $this->translation['request_success'];
            }
        }

        return json_encode(array(
            's' => $s,
            'm' => implode('<br>', $e)
        ));
    }

    public function like()
    {
        $user_id = $_POST['user_id'];
        $client_id = $_POST['client_id'];

        $c = $this->mUsers->validateLike($user_id,$client_id);
        if ($c>0) return 1;

        $id = $this->mUsers->addLike($user_id,$client_id);

        return json_encode(array('c'=>$id));
    }



    /**
     * @param $id
     * @return string
     */
    public static function hashID($id)
    {
        return md5('n'. $id . 'x');
    }

    public static function isWorker()
    {
        return self::data('users_group_id') != 3;
    }

    public function changePassword()
    {
        if (!empty($_POST)) {
            $s = 0; $m = '';
            $data = $_POST['data'];

            if (empty($data)) return json_encode(['s' => $s, 'm' => $m]);

            if(!empty($data['password'])) {
                if (!$data['r_password']) {
                    $m = 'Повторіть пароль';
                }
                if ($data['password'] != $data['r_password']) {
                    $m = 'Паролі не співпадають';
                }

                if (empty($m)) {
                     $password = $this->mUsers->encodePassword($data['password']);

                     $s = $this->mUsers->update($_SESSION['app']['user']['id'], array(
                         'password' => $password
                     ));
                }
            }

            return json_encode(['s' => $s, 'm' => $m]);
        } else {
            return $this->template->fetch('modules/users/change_password');
        }
    }
}
