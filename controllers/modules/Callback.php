<?php
/**
 * Company Otakoyi.com
 * Author: wg
 * Date: 02.01.15 16:22
 */

namespace controllers\modules;

use controllers\core\DB;
use controllers\Module;

defined("SYSPATH") or die();

/**
 * Class Callback
 * @name Callback
 * @description Callback module
 * Class Callback
 * @package modules
 * @author wmgodyak mailto:wmgodyak@gmail.com
 * @version 1.0
 * @copyright &copy; 2014 Otakoyi.com
 *
CREATE TABLE IF NOT EXISTS `callbacks` (
`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
`users_id` int(10) unsigned DEFAULT NULL,
`phone` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
`name` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
`comment` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
`date` timestamp NULL DEFAULT NULL,
`processed` tinyint(1) NOT NULL DEFAULT '0',
PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
 *
 */
class Callback extends Module {
    private $db;

    public function __construct()
    {
        parent::__construct();
        $this->db = DB::instance();

    }
    public function index(){ return 'Callback::index()';}

    public function form(){
       return $this->template->fetch('modules/callback/form');
    }

    public function process()
    {
        if(!isset($_POST['skey'])) return '';
        if(SKEY != $_POST['skey']) return '';

        include_once DOCROOT . "/vendor/phpmailer/PHPMailer.php";
        $s=0; $e=array();
        $data = $_POST['data'];
        $data['date'] = date('Y-m-d H:i:s');
        $data['ip'] = $_SERVER['REMOTE_ADDR'];

        if(empty($data['name']) || strlen($data['phone']) < 10 || empty($data['comment']) ){
            $e[] = $this->translation['callback_e_fields']; // Заповніть вірно всі поля
        } else {
            $data['comment'] = htmlspecialchars(strip_tags($data['comment']));
            $s = $this->db->insert('callbacks', $data);
            $e[] = $this->translation['callback_success'];
        }

        return json_encode(array('s'=>$s, 'm' => implode('<br>', $e)));
    }

    public function install()
    {
    }

    public function uninstall()
    {
    }
}