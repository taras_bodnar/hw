<?php
/**
 * Company Otakoyi.com
 * Author: wg
 * Date: 02.01.15 16:22
 */

namespace controllers\modules;

use controllers\Module;
use models\app\Mailer;
use models\modules\Orders as Orders1;
use models\modules\Users;

defined("SYSPATH") or die();

/**
 * Class Notify
 * @name OYi.Notify
 * @description Моділь повідомлень
 * Class Notify
 * @package modules
 * @author wmgodyak mailto:wmgodyak@gmail.com
 * @version 1.0
 * @copyright &copy; 2015 Otakoyi.com
 */
class Notify extends Module {
    private $mMailer;
    private $mNotify;

    public function __construct()
    {
        parent::__construct();

        include_once DOCROOT . "/vendor/phpmailer/PHPMailer.php";

        $this->mNotify = new \models\modules\Notify();
        $this->mMailer   = new Mailer();
        $this->mUsers = new Users();
    }

    /**
     * @param $orders_id
     * @return bool
     * @throws \Exception
     * @throws \phpmailerException
     */
    public function ordersToCustomers($orders_id)
    {
        $template = $this->mMailer->getTemplate('order_user');

        $orders   = $this->mNotify->getOrdersInfo($orders_id);

        $mail = new \PHPMailer;

        $mail->setFrom($template['email'], $template['reply_to']);
        $mail->addAddress($orders['user_email'], $orders['user_name']);
        $mail->addReplyTo($orders['user_email'], $orders['user_name']);

        $mail->isHTML(true);

        $mail->Subject = $template['subject'];

        $this->template->assign('orders', $orders);
        $mail->Body = $this->template->fetch('string:' . $template['body']);
        $mail->Body = $this->makeFriendlyUrl($mail->Body);

        return $mail->send();
    }

    public function sendPassword($id,$password)
    {
        $template = $this->mMailer->getTemplate('send_password');

        $user = $this->mUsers->getInfoById($id);
        $mail = new \PHPMailer;

        $mail->setFrom($template['email'], $template['reply_to']);
        $mail->addAddress($user['email'], $user['name']);
        $mail->addReplyTo($user['email'], $user['name']);

        $mail->isHTML(true);

        $mail->Subject = $template['subject'];

        $this->template->assign(array(
            'email'=> $user['email'],
            'password'=> $password
        ));

        $mail->Body = $this->template->fetch('string:' . $template['body']);
        $mail->Body = $this->makeFriendlyUrl($mail->Body);

        return $mail->send();
    }

    /**
     * @param $orders_id
     * @return bool
     * @throws \Exception
     * @throws \phpmailerException
     */
    public function ordersToAdmin($orders_id)
    {
        $template = $this->mMailer->getTemplate('order_admin');

        $orders   = $this->mNotify->orderAccountInfo($orders_id);
        switch($orders['type']) {
            case 'week':
                $orders['type'] = 'здійснив платіж Hot Виконавця на 14 днів';
                break;
            case 'month':
                $orders['type'] = 'здійснив платіж Hot Виконавця на 30 днів';
                break;
            case 'year':
                $orders['type'] = 'здійснив платіж gold Акаунта';
                break;
            case 'oneC':
                $orders['type'] = 'здійснив платіж на ще одне місто';
                break;
            case 'twoC':
                $orders['type'] = 'здійснив платіж на два міста';
                break;

        }

        $mail = new \PHPMailer;

        $mail->setFrom('no-reply@hotwed.com.ua');
        $mail->addAddress($template['email'], $template['reply_to']);
        $mail->addReplyTo($template['email'], $template['reply_to']);

        $mail->isHTML(true);

        $mail->Subject = $template['subject'];

        $this->template->assign('orders', $orders);
        $mail->Body = $this->template->fetch('string:' . $template['body']);
        $mail->Body = $this->makeFriendlyUrl($mail->Body);
//        $mail->Body .= '<br>-----------------------------<br>';
//        $mail->Body .= var_export($orders, 1);
//
//        $mail->Body .= '<br>-----------------------------<br>';
//        $mail->Body .= $template['body'];
        return $mail->send();
    }

    public function customer($orders_id)
    {
        $template = $this->mMailer->getTemplate('order_costumer');

        $orders   = $this->mNotify->orderAccountInfo($orders_id);
        if(empty($orders['email'])) return;
        switch($orders['type']) {
            case 'week':
                $orders['type'] = 'здійснили платіж Hot Виконавця на 14 днів';
                break;
            case 'month':
                $orders['type'] = 'здійснили платіж Hot Виконавця на 30 днів';
                break;
            case 'year':
                $orders['type'] = 'здійснили платіж gold Акаунта';
                break;
            case 'oneC':
                $orders['type'] = 'здійснили платіж на ще одне місто';
                break;
            case 'twoC':
                $orders['type'] = 'здійснили платіж на два міста';
                break;

        }

        $mail = new \PHPMailer;

        $mail->setFrom('no-reply@hotwed.com.ua');
        $mail->addAddress($orders['email'], $orders['name']." ".$orders['surname']);
        $mail->addReplyTo($orders['email'], $orders['name']." ".$orders['surname']);

        $mail->isHTML(true);

        $mail->Subject = $template['subject'];

        $this->template->assign('orders', $orders);
        $mail->Body = $this->template->fetch('string:' . $template['body']);
        $mail->Body = $this->makeFriendlyUrl($mail->Body);

        return $mail->send();
    }

    public function admin($oId)
    {
        $mt = $this->load->model('app\Mailer');

//        $data= $this->data;

        $template = $mt->getTemplate('infoAdminPay');

        $mail = new \PHPMailer;
        $mail->CharSet = "UTF-8";
        $mail->setFrom('no-reply@'. $_SERVER['HTTP_HOST']);

        $mail->addAddress($template['email'], $template['reply_to']);

        $mail->isHTML(true);

        $mail->Subject = $template['subject'];
        $data['number'] = $oId;
        $order = new Orders1();
        $infoOrder = $order->getOrderInfo($oId);

        $data['link'] = "<a href=".$_SERVER['HTTP_HOST']."/engine/Orders/edit/".$infoOrder['id'].">Лінк</a>";
        $this->template->assign('data', $data);
        $mail->Body = $this->template->fetch('string:' . $template['body']);

//        if($this->debug){
//            echo $mail->Body; die();
//        }
        $this->log($mail->isError());
        return $mail->send();
    }

    protected function log($msg, $display=false)
    {
        $msg = date('Y-m-d H:i:s') .' '. $msg . "\n\n";
        file_put_contents($this->log_file, $msg, FILE_APPEND);
        if($display){
            echo $msg, '<br>';
        }
        return $this;
    }



    public function index(){}
    public function install(){return 1;}
    public function uninstall(){return 1;}
}