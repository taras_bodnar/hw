<?php
/**
 * Company Otakoyi.com
 * Author: wg
 * Date: 02.01.15 16:22
 */

namespace controllers\modules;

use controllers\Module;
use models\app\Guides;
use models\modules\UsersNotifications;
use models\modules\UsersOnline;

defined("SYSPATH") or die();

/**
 * Class Messages
 * @name Messages
 * @description Messages module
 * Class Messages
 * @package modules
 * @author wmgodyak mailto:wmgodyak@gmail.com
 * @version 1.0
 * @copyright &copy; 2014 Otakoyi.com
 */
class Messages extends Module {
    private $users_id;
    private $mMessages;

    public function __construct()
    {
        parent::__construct();

        $this->users_id = Users::data('id');

        if(!$this->users_id){
            $this->redirect('/');
        }

        $this->mMessages = new \models\modules\Messages();

    }

    public function dialogs()
    {
        $to_id = $this->request->get('p');
        if(!$to_id) $this->redirect404();

        $this->template->assign('to_id', $to_id);
        $items = array();
        foreach ($this->mMessages->get($this->users_id, $to_id) as $item) {
            $items[] = $this->dialogItem($item);

            // позначу як прочитані
            if($item['is_read'] == 0 && $item['is_out'] == 0){
                $this->mMessages->setRead($item['id']);
                UsersNotifications::instance()->setIsRead($item['id'], 'messages');
            }
        }

        $this->template->assign('items', implode('', $items));
        return $this->template->fetch('modules/messages/dialogs');
    }

    public function check()
    {
        $to_id = $this->request->post('p');
        if(!$to_id) return '';

//        $this->template->assign('to_id', $to_id);
        $items = array();
        $r = $this->mMessages
            ->getNew($this->users_id, $to_id);
        foreach ($r as $item) {
            $items[] = $this->dialogItem($item);

            // позначу як прочитані
            if($item['is_read'] == 0 && $item['is_out'] == 0){
                $this->mMessages->setRead($item['id']);
            }
        }

        return implode('', $items);
    }

    private function dialogItem($item)
    {
        $this->template->assign('item', $item);
        return $this->template->fetch('modules/messages/d_item');
    }

    /**
     * @return string
     */
    public function send()
    {
        $data = $this->request->post('data');
        if(!$data) die();

        $s = 0; $m = '';

        $data['message'] = strip_tags(trim($data['message']));

        if(empty($data['message'])) die();

        $id =  $this->mMessages->create($this->users_id, $data['to_id'], $data['message']);

        if($id > 0){
            $s = 1;
            $item = $this->mMessages->getItem($id);
            $item['is_out'] = 1;
            $m = $this->dialogItem($item);

            // notification
//            if(! UsersOnline::instance()->isOnline($data['to_id'])){
                $user = Users::data();
                if($user){
                    if($user['users_group_id'] == 3){
                        // user
                        $notify_id = UsersNotifications::instance()->newMessage($data['to_id'], $this->users_id, $data['message'], $id, 'worker');
                        \controllers\modules\UsersNotifications::sendNotificationMessage($notify_id);
                    } else{
                        // worker
                        $notify_id = UsersNotifications::instance()->newMessage($this->users_id, $data['to_id'], $data['message'], $id, 'user');
                        \controllers\modules\UsersNotifications::sendNotificationMessage($notify_id);
                    }
                }
//            }

        }


        return json_encode(
            array(
                's' => $s,
                'm' => $m
            )
        );
    }

    private function messageItem($item)
    {
        if($item['is_read'] == 0 && $item['is_out'] == 0){
            $this->mMessages->setRead($item['id']);
            UsersNotifications::instance()->setIsRead($item['id'], 'messages');
        }
        $this->template->assign('item', $item);
        return $this->template->fetch('modules/messages/m_item');
    }

    public function index()
    {
        $order = $this->request->get('order');
        switch($order){
            case 25: // new
                     $this->mMessages->where(" and m.is_read= 0 ");
                     $this->mMessages->where(" and m.to_id = {$this->users_id}  ");
                break;
            case 26: // important
                     $this->mMessages->where(" and m.important = 1 ");
                break;
            default:
                break;
        }


        $items = array();
//        $this->mMessages->debug();
        foreach ($this->mMessages->getDialogs($this->users_id) as $item) {
//            $this->dump($item);
            $items[] = $this->messageItem($item);
        }
//        die;
        // сортування
        $g = new Guides();


        $this->template->assign('sorting',$g->get(24));
        $this->template->assign('no_read', $this->mMessages->getTotalNewMessages($this->users_id));
        $this->template->assign('items', implode('', $items));
        return $this->template->fetch('modules/messages/index');
    }

    public function delete()
    {
        $id = (int)$_POST['id'];
        return $this->mMessages->delete($id, $this->users_id);
    }

    public function setImportant()
    {
        $id = (int)$_POST['id'];
        $status = (int)$_POST['status'];
        return $this->mMessages->setImportant($id, $status, $this->users_id) > 0 ? 1 : 0;
    }

    public function install()
    {
//        $this->addSQL("delete from settings where name='Messages_install' limit 1");
//        $this->addSQL("insert into settings (name,value) values ('Messages_install','OKI')");
//        $this->addTranslation('mod_Messages_hello', 'hello');
//        $this->addSetting('t1','Messages 1', 1 )
//             ->addSetting('t2', 'Messages2', 2)
//             ->addSetting('t3', 'Messages3', 2)
//             ->addSetting('t4', 'Messages4', 2)
//        ;
    }

    public function uninstall()
    {
//        $this->addSQL("delete from settings where name='Messages_install' limit 1");
//        $this->rmTranslations(array('mod_Messages_hello'));
    }
}