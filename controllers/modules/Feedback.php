<?php
/**
 * Company Otakoyi.com
 * Author: wg
 * Date: 27.09.14 12:31
 */

namespace controllers\modules;

use controllers\Module;

defined("SYSPATH") or die();

include_once DOCROOT . "/vendor/phpmailer/PHPMailer.php";

/**
 * Class Feedback
 * @name Feedback
 * @description форма зворотнього звязку
 * Class Feedback
 * @package modules
 * @author wmgodyak mailto:wmgodyak@gmail.com
 * @version 1.0
 * @copyright &copy; 2014 Otakoyi.com
 */
class Feedback extends Module {

    public function index()
    {
        return $this->template->fetch('modules/feedback');
    }

    /**
     * @return string
     * @throws \Exception
     * @throws \controllers\core\Exceptions
     * @throws \phpmailerException
     */
    public function process()
    {
        $data = $_POST['data'];
        $error = array();
        $s=0;

        if(empty($data['name']) || empty($data['email']) || empty($data['message']))
        {
            $error[] = $this->translation['feedback_e_empty_fields'];
        } else if(! \PHPMailer::validateAddress($data['email'])){
            $error[] = $this->translation['feedback_e_bad_email'];
        } else {
            $mt = $this->load->model('app\Mailer');
            $template = $mt->getTemplate('feedback');
            $mail = new \PHPMailer;

            $mail->setFrom($data['email'], $data['name']);
            $mail->addAddress($template['email'], $template['reply_to']);
            $mail->addReplyTo($template['email'], $template['reply_to']);

            $mail->isHTML(true);

            $mail->Subject = $template['subject'];
            $mail->Body    = $template['body'];

            $mail->Body = preg_replace_callback(
                '({[a-z-_]+})',
                function ($matches) use ($data) {
                    $matches[0] = str_replace(array('{','}'), array(), $matches[0]);
                    return isset($data[$matches[0]]) ? $data[$matches[0]] : $matches[0];
                },
                $template['body']
            );


            if(!$mail->send()) {
                $error[] = $mail->ErrorInfo;
            } else {
                $s=1;
                $error[] = $this->translation['feedback_success'];
            }
        }
        return json_encode(
            array(
                's' => $s,
                'm' => implode('<br>', $error)
            )
        );
    }

    public function getForm()
    {
        $this->template->assign(
            array(
                    "travel_name"=>$_POST['travel_name'],
                    "travel_price"=>$_POST['travel_price'],
                    "travel_date"=>$_POST['travel_date'],
                )
            );
        return $this->template->fetch("modules/feedback/reserveform");
    }

    public function reserveTravel()
    {
        $data = $_POST['data'];
        $error = array();
        $s=0;

        if(empty($data['name']) || empty($data['email']))
        {
            $error[] = $this->translation['feedback_e_empty_fields'];
        } else if(! \PHPMailer::validateAddress($data['email'])){
            $error[] = $this->translation['feedback_e_bad_email'];
        } else {
            $mt = $this->load->model('app\Mailer');
            $template = $mt->getTemplate('reservedTravel');
            $mail = new \PHPMailer;

            $mail->setFrom($data['email'], $data['name']);
            $emails = explode(',',$template['email']);
            foreach ( $emails as $item ) {
                $mail->addAddress($item, $template['reply_to']);
                $mail->addReplyTo($item, $template['reply_to']);
            }


            $mail->isHTML(true);

            $mail->Subject = $template['subject'];
            $mail->Body    = $template['body'];

            $mail->Body = preg_replace_callback(
                '({[a-z-_]+})',
                function ($matches) use ($data) {
                    $matches[0] = str_replace(array('{','}'), array(), $matches[0]);
                    return isset($data[$matches[0]]) ? $data[$matches[0]] : $matches[0];
                },
                $template['body']
            );


            if(!$mail->send()) {
                $error[] = $mail->ErrorInfo;
            } else {
                $s=1;
                $error[] = $this->translation['feedback_success'];
            }
        }
        return json_encode(
            array(
                's' => $s,
                'view'=> $this->template->fetch("modules/feedback/reserve_success"),
                'm' => implode('<br>', $error)
            )
        );
    }

    public function install()
    {
        $this
          /*  ->addTranslation(
            'feedback_title',
            'Виникли питання чи пропозиції? Звертайтесь'
        )
            ->addTranslation(
            'feedback_name',
            'Ім\'я та прізвище'
        )
            ->addTranslation(
            'feedback_email',
            'Електронна скринька'
        )
            ->addTranslation(
            'feedback_message',
            'Текст повідомлення'
        )
            ->addTranslation(
            'feedback_btn_send',
            'Текст повідомлення'
        )
        ->addTranslation(
            'feedback_description',
            'Шановні користувачі якщо у вас зявились питання чи пропозиції по роботі нашого сайту
            напишіть нам. Ми обовязково візьмемо до уваги надану вами інформацію. Приємного користування!'
        )*/
        ->addTranslation(
            'feedback_e_empty_fields',
            'Заповніть всі поля'
        )
        ->addTranslation(
            'feedback_e_bad_email',
            'Введіть вірну електронну адресу'
        )
        ->addTranslation(
            'feedback_success',
            'Дякуємо за ваше повідомлення. Нам важлива ваша думка'
        )
        ;
        return 1;
    }

    public function uninstall()
    {
        return 1;
    }
}