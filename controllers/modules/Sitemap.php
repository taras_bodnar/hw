<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 27.09.14 12:31
 */

namespace controllers\modules;

use controllers\Module;

defined("SYSPATH") or die();

/**
 * Class Sitemap
 * @name Sitemap
 * @description Sitemap module
 * Class Sitemap
 * @package modules
 * @author wmgodyak mailto:wmgodyak@gmail.com
 * @version 1.0
 * @copyright &copy; 2014 Otakoyi.com
 */
class Sitemap extends Module {

    private $m;

    public function __construct()
    {
        parent::__construct();
        $this->m = $this->load->model('modules\Sitemap');
    }

    public function index()
    {
        return $this->load->view(
            'modules/sitemap',
            array(
                'categories' => $this->getChildren(1, 2,1)
            )
        );
    }

    private function getChildren($parent_id, $level,$isfolder)
    {
        if($level < 0) return array();
        $items = array();
        $r = $this->m->getChildren($parent_id, $isfolder);
        foreach ($r as $row) {
//            echo $row['name'], ':' , $row['isfolder'],':', $level, '<br>';
            if($row['isfolder']){
                $l = $level;
                $c = $this->getChildren($row['id'], --$l,0);
                if(!empty($c)){
                    $row['children'] = $c;
                }
            }
            $items[] = $row;
        }
        return $items;
    }

    public function install(){}
    public function uninstall(){}
}