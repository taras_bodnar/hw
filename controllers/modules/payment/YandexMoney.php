<?php
/**
 * Company Otakoyi.com
 * Author: wg
 * Date: 12.01.15 18:05
 */

namespace controllers\modules\payment;

use controllers\modules\Payment;

defined("SYSPATH") or die();

/**
 * Class YandexMoney
 * @name YandexMoney
 * @author wmgodyak mailto:wmgodyak@gmail.com
 * @version 1.0
 * @copyright &copy; 2014 Otakoyi.com
 * @package controllers\modules\payment
 */
class YandexMoney extends Payment {
    public function index(){}
    public function install()
    {
        $this
            ->addSetting('yandex_id', 'Гаманець одержувача')
            ->addSetting('yandex_secret', 'Секретний ключ')
        ;
    }
    public function uninstall(){}
    public function checkout(){}
    public function callback(){}
}