<?php
/**
 * Company Otakoyi.com
 * Author: wg
 * Date: 12.01.15 18:05
 */

namespace controllers\modules\payment;

use controllers\modules\Payment;

defined("SYSPATH") or die();

/**
 * Class Webmoney
 * @name Webmoney
 * @author wmgodyak mailto:wmgodyak@gmail.com
 * @version 1.0
 * @copyright &copy; 2015 Otakoyi.com
 * @package controllers\modules\payment
 */
class Webmoney extends Payment {
    public function index(){}
    public function install()
    {
        $this
            ->addSetting('purse', 'Номер гаманця')
            ->addSetting('secret_key', 'Секретний ключ')
        ;
    }
    public function uninstall(){}
    public function checkout(){}
    public function callback(){}
}