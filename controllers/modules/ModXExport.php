<?php
/**
 * Company Otakoyi.com
 * Author: wg
 * Date: 02.01.15 16:22
 */

namespace controllers\modules;

use controllers\core\DB;
use controllers\Module;

defined("SYSPATH") or die();

/**
 * Class ModXExport
 * @name ModXExport
 * @description ModXExport module
 * Class ModXExport
 * @package modules
 * @author wmgodyak mailto:wmgodyak@gmail.com
 * @version 1.0
 * @copyright &copy; 2014 Otakoyi.com
 */
class ModXExport extends Module {
    private $db;
    private $content = array();
    private $total=0;

    public function __construct()
    {
        parent::__construct();

        $this->db = DB::instance()->switchDB('localhost','root','dell','plantsclub_orig');
    }

    public function index(){
        echo '<pre>';

        // експорт контенту
        /*
        $res = $this->parseModxSiteContent(0, 70);

        // вставка даних
        $this->db = $this->db->switchDB('localhost','root','dell','budyak');
//        $this->dump($this->db); die();

        $this->db->beginTransaction();
        if($this->insertContent($res)){
            $this->db->commit();
            echo 'Вставлено ' . $this->total . '<br>';
        } else{
            $this->db->rollBack();
        }
        */


        // експорт параметрів
//        $res  = $this->parseTmplvars();
//        $this->dump($res);
//        $this->insertTV($res);
//
        // експорт значень паратмерів
        $this->parseContentTV();


        // імпорт заображень
//        $this->importImages();

        //  внесення даних в базу

//        $this->setContentImages();


/*
        оновив контент
        $r = $this->db->select("select id,lang_id,content from plantsclub_orig.modx_site_content order by id asc")->all();

        $this->db = $this->db->switchDB('localhost','root','dell','budyak');
        foreach ($r as $row) {
            $content_id = $this->db->select("select id from content where modx_id={$row['id']} limit 1")->row('id');
            $languages_id = $row['lang_id'] == 1 ? 2 : 1;
            if(empty($content_id) || empty($languages_id)) continue;

            $this->db->update('content_info', array('content' => $row['content']) , " content_id={$content_id} and languages_id ={$languages_id} limit 1");

        }

*/
        die(' oki');
    }

    private function importImages()
    {
        $features_id = '27,29'; // для категоріq
        $features_id = '22,35,36,37,38';

        $files_not_ex = array(); $files_copied = array();
        $t = $this->db->select("
                select count(*) as t
                from `features_content_values`
                where features_id in ($features_id) and value <> ''
        ")->row('t');

        echo 'Загальна кілкість файлів ' . $t . '<br>';
        $start = isset($_GET['start']) ? (int)$_GET['start'] : 0;
        $r = $this->db->select("
                select id, value, content_id
                from `features_content_values`
                where features_id in ($features_id) and value <> ''
                 order by id limit $start, 10
        ")->all();
        foreach ($r as $row) {
            $from = DOCROOT . 'import/' . $row['value'];
            if(! file_exists($from)) {
                $files_not_ex[] = $from;
                continue;
            }
            // отримаю назву файлу
            ///assets/images/category/klen2.jpg
            $a = explode('/',  $row['value']);
            $file = end($a);
            $a = explode('.', $file);
            $ext = end($a);
            $file_name = md5($row['id']) . '.' . $ext;
            echo $file, ' : ' ,$file_name;
//            continue;
            // папка
            $to_dir   = DOCROOT . "uploads/content/{$row['content_id']}/source/";
            if(!is_dir($to_dir)){
                mkdir($to_dir, 0777, true);
            }

            // захист від дублів
            if(file_exists($to_dir . $file_name)) {
                echo '<br>'; continue;
            };

            if(copy($from, $to_dir . $file_name)){
                $files_copied[] = $file_name;
                echo ' OK';
            }

            echo '<br>';
        }


        echo "Перенесення завершено.<br>";

        echo 'неіснуючих шляхів:' . count($files_not_ex) . '<br>';
        if(!empty($files_not_ex)) $this->dump($files_not_ex);

        echo 'Перенесено зображень:' . count($files_copied);

        if(isset($_GET['continue'])){
            $files_not_ex = array();
        }

        if(empty($files_not_ex) && $start < $t){
            $start += 10;
            echo "<script>self.location.href='/ajax/ModXExport/index?start={$start}';</script>";
        } else{
            $this->log(var_export($files_not_ex, 1));
            echo "<br>
                <a href='/ajax/ModXExport/index?start={$start}&continue'>Пропустити</a> | <a href='/ajax/ModXExport/index?start={$start}'>Оновити</a>
            <br>";
        }
    }

    private function log($msg)
    {
        $msg = date('Y-m-d H:i:s') . '    ' . $msg;
        file_put_contents(DOCROOT . 'import.log', $msg, FILE_APPEND);
    }

    private function setContentImages()
    {
        echo 'Внесення зображень в базу <br>';
        $limit = 70000; $i=0; $d=0;
        if ($handle = opendir(DOCROOT . 'uploads/content/')) {
            while (false !== ($content_id = readdir($handle)) && $i <= $limit) {
                if ($content_id == "." || $content_id == "..")  continue;

                $source_dir = DOCROOT. "uploads/content/{$content_id}/source/";
                echo '<br><b>', $source_dir, '</b><br><br>'; $d++;
                // зчитую файли
                if ($handle2 = opendir($source_dir)) {
                    $sort = 0;
                    while (false !== ($file = readdir($handle2))) {
                        if ($file == "." || $file == "..") continue;

                        echo $i, ' ', $source_dir . $file ,' |   ', $content_id, ' ' , $file, '<br>';// continue;

                        $this->db->insert('content_images', array('content_id' => $content_id, 'name' => $file, 'sort' => $sort));
                        $sort++;
                    }
                }

                $i++;
            }
            closedir($handle);
        }
        echo 'Папок: ' . $d;
    }

    public function setFeaturesContent()
    {
        die('перенесено');
        $this->db = $this->db->switchDB('localhost','root','dell','plantsclub_orig');
        $r = $this->db->select("select * from modx_site_tmplvars_category group by content_id,tmplvars_id order by id asc ")->all();

        $this->db = $this->db->switchDB('localhost','root','dell','budyak');

        foreach ($r as $row) {
            $features_id = $this->db->select("select id from features where modx_id={$row['tmplvars_id']} limit 1")
                ->row('id');
            $content_id =  $this->db->select("select id from content where modx_id={$row['content_id']} limit 1")
                ->row('id');
            if(empty($content_id) || empty($features_id)) continue;

            $this->db->insert(
                "content_features",
                array('content_id' => $content_id, 'features_id' => $features_id, 'sort' => $row['position'])
            );
        }


    }


    private function parseContentTV()
    {

        $this->db = $this->db->switchDB('localhost','root','dell','plantsclub_orig');
        $r = $this->db->select("
            select * from modx_site_tmplvar_contentvalues order by id asc limit 150000,50000
        ")->all();
        $this->db = $this->db->switchDB('localhost','root','dell','budyak');
        foreach ($r as $row) {
            $features = $this->db->select("select id,type from features where modx_id={$row['tmplvarid']} limit 1")
                ->row();
            $content_id =  $this->db->select("select id from content where modx_id={$row['contentid']} limit 1")
                ->row('id');
            if(empty($content_id) || empty($features)) continue;

            $features_id = $features['id'];
            $languages_id = in_array($features['type'], array('number')) ? 0 : 1;
            $iv = array(
              'features_id'  => $features_id,
              'content_id'   => $content_id,
              'languages_id' => $languages_id,
              'value'        => $row['value']
            );
            $this->db->insert("features_content_values", $iv);
        }

    }
    private function insertTV($data)
    {
        $this->db = $this->db->switchDB('localhost','root','dell','budyak');
        $this->db->beginTransaction();
        foreach ($data as $iv) {
            $features_id = $this->db->insert('features', $iv['data']);
            if($features_id>0){
                foreach ($iv['info'] as $info) {
                    $info['features_id'] = $features_id;
                    $this->db->insert("features_info", $info);
                }

            }
        }
        $this->db->commit();
    }


    public function insertProductOptions()
    {
        die('ok');
        $iv = array();
        $this->db = $this->db->switchDB('localhost','root','dell','budyak');
        $r = $this->db->select("select id as products_id, modx_id from content where type_id=5 order by id asc")->all();

        $this->db = $this->db->switchDB('localhost','root','dell','plantsclub_orig');

        foreach ($r as $row) {
            $price = $this->db->select("select value from modx_site_tmplvar_contentvalues where contentid={$row['modx_id']} and tmplvarid=21 limit 1")->row('value');
            $code = $this->db->select("select value from modx_site_tmplvar_contentvalues where contentid={$row['modx_id']} and tmplvarid=31 limit 1")->row('value');
            $sale = $this->db->select("select value from modx_site_tmplvar_contentvalues where contentid={$row['modx_id']} and tmplvarid=40 limit 1")->row('value');
            $iv[] = array(
                'products_id' => $row['products_id'],
                'manufacturers_id' => 4666,
                'is_variant'  => 0,
                'price'       => empty($price) ? 0 : $price,
                'code'        => $code,
                'sale'        => empty($sale) ? 0 : 1
            );
        }
        $this->db = $this->db->switchDB('localhost','root','dell','budyak');
//        $this->dump($iv);
        foreach ($iv as $data) {
            $this->db->insert('products_options', $data);
        }

    }

    private function insertContent($res, $parent_id=0)
    {
        foreach ($res as $row) {
            $this->total++;
            $row['data']['parent_id'] = $parent_id;
            $id = $this->db->insert("content", $row['data']);
            foreach ($row['info'] as $info) {
                $info['content_id'] = $id;
                $this->db->insert("content_info", $info);
            }

            if(isset($row['children'])){
                $this->insertContent($row['children'], $id);
            }
        }

        return 1;
    }

    /**
     * @param $parent_id
     * @param int $level
     * @return array
     */
    private function parseModxSiteContent($parent_id, $level = 30)
    {
        $r = $this->db->select("
              select ua.*,
              ru.menutitle as menutitle_ru, ru.alias as alias_ru, ru.longtitle as longtitle_ru, ru.content as content_ru, ru.description as description_ru
              from modx_site_content ua
              join modx_site_content ru on ua.id=ru.id and ru.lang_id=1
              where ua.parent='{$parent_id}' and ua.lang_id=0
              group by id
              order by ua.id asc
              ")->all();
        if(empty($r)) return array();

        $res = array();
        foreach ($r as $i=>$row) {
            $this->total++;

            switch($row['template']){
                case 5:// 'main_page':
                    $templates_id = 1;
                    $type_id      = 1;
                    break;
                case 48:// 'category_product':
                case 67://'sub_renament':
                case 53://'subcat':
                    $templates_id = 3;
                    $type_id      = 3;
                    break;
                case 47://'manufacturer':
                case 62://'plants':
                    $templates_id = 5;
                    $type_id      = 5;
                    break;
                default:
                    $templates_id = 1;
                    $type_id      = 1;
                    break;
            }

            $res[$i] = array(
                'data'         => array(
                    'parent_id'    => $parent_id,
                    'isfolder'     => $row['isfolder'],
                    'type_id'      => $type_id,
                    'owner_id'     => 1,
                    'templates_id' => $templates_id, // підставити шаблон
                    'published'    => $row['published'],
                    'createdon'    => date('Y-m-d H:i:s', $row['createdon']),
                    'editedon'     => date('Y-m-d H:i:s', $row['editedon']),
                    'auto'         => 0,
                    'modx_id'      => $row['id'],
                ),
                'info'         => array(
                    array(
                        'languages_id' => 1,
                        'name'         => $row['menutitle'],
                        'alias'        => $row['alias'],
                        'title'        => $row['longtitle'],
                        'keywords'     => '',
                        'description'  => $row['description'],
//                        'content'      => $row['content'],
                    ),
                    array(
                        'languages_id' => 2,
                        'name'         => $row['menutitle_ru'],
                        'alias'        => $row['alias_ru'],
                        'title'        => $row['longtitle_ru'],
                        'keywords'     => '',
                        'description'  => $row['description_ru'],
//                        'content'      => $row['content_ru'],
                    )
                )
            );

            if($row['isfolder'] && $level >= 0){
                $res[$i]['children'] = $this->parseModxSiteContent($row['id'], --$level);
            }
        }

        return $res;
    }



    /**
     * @return array
     */
    private function parseTmplvars()
    {
        $res = array();
        $r = $this->db->select("select * from modx_site_tmplvars order by id asc")->all();
        foreach ($r as $i=>$row) {
            switch($row['type']){
                case 'richtext':
                case 'textarea':
                    $type = 'textarea';
                    break;
                case 'image':
                    $type = 'number';
                    break;
                case 'dropdown':
                case 'listbox':
                    $type = 'select';
                    break;
                case 'date':
                    $type = 'number';
                    break;
                case 'checkbox':
                    $type = 'checkbox';
                    break;
                case 'url':
                    $type = 'number';
                    break;
                case 'listbox-multiple':
                    $type = 'sm';
                    break;
                default:
                    $type = 'text';
                    break;
            }
            $res[$i] = array(
                'data' => array(
                    'modx_id'   => $row['id'],
                    'type'      => $type,
                    'published' => 1,
                    'auto'      => 0,
                    'owner_id'  => 1,
                    'external_id' => $row['foreign_key']
                 ),
                'info' => array(
                    array(
                        'languages_id' => 1,
                        'name'         => $row['caption']
                    ),
                    array(
                        'languages_id' => 2,
                        'name'         => $row['description']
                    )
                )
            );
        }

        return $res;
    }

    public function install()
    {
//        $this->addSQL("delete from settings where name='ModXExport_install' limit 1");
//        $this->addSQL("insert into settings (name,value) values ('ModXExport_install','OKI')");
//        $this->addTranslation('mod_ModXExport_hello', 'hello');
//        $this->addSetting('t1','ModXExport 1', 1 )
//             ->addSetting('t2', 'ModXExport2', 2)
//             ->addSetting('t3', 'ModXExport3', 2)
//             ->addSetting('t4', 'ModXExport4', 2)
//        ;
    }

    public function uninstall()
    {
//        $this->addSQL("delete from settings where name='ModXExport_install' limit 1");
//        $this->rmTranslations(array('mod_ModXExport_hello'));


    }
}