<?php
/**
 * Created by PhpStorm.
 * User: taras
 * Date: 09.03.16
 * Time: 17:26
 */

namespace controllers\modules;


use controllers\App;
use controllers\Module;
use models\app\Content;
use models\app\Guides;
use models\app\Mailer;
use models\modules\Pages;
use models\modules\Users as UserM;

include_once DOCROOT  ."vendor/acimage/AcImage.php";
class Forum extends Module
{
    private $root_id = 206;
    private $items_per_page = 10;
    private $item_pp = 5;

    private $avatars=false;

    public function __construct()
    {
        parent::__construct();
        $this->m = new \models\modules\Forum();
        $this->bC = new Blog();
        $this->users = new UserM();
        $this->mMailer = new Mailer();
        $this->pages = new Pages();
        $this->g = new Guides();
    }

    public function index()
    {
        $id = $this->request->param('handle');

        if (empty($id)) $this->redirect404();

        $info = $this->m->getInfo($id);

        if(empty($info)) $this->redirect404();

        $_GET['article'] = $id;
        $_POST['article'] = $id;
        $this->request->set('article', $info);
        $this->template->assign('article', $info);
        $lang_id = self::langugesId();
        $c = new Content();
        $alias = $c->getAliasById(207, $this->languages_id);
        $this->request->set('alias', $alias);
        $app = new App();
        $app->index();
    }

    public function install()
    {
        // TODO: Implement install() method.
    }

    public function uninstall()
    {
        // TODO: Implement uninstall() method.
    }

    public function getCategory()
    {
        $items = $this->m->getCategory();
        if(isset($_SESSION['app']['user']['users_group_id'])) {
            $uid = $_SESSION['app']['user']['users_group_id'];
            $groupId = $this->m->getParentGroupId($uid);
            $this->template->assign("groupId",$groupId);
        }

//        $this->dump($_SESSION['app']['user']);
        $this->template->assign(array(
            'items'=>$items
        ));

        return $this->template->fetch('modules/forum/category');
    }

    public function getArticles()
    {
        $id = $this->request->get('id');

        $id = $id!=$this->root_id?$id:0;

        if(isset($_SESSION['app']['user']['id'])) {
            $this->m->setJoin("left join forum_follow ff on ff.forum_id=f.id and ff.users_id={$_SESSION['app']['user']['id']}");
            $this->m->key("ff.status as follow");
        }

        if($id == 208 || $id == 209 || $id == 210) {
            if(isset($_SESSION['app']['user']['id']))
            {
                if($id==208) {
                    $this->m->setWhere(" and ff.status='follow' and ff.users_id={$_SESSION['app']['user']['id']}");
                } elseif($id==209) {
                    $this->m->setWhere(" and ff.status='ignore' and ff.users_id={$_SESSION['app']['user']['id']}");
                } else {
                    $this->m->setWhere(" and f.owner_id={$_SESSION['app']['user']['id']}");
                }
            } else {
                $this->redirect404();
            }
        } else {
            if($id>0) {
                $this->m->setWhere(" and cid={$id}");
            } else {
                if(isset($_SESSION['app']['user']['id'])) {
                    $this->m->setWhere(" and ff.status is NULL or ff.status!='ignore'");
                } else {
//                    $this->m->setWhere(1);
                }
            }
        }

        if(isset($_SESSION['app']['user']['users_group_id'])) {
            $uid = $_SESSION['app']['user']['users_group_id'];
            $groupId = $this->m->getParentGroupId($uid);
            if($groupId==2) {
                $this->m->setWhere(" and f.status in ('publish','onlyworker')");
            } else {
                $this->m->setWhere(" and f.status in ('publish')");
            }
        } else {
            $this->m->setWhere(" and f.status in ('publish')");
        }

        $total = $this->m->getTotalArticles();
        $t_total = $total;
//        die($t_total);
        $paginator = new Paginator($total, $this->items_per_page, $this->root_id . ';');
        $limit = $paginator->getLimit();
        $this->m->limit($limit['start'], $limit['num']);
        $this->template->assign('pagination', $paginator->getPages());

        $items = $this->getItems();
//        $items = array();
        $this->template->assign('root_id', $id);
        $this->template->assign('items', implode('', $items));

        $start = $this->request->get('p');
        if(!$start) $start=1;
        $total = $total - $start*$this->items_per_page;
        if($total<0) $total=0;
        $this->template->assign('start', $start);
        $this->template->assign('total', $total);
        $this->template->assign('t_total', $t_total);

        return $this->template->fetch('modules/forum/list');
    }

    private function getItems()
    {
        $res = array();

        foreach ($this->m->getArticles() as $item) {
            $item['img'] = array ();
            foreach ( $this->m->getUploads($item['id'], 5) as $k => $img ) {
                if ( isset($img['type']) && $img['type'] == 'video' ) {
                    if ( !isset($img['provider']) ) continue;
                    $v = self::makeVideoUrl($img['path'], $img['provider'], $img['thumb']);
//                    $this->dump($v);
                    $item['img'][$k]['big'] = $v['big'];
                    $item['img'][$k]['thumb'] = $v['thumb'];
//                    $item['img'][$k]['big']   = "http://www.youtube.com/embed/{$img['file']}";
//                    $item['img'][$k]['thumb'] = "http://img.youtube.com/vi/{$img['file']}/0.jpg";
                    $item['img'][$k]['type'] = 'video';
                } else {
                    $item['img'][$k]['big'] = '/uploads/forum/' . $item['id'] . '/' . 'big_' . $img['path'];
                    $item['img'][$k]['thumb'] = '/uploads/forum/' . $item['id'] . '/' . 'thumb_' . $img['path'];
                }
            }
            $item['date'] = $item['dd']." ".$this->bC->formatMonth($item['mm']);
            $item['t_comment'] = $this->m->clearWhere()->getTotalComment($item['id']);
//            $item['content'] = $this->tease($item['content'],7);
            $this->template->assign('item', $item);
            $res[] = $this->template->fetch('modules/forum/item');
        }
        return $res;
    }

    private function tease($body, $sentencesToDisplay = 2) {
        $nakedBody = preg_replace('/\s+/',' ',strip_tags($body));
        $sentences = preg_split('/(\.|\?|\!)(\s)/',$nakedBody);

        if (count($sentences) <= $sentencesToDisplay)
            return $nakedBody;

        $stopAt = 0;
        foreach ($sentences as $i => $sentence) {
            $stopAt += strlen($sentence);

            if ($i >= $sentencesToDisplay - 1)
                break;
        }

        $stopAt += ($sentencesToDisplay * 2);
        return trim(substr($nakedBody, 0, $stopAt));
    }

    public function following()
    {
        $id = (int)$_POST['id'];
        $uid = (int)$_POST['uid'];
        $status = $_POST['status'];

        if($status!='follow' && $status!='ignore') return false;

        return $this->m->following($id,$uid,$status);
    }

    public function article()
    {
        $article = $this->request->get('article');

        $ui = $this->users->getInfoById($article['owner_id']);

        $this->template->assign('ui',$ui);

        $images = array ();
        foreach ( $this->m->getUploads($article['id']) as $k => $img ) {
            if ( isset($img['type']) && $img['type'] == 'video' ) {
                $v = self::makeVideoUrl($img['path'], $img['provider'], $img['thumb']);
                if ( empty($v['thumb']) ) continue;
//                $this->dump($v);
                $images[$k]['big'] = $v['big'];
                $images[$k]['thumb'] = $v['thumb'];
//                $images[$k]['thumb'] = "http://img.youtube.com/vi/{$img['file']}/0.jpg";
                $images[$k]['type'] = 'video';
            } else {
                $images[$k]['big'] = '/uploads/forum/' . $article['id'] . '/' . 'big_' . $img['path'];
                $images[$k]['thumb'] = '/uploads/forum/' . $article['id'] . '/' . 'thumb_' . $img['path'];
            }
        }
//        $this->dump($images);
        $this->template->assign('img',$images);

        $info = $this->m->getInfo($article['id']);
        $info['start_date'] = $info['dds']." ".$this->bC->formatMonth($info['mms']);
        $info['end_date'] = $info['dde']." ".$this->bC->formatMonth($info['mme']);
//        $this->dump($info);
        $this->template->assign('info',$info);

        $participants = $this->m->getParticipants($article['id']);
        if(isset($_SESSION['app']['user']['id'])) {
            $takePart = $this->m->getUserPart($article['id'],$_SESSION['app']['user']['id']);
            $this->template->assign('takePart',$takePart);
        }
        $this->template->assign('part',$participants);
//        $this->dump($participants);

        return $this->template->fetch('modules/forum/article');
    }

    public function getPart()
    {
        $article_id = (int)$_POST['id'];
        $user_id = (int)$_POST['uid'];
        $ui = $this->users->getInfoById($user_id);

        $s = $this->m->takePart($article_id,$user_id);

        return json_encode(array(
            's'=>$s,
            'ui'=>$ui
        ));
    }

    public function create()
    {
        if(isset($_SESSION['app']['user']['users_group_id'])) {
            $uid = $_SESSION['app']['user']['users_group_id'];
            $groupId = $this->m->getParentGroupId($uid);
            if($groupId==2) {
                $id = $this->m->createAuto($_SESSION['app']['user']['id'],214);
                return $this->edit($id);
            } else {
                $this->redirect404();
            }
        } else {
            $this->redirect404();
        }
    }

    public function edit($id)
    {
        $permission = $this->m->getPermision($id,$_SESSION['app']['user']['id']);
           if(empty($permission)) {
               $this->redirect404();
           }

        $this->template->assign('id',$id);

        $type = $this->pages->getItems(211);
        $this->template->assign('type',$type);

        $category = $this->m->getCategory();
        $this->template->assign('category',$category);

        $town = $this->g->get(9);
        $this->template->assign('town',$town);

        $this->template->assign('info',$this->m->getInfo($id));

        $this->template->assign('media',$this->media($id));

        return $this->template->fetch('modules/forum/create');
    }

    public function media($id)
    {
        if(! $_SESSION['app']['user']['id']) $this->redirect('/');

        $items = '';
        $c = count($this->m->getMedia($id));
        foreach ($this->m->getMedia($id) as $item) {
            $items .= $this->renderItem($item,$id);
        }

        $this->template->assign('items', $items)->assign('c',$c)->assign('id',$id);
        return $this->template->fetch('modules/forum/media/portfolio');
    }

    private function renderItem($item,$id)
    {
//        $this->dump($item);
        if(isset($item['type']) && $item['type'] == 'video'){
//            $item['big']   = "http://www.youtube.com/embed/{$item['file']}";
            $item['thumb'] = "http://img.youtube.com/vi/{$item['path']}/default.jpg";
            $v = self::makeVideoUrl($item['path'], $item['provider'], $item['thumb']);
            $item['big'] = $v['big'];
            $item['thumb'] = $v['thumb'];
        } else{
            $item['big']   = '/uploads/forum/' . $id .'/' . 'big_'. $item['path'];
            $item['thumb'] = '/uploads/forum/' . $id .'/' . 'thumb_'. $item['path'];
        }
        $this->template->assign('item', $item);
        return $this->template->fetch('modules/forum/media/portfolio_item');
    }


    public static function makeVideoUrl($file, $provider, $thumb = '')
    {
        $item = array();
        switch($provider){
            case 'youtube':

                $item['big']   = "http://www.youtube.com/embed/{$file}";
                $item['thumb'] = "http://img.youtube.com/vi/{$file}/default.jpg";
                break;
            case 'vimeo':
                //https://developer.vimeo.com/player/embedding
                $item['big']   = "//player.vimeo.com/video/{$file}";
                $item['thumb'] = $thumb;
                /*
                $thumb = @file_get_contents("https://vimeo.com/api/v2/video/{$file}.json");
                if($thumb){
                    $a = json_decode($thumb, true);
//                    $this->dump($a);
                    $item['thumb'] = $a[0]['thumbnail_medium'];
                }
                */
                break;
        }

        return $item;
    }

    public function uploadImages()
    {

        $forum_id = (int)$_POST['forum_id'];
        $c = count($this->m->getMedia($forum_id));

        if($c>35) return 1;

        if(!isset($_FILES['images']) || !$forum_id ) return '';

        $allowed_type = array("image/jpeg","image/png");
        $res = array();

        $dir = '/uploads/forum/' .$forum_id. '/';
        $upload_dir = DOCROOT . $dir;
        if(!is_dir($upload_dir)) mkdir($upload_dir, 0775);


        foreach ($_FILES['images']['name'] as $k=>$name) {
            // check allowed type

            if(! in_array($_FILES['images']['type'][$k], $allowed_type)) continue;

            $image_tmp = $_FILES['images']['tmp_name'][$k];

            $ext = '.' . pathinfo($name, PATHINFO_EXTENSION);
            $image_name = md5(microtime(true)) . $ext;

            // resize to 1600x1200
            $img = \AcImage::createImage($image_tmp);
            \AcImage::setRewrite(true);
            \AcImage::setQuality(70);
//            $img->resize(1600, 1200);
            $img->resizeByWidth(1000);
            $img->save($upload_dir . 'big_'.$image_name);

            // resize to 162x162
            $img = \AcImage::createImage($image_tmp);
            \AcImage::setRewrite(true);
            \AcImage::setQuality(100);
            $img->cropCenter('1pr', '1pr');
            $img->thumbnail(360, 360);
            $img->save($upload_dir . 'thumb_'.$image_name);

            $id = $this->m->createMedia($forum_id, $image_name);

            if($id > 0){
                $res['images'][] = $this->renderItem(
                    array(
                        'id'   => $id,
                        'path' => $image_name
                    ),$forum_id
                );
            }
        }

        return json_encode($res);
    }

    public function rmMediaItem()
    {
        $id = $this->request->post('id');

        if(! $id) return 0;

        $info = $this->m->getMediaInfo($id);

        if(empty($info)) return 0;

        if($info['type'] = 'img'){
            $dir = '/uploads/forum/' . $id . '/';
            $upload_dir = DOCROOT . $dir;
            @unlink($upload_dir . 'big_'.$info['path']);
            @unlink($upload_dir . 'thumb_'.$info['path']);
        }

        return $this->m->deleteRow('forum_photo', $id);
    }

    public function process($id)
    {
        if(empty($id)) die('WRONG ID');

        $data = $_POST['data'];

        $s=0;
        $e = array();

        if(empty($e)) {
            $data['auto'] = 0;
            $data['editedon'] = date('Y-m-d H:i:s');
            $data['content'] = htmlspecialchars($data['content']);
            if(empty($id)) return '';

            if(isset($_POST['start_date']) && isset($_POST['start_time'])) {
                $data['event_start_date'] = $_POST['start_date']." ".$_POST['start_time'];
            }

            if(isset($_POST['end_date']) && isset($_POST['end_time'])) {
                $data['event_end_date'] = $_POST['end_date']." ".$_POST['end_time'];
            }

            $s = $this->m->update($id, $data);
            if($s > 0) {
                $this->m->updateMedia($id);
                $e[] = 'save_success';
            } else {
                $e[] = 'error';
            }
        }
        $c = new Content();

        $event_id = isset($data['event']) && $data['event']==1?213:212;
        return json_encode(array(
            's'=>$s > 0,
            'e'=>implode('<br>', $e),
            'id'=>$id,
            'r'=>$c->getAliasById(206, self::langugesId())
        ));
    }

    public function delete()
    {
        $id = (int)$_POST['id'];
        return $this->m->delete($id);
    }

    public function uploadVideo()
    {
        if($this->request->post('process') == 1){
            $forum_id = (int)$_POST['forum_id'];
            $url = $this->request->post('url');

            if(!$forum_id || ! $url) return 0;

            if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match)) {
                $video_id = $match[1];

                $id = $this->m->createMedia($forum_id, $video_id, 'video', 'youtube');

                return json_encode(array(
                    'images' =>  $this->renderItem(
                        array(
                            'id'    => $id,
                            'path'  => $video_id,
                            'type'  => 'video',
                            'provider' => 'youtube'
                        ),$forum_id
                    ),
                    's' => 1
                ));
            } elseif (preg_match("/(?:https?:\/\/)?(?:www\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|)(\d+)(?:$|\/|\?)/", $url, $id)) {
                $video_id = $id[3];
                //https://vimeo.com/137591399
                if($video_id > 0){
                    $thumb = '';
                    $u = @file_get_contents("https://vimeo.com/api/v2/video/{$video_id}.json");
                    if($u){
                        $a = json_decode($u, true);
//                    $this->dump($a);
                        $thumb = $a[0]['thumbnail_medium'];
                    }


                    $id = $this->m->createMedia($forum_id, $video_id, 'video', 'vimeo', $thumb);
                    return json_encode(array(
                        'images' =>  $this->renderItem(
                            array(
                                'id'    => $id,
                                'file'  => $video_id,
                                'type'  => 'video',
                                'provider' => 'vimeo',
                                'thumb'    => $thumb
                            ),$forum_id
                        ),
                        's' => 1
                    ));
                }
            }

            return '';
        }
        $this->template->assign('forum_id',$_POST['forum_id']);

        return $this->template->fetch('modules/forum/media/upload_video');
    }


    public function comments()
    {
        $workers_id = $this->request->get('p', 'i');
        $article = $this->request->get('article');
        $total = $this->m->getTotalComment($article['id']);
//        if(empty($total)) return ''; //$this->translation['no_comments'];
        $this->m->setWhere(" and parent_id=0");
        $t_parent = $this->m->getTotalComment($article['id']);

        $items  = '';
        foreach ($this->m->limit(0, $this->item_pp)->getComments($article['id']) as $item) {
            $items .= $this->item($item);
        }
        $left = $t_parent -  $this->item_pp;
        if($left < 0 ) $left = 0;

        $this->template->assign('comments_show_more', $left>0);
        $this->template->assign('comments_total', $total);
        $this->template->assign('comments_items', $items);
        $this->template->assign('workers_id', $workers_id);
        $this->template->assign('workers_id', $workers_id);
        $this->template->assign('is_worker', Users::isWorker());
        return $this->template->fetch('modules/forum/comments/items');
    }

    public function moreComments()
    {
        $start = $this->request->post('p', 'i');

        $total = $this->m->getTotalComment($_POST['id']);

        $items  = '';
        foreach ($this->m->limit($start, $this->item_pp)->getComments($_POST['id']) as $item) {
            $items .= $this->item($item);
        }
        $left = $total -  $this->item_pp - $start;
        if($left < 0 ) $left = 0;
        return json_encode(array(
            't'     => $left,
            'items' => $items
        ));
    }

    private function item($item)
    {
//        $this->dump($item);
        if($this->avatars){
            $item['icon'] = 'http://www.gravatar.com/avatar/' . md5( strtolower( trim( $item['email'] ) ) ) .'?s=80&d=mm&r=g';
        }
        $item['portfolio'] = $this->m->getCommentMedia($item['id']);
        $this->template->assign('item', $item);
        return $this->template->fetch('modules/forum/comments/item');
    }

    public function form()
    {
        $uid = $this->request->post('uid', 'i');
        $id = $this->request->post('id', 'i');
        if(!$uid) return 'Invalid contentID';

        $comment_id = $this->m->createCommentAuto($id,$uid);

        $this->template->assign('uid', $uid);
        $this->template->assign('id', $id);
        $this->template->assign('comment_id', $comment_id);
        $this->template->assign('portfolio',$this->mediaComment($comment_id));
        return $this->template->fetch('modules/forum/comments/form');
    }

    public function createCommentAuto()
    {
        $uid = $this->request->post('user_id', 'i');
        $id = $this->request->post('forum_id', 'i');
        if(!$uid) return 'Invalid contentID';

        return $this->m->createCommentAuto($id,$uid);
    }

    public function createComment()
    {
        include_once DOCROOT . "/vendor/phpmailer/PHPMailer.php";
        $s=0; $e=array(); $item = '';

        $users_id = Users::data('id');

        $data = $_POST['data'];
        $data['ip'] = $_SERVER['REMOTE_ADDR'];
        $data['content'] = htmlspecialchars(strip_tags($data['content']));

        if(isset($data['parent_id'])) {
            $this->m->updateRow('forum_comment',$data['parent_id'],array('isfolder'=>1));
        }

        if( ! $users_id){
            $e[] = $this->translation['comments_not_login'];
        } elseif(empty($data['content'])){
            $e[] = $this->translation['comments_empty_msg'];
        } else {
            $data['users_id'] = $users_id;
            $data['date'] = date("Y-m-d H:i:s");

            $s = $this->m->createComment($data);
            $e[] = $this->translation['fcomments_success'];

            $template = $this->mMailer->getTemplate('new_forum_comment');

            $mail = new \PHPMailer;

            $owner_id = $this->m->getInfo($data['forum_id']);

            $ui = $this->users->getInfoById($owner_id['owner_id']);

            if(!empty($ui['email'])) {
                $mail->setFrom('no-reply@hotwed.com.ua', $template['reply_to']);
                $mail->addAddress($ui['email'], $ui['name']);
                $mail->addReplyTo($ui['email'], $ui['name']);

                $this->template->assign("forum_id",$data['forum_id']);
                $mail->Body = $this->template->fetch('string:' . $template['body']);
                $mail->Body = $this->makeFriendlyUrl($mail->Body);
                $mail->isHTML(true);

                $mail->Subject = $template['subject'];

                $mail->Send();
            }

            $lastComment = $this->m->getLastComment($data['comment_id']);
            $item = $this->item($lastComment);
        }

        return json_encode(array('s'=>$s, 'm' => implode('<br>', $e),'item'=>$item,'parent_id'=>$lastComment['parent_id']));
    }

    public function mediaComment($id)
    {
        if(! $_SESSION['app']['user']['id']) $this->redirect('/');

        $items = '';
        $c = count($this->m->getCommentMedia($id));
        foreach ($this->m->getCommentMedia($id) as $item) {
            $items .= $this->renderItemComment($item,$id);
        }

        $this->template->assign('items', $items)->assign('c',$c)->assign('id',$id);
        return $this->template->fetch('modules/forum/comments/media/portfolio');
    }

    private function renderItemComment($item,$id)
    {
//        $this->dump($item);
        if(isset($item['type']) && $item['type'] == 'video'){
//            $item['big']   = "http://www.youtube.com/embed/{$item['file']}";
            $item['thumb'] = "http://img.youtube.com/vi/{$item['path']}/default.jpg";
            $v = self::makeVideoUrl($item['path'], $item['provider'], $item['thumb']);
            $item['big'] = $v['big'];
            $item['thumb'] = $v['thumb'];
        } else{
            $item['big']   = '/uploads/forum_comment/' . $id .'/' . 'big_'. $item['path'];
            $item['thumb'] = '/uploads/forum_comment/' . $id .'/' . 'thumb_'. $item['path'];
        }
        $this->template->assign('item', $item);
        return $this->template->fetch('modules/forum/comments/media/portfolio_item');
    }

    public function uploadCommentImages()
    {

        $forum_comment_id = (int)$_POST['forum_comment_id'];

        $c = count($this->m->getCommentMedia($forum_comment_id));

        if($c>35) return 1;

        if(!isset($_FILES['images']) || !$forum_comment_id ) return '';

        $allowed_type = array("image/jpeg","image/png");
        $res = array();

        $dir = '/uploads/forum_comment/' .$forum_comment_id. '/';
        $upload_dir = DOCROOT . $dir;
        if(!is_dir($upload_dir)) mkdir($upload_dir, 0775);


        foreach ($_FILES['images']['name'] as $k=>$name) {
            // check allowed type

            if(! in_array($_FILES['images']['type'][$k], $allowed_type)) continue;

            $image_tmp = $_FILES['images']['tmp_name'][$k];

            $ext = '.' . pathinfo($name, PATHINFO_EXTENSION);
            $image_name = md5(microtime(true)) . $ext;

            // resize to 1600x1200
            $img = \AcImage::createImage($image_tmp);
            \AcImage::setRewrite(true);
            \AcImage::setQuality(70);
//            $img->resize(1600, 1200);
            $img->resizeByWidth(1000);
            $img->save($upload_dir . 'big_'.$image_name);

            // resize to 162x162
            $img = \AcImage::createImage($image_tmp);
            \AcImage::setRewrite(true);
            \AcImage::setQuality(100);
            $img->cropCenter('1pr', '1pr');
            $img->thumbnail(360, 360);
            $img->save($upload_dir . 'thumb_'.$image_name);

            $id = $this->m->createCommentMedia($forum_comment_id, $image_name);

            if($id > 0){
                $res['images'][] = $this->renderItemComment(
                    array(
                        'id'   => $id,
                        'path' => $image_name
                    ),$forum_comment_id
                );
            }
        }

        return json_encode($res);
    }
}