<?php
/**
 * Company Otakoyi.com
 * Author: wg
 * Date: 02.01.15 16:22
 */

namespace controllers\modules;

use controllers\core\Config;
use controllers\core\DB;
use controllers\core\Settings;
use controllers\Module;
use models\modules\Catalog;
use models\modules\Users;
use models\modules\UsersOnline;

defined("SYSPATH") or die();

/**
 * Class Cron
 * @name Cron
 * @description Cron module
 * Class Cron
 * @package modules
 * @author wmgodyak mailto:wmgodyak@gmail.com
 * @version 1.0
 * @copyright &copy; 2014 Otakoyi.com
 * /usr/bin/wget -O /dev/null http://hotved.demo24.com.ua/route/Cron/index
 * /usr/bin/wget -O /dev/null http://hotved.demo24.com.ua/route/Cron/stat
 */
class Cron extends Module
{

    private $db;

    public function __construct()
    {
        parent::__construct();

        $this->db = DB::instance();
        $this->mUsers = new Users();
        $this->mCatalog = new Catalog();
    }

    public function index()
    {
        UsersOnline::instance()->clearOffline();
        $this->averagePrices();
    }

    /**
     * Оновлення статистики середніх цін в каталозі виконавців
     */
    private function averagePrices()
    {
        $r = $this->db->select("
            select SUM(price_per_hour) as pph, SUM(price_per_day) as ppd, COUNT(id) as t
            from users
            where users_group_id in(select id from users_group where parent_id= 2) and price_per_hour > 0 and price_per_day > 0
        ")->row();

        $prices = array(
            'pph' => ceil($r['pph'] / $r['t']),
            'ppd' => ceil($r['ppd'] / $r['t'])
        );

        // save to settings
        Settings::instance()->set('workers_pph', $prices['pph']);
        Settings::instance()->set('workers_ppd', $prices['ppd']);

    }

    public function stat()
    {
        $now = date('Y-m-d');
        $week = date('Y-m-d', strtotime('-1 week'));

        $r = $this->db->select("select s.users_id, SUM(s.views) as t, u.email, u.name,u.surname
              from  users_stat s
              join users u on u.id=s.users_id
              where s.date between '{$week}' and '{$now}'
              group by s.users_id")->all();
//        $this->dump($r);
        foreach ($r as $row) {
            UsersNotifications::sendStatMessage($row);
        }

    }

    public function formatMark()
    {
        $this->log("formatMark ".date("Y-m-d H:i:s"));

        $items = $this->db->select("
                Select * from users
        ")->all();

        foreach ($items as $item) {
            $this->db->update("users", array(
                'mark' => $this->points($item['id'])
            ), " id={$item['id']} limit 1");
        }
    }

    public function points($id)
    {
        $point = $this->mUsers->getInfoById($id, 'point');

        //Витягую вартість за послугу

        $s = $this->mUsers->getPrice($id);

        $point = $s == 1 ? $point + 5 : $point;

        //Витягую знижки
        $s = $this->mUsers->getDiscount($id);
        $point = $s == 1 ? $point + 10 : $point;

        //Витягую портфоліо
        $s = $this->mUsers->getPortfolio($id);
        $point = $s > 0 ? $point + 10 : $point;

        //Витягую коментарі
        $s = $this->mCatalog->getCommentsTotal($id);
        $point = $s > 0 ? $point + (10 * $s) : $point;

        //Витягую лайки
        $s = $this->mUsers->getTotalLike($id);
        $point = $s > 0 ? $point + (1 * $s) : $point;

        //витягую Підтверджені замовлення
        $s = $this->mUsers->getConfirmOrders($id);
        $point = $s > 0 ? $point + (20 * $s) : $point;


        return $point;
    }

    public function deleteOrderAccount()
    {
        $orders = $this->db->select("select * from ordersAccount
          where status_id=1 and UNIX_TIMESTAMP(end_date)<=UNIX_TIMESTAMP(NOW()) ")->all();

        foreach ($orders as $order) {
            if ($order['type'] == 'week' || $order['type'] == 'month') {
                $this->db->update('users', array(
                    'hot' => 0,
                    'hot_date' => 0
                ), " id={$order['users_id']}");
            } elseif ($order['type'] == 'year') {
                $this->db->update('users', array(
                    'gold' => 0,
                    'gold_date' => 0
                ), " id={$order['users_id']}");
            } elseif ($order['type'] == 'oneC') {
                $this->db->delete("users_cities", " users_id={$order['users_id']} and main=0 limit {$order['city_index']}");
            } elseif ($order['type'] == 'twoC') {
                $this->db->delete("users_cities", " users_id={$order['users_id']} and main=0 limit {$order['city_index']}");
            }
            $this->db->update("ordersAccount",array(
              'status_id' => 0
            )," id = {$order['id']} limit 1");
        }
    }

    public function photoDay()
    {
        $this->log("photoDay ".date("Y-m-d H:i:s"));
//        if(date('H:i') != '08:00') return false;
//        return 1;
        $res = $this->db->select("
                SELECT uu.*,CONCAT_WS( ' ' ,u.name, u.surname) as username FROM `img_like` il
                join users_uploads uu on uu.id=il.users_uploads_id
                join users u on u.id = uu.users_id
                WHERE  uu.type='img'  and uu.createdon > DATE_SUB(CURDATE(), INTERVAL 1 WEEK)  and uu.day_check!=1
                group by il.users_uploads_id order by count(il.id) desc limit 1
        ")->row();

        if (!empty($res)) {
            $this->db->update("users_uploads", array(
                "photo_day" => date("Y-m-d", strtotime(' -1 day')),
                "day_check" => 1
            ),
                "id={$res['id']} limit 1");
        }

        return 1;
    }

    public function videoDay()
    {
        $this->log("VideoDay ".date("Y-m-d H:i:s"));

        $res = $this->db->select("
                SELECT uu.*,CONCAT_WS( ' ' ,u.name, u.surname) as username FROM `img_like` il
                join users_uploads uu on uu.id=il.users_uploads_id
                join users u on u.id = uu.users_id
                WHERE  uu.type='video'  and uu.createdon > DATE_SUB(CURDATE(), INTERVAL 1 WEEK)  and uu.day_check!=1
                group by il.users_uploads_id order by count(il.id) desc limit 1
        ")->row();

        if (!empty($res)) {

            $this->db->update("users_uploads", array(
                "photo_day" => date("Y-m-d", strtotime(' -1 day')),
                "day_check" => 1
            ),
                "id={$res['id']} limit 1");
        }


        return 1;
    }

    public function install()
    {
    }

    public function uninstall()
    {
    }

    public function generateCityAlias()
    {
        $items = $this->db->select("select g.id,i.name from guides g
                            join guides_info i on i.guides_id=g.id and i.languages_id=1
                            where g.parent_id=9
                            ")->all();

        foreach ($items as $item) {
            $alias = $this->translit($item['name'],'uk');
            $this->db->update('guides',array(
                'value' => $alias
            ),"id={$item['id']} limit 1",1);
        }
    }

    public static function translit($text,$code){

        $text = mb_strtolower(trim($text),'utf8');
        $text = preg_replace("/[^A-Za-z0-9а-яА-Яіїєёыэъñéèàùêâôîûëïüÿç\- \/]/u", "", $text);
        $text = str_replace('/','-', $text);
        $table = Config::instance()->get('translit.' . $code);
        if(empty($table)) {
            $table = Config::instance()->get('translit.def');
        }

        $text = strtr($text,$table);

        return $text;
    }

    /**
     * @param $msg
     * @return int
     */
    private function log($msg)
    {
        $msg = date('Y-m-d H:i:s') . '    ' . $msg ."\r\n";
//        if($this->debug){
//            echo $msg;
//        }
        return file_put_contents(DOCROOT . 'logs/cron-'. date('Ymd') .'.log', $msg, FILE_APPEND);
    }
}