<?php
/**
 * Company Otakoyi.com
 * Author: wg
 * Date: 02.01.15 16:22
 */

namespace controllers\modules;

use models\app\Mailer;
use models\modules\UsersOnline;

defined("SYSPATH") or die();

/**
 * Class Users
 * @name Users
 * @description Users module
 * Class Users
 * @package modules
 * @author wmgodyak mailto:wmgodyak@gmail.com
 * @version 1.0
 * @copyright &copy; 2014 Otakoyi.com
 */
class UsersNotifications extends Users {

    private $items_per_page = 8;
    private $type;
    private $mNotifications;
    private $root_id = 12;

    public function __construct()
    {
        parent::__construct();

        $this->mNotifications = new \models\modules\UsersNotifications();

        $user = self::data();
        if($user){
            if($user['users_group_id'] == 3){
                $this->type = 'user';
                $this->mNotifications->setUserType('user');
            } else{
                $this->type = 'worker';
                $this->mNotifications->setUserType('worker');
            }
        }
    }

    public function index()
    {
//        var_dump(self::sendNotificationMessage(13)); die();
        $id = self::data('id');
        if(! $id) $this->redirect('/');

        $this->load->helper('string');

        $total = $this->mNotifications->getNotificationTotal($id);
        if($total == 0){
            return $this->noResults();
        }

        $new = $this->mNotifications->getNotificationTotal($id, " and is_read = 0");
        $tt  = $this->mNotifications->getNotificationTotal($id, " and is_read  = 1");

        $title = 'У вас {tn} {new} і  {tt} {msgs}';
        $title = str_replace(
                    array(
                        '{tn}', '{tt}', '{new}', '{msgs}'
                    ),
                    array(
                        $new,
                        $tt ,
                        pluralForm(
                            $new,
                            $this->translation['new'],
                            $this->translation['new1'],
                            $this->translation['new2']
                        ),
                        pluralForm(
                            $tt,
                            $this->translation['p_messages1'],
                            $this->translation['p_messages2'],
                            $this->translation['p_messages3'])
                    ),
                    $title
                );
        $this->template->assign('title', $title);


        $paginator = new Paginator($total, $this->items_per_page, $this->root_id . ';');
        $limit = $paginator->getLimit();
        $this->mNotifications->limit($limit['start'], $limit['num']);
        $this->template->assign('pagination', $paginator->getPages());
        $items = array();
        foreach ($this->mNotifications->getNotifications($id) as $item) {
//            $this->dump($item);
            $items[] = $this->item($item);
        }

        $this->template->assign('items', implode('', $items));

        // показати ще
        $start = $this->request->get('p');
        if(!$start) $start=1;
        $total = $total - $start*$this->items_per_page;
        if($total<0) $total=0;
        $this->template->assign('start', $start);
        $this->template->assign('total', $total);
        $this->template->assign('type', $this->type);
        $this->template->assign('root_id', $this->root_id);

        return $this->template->fetch('modules/users/notifications/index');
    }

    public function more()
    {
        $id = self::data('id');
        $start = isset($_POST['p'])?(int)$_POST['p']: 0;
        $start++;
        $this->request->param('p', $start);
        if(empty($id)) die();

        $total = $this->mNotifications->getNotificationTotal($id);

        $paginator = new Paginator($total, $this->items_per_page, $this->root_id . ';');
        $limit = $paginator->getLimit();
        $this->mNotifications->limit($limit['start'], $limit['num']);
        $this->template->assign('pagination', $paginator->getPages());

        $items = array();
        foreach ($this->mNotifications->getNotifications($id) as $item) {
            $items[] = $this->item($item);
        }

        // показати ще
        $total = $total - $start*$this->items_per_page;
        if($total<0) $total=0;

        return json_encode(array(
            't'          => $total,
            'items'      => $items,
            'pagination' => $this->makeFriendlyUrl($paginator->getPages()),
            'p'          => $start
        ));
    }

    /**
     * @param $item
     * @return string
     */
    private function item($item)
    {
//        $this->dump($item);
        $item['message'] = str_replace(array(
            'NEW_MESSAGE', 'CANCEL_ORDERS','NEW_ORDERS', 'DISMISS_ORDERS', 'NEW_COMMENT', 'CONFIRMED_ORDERS'
        ), array(
            $this->translation['n_new_message'],
            $this->translation['n_cancel_orders'],
            $this->translation['n_new_orders'],
            $this->translation['n_dismiss_orders'],
            $this->translation['n_new_comment'],
            $this->translation['n_confirmed_orders'],
        ), $item['message']);

        if($item['is_read'] == 0){
            $this->mNotifications->maskAsRead($item['id']);
        }

        $this->template->assign('item', $item);
        return $this->makeFriendlyUrl($this->template->fetch('modules/users/notifications/item'));
    }

    public function noResults()
    {
        return $this->template->fetch('modules/users/notifications/no_results');
    }

    public function check()
    {
        $users_id = self::data('id');
        if( ! $users_id) return '';
        UsersOnline::instance()->set($users_id);
        $t = $this->mNotifications->getNotificationTotal($users_id, ' and is_read = 0 ');
        $tm = $this->mNotifications->getNotificationTotal($users_id, " and is_read = 0 and section = 'messages' ");
        $to = $this->mNotifications->getNotificationTotal($users_id, " and is_read = 0 and section = 'orders' ");
//        $tc = $this->mNotifications->getNotificationTotal($users_id, " and is_read = 0 and section = 'comments' ");
        return json_encode(
            array(
                't' => (int)$t,
                'm' => $tm,
//                'c' => $tc,
                'o' => $to
            )
        );
    }

    public static function sendNotificationMessage($id)
    {
        include_once DOCROOT . "/vendor/phpmailer/PHPMailer.php";
        $info = \models\modules\UsersNotifications::instance()->getFullInfo($id);
        $m = new Mailer();
        $template = $m->getTemplate('notification');

        $mail = new \PHPMailer;

        $mail->setFrom($template['email'], $template['reply_to']);
        $mail->addAddress($info['email'], $info['name']);

        $mail->isHTML(true);

        $mail->Subject = $template['subject'];

        $info['message'] = str_replace(array(
            'NEW_MESSAGE', 'CANCEL_ORDERS','NEW_ORDERS', 'DISMISS_ORDERS', 'NEW_COMMENT', 'CONFIRMED_ORDERS'
        ), array(
            $info['sender_name'] .' ' . self::$t['n_new_message'],
            $info['sender_name'] .' ' . self::$t['n_cancel_orders'],
            $info['sender_name'] .' ' . self::$t['n_new_orders'],
            $info['sender_name'] .' ' . self::$t['n_dismiss_orders'],
            $info['sender_name'] .' ' . self::$t['n_new_comment'],
            $info['sender_name'] .' ' . self::$t['n_confirmed_orders'],
        ), $info['message']);
        $mail->Body = preg_replace_callback(
            '({[a-z-_]+})',
            function ($matches) use ($info) {
                $matches[0] = str_replace(array('{','}'), array(), $matches[0]);
                return isset($info[$matches[0]]) ? $info[$matches[0]] : $matches[0];
            },
            $template['body']
        );

        return $mail->send();
    }

    public static function sendStatMessage($data)
    {
        include_once DOCROOT . "/vendor/phpmailer/PHPMailer.php";

        $m = new Mailer();
        $template = $m->getTemplate('week_stat');

        $mail = new \PHPMailer;

        $mail->setFrom($template['email'], $template['reply_to']);
        $mail->addAddress($data['email'], $data['name']);

        $mail->isHTML(true);

        $mail->Subject = $template['subject'];

        $mail->Body = preg_replace_callback(
            '({[a-z-_]+})',
            function ($matches) use ($data) {
                $matches[0] = str_replace(array('{','}'), array(), $matches[0]);
                return isset($data[$matches[0]]) ? $data[$matches[0]] : $matches[0];
            },
            $template['body']
        );

        return $mail->send();
    }
}