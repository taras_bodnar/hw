<?php
/**
 * Company Otakoyi.com
 * Author: wg
 * Date: 02.01.15 16:22
 */

namespace controllers\modules;

use models\app\Guides;
use models\modules\UsersNotifications;

defined("SYSPATH") or die();

/**
 * Class Users
 * @name Users
 * @description Users module
 * Class Users
 * @package modules
 * @author wmgodyak mailto:wmgodyak@gmail.com
 * @version 1.0
 * @copyright &copy; 2014 Otakoyi.com
 */
class UsersOrders extends Users {

    private $items_per_page = 10;
    private $type;

    public function __construct()
    {
        parent::__construct();

        $this->mUsers = new \models\modules\UsersOrders();
        $this->g = new Guides();

        $id = self::data('id');
        if(! $id) $this->redirect('/');
        $user = self::data();
        if($user['users_group_id'] == 3){
            $this->type = 'user';
            $this->mUsers->setUserType('user');
        } else{
            $this->type = 'worker';
            $this->mUsers->setUserType('worker');
        }

    }

    public function index()
    {
        $id = self::data('id');
        if(! $id) $this->redirect('/');

        $total = $this->mUsers->getOrdersTotal($id);
        if($total == 0){
            return $this->noResults();
        }

        $paginator = new Paginator($total, $this->items_per_page, $id . ';');
        $limit = $paginator->getLimit();
        $this->mUsers->limit($limit['start'], $limit['num']);
        $this->template->assign('pagination', $paginator->getPages());
        $items = array();
        foreach ($this->mUsers->getOrders($id) as $item) {
//            $this->dump($item);
            $items[] = $this->item($item);
        }

        $this->template->assign('items', implode('', $items));

        // показати ще
        $start = $this->request->get('p');
        if(!$start) $start=1;
        $total = $total - $start*$this->items_per_page;
        if($total<0) $total=0;
        $this->template->assign('start', $start);
        $this->template->assign('total', $total);
        $this->template->assign('type', $this->type);

        return $this->template->fetch('modules/users/orders/index');
    }

    public function more()
    {
        $id = self::data('id');
        $start = isset($_POST['p'])?(int)$_POST['p']: 0;
        $start++;
        $this->request->param('p', $start);
        if(empty($id)) die();

        $total = $this->mUsers->getOrdersTotal($id);

        $paginator = new Paginator($total, $this->items_per_page, $id . ';');
        $limit = $paginator->getLimit();
        $this->mUsers->limit($limit['start'], $limit['num']);
        $this->template->assign('pagination', $paginator->getPages());

        $items = array();
        foreach ($this->mUsers->getOrders($id) as $item) {
            $items[] = $this->item($item);
        }

        // показати ще
        $total = $total - $start*$this->items_per_page;
        if($total<0) $total=0;

        return json_encode(array(
            't'          => $total,
            'items'      => $items,
            'pagination' => $this->makeFriendlyUrl($paginator->getPages()),
            'p'          => $start
        ));
    }

    /**
     * @param $item
     * @return string
     */
    private function item($item)
    {
        $item['termin'] = $item['termin'] == 'day' ? $this->translation['day'] : $this->translation['hour'];
        $item['city'] = $this->g->getUserCities($item['users_id']);
//        $this->dump($item);
        $notify = $item['type'] == 'user' ? 'user' : 'worker';
        UsersNotifications::instance()->setIsRead($item['id'], 'orders', $notify);
        $this->template->assign('item', $item);
        return $this->makeFriendlyUrl($this->template->fetch('modules/users/orders/item_' . $item['type']));
    }

    public function changeStatus()
    {
        $id = $this->request->post('id');
        $status = $this->request->post('action');

        $user = self::data();

        if(!$id || !$status || ! $user){
            return 'WRONG ID OR STATUS';
        }
        $users_id = null;
        $workers_id = null;
        if($user['users_group_id'] == 3){
            $users_id = $user['id'];
        }else{
            $workers_id = $user['id'];
        }
        if($this->mUsers->changeStatus($id, $status, $users_id, $workers_id )){
            return $this->item($this->mUsers->getOrdersItem($id));
        }
    }


    public function noResults()
    {
        return $this->template->fetch('modules/users/orders/no_results');
    }
}