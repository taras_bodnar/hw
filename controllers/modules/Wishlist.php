<?php
/**
 * Company Otakoyi.com
 * Author: wg
 * Date: 02.01.15 16:22
 */

namespace controllers\modules;

use controllers\Module;
use models\app\Guides;
use models\modules\Catalog;

defined("SYSPATH") or die();

/**
 * Class Wishlist
 * @name Wishlist
 * @description Wishlist module
 * Class Wishlist
 * @package modules
 * @author wmgodyak mailto:wmgodyak@gmail.com
 * @version 1.0
 * @copyright &copy; 2014 Otakoyi.com
 */
class Wishlist extends Module {

    private $model;
    private $mCatalog;
    private $mGuides;
    private $items_per_page = 10;

    public function __construct()
    {
        parent::__construct();

        $this->model = new \models\modules\Wishlist();
        $this->mCatalog = new Catalog();
        $this->mGuides  = new Guides();

        if(isset($_REQUEST['order'])){
            switch($_REQUEST['order']){
                case 22: // дешевше
                    $this->mCatalog->order(" u.price_per_day asc");
                    break;
                case 23: // дорожчі
                    $this->mCatalog->order(" u.price_per_day desc");
                    break;
            }
        }
    }

    public function index()
    {
        $users_id = Users::data('id');
        if( !$users_id){
            $this->redirect('/');
        }

        $id = $this->request->get('id');

        $this->mCatalog->join(" join users_wishlist uwl on uwl.users_id = {$users_id} and uwl.workers_id = u.id ");

        $total = $this->mCatalog->getWorkersTotal();
        $t_total = $total;
        if($total == 0){
            return $this->noResults();
        }

        $paginator = new Paginator($total, $this->items_per_page, $id . ';');
        $limit = $paginator->getLimit();
        $this->mCatalog->limit($limit['start'], $limit['num']);
        $this->template->assign('pagination', $paginator->getPages());

        $items = $this->getWorkers();

        $this->template->assign('root_id', $id);
        $this->template->assign('items', implode('', $items) );
        $this->template->assign('sorting', $this->mGuides->get(19));

        // показати ще
        $start = $this->request->get('p');
        if(!$start) $start=1;
        $total = $total - $start*$this->items_per_page;
        if($total<0) $total=0;
        $this->template->assign('start', $start);
        $this->template->assign('total', $total);
        $this->template->assign('t_total', $t_total);
        return $this->template->fetch('modules/wishlist/index');
    }

    public function noResults()
    {
        return $this->template->fetch('modules/wishlist/no_results');
    }

    public function more()
    {
        $id = (int)$_POST['id'];
        $start = isset($_POST['p'])?(int)$_POST['p']: 0;
        $start++;
        $this->request->param('p', $start);
        if(empty($id)) die();

        $total = $this->mCatalog->getWorkersTotal();

        $paginator = new Paginator($total, $this->items_per_page, $id . ';');
        $limit = $paginator->getLimit();
        $this->mCatalog->limit($limit['start'], $limit['num']);
        $this->template->assign('pagination', $paginator->getPages());

        $items = array();
        foreach ($this->getWorkers() as $item) {
            $items[] = $this->makeFriendlyUrl($item);
        }

        // показати ще
        $total = $total - $start*$this->items_per_page;
        if($total<0) $total=0;

        return json_encode(array(
            't'          => $total,
            'items'      => $items,
            'pagination' => $this->makeFriendlyUrl($paginator->getPages()),
            'p'          => $start
        ));
    }


    public function getWorkers()
    {
        $res = array();
        foreach ($this->mCatalog->getWorkers() as $item) {
            $item['img'] = array();
            foreach ($this->mCatalog->getUploads($item['id'], 4) as $k => $img) {
                $hash_id = Users::hashID($item['id']);
                if(isset($img['type']) && $img['type'] == 'video'){
                    $item['img'][$k]['big']   = "http://www.youtube.com/embed/{$img['file']}";
                    $item['img'][$k]['thumb'] = "http://img.youtube.com/vi/{$img['file']}/0.jpg";
                    $item['img'][$k]['type'] = 'video';
                } else {
                    $item['img'][$k]['big']   = '/uploads/portfolio/' . $hash_id .'/' . 'big_'. $img['file'];
                    $item['img'][$k]['thumb'] = '/uploads/portfolio/' . $hash_id .'/' . 'thumb_'. $img['file'];
                }

                // comments
                $item['comments'] = $this->mCatalog->getCommentsTotal($item['id']);
            }
            $this->template->assign('item', $item);
            $res[] = $this->template->fetch('modules/wishlist/item');
        }

        return $res;
    }

    public function add()
    {
        $users_id = Users::data('id');
        if(!$users_id || !isset($_POST['id'])) return 0;

        $workers_id= (int)$_POST['id'];

        return $this->model->create($users_id, $workers_id);
    }

    /**
     * @return int
     */
    public function rm()
    {
        $users_id = Users::data('id');
        if(!$users_id || !isset($_POST['id'])) return 0;
        $workers_id = (int)$_POST['id'];

        return $this->model->delete($users_id, $workers_id);
    }
    public function install()
    {
//        $this->addSQL("delete from settings where name='Wishlist_install' limit 1");
//        $this->addSQL("insert into settings (name,value) values ('Wishlist_install','OKI')");
//        $this->addTranslation('mod_Wishlist_hello', 'hello');
//        $this->addSetting('t1','Wishlist 1', 1 )
//             ->addSetting('t2', 'Wishlist2', 2)
//             ->addSetting('t3', 'Wishlist3', 2)
//             ->addSetting('t4', 'Wishlist4', 2)
//        ;
    }

    public function uninstall()
    {
//        $this->addSQL("delete from settings where name='Wishlist_install' limit 1");
//        $this->rmTranslations(array('mod_Wishlist_hello'));


    }
}