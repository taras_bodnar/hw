<?php
/**
 * Company Otakoyi.com
 * Author: wg
 * Date: 02.01.15 16:22
 */

namespace controllers\modules;

defined("SYSPATH") or die();

/**
 * Class Users
 * @name Users
 * @description Users module
 * Class Users
 * @package modules
 * @author wmgodyak mailto:wmgodyak@gmail.com
 * @version 1.0
 * @copyright &copy; 2014 Otakoyi.com
 */
class Calendar extends Users {

    private $type;

    public function __construct()
    {
        parent::__construct();

        $this->mUsers = new \models\modules\UsersOrders();

        $id = self::data('id');
        if(! $id) $this->redirect('/');
        $user = self::data();
        if($user['users_group_id'] == 3){
            $this->type = 'user';
            $this->mUsers->setUserType('user');
        } else{
            $this->type = 'worker';
            $this->mUsers->setUserType('worker');
        }

    }

    /**
     * @return string
     */
    public function index()
    {
        return $this->template->fetch('modules/users/calendar');
    }

    public function dayClick()
    {
        $users_id = self::data('id');

        if(!$users_id) return 0;

        $date = $_POST['date'];

        $id = $this->mUsers->getOdateID($users_id, $date);

        if(empty($id)){
            $id = $this->mUsers->setOdate($users_id, $date, $this->translation['is_disabled']);
            $s = 1;
        } else{
            $this->mUsers->removeODate($id);
            $s = -1;
        }

        return json_encode(array(
            's'  => $s,
            'id' => $id
        ));
    }

    public function oDate()
    {
        $users_id = self::data('id');

        if(!$users_id) return 0;

        $df = $this->request->post('df');
        $dt = $this->request->post('dt');
        if(! $df || ! $dt){
            return 0;
        }
        $this->load->helper('date');
        $range = createDateRangeArray($df, $dt);
//        $this->dump($range);
        foreach ($range as $k=>$date) {
            $this->mUsers->setOdate($users_id, $date, addslashes($_POST['comment']));
        }

        return 1;
    }

    public function getEvents()
    {
        $users_id = self::data('id');

        if(!$users_id) return '';

        $user = self::data();
        if($user['users_group_id'] == 3){return '';}

        $start = $_POST['start'];
        $end   = $_POST['end'];


        $this->mUsers
            ->setUserType('worker')
            ->where( " and o.odate between '$start' and '$end'")
            ->where(" and o.status <> 'canceled' ")

        ;
        $events = array();
        foreach ($this->mUsers->getOrders($users_id) as $row) {
            $desc = array();
            if(!empty($row['phone']))   $desc[] = $row['phone'];
            if(!empty($row['email']))   $desc[] = $row['email'];
            if(!empty($row['city']))    $desc[] = $row['city'];
            if(!empty($row['amount']))  $desc[] = '$'.$row['amount'];

            $events[] = array(
                'id'    => $row['id'],
                'title' => $row['username'],
                'start' => $row['eDate'],
                'allDay' => true,
                'className' => $row['status'],
                'description' => implode(' / ', $desc)
            );
        }

        foreach ($this->mUsers->getOdates($users_id, $start, $end) as $row) {
            $events[] = array(
                'title' => $row['comment'],
                'start' => $row['odate'],
                'allDay' => true,
                'className' => 'i-disable',
                'description' => ''
            );
        }


        return json_encode($events);
    }
}