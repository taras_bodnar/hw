<?php
/**
 * Company Otakoyi.com
 * Author: wg
 * Date: 02.01.15 16:22
 */

namespace controllers\modules;

use controllers\Module;

defined("SYSPATH") or die();

/**
 * Class News
 * @name News
 * @description News module
 * Class News
 * @package modules
 * @author wmgodyak mailto:wmgodyak@gmail.com
 * @version 1.0
 * @copyright &copy; 2014 Otakoyi.com
 */
class News extends Module {

    /**
     * ід сторінки, до якої прикручено новини
     * @var int
     */
    private $root_id=5;

    /**
     * Кількість статтей на сторінку
     * @var int
     */
    private $items_per_page=6;

    public function __construct()
    {
        parent::__construct();
        $this->m = new \models\modules\News();
    }

    /**
     * @return string
     */
    public function pubDate()
    {
        $page = $this->request->get('page');
        $post_date = $page['dd'] .' '. $this->formatMonth($page['mm']) .' '. $page['yy'];
        return $post_date;
    }

    /**
     * @return string
     */
    public function index()
    {
        $items = '';
        $id = $this->request->get('id');

        // мітки
        $tag = $this->request->get('tag');
        if(!empty($tag)){
            $this->m->setJoin("join tags t on t.alias = '{$tag}' ");
            $this->m->setJoin("join tags_content tc on tc.tags_id = t.id and tc.content_id=c.id");
        }


        $total = $this->m->getTotal($id);

        if($total > 0){
            $paginator = new Paginator($total, $this->items_per_page, $id . ';', 7);
            $limit = $paginator->getLimit();
            $this->m->setLimit($limit['start'], $limit['num']);
            $this->template->assign('pagination', $paginator->getPages());

            foreach ($this->m->get($id) as $item) {
                $item['mm'] = $this->formatMonth($item['mm']);
                $item['img'] = $this->images->cover($item['id'],'news');
//                $item['tags'] = $this->m->getTags($item['id']);
                $this->template->assign('item', $item);
                $items .= $this->makeFriendlyUrl($this->template->fetch('modules/news/item'));
            }
        }

        $this->template->assign('root_id', $this->root_id);
        $this->template->assign('items', $items);

        // показати ще
        $start = $this->request->get('p');
        if(!$start) $start=1;
        $total = $total - $start*$this->items_per_page;
        if($total<0) $total=0;
        $this->template->assign('start', $start);
        $this->template->assign('total', $total);

        return $this->template->fetch('modules/news/index');
    }

    public function more()
    {
        $items = '';
        $id = (int)$_POST['id'];
        $start = isset($_POST['p'])?(int)$_POST['p']: 0;
        $start++;
        $this->request->param('p', $start);
        if(empty($id)) die();

        $total = $this->m->getTotal($id);

        $paginator = new Paginator($total, $this->items_per_page, $id . ';', 7);
        $limit = $paginator->getLimit();
        $this->m->setLimit($limit['start'], $limit['num']);
        $this->template->assign('pagination', $paginator->getPages());

        foreach ($this->m->get($id) as $item) {
            $item['mm'] = $this->formatMonth($item['mm']);
            $item['img'] = $this->images->cover($item['id'],'news');
            $this->template->assign('item', $item);
            $items .= $this->makeFriendlyUrl($this->template->fetch('modules/news/item'));
        }

        // показати ще
        $total = $total - $start*$this->items_per_page;
        if($total<0) $total=0;

        return json_encode(array(
            't'          => $total,
            'items'      => $items,
            'pagination' => $this->makeFriendlyUrl($paginator->getPages()),
            'p'          => $start
        ));
    }

    public function widget()
    {
        $this->m->setLimit(0, 12);
        $items= array();
        foreach ($this->m->get($this->root_id) as $item) {
            $item['img'] = $this->images->cover($item['id'],'news');
            $item['pub_date'] = $item['dd'] .' '. $this->formatMonth($item['mm']) .' '. $item['yy'];
            $items[] = $item;
        }
        $this->template->assign('items', $items);
        return $this->template->fetch('modules/news/widget');
    }

    public function getNews()
    {
        $this->m->setJoin("left join pages_workers_group pw 
          on c.id=pw.content_id");

//        $this->m->setLimit(0, 12);
        $items = array();

        foreach ($this->m->getNews() as $item) {
            if(!empty($_SESSION['app']['user']['users_group_id']) && !empty($item['users_group_id']) && $_SESSION['app']['user']['users_group_id']!=$item['users_group_id']) {
                continue;
            }

            if (empty($_SESSION['app']['user']['users_group_id']) && !empty($item['users_group_id'])) {
                continue;
            }

            $item['img'] = $this->images->cover($item['id'],'news');
            $item['pub_date'] = $item['dd'] .' '. $this->formatMonth($item['mm']) .' '. $item['yy'];
            $items[] = $item;
        }

        $this->template->assign('items', $items);
        return $this->template->fetch('modules/news/index');
    }


    public function install()
    {
//        $this->addSQL("delete from settings where name='News_install' limit 1");
//        $this->addSQL("insert into settings (name,value) values ('News_install','OKI')");
//        $this->addTranslation('mod_News_hello', 'hello');
//        $this->addSetting('t1','News 1', 1 )
//             ->addSetting('t2', 'News2', 2)
//             ->addSetting('t3', 'News3', 2)
//             ->addSetting('t4', 'News4', 2)
//        ;
    }

    public function uninstall()
    {
//        $this->addSQL("delete from settings where name='News_install' limit 1");
//        $this->rmTranslations(array('mod_News_hello'));


    }

    private final function formatMonth($m)
    {
        $m--;
        $a = array(1,2,3);
        $a[1] = array('Січня','Лютого','Березня','Квітня','Травня','Червня','Липня','Серпня','Вересня','Жовтня','Листопада','Грудня');
        $a[2] = array('Января','Февраля','Марта','Апреля','Мая','Июня','Июля','Августа','Сентября','Октября','Ноября','Декабря');
        $a[3] = array('January','February','March','April','May','June','July','August','September','October','November','December');
        return isset($a[$this->languages_id][$m]) ? $a[$this->languages_id][$m] : 'Bad format: ' .$m;
    }
}