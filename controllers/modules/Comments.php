<?php
/**
 * Company Otakoyi.com
 * Author: wg
 * Date: 02.01.15 16:22
 */

namespace controllers\modules;

use controllers\Module;
use models\app\Mailer;
use models\modules\Users as UsersT;

defined("SYSPATH") or die();

/**
 * Class Comments
 * @name OYi.Comments
 * @description Моділь відгуків / коментарів
 * Class Comments
 * @package modules
 * @author wmgodyak mailto:wmgodyak@gmail.com
 * @version 1.0
 * @copyright &copy; 2015 Otakoyi.com
 */
class Comments extends Module {

    private $model;
    private $item_pp = 5;

    /**
     * показ аватарок
     * @var bool
     */
    private $avatars=false;


    public function __construct($workers_id=0)
    {
        parent::__construct();
        $this->model = new \models\modules\Comments($workers_id);
        $this->mMailer = new Mailer();
    }

    public function index(){}


    public function get()
    {
        $workers_id = $this->request->get('p', 'i');
        $total = $this->model->getTotal();
//        if(empty($total)) return ''; //$this->translation['no_comments'];

        $items  = '';
        foreach ($this->model->limit(0, $this->item_pp)->get() as $item) {
            $items .= $this->item($item);
        }
        $left = $total -  $this->item_pp;
        if($left < 0 ) $left = 0;
        $this->template->assign('comments_show_more', $left>0);
        $this->template->assign('comments_total', $total);
        $this->template->assign('comments_items', $items);
        $this->template->assign('workers_id', $workers_id);
        $this->template->assign('workers_id', $workers_id);
        $this->template->assign('is_worker', Users::isWorker());
        return $this->template->fetch('modules/comments/items');
    }

    public function more()
    {
        $start = $this->request->post('p', 'i');
        $this->model->workers_id = $this->request->post('user_id', 'i');
        $total = $this->model->getTotal();
        $items  = '';
        foreach ($this->model->limit($start, $this->item_pp)->get() as $item) {
            $items .= $this->item($item);
        }
        $left = $total -  $this->item_pp - $start;
        if($left < 0 ) $left = 0;
        return json_encode(array(
            't'     => $left,
            'items' => $items
        ));
    }

    private function item($item)
    {
        if($this->avatars){
            $item['icon'] = 'http://www.gravatar.com/avatar/' . md5( strtolower( trim( $item['email'] ) ) ) .'?s=80&d=mm&r=g';
        }

        $this->template->assign('item', $item);
        return $this->template->fetch('modules/comments/item');
    }

    public function show()
    {
        $this->template->assign('comments', $this->get());
    }

    public function getTotal()
    {
        return $this->model->getTotal();
    }

    /**
     * @return string
     */
    public function form()
    {
        $workers_id = $this->request->post('workers_id', 'i');
        if(!$workers_id) return 'Invalid contentID';

        $this->template->assign('workers_id', $workers_id);
        return $this->template->fetch('modules/comments/form');
    }

    /**
     * @return string
     */
    public function create()
    {
        include_once DOCROOT . "/vendor/phpmailer/PHPMailer.php";
        $s=0; $e=array();

        $users_id = Users::data('id');

        $data = $_POST['data'];
        $data['ip'] = $_SERVER['REMOTE_ADDR'];
        $data['message'] = htmlspecialchars(strip_tags($data['message']));

        if( ! $users_id){
            $e[] = $this->translation['comments_not_login'];
        } elseif(empty($data['message'])){
            $e[] = $this->translation['comments_empty_msg'];
        } else {
            $data['users_id'] = $users_id;
            $s = $this->model->create($data);
            $e[] = $this->translation['comments_success'];

            $template = $this->mMailer->getTemplate('new_comments');

            $mail = new \PHPMailer;

            $mail->setFrom('no-reply@hotwed.com.ua', $template['reply_to']);
            $mail->addAddress($template['email'], $template['reply_to']);
            $mail->addReplyTo($template['email'], $template['reply_to']);

            $mail->isHTML(true);

            $mail->Subject = $template['subject'];
            $ui = new UsersT();
            $info = $ui->getInfoById($users_id);
            $worker = $ui->getInfoById($data['workers_id']);

            $this->template->assign(array(
                'clientName' => $info['name'].' '.$info['surname'],
                'workerName' => $worker['name'].' '.$worker['surname'],
                'link' => $_SERVER['HTTP_HOST']."/vykonavets/".$worker['id']
            ));

            $mail->Body = $this->template->fetch('string:' . $template['body']);
//            $mail->Body = $this->makeFriendlyUrl($mail->Body);

            $mail->send();
        }

        return json_encode(array('s'=>$s, 'm' => implode('<br>', $e)));
    }
    public function install(){return 1;}
    public function uninstall(){return 1;}
}