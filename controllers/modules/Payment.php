<?php
/**
 * Company Otakoyi.com
 * Author: wg
 * Date: 02.01.15 16:22
 */

namespace controllers\modules;

use controllers\Module;

defined("SYSPATH") or die();

/**
 * Class Payment
 * @name OYi.Payment
 * @description Модуль планіжних систем
 * Class Payment
 * @package modules
 * @author wmgodyak mailto:wmgodyak@gmail.com
 * @version 1.0
 * @copyright &copy; 2015 Otakoyi.com
 */
abstract class Payment extends Module {
    /**
     * Payment model
     * @var mixed
     */
    protected $mPayment;
    /**
     * order info
     * @var
     */
    private $oData;

    private $log_file;

    public function __construct()
    {
        parent::__construct();

//        $this->mPayment = $this->load->model('modules\Payment');
        $this->mPayment = new \models\modules\Payment();
        $this->log_file = DOCROOT. 'logs/payment.log';
    }

    /**
     * @param $data
     * @return $this
     */
    public function setOData($data)
    {
        $this->oData = $data;
        return $this;
    }

    /**
     * @return mixed
     */
    protected function getOData()
    {
        return $this->oData;
    }

    /**
     * @param $msg
     * @return $this
     */
    protected function log($msg, $display=false)
    {
        $msg = date('Y-m-d H:i:s') .' '. $msg . "\n\n";
        file_put_contents($this->log_file, $msg, FILE_APPEND);
        if($display){
            echo $msg, '<br>';
        }
        return $this;
    }

    abstract function checkout();

    abstract function callback();

}