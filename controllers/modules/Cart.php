<?php
/**
 * Company Otakoyi.com
 * Author: wg
 * Date: 02.01.15 16:22
 */

namespace controllers\modules;

use controllers\Module;

defined("SYSPATH") or die();

/**
 * Class Cart
 * @name Cart
 * @description Cart module
 * Class Cart
 * @package modules
 * @author wmgodyak mailto:wmgodyak@gmail.com
 * @version 1.0
 * @copyright &copy; 2014 Otakoyi.com
 */
class Cart extends Module {
    private $mCart;

    public function __construct()
    {
        parent::__construct();

        if($this->request->isPost()){
            if( !isset($_POST['skey']) || $_POST['skey'] != SKEY) die('invalid token');
        }

        $this->mCart = new \models\modules\Cart();
    }
    public function index(){}

    public function block()
    {
        $total = 0; $total_price=  0;

        if(isset($_SESSION['cart'])){
            foreach ($_SESSION['cart'] as $row) {
                $total += $row['quantity'];
                $total_price +=  $row['quantity'] *  $row['price'];
            }

            $this->template->assign('items', $_SESSION['cart']);
        }
        $this->template->assign('total', $total);
        $this->template->assign('total_price', $total_price);
        return $this->makeFriendlyUrl($this->template->fetch('modules/cart/block'));
    }

    public function popup()
    {
//        unset($_SESSION['cart']);
        if(!isset($_SESSION['cart'])) return $this->translation['cart_empty_content'];

        $this->template->assign('items', $_SESSION['cart']);
        $content =  $this->template->fetch('modules/cart/popup');
        $content = $this->makeFriendlyUrl($content);

        return $content;
    }

    /**
     * add to cart
     * @return int
     */
    public function add()
    {
        if(!isset($_POST['variant_id']) || !isset($_POST['quantity'])) return 0;

        $variant_id = (int)$_POST['variant_id'];
        $quantity   = (int)$_POST['quantity'];
        if( ! isset($_SESSION['cart'][$variant_id])){
            // витягую інформацію про товар
            $info = $this->mCart->getInfo($variant_id);
            if(empty($info)) return 0;

            // записую в сесію
            $info['price'] = round($info['price'], 2);
            $_SESSION['cart'][$variant_id] = $info;
            $_SESSION['cart'][$variant_id]['img'] = $this->images->cover($info['products_id'], 'thumbnails');
        }

        $_SESSION['cart'][$variant_id]['quantity'] = $quantity;

        return 1;
    }

    /**
     * @return int
     */
    public function update()
    {
        $id = (int)$_POST['id']; $qtt = (int)$_POST['qtt'];
        if(! isset($_SESSION['cart'][$id])) return 0;

        $_SESSION['cart'][$id]['quantity']  = $qtt;
        return 1;
    }

    /**
     * @return int
     */
    public function delete()
    {
        $id = (int)$_POST['id'];
        if(! isset($_SESSION['cart'][$id])) return 0;
        unset($_SESSION['cart'][$id]);

        return 1;
    }


    public function install()
    {
//        $this->addSQL("delete from settings where name='Cart_install' limit 1");
//        $this->addSQL("insert into settings (name,value) values ('Cart_install','OKI')");
//        $this->addTranslation('mod_Cart_hello', 'hello');
//        $this->addSetting('t1','Cart 1', 1 )
//             ->addSetting('t2', 'Cart2', 2)
//             ->addSetting('t3', 'Cart3', 2)
//             ->addSetting('t4', 'Cart4', 2)
//        ;
    }

    public function uninstall()
    {
//        $this->addSQL("delete from settings where name='Cart_install' limit 1");
//        $this->rmTranslations(array('mod_Cart_hello'));


    }
}