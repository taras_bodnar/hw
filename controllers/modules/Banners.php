<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 27.09.14 12:31
 */

namespace controllers\modules;
use controllers\Module;

defined("SYSPATH") or die();
/**
 * Class Banners
 * @name OYi.Banners
 * @description Banners module
 * Class Banners
 * @package modules
 * @author wmgodyak mailto:wmgodyak@gmail.com
 * @version 1.0
 * @copyright &copy; 2014 Otakoyi.com
 */
class Banners extends Module {


    public function index(){}

    public function slider()
    {
        return $this->template->fetch('modules/banners/slider');
    }

    public function install(){}
    public function uninstall(){}
}