<?php
/**
 * Created by PhpStorm.
 * User: taras
 * Date: 17.09.15
 * Time: 10:02
 */

namespace controllers\modules;


use controllers\core\DB;
use controllers\core\Settings;
use controllers\Module;

class Migration extends Module
{

    public function __construct()
    {
        $this->migrate = new \models\modules\Migration();
    }

    public function install()
    {
        return 1;
    }

    public function uninstall()
    {
        return 1;
    }

    public function migrateCities()
    {
        $items = $this->migrate->getCities();
//        $this->dump($items);
        foreach ($items as $item) {
            $this->migrate->insertCities($item['id'], $item['city_id']);
        }
    }

    public function getEmailVk()
    {
        $items = array();
        $items = DB::instance()->select("
                    Select u.id,uo.social_id from users u
                    join users_oauth uo on u.id=uo.users_id
                    where provider='vk' limit 1
        ")->all();

        foreach ($items as $k => $item) {
            $items[$k]['email'] = $this->getEmail($item['social_id']);
        }

        $this->dump($items);
    }


    public function getEmail($id)
    {
        $email = '';
        $params = array(
            'client_id' => Settings::instance()->get('vk_app_id'),
            'scope' => 'email',
            'response_type' => 'code',
            'v' => '5.45',
            'redirect_uri' => 'http://hotved.demo24.com.ua/'
        );

        if ($curl = curl_init()) {
            curl_setopt($curl, CURLOPT_URL, 'https://oauth.vk.com/authorize?' . urldecode(http_build_query($params)));
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $out = curl_exec($curl);
            echo $out;
            curl_close($curl);

        }
        die();
        $params = array(
            'client_id' => Settings::instance()->get('vk_app_id'),
            'client_secret' => Settings::instance()->get('vk_app_secret'),
            'code' => '99aa7aea208329b40c',
            'redirect_uri' => '/',
        );

        $tokenInfo = $this->get('https://oauth.vk.com/access_token', $params);

        if (isset($tokenInfo['access_token'])) {
            $params = array(
                'uids' => $id,
                'fields' => 'uid,first_name,last_name,screen_name,sex,bdate,photo_big,email',
                'access_token' => $tokenInfo['access_token']
            );

            $userInfo = $this->get('https://api.vk.com/method/users.get', $params);
            if (isset($userInfo['response'][0]['uid'])) {
                $email = $tokenInfo['email'];

            }
        }


        return $email;
    }

    protected function get($url, $params, $parse = true)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url . '?' . urldecode(http_build_query($params)));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($curl);
        curl_close($curl);

        if ($parse) {
            $result = json_decode($result, true);
        }

        return $result;
    }

    public function relatedPosts()
    {
        $items = DB::instance()->select("
                    Select c.id,ci.name from content c
                    join content_info ci on ci.content_id = c.id and ci.languages_id=1
                    where c.type_id=8
        ")->all();

        foreach ($items as $k => $item) {
            $items[$k]['cat'] = DB::instance()->select("
                    Select c.id from content c
                   where c.id<>{$item['id']} and c.type_id=8
                   ORDER BY RAND()
                   limit 5")->all();


            foreach ($items[$k]['cat'] as $post) {
                DB::instance()->insert("posts_related",
                    array(
                        'products_id' => $item['id'],
                        'related_id' => $post['id']
                    ), 1
                );

            }

        }
    }
}