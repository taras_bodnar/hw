<?php
/**
 * Company Otakoyi.com
 * Author: wg
 * Date: 02.01.15 16:22
 */

namespace controllers\modules;

use controllers\Module;
use models\app\Content;
use models\app\Guides;
use models\modules\Users;

defined("SYSPATH") or die();

/**
 * Class Breadcrumb
 * @name Breadcrumb
 * @description Breadcrumb module
 * Class Breadcrumb
 * @package modules
 * @author wmgodyak mailto:wmgodyak@gmail.com
 * @version 1.0
 * @copyright &copy; 2014 Otakoyi.com
 */
class Breadcrumb extends Module
{

    private $model;
    private $user;
    private $guides;

    public function __construct()
    {
        parent::__construct();

        $this->model = new \models\modules\Breadcrumb();
        $this->user = new Users();
        $this->guides = new Guides();
    }

    public function index()
    {
        $id = $this->request->get('id', 'int');
        $templateID = $this->request->get('page')['templates_id'];

        $items = array();
        $current = $this->model->current($id);
        $items[] = $this->model->home();
        switch ($current['type_id']) {
            case 5:
            case 8:
                $handle = $this->request->param('handle');
                $route = explode('.', $handle);
                if (!empty($route[0])) {

                }
                $category = $this->model->category($current['id'], $current['type_id']);

                if ($category['parent_id'] > 1) {
                    $r = $this->model->parents($category['parent_id']);
                    if ($r['parent_id'] > 1) {
                        $p = $this->model->parents($r['parent_id']);
                        $items[] = $p;
                    }
                    $items[] = $r;
                } else {

                    $items[] = $this->model->parents(4);
                }
                $items[] = $category;
                break;
            default:
                $ui = array(); $workerID = 0;
                if ($current['id'] == 45) {
                    $workerID = $this->request->get('p');
                    if (!empty($workerID)) {
                        $ui = $this->user->info($workerID);
//                        $current['name'] = $current['title'] .= ' ' . $ui['name'] . ' ' . $ui['surname'];
                        // забрали слово виконавець
                        $current['name'] = $ui['name'] . ' ' . $ui['surname'];
                    }

                    $current['parent_id'] = 3;
                }

                if ($current['parent_id'] == 6) {
                    $current['parent_id'] = 0;
                    if (!empty($_SESSION['app']['user'])) {
                        //  для елементів особистого кабінету додаємо посилання на профіль
                        $items[] = [
                            'id'        => 45,
                            'url'       => $_SESSION['app']['user']['url'],
                            'name'      => $this->translation['profile'],
                            'title'     => $this->translation['profile'],
                            'type_id'   => 1
                        ];
                    }
                }
                if ($current['parent_id'] > 1) {
                    $r = $this->model->parents($current['parent_id']);
                    if ($r['parent_id'] > 1) {
                        $p = $this->model->parents($r['parent_id']);
                        $items[] = $p;
                    }
                    $items[] = $r;
                } else {
                    if ($current['type_id'] == 9) {
                        $items[] = $this->model->parents(4);
                    } elseif ($current['type_id'] == 15) {
                        $items[] = $this->model->parents(5);
                    }
                }
        }

        if ($current['id'] == 45) {
            if (!empty($workerID)) {
                $cat = 0;
                if(!empty($ui['users_group_id'])) {
                    $cat = $this->model->getWorkersGroup($ui['users_group_id']);
                    $items[] = $this->model->current($cat);
                }

                $city = $this->user->getUserCityMain($workerID);
                $c = new Content();
                $alias = '';
                $langID = self::langugesId();
                if($langID!=1) {
                    $alias = 'ru/';
                }

                $alias .= $c->getAliasById($cat,self::langugesId());

                if(!empty($city)) {
                    $items[] = [
                        'id' => $alias.".".$this->guides->getValue($city),
                        'name' => $this->guides->getNameByID($city)
                    ];
                }
            }
        }

        $items[] = $current;
        if ($templateID == 18) {
            $handle = $this->request->param('handle');
            $route = explode('.', $handle);
            if (!empty($route[1])) {
                $items[] = [
                    'id'=> 1,
                    'name'=> $this->guides->getNameByAlias($route[1])
                ];
            }
        }

        foreach ($items as $k => $item) {
            // обрізаємо всі елементи крихт до 60 символів
            $items[$k]['name'] = mb_strimwidth($item['name'], 0, 60, '...');
        }

        $this->template->assign('items', $items);
//        $this->template->assign('snippet', $this->snippet($items));
        $this->template->assign('id', $id);
        $this->template->assign('tc', count($items) - 1);
        return $this->template->fetch('modules/breadcrumb');
    }

    public function getLastBreadCrumb()
    {
        $id = $this->request->get("id", "int");
        $current = $this->model->current($id);
//        $this->dump($current);
        $r = array();
        if ($current['parent_id'] > 1) {
            switch ($current['type_id']) {
                case 5:
                case 8:
                    $r = $this->model->category($current['id'], $current['type_id']);
                    break;
                default:
                    if ($current['parent_id'] >= 1) {
                        $r = $this->model->parents($current['parent_id']);
//                        $this->dump($r);
                    }
            }
        } else {
            $r = array('id' => 1, 'name' => 'Головна');
        }
        $this->template->assign('item', $r);
        return $this->template->fetch("modules/mob_breadcrumb");
    }

    public function snippet($data)
    {
//        die($this->languages_id);
        if ($_SESSION['app']['languages']['id'] == 2) {
            $url = APPURL . 'ru/';
        } else {
            $url = APPURL;
        }
        $crumbs = '';
        $i = 1;
        foreach ($data as $item) {
            if ($item['id'] == 1) {
                $crumbs .= '
                {
                    "@type": "ListItem",
                    "position": ' . $i . ',
                    "item": {
                    "@id": "' . rtrim($url, '/') . '",
                    "name": "' . $item['name'] . '"
                    }
                }
                ';
            } else {
                $crumbs .= '
                    ,{
                        "@type": "ListItem",
                        "position": ' . $i . ',
                        "item": {
                        "@id": "' . $url . $this->model->getUrl($item['id']) . '",
                        "name": "' . $item['name'] . '"
                        }
                    }
                ';
            }
            $i++;
        }
        return '
            <script type="application/ld+json">
                {
                    "@context": "http://schema.org",
                    "@type": "BreadcrumbList",
                    "itemListElement": [
                    ' . $crumbs . '
                    ]
                }
            </script>';
    }

    public function install()
    {
    }

    public function uninstall()
    {
    }
}