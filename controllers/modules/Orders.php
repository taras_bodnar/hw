<?php
/**
 * Company Otakoyi.com
 * Author: wg
 * Date: 02.01.15 16:22
 */

namespace controllers\modules;

use controllers\core\Settings;
use controllers\Module;
//use models\modules\Delivery;
//use models\modules\Payment;
use models\modules\Pages;
use models\modules\Users as MUsers;

defined("SYSPATH") or die();

/**
 * Class Orders
 * @name Orders
 * @description Orders module
 * Class Orders
 * @package modules
 * @author wmgodyak mailto:wmgodyak@gmail.com
 * @version 1.0
 * @copyright &copy; 2014 Otakoyi.com
 */
class Orders extends Module {
    private $mOrders;
    private $mPayment;
    private $mDelivery;
    private $mUsers;
    private $notify;

    public function __construct()
    {
        parent::__construct();

        if($this->request->isPost()){
            if( !isset($_POST['skey']) || $_POST['skey'] != SKEY) die();
        }
//        include_once DOCROOT . "/vendor/phpmailer/PHPMailer.php";

        $this->mOrders   = new \models\modules\Orders();
//        $this->mPayment  = new Payment();
//        $this->mDelivery = new Delivery();
        $this->mUsers    = new MUsers();
//        $this->notify    = new Notify();
        $this->pages = new Pages();
    }

    public function index()
    {

    }

    public function calc($workers_id = null, $date = null)
    {
        if( !$workers_id && isset($_POST['worker_id'])){
            $workers_id = (int)$_POST['worker_id'];
        }

        if(! $date) {
            $date = date('d/m/Y');
        }

        if(isset($_POST['date']) && !empty($_POST['date'])){
            $date = $_POST['date'];
        }

        if(!$workers_id) return 'invalid workers_ID';

        $worker = $this->mOrders->getUserInfo($workers_id);


        $worker['odate'] = $date;
        // знижка
        $discount = $this->getDiscount($worker, $date);
        $worker['calc_price_per_day'] = $discount['per_day'];
        $worker['calc_price_per_hour'] = $discount['per_hour'];

//        $this->dump(Users::data());
        $can_order = Users::data('users_group_id') == 3;
        if(! Users::data('users_group_id') ) $can_order = true;
        $this->template->assign('can_order', $can_order);
        $this->template->assign('worker', $worker);
        return $this->template->fetch('modules/orders/calc');
    }

    private function getDiscount($worker, $date)
    {
        // обрахунок знижки
        $calc_per_day = $worker['price_per_day'];
        $calc_per_hour =  $worker['price_per_hour'];

        $a = date_create_from_format('d/m/Y', $date);

        $sTime = date_format($a, 'U');
        $now  = time();

        $datediff = $sTime - $now ;
        $days = floor($datediff/(60*60*24));

        if( $days <= 7 ){
            $calc_per_day = $calc_per_day - $calc_per_day * $worker['discount_per_week'] / 100;
            $calc_per_hour = $calc_per_hour - $calc_per_hour * $worker['discount_per_week'] / 100;
        } elseif( $days > 7 && $days <= 31 ){
            $calc_per_day = $calc_per_day - $calc_per_day * $worker['discount_per_month'] / 100;
            $calc_per_hour = $calc_per_hour - $calc_per_hour * $worker['discount_per_month'] / 100;
        } elseif( $days > 31 && $days < 62 ){
            $calc_per_day = $calc_per_day - $calc_per_day * $worker['discount_per_2_month'] / 100;
            $calc_per_hour = $calc_per_hour - $calc_per_hour * $worker['discount_per_2_month'] / 100;
        }

        return array(
            'per_day'  => $calc_per_day,
            'per_hour' => $calc_per_hour
        );
    }

    /**
     * @return string
     */
    public function process()
    {
        $users_id = Users::data('id');
        $s= 0; $e = array();$data=array();
        if(! $users_id) {
            $e[] = $this->translation['orders_e_login'];
        }

        if(empty($e)){
            $data = $_POST['data'];
            $worker   = $this->mOrders->getUserInfo($data['workers_id']);
            // переформатую дату
            $a = date_create_from_format('d/m/Y', $data['odate']);
            $oDate = date_format($a, 'Y-m-d');

            if(empty($data['workers_id'])){
                $e[] = 'WRONG_WORKERS_ID';
            } elseif(!in_array($data['termin'], array('day','hour'))){
                $e[] = 'WRONG_PERIOD';
            } elseif(empty($data['odate'])){
                $e[] = 'WRONG_DATE';
            } elseif($this->mOrders->isOrdered($worker['id'], $oDate)){
                $e[] = $this->translation['orders_e_date_ordered'];
            }
        }

        if(empty($e)){
            // запищу юзера
            $data['users_id'] = $users_id;

            // сума
            $discount = $this->getDiscount($worker, $data['odate']);
            if($data['termin'] == 'day'){
                $data['amount'] = $discount['per_day'];
            } elseif($data['termin'] == 'hour'){
                $data['amount'] = $discount['per_hour'];
            }
            $data['odate'] = $oDate;

            $orders_id = $this->mOrders->create($data);
            $s = $orders_id > 0;
        }

        return json_encode(array(
            's' => $s,
            'e' => implode('<br>', $e),
            'data' => $data,
            'r'    => $s ? $this->mkUrl(48)."?date=".$data['odate'] : ''
        ));
    }

    public function getTravel()
    {
        $date = $this->request->get("date");
        if (!isset($date) && empty($date)) return;
        $this->pages->setWhere(" and c.type_id=10 and c.isfolder=0 and parent_id<>0");

        $items = $this->pages->getTravelbyDate($date);
        foreach ( $items as $k=>$item ) {
            $items[$k]['img'] = $this->images->cover($item['id'],"travel");
            $date = explode(',',$item['travel_date']);
            $items[$k]['date']['from'] = $this->formatDate_in($date[0]);
            $items[$k]['date']['to'] = $this->formatDate_in($date[1]);
        }

        $this->template->assign("items",$items);

        return $this->template->fetch("modules/orders/travel");

    }

    private function formatDate_in($date){
        $out = explode("-", $date);
        $out['year'] = $out[0];
        $out['mounth'] = $out[1];
        if ($out[2])
        {
            $out['day'] = substr($out[2],0,2);
        }
        unset($out[0]);
        unset($out[1]);
        unset($out[2]);

        return $out['day']."/".$out['mounth'];
    }

    public function getOrderedRange()
    {
        $workers_id = $_POST['id'];
        if(!$workers_id) return '';
        $range = array();
        foreach ($this->mOrders->getOrderedRange($workers_id) as $row) {
            $range[] = $row['odate'];
        }

        return implode(',', $range);
    }

    public function hotProcess()
    {
        $mc = $this->load->model('app\Content');
        $data = $_POST['data'];
        $payMethod = '';
        if(empty($data)) return 1;
        $s = 0;
        $m = array();
        $data['users_id'] = (int)$_SESSION['app']['user']['id'];
        $data['payment_id'] = 2;

        if(!isset($data['price']) && empty($data['price'])) {
            $m[] = 'Виберіть пакет';
        } else {
            if($data['price']!=Settings::instance()->get('priceWeek') && $data['price']!=Settings::instance()->get('priceMonth')) {
                $m[] = 'Не правильна ціна';
            };

            switch($data['price']) {
                case Settings::instance()->get('priceWeek'):
                    $data['type'] = 'week';
                    break;
                case Settings::instance()->get('priceMonth') :
                    $data['type'] = 'month';
                    break;
                default:
                    $data['type'] = 'week';
                    break;
            }
        }

        if( empty($m)) {
            $oid = $this->mOrders->createPay($data);

            if($oid>0) {
                $s = 1;
                $payMethod = $this->payMethod($oid);
            }
        }

        return json_encode(array(
            's'=>$s,
//            'payMethod'=>$payMethod,
            'href'=>$alias = $mc->getAliasById(180, self::langugesId()),
            'm'=>implode('<br>', $m)
        ));
    }

    public function goldProcess()
    {
        $mc = $this->load->model('app\Content');
        $data = $_POST['data'];
        if(empty($data)) return 1;
        $s = 0;
        $data['users_id'] = (int)$_SESSION['app']['user']['id'];
        $data['payment_id'] = 2;
        $data['price'] = Settings::instance()->get('priceYear');
        $data['type'] = 'year';


        $oid = $this->mOrders->createPay($data);

        if($oid>0) {
            $s = 1;
//           $payMethod = $this->payMethod($oid);
        }

        return json_encode(array(
            's'=>$s,
//            'payMethod'=>$payMethod,
            'href'=>$alias = $mc->getAliasById(180, self::langugesId()),
        ));
    }



    public function payMethod($oid)
    {
        $payment = $this->load->model('modules\Payment');
        $methods = $payment->getMethods();

        $info = $this->mOrders->getOrderInfo($oid);

//        foreach ($methods as $i=>$m) {
            $module = 'controllers\modules\payment\\' . $info['module'];
            $m = new $module();
            // записую інформацію про замовлення
            $m->setOData($info);

            $methods['checkout'] = $m->checkout();
//        }

//        $this->template->assign('pay_methods', $methods);

        return $methods;
    }

    public function moreCities()
    {
        $data = $_POST['data'];
        $payMethod = '';
        if(empty($data)) return 1;
        $s = 0;
        $m = array();
        $data['users_id'] = (int)$_SESSION['app']['user']['id'];
        $data['payment_id'] = 2;

        if(!isset($data['price']) && empty($data['price'])) {
            $m[] = 'Виберіть пакет';
        } else {
            if($data['price']!=Settings::instance()->get('priceOneC') && $data['price']!=Settings::instance()->get('priceTwoC')) {
                $m[] = 'Не правильна ціна';
            };

            switch($data['price']) {
                case Settings::instance()->get('priceOneC'):
                    $data['type'] = 'oneC';
                    $data['city_index'] = 1;
                    break;
                case Settings::instance()->get('priceTwoC') :
                    $data['type'] = 'twoC';
                    $data['city_index'] = 2;
                    break;
                default:
                    $data['type'] = 'oneC';
                    $data['city_index'] = 2;
                    break;
            }
        }

        if( empty($m)) {
            $oid = $this->mOrders->createPay($data);

            if($oid>0) {
                $s = 1;
                $payMethod = $this->payMethod($oid);
            }
        }

        return json_encode(array(
            's'=>$s,
            'payMethod'=>$payMethod,
            'm'=>implode('<br>', $m)
        ));
    }


    public function install()
    {
//        $this->addSQL("delete from settings where name='Orders_install' limit 1");
//        $this->addSQL("insert into settings (name,value) values ('Orders_install','OKI')");
//        $this->addTranslation('mod_Orders_hello', 'hello');
//        $this->addSetting('t1','Orders 1', 1 )
//             ->addSetting('t2', 'Orders2', 2)
//             ->addSetting('t3', 'Orders3', 2)
//             ->addSetting('t4', 'Orders4', 2)
//        ;
    }

    public function uninstall()
    {
//        $this->addSQL("delete from settings where name='Orders_install' limit 1");
//        $this->rmTranslations(array('mod_Orders_hello'));


    }
}