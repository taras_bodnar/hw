<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 27.09.14 12:31
 */

namespace controllers\modules;
use controllers\Module;

defined("SYSPATH") or die();

/**
 * Class Nav
 * @name OYi.Nav
 * @description Nav module
 * Class Nav
 * @package modules
 * @author wmgodyak mailto:wmgodyak@gmail.com
 * @version 1.0
 * @copyright &copy; 2014 Otakoyi.com
 */
class Nav extends Module {

    private $mNav;

    public function __construct()
    {
        parent::__construct();

        $this->mNav = new \models\app\Nav();
    }

    public function index(){}

    /**
     * site languages
     * @return string
     */
    public function languages()
    {
        $this->template->assign('items',   $this->mNav->languages());
        $this->template->assign('id', $this->request->param('languages_id'));
        return $this->template->fetch('modules/nav/languages');
    }

    /**
     * @return string
     */
    public function top()
    {
        $this->template
            ->assign('items',$this->menu(2))
            ->assign('id', $this->request->get('id', 'int'))
        ;
        return $this->template->fetch('modules/nav/top');
    }

    /**
     * top menu
     * @return string
     */
    public function main()
    {
        $this->template->assign('languages',   $this->mNav->languages());
        $this->template->assign('languages_id', $this->request->param('languages_id'));

        $this->template
            ->assign('items',$this->menu(2,1))
            ->assign('id', $this->request->get('id', 'int'))
            ->assign('p', $this->request->param('p'))
        ;
//        $this->dump($this->request->get('p', 'int'));die;
        return $this->template->fetch('modules/nav/main');
    }

    public function user()
    {
        $u = Users::data('users_group_id');
        if($u == 4) return '';
        $menu_id = $u == 3 ? 4 : 5;
        $this->template
            ->assign('items',$this->menu($menu_id))
            ->assign('id', $this->request->get('id', 'int'))
        ;
        return $this->template->fetch('modules/nav/user');
    }

    /**
     * bottom nav
     * @return string
     */
    public function bottom()
    {
        $this->template->assign('items', $this->menu(3));
        $this->template->assign('id', $this->request->get('id', 'int'));
        return $this->template->fetch('modules/nav/bottom');
    }

    /**
     * catalog menu
     * @return string
     */
    public function catalog()
    {
        $items = $this->menu(1, 15);
//        $this->dump($items); die();
        /*
        якщо треба зображення для категорій
        $r = $this->mNavenu(2, 1);
        $im = $this->load->model('app\Images');
        foreach ($r as $row) {

            if(!empty($row['children'])){
                $i=0;
                foreach ($row['children'] as $c) {
                    $row['children'][$i]['img'] = $im->cover($c['id'], 'cat_thumb');
                    $i++;
                }
            }

            $items[] = $row;
        }
        */
        $this->template->assign('items', $items);

        return $this->template->fetch('modules/nav/catalog');
    }

/** **************************** приватні методи для роботи з навігацією ***************************************/
    /**
     * @param $menu_id
     * @param int $level
     * @return array
     */
    private function menu($menu_id, $level=0)
    {
        $res = array();

        $r = $this->mNav->menuItems($menu_id);

        foreach ($r as $row) {
            if($level > 0 ){
                $c = $this->getChildren($row['id'], $level);
                if(!empty($c)){
                    $row['children'] = $c;
                }
            }

            $res[] = $row;
        }

        return $res;
    }

    /**
     * @param int $parent_id
     * @param int $level of recursion
     * @return array
     */
    private function getChildren($parent_id, $level)
    {
        if($level <= 0) return array();
        $items = array();
        $r = $this->mNav->getChildren($parent_id);
        foreach ($r as $row) {
            if($row['isfolder']){
                $c = $this->getChildren($row['id'], --$level);
                if(!empty($c)){
                    $row['children'] = $c;
                }
            }
            $items[] = $row;
        }
        return $items;
    }

    public function install(){}
    public function uninstall(){}
}