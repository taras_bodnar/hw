<?php
/**
 * Company Otakoyi.com
 * Author: wg
 * Date: 02.01.15 16:22
 */

namespace controllers\modules;

use controllers\Module;
use models\app\Features;
use models\app\Images;

defined("SYSPATH") or die();

/**
 * Class Pages
 * @name Pages
 * @description Pages module
 * Class Pages
 * @package modules
 * @author wmgodyak mailto:wmgodyak@gmail.com
 * @version 1.0
 * @copyright &copy; 2014 Otakoyi.com
 */
class Pages extends Module {

    private $mi;
    private $pages;
    private $features;

    public function __construct()
    {
        parent::__construct();
        $this->pages = new \models\modules\Pages();
        $this->mi = new Images();
        $this->features = new Features();
    }

    /**
     * @return string
     */
    public function index()
    {

    }

    public function design()
    {
        $items = array();
        $r = $this->pages->getItems(3);
        $i= 0.5;
        foreach ($r as $row) {
            $v = $this->features->getContentValues($row['id'], 4, 'number');
            if(!empty($v['value'])){
                $row['icon'] = $v['value'];
            }
            $row['duration'] = $i;
            $i +=  0.2;
            $items[] = $row;
        }

        $this->template->assign('title', $this->pages->getInfo(3, 'name'));
        $this->template->assign('items', $items);
        return $this->template->fetch('modules/pages/main/design');
    }

    public function development()
    {
        $items = array();
        $r = $this->pages->getItems(4);
        $i= 0.5;
        foreach ($r as $row) {
            $v = $this->features->getContentValues($row['id'], 4, 'number');
            if(!empty($v['value'])){
                $row['icon'] = $v['value'];
            }
            $row['duration'] = $i;
            $i +=  0.2;
            $items[] = $row;
        }

        $this->template->assign('title', $this->pages->getInfo(4, 'name'));
        $this->template->assign('items', $items);
        return $this->template->fetch('modules/pages/main/development');
    }

    public function getTravelCountry($id,$size)
    {
//        $id = $this->request->get("id");
        if($size=="travel_country") {
            $this->pages->setWhere(" and c.type_id=10");
            $items = $this->pages->getItems(0);
        } else{
            $items = $this->pages->getItems($id);
        }

        foreach ( $items as $k => $item ) {
            $items[$k]['img'] = $this->images->cover($item['id'],$size);
            $items[$k]['count'] = $this->pages->getTotal($item['id']);
            $items[$k]['plural'] = $this->pluralForm(
                                            $items[$k]['count'],
                                            $this->translation['tour'],
                                            $this->translation['tours2-4'],
                                            $this->translation['tours5']
                );

            if($size == "travel") {
                $date = explode(',',$item['travel_date']);
                $items[$k]['date']['from'] = $this->formatDate_in($date[0]);
                $items[$k]['date']['to'] = $this->formatDate_in($date[1]);
            }
        }

        $this->template->assign("items",$items);

        return $this->template->fetch("modules/pages/$size");

    }

    private function formatDate_in($date){
        $out = explode("-", $date);
        $out['year'] = $out[0];
        $out['mounth'] = $out[1];
        if ($out[2])
        {
            $out['day'] = substr($out[2],0,2);
        }
        unset($out[0]);
        unset($out[1]);
        unset($out[2]);

        return $out['day']."/".$out['mounth'];
    }

    private function pluralForm($n, $form1, $form2, $form5)
    {
        $n = abs($n) % 100;
        $n1 = $n % 10;
        if ($n > 10 && $n < 20) return $form5;
        if ($n1 > 1 && $n1 < 5) return $form2;
        if ($n1 == 1) return $form1;
        return $form5;
    }

    public function install(){}
    public function uninstall(){}
}