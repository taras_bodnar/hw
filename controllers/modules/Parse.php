<?php
/**
 * Company Otakoyi.com
 * Author: wg
 * Date: 02.01.15 16:22
 */

namespace controllers\modules;

use controllers\App;
use controllers\Module;
use models\app\Content;
use models\app\Languages;

defined("SYSPATH") or die();

/**
 * Class Catalog
 * @name Catalog
 * @description Catalog module
 * Class Catalog
 * @package modules
 * @author wmgodyak mailto:wmgodyak@gmail.com
 * @version 1.0
 * @copyright &copy; 2014 Otakoyi.com
 */
class Parse extends Module
{
    private $mCatalog;

    public function __construct()
    {
        parent::__construct();

        $this->mCatalog = new \models\modules\Catalog();
    }

    public function index()
    {
        $url = $this->request->param('handle');
        $lang = $this->request->param('lang');
        if (empty($url)) $this->redirect404();

        $id = $this->mCatalog->getIdByUrl($url);

        if (empty($id)) $this->redirect404();

        $this->request->set('p', $id);

        $l = new Languages();
        $lang_id = $l->getIdByCode($lang);
        if (empty($lang_id)) {
            $lang_id = $l->getDefault('id');
        }

        $c = new Content();
        $alias = $c->getAliasById(45, $lang_id);
        $this->request->set('alias', $alias);
        $app = new App();
        $app->index();
    }


    public function install()
    {

    }

    public function uninstall()
    {

    }
}