<?php
/**
 * Company Otakoyi.com
 * Author: wg
 * Date: 02.01.15 16:22
 */

namespace controllers\modules;

use models\app\Guides;

defined("SYSPATH") or die();

/**
 * Class OrdersHistory
 * @name OrdersHistory
 * @description Users module
 * Class Users
 * @package modules
 * @author wmgodyak mailto:wmgodyak@gmail.com
 * @version 1.0
 * @copyright &copy; 2014 Otakoyi.com
 */
class OrdersHistory extends Users {

    private $items_per_page = 5;
    private $type;
    private $root_id = 49 ;

    public function __construct()
    {
        parent::__construct();

        $this->mUsers = new \models\modules\UsersOrders();

        $id = self::data('id');
        if(! $id) $this->redirect('/');
        $user = self::data();
        if($user['users_group_id'] != 3){
            $this->logout();
            $this->redirect('/');
        }
        $this->type = 'user';
        $this->mUsers->setUserType('user');
        $this->g = new Guides();
    }

    public function index()
    {
        $id = self::data('id');
        if(! $id) $this->redirect('/');
        $now = date('Y-m-d');

        $this->mUsers->limit(0, 30);
        $items2 = array();
        foreach ($this->mUsers->where(" and o.odate >= '{$now}' and o.status = 'confirmed' ")->getOrders($id) as $item) {
            $items2[] = $this->item($item);
        }

        $this->template->assign('items_new', implode('', $items2));


        // минулі бронювання

        $total = $this->mUsers->clearWhere()->where(" and o.odate < '{$now}' and o.status = 'confirmed' ")->getOrdersTotal($id);

        if(empty($items2) && $total == 0){
            return $this->noResults();
        }

        $paginator = new Paginator($total, $this->items_per_page, $this->root_id . ';');
        $limit = $paginator->getLimit();

        $this->mUsers->limit($limit['start'], $limit['num']);

        $this->template->assign('pagination', $paginator->getPages());

        $items = array();
        foreach ($this->mUsers->getOrders($id) as $item) {
//            $this->dump($item);
            $items[] = $this->item($item);
        }

        $this->template->assign('items', implode('', $items));

        // показати ще
        $start = $this->request->get('p');
        if(!$start) $start=1;
        $total = $total - $start*$this->items_per_page;
        if($total < 0) $total=0;
        $this->template->assign('start', $start);
        $this->template->assign('total', $total);
        $this->template->assign('type', $this->type);
        $this->template->assign('root_id', $id);


        return $this->template->fetch('modules/users/orders_history/index');
    }

    public function more()
    {
        $id = self::data('id');
        $start = isset($_POST['p'])?(int)$_POST['p']: 0;
        $start++;
        $this->request->param('p', $start);
        if(empty($id)) die();
        $now = date('Y-m-d');

        $total = $this->mUsers->where(" and o.odate < '{$now}' and o.status = 'confirmed' ")->getOrdersTotal($id);

        $paginator = new Paginator($total, $this->items_per_page, $this->root_id . ';');
        $limit = $paginator->getLimit();
        $this->mUsers->limit($limit['start'], $limit['num']);
        $this->template->assign('pagination', $paginator->getPages());

        $items = array();
        foreach ($this->mUsers->getOrders($id) as $item) {
            $items[] = $this->item($item);
        }

        // показати ще
        $total = $total - $start*$this->items_per_page;
        if($total<0) $total=0;

        return json_encode(array(
            't'          => $total,
            'items'      => $items,
            'pagination' => $this->makeFriendlyUrl($paginator->getPages()),
            'p'          => $start
        ));
    }

    /**
     * @param $item
     * @return string
     */
    private function item($item)
    {
        $item['termin'] = $item['termin'] == 'day' ? $this->translation['day'] : $this->translation['hour'];
        $item['city'] = $this->g->getUserCities($item['users_id']);
        $this->template->assign('item', $item);
        return $this->makeFriendlyUrl($this->template->fetch('modules/users/orders_history/item'));
    }

    public function noResults()
    {
        return $this->template->fetch('modules/users/orders_history/no_results');
    }
}