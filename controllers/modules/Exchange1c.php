<?php
/**
 * Created by PhpStorm.
 * User: wg
 * Date: 30.06.15
 * Time: 9:11
 */

namespace controllers\modules;


use controllers\core\Config;
use controllers\core\DB;
use controllers\core\Settings;
use controllers\Module;
use models\app\Languages;

/**
 * Class Exchange1c
 * @description Синхронізація каталогу і замовлень з 1C
 *
 * @name Exchange1c
 * @description Exchange1c module
 * @copyright &copy; 2014 Otakoyi.com
 * @package controllers\modules
 * @author wmgodyak mailto:wmgodyak@gmail.com
 * @link http://v8.1c.ru/edi/edi_stnd/131/
 * 0. alter table setting change value to text
 * 1. create user on mkUser();
 * ...
 *
    ALTER TABLE `settings` CHANGE `value` `value` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ;
    ALTER TABLE `content` ADD `external_id` VARCHAR( 60 ) NULL ,
    ADD INDEX ( `external_id` ) ;

    ALTER TABLE `products_options` ADD `external_id` VARCHAR( 60 ) NULL ,
    ADD INDEX ( `external_id` ) ;
 *
 * create user cron@budyak.net and
 *
 * виключити debug
 * login: 1c_user
 * pass: c595e2ed1dbd469bb1e97c177f182438
 * http://budyak.net/route/Exchange1c/index
 */
class Exchange1c extends Module {
    private $type;
    private $mode;
    private $db;
    private $settings;
    private $debug = false;

    private $login;
    private $password;

    private $time;

    private $config;

    private $tmp_dir;
    private $backup_dir;

    private $allowed_image_extensions = array('jpg', 'jpeg', 'png', 'gif');

    private $data=array();

    private $cron_id = 7; // id of cron user
    private $cat_id = 4521;

    private $externalIds = array(); // external_id => id
    private $featuresExternalIds = array(); // external_id => id

    private $errors = array();

    public function __construct()
    {
        parent::__construct();

        // імпортувати категорії
        $this->config['import_categories'] = 0;

        $this->tmp_dir    = DOCROOT . 'tmp/1c_exchange/';
        $this->backup_dir = DOCROOT . 'tmp/db_backup/';

        $this->time = microtime(true);
        $this->db = DB::instance();

        // список мов
        $l = new Languages();

        $this->languages = $l->all(1);

        $this->type = isset($_GET['type']) ? $_GET['type'] : null;
        $this->mode = isset($_GET['mode']) ? $_GET['mode'] : null;

        $this->settings = Settings::instance();
        $this->login    = isset($_SERVER['PHP_AUTH_USER']) ? trim($_SERVER['PHP_AUTH_USER']) : NULL;
        $this->password = isset($_SERVER['PHP_AUTH_PW']) ? trim($_SERVER['PHP_AUTH_PW']) : NULL;

        // інформація про користуача
        $s = $this->settings->get('1c_exchange');
        if(! $s){
            $this->mkUser();
        }
        $user = unserialize($s);
        $this->config['login']     = $user['login'];
        $this->config['password']  = $user['pass'];

        // перевірка підтримки zip
        if (class_exists('ZipArchive')) {
            $this->config['zip'] = 'yes';
        } else {
            $this->config['zip'] = 'no';
        }

        // post max file size
        $this->config['file_limit'] = $this->return_bytes(ini_get('post_max_size'));

        if(!is_dir($this->tmp_dir)){
            mkdir($this->tmp_dir, 0777, true);
        }

        if(!is_dir($this->backup_dir)){
            mkdir($this->backup_dir, 0777, true);
        }


        if($this->debug) {
            // примусово авторизую
            $this->login    = $this->config['login'];
            $this->password = $this->config['password'];

            echo '<pre>';

//            $this->dump($this->config);
        }
    }

    private function return_bytes ($size_str)
    {
        switch (substr ($size_str, -1))
        {
            case 'M': case 'm': return (int)$size_str * 1048576;
            case 'K': case 'k': return (int)$size_str * 1024;
            case 'G': case 'g': return (int)$size_str * 1073741824;
            default: return $size_str;
        }
    }

    public function index()
    {
        $this->log('******* Begin exchange *******');
//        $this->log('SERVER INFO:' . var_export($_SERVER,1));
        $method = '';

        //preparing method and mode name from $_GET variables
        if (isset($_GET['type']) && isset($_GET['mode'])) {
            $method .= strtolower($_GET['type']) . '_' . strtolower($_GET['mode']);
        }

        //run method if exist
        if (method_exists($this, $method)) {
            $this->log("Run method: {$method}");
            $this->$method();
        }
    }

    /**
     * create user to exchange
     */
    public function mkUser()
    {
        $key = '1c_user';
        $pass = md5(str_shuffle(chr(mt_rand(32, 126)) . uniqid() . microtime(TRUE)));
        $this->settings->set('1c_exchange', serialize(array('login' => $key, 'pass' => $pass)));
    }

    /**
     * A. Начало сеанса
     */
    private function catalog_checkauth()
    {
        // примусово авторизую
        $key  = session_name();
        $pass = session_id();
        $this->settings->set('1c_token', md5($pass));
        $this->log("Created token: {$pass}");
        $this->response(array( 'success', $key, $pass ));


        if (($this->config['login'] == $this->login)/*  && ($this->config['password'] == $this->password) */) {
            $key  = session_name();
            $pass = session_id();
            $this->settings->set('1c_token', md5($pass));
            $this->log("Created token: {$pass}");
            $this->response(array( 'success', $key, $pass ));
        } else{
            $this->log('Bad login or password');
            $this->response(array( 'failure', "Bad login or password. login: $this->login password: $this->password" ));
        }
    }

    /**
     * B. Запрос параметров от сайта
     */
    private function catalog_init()
    {
        if(! $this->auth()) $this->response(array( 'failure', "Wrong token" ));

        $this->response(array("zip={$this->config['zip']}", "file_limit={$this->config['file_limit']}"));
    }

    /**
     * C. Выгрузка на сайт файлов обмена
     */
    private function catalog_file()
    {
        if(! $this->auth()) $this->response(array( 'failure', "Wrong token" ));

        $file_info = pathinfo($this->request->get('filename', 's'));

        if(empty($file_info['basename'])){
            $this->response(array( 'failure', "empty filename" ));
        }

//        $this->dump($file_info); die();

        $file_extension = $file_info['extension'];
        $path = $file_info['dirname'];
        $basename = $file_info['basename'];

        $this->log('Loading filename:' . $basename);
        $status = '';
        $file_content = file_get_contents('php://input');
        if(empty($file_content)){
            $this->log('failure php://input  return empty string');
            $this->response(array('failure', 'php://input  return empty string'));
        }

        if ($file_extension != 'zip' && $file_extension != 'xml' && in_array($file_extension, $this->allowed_image_extensions)) {
            //saving images to tmp/1c_exchange/images folder
            @mkdir($this->tmp_dir . $path, 0777, TRUE);
            if ($this->write_file($this->tmp_dir . $this->request->get('filename', 's'), $file_content, 'w+')) {
                $status  ='success';
            }

        } else {
            //saving xml files to tmp/1c_exchange/
            if ($this->write_file($this->tmp_dir . $this->request->get('filename', 's'), $file_content, 'w+')) {
                $status = 'success';
            }
        }

        // extract archive
        if ($file_extension == 'zip' && class_exists('ZipArchive')) {
            $this->log("Extract archive $basename");

            $zip = new \ZipArchive();
            $res = $zip->open($this->tmp_dir . $this->request->get('filename', 's'));
            if ($res > 0 && $res != TRUE) {
                switch ($res) {
                    case \ZipArchive::ER_NOZIP :
                        $this->log('Not a zip archive.');
                        break;
                    case \ZipArchive::ER_INCONS :
                        $this->log('Zip archive inconsistent.');
                        break;
                    case \ZipArchive::ER_CRC :
                        $this->log('checksum failed');
                        break;
                    case \ZipArchive::ER_EXISTS :
                        $this->log('File already exists.');
                        break;
                    case \ZipArchive::ER_INVAL :
                        $this->log('Invalid argument.');
                        break;
                    case \ZipArchive::ER_MEMORY :
                        $this->log('Malloc failure.');
                        break;
                    case \ZipArchive::ER_NOENT :
                        $this->log('No such file.');
                        break;
                    case \ZipArchive::ER_OPEN :
                        $this->log("Can't open file.");
                        break;
                    case \ZipArchive::ER_READ :
                        $this->log("Read error.");
                        break;
                    case \ZipArchive::ER_SEEK :
                        $this->log("Seek error.");
                        break;
                }
                $status = 'failure';

            } else {
                $zip->extractTo($this->tmp_dir);
            }
            $zip->close();
        }

        $this->response(array($status));
    }

    /**
     * D. Пошаговая загрузка каталога
     */
    private function catalog_import()
    {
        if(! $this->auth()) $this->response(array( 'failure', "Wrong token" ));
        $this->createDBBackup();
        $filename = $this->request->get('filename', 's');
        if(!file_exists($this->tmp_dir . $filename)){
            $this->log("$filename not exist");
            $this->response(array('failure', "$filename not exist"));
        }

        $this->data = simplexml_load_file($this->tmp_dir . $filename);
        $json = json_encode($this->data);
        $this->data = json_decode($json, TRUE);

        $this->parseData();

        $time_end = microtime(true);
        $exec_time = round($time_end-$this->time, 4);
        $this->log("******* End exchange ******* Time: $exec_time");

        if(empty($this->errors)){
            $this->response(array('success'));
        } else{
            $this->response(array('failure', implode("\n", $this->errors)));
        }
    }

    private function parseData()
    {
        if($this->config['import_categories'] && isset($this->data['Классификатор']['Группы'])){
            $this->log('Import categories...');
            $this->eachCategories($this->data['Классификатор']['Группы'], 4521);
        }else{
            $this->log('Import categories is disabled on config');
        }

        if(isset($this->data['Классификатор']['Свойства'])){
            $this->eachFeaturesNames($this->data['Классификатор']['Свойства']);
        }

        if(isset($this->data['Каталог']['Товары'])){
//            $this->dump($this->data['Каталог']);die();
            if(
                isset($this->data['Каталог']['@attributes']['СодержитТолькоИзменения'])
                && $this->data['Каталог']['@attributes']['СодержитТолькоИзменения'] == false
            ){
                $this->hideProducts();
            }
            // запишу в кеш features -> id
            $this->getFeaturesIds();

            $this->eachProducts($this->data['Каталог']['Товары']);
        }

        if(isset($this->data['ПакетПредложений'])){
            $this->eachOffers($this->data['ПакетПредложений']);
        }
    }

    private function getFeaturesIds()
    {
        $this->log('Load Features IDS to cache');
        foreach($this->db->select("select id,external_id from features where external_id <> ''")->all() as $row){
            $this->featuresExternalIds[$row['external_id']] = $row['id'];
        }
//        $this->dump($this->featuresExternalIds);die();
    }

    private function eachFeaturesNames($data)
    {
        $this->log('EachFeaturesNames begin');
        foreach ($data['Свойство'] as $row) {
            $id=$this->db->select("select id from features where external_id='{$row['Ид']}' limit 1")->row('id');
            if(empty($id)){
                $s=0;
                $this->db->beginTransaction();
                $features_id = $this->db->insert(
                                    "features",
                                    array(
                                        'type'        => 'text',
                                        'published'   => 1,
                                        'auto'        => 0,
                                        'owner_id'    => $this->cron_id,
                                        'extends'     => 5, // товар
                                        'external_id' => trim($row['Ид'])
                                    )
                                );
                if($features_id>0){
                    foreach ($this->languages as $lang) {
                        $s+= $this->db->insert('features_info', array(
                            'features_id'  => $features_id,
                            'languages_id' => $lang['id'],
                            'name'         => $row['Наименование']
                        ));
                    }
                }

                if($features_id>0 && $s>0){
                    $this->db->commit();
                } else{
                    $this->db->rollBack();
                }
            }
        }
        $this->log('EachFeaturesNames end');
    }

    private function eachOffers($data)
    {
        $this->log('Import offers begin');

        if(!isset($data['ТипыЦен'])){
            $this->log('Не вказано тип цін');
            return;
        }
//        $this->dump($data);return;

        $price_code_def = '2a586db8-0736-11e3-87d0-9cb70dedbc3c';
        $price_code_act  = '97f18301-4b0b-11e4-88e5-d4ae52c95c53';

        foreach ($data['ТипыЦен']['ТипЦены'] as $row) {
            if($row['Ид'] != $price_code_def && $row['Ид'] != $price_code_act){
                $this->log('Невідповідний тип ціни ' . $row['Ид'] );
                $this->errors[] = "Невідповідний тип ціни {$row['Ид']}";
                return;
            }
        }

        foreach ($data['Предложения']['Предложение'] as $i=>$row) {
//            if($i == 10) return;
            if(!isset($row['Артикул'])){
                $this->log("Відсутній артикул для {$row['Ид']} {$row['Наименование']}");
                $this->errors[] = "Відсутній артикул для {$row['Ид']} {$row['Наименование']}. Пропускаю";
                continue;
            }
            $code         = trim($row['Артикул']);
            $price        = 0;
            $price_act    = 0;

            $iv = array(
                'quantity' => $row['Количество'],
                'availability' => $row['Количество'] > 0 ? 1 : 0
            );

            if(isset($row['Цены']['Цена'])){
                if(isset($row['Цены']['Цена']['ИдТипаЦены'])){
                    $price = $row['Цены']['Цена']['ЦенаЗаЕдиницу'];
                } else {
                    foreach ($row['Цены']['Цена'] as $item) {
                        switch($item['ИдТипаЦены']){
                            case '2a586db8-0736-11e3-87d0-9cb70dedbc3c':
                                $price     = $item['ЦенаЗаЕдиницу'];
                                break;
                            case '97f18301-4b0b-11e4-88e5-d4ae52c95c53':
                                $price_act = $item['ЦенаЗаЕдиницу'];
                                break;
                            default:
                                $this->log('Невідповідний тип ціни ' . $row['Ид'] );
                                $this->errors[] = "Невідповідний тип ціни {$row['Ид']}";
                                return;
                                break;
                        }
                    }
                }
            }

            $iv['price']     = $price;
            $iv['price_old'] = $price_act;
            $iv['sale']      = 0;
            if($price_act > 0){
                $iv['price']     = $price_act;
                $iv['price_old'] = $price;
                $iv['sale']      = 1;
            }

            $this->db->update('products_options', $iv, " code = '{$code}' limit 1");
        }

        $this->log('Import offers  end');
    }

    /**
     * @return bool
     */
    private function hideProducts()
    {
        $this->log('Hide all products');
        return $this->db->update('content', array('published'=> 0), " type_id =5 ");
    }

    /**
     * @param $products
     */

    private function eachProducts($products)
    {
        $this->log('Import products ...');

        foreach ($products['Товар'] as $i=> $item) {
            if(!isset($item['Артикул'])) continue;
//            if($i == 10) break;
//            $this->dump($item);
            $external_id = trim($item['Ид']);
            $name        = $item['Наименование'];
            $code        = trim($item['Артикул']);
            $categories  = $this->readGroups($item);

            $id = $this->getProductsIDByCode($code);
            if(empty($id)){
                // створення
                $names = $this->makeProductName($item);
                if(!isset($names['ru']) || !isset($names['uk'])){
                    $names['ru'] = $name;
                    $names['uk'] = $name;
//                    $this->log("Empty product name from code $code");
//                    $this->errors[] = "Empty product name from code $code";
//                    continue;
                }
                $id = $this->createProduct($external_id, $code, $names);
                if($id == 0) continue;
            } else{
                $this->updateProduct($id);
            }

            // записую властивості
            if($id > 0 && isset($item['ЗначенияСвойств']['ЗначенияСвойства'])){
                $this->eachProductsFeaturesValues($id, $item['ЗначенияСвойств']['ЗначенияСвойства']);
            }

            // привязка товарів до категорії
            $this->setProductsCategories($id, $categories);
        }

        $this->log('Import products done');
    }

    /**
     * @param $content_id
     * @param $data
     */
    private function eachProductsFeaturesValues($content_id, $data)
    {
//        $this->log('EachProductsFeaturesValues');
        foreach ($data as $item) {
//            echo $item['Ид'], ' ', $item['Значение'], '<br>'; continue;
            if(!isset($item['Ид']) || !isset($this->featuresExternalIds[$item['Ид']])){
//                $this->log("Відсутній параметр Ид");
//                $this->errors[] = "Відсутній параметр Ид";
                continue;
            }

            if(!isset($item['Значение'])){
                $this->log("Відсутній параметр Значение для ЗначенияСвойства {$item['Ид']}");
                $this->errors[] = "Відсутній параметр Значение для ЗначенияСвойства {$item['Ид']}";
                continue;
            }

            foreach ($this->languages as $lang) {
                $aid=$this->db->select("
                     select id
                     from features_content_values
                     where features_id = '{$this->featuresExternalIds[$item['Ид']]}'
                           and content_id = '{$content_id}'
                           and languages_id = '{$lang['id']}'
                           limit 1
                     ")->row('id');
                   if($aid>0){
                       $this->db->update('features_content_values', array('value' => $item['Значение']), " id = {$aid} limit 1");
                   } else {
                       $this->db->insert('features_content_values', array(
                           'features_id'  => $this->featuresExternalIds[$item['Ид']],
                           'content_id'   => $content_id,
                           'languages_id' => $lang['id'],
                           'value'        => $item['Значение']
                       ));
                   }

            }
        }
//        die();
    }

    /**
     * @param $id
     * @return bool
     */
    private function updateProduct($id)
    {
        // покищо публікація
        return $this->db->update('content', array('published' => 1), " id={$id} limit 1");
    }

    /**
     * @param $products_id
     * @param array $categories
     */
    private function setProductsCategories($products_id, array $categories)
    {
        if(empty($categories)) return ;

        foreach ($categories as $k=>$cid) {
            $this->db->insert('products_categories', array('pid' => $products_id, 'cid' => $cid), 0, 1);
        }
    }

    /**
     * @param $external_id
     * @param $code
     * @param $names
     * @return bool|string
     */
    private function createProduct($external_id, $code, $names)
    {

//        $this->log("Створення товару    $external_id    {$names['ru']}    $code");
        $this->db->beginTransaction(); $s=0; $o=0;
        $content_id = $this->db->insert(
            'content',
            array(
                'external_id'  => $external_id,
                'type_id'      => 5,
                'templates_id' => 5,
                'owner_id'     => $this->cron_id,
                'createdon'    => date('Y-m-d H:i:s '),
                'auto'         => 0
            )
        );
        if($content_id > 0){
            $o = $this->db->insert(
                'products_options',
                array(
                    'external_id'      => $external_id,
                    'products_id'      => $content_id,
                    'manufacturers_id' => 4666,
                    'code'             => $code,
                    'is_variant'       => 0
                )
            );
            foreach ($this->languages as $lang) {
                $alias = $lang['code'] .'/'. $this->translit($names[$lang['code']]);
                if($this->issetAlias($alias)){
                    $this->db->rollBack();
                    $this->log("Dublicate alias $alias on $external_id ");
                    $this->errors[] = "Dublicate alias $alias on $external_id ";
                    return 0;
                }
                $s=$this->db->insert(
                    'content_info',
                    array(
                        'content_id'   => $content_id,
                        'languages_id' => $lang['id'],
                        'name'         => $names[$lang['code']],
                        'title'        => $names[$lang['code']],
                        'alias'        => $alias
                    )
                );
            }
        }

        if($content_id > 0 && $s > 0 && $o > 0){
            $this->db->commit();
//            $this->log("Створено {$names['ru']}  з ід $content_id    $external_id");
        } else{
            $this->db->rollBack();
            die('помилка створення товару ' . $external_id);
        }

        return $content_id;
    }

    public function issetAlias($alias)
    {
        return $this->db->select("select id from content_info where alias = '{$alias}' limit 1")->row('id');
    }

    private function makeProductName($data)
    {
        $res = array();

        foreach ($data['ЗначенияРеквизитов']['ЗначениеРеквизита'] as $row) {
            if($row['Наименование'] == 'НаименованиеРус' && isset($row['Значение'])){
                $res['ru'] = $row['Значение'];
            } elseif($row['Наименование'] == 'НаименованиеУкр' && isset($row['Значение'])){
                $res['uk'] = $row['Значение'];
            }
        }

        return $res;
    }

    /**
     * @param $data
     * @return array
     */
    private function readGroups($data)
    {
        $res = array();
        if(isset($data['Группы'])){
            if(isset($data['Группы']['Ид'])){
                $external_id = $data['Группы']['Ид'];
                if(isset($this->externalIds[$external_id])){
                    $res[] = $this->externalIds[$external_id];
                } else{
                    $id = $this->getContentID($external_id);
                    if(!empty($id)){
                        $res[] = $id;
                        $this->externalIds[$external_id] = $id;
                    } else {
                        if(!in_array($this->cat_id, $res)){
                            $res[] = $this->cat_id;
                        }
                    }
                }
            }
        }
        return $res;
    }

    /**
     * @param $data
     * @param int $parent_id
     * @return int
     */
    private function eachCategories($data, $parent_id = 0)
    {
        foreach ($data as $row) {
            if(isset($row['Ид'])){
                $external_id = $row['Ид'];
                $name        = $row['Наименование'];

                if(empty($external_id) || empty($name)) continue;

//                echo $external_id , '    ', $name , '<br>';

                // витягую ід категорії, якщо нема то створюю
                $id = $this->getContentID($external_id);
                if(empty($id)){
                    $id = $this->createCategory($external_id, $name, $parent_id);
                }

                if(isset($row['Группы'])){
                    $this->eachCategories($row['Группы'], $id);
                }

                continue;
            }

            foreach ($row as $group) {
                $external_id = $group['Ид'];
                $name        = $group['Наименование'];

                if(empty($external_id) || empty($name)) continue;

//                echo $external_id , '    ', $name , '<br>';

                // витягую ід категорії, якщо нема то створюю
                $id = $this->getContentID($external_id);
                if(empty($id)){
                    $id = $this->createCategory($external_id, $name, $parent_id);
                }

                if(isset($group['Группы'])){
                    $this->eachCategories($group['Группы'], $id);
                }
            }
        }

        return 1;
    }



    /**
     * @param $external_id
     * @return array|mixed
     */
    private function getContentID($external_id)
    {
        return $this->db->select("select id from content where external_id='{$external_id}' limit 1")->row('id');
    }

    /**
     * @param $code
     * @return array|mixed
     */
    private function getProductsIDByCode($code)
    {
        return $this->db->select("select products_id as id from products_options where code='{$code}' limit 1")->row('id');
    }

    private function createCategory($external_id, $name, $parent_id = 0)
    {
        $this->log("Створення категорії $name   $external_id");
        $this->db->beginTransaction(); $s=0;
        $content_id = $this->db->insert(
            'content',
            array(
                'external_id'  => $external_id,
                'parent_id'    => $parent_id,
                'type_id'      => 3,
                'templates_id' => 3,
                'owner_id'     => $this->cron_id,
                'createdon'    => date('Y-m-d H:i:s '),
                'auto'         => 0
            )
        );
        if($content_id>0){
            foreach ($this->languages as $lang) {
                $parent_alias = $this->db->select("
                    select alias
                    from content_info
                    where content_id = {$content_id} and languages_id = {$lang['id']}
                    limit 1
                ")->row('alias');
                if(!empty($parent_alias)) $parent_alias .= '/';

                $alias = $parent_alias . $this->translit($lang['code'] .'/'. $name);
                $s=$this->db->insert(
                    'content_info',
                    array(
                        'content_id'   => $content_id,
                        'languages_id' => $lang['id'],
                        'name'         => $name,
                        'title'        => $name,
                        'alias'        => $alias
                    )
                );
            }

            // update parent
            $this->db->update('content', array('isfolder' => 1), "id = {$parent_id} limit 1");
        }

        if($content_id > 0 && $s > 0){
            $this->db->commit();
            $this->log("Створено $name  з ід $content_id");
        } else{
            $this->db->rollBack();
            die('помилка створення категорії ' . $name . ' ' . $external_id);
        }

        return $content_id;
    }

    private function translit( $string )
    {
        $trans = array (
            "а"     => "a",
            "б"     => "b",
            "в"     => "v",
            "г"     => "h",
            "д"     => "d",
            "е"     => "e",
            "є"     => "ye",
            "ж"     => "zh",
            "з"     => "z",
            "и"     => "y",
            "і"     => "i",
            "ї"     => "yi",
            "й"     => "y",
            "к"     => "k",
            "л"     => "l",
            "м"     => "m",
            "н"     => "n",
            "о"     => "o",
            "п"     => "p",
            "р"     => "r",
            "с"     => "s",
            "т"     => "t",
            "у"     => "u",
            "ф"     => "f",
            "х"     => "kh",
            "ц"     => "ts",
            "ч"     => "ch",
            "ш"     => "sh",
            "щ"     => "sch",
            "ю"     => "yu",
            "я"     => "ya",
            "ь"     => "",
            "А"     => "A",
            "Б"     => "B",
            "В"     => "V",
            "Г"     => "H",
            "Д"     => "D",
            "Е"     => "E",
            "Є"     => "Ye",
            "Ж"     => "Zh",
            "З"     => "Z",
            "И"     => "Y",
            "І"     => "I",
            "Ї"     => "Yi",
            "Й"     => "Y",
            "К"     => "K",
            "Л"     => "L",
            "М"     => "M",
            "Н"     => "N",
            "О"     => "O",
            "П"     => "P",
            "Р"     => "R",
            "С"     => "S",
            "Т"     => "T",
            "У"     => "U",
            "Ф"     => "F",
            "Х"     => "Kh",
            "Ц"     => "Ts",
            "Ч"     => "Ch",
            "Ш"     => "Sh",
            "Щ"     => "Sch",
            "Ю"     => "Yu",
            "Я"     => "Ya",
            "Ь"     => "",
            "ы"     => "y",
            "э"     => "е",
            "Ы"     => "Y",
            "`"     => "",
            "’"     => "",
            "'"     => "",
            "."     => "_",
            "("     => "_",
            ")"     => "_",
            ","     => "",
            ">"     => "_less_",
            "<"     => "_over_",
            '"'     => "",
            "_"     => "",
            " "     => "-",
            "№"     => "Numb",
            "/"     => "_",
            "%"     => "proc",
            "&"     => "_and_",
            "&amp;" => "_and_",
            "+"     => "_plus_",
            "?"     => "",
            "!"     => "" );

        return strtr ( $string, $trans );
    }

    /**
     * creating db mysql backup
     */
    private function createDBBackup()
    {
        $this->log('Create DB backup');
        $conf = Config::instance()->get('db');
        $filename= $conf['db']. date('_Y-m-d_H-i-s').'.sql';

        exec("mysqldump {$conf['db']} --password={$conf['pass']} --user={$conf['user']} --single-transaction >{$this->backup_dir}" . $filename, $output);

        if(empty($output)){
            $this->log('Backup Db created. File: '. $filename);
        }
        else {
            $this->log('Backup error: '. var_export($output, 1));
        }
    }
    /**
     * @param $path
     * @param $data
     * @param string $mode
     * @return bool
     */
    private function write_file($path, $data, $mode = 'w+b')
    {
        if ( ! $fp = @fopen($path, $mode))
        {
            return FALSE;
        }

        flock($fp, LOCK_EX);
        fwrite($fp, $data);
        flock($fp, LOCK_UN);
        fclose($fp);

        return TRUE;
    }

    /**
     * @param $file
     * @return bool|string
     */
    private function read_file($file)
    {
        if ( ! file_exists($file))
        {
            return FALSE;
        }

        if (function_exists('file_get_contents'))
        {
            return file_get_contents($file);
        }

        if ( ! $fp = @fopen($file, 'rb'))
        {
            return FALSE;
        }

        flock($fp, LOCK_SH);

        $data = '';
        if (filesize($file) > 0)
        {
            $data =& fread($fp, filesize($file));
        }

        flock($fp, LOCK_UN);
        fclose($fp);

        return $data;
    }

    /**
     * первинна авторизація
     * @return bool
     */
    private function auth()
    {
        $pass = $this->settings->get('1c_token');
        if($pass == md5(session_id())){
            $this->log('Auth OK');
            return true;
        }

        $this->log('Auth FAIL');

        return false;
    }

    /**
     * display response
     * @param array $data
     */
    private function response(array $data)
    {
        header('Content-type: text/html; charset=cp1251');
        echo implode("\n", $data); die();
    }


    /**
     * @param $msg
     * @return int
     */
    private function log($msg)
    {
        $msg = date('Y-m-d H:i:s') . '    ' . $msg ."\r\n";
//        if($this->debug){
//            echo $msg;
//        }
        return file_put_contents(DOCROOT . 'logs/exchange1c-'. date('Ymd') .'.log', $msg, FILE_APPEND);
    }

    public function install(){return 1;}
    public function uninstall(){return 1;}
}