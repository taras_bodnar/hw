<?php
/**
 * Company Otakoyi.com
 * Author: wg
 * Date: 02.01.15 16:22
 */

namespace controllers\modules;

use controllers\core\DB;
use controllers\Module;

defined("SYSPATH") or die();

/**
 * Class XmlSitemap
 * @name XmlSitemap
 * @description SiteMap Generator. In robots.txt write: Sitemap: http://mysite.com/route/XmlSitemap/index
 * @package modules
 * @author wmgodyak mailto:wmgodyak@gmail.com
 * @version 1.0
 * @copyright &copy; 2015 Otakoyi.com
 */
class XmlSitemap extends Module {
    private $db;
    private $lang;

    public function __construct()
    {
        parent::__construct();

        $this->db = DB::instance();
        $this->lang = new \models\app\Languages();

        $lang_code = $this->request->get('l');
        if($lang_code){
            $this->languages_id = $this->lang->getIdByCode($lang_code);
        }
    }

    public function index()
    {
        $appurl = APPURL;
        $ct = $this->request->get('ct');
        if(!$ct){
            $items = array();
//            $items[] = array(
//                'loc'     =>  APPURL ."route/XmlSitemap/index",
//                'lastmod' => date('c')
//            );
            foreach($this->lang->get() as $lang){
                $l = $lang['front_default'] == 1 ? '' : "&amp;l={$lang['code']}";
                $types = $this->db->select("select type from content_type
                  where type not in ('Messages','category','manufacturer','product')
                  ")->all();
                foreach ($types as $ct) {
                    $items[] = array(
                        'loc'     => APPURL ."route/XmlSitemap/index/?ct={$ct['type']}{$l}",
                        'lastmod' => date('c')
                    );
                }
                $items[] = array(
                    'loc'     => APPURL ."route/XmlSitemap/index/?ct=users{$l}",
                    'lastmod' => date('c')
                );
            }

            $x=new \XMLWriter();
            $x->openMemory();
            $x->startDocument('1.0', 'UTF-8');
//            $x->writePi('xml-stylesheet', "type=\"text/xsl\" href=\"{$appurl}themes/default/views/modules/sitemap/sitemap-index.xsl\"");

            $x->startElement('sitemapindex');
            $x->writeAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');
            $x->writeAttribute('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
            $x->writeAttribute('xsi:schemaLocation', 'http://www.sitemaps.org/schemas/sitemap/0.9/siteindex.xsd');
            foreach ($items as $item) {
                $x->startElement('sitemap');
                $x->writeElement('loc',     $item['loc']);

                $x->writeElement('lastmod', date( 'c', strtotime( $item['lastmod'] )));
                $x->endElement(); //sitemap
            }

            $x->endElement(); //sitemapindex
            $out = $x->outputMemory();
        } else {
            $items = $this->getItemsByType($ct);

            $x=new \XMLWriter();
            $x->openMemory();
            $x->setIndent(1);
            $x->startDocument('1.0', 'UTF-8');
//            $x->writePi('xml-stylesheet', "type=\"text/xsl\" href=\"{$appurl}themes/default/views/modules/sitemap/sitemap.xsl\"");

            $x->startElement('urlset');
            $x->writeAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');
            $x->writeAttribute('xmlns:image', 'http://www.google.com/schemas/sitemap-image/1.1');

            if($ct=='users') {
                $priority = 0.9;

                $users = $this->db->select("select u.id,u.createdon,'{$priority}' as priority from users u
                  join users_group ug on u.users_group_id=ug.id
                  where ug.parent_id=2
                ")->all();

                foreach($users as $user) {
                    $lang_id = isset($_GET['l'])?2:1;
                    $code = isset($_GET['l'])?"ru/":"";
                    $alias = $this->db->select("select alias
                                  from content_info
                                  where content_id = 45 and languages_id = {$lang_id}
                                  limit 1
                                  ")->row('alias');
                    $x->startElement('url');
                    $x->writeElement('loc',    $appurl .$code.$alias."/". $user['id']);
                    $x->writeElement('lastmod', date( 'c', strtotime( $user['createdon'] ) ));
                    $x->writeElement('changefreq', 'weekly');
                    $x->writeElement('priority', $user['priority']);
                    $x->endElement(); //url
                }
            } else {
                foreach ($items as $item) {
                    $x->startElement('url');
                    $x->writeElement('loc',    $appurl . $item['loc']);
                    $x->writeElement('lastmod', date( 'c', strtotime( $item['lastmod'] ) ));
                    $x->writeElement('changefreq', $item['changefreq']);
                    $x->writeElement('priority', $item['priority']);
                    $x->endElement(); //url
                }
            }

            $x->endElement(); //urlset
            $out = $x->outputMemory();
        }

        header('Content-Type: application/xml; charset=utf-8');
        echo $out; die();
    }

    /**
     * @param $type
     * @param float $priority
     * @param string $changefreq
     * @return array|mixed
     */
    private function getItemsByType($type, $priority = 0.9, $changefreq = 'Weekly')
    {
        $res = $this->db
            ->select("
                select
                  c.id,
                  IF(l.id=ld.id, ci.alias, CONCAT(l.code, '/', ci.alias) ) as loc,
                  0 as images,
                  '{$priority}' as priority,
                  '{$changefreq}' as changefreq,
                  IF(c.editedon = '0000-00-00 00:00', c.createdon, c.editedon) as lastmod
                from content_type ct
                join content c on c.type_id=ct.id and c.published=1 and c.auto = 0
                join languages ld on ld.front_default = 1
                join languages l on l.id = '{$this->languages_id}'
                join content_info ci on ci.content_id = c.id and ci.languages_id = '{$this->languages_id}'
                where ct.type = '{$type}'
                order by lastmod desc
             ")
            ->all();

        foreach ($res as $i => $row) {
            $im = $this->images->get($row['id'], 'source');
            $res[$i]['image'] = $im;
        }

        return $res;
    }

    public function install(){}
    public function uninstall(){}
}