<?php
/**
 * Company Otakoyi.com
 * Author: wg
 * Date: 02.01.15 16:22
 */

namespace controllers\modules;

use controllers\core\Request;
use controllers\Module;

defined("SYSPATH") or die();

/**
 * Class Blog
 * @name Blog
 * @description Blog module
 * Class Blog
 * @package modules
 * @author wmgodyak mailto:wmgodyak@gmail.com
 * @version 1.0
 * @copyright &copy; 2014 Otakoyi.com
 */
class Blog extends Module {

    /**
     * ід сторінки, до якої прикручено блог
     * @var int
     */
    private $root_id=4;

    /**
     * Кількість статтей на сторінку
     * @var int
     */
    private $items_per_page=6;

    public function __construct()
    {
        parent::__construct();
        $this->m = new \models\modules\Blog();
    }

    /**
     * @return string
     */
    public function postDate()
    {
        $page = $this->request->get('page');
        $post_date = $page['dd'] .' '. $this->formatMonth($page['mm']) .' '. $page['yy'];
        return $post_date;
    }

    /**
     * @return string
     */
    public function index()
    {
        $items = '';
        $id = $this->request->get('id');

        // мітки
        $tag = $this->request->get('tag');
        if(!empty($tag)){
            $this->m->setJoin("join tags t on t.alias = '{$tag}' ");
            $this->m->setJoin("join tags_content tc on tc.tags_id = t.id and tc.content_id=c.id");
        }

        if($id != $this->root_id){
            $this->m->setJoin("join posts_categories pc on pc.cid={$id} and pc.pid=c.id");
        }

        $total = $this->m->getTotal();

        if($total > 0){
            $paginator = new Paginator($total, $this->items_per_page, $id . ';', 7);
            $limit = $paginator->getLimit();
            $this->m->setLimit($limit['start'], $limit['num']);
            $this->template->assign('pagination', $paginator->getPages());

            foreach ($this->m->getPosts() as $item) {
//                $item['mm'] = $this->formatMonth($item['mm']);
//                $item['img'] = $this->images->cover($item['id'],'post');
//                $item['tags'] = $this->m->getTags($item['id']);
                $this->template->assign('item', $item);
                $items .= $this->makeFriendlyUrl($this->template->fetch('modules/blog/item'));
            }
        }

        $this->template->assign('root_id', $this->root_id);
        $this->template->assign('items', $items);
        // показати ще

        $start = $this->request->get('p');
        if(!$start) $start=1;
        $total = $total - $start*$this->items_per_page;
        if($total<0) $total=0;
        $this->template->assign('start', $start);
        $this->template->assign('total', $total);
        $this->template->assign('categories', $this->m->clearLimit()->getCategories());

        return $this->template->fetch('modules/blog/index');
    }

    public function more()
    {
        $items = '';
        $id = (int)$_POST['id'];
        $start = isset($_POST['p'])?(int)$_POST['p']: 0;
        $start++;
        $this->request->param('p', $start);
        if(empty($id)) die();

        if($id != $this->root_id){
            $this->m->setJoin("join posts_categories pc on pc.cid={$id} and pc.pid=c.id");
        }

        $total = $this->m->getTotal();

        $paginator = new Paginator($total, $this->items_per_page, $id . ';', 7);
        $limit = $paginator->getLimit();
        $this->m->setLimit($limit['start'], $limit['num']);
        $this->template->assign('pagination', $paginator->getPages());

        foreach ($this->m->getPosts() as $item) {
            $this->template->assign('item', $item);
            $items .= $this->makeFriendlyUrl($this->template->fetch('modules/blog/item'));
        }

        // показати ще
        $total = $total - $start*$this->items_per_page;
        if($total<0) $total=0;

        return json_encode(array(
            't'          => $total,
            'items'      => $items,
            'pagination' => $this->makeFriendlyUrl($paginator->getPages()),
            'p'          => $start
        ));
    }

    public function widget()
    {
        $page = Request::instance()->get('page');

        if (in_array($page['id'], [8])) return '';

        if (in_array($page['id'], [7, 8, 10, 11, 12, 260]) && $_SESSION['app']['user']['users_group_id'] != 3) {
            $this->m->type_post = 'news';
        }

        $this->m->setLimit(0, 12);
        $items= array();
        foreach ($this->m->getPosts() as $item) {
//            $item['img'] = $this->images->cover($item['id'],'post');
            $items[] = $item;
        }

        $this->template->assign('blog_type', $this->m->type_post);
        $this->template->assign('items', $items);
        return $this->template->fetch('modules/blog/widget');
    }

    public function relatedPost()
    {
        $id = $this->request->get("id");
        $items = $this->m->getRelatedPosts($id);
        if(empty($items)) return;

        foreach($items as $k=>$item) {
            $items[$k]['img'] = $this->images->cover($item['id'],"news");
        }

        $this->template->assign("items",$items);

        return $this->template->fetch("modules/blog/relatedPosts");
    }


    public function install()
    {
//        $this->addSQL("delete from settings where name='Blog_install' limit 1");
//        $this->addSQL("insert into settings (name,value) values ('Blog_install','OKI')");
//        $this->addTranslation('mod_Blog_hello', 'hello');
//        $this->addSetting('t1','Blog 1', 1 )
//             ->addSetting('t2', 'Blog2', 2)
//             ->addSetting('t3', 'Blog3', 2)
//             ->addSetting('t4', 'Blog4', 2)
//        ;
    }

    public function uninstall()
    {
//        $this->addSQL("delete from settings where name='Blog_install' limit 1");
//        $this->rmTranslations(array('mod_Blog_hello'));


    }

    public final function formatMonth($m)
    {
        $m--;
        $a = array(1,2,3);
        $a[1] = array('Січня','Лютого','Березня','Квітня','Травня','Червня','Липня','Серпня','Вересня','Жовтня','Листопада','Грудня');
        $a[2] = array('Января','Февраля','Марта','Апреля','Мая','Июня','Июля','Августа','Сентября','Октября','Ноября','Декабря');
        $a[3] = array('January','February','March','April','May','June','July','August','September','October','November','December');
        return isset($a[$this->languages_id][$m]) ? $a[$this->languages_id][$m] : 'Bad format: ' .$m;
    }
}