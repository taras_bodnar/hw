<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 14.10.14 12:32
 */

namespace controllers\modules;
use controllers\core\Request;
use controllers\Module;

defined("SYSPATH") or die();

/**
 * Class Paginator
 * @name OYi.Paginator
 * @description Paginator module
 * Class Paginator
 * @package modules
 * @author wmgodyak mailto:wmgodyak@gmail.com
 * @version 1.0
 * @copyright &copy; 2014 Otakoyi.com
 */
class Paginator extends Module {
    private $current_page;
    private $items_per_page;
    private $limit_end;
    private $limit_start;
    private $num_pages;
    private $total_items;
//    private $ipp_array;
//    private $limit;
    private $mid_range;
    private $querystring;
//    private $return;
//    private $get_ipp;
    private $page;
    private $url;
    private $translations;

    private $prev_page = null;
    private $next_page = null;
    private $all_pages;
    private $items;

    public function __construct(
        $total=0,
        $items_per_page=5,
        $url='',
        $mid_range=7,
        $page='p',
        $generated = false
    )
    {

        parent::__construct();

        $this->total_items = (int) $total;
        if($this->total_items <= 0) return;
        $this->mid_range = (int) $mid_range; // midrange must be an odd int >= 1
        if($this->mid_range%2 == 0 Or $this->mid_range < 1) exit("Unable to paginate: Invalid mid_range value (must be an odd integer >= 1)");

        $this->url = $url;
        $this->page = $page;

       /* $this->translations = array(
                                'prev'=>!isset($conf['paginator_prev']) ? null : $conf['paginator_prev'],
                                'next'=>!isset($conf['paginator_next']) ? null : $conf['paginator_next'],
                                'all'=>!isset($conf['paginator_all']) ? null : $conf['paginator_all']
                            );
        */
        $this->items_per_page = (int) $items_per_page;

        if($this->items_per_page == "All") {
            $this->num_pages = 1;
        } else {
            $this->num_pages = ceil($this->total_items/$this->items_per_page);
        }

        $p = Request::instance()->param('p');
        $this->current_page = ($p) ? (int) $p : 1 ; // must be numeric > 0

        if($_REQUEST) {
            $args = array();
            parse_str($_SERVER["QUERY_STRING"], $qs);
            $qs = array_merge($qs, $_REQUEST);
            foreach($qs as $k=>$v){
                if(in_array($k,array('q','p','module','action','route', 'id', 'skey', 'oyiengine'))) continue;

                if(is_array($v)){
                    foreach($v as $name=>$value){
                        if(empty($value)) continue;
                        if(is_array($value)){
                            foreach($value as $vk=>$vv){
                                if(is_array($vv)){
                                    foreach($vv as $vvk=>$vvv){
                                        @$args[] = $k.'['. $name .']['. $vk .']['. $vvk .']=' . $vvv;
                                    }
                                }else {

                                    @$args[] = $k.'['. $name .']['. $vk .']=' . $vv;
                                }
                            }
                        }else{
                            @$args[] = $k.'['. $name .']=' . $value;
                        }
                    }
                } else{
                    @$args[] = "$k=$v";
                }
            }
            if(!empty($args)) $this->querystring .= '&' . implode('&',$args);
        }
        if($this->num_pages > 1) {
            if($this->current_page > 1 ){
                $this->prev_page = array(
                    'url'  => "$this->url$this->page=".($this->current_page-1)."?$this->querystring",
                    'name' => 'prev'
                );
            }

            $this->start_range = $this->current_page - floor($this->mid_range/2);
            $this->end_range = $this->current_page + floor($this->mid_range/2);
            if($this->start_range <= 0) {
                $this->end_range += abs($this->start_range)+1;
                $this->start_range = 1;
            }
            if($this->end_range > $this->num_pages) {
                $this->start_range -= $this->end_range-$this->num_pages;
                $this->end_range = $this->num_pages;
            }
            $this->range = range($this->start_range,$this->end_range);
            for($i=1;$i<=$this->num_pages;$i++) {
                if($this->range[0] > 2 And $i == $this->range[0]) {
                    // $this->return .= "<li class=\"disabled\"><a> ... </a></li>";
                    $this->addItem('', ' ... ', 'disabled');
                }
                // loop through all pages. if first, last, or in range, display
                if($i==1 Or $i==$this->num_pages Or in_array($i,$this->range)) {
                    if($i == $this->current_page And $this->items_per_page != "All"){
                        $this->addItem('', $i, 'active');
                    } else {
                        if(!$generated) {
                            $this->addItem("$this->url$this->page=$i;$this->querystring", $i);
                        } else {
                            if($i>1) {
                                $this->addItem("$this->url/$i" . (!empty($this->querystring)?'?'.$this->querystring:''), $i);
                            } else {
                                $this->addItem("$this->url" . (!empty($this->querystring)?'?'.$this->querystring:''), $i);
                            }
                        }
                    }
                }
                if($this->range[$this->mid_range-1] < $this->num_pages-1 And $i == $this->range[$this->mid_range-1]) {
                    $this->addItem('', ' ... ', 'disabled');
                }
            }

            if(($this->current_page < $this->num_pages ) And ($this->items_per_page != "All") And $this->current_page > 0) {
//                echo $this->url;
                $this->next_page = array(
                    'url'  => "$this->url$this->page=" . ($this->current_page+1) ."?$this->querystring",
                    'name' => 'next'
                );
            }
        } else	{
            for($i=1;$i<=$this->num_pages;$i++) {
                if($i == $this->current_page) {
                    $this->addItem('', $i, 'active');
                } else {
                    $this->addItem("$this->url$this->page=$i;$this->querystring", $i);
                }

            }
        }
        $this->all_pages = array(
            'url'  => "$this->url&ipp=all$this->querystring", $this->translations['all'],
            'name' => $this->translations['all']
        );
        $this->limit_start = ($this->current_page <= 0) ? 0:($this->current_page-1) * $this->items_per_page;
        if($this->current_page <= 0) $this->items_per_page = 0;

        $this->limit_end = ($this->items_per_page == "All") ? (int) $this->total_items: (int) $this->items_per_page;
    }

    /**
     * add item to list
     * @param $url
     * @param $name
     * @param $class
     * @return $this
     */
    private function addItem($url, $name, $class='')
    {
        $this->items[] = array(
                            'url'   => $url,
                            'name'  =>$name,
                            'class' => $class
                        );
        return $this;
    }

    public function getLimit(){
        if($this->total_items <= 0) {
            $this->limit_start=0;
            $this->limit_end=0;
        }
        return array('start' => $this->limit_start, 'num' => $this->limit_end);
    }

    /**
     * @return mixed
     */
    public function getItems()
    {
        return $this->items;
    }
    public function getPrev()
    {
        return $this->prev_page;
    }

    public function getNext()
    {
        return $this->next_page;
    }
    public function getPages()
    {

//        $this->dump($this->next_page);
//        $this->dump($this->items);
        $this->template
            ->assign('items', $this->items)
            ->assign('prev', $this->prev_page)
            ->assign('next', $this->next_page)
            ->assign('all', $this->all_pages)
        ;

        return $this->template->fetch('modules/pagination');
    }

    public function index(){}
    public function install(){}
    public function uninstall(){}

} 