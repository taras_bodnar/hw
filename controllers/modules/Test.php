<?php
/**
 * Company Otakoyi.com
 * Author: wg
 * Date: 02.01.15 16:22
 */

namespace controllers\modules;

use controllers\App;
use controllers\core\DB;
use controllers\Module;

defined("SYSPATH") or die();

/**
 * Class Test
 * @name Test
 * @description Test module
 * Class Test
 * @package modules
 * @author wmgodyak mailto:wmgodyak@gmail.com
 * @version 1.0
 * @copyright &copy; 2014 Otakoyi.com
 */
class Test extends Module {

    public function index(){

        $db = DB::instance();

        foreach ($db->select("select id, file from users_uploads where provider = 'vimeo' and thumb IS NULL")->all() as $item) {
            $this->dump($item);
            $u = @file_get_contents("https://vimeo.com/api/v2/video/{$item['file']}.json");
            if($u){
                $a = json_decode($u, true);
                    $this->dump($a);
                $thumb = $a[0]['thumbnail_medium'];
                if(!empty($thumb)){
                    $db->update('users_uploads', array('thumb' => $thumb) , " id = {$item['id']} limit 1");
                }
            }
        }
        die('ok');
    }
    public function hello($id, $pid)
    {
        $page_id = $this->request->get('id', 'int');
        $page_name = $this->request->get('name');
//        print_r($this->request);

        $sum = $id + $pid;

        print_r($this->translation);

        return "Test::hello($id, $pid, $sum,Current page id is  $page_id) $page_name &
            currenct languages id is {$this->languages_id}";
    }


    public function install()
    {
//        $this->addSQL("delete from settings where name='Test_install' limit 1");
//        $this->addSQL("insert into settings (name,value) values ('Test_install','OKI')");
//        $this->addTranslation('mod_Test_hello', 'hello');
//        $this->addSetting('t1','Test 1', 1 )
//             ->addSetting('t2', 'Test2', 2)
//             ->addSetting('t3', 'Test3', 2)
//             ->addSetting('t4', 'Test4', 2)
//        ;
    }

    public function uninstall()
    {
//        $this->addSQL("delete from settings where name='Test_install' limit 1");
//        $this->rmTranslations(array('mod_Test_hello'));


    }

    public function gen()
    {
        $db = DB::instance();
        $k = !empty($_GET['start']) ? intval($_GET['start']) : 0;
        $total = !empty($_GET['total']) ? intval($_GET['total']) : $db->select("select count(id) as t from users")->row('t');
        $item = $db->select("select id, name, surname from users limit {$k}, 1")->row();
        $url = $this->translit(trim($item['name']) . (empty($item['surname']) ? '' : '-' . trim($item['surname'])));
        $is = $db->select("select id from users where url = '{$url}' and id <> '{$item['id']}' limit 1")->row('id');
        if ($is > 0) {
            $url = $url . '-' . $item['id'];
        }
        $db->update('users', array('url' => $url), "id = '{$item['id']}'");
        $k++;
        if ($k >= $total) {
            die('ok');
        } else {
            die('<script>window.location.search = "?start=' . $k . '&total=' . $total . '";</script>');
        }
    }

    public static function translit($text)
    {
        $text = mb_strtolower($text);
        $table = array(
            'а'=>'a',
            'б'=>'b',
            'в'=>'v',
            'г'=>'g',
            'ґ'=>'g',
            'д'=>'d',
            'е'=>'e',
            'є'=>'e',
            'ж'=>'zh',
            'з'=>'z',
            'и'=>'y',
            'і'=>'i',
            'ї'=>'yi',
            'й'=>'y',
            'к'=>'k',
            'л'=>'l',
            'м'=>'m',
            'н'=>'n',
            'о'=>'o',
            'п'=>'p',
            'р'=>'r',
            'с'=>'s',
            'т'=>'t',
            'у'=>'u',
            'ф'=>'f',
            'х'=>'kh',
            'ц'=>'ts',
            'ч'=>'ch',
            'ш'=>'sh',
            'щ'=>'sch',
            'ь'=>'',
            'ю'=>'yu',
            'я'=>'ya',
            'ё'=>'e',
            'ы'=>'y',
            'э'=>'e',
            'ъ'=>'',
            ' '=>'-'
        );

        $text = strtr($text, $table);
        $text = preg_replace('/[^0-9a-z_\-]+/u', '', $text);
        return $text;
    }


}