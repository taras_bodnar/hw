<?php
/**
 * Company Otakoyi.com
 * Author: wg
 * Date: 02.01.15 16:22
 */

namespace controllers\modules;

use controllers\core\Settings;
use controllers\Module;
use models\app\Guides;
use models\modules\Users as MU;

defined("SYSPATH") or die();

/**
 * Class Catalog
 * @name Catalog
 * @description Catalog module
 * Class Catalog
 * @package modules
 * @author wmgodyak mailto:wmgodyak@gmail.com
 * @version 1.0
 * @copyright &copy; 2014 Otakoyi.com
 */
class Catalog extends Module
{
    private $mCatalog;
    private $mGuides;
    private $mUsers;
    private $items_per_page = 30;
    private $pph = 0;
    private $ppd = 0;

    public function __construct()
    {
        parent::__construct();

        $this->mCatalog = new \models\modules\Catalog();
        $this->mGuides = new Guides();
        $this->mUsers = new MU();

        $this->pph = Settings::instance()->get('workers_pph');
        $this->ppd = Settings::instance()->get('workers_ppd');

        // налаштування фільтру
        if ( $this->request->isXhr() ) {
            $id = $this->request->post('id');
            $templates_id = $this->mCatalog->getTemplatesID($id);
            if ( $id != 3 && $templates_id == 18 ) {
                $users_group_id = $this->mCatalog->workersGroupID($id);
                $this->mCatalog->where("  u.users_group_id = {$users_group_id} ");
            } else {
                $this->mCatalog->join(" join users_group ug on ug.parent_id=2 and ug.id=u.users_group_id");
            }
        } else {

            $page = $this->request->get('page');
//        $this->mCatalog->debug();
            if ( $page['id'] != 3 && $page['templates_id'] == 18 ) {
                $users_group_id = $this->mCatalog->workersGroupID($page['id']);
                $this->mCatalog->where("  u.users_group_id = {$users_group_id} ");
            } else {
//            $g = $this->mCatalog->getWorkersGroupsID();
//            $in = implode(',', $g);
//            $this->mCatalog->where("  u.users_group_id in (select id from users_group where parent_id=2) ");
                $this->mCatalog->join(" join users_group ug on ug.parent_id=2 and ug.id=u.users_group_id");
            }

        }
        $sUrl = '';
//        print_r($_REQUEST);
        if ( isset($_REQUEST['filter']) ) {
            $filter = $_REQUEST['filter'];
            $sUrl = http_build_query(array('filter' => $filter));
            if ( isset($filter['city_id']) ) {
                $filter['city_id'] = (int)$filter['city_id'];
                if ( $filter['city_id'] > 0 ) {
//                    $this->mCatalog->where(" u.city_id={$filter['city_id']} ");
//                    $this->mCatalog->key(" ,if(us.sort!='',us.sort,u.sort) as new_sort");
                    $this->mCatalog->join("  join users_cities uc on uc.guides_id={$filter['city_id']} and uc.users_id=u.id");
//                    $this->mCatalog->join(" left join users_sort us on us.users_id=u.id and us.guides_id={$filter['city_id']}");
//                    $this->mCatalog->where(" u.city_id={$filter['city_id']} ");
//                    $this->mCatalog->order(" abs(us.sort) asc");
                }
            }
            /*if(isset($filter['city_id'])){
                $filter['city_id'] = (int)$filter['city_id'];
                if($filter['city_id']>0){
                    $this->mCatalog->where(" u.city_id={$filter['city_id']} ");
                }
            }*/
        }

        if ( isset($_GET['filter']) ) {
            $filter = $_GET['filter'];
            $sUrl = http_build_query(array('filter' => $filter));
            if ( isset($filter['city_id']) ) {
                $filter['city_id'] = (int)$filter['city_id'];
                if ( $filter['city_id'] > 0 ) {
                    $this->mCatalog->join("  join users_cities uc1 on uc1.guides_id={$filter['city_id']} and uc1.users_id=u.id");
                }
            }
        }

        if ( isset($_REQUEST['order']) ) {
            switch ($_REQUEST['order']) {
                case 22: // дешевше
                    $this->mCatalog->order(" new_price asc");
                    break;
                case 23: // дорожчі
                    $this->mCatalog->order(" new_price desc");
                    break;
                case 52: // %
                    $this->mCatalog->order(" abs((SUM(discount_per_week)+SUM(discount_per_month)+SUM(discount_per_2_month))/3) desc");
                    break;
                default:
                    $this->mCatalog->order(" abs(u.hot) desc,u.hot_date desc,abs(u.gold) desc,u.gold_date desc,abs(u.mark) desc ");
                    break;
            }
        } else {
            // $this->mCatalog->order(" abs(u.sort) asc");
        }
//        $this->mCatalog->debug();
        if ( isset($_REQUEST['q']) ) {

            $sq = trim(strip_tags($_REQUEST['q']));
            if ( strlen($sq) > 2 ) {

                $q = explode(' ', $sq);
                $sql = array ();
                foreach ( $q as $k => $v ) {
                    $v = trim($v);
                    $v = strip_tags($v);

                    $sql[] = " u.name like '%$v%' or u.surname like '%$v%' or u.phone like '%$v%' or u.email like '%$v%' ";
                }

                // задаю критерії пошуку
                if ( !empty($sql) ) {
                    $this->mCatalog->where(" (" . implode(' and ', $sql) . ") ");
                }
            }

        }

        $this->template->assign('sUrl', $sUrl);
        $this->template->assign('sppd', $this->ppd);
        $this->template->assign('spph', $this->pph);
    }

    public function index()
    {
        if((isset($_GET['filter']['df']) && empty($_GET['filter']['df'])) && (isset($_GET['order']) && empty($_GET['order']))) {
            $url = $_SERVER['REQUEST_URI'];
            $url = explode('?', $url);
            header('Location: ' . $url[0]);die;
        }

        $id = $this->request->get('id');

//        if(isset($_SESSION['admin'])){ $this->mCatalog->debug(); }

        $total = $this->mCatalog->getWorkersTotal();

        $t_total = $total;
        if ( $total == 0 ) {
            return $this->template->fetch('modules/catalog/no_results');
        }
//        $this->formateMark();

        $handle = $this->request->param('handle');
        $lang = $this->request->param('lang');
        if(isset($handle) && !empty($handle)) {
            $handle = !empty($lang)?$lang.'/'.$handle:$handle;
            $paginator = new Paginator($total, $this->items_per_page, $handle,7,'p', true);
        } else {
            $paginator = new Paginator($total, $this->items_per_page, $id.';',7);
        }
        $limit = $paginator->getLimit();
        $this->mCatalog->limit($limit['start'], $limit['num']);
        $this->template->assign('pagination', $paginator->getPages());

        $items = $this->getWorkers();
//        $this->dump($items);die;
        foreach ( $items as $k=>$item ) {
            $items[$k] = $item['html'];
        }
//        $this->dump($items[0]);die;

        $search = array('/>[^S ]+/s','/[^S ]+</s','/(s)+/s');
        $replace = array('>','<','1');
        $items = str_replace(array("\r", "\n"), '',$items);

        $this->template->assign('root_id', $id);
        $this->template->assign('items', implode('', $items));
        $this->template->assign('sorting', $this->mGuides->get(19));

        // показати ще
        $start = $this->request->get('p');
        if ( !$start ) $start = 1;
        $total = $total - $start * $this->items_per_page;
        if ( $total < 0 ) $total = 0;
        $this->template->assign('start', $start);
        $this->template->assign('total', $total);
        $this->template->assign('t_total', $t_total);


        $filter = isset($_GET['filter'])?$_GET['filter']:'';
        $city_id = isset($filter['city_id']) && !empty($filter['city_id'])?$filter['city_id']:'';

        if (isset($city_id) && !empty($city_id)) {
            $seo = $this->mCatalog->getSeo($id, $city_id);
            $this->template->assign('seo', $seo);
        } else{
            $seo = array();
        }

        return $this->template->fetch('modules/catalog/index');
    }

    public function formateMark()
    {
        $items = $this->mCatalog->getAllWorkers();

        foreach ($items as $item) {
            $this->mCatalog->updateMark($item['id'],$this->points($item['id']));
        }
    }

    public function more()
    {
//        $this->dump($_POST['filter']['city_id']);die();
//        die();
        $id = (int)$_POST['id'];
        $start = isset($_POST['p']) ? (int)$_POST['p'] : 0;
        $start++;
        $this->request->param('p', $start);
        if ( isset($_POST['filter']) ) {
            $city_id = str_replace(',', '', implode(',', $_POST['filter']));

            if ( isset($city_id) && !empty($city_id) && $city_id > 0 ) {
                $this->mCatalog->join(" left join users_sort us on us.users_id=u.id and us.guides_id={$city_id}");
                $this->mCatalog->where(" u.city_id={$city_id} ");
                $this->mCatalog->order(" abs(us.sort) asc");
            }
        }

        if ( empty($id) ) die();

        $total = $this->mCatalog->getWorkersTotal();

        $paginator = new Paginator($total, $this->items_per_page, $id . ';');
        $limit = $paginator->getLimit();
        $this->mCatalog->limit($limit['start'], $limit['num']);
        $this->template->assign('pagination', $paginator->getPages());

        $items = array ();
        foreach ( $this->getWorkers() as $item ) {
            $items[] = $this->makeFriendlyUrl($item);
        }


        // показати ще
        $total = $total - $start * $this->items_per_page;
        if ( $total < 0 ) $total = 0;

        return json_encode(array (
            't' => $total,
            'items' => $items,
            'pagination' => $this->makeFriendlyUrl($paginator->getPages()),
            'p' => $start
        ));
    }

    public function getWorkers()
    {
        $res = array ();
        $filter = isset($_REQUEST['filter']) ? $_REQUEST['filter'] : array();
        foreach ( $this->mCatalog->getWorkers() as $h=>$item ) {
            $item['img'] = array ();
            $item['avatar'] = !empty($item['avatar'])?$item['avatar']:'';
            foreach ( $this->mCatalog->getUploads($item['id'], 5) as $k => $img ) {
                $hash_id = Users::hashID($item['id']);
                if ( isset($img['type']) && $img['type'] == 'video' ) {
                    if ( !isset($img['provider']) ) continue;
                    $v = Users::makeVideoUrl($img['file'], $img['provider'], $img['thumb']);
//                    $this->dump($v);
                    $item['img'][$k]['big'] = $v['big'];
                    $item['img'][$k]['thumb'] = $v['thumb'];
//                    $item['img'][$k]['big']   = "http://www.youtube.com/embed/{$img['file']}";
//                    $item['img'][$k]['thumb'] = "http://img.youtube.com/vi/{$img['file']}/0.jpg";
                    $item['img'][$k]['type'] = 'video';
                } else {
                    $item['img'][$k]['big'] = HOTWEDURL.'/uploads/portfolio/' . $hash_id . '/' . 'big_' . $img['file'];
                    $item['img'][$k]['thumb'] = HOTWEDURL.'/uploads/portfolio/' . $hash_id . '/' . 'thumb_' . $img['file'];
                }
                $item['img'][$k]['day_check']= $img['day_check'];
            }
            $item['price_per_day'] = round($item['price_per_day']);
            $item['price_per_hour'] = round($item['price_per_hour']);

            $item['city'] =$this->mGuides->getUserCities($item['id']);
            if (count($item['city']) == 1 && !empty($filter['city_id']) && $item['city'][0]['id'] != $filter['city_id']) {
                unset($item);
                continue;
            }
            $item['comments'] = $this->mCatalog->getCommentsTotal($item['id']);
            $info = $this->mCatalog->getWorkerInfo($item['id']);
            $item['category'] = $info['group_name'];
//            $this->dump($info);
//            $this->dump($this->g->getUserCities($item['id']));

            // різниця між вартістю виконавця і середньою вартістю
            $item['sppd'] = round(100 * ($item['price_per_day'] - $this->ppd) / $this->ppd, 1);
            $item['spph'] = round(100 * ($item['price_per_hour'] - $this->pph) / $this->pph, 1);
            $item['point'] = $this->points($item['id']);

//            echo $this->ppd, ' ', $item['price_per_day'] ,' ', $item['sppd'], '<br>';

            // comments
            $item['views'] = $this->mCatalog->getViews($item['id']);

            $this->template->assign('item_url', $this->mkUrl(45) . '/' . $item['url'] . (!empty($_GET['filter']['df']) ? '?' . trim($_GET['filter']['df']) : ''));
            $this->template->assign('item', $item);
            $res[$h]['html'] = $this->template->fetch('modules/catalog/item');
            $res[$h]['point'] = $this->points($item['id']);
//            $res[] = $itemF;
        }

//        if(!isset($_GET['order'])) {
//            usort($res, array("controllers\\modules\\Catalog", "mySortDesc"));
//        }
//        $this->dump($res);
//        die();
        return $res;
    }

    public function mySortDesc($a, $b)
    {

        $point_1 = $a['point'];

        $point_2 = $b['point'];

        if ( $point_1 == $point_2 ) {
            return 0;
        }
        return ($point_1 > $point_2) ? -1 : 1;
    }


    public function filter()
    {
//        $id = $this->request->get('id');
        $this->template->assign('city', $this->mGuides->get(9));
        $this->template->assign('group', $this->mCatalog->getUsersGroup(2));
        return $this->template->fetch('modules/catalog/filter');
    }

    public function categoriesOnMain()
    {
        $items = $this->mCatalog->categoriesOnMain(3);
//        $items = $this->mCatalog->getItems(3);
        foreach ( $items as $k => $item ) {
            $items[$k]['img'] = $this->images->cover($item['id'], 'wg');
            $items[$k]['t'] = $this->mCatalog->getWorkersCount($item['id']);
        }

        $this->template->assign('show_all_image', $this->images->cover(3, 'wg'));
        $this->template->assign('items_total', count($this->mCatalog->getItems(3)));
        $this->template->assign('items', $items);

        return $this->template->fetch('modules/catalog/main_categories');
    }

    public function mainGallery()
    {
        $items = $this->mCatalog->getMainGalleryItems();
        foreach ( $items as $k => $item ) {
            $hash_id = Users::hashID($item['users_id']);
            if ( isset($item['type']) && $item['type'] == 'video' ) {
//                $item['big']   = "http://www.youtube.com/embed/{$item['file']}";
//                $items[$k]['img'] = "http://img.youtube.com/vi/{$item['file']}/default.jpg";
                $v = Users::makeVideoUrl($item['file'], $item['provider'], $item['thumb']);
                if ( empty($v['thumb']) ) continue;
              if($item['provider']=='youtube') {
                  $items[$k]['img'] = "http://img.youtube.com/vi/{$item['file']}/default.jpg";
              } else {
                  $items[$k]['img'] = $v['thumb'];
              }
            } else {
//                $item['big']   = '/uploads/portfolio/' . $hash_id .'/' . 'big_'. $item['file'];
                $items[$k]['img'] = HOTWEDURL.'/uploads/portfolio/' . $hash_id . '/' . 'thumb_' . $item['file'];
            }
        }
//        $this->dump($items);
        $this->template->assign('items', $items);
        return $this->template->fetch('modules/catalog/main_gallery');
    }

    public function worker()
    {
//        die($this->dump($_SESSION));
        $users_id = $this->request->get('p');
        if ( !$users_id ) $this->redirect404();
        $guest_id = (int)Users::data('id');
        $user = $this->mCatalog->getWorkerInfo($users_id, $guest_id);
        $this->mCatalog->stat($users_id);
//        $this->dump($user);
        $re = "/(?:(?:(?:https|http):\\/\\/+)|)(?:www|)(instagram.com\\/)([a-zA-Z]+)/";
        $str = $user['instagram'];

        preg_match($re, $str, $matches);
//        $this->dump($matches);
        if ( (empty($matches) || $matches[1] != "instagram.com/") && !empty($user['instagram']) ) {
            $user['instagram'] = "https://instagram.com/" . $user['instagram'];
        }
        if ( empty($user) ) {
            return;
            $this->redirect404();
        }
//        $this->dump($user);

        $user['city'] =$this->mGuides->getUserCities($users_id);

        $images = array ();
        foreach ( $this->mCatalog->getUploads($users_id) as $k => $img ) {
            $hash_id = Users::hashID($users_id);
            if ( isset($img['type']) && $img['type'] == 'video' ) {
                $v = Users::makeVideoUrl($img['file'], $img['provider'], $img['thumb']);
                if ( empty($v['thumb']) ) continue;
//                $this->dump($v);
                $images[$k]['big'] = $v['big'];
                $images[$k]['thumb'] = $v['thumb'];
//                $images[$k]['thumb'] = "http://img.youtube.com/vi/{$img['file']}/0.jpg";
                $images[$k]['type'] = 'video';
            } else {
                $images[$k]['big'] = HOTWEDURL.'/uploads/portfolio/' . $hash_id . '/' . 'big_' . $img['file'];
                $images[$k]['thumb'] = HOTWEDURL.'/uploads/portfolio/' . $hash_id . '/' . 'thumb_' . $img['file'];
            }
            $images[$k]['day_check'] = $img['day_check'];
        }
        if ( !empty($images) ) {
            $images = array_chunk($images, 15);
        }

        $user['images'] = $images;


        // різниця між вартістю виконавця і середньою вартістю
        $user['sppd'] = round(100 * ($user['price_per_day'] - $this->ppd) / $this->ppd, 1);
        $user['spph'] = round(100 * ($user['price_per_hour'] - $this->pph) / $this->pph, 1);

//        $this->dump($user);
        $user['site'] = str_replace(array ('http://', 'https://'), array (), $user['site']);
        $user['blog'] = str_replace(array ('http://', 'https://'), array (), $user['blog']);
        $user['facebook'] = str_replace(array ('http://', 'https://'), array (), $user['facebook']);
        $user['vk'] = str_replace(array ('http://', 'https://'), array (), $user['vk']);
        $user['instagram'] = str_replace(array ('http://', 'https://'), array (), $user['instagram']);

        $this->template->assign('user', $user);

        $o = new Orders();
        $this->template->assign('order_form', $o->calc($users_id));

        // коментарі
        $c = new Comments($users_id);
        $c->show();

        //Лайки
        $like = $this->mUsers->getTotalLike($users_id);

        //Бали
        $points = $this->points($users_id);

        $this->template->assign('is_worker', Users::isWorker());
        $this->template->assign('is_online', Users::isOnline());
        $this->template->assign('like', $like);
        $this->template->assign('points', $points);
        $this->template->assign('pointsDel', $points/10);
        $this->template->assign('views', $this->mCatalog->getViews($users_id));
        return $this->template->fetch('modules/catalog/worker');
    }

    public function points($id)
    {
        $point = $this->mUsers->getInfoById($id,'point');

        //Витягую вартість за послугу

        $s = $this->mUsers->getPrice($id);

        $point = $s == 1 ? $point + 5 : $point;

        //Витягую знижки
        $s = $this->mUsers->getDiscount($id);
        $point = $s == 1 ? $point + 10 : $point;

        //Витягую портфоліо
        $s = $this->mUsers->getPortfolio($id);
        $point = $s > 0 ? $point + 10 : $point;

        //Витягую коментарі
        $s = $this->mCatalog->getCommentsTotal($id);
        $point = $s > 0 ? $point + (10 * $s) : $point;

        //Витягую лайки
        $s = $this->mUsers->getTotalLike($id);
        $point = $s > 0 ? $point + (1 * $s) : $point;

        //витягую Підтверджені замовлення
        $s = $this->mUsers->getConfirmOrders($id);
        $point = $s > 0 ? $point + (20 * $s) : $point;


        return $point;
    }


    public function seo()
    {
        $service_id = $this->request->get('id');
        $filter = isset($_GET['filter'])?$_GET['filter']:'';
        $city_id = isset($filter['city_id']) && !empty($filter['city_id'])?$filter['city_id']:'';

        if (isset($city_id) && !empty($city_id)) {
            $seo = $this->mCatalog->getSeo($service_id, $city_id);

        } else {
            $seo = array();
        }

        $this->template->assign('seo', $seo);

        return $this->template->fetch('modules/catalog/seo');
    }


    public function install()
    {
//        $this->addSQL("delete from settings where name='Catalog_install' limit 1");
//        $this->addSQL("insert into settings (name,value) values ('Catalog_install','OKI')");
//        $this->addTranslation('mod_Catalog_hello', 'hello');
//        $this->addSetting('t1','Catalog 1', 1 )
//             ->addSetting('t2', 'Catalog2', 2)
//             ->addSetting('t3', 'Catalog3', 2)
//             ->addSetting('t4', 'Catalog4', 2)
//        ;
    }

    public function uninstall()
    {
//        $this->addSQL("delete from settings where name='Catalog_install' limit 1");
//        $this->rmTranslations(array('mod_Catalog_hello'));


    }
}