<?php
/**
 * Created by PhpStorm.
 * User: taras
 * Date: 29.03.16
 * Time: 17:25
 */

namespace controllers\modules;
ini_set('max_execution_time', 10000000);
include_once DOCROOT . "vendor/acimage/AcImage.php";
class Resize
{
    private $path = "uploads/portfolio/";
    private $quality = 70;

    public function get()
    {
        $this->path = DOCROOT . $this->path;
        $i=0;
        if ($handle = opendir($this->path)) {
            while (false !== ($entry = readdir($handle))) {
                if ($entry != "." && $entry != ".." ) {
//                    if($i > 5) break;
//                    $i++;
                    $finishPath = $entry;
                   if($handle_1 =  opendir($this->path.'/'.$finishPath)) {
                       while (false !== ($img = readdir($handle_1))) {
                           if ($img != "." && $img != ".." ) {
                                $image = $this->path.$finishPath."/".$img;
//                                $image_r = $this->path.$finishPath."/r___".$img;
                               try{
                                   $img = \AcImage::createImage($image);
                                   \AcImage::setRewrite(true);
                                   \AcImage::setQuality($this->quality);

                                   $img->resizeByWidth(1000);
                                   $img->save($image);
                                   echo "$image<br>";
                               }
                               catch(\InvalidFileException $e) {
                                   echo 'Import error: ' . $e->getMessage() ;
                               }

                           }
                       }
                   }
                }
            }
            closedir($handle);
        }
    }
}