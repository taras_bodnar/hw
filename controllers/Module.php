<?php
/**
 * Company Otakoyi.com
 * Author: wg
 * Date: 02.01.15 17:23
 */

namespace controllers;

defined("SYSPATH") or die();

/**
 * Class Module
 * @package controllers
 */
abstract class Module extends App{

    protected $mModules;
    private $sql = array();
    private $ts = array();
    private $settings = array();

    public function __construct()
    {
        parent::__construct();

        $this->mModules = $this->load->model('engine\Modules');

        $token_exceptions = array(
            'controllers\modules\payment\LiqPay',
            'controllers\modules\Notify',
        );

        if($this->request->isPost() && !in_array(get_class($this), $token_exceptions)){
//            if( !isset($_POST['skey']) || $_POST['skey'] != SKEY) die('invalid token');
        }
    }

    abstract protected function install();
    abstract protected function uninstall();

    /**
     * @param $q
     * @return $this
     */
    protected function addSQL($q)
    {
        $this->sql[] = rtrim($q,';');
        return $this;
    }

    public function getSQL()
    {
        return $this->sql;
    }
    /**
     * @param $name
     * @param $value
     * @return $this
     */
    protected function addTranslation($name, $value)
    {
        $this->ts[$name] = $value;
        return $this;
    }

    protected function rmTranslations(array $t)
    {
        if(empty($t)) return 0;

        foreach ($t as $k=>$v) {
            $t[$k] = "'{$v}'";
        }

        $t = implode(',', $t);
        return $this->mModules->rmTranslations($t);
    }

    /**
     * @return array
     */
    public function getTranslations()
    {
        return $this->ts;
    }

    /**
     * @param $name
     * @param $value
     * @param $description
     * @return $this
     */
    protected function addSetting($name, $description = null, $value=null)
    {
        $this->settings[] = array(
            'name'        => $name,
            'value'       => $value,
            'description' => $description
        );

        return $this;
    }

    public function installSettings()
    {
        return $this->settings;
    }

    /**
     * return module settings
     * @param $name
     * @return mixed
     */
    protected final function getSettings($name)
    {
        $res = array();
        $s =  unserialize($this->mModules->getSettings($name));
        foreach ($s as $row) {
            $res[$row['name']] = $row['value'];
        }

        return $res;
    }

}