<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 06.05.14 22:34
 */
namespace controllers;

use controllers\engine\Admin;
use controllers\engine\Plugins;
use controllers\core\Controller;
use controllers\core\Languages;
use models\engine\content\Images;

if ( !defined("SYSPATH") ) die();

/**
 * Class Engine
 * @package controllers
 */
abstract class Engine extends Controller
{
    /**
     * static errors classes for bootstrap alert
     * @var array
     */
    protected $errors = array(
        'error' => 'error',
        'success' => 'success',
        'info' => 'info',
        'warning' => 'warning'
    );

    /**
     * all translations
     * @var array|null|\stdClass
     */
    protected $lang = array(); // тут будуть завантажені всі переклади і core і конктетного модуля
    /**
     * languages model
     * @var array
     */
    protected $languages = array();

    /**
     * @var int default language ID
     */
    protected $language_id = 1;

    protected $error = array();

    /**
     * content of body
     * @var string
     */
    private $content;
    /**
     * buttons
     * @var array
     */
    private $buttons = array();
    /**
     * right sidebar
     * @var
     */
    private $sidebar;
    /**
     * plugins list
     * @var
     */
    private $plugins;

    protected $images;

    public function __construct()
    {
        parent::__construct();

        // init languages
        $this->lang = Languages::instance()->getTranslations();
        $this->images = new Images();

        $this->languages = new \models\engine\Languages();
        $this->language_id = $this->languages->getFrontendDefault('id');
        if(!$this->request->engine_init){
            $this->init();
        }

        // права доступу

        $admin_rang = (int) Admin::data('rang');
//        $this->dump($admin_rang);die;
        if($admin_rang > 0 ) {

            $page_rang = (int) $this->request->rang;
            if($page_rang > $admin_rang){
                echo $this->load->view(403);
                die();
            }
        }
    }

    /**
     *
     */
    private function init()
    {
        // витягну назву контроллера
//        $request = Request::instance();
        //
        $this->request->mode = "engine"; // engine | app

        if($this->request->isXhr()) return ;

        // грузим структуру
        $st= $this->load->model('engine\Components');

        $n = $this->request->namespace;
        $c = $this->request->controller;

        $ci = $st->getControllerData($n.$c);

        $this->request->title =($c == 'Dashboard') ?  $this->sys_name :  $ci['name'] . ' :: ' . $this->sys_name;
        $this->request->name = $ci['name'];
        $this->request->description = $ci['description'];
        $this->request->icon = $ci['icon'];
        $this->request->rang = $ci['rang'];

        $this->request->breadcrumbs = null;
        $this->request->nav = $this->mainNav();

        $this->language_id = $this->languages->getBackendDefault('id');

        $this->request->engine_init = true;
    }

    /**
     * вантажить плагіни в залежності від action
     * @param $id
     * @param $action
     * @return string
     */
    protected function getPlugins($id, $action)
    {

        $plugins = new Plugins();
        $this->plugins = $plugins->get($id, $action);

        return $this->plugins;
    }

    private function breadcrumb($nc)
    {
        $bm = $this->load->model('engine\Breadcrumb');
        $id = $bm->controllerId($nc);

        return $this->load->view('breadcrumb', array('items' => $bm->crumbs($id)));
    }


    protected function setContent($content)
    {
        $this->content = $content;
        return $this;
    }

    protected function setButtons($buttons)
    {
        if(is_string($buttons)){
            $this->buttons = array($buttons);
        } else {
            $this->buttons = $buttons;
        }
        return $this;
    }

    /**
     * додає кнопку перед кнопками
     * @param $button
     * @return $this
     */
    protected function prependToButtons($button)
    {
        array_unshift($this->buttons, $button);
        return $this;
    }

    /**
     * додає кнопку в кінець кнопочок
     * @param $button
     * @return $this
     */
    protected function appendToButtons($button)
    {
        $this->buttons[] = $button;

        return $this;
    }

    protected function setSidebar($sidebar)
    {
        $this->sidebar = $sidebar;
        return $this;
    }

    /**
     * @return string
     */
    protected function output()
    {
        $this->request->mode = "engine";
        return $this->load->view('content',array(
            'title_block' => $this->load->view('title_block',array(
                    'title'       => $this->request->name,
                    'icon'       => $this->request->icon,
                    'description' => $this->request->description,
                    'buttons'     => $this->buttons
                )),
            'user_menu'   => $this->load->view('user_menu'),
            'breadcrumb'  => $this->breadcrumb($this->request->namespace . $this->request->controller),
            'sidebar'     => $this->sidebar,
            'content'     => $this->content,
//            'js_file'     => $this->getJs()
        ));
    }

    /**
     * @return string
     */
    private function mainNav()
    {
        $rang = (int) Admin::data('rang');
        $st = $this->load->model('engine\Components');

        return  $this->load->view(
            'main-nav',
            array(
                'dashInfo' => $st->itemInfo(1),
                'nav'      => $st->nav(1, $rang)
            )
        );
    }


    /**
     * Delete all files of content item
     * @param $path
     * @return bool
     */
    protected final function deleteFiles($path)
    {
        if (is_dir($path) === true)
        {
            $files = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($path), \RecursiveIteratorIterator::CHILD_FIRST);

            foreach ($files as $file)
            {
                if (in_array($file->getBasename(), array('.', '..')) !== true)
                {
                    if ($file->isDir() === true)
                    {
                        rmdir($file->getPathName());
                    }

                    else if (($file->isFile() === true) || ($file->isLink() === true))
                    {
                        unlink($file->getPathname());
                    }
                }
            }

            return rmdir($path);
        }

        else if ((is_file($path) === true) || (is_link($path) === true))
        {
            return unlink($path);
        }

        return false;
    }

    /**
     * @return mixed
     */
    abstract public function create();

    /**
     * @param $id
     * @return mixed
     */
    abstract public function edit($id);

    /**
     * @param $id
     * @return mixed
     */
    abstract public function process($id);

    /**
     * @param $id
     * @return mixed
     */
    abstract public function delete($id);
}
