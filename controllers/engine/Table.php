<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 07.06.14 22:29
 */

namespace controllers\engine;

use controllers\core\Controller;

defined('SYSPATH') or die();

/**
 * Class Table
 *$t = new Table('structure', "structure/items/$parent_id");
$t
// enable sorting ws drag & drop
->setConf('sortable', true)
// виключу сортування останнтьої колонки
->setConf('aoColumnDefs', array(
    'bSortable'=> false,
    'aTargets'=> [ -1 ]
))
->sortableConf('components','id')
->setTitle($this->lang->structure['table_title'])
->addTh($this->lang->structure['id'])
->addTh($this->lang->structure['name'])
->addTh($this->lang->structure['controller'])
->addTh($this->lang->structure['rang'])
->addTh($this->lang->core['func'] , 'w-160')
// enable checkboxes (group functions)
->setConf('groupFunctions', true)
->addGroupActionButton(
Form::button(
$this->lang->core['delete'],
Form::icon('icon-remove'),
array('class'=>'btn-danger', 'onclick' => 'engine.structure.delSelected();')
)
)
->addGroupActionButton(
Form::button(
$this->lang->core['published'],
Form::icon('icon-eye-open'),
array('class'=>'btn-primary', 'onclick' => 'engine.structure.delSelected();')
)
)
->addGroupActionButton(
Form::button(
$this->lang->core['unpublished'],
Form::icon('icon-eye-close'),
array('class'=>'btn-primary', 'onclick' => 'engine.structure.delSelected();')
)
)
;
 * ...
 * @package controllers\engine
 */
class Table extends Controller
{
    /**
     * set title of table
     * @var string
     */
    private $title;

    /**
     * @var array
     */
    private $config = array();

    /**
     * table ID
     * @var string
     */

    private $table_id;

    /**
     * table heading columns
     * @var array
     */
    private $th = array();

    /**
     * table row columns
     * @var array
     */
    private $td = array();
    /**
     * row of table body
     * @var array
     */
    private $tr = array();
    /**
     * @var array
     */
    private $language = array(
        'processing' => 'Processing',
        'empty_data' => 'Empty Data'
    );

    /**
     * server side params
     * @var array
     */
    private $ssParams = array();

    private $gaButtons = array();

    private $json_data = array();

    /**
     * data rows
     * @var array
     */
    private $rows=array();

    private $aaData = array();

    private $iFilteredTotal;
    private $iTotal;

    private $mt;

    private $iDisplayLength = 10;

    public function __construct( $id = 'table', $ajax_url = '')
    {
        parent::__construct();

        $this->table_id = $id;

        if(!empty($ajax_url)){
            $this->config = array_merge($this->config, array(
                'processing' => true,
                'serverSide' => true,
                'ajax'       => array(
                    'url'    => $ajax_url,
                    'type'   => 'POST'
                )
            ));
        }

        $this->mt = $this->load->model('engine\Table');
    }

    /**
     *
     * @param $status true | false
     * @return $this
     */
    public function debug($status)
    {
        $this->mt->debug($status);
        return $this;
    }

    public function addGroupActionButton($button)
    {
        if($this->getConf('groupFunctions')){
            $this->gaButtons[] = $button;
        }
        return $this;
    }

     /**
     * set title of table
     * @param $title
     * @return $this
     */
    public function setTitle($title)
    {

        $this->title = $title;

        return $this;
    }
    public function sortIndexCol($column)
    {
        $this->config['ajax']['data']['sortIndexCol'] = $column;
    }

    /**
     * set options for config
     * @param $key
     * @param $val
     * @return $this
     */
    public function setConf($key, $val)
    {
        $this->config = array_merge(
            $this->config,
            array( $key => $val )
        );

        return $this;
    }
    public function getConf($key)
    {
        if(isset($this->config[$key])) {
            return $this->config[$key];
        } else {
            return null;
        }
    }

    public function setDisplayLength($num)
    {
        $this->iDisplayLength = $num;

        return $this;
    }
    /**
     * def func
     */
    public function index(){}

    /**
     * set custom format of column
     * @param $index int index of column
     * @param $format string html format buttons or links etc.
     * @return $this
     */
    public function formatColumn($index, $format)
    {
        $this->td[$index] = $format;

        return $this;
    }

    /**
     * add table th
     * @param $name
     * @param string $class
     * @return $this
     */
    public function addTh($name, $class = '')
    {
        if(empty($this->th)) {
            if($this->getConf('groupFunctions')){
                $this->config['ajax']['data']['groupFunction'] = true;
                $this->th[] = array(
                    'name' => "<input type='checkbox' class='check-all'>",
                    'class' => 'w-20'
                );
            }
            if($this->getConf('sortable')){
                $this->config['ajax']['data']['sortable'] = true;
                $this->th[] = array(
                    'name' => "<i class='icon-reorder'></i>",
                    'class' => 'w-20'
                );
            }
        }

        $th =  array(
            'name' => $name,
            'class'   => $class
        );
        $this->th[] = $th;
        return $this;
    }

    /**
     * add row
     * @param $tr array list of td
     * @return $this
     */
    public function addRow($tr)
    {
        $_tr = array();

        foreach ($tr as $k => $td) {
            $_tr[] = "<td>{$td}</td>\r\n";
        }

        $this->tr[] = "<tr>". implode('', $_tr) ."</tr>\r\n";

        return $this;
    }
    /**
     * render table
     */
    private function tableBody()
    {
        $c =0; // count th
        $html = "<div class='panel panel-default panel-block'>";
        $html .= "<div id='data-table' class='panel-heading datatable-heading'>
                        <h4 class='section-title'>{$this->title}</h4>
                    </div>
                 ";
        $html .= "
                    <table class='table table-bordered table-striped' id='{$this->table_id}'>
                    <thead>
                    <tr>\r\n";

        foreach ($this->th as $th) {
            $class = empty($th['class']) ? '' : "class=\"{$th['class']}\"";
            $html .= "<th {$class}>{$th['name']}</th>\r\n";
            $c++;
        }

        $html .= "
                    </tr>
                </thead>
                ";
        if(!empty($this->tr)) {
            $html .= "<tbody>\r\n". implode('', $this->tr) ."</tbody>";
        } else {
            $html .= "
                <tbody>
                    <tr>
                        <td colspan=\"{$c}\" class=\"dataTables_empty\">{$this->language['empty_data']}</td>
                    </tr>
                </tbody>\r\n";
        }
        $html .= "</table>";

        return $html;
    }

    /**
     * render js file
     * @return string
     */
    private function js()
    {
        $sConf = $this->getSortableConf();
//        if(!$sConf) throw new \ErrorException('$sConf issues');
        $this->setConf('iDisplayLength', $this->iDisplayLength);
        $this->setConf('fnInitComplete','function(){
                $(".dataTables_wrapper select").select2({
                    dropdownCssClass: \'noSearch\'
                });
                if($(".check-all").length){
                    $(".check-all").change(function(){
                        if($(this).is(":checked")){
                            $(".dt-check").attr(\'checked\', true);
                        } else {
                            $(".dt-check").removeAttr(\'checked\');
                        }
                    });
                }

                $(\'.dataTables_wrapper\').find(\'input, select\').addClass(\'form-control\');
                $(\'.dataTables_wrapper\').find(\'input\').attr(\'placeholder\', engine.lang.table.quick_search);

                '. ((!empty($this->gaButtons)) ? '
                $(\'#'. $this->table_id .'_wrapper\')
                .find(\'.table-footer-row\')
                .append(\'<div class="col-sm-12 group-actions table-header-row hide">
                    <div class="btn-cnt"  style="padding:10px 0;">
                        '. implode('', $this->gaButtons) .'
                    </div>
                    <div class="clearfix"></div>
                </div>\');

                ' : '') .'

                /* Sortable */

                '. (($this->getConf('sortable')) ? '
                    $("#'. $this->table_id .' .icon-reorder").each(function(i,e){
                        $(this).parents("tr").attr("id", $(e).attr("id"));
                    });
                    $("#'. $this->table_id .' tbody").sortable({
                        handle: ".icon-reorder",
                        update: function(event, ui) {
                            var newOrder = $(this).sortable(\'toArray\').toString();
                            $.get(\'table/sort/'. $sConf['table'] .'/'. $sConf['pk'] .'/'. $sConf['col'] .'/\'+newOrder);
                            if($("#tree").length) {
                              $("#tree").jstree("refresh");
                            }
                        }
                    });
                ': '') .'
            }');


        // disable sorting columns
        $sort=array();
        for($i=0;$i<count($this->th); $i++) {
            $sort[] = null;
        }


        if($this->getConf('groupFunctions')){
            $sort[0] = array('bSortable' => false);
            $this->sOrder(0,'asc');
        }

        $this->setConf('aoColumns', $sort);


        $config = json_encode($this->config);
        $config = str_replace(
            array(
            '\n', '\"', '\/', '"function', '}"'
            )
            ,array(
            '',   '"' , '/',  'function',  '}'
            ),
            $config
        );
        return "
        <script>
        $(document).ready(function() {
            $('#{$this->table_id}').dataTable({$config});
        });
        </script>";
    }

    public function sort($table, $pk, $col='sort', $sort)
    {
//        echo $table,'|', $pk,'|', $col;        die('oki');

        $mt = $this->load->model('engine\Table');

        $sort = explode(',',$sort);
        $i=1;
        foreach($sort as $k=>$id){
            $mt->sort($table, $pk, $col, $i, $id);
            $i++;
        }
        return $i;
    }

    /**
     * set sorting order
     * @param $indexCol
     * @param string $order
     * @return $this
     */
    public function sOrder($indexCol, $order='asc')
    {
        return $this->setConf('order', array($indexCol, $order));
    }

    public function render()
    {
        return $this->tableBody() . $this->js();
    }

    public function sortableConf($table, $pk, $col = 'sort')
    {
        $this->config['ajax']['data']['sortIndexCol'] = $col;
        $this->config['sSortConf'] = array(
            'table' => $table,
            'pk'    => $pk,
            'col'   => $col
        );

        return $this;
    }

    private function getSortableConf(){
        return isset($this->config['sSortConf']) ? $this->config['sSortConf'] : null;
    }


//------------------------ \ REQUEST METHODS \ ---------------------

    public function table($table)
    {
        $this->ssParams['table'] = $table;
        return $this;
    }
    public function where($where)
    {
        if(!empty($where)) {
            $this->ssParams['sWhere'][] = $where;
        }
        return $this;
    }
    public function join($join)
    {
        $this->ssParams['join'][] = $join;
        return $this;
    }
    public function columns(array $columns)
    {
        $this->ssParams['iSortCol'] = $columns;
        return $this;
    }

    public function searchCol(array $columns)
    {
        $this->ssParams['sSearchCol'] = $columns;
        return $this;
    }
    public function returnCol(array $columns)
    {
        $this->ssParams['outputCol'] = $columns;
        return $this;
    }
    public function formatCol(array $columns)
    {
        $this->ssParams['formatCol'] = $columns;
        return $this;
    }

    private function query()
    {

        $table    = $this->ssParams['table'];
        $join    = isset($this->ssParams['join']) ? $this->ssParams['join'] : array();
        $iSortCol = $this->ssParams['iSortCol'];
        $sSearchCol = $this->ssParams['sSearchCol'];



        if(!$iSortCol) throw new \Exception('iSortCol issues');

        // зміщую ключі якщо включені групові функції або сортування
        if(isset($_POST['groupFunction'])){
            $iSortCol = array_merge((array) 0, $iSortCol );
        }
        if(isset($_POST['sortable'])){
            $iSortCol = array_merge((array) 0, $iSortCol );
        }
//        $this->dump($iSortCol);

        $sLimit = "";
        if(isset($this->ssParams['sWhere'])) {
            $sWhere = 'where ' . implode(' AND ', $this->ssParams['sWhere']);
        } else {
            $sWhere = "where 1 ";
        }

        if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ){
            $sLimit = "LIMIT ". $_POST['start'].", ".
                $_POST['length'] ;
        }
        /*
         * sorting
         */
        $sOrder = "";
        if ( isset( $_POST['order'][0]['column'] ) ){

            if($_POST['order'][0]['column'] == 0){
                // todo sortIndexCol _POST
                $sort = isset($_POST['sortIndexCol']) && !empty($_POST['sortIndexCol']) ? $_POST['sortIndexCol'] : 'id';
                $sOrder = "ORDER BY  ABS($sort)";
                // todo пофіксати сортування по індексу
            }else {

                if(isset($_POST['groupFunction'])) {
                    $_POST['order'][0]['column'] --;
                }

                $_POST['order'][0]['column'] --;

                if(isset($outputCol[$_POST['order'][0]['column']]) && $outputCol[$_POST['order'][0]['column']] != 'func') {

                    $sOrder = "ORDER BY  ";
                    $sOrder .=  $outputCol[$_POST['order'][0]['column']] ;
                }
            }

            if(isset($_POST['order'][0]['dir']) && !empty($sOrder)){
                $sOrder .= ' ' . $_POST['order'][0]['dir'];
            }
        }

        /*
         * Search
         * search[value]
         * */
        if (isset($_POST['search']) && $_POST['search'] != "" && !empty($sSearchCol)){
            $sq = $_POST['search']['value'];
            if(strlen($sq) >= 3) {
                $_sWhere=array();
                foreach ($sSearchCol as $k=>$col) {
                    $_sWhere[] = "($col like '%{$sq}%')";
                }
                $sWhere .= " and (". implode(' OR ', $_sWhere) .")";
            }
        }

        $this->rows = $this->mt->rows($iSortCol, $table , $join, $sWhere, $sOrder, $sLimit);

//        print_r($this->rows);
        // found rows in query
        $this->iFilteredTotal = $this->mt->foundRows();
        // total records
        $this->iTotal = $this->mt->total($table, $join, $sWhere);
    }

    public function request()
    {
        // Table model
        $outputCol = isset($this->ssParams['outputCol']) ? $this->ssParams['outputCol'] : array();
//        if(!$outputCol) throw new \Exception('outputCol issues');
//        $mt = $this->load->model('engine\Table');
        $formatColumns = isset($this->ssParams['formatCol']) ? $this->ssParams['formatCol'] : array();
        /*
         * Output
        */
        $output = array(
            'draw'=> (int) $_POST['draw'],
            "aaData" => array()
        );


            if(empty($this->aaData)){
                $this->query();
                if(is_array($this->rows)) {
                    foreach ($this->rows as $aRow) {
                        $row = array(); // clear row
                        if(isset($_POST['groupFunction'])){
                            $row[] = "<input type='checkbox' class='dt-check' name='check[]' value=\"{$aRow['id']}\">\r\n";
                        }
                        if(isset($_POST['sortable'])){
                            $row[] = "<i id='{$aRow['id']}' style='cursor: move;opacity:0.5' class='icon-reorder'></i>\r\n";
                        }

                        foreach($outputCol as $k => $col) {
                            $col = preg_replace('/^[a-z]\./u', '', $col);
                            if(isset($formatColumns[$k])){
                                $row[] = preg_replace_callback(
                                    '/{([a-z_]+)}/u',
                                    function ($matches) use ($aRow) {
                                        $str = $matches[1];
//                                echo $str, ' | ';
                                        if(isset($aRow[$matches[1]])) {
                                            $str = $aRow[$matches[1]];
                                        }
                                        return $str;
                                    },
                                    $formatColumns[$k]
                                );
//                        $row[] = str_replace("{".$col."}", $col, $formatColumns[$k]);
                            } else {
                                if(isset($aRow[$col])) {
                                    $row[] = $aRow[$col];
                                }

                            }
                        }
                        $output['aaData'][] = $row;
                    }
                }

            } else {
                $output['aaData'] = $this->aaData;
            }
        $output['recordsTotal'] = $this->iTotal;
        $output['recordsFiltered'] = $this->iFilteredTotal;

        $error = $this->mt->error();
        if(!empty($error)) echo $error;
        return json_encode( $output );
    }

    /**
     * format output data
     * @param $aaData
     * @return $this
     */
    public function beforeRender($aaData)
    {
        $this->aaData = $aaData;
        return $this;
    }

    /**
     * return db selected rows
     * @return array
     */
    public function getRows()
    {
        $this->query();
        return $this->rows;
    }

    /**
     * @param array $data
     * @return string
     */
    public function renderJSONData(array $data)
    {
        $_data = array(); $draw= count($data); $recordsTotal = $draw; $recordsFiltered = $draw;

        foreach ($data as $row) {
            $_data[] = array_values($row);
        }

        return json_encode(
            array(
                'draw' => $draw,
                'recordsTotal' => ''. $recordsTotal,
                'recordsFiltered' => '' . $recordsFiltered,
                'aaData' => $_data
            )
        );
    }

} 