<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 26.06.14 12:58
 */

namespace controllers\engine;

use controllers\Engine;

defined('SYSPATH') or die();

class Settings extends Engine {
    /**
     * settings model
     * @var mixed
     */
    private $mSettings;

    public function __construct()
    {
        parent::__construct();

        $this->mSettings = $this->load->model('engine\Settings');
    }



    /**
     * @return mixed
     */
    public function index(){
        $buttons= array(
            Form::button(
                $this->lang->core['save'],
                Form::icon('icon-save', false),
                array(
                    'class'=>'btn-success form-submit'
                ))
        );
        $data = array();
        $settings = $this->mSettings->get();
        foreach ($settings as $row) {
            $data[$row['name']] = $row;
        }


        $content = $this->load->view(
            'settings',
            array(
                'data' => $data
            )
        );

        $this->setButtons($buttons);
        $this->setContent($content);

        return $this->output();
    }
    public function update()
    {
        if(isset($_POST['data'])){
            foreach ($_POST['data'] as $name=>$value) {
                $this->mSettings->set($name, $value);
            }
        }

        return json_encode(array(
            's' => 1 , // status
            'r' => '', // redirect url
            'e' => 'success', // error class
            't' => $this->lang->settings['e_title'], // error title
            'm' => $this->lang->settings['e_msg'] // error message
        ));
    }

    /**
     * @return mixed|void
     */
    public function create(){}

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id){}


    /**
     * @param $id
     * @return mixed
     */
    public function delete($id){}
    public function process($id){}
}