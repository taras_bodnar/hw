<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 31.05.14 18:36
 */

namespace controllers\engine;

defined('SYSPATH') or die();

/**
 * Class manufacturers
 * @package controllers\engine
 */
class Manufacturers extends Content implements Component  {

    public function __construct()
    {
        parent::__construct();

        // встановлюю тип контенту сторінка
        $this->setType('manufacturer');
    }
    
    public function install()
    {
        return 1;
    }
    public function uninstall()
    {
        return 1;
    }



    /**
     * список сторінок
     * @param string $parent_id
     * @return mixed|string
     */
    public function index($parent_id = '')
    {
        $this->setButtons
            (
                Form::button(
                    $this->lang->core['create'],
                    Form::icon('icon-file'),
                    array(
                        'class'   => Form::BTN_TYPE_PRIMARY,
                        'onclick' => "self.location.href='manufacturers/create/{$parent_id}'"
                    ))
            );

        $t = new Table('manufacturers', "manufacturers/items/$parent_id");
        $t
           ->setConf('sortable', true)
           ->setConf('columns', array(
                'orderable'=> false,

            ))
           ->sortableConf('content','id')
           ->setTitle($this->lang->manufacturers['table_title'])
           ->addTh($this->lang->content['id'], 'w-20')
           ->addTh($this->lang->content['name'])
           ->addTh($this->lang->core['func'] , 'w-180')
        ;

        $this->setContent($t->render());

        return $this->output();
    }

    /**
     * ajax response tale items
     * @param int $parent_id
     * @return string
     */
    public function items($parent_id=0)
    {

        $dt = new Table();
        return $dt
            -> table('content c')
            -> where("c.parent_id=$parent_id and c.type_id={$this->type_id}")
            -> join("join content_info i on i.content_id = c.id and i.languages_id = {$this->language_id}")
            -> columns(
                array(
                    'c.id',
                    'i.name',
                    'c.published',
                    'CONCAT("manufacturers/", IF(c.isfolder, "index/", "edit/"), c.id) as href'
                )
            )
            -> searchCol(array('c.id','i.name'))
            -> returnCol(array('c.id','name' , 'func'))
            -> formatCol(
                array(
                1 => '<a href="{href}">{name}</a>',
                2 => Form::link(
                        '',
                        Form::icon('icon-eye-{published}'),
                        array(
                           'class'    =>'btn-primary',
                            'title'   => $this->lang->core['published_tip'],
                            'onclick' => 'engine.manufacturers.pub({id},{published})'
                        )
                    ) .
                    Form::link(
                        '',
                        Form::icon('icon-edit'),
                        array(
                           'class'  =>'btn-info',
                            'title' => $this->lang->core['edit'],
                            'href'  => 'manufacturers/edit/{id}'
                        )
                    ) .
                    Form::button(
                        '',
                        Form::icon('icon-remove'),
                        array(
                            'title'   => $this->lang->core['delete'],
                            'class'   =>'btn-danger',
                            'onclick' => 'engine.manufacturers.delete({id})'
                        )
                    )
            ))
            -> request();
    }

    /**
     * auto crate manufacturer id vs default params
     * @param int $parent_id
     * @return mixed|string
     */
    public function create($parent_id=0)
    {
        return parent::create($parent_id);
    }

    /**
     * edit manufacturer
     * @param $id
     * @return mixed|string
     */
    public function edit($id)
    {
        # 1. Встановлюю які поля потрібно відображати у формі

        # 2. Заповнюю контент даними по замовчуванню
        parent::edit($id);

        // тайтл форми
        $this->formAppendData('title', $this->lang->manufacturers['title']);
        # 2.a Додаю кнопочку назад

        $this->prependToButtons(
            Form::button(
                $this->lang->core['back'],
                Form::icon('icon-external-link'),
                array(
                    'class'=>'btn-link',
                    'onclick' => 'self.location.href=\'manufacturers\''
                )
            )
        );

        # 3. Додаю параметри для форми

        $this->formAppendData('form_action',       "manufacturers/process/$id")
             ->formAppendData('redirect_url',     'manufacturers');


        # 4. Генерую контент і виводжу на екран

        return $this->renderContent();
    }


    /**
     * @param $id
     * @return mixed|string
     */
    public function process($id)
    {
        parent::process($id);

        $this->saveNavMenus($id);

        return json_encode($this->getFormResponse());
    }

    /**
     * видалення сторінки
     * @param $id
     * @return mixed 1
     */
    public function delete($id)
    {
        return parent::delete($id);
    }

    /**
     * @return string
     */
    public function tree()
    {
        $parent_id = isset($_POST['id']) ? (int)$_POST['id'] : 0;
        $tree=array();
        $r = $this->ms->tree($parent_id, $this->type_id);
        $i=0;
        if(!empty($r)){
            foreach ( $r as $row) {
                $tree[$i]['data'] = $row['name'] . ' #' . $row['id'] ;
                $tree[$i]['state'] = $row['isfolder'] ? 'closed' : '';
                $tree[$i]['attr'] = array(
                    'id' => $row['id'],
                    "rel"=> (($row['isfolder'])? 'folder':'file'),
                    "href"=>   './manufacturers/'. (($row['isfolder'])? 'index' : 'edit') ."/" . $row['id'] ."/"
                );
                $i++;
            }
        }

        return json_encode($tree);
    }

    /**
     * return controller content
     * @return string
     */
    protected function output()
    {
        return parent::output();
    }
}
