<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wg.
 * Date: 31.05.14 18:36
 */
namespace controllers\engine;

use controllers\core\DB;
use controllers\Engine;

defined('SYSPATH') or die();

class Messages extends Engine {

    private $model;
    private $db;

    public function __construct()
    {
        parent::__construct();

        $this->model = new \models\engine\Messages();
        $this->db = DB::instance();

    }
    /**
     * @return mixed|string
     */
    public function index()
    {
        $t = new DataTables();
        $t ->setId('messages')
            ->setTitle('Список всіх діалогів')
            ->setConfig('order', array(array(0,'desc')))
            ->ajaxConfig('Messages/items')
            ->th('From')
            ->th('To')
            ->th('Message')
            ->th($this->lang->core['func'] , 'w-180');

        $this->setContent($t->render());

        return $this->output();
    }

    /**
     * messages list
     * @return string
     */
    public function items()
    {
        $dt = new DataTables();
        $dt
            ->table('messages m')
            ->join(" join messages mm on mm.from_id!=m.to_id")
            ->join(" join users f on f.id=m.from_id")
            ->join(" join users t on t.id=m.to_id")
//            ->debug(true)
            ->get(
                array(
                    'm.from_id',
                    'm.to_id',
                    "DATE_FORMAT(m.createdon, '%d.%m.%y') as dt",
                    "CONCAT_WS(' ',f.name,f.surname) as from_name",
                    "CONCAT_WS(' ',t.name,t.surname) as to_name",
                    "m.message",
                )
            )
            ->orderBy("m.createdon desc")
            -> groupBy("m.from_id")
            -> searchCol("m.message,f.name,f.surname,t.name,t.surname, m.from_id, m.to_id")
            -> execute();

        $out = array();
        foreach ($dt->getResults(false) as $item) {
            $out[] = array(
            $item['from_name'],
            $item['to_name'],
            $item['message'],
                Form::link(
                    '',
                    Form::icon('icon-file'),
                    array(
                        'class'    =>'btn-info',
                        'title'   => 'Preview',
                        'href' => 'messages/view/'.$item['from_id'].'/'.$item['to_id']
                    )
                )
            );
        }

        return $dt->renderJSON($out, $dt->getTotal());
    }

    public function edit($id)
    {
        // TODO: Implement edit() method.
    }

    public function view($from, $to)
    {
        $msgs = $this->db->select("SELECT m.*, u.name, u.surname FROM `messages` m 
                                   JOIN users u ON u.id = m.from_id
                                   WHERE (m.from_id = $from AND m.to_id = $to) OR (m.from_id = $to AND m.to_id = $from) 
                                   ORDER BY m.createdon ASC")
                                  ->all();

        $this->setContent($this->load->view('messages/list', array(
            'msgs' => $msgs
        )));

        return $this->output();
    }

    public function create(){}


    public function delete($id)
    {
        return $this->model->delete($id);
    }

    public function process($id)
    {
        // TODO: Implement process() method.
    }

    public function install()
    {
        return 1;
    }
    public function uninstall()
    {
        return 1;
    }
}
