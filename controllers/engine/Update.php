<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 18.10.14 22:31
 */

namespace controllers\engine;

use controllers\Engine;
use controllers\core\Settings;

defined("SYSPATH") or die();

class Update extends Engine{

    private $host = "http://svn.engine.loc";
    private $version ='';




    public function check()
    {

        $version_update = Settings::instance()->get('version_update');

        if($version_update == 0) return '';

        $data = file_get_contents($this->host);

        if(empty($data)) return '';

        $data = json_decode($data);

        $current_version =  Settings::instance()->get('version');

        if (version_compare($current_version, $data->version, '>=') == true) {
            return '';
        }

        return $this->load->view(
            'updates/alert',
            array(
                'e' => $data
            )
        );
    }

    public function get()
    {
        $v = Settings::instance()->get('version');
        $h = $_SERVER['REMOTE_ADDR'];
        $d = $_SERVER['HTTP_HOST'];
        $url = "{$this->host}?a=get&v={$v}&h=$h&d=$d";

        $source = file_get_contents($url);
        $ext = '.zip';

        $fname =  md5(time());

        $dir = $_SERVER['DOCUMENT_ROOT'] . '/tmp/';
//        chmod($dir, 0777);
        if(empty($source)) return 0;

        // записую архів

        if(!file_put_contents($dir . $fname . $ext, $source)) return 0;

        if(!$this->extractZip($dir . $fname . $ext, $dir . $fname)) return 0;

        $this->manualInstall( $dir . $fname );

        $t = 'Оновлення встановлено';
        $m = 'Жодних проблем при встановленні оновлення не виявлено. Бажаємо приємного дня.';
        $e = 'success';

        if(!empty($this->error)){
            $t = 'При встановленні оновлення виникли помилки.';
            $m = 'Деталіпомилки:<br>' . implode('<br>', $this->error);
            $e = 'danger';
        }

        return $this->load->view(
            'updates/errors',
            array(
                't' => $t,
                'e' => $e,
                'm' => $m
            )
        );
    }

    private function manualInstall($path)
    {
        $composer = $path . '/composer.json';
        if(!file_exists($composer)) return 0;

        $mu = $this->load->model('engine\Update');

        $data = file_get_contents($composer);
        $data = json_decode($data);
//        $this->dump($data);

        // exec sql data

        $mu->execSQL($data->sql);
        // todo пофіксати права доступу на файли
        $this->copyFiles($path, $data->files);
        // todo і видалити тимасові файли

        $this->deleteDirectory($path);
        $this->deleteZip($path);

        // оновлюю версію
        $mu->setVersion($data->version);
        return 1;
    }

    private function deleteZip($path)
    {
        return unlink($path.'.zip');
    }
    /**
     * recursive delete directory
     * @param $dir
     * @return bool
     */
    private function deleteDirectory($dir) {
        if (!file_exists($dir)) {
            return true;
        }

        if (!is_dir($dir)) {
            return unlink($dir);
        }

        foreach (scandir($dir) as $item) {
            if ($item == '.' || $item == '..') {
                continue;
            }

            if (!$this->deleteDirectory($dir . DIRECTORY_SEPARATOR . $item)) {
                return false;
            }

        }

        return rmdir($dir);
    }

    private function copyFiles($zip_path, $files)
    {
        $docroot = $_SERVER['DOCUMENT_ROOT'] . '/';
        // визначаю актуальну версію
        $current_version =  Settings::instance()->get('version');

        // бекап файлів
        $backup_dir = $docroot . 'tmp/' . $current_version .'/' ;

        // створюю папку 
        if(!is_dir($backup_dir)){
            mkdir($backup_dir, 0777);
        }

        foreach ($files as $k=>$file) {
            $file_info = pathinfo($backup_dir . $file);

            if(!is_dir($file_info['dirname'])){
                if(!mkdir($file_info['dirname'], 0755, true)) {
                    $this->error[] = 'Не можу створити директорію ' . $file_info['dirname'];
                    break;
                }
            }

            if( file_exists($docroot . $file) && !file_exists( $backup_dir . $file)) {
                copy( $docroot . $file, $backup_dir . $file);
            }

            if(file_exists($docroot . $file)){
                unlink($docroot . $file);
            }

            copy($zip_path .'/'. $file, $docroot . $file);
        }
    }

    private function extractZip( $file = '', $path = '' )
    {
        $zip = new \ZipArchive;
        if ($zip->open($file) === TRUE) {
            $zip->extractTo($path);
            $zip->close();
            return 1;
        }

        return 0;
    }

    public function disable()
    {
        return Settings::instance()->set('version_update', 0);
    }

    public function index(){}
    public function create(){}
    public function edit($id){}
    public function process($id){}
    public function delete($id){}
}