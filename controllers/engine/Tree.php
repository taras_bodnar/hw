<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 01.06.14 10:04
 */

namespace controllers\engine;

use controllers\Engine;
use controllers\core\Load;
use controllers\core\Request;

defined('SYSPATH') or die();

class Tree  {

    /**
     * @var string table
     */
    private $table;
    /**
     * ajax url
     * @var string
     */
    private $ajax_url = '';
    /**
     * @var string title of sidebar
     */
    private $title;
    /**
     * @var string icon
     */
    private $icon;
    /**
     * @var string button
     */
    private $button;

    /**
     * @var bool use search
     */
    private $search;

    /**
     * @var object Load object
     */
    private $load;

    /**
     * context menu items
     * @var array
     */
    private $contextmenu = array();

    /**
     * @param string $table
     * @param string $ajax_url
     * @param string $button
     * @param array $contextmenu
     * @param bool $search
     */
    public function __construct($table ='', $ajax_url ='', $button = '', $contextmenu = array() , $search = false)
    {
        $this->table = $table;
        $this->ajax_url = $ajax_url;
        $this->load = Load::instance();
        $this->title  = Request::instance()->name;
        $this->icon   = Request::instance()->icon;
        $this->button = $button;
        $this->search = $search;
        $this->contextmenu = $contextmenu;
    }

    /**
     * @param $name
     * @param string $icon
     * @param string $action
     * @return string
     */
    public static function button($name,$action, $icon = '')
    {
        $icon = $icon == '' ? '' : "<i class='$icon'></i>";
        $action =  "onclick='$action'";

        return "<a class='add' href='javascript:void(0);' $action>$icon $name</a>";
    }

    /**
     * Create Context Menu action
     * @param $name
     * @param $icon
     * @param $action string use id id need
     * @return array
     */
    public static function contextMenu($name, $icon, $action)
    {
        return array(
            'label'=> $name,
            'icon' => $icon,
            'action'=> "function(o) { var id = o.attr('id'); $action}"
        );
    }

    public function move($table, $id, $from, $to)
    {
        $from = (int)$from;
        $r=0;

        if($id > 0 && !empty($table)) {
            $mt = $this->load->model('engine\Tree');

            $r = $mt->moveToParent($table, $id, $to);
            if( $r > 0 && $from > 0){
                $r+= $mt->checkIsFolderPrevParent($table, $from);
            }
            if( $r > 0 && $table == 'content') {
                $this->regenerateAlias($id);
            }
        }

        echo $r;
    }

    /**
     * @param $id
     * @return int
     */
    public function regenerateAlias($id)
    {
//        echo 'c'.$id, '+';
        $languages = $this->load->model('engine\Languages');
        $mContent = $this->load->model('engine\Content');
        $parent_id = $mContent->data('content', $id, 'parent_id');
        $isfolder = $mContent->data('content', $id, 'isfolder');
        $content = new Content();
        $r = $languages->all(1);
        foreach ($r as $lng) {
            $name = $mContent->getRowInfo($id, $lng['id'], 'name');
            $alias = $content->mkAlias($parent_id, $lng['id'], $lng['code'], $name);
            $mContent->updateAlias($id,$alias);
        }

        if($isfolder){
            // get children
            $children = $mContent->getChildren($id);
            foreach ($children as $child) {
                $this->regenerateAlias($child['id']);
            }

        }

        return 1;
    }



    /**
     * render tree
     * @return string
     */
    public function render()
    {
        //http://stackoverflow.com/questions/14959988/search-jstree-nodes-using-jquery-javascript
        return $this->load->view('sidebar',array(
            'table'  => $this->table,
            'ajax_url'  => $this->ajax_url,
            'title'     => $this->title,
            'icon'  => $this->icon,
            'button' => $this->button,
            'search' => $this->search,
            'contextmenu' => $this->contextmenu
        ));
    }

    public function __toString()
    {
        return $this->render();
    }

}