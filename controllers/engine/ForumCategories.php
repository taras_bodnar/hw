<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author Taras Bodnar.
 * Date: 07.03.16 17:34
 */

namespace controllers\engine;

use controllers\core\Settings;

defined('SYSPATH') or die();

/**
 * Class manufacturers
 * @package controllers\engine
 */
class ForumCategories extends Content {

    public function __construct()
    {
        parent::__construct();

        // встановлюю тип контенту
        $this->setType('ForumCategories');
    }



    /**
     * список сторінок
     * @param string $parent_id
     * @return mixed|string
     */
    public function index($parent_id = ''){}

    /**
     * auto crate manufacturer id vs default params
     * @param int $parent_id
     * @return mixed|string
     */
    public function create($parent_id=0)
    {
        return parent::create($parent_id, false);
    }

    /**
     * edit manufacturer
     * @param $id
     * @return mixed|string
     */
    public function edit($id)
    {
        $languages = $this->load->model('engine\Languages');
        $data = $this->ms->data('content', $id);
        return $this->load->view(
            'forum/category',
            array(
                'id' => $id,
                'data' => $data,
                'info' => $this->ms->info($id),
                'parent_id' => $data['parent_id'],
                'created' => 0,
                'languages'  => $languages->all(1),
                'autofil_title'       => Settings::instance()->get('autofil_title'),
                'autotranslit_alias'  => Settings::instance()->get('autotranslit_alias')
            )
        );
    }


    /**
     * @param $id
     * @return mixed|string
     */
    public function process($id)
    {
        parent::process($id);

        return json_encode($this->getFormResponse());
    }

    /**
     * видалення сторінки
     * @param $id
     * @return mixed 1
     */
    public function delete($id)
    {
        return parent::delete($id);
    }

    /**
     * @return string
     */
    public function tree()
    {
        $parent_id = isset($_POST['id']) ? (int)$_POST['id'] : 0;
        $tree=array();
        $r = $this->ms->tree($parent_id, $this->type_id);
        $i=0;
        if(!empty($r)){
            foreach ( $r as $row) {
                $tree[$i]['data'] = $row['name'] . ' #' . $row['id'] ;
                $tree[$i]['state'] = $row['isfolder'] ? 'closed' : '';
                $tree[$i]['attr'] = array(
                    'id' => $row['id'],
                    "rel"=> (($row['isfolder'])? 'folder':'file'),
                    "href"=>   './manufacturers/'. (($row['isfolder'])? 'index' : 'edit') ."/" . $row['id'] ."/"
                );
                $i++;
            }
        }

        return json_encode($tree);
    }

    /**
     * return controller content
     * @return string
     */
    protected function output()
    {
        return parent::output();
    }
}
