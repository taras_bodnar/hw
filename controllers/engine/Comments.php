<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wg.
 * Date: 31.05.14 18:36
 */
namespace controllers\engine;

use controllers\Engine;

defined('SYSPATH') or die();

class Comments extends Engine {

    private $model;

    public function __construct()
    {
        parent::__construct();

        $this->model = new \models\engine\Comments();

    }
    /**
     * @return mixed|string
     */
    public function index()
    {
        $t = new DataTables();
        $t ->setId('comments')
           ->setTitle('Список всіх коментарів')
           ->setConfig('order', array(array(0,'desc')))
           ->ajaxConfig('Comments/items')
           ->th('#')
           ->th('Автор')
           ->th('Коментар')
           ->th($this->lang->core['func'] , 'w-180');

        $this->setContent($t->render());

        return $this->output();
    }

    /**
     * comments list
     * @return string
     */
    public function items()
    {
        $dt = new DataTables();
        $dt
            -> table('comments c')
            -> join("join users u on u.id = c.users_id")
            -> join("join users w on w.id = c.workers_id")
            -> get(
                array(
                    'c.id',
                    'c.status',
                    'c.users_id',
                    'c.workers_id',
                    'CONCAT(u.name , " " , u.surname) as uname',
                    'u.email as uemail',
                    'CONCAT(w.name , " " , w.surname) as wname',
                    'w.email as wemail',
                    'c.message',
                    'c.ip',
                    "DATE_FORMAT(c.createdon, '%d.%m.%y') as dt",
                    "DATE_FORMAT(c.createdon, '%H:%i') as hi"
                )
            )
            -> searchCol("c.message, u.name, u.surname, u.email, w.name, w.surname, w.email")
            -> execute();

        $out = array();
        foreach ($dt->getResults(false) as $i=>$row) {
//            $res[$i][] = $row['id'];
//            $res[$i][] = "<a href='Customers/edit/{$row['users_id']}'>{$row['uname']}</a>";
//            $res[$i][] = "<a class='btn btn-primary' href='{$row['id']}'><i class='icon-edit icon-white'></i></a>";

            $out[] = array(
                $row['id'],
                "{$row['uname']}<br><a href='mailto:{$row['uemail']}'>{$row['uemail']}</a>"
                . (!empty($row['ip']) ? "<br><span class='text-muted'>{$row['ip']}</span>" : ''),
                "<span class='small'>{$row['message']}</span>
                 <br><small>Коментар додано <span class='text-info'>{$row['dt']} в {$row['hi']}</span> для {$row['wname']} {$row['wemail']}</small>
                ",
                Form::link(
                    '',
                    Form::icon('icon-eye-'. ($row['status'] == 'approved' ? 'open' : 'close')),
                    array(
                        'class'    =>'btn-primary',
                        'onclick' => 'engine.comments.pub('.$row['id'].','. ($row['status'] == 'approved' ? 1 : 0) .')'
                    )
                ) .
                /*Form::link(
                    '',
                    Form::icon('icon-reply'),
                    array(
                        'title' => 'Відповісти',
                        'class'    =>'btn-primary',
                        'onclick' => 'engine.comments.reply('.$row['id'].')'
                    )
                ) .*/
                Form::button(
                    '',
                    Form::icon('icon-remove'),
                    array(
                        'title'   => $this->lang->core['delete'],
                        'class'   =>'btn-danger',
                        'onclick' => 'engine.comments.delete('.$row['id'].')'
                    )
                )
            );

        }

        return $dt->renderJSON($out, $dt->getTotal());
    }

    public function edit($id)
    {

    }


    public function create(){}

    public function delete($id)
    {
        return $this->model->delete($id);
    }

    /**
     * publish comment
     * @param $id
     * @param $published
     * @return mixed
     */
    public function pub($id, $published)
    {
        $published = $published == 1 ? 0 : 1;
        $s = $this->model->pub($id, $published);
        return $s;
    }

    /**
     * reply to comment
     * @param $id
     * @return string
     */
    public function reply($id)
    {
        return $this->load->view('comments/reply',array('parent_id'=>$id));
    }

    public function process($id)
    {
        if(empty($_POST['data']['message'])) return '';

        $info = $this->model->getData($id);

        $data = array(
            'parent_id'  => $id,
            'content_id' => $info['content_id'],
            'pib'        => Admin::data('name'),
            'email'      => Admin::data('email'),
            'message'    => strip_tags($_POST['data']['message']),
            'published'  => 1,
            'createdon'  => date('Y-m-d H:i:s')

        );
        $comment_id = $this->model->create($data);
        return json_encode(array(
            's' => $comment_id > 0 ? 1 : 0
        ));
    }
    public function install()
    {
        return 1;
    }
    public function uninstall()
    {
        return 1;
    }


}
