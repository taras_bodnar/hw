<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wg.
 * Date: 31.05.14 18:36
 */

namespace controllers\engine;

defined('SYSPATH') or die();

/**
 * Class Posts
 * @package controllers\engine
 */
class Travel extends Content {

    private $cat = array();
    protected $parent_id='';

    public function __construct()
    {
        parent::__construct();

        $this->setType('Travel');
    }

    public function install(){return 1;}
    public function uninstall(){return 1;}
    
    /**
     * список сторінок
     * @param string $parent_id
     * @return mixed|string
     */
    public function index($parent_id = '')
    {
        $this->setButtons
            (
                Form::button(
                    $this->lang->core['create'],
                    Form::icon('icon-file'),
                    array(
                        'class'   => Form::BTN_TYPE_PRIMARY,
                        'onclick' => "self.location.href='Travel/create/{$parent_id}'"
                    ))
            );

        $t = new Table('Travel', "Travel/items/$parent_id");
        $t
           ->setConf('sortable', true)
           ->setConf('columns', array(
                'orderable'=> false,
            ))
           -> sortableConf('content','id')
           -> setTitle($this->lang->Travel['table_title'])
           -> addTh($this->lang->content['id'], 'w-20')
           -> addTh($this->lang->core['date'], 'w-20')
           -> addTh($this->lang->content['name'])
           -> addTh($this->lang->core['func'] , 'w-180')
        ;

        $this->setContent($t->render());

        return $this->output();
    }

    /**
     * ajax response tale items
     * @param int $parent_id
     * @return string
     */
    public function items($parent_id=0)
    {
        $dt = new Table();
        return $dt
            -> table('content c')
            -> where(($parent_id> 0 ? "c.parent_id={$parent_id} and " : '') . "c.type_id={$this->type_id}")
            -> join("join content_info i on i.content_id = c.id and i.languages_id = {$this->language_id}")
            -> columns(
                array(
                    'c.id',
                    'c.createdon',
                    'i.name',
                    'c.published',
                    'CONCAT("Travel/", "edit/", c.id) as href'
                )
            )
            -> searchCol(array('c.id','c.createdon','i.name'))
            -> returnCol(array('c.id','createdon','name' , 'func'))
            -> formatCol(
                array(
                1 => '{createdon}',
                2 => '<a href="{href}">{name}</a>',
                3 => Form::link(
                        '',
                        Form::icon('icon-eye-{published}'),
                        array(
                           'class'    =>'btn-primary',
                            'title'   => $this->lang->core['published_tip'],
                            'onclick' => 'engine.Travel.pub({id},{published})'
                        )
                    ) .
                    Form::link(
                        '',
                        Form::icon('icon-edit'),
                        array(
                           'class'  =>'btn-info',
                            'title' => $this->lang->core['edit'],
                            'href'  => 'Travel/edit/{id}'
                        )
                    ) .
                    Form::button(
                        '',
                        Form::icon('icon-remove'),
                        array(
                            'title'   => $this->lang->core['delete'],
                            'class'   =>'btn-danger',
                            'onclick' => 'engine.Travel.del({id})'
                        )
                    )
            ))
            -> request();
    }

    /**
     * auto crate page id vs default params
     * @param int $parent_id
     * @return mixed|string
     */
    public function create($parent_id=0)
    {
        $this->parent_id = $parent_id;
        return parent::create($parent_id);
    }

    /**
     * edit page
     * @param $id
     * @return mixed|string
     */
    public function edit($id)
    {
        # 1. Встановлюю які поля потрібно відображати у формі
        $this->configFormField('nav_menu',0);

        # 2. Заповнюю контент даними по замовчуванню
        parent::edit($id);

        # 2.a Додаю кнопочку назад

        $this->prependToButtons(
            Form::button(
                $this->lang->core['back'],
                Form::icon('icon-external-link'),
                array(
                    'class'=>'btn-link',
                    'onclick' => "self.location.href='Travel/index/{$this->parent_id}'"
                )
            )
        );

        # 3. Додаю параметри для форми
//        $data = $this->ms->data('content', $id);

        $this->formAppendData('form_action',       "Travel/process/$id")
             ->formAppendData('redirect_url',     'Travel')
             ->formAppendData('date', '')
          ;

        # 4. Генерую контент і виводжу на екран

        return $this->renderContent();
    }


    /**
     * @param $id
     * @return mixed|string
     */
    public function process($id)
    {
        parent::process($id);
       // $this->updateCategories($id);

        return json_encode($this->getFormResponse());
    }

    /**
     * @param $id
     * @return int
     */


    /**
     * видалення сторінки
     * @param $id
     * @return mixed 1
     */
    public function delete($id)
    {
        return parent::delete($id);
    }

    /**
     * @return string
     */
    public function tree()
    {
        $tree=array();
        $parent_id = isset($_POST['id']) ? (int)$_POST['id'] : 0;
        $type= $this->ms->getTypeId('Travel');
        if(empty($type)) return 'wrong type id';
        $r = $this->ms->tree($parent_id, $type);
        $i=0;
        if(!empty($r)){
            foreach ( $r as $row) {
//                $row['isfolder']   = $this->ms->isFolderManual($row['id'], 'postCategory');
                $tree[$i]['data']  = $row['name'] . ' #' . $row['id'] ;
                $tree[$i]['state'] = $row['isfolder'] ? 'closed' : '';
                $tree[$i]['attr']  = array(
                                        'id' => $row['id'],
                                        "rel"=> (($row['isfolder'])? 'folder':'file'),
                                        "href"=>   './Travel/edit/'. $row['id'] ,
                                    );
                $i++;
            }
        }

        return json_encode($tree);
    }

    /**
     * return controller content
     * @return string
     */
    protected function output()
    {
        $this->setSidebar(
            new Tree(
                'content',
                'Travel/tree/',
                '',
//                Tree::button($this->lang->pages['tree_add'], 'self.location.href="./pages/create";', 'icon-plus-sign'),
                array(
                    Tree::contextMenu($this->lang->pages['tree_cmenu_add'], 'icon-plus' ,'self.location.href="./Travel/create/"+id;'),
                    Tree::contextMenu($this->lang->pages['tree_cmenu_edit'], 'icon-pencil' ,'self.location.href="./Travel/edit/"+id;'),
                    Tree::contextMenu($this->lang->pages['tree_cmenu_list'], 'icon-list' ,'self.location.href="./Travel/index/"+id;'),
                    Tree::contextMenu($this->lang->pages['tree_cmenu_del'], 'icon-remove' ,'engine.pages.delete(id)')
                ),
                true
            )
        );

        return parent::output();
    }
}
