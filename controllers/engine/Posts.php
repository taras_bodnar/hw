<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wg.
 * Date: 31.05.14 18:36
 */

namespace controllers\engine;

use controllers\core\DB;

defined('SYSPATH') or die();

/**
 * Class Posts
 * @package controllers\engine
 */
class Posts extends Content {

    private $cat = array();
    protected $parent_id='';

    public function __construct()
    {
        parent::__construct();

        $this->setType('post');
    }

    public function install(){return 1;}
    public function uninstall(){return 1;}

    /**
     * СЃРїРёСЃРѕРє СЃС‚РѕСЂС–РЅРѕРє
     * @param string $parent_id
     * @return mixed|string
     */
    public function index($parent_id = '')
    {
        $this->setButtons
        (
            Form::button(
                $this->lang->core['create'],
                Form::icon('icon-file'),
                array(
                    'class'   => Form::BTN_TYPE_PRIMARY,
                    'onclick' => "self.location.href='Posts/create/{$parent_id}'"
                ))
        );

        $t = new DataTables();
        $t -> ajaxConfig("Posts/items/$parent_id")
            -> setId('Posts')
//            -> setConfig('sortable', true)
            -> setConfig("order",
                array(
                    5,"asc"
                )
            )
            -> sortable('content','sort')
            -> setDisplayLength(100)
            -> setTitle($this->lang->blog['table_title'])
            -> th($this->lang->content['id'], 'w-20')
            -> th($this->lang->core['date'], 'w-20')
            -> th($this->lang->content['name'])
            -> th('Порядковий номер')
            -> th($this->lang->core['func'] , 'w-180')
        ;

        $this->setContent($t->render());

        return $this->output();
    }

    /**
     * ajax response tale items
     * @param int $parent_id
     * @return string
     */
    public function items($parent_id=0)
    {
        $t = new DataTables();
        $t
            -> table('content c')
            -> where("c.type_id={$this->type_id}")
            -> join("join content_info i on i.content_id = c.id and i.languages_id = {$this->language_id}");

        if($parent_id>0) {
            $t->join(" join posts_categories pc on pc.cid={$parent_id} and pc.pid=c.id");
        }
        $t
            ->get(
                array(
                    'c.id',
                    'c.createdon',
                    'i.name',
                    'c.published',
                    'c.sort'
                )
            )
            -> searchCol(array('c.id','i.name'))
            ->execute();

        $res = array();
        foreach ($t->getResults(false) as $i=>$row) {
            $res[$i][] = '<i class="icon-reorder" style="cursor:move" id="'. $row['id'] .'"></i>';
            $res[$i][] = $row['id'];
            $res[$i][] = $row['createdon'];
            $res[$i][] = "<a href='posts/edit/{$row['id']}'>{$row['name']}</a>";
            $res[$i][] = $row['sort'];
            $res[$i][] = "
                    <a class='btn btn-primary' href='posts/edit/{$row['id']}'><i class='icon-edit icon-white'></i></a>
                    <a class='btn btn-danger' onclick='engine.blog.posts.del({$row['id']});' title='Р’РёРґР°Р»РёС‚Рё' ><i class='icon-remove icon-white'></i></a>
                    ";
        }

        return $t->renderJSON($res, $t->getTotal());
    }

    /**
     * auto crate page id vs default params
     * @param int $parent_id
     * @return mixed|string
     */
    public function create($parent_id=0)
    {
        $this->parent_id = $parent_id;
        return parent::create($parent_id);
    }

    /**
     * edit page
     * @param $id
     * @return mixed|string
     */
    public function edit($id)
    {
        # 1. Р’СЃС‚Р°РЅРѕРІР»СЋСЋ СЏРєС– РїРѕР»СЏ РїРѕС‚СЂС–Р±РЅРѕ РІС–РґРѕР±СЂР°Р¶Р°С‚Рё Сѓ С„РѕСЂРјС–
        $this->configFormField('nav_menu',0);
        $this->configFormField('preview',1);
        $this->configFormField('main',1);

        # 2. Р—Р°РїРѕРІРЅСЋСЋ РєРѕРЅС‚РµРЅС‚ РґР°РЅРёРјРё РїРѕ Р·Р°РјРѕРІС‡СѓРІР°РЅРЅСЋ
        parent::edit($id);

        # 2.a Р”РѕРґР°СЋ РєРЅРѕРїРѕС‡РєСѓ РЅР°Р·Р°Рґ

        $this->prependToButtons(
            Form::button(
                $this->lang->core['back'],
                Form::icon('icon-external-link'),
                array(
                    'class'=>'btn-link',
                    'onclick' => "self.location.href='Posts/index/{$this->parent_id}'"
                )
            )
        );

        # 3. Р”РѕРґР°СЋ РїР°СЂР°РјРµС‚СЂРё РґР»СЏ С„РѕСЂРјРё
//        $data = $this->ms->data('content', $id);

        $this->formAppendData('form_action',       "Posts/process/$id")
            ->formAppendData('redirect_url',     'Posts')
            ->formAppendData('date', '')
            ->formAppendData('categories', $this->categories(0, '', $id));

        # 4. Р“РµРЅРµСЂСѓСЋ РєРѕРЅС‚РµРЅС‚ С– РІРёРІРѕРґР¶Сѓ РЅР° РµРєСЂР°РЅ

        return $this->renderContent();
    }

    /**
     * categories list
     * @param int $parent_id
     * @param string $parent_name
     * @param array $selected
     * @return array
     */
    private function categories($parent_id=0, $parent_name='', $selected)
    {
        $mb = $this->load->model('engine\Blog');

        $cid=array();
        foreach ($mb->getCategories($selected) as $r) {
            $cid[] = $r['cid'];
        }
        $cat_type = $this->ms->getTypeId('PostsCategories');

        $r = $this->ms->tree($parent_id, $cat_type);
        foreach ($r as $row) {
            if($parent_name != '') {
                $row['name'] = $parent_name .' / '. $row['name'];
            }
            if(in_array($row['id'], $cid)) {
                $row['selected'] = 'selected';
            }
            $this->cat[] = $row;
            if($row['isfolder']) {
                $this->categories($row['id'], $row['name'], $selected);
            }
        }

        return $this->cat;
    }
    /**
     * @param $id
     * @return mixed|string
     */
    public function process($id)
    {
        $main = (int)$_POST['data']['main'];
        $main_id = DB::instance()->select("select id from content where main=1 limit 1")->row("id");

        if($main && $id!=$main_id) {
            DB::instance()->update('content',array(
                'main'=>0
            ));
        }

        parent::process($id);
        $this->updateCategories($id);

        return json_encode($this->getFormResponse());
    }

    /**
     * @param $id
     * @return int
     */
    private function updateCategories($id)
    {
        $mb = $this->load->model('engine\Blog');

        $cid=array();
        $c = $mb->getCategories($id);
        foreach ($c as $r) {
            $cid[$r['cid']] = $r['cid'];
        }

        foreach ($_POST['categories'] as $k=>$c) {
            if(in_array($c, $cid)){
                unset($cid[$c]);
                continue;
            }
            $mb->setCategories($id, $c);
        }

        foreach ($cid as $k=>$v) {
            $mb->deleteCategories($id, $v);
        }


        return 1;
    }

    /**
     * РІРёРґР°Р»РµРЅРЅСЏ СЃС‚РѕСЂС–РЅРєРё
     * @param $id
     * @return mixed 1
     */
    public function delete($id)
    {
        return parent::delete($id);
    }

    /**
     * @return string
     */
    public function tree()
    {
        $tree=array();
        $parent_id = isset($_POST['id']) ? (int)$_POST['id'] : 0;
        $type= $this->ms->getTypeId('PostsCategories');
        if(empty($type)) return 'wrong type id';
        $r = $this->ms->tree($parent_id, $type);
        $i=0;
        if(!empty($r)){
            foreach ( $r as $row) {
//                $row['isfolder']   = $this->ms->isFolderManual($row['id'], 'postCategory');
                $tree[$i]['data']  = $row['name'] . ' #' . $row['id'] ;
                $tree[$i]['state'] = $row['isfolder'] ? 'closed' : '';
                $tree[$i]['attr']  = array(
                    'id' => $row['id'],
                    "rel"=> (($row['isfolder'])? 'folder':'file'),
                    "href"=>   './Posts/index/'. $row['id'] ,
                );
                $i++;
            }
        }

        return json_encode($tree);
    }

    /**
     * return controller content
     * @return string
     */
    protected function output()
    {
        $this->setSidebar(
            new Tree(
                'content',
                'Posts/tree/',
                Tree::button($this->lang->categories['tree_add'], 'engine.blog.categories.create()', 'icon-plus-sign'),
                array(
                    Tree::contextMenu($this->lang->categories['tree_cmenu_add'], 'icon-plus' ,'engine.blog.categories.create(id)'),
                    Tree::contextMenu($this->lang->categories['tree_cmenu_edit'], 'icon-pencil' ,'engine.blog.categories.edit(id)'),
                    Tree::contextMenu($this->lang->categories['tree_cmenu_del'], 'icon-remove' ,'engine.blog.categories.del(id)'),
                ),
                true
            )
        );
        return parent::output();
    }
}