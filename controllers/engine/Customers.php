<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 26.06.14 12:58
 */

namespace controllers\engine;

use controllers\Engine;
use models\app\Guides;
use models\modules\Catalog;
use models\modules\Users;

defined('SYSPATH') or die();

class Customers extends Engine {
    private $rang = array(1, 99);

    public function __construct()
    {
        parent::__construct();

        $this->mg = $this->load->model('engine\Users');
        $this->g = new Guides();

        $this->mg->setRang($this->rang[0], $this->rang[1]);

        $this->mUsers = new Users();
        $this->mCatalog = new Catalog();
    }

    /**
     * @param int $parent_id
     * @return mixed
     */
    public function index($parent_id=0)
    {
        if(empty($parent_id)) $parent_id='';
        $buttons= array(
            Form::button(
                $this->lang->core['create'],
                Form::icon('icon-file'),
                array(
                    'class'   => Form::BTN_TYPE_PRIMARY,
                    'onclick' => "self.location.href='customers/create/{$parent_id}'"
                ))
        );

        $t = new DataTables();
        $t->setId('users')
            ->ajaxConfig('Customers/items/' . $parent_id)
            ->setTitle($this->lang->customers['table_title'])
            -> setConfig("order",
                array(
                    6,"desc"
                )
            )
            ->sortable('users u', 'u.mark', 'u.id')
            ->setDisplayLength(100)

            ->th($this->lang->customers['id'])
            ->th($this->lang->customers['name'])
            ->th($this->lang->customers['phone'])
            ->th($this->lang->customers['email'])
            ->th("Востаннє авторизований")
            ->th($this->lang->customers['createdon'])
            ->th($this->lang->core['func'] , 'w-120')
        ;

        $this->setButtons($buttons);
//        $this->setContent($this->filterForm().$t->render());
        $this->setContent($t->render());

        return $this->output();
    }

    /**
     * table users group items
     * @param $users_group_id
     * @return string
     */
    public function items($users_group_id=0)
    { $filter_type = 0;

//        if(isset($_POST['filter']['city_id']) && $_POST['filter']['city_id']>0 && $users_group_id>0) {
//            $filter_type = $_POST['filter']['city_id'];
//            $where = "u.city_id={$filter_type} and u.users_group_id=$users_group_id";
//            // die($filter_type);
//        } else if(isset($_POST['filter']['city_id']) && $users_group_id==0){
//            $filter_type = $_POST['filter']['city_id'];
//            $where = "u.city_id={$filter_type}";
//        } else

            if($users_group_id>0) {
            $where = "u.users_group_id=$users_group_id";
        } else {
            $where = "1";
        }

        $t = new DataTables();
        $t  -> table('users u')
//            ->debug(1)
            -> get("u.id,u.name, u.phone, u.email,u.last_login, u.createdon,u.surname,u.url")

            -> join("join users_group g on g.id=u.users_group_id and g.rang between {$this->rang[0]} and {$this->rang[1]}")
            -> join("left join users_sort us on us.users_id=u.id and us.guides_id={$filter_type}")
            -> where($where)

            -> searchCol(array('u.id','u.phone','u.email','u.name','u.surname'))
            -> execute();

        $res = array();

        foreach ($t->getResults(false) as $i=>$row) {
            $deleteBtn = $_SESSION['admin']['rang']>=200?Form::button(
                '',
                Form::icon('icon-remove'),
                array(
                    'title'   => $this->lang->core['delete'],
                    'class'   =>'btn-danger',
                    'onclick' => 'engine.users.delete('.$row['id'].')'
                )
            ):"";
            $res[$i][] = '<i class="icon-reorder" style="cursor:move" id="'. $row['id'] .'"></i>';
//            $res[$i][] = '';
            $res[$i][] = $row['id'];
            $res[$i][] = "<a href='customers/edit/{$row['id']}'>{$row['name']} {$row['surname']}</a>";
            $res[$i][] = $row['phone'];
            $res[$i][] = $row['email'];
            $res[$i][] = $row['last_login'];
            $res[$i][] = $row['createdon'];
            $res[$i][] = Form::link(
                    '',
                    Form::icon('icon-edit'),
                    array(
                        'class'  =>'btn-info',
                        'title' => $this->lang->core['edit'],
                        'href'  => 'customers/edit/' . $row['id']
                    )
                ) .$deleteBtn.
                Form::link(
                    '',
                    Form::icon('icon-link'),
                    array(
                        'class'  =>'btn-info',
                        'title' => $this->lang->core['edit'],
                        'href'  => APPURL.$row['url'],
                        'target' => '_blank'
                    ));

        }


        return $t->renderJSON($res, $t->getTotal());
        $dt = new Table();
        return $dt
            -> table('users u')
            -> where($users_group_id > 0 ? "u.users_group_id=$users_group_id " : '')
            -> join("join users_group g on g.id=u.users_group_id and g.rang between {$this->rang[0]} and {$this->rang[1]}")
            -> columns(
                array(
                    'u.id',
                    'u.name',
                    'u.phone',
                    'u.email',
                    'u.createdon'
                )
            )
            -> searchCol(array('u.id','u.phone','u.email','u.name'))
            -> returnCol(array('u.id','name','phone','email','createdon', 'func'))
            -> formatCol(
                array(
                    1 => '<a href="customers/edit/{id}">{name}</a>',
                    5 =>
                        Form::link(
                            '',
                            Form::icon('icon-edit'),
                            array(
                                'class'  =>'btn-info',
                                'title' => $this->lang->core['edit'],
                                'href'  => 'customers/edit/{id}'
                            )
                        ) .
                        Form::button(
                            '',
                            Form::icon('icon-remove'),
                            array(
                                'title'   => $this->lang->core['delete'],
                                'class'   =>'btn-danger',
                                'onclick' => 'engine.users.delete({id})'
                            )
                        )
                ))
            -> request();
    }


    public function create($parent_id='')
    {
        $buttons= array(
            Form::button(
                $this->lang->core['back'],
                Form::icon('icon-external-link'),
                array(
                    'class'=>'btn-link',
                    'onclick' => 'self.location.href=\'customers/index/'.$parent_id.'\''
                )
            ),
            Form::button(
                $this->lang->core['save'],
                Form::icon('icon-save', false),
                array(
                    'class'=>'btn-success form-submit'
                ))
        );

        $content = $this->load->view('users/form',array(
            'action' => 'create',
            'id'        => 0,
            'group'     => $this->mg->group(),
            'languages' => $this->mg->languages(),
            'form_action' => 'customers/process',
            'plugins'   => $this->getPlugins(0, 'create')
        ));

        $this->setButtons($buttons);
        $this->setContent($content);

        return $this->output();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $buttons= array(
            Form::button(
                $this->lang->core['back'],
                Form::icon('icon-external-link'),
                array(
                    'class'=>'btn-link',
                    'onclick' => 'self.location.href=\'customers\''
                )
            ),
            Form::button(
                $this->lang->core['save'],
                Form::icon('icon-save', false),
                array(
                    'class'=>'btn-success form-submit'
                ))
        );
        

        $content = $this->load->view('users/form',array(
            'action'     => 'edit',
            'data'       => $this->mg->data('users', $id),
            'point'      =>$this->points($id),
            'id'         => $id,
            'group'     => $this->mg->group(0,$id),
            'form_action' => 'customers/process',
            'languages' => $this->mg->languages(),
            'plugins'   => $this->getPlugins($id, 'edit')
        ));

        // output
        $this->setButtons($buttons);
        $this->setContent($content);

        return $this->output();
    }

    public function points($id)
    {
        $point = 0;

        //Витягую вартість за послугу

        $s = $this->mUsers->getPrice($id);

        $point = $s == 1 ? $point + 5 : $point;

        //Витягую знижки
        $s = $this->mUsers->getDiscount($id);
        $point = $s == 1 ? $point + 10 : $point;

        //Витягую портфоліо
        $s = $this->mUsers->getPortfolio($id);
        $point = $s > 0 ? $point + 10 : $point;

        //Витягую коментарі
        $s = $this->mCatalog->getCommentsTotal($id);
        $point = $s > 0 ? $point + (10 * $s) : $point;

        //Витягую лайки
        $s = $this->mUsers->getTotalLike($id);
        $point = $s > 0 ? $point + (1 * $s) : $point;

        //витягую Підтверджені замовлення
        $s = $this->mUsers->getConfirmOrders($id);
        $point = $s > 0 ? $point + (20 * $s) : $point;

        return $point;
    }

    /**
     * @param $id
     * @return mixed|string
     */
    public function process($id='')
    {
        error_reporting(0);
        $s=0;
        $e = $this->errors['warning'];
        $r = 'users'; // redirect url
        $data = $_POST['data'];

        // check required fields
        if(
            empty($data['name']) ||
//            empty($data['phone']) ||
            empty($data['email'])
        ) {
            $this->error[] = $this->lang->customers['e_required_fields'];
        }

        if($_POST['action'] == 'create' && empty($data['password'])) {
            $this->error[] = $this->lang->customers['e_enter_pass'];
        }
        // add or update values
        if(empty($this->error)) {
            switch($_POST['action']){
                case 'create':
                    $data['password'] = $this->cryptPass($data['password']);
                    $data['createdon'] = date('Y-m-d H:i:s');
                    $s = $this->mg->create($data);
                    if($s > 0) {
                        $e = $this->errors['success'];
                        $this->error[] = $this->lang->customers['save_success'];
                    } else {
                        $e = $this->errors['error'];
                        $this->error[] = $this->mg->error();
                    }
                    break;
                case 'edit':
                    if(empty($id)) return '';
                    $r='';

                    if(empty($data['password'])){
                        unset($data['password']);
                    } else {
                        $data['password'] = crypt($data['password']);
                    }

                    $s = $this->mg->update($id, $data);

                    if($s > 0) {
                        $e = $this->errors['success'];
                        $this->error[] = $this->lang->customers['save_success'];
                    } else {
                        $e = $this->errors['error'];
                        $this->error[] = $this->mg->error() ;
                    }

                    break;
                default:
                    break;
            }

            $this->getPlugins($id, 'process');
        }

        return json_encode(array(
            's' => $s > 0 , // status
            'r' => $r, // redirect url
            'e' => $e, // error class
            't' => $this->lang->core[$e], // error title
            'm' => implode('<br>', $this->error) // error message
        ));
    }

    /**
     * crypt passw vs salt
     * @param $pass
     * @return string
     */
    private function cryptPass($pass)
    {
        return crypt($pass);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $this->getPlugins($id, 'delete');
        return $this->mg->delete($id);
    }

    /**
     * users group tree
     * @return string
     */
    public function tree()
    {
        $parent_id = isset($_POST['id']) ? (int)$_POST['id'] : 0;
        $tree=array();
        $r = $this->mg->tree($parent_id);
        $i=0;
        if(!empty($r)){
            foreach ( $r as $row) {
                $tree[$i]['data'] = $row['name'] . ' #' . $row['id'] ;
                $tree[$i]['state'] = $row['isfolder'] > 0  ? 'closed' : '';

                //todo нада повіксати шлях

                $tree[$i]['attr'] = array(
                    'id'   => $row['id'],
                    "rel"  => (($row['isfolder'])? 'folder':'file'),
                    "href" =>   'customers/index/' . $row['id'] ."/"
                );
                $i++;
            }
        }

        return json_encode($tree);
    }

    /**
     * @return string
     * @throws \controllers\core\Exceptions
     */
    private function filterForm()
    {
        $filterContent = $this->g->getFilter(9);

        return $this->load->view(
            'users/filter',
            array(
                'filterContent'=>$filterContent
            )
        );
    }

    /**
     * get admin info
     * @param null $key
     * @return mixed
     */
    public static function adminInfo($key=null)
    {
        if(!isset($_SESSION['admin'])) return null;

        return $key ? isset($_SESSION['admin'][$key]) : $_SESSION['admin'];
    }

    /**
     * return controller content
     * @return string
     */
    protected function output()
    {
        $this->setSidebar(
            new Tree(
                'users',
                'customers/tree/',
                Tree::button($this->lang->customers['tree_add'], 'self.location.href="customers/create";', 'icon-plus-sign'),
                array(
                    Tree::contextMenu($this->lang->customers['tree_cmenu_add'], 'icon-plus' ,'self.location.href="UsersGroup/create/"+id;'),
                    Tree::contextMenu($this->lang->customers['tree_cmenu_edit'], 'icon-pencil' ,'self.location.href="UsersGroup/edit/"+id;'),
                    Tree::contextMenu($this->lang->customers['tree_cmenu_del'], 'icon-remove' , 'engine.users.delete(id)')
                ),
                true
            )
        );

        return parent::output();
    }
} 