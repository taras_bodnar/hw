<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 13.07.14 9:57
 */

namespace controllers\engine;

 
use controllers\Engine;

defined('SYSPATH') or die();

/**
 * Class OrdersStatus
 * @package controllers\engine
 */
class OrdersStatus  extends Engine{

    public function __construct()
    {
        parent::__construct();

        $this->mg = $this->load->model('engine\OrdersStatus');
    }

    /**
     * @return mixed|string
     */
    public function index()
    {
        $buttons = array();

        $buttons[] = Form::button(
                $this->lang->core['create'],
                Form::icon('icon-file'),
                array(
                    'class'   => Form::BTN_TYPE_PRIMARY,
                    'onclick' => "engine.ordersStatus.create()"
                ));

        $t = new DataTables();
        $t
            -> setId('ordersStatus')
            -> ajaxConfig("OrdersStatus/items")
            -> setTitle($this->lang->ordersStatus['table_title'])
            -> th($this->lang->ordersStatus['id'], '', 'width:160px;')
            -> th($this->lang->ordersStatus['name'])
            -> th($this->lang->ordersStatus['bg_color'])
            -> th($this->lang->core['func'], '', 'width:220px;')
        ;

        $this->setButtons($buttons);
        $this->setContent($t->render());

        return $this->output();
    }

    /**
     * @return string
     */
    public function items()
    {
        $t = new DataTables();
        $t  -> table('orders_status s')
//            -> debug()
            -> searchCol('i.name, s.id')
            -> get('s.id, i.name, s.bg_color, s.text_color')
            -> join("join orders_status_info i on i.orders_status_id=s.id and i.languages_id={$this->language_id}")
            -> execute();

        $r   = $t->getResults(false);

        $res = array();

        foreach ($r as $row) {
            $res[] = array(
                $row['id'],
                $row['name'],
                "<label class='label' style='background: {$row['bg_color']}; color: {$row['text_color']}'>{$row['name']}</label>",
                "
                <button onclick='engine.ordersStatus.edit({$row['id']});' class='btn btn-info' title='Редагувати'><i class='icon-pencil'></i></button>
                <button onclick='engine.ordersStatus.del({$row['id']});' class='btn btn-danger' title='Видалити'><i class='icon-trash'></i></button>"
            );
        }


        return $t->renderJSON($res, $t->getTotal());
    }

    /**
     * @return mixed|string
     */
    public function create()
    {
        return $this->load->view('OrdersStatus/form',array(
            'action'    => 'create',
            'id'        => 0,
            'languages' => $this->languages->all()
        ));
    }

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        if(empty($id)) return 'Empty ID';

        $data = $this->mg->data('orders_status', $id);

        return $this->load->view('OrdersStatus/form',array(
            'action' => 'edit',
            'id'     => $id,
            'data'   => $data,
            'info'   => $this->mg->getInfo($id),
            'languages' => $this->languages->all()
        ));
    }

    /**
     * @param $id
     * @return mixed|string
     */
    public function process($id=0)
    {
        $s=0;
        $e = $this->errors['warning'];
        $data = $_POST['data'];

        // add or update values
        if(empty($this->error)) {
             switch($_POST['action']){
                case 'create':
                    $s = $this->mg->create($data, $_POST['info']);
                    if($s > 0) {
                        $e = $this->errors['success'];
                        $this->error[] = $this->lang->ordersStatus['create_success'];
                    } else {
                        $e = $this->errors['error'];
                        $this->error[] = $this->mg->error();
                    }
                    break;
                case 'edit':
                    if(empty($id)) return '';
                    $s = $this->mg->update($id, $data, $_POST['info']);
                    if($s > 0) {
                        $e = $this->errors['success'];
                        $this->error[] = $this->lang->ordersStatus['save_success'];
                    } else {
                        $e = $this->errors['error'];
                        $this->error[] = $this->mg->error() ;
                    }
                    break;
                default:
                    break;
            }
        }

        return json_encode(array(
            's' => $s > 0 , // status
            'r' => '', // redirect url
            'e' => $e, // error class
            't' => $this->lang->core[$e], // error title
            'm' => implode('<br>', $this->error) // error message
        ));
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        return $this->mg->delete($id);
    }

} 