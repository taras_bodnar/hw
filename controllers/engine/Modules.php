<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 31.05.14 18:36
 */
namespace controllers\engine;

use controllers\Engine;

defined('SYSPATH') or die();

class Modules extends Engine{
    /**
     * path to modules
     * @var
     */
    private $cpath = '/controllers/modules/';

    private $ns;
    /**
     * model instance
     * @var
     */
    private $model;

    public function __construct()
    {
        parent::__construct();

        $this->model = $this->load->model('engine\Modules');

        $this->ns = str_replace('/', '\\', $this->cpath);
    }
    /**
     * @return mixed|string
     */
    public function index()
    {
//        $this->setButtons
//        (
//            Form::button
//            (
//                $this->lang->modules['install'],
//                Form::icon('icon-archive'),
//                array(
//                    'class'   => Form::BTN_TYPE_PRIMARY,
//                    'onclick' => "self.location.href='./modules/install'"
//                )
//            )
//        );

        $t = new Table('modules', 'modules/items');

        $t ->setTitle($this->lang->modules['table_title']);
        $t ->addTh($this->lang->modules['name']);
        $t ->addTh($this->lang->modules['description']);
        $t ->addTh($this->lang->modules['author']);
        $t ->addTh($this->lang->modules['version']);
        $t ->addTh($this->lang->core['func'] , 'w-120');
        ;
        $this->setContent($t->render());

        return $this->output();
    }

    /**
     * modules list
     * @return string
     */
    public function items()
    {
       $dt = new Table();
       $modules = $this->listModules();
       return $dt->renderJSONData($modules);
    }

    /**
     * return modules list
     * @return array
     */
    private function listModules()
    {
        $modules = array();
        if ($handle = opendir(DOCROOT . $this->cpath)) {
            while (false !== ($entry = readdir($handle))) {
                if ($entry != "." && $entry != ".." && !is_dir(DOCROOT . $this->cpath . $entry)) {

                    $module = str_replace('.php', '', $entry);
//                    echo $entry , '<br>';
                    $row = $this->readPhpDoc($module);

                    $row['func'] = "";

                    if($id=$this->model->isInstalled($module)){
                        $row['func'] .= "<button class='btn btn-primary' onclick=\"engine.modules.edit('{$id}')\"><i class=\"icon-edit icon-white\"></i></button>";
                        $row['func'] .= "<button class='btn btn-danger uninstall-plugin' onclick=\"engine.modules.uninstall('{$id}')\"><i class=\"icon-off icon-white\"></i></button>";
                    } else {
                        $row['func'] .= "<button class='btn btn-success' onclick=\"engine.modules.install('{$module}')\"><i class=\"icon-off icon-white\"></i></button>";
                    }

                    $row['name']        = "<b>{$row['name']}</b>";
                    $row['description']   .= "<br>package: " . $row['package'];
                    $row['author']      .= "<br>" . $row['copyright'];

//                    unset($row['description']);
                    unset($row['package']);
                    unset($row['copyright']);

                    $modules[] = $row;
                }
            }
            closedir($handle);
        }

        return $modules;
    }

    /**
     * @param $module
     * @return array
     */
    private function readPhpDoc($module)
    {
        $row = array();
        $rc = new \ReflectionClass($this->ns . $module);
        $dc = $rc->getDocComment();

        //Get the comment
        if(preg_match('#^/\*\*(.*)\*/#s', $dc, $comment) === false){
            $row['name'] = 'Невірно внесено DocComment';
            return $row;
        }
        $comment = trim($comment[1]);
        if(preg_match_all('#^\s*\*(.*)#m', $comment, $lines) === false){
            $row['name'] = 'Невірно внесено DocComment';
            return $row;
        }
        foreach ($lines[1] as $line) {
            $line = trim($line);

            if(empty($line)) continue;

            if(strpos($line, '@') === 0) {
                $param = substr($line, 1, strpos($line, ' ') - 1); //Get the parameter name
                $value = substr($line, strlen($param) + 2); //Get the value
                $row[$param] = $value;
            }
        }

        return $row;
    }

    /**
     * істаляція модулю
     * - запустити транзакцію
     * - викликати інстал модуля парент інсталл модуля
     * @param $module
     * @return string
     */
    public function install($module)
    {
        $c=null;

        $controller = $this->ns . $module;

        try{
            $c = new $controller;
        } catch(\Exception $e){
            $this->error[] = $e->getCode() . $e->getMessage();
        }
        if(empty($this->error)){
            try{
                $c->install();
            } catch(\Exception $e){
                $this->error[] = $e->getCode() . $e->getMessage();
            }
        }

        // запуск транзакції
        if(empty($this->error)){
            $this->model->beginTransaction();
        }

        // запис модуля
        if(empty($this->error)){
            $settings = null;
            $_s = $c->installSettings();
            if(!empty($_s)){
                $settings = serialize($_s);
            }
            $this->model->create(array(
                'module' => $module,
                'settings' => $settings
            ));
        }
        if(empty($this->error)){
            try{
                $t = $c->getTranslations();
                if(!empty($t)){

                    // мови
                    $ml = $this->load->model('app\Languages');
                    $languages = $ml->get();

                    // перекладач

                    $translator = new Translator();

                    foreach ($t as $n=>$v) {
                        $info = array(); $def = '';
                        foreach ($languages as $lang) {
                            if($lang['front_default'] == 1) {
                                $def= $lang['code'];
                                $info[$lang['id']] = $v;
                            } else {
                                $info[$lang['id']] = $translator->translate($v, $lang['code'], $def);
                            }
                        }
//                        $this->dump($info);

                        if(!$this->model->createTranslations($n, $info)){
                            $this->error[] = 'Fail to create translation ' . $n;
                            break;
                        }

                    }
                }
            } catch(\Exception $e){
                $this->error[] = $e->getCode() . $e->getMessage();
            }
        }

        if(empty($this->error)){
            $sql = $c->getSQL();
            if(!empty($sql)){
                $sql= implode(';', $sql);
                $this->model->exec($sql);
            }
        }

        if(empty($this->error)){
            $this->model->commit();
        } else {
            $this->model->rollback();
        }

        return json_encode(
            array(
                's' => empty($this->error) ? 1 : 0,
                'm' => implode('<br>', $this->error)
            )
        );
    }


    public function uninstall($id)
    {
        $module = $this->model->getData($id, 'module');
        $c=null;

        $controller = $this->ns . $module;

        try{
            $c = new $controller;
        } catch(\Exception $e){
            $this->error[] = $e->getCode() . $e->getMessage();
        }
        if(empty($this->error)){
            try{
                $c->uninstall();
            } catch(\Exception $e){
                $this->error[] = $e->getCode() . $e->getMessage();
            }
        }

        // запуск транзакції
        if(empty($this->error)){
            $this->model->beginTransaction();
        }

        // видалю модуль
        if(empty($this->error)){
            $this->model->delete($id);
        }

        if(empty($this->error)){
            $sql = $c->getSQL();
            if(!empty($sql)){
                $sql= implode(';', $sql);
                $this->model->exec($sql);
            }
        }

        if(empty($this->error)){
            $this->model->commit();
        } else {
            $this->model->rollback();
        }

        return json_encode(
            array(
                's' => empty($this->error) ? 1 : 0,
                'm' => implode('<br>', $this->error)
            )
        );
    }

    public function create(){}

    public function edit($id)
    {
        $settings = $this->model->getData($id, 'settings');
        if(!empty($settings)){
            $settings = unserialize($settings);

        }

        return $this->load->view('modules_settings', array('id'=>$id, 'settings'=>$settings));
    }
    public function delete($id)
    {
        return $this->model->delete($id);
    }

    public function process($id){}

    /**
     * @param $sql
     * @return mixed
     */
    protected function executeSQL($sql)
    {
        return $this->model->exec($sql);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function updateSettings($id)
    {
        return $this->model->updateSettings($id, $_POST['data']);
    }
}
