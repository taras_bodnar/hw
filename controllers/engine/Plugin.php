<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 27.12.14 20:31
 */

namespace controllers\engine;

defined("SYSPATH") or die();

/**
 * Interface Plugin
 * @package controllers\engine
 */
interface Plugin {
    /**
     * call function when install plugin
     * @return mixed
     */
    public function install();

    /**
     * call function when uninstall plugin
     * @return mixed
     */
    public function uninstall();
}