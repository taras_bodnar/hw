<?php
/**
 * Company Otakoyi.com
 * Author: wg
 * Date: 12.01.15 13:24
 */

namespace controllers\engine;

use controllers\Engine;

defined("SYSPATH") or die();

class Payment extends Engine {
    private $model;
    private $path = 'controllers/modules/payment/';
    private $ns = null;

    public function __construct()
    {
        parent::__construct();

        $this->ns = str_replace('/', '\\', $this->path);

        $this->model = $this->load->model('engine\Payment');
    }
    /**
     * @return mixed|string
     */
    public function index()
    {
        $this->setButtons(
            Form::button
                (
                    $this->lang->core['create'],
                    Form::icon('icon-file'),
                    array(
                        'class'   => Form::BTN_TYPE_PRIMARY,
                        'onclick' => "self.location.href='payment/create'"
                    )
                )
        );
        $t = new Table('payment', 'payment/items');

        $t
            ->setTitle($this->lang->payment['table_title'])
            ->sortableConf('payment','d.id','d.sort');
        $t ->addTh('#');
        $t ->addTh($this->lang->payment['name']);
        $t ->addTh($this->lang->core['func'] , 'w-180');

        $this->setContent($t->render());

        return $this->output();
    }

    /**
     * payment list
     * @return string
     */
    public function items()
    {
        $dt = new Table();
        $dt
            -> table('payment d')
            -> join("join payment_info i on i.payment_id = d.id and i.languages_id = {$this->language_id}")
            -> columns(
                array(
                    'd.id',
                    'i.name',
                    'd.published'
                )
            )
            -> searchCol(array('i.name'))
//            -> returnCol(array('d.id','d.pib','d.message'))
        ;
        $rows = $dt->getRows();
        $out = array();
        foreach ($rows as $row) {
            $out[] = array(
                $row['id'],
                "<a href='payment/edit/{$row['id']}'>{$row['name']}</a>",
                Form::link(
                    '',
                    Form::icon('icon-eye-'. $row['published']),
                    array(
                        'class'    =>'btn-primary',
                        'onclick' => 'engine.payment.pub('.$row['id'].','. $row['published'] .')'
                    )
                ) .
                Form::link(
                    '',
                    Form::icon('icon-edit'),
                    array(
                        'title' => $this->lang->core['edit'],
                        'class'    =>'btn-primary',
                        'href' => 'payment/edit/'.$row['id']
                    )
                ) .
                Form::button(
                    '',
                    Form::icon('icon-remove'),
                    array(
                        'title'   => $this->lang->core['delete'],
                        'class'   =>'btn-danger',
                        'onclick' => 'engine.payment.del('.$row['id'].')'
                    )
                )
            );
        }
        $dt->beforeRender($out);

        return $dt->request();
    }

    public function create()
    {
        $buttons= array(
            Form::link(
                $this->lang->core['back'],
                Form::icon('icon-external-link'),
                array(
                    'class'=>'btn-link',
                    'href' => 'payment'
                )
            ),
            Form::button(
                $this->lang->core['save'],
                Form::icon('icon-save', false),
                array(
                    'class'=>'btn-success form-submit'
                ))
        );

        $languages = $this->load->model('engine\Languages');

        $content = $this->load->view('payment/form',array(
            'languages' => $languages->all(),
            'action' => 'create',
            'id'        => 0,
            'delivery'  => $this->model->getDelivery(0),
            'currency'  => $this->model->getCurrency(),
            'modules'   => $this->getModules(),
            'settings'  => $this->displaySettings(0)
        ));

        $this->setButtons($buttons);
        $this->setContent($content);

        return $this->output();
    }

    public function edit($id)
    {

        $buttons= array(
            Form::link(
                $this->lang->core['back'],
                Form::icon('icon-external-link'),
                array(
                    'class'=>'btn-link',
                    'href' => 'payment'
                )
            ),
            Form::button(
                $this->lang->core['save'],
                Form::icon('icon-save', false),
                array(
                    'class'=>'btn-success form-submit'
                ))
        );

        $languages = $this->load->model('engine\Languages');

        $data = $this->model->params($id);

        $content = $this->load->view('payment/form',array(
            'languages' => $languages->all(),
            'action' => 'edit',
            'id'        => $id,
            'data'   => $data,
            'info'   => $this->model->info($id),
            'delivery'  => $this->model->getDelivery($id),
            'currency'  => $this->model->getCurrency(),
            'modules'   => $this->getModules(),
            'settings'  => $this->displaySettings($id, $data['module'])
        ));

        $this->setButtons($buttons);
        $this->setContent($content);

        return $this->output();

    }

    public function displaySettings($payment_id, $module=null)
    {
        if(empty($module)) return '';
        $settings = array();
        $c = $this->ns . $module;

        try{
            $m = new $c;
            if(is_callable(array($c,'install'))){
                $m->install();
                $settings = $m->installSettings();
                if(!empty($settings) && $payment_id > 0){
                    $ps = $this->model->getSettings($payment_id);
                    if(!empty($ps)){
                        $ps = unserialize($ps);
//                        $this->dump($ps);
                        foreach ($settings as $i=>$a) {
                            if(!empty($ps[$a['name']])){
                                $settings[$i]['value'] = $ps[$a['name']];
                            }
                        }

                    }
                }
            }
        } catch(\Exception $e){
            echo $e->getMessage();
        }

        $this->request->mode='engine';
        return $this->load->view(
            'payment/settings',
            array(
                'settings' => $settings
            )
        );
    }

    private function getModules()
    {
        $modules = array(
            array('name'=>$this->lang->payment['s_select'] , 'value'=> '')
        );
        if ($handle = opendir(DOCROOT . $this->path)) {
            while (false !== ($entry = readdir($handle))) {
                if ($entry != "." && $entry != "..") {

                    $module = str_replace('.php', '', $entry);
                    $row = $this->readPhpDoc($module);
                    $modules[] = array(
                        'name'  => $row['name'],
                        'value' => $row['name']
                    );
                }
            }
            closedir($handle);
        }
        $this->request->mode='engine';

        return $modules;
    }

    private function readPhpDoc($module)
    {
        $row = array();
        $rc = new \ReflectionClass($this->ns . $module);
        $dc = $rc->getDocComment();

        //Get the comment
        if(preg_match('#^/\*\*(.*)\*/#s', $dc, $comment) === false){
            $row['name'] = 'Невірно внесено DocComment';
            return $row;
        }
        $comment = trim($comment[1]);
        if(preg_match_all('#^\s*\*(.*)#m', $comment, $lines) === false){
            $row['name'] = 'Невірно внесено DocComment';
            return $row;
        }
        foreach ($lines[1] as $line) {
            $line = trim($line);

            if(empty($line)) continue;

            if(strpos($line, '@') === 0) {
                $param = substr($line, 1, strpos($line, ' ') - 1); //Get the parameter name
                $value = substr($line, strlen($param) + 2); //Get the value
                $row[$param] = $value;
            }
        }

        return $row;
    }

    public function delete($id)
    {
        return $this->model->delete($id);
    }

    /**
     * publish comment
     * @param $id
     * @param $published
     * @return mixed
     */
    public function pub($id, $published)
    {
        $published = $published == 1 ? 0 : 1;
        return $this->model->pub($id, $published);
    }

    public function process($id=0)
    {
        $s=0;
        $e = $this->errors['warning'];
        $r = 'payment';
        $data = $_POST['data'];
        $info = $_POST['info'];
/*
        if(
            empty($data['price'])
        ){
            $this->error[] = $this->lang->payment['e_required_fields'];
        }
*/
        foreach ($info as $languages_id=>$arr) {
            if(empty($info[$languages_id]['name'])) {
                $this->error[] = $this->lang->payment['e_name'];
            }
        }

        if(empty($this->error)) {
            if(!empty($_POST['settings'])){

                $data['settings'] = serialize($_POST['settings']);
            }
            switch($_POST['action']){
                case 'create':
                    $s = $this->model->create($data, $info);
                    if($s > 0){
                        $id=$s;
                        $e = $this->errors['success'];
                        $this->error[] = $this->lang->payment['save_success'];
                    } else {
                        $e = $this->errors['error'];
                        $this->error[] = $this->model->error();
                    }
                    break;
                case 'edit':
                    if(empty($id)) return '';
                    $r='';
                    $s = $this->model->update($id, $data, $info);
                    if($s > 0) {
                        $e = $this->errors['success'];
                        $this->error[] = $this->lang->payment['save_success'];
                    } else {
                        $e = $this->errors['error'];
                        $this->error[] = $this->model->error() ;
                    }
                    break;
                default:
                    break;
            }
            if($id > 0){
                $dp = isset($_POST['dp']) ? $_POST['dp'] : array();
                $this->model->setDelivery($id, $dp);
            }
        }

        return json_encode(array(
            's' => $s > 0 , // status
            'r' => $r, // redirect url
            'e' => $e, // error class
            't' => $this->lang->core[$e], // error title
            'm' => implode('<br>', $this->error) // error message
        ));
    }

    public function sort($payment_id, $sort)
    {
        $s = explode(',', $sort);

        foreach ($s as $k=> $delivery_id) {
            $this->model->updateSort($payment_id, $delivery_id, $k);
        }
        return 1;
    }


    
    public function install()
    {
        return 1;
    }
    public function uninstall()
    {
        return 1;
    }

} 