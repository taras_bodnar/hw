<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 31.05.14 18:36
 */
namespace controllers\engine;

use controllers\Engine;
use controllers\modules\Notify;

defined('SYSPATH') or die();

class Orders extends Engine{
    private $mOrders;

    public function __construct()
    {
        parent::__construct();

        $this->mOrders = $this->load->model('engine\Orders');
    }
    /**
     * @return mixed|string
     */
    public function index()
    {
        $t = new DataTables();

        $t->setId('orders')
            ->ajaxConfig('Orders/items')
            ->setTitle('Замовлення')
            ->th('#')
            ->th('Покупець')
            ->th('Сума')
            ->th('Оплачений')
            ->th('Дата')
            ->th('Функції');


        $this->setContent($t->render());

        return $this->output();
    }

    /**
     * orders list
     * @return string
     */
    public function items()
    {
        $t = new DataTables();
        $t  -> table('orders o')
//            -> debug()
            -> searchCol('o.code,o.user_email, o.user_phone,o.user_name')
            -> get("o.id,o.code,o.user_email,o.user_phone,o.user_name,o.pay,o.pay_date, o.createdon, o.one_click,
                IF(o.status_id > 0, osi.name, 'Нове' ) as status, os.bg_color,os.text_color
                ")
            -> join("left join orders_status os on os.id=o.status_id")
            -> join("left join orders_status_info osi on osi.orders_status_id=o.status_id and osi.languages_id={$this->language_id}")
            -> execute();

        $r   = $t->getResults(false);

        $res = array();

        foreach ($r as $row) {
            $total = $this->mOrders->sumTotal($row['id']);
            $res[] = array(
                $row['code'] . "<br>
                <label class='label label-primary'
                ". (($row['bg_color'] == '') ? '' : "style='background: {$row['bg_color']}; color:{$row['text_color']}'") ."
                >{$row['status']}</label>
                " . (($row['one_click']) ? "<label class='label label-success'>Один клік</label>" : ''),
                "{$row['user_name']} <br>
                тел: {$row['user_phone']} <a href='mailto:{$row['user_email']}'>{$row['user_email']}</a>
                ",
                $total . ' грн.',
                ($row['pay']? "<label class='label label-success'>ТАК</label> {$row['pay_date']}" : 'НІ'),
                $row['createdon'],
                "<a href='./orders/edit/{$row['id']}' class='btn btn-info' title='Редагувати'><i class='icon-pencil'></i></a>"
                . (($row['pay'] == 0) ? "<button onclick='engine.orders.del({$row['id']});' class='btn btn-danger' title='Видалити'><i class='icon-trash'></i></button>" : '')
            );
        }


        return $t->renderJSON($res, $t->getTotal());
    }

    public function create(){}


    public function edit($id)
    {
        $buttons= array(
            Form::button(
                $this->lang->core['back'],
                Form::icon('icon-external-link'),
                array(
                    'class'=>'btn-link',
                    'onclick' => 'self.location.href=\'orders\''
                )
            ),
            Form::button(
                $this->lang->core['save'],
                Form::icon('icon-save', false),
                array(
                    'class'=>'btn-success form-submit'
                ))
        );

        $data    = $this->mOrders->getData($id);
        $products = $this->mOrders->getProducts($id);

        $total = $data['delivery_price'];
        foreach ($products as $item) {
            $total += $item['price'] * $item['quantity'];
        }

        $data['total'] = round($total, 2);

        $content = $this->load->view('orders/edit',array(
            'action'     => 'edit',
            'data'       => $data,
            'id'         => $id,
            'products'   => $this->ordersProducts($id),
            'statuses'   => $this->mOrders->getStatuses(),
            'status_history' => $this->mOrders->getStatusHistory($id)
        ));


        $this->setButtons($buttons);
        $this->setContent($content);

        return $this->output();
    }

    /**
     * @param $orders_id
     * @return string
     * @throws \controllers\core\Exceptions
     */
    private function ordersProducts($orders_id)
    {
        $data  = $this->mOrders->getData($orders_id);
        return $this->load->view(
            'orders/products',
            array(
                'products' => $this->mOrders->getProducts($orders_id),
                'data'     => $data
            )
        );
    }

    public function delete($id)
    {
        return $this->mOrders->delete($id);
    }

    public function process($id)
    {
        if(empty($id)) return '';

        $data = $_POST['data'];
        $e=array(); $s=0; $t = 'Помилка';

        /*if(empty($data['date_from']) || empty($data['date_from'])){
            $e[] = 'Вкажіть період бронювання';
        } */

        if(empty($e)){
            $odata= $this->mOrders->getData($id);
            if(isset($data['pay']) && $data['pay'] == 1 && $odata['pay'] == 0){
                $data['pay_date'] = date('Y-m-d H:i:s');
                // надсилаю повідомлення замовнику і власнику
//                    $notify = new Notify($id);
//                     повідомлення замовнику
//                    $notify->customer();

//                     повідомлення власнику
//                    $notify->owner();

            }

            // оновлюю замовлення
            $s   = $this->mOrders->update($id,$data);
            // оновлюю сатус
            $this->mOrders->saveStatusHistory($id, $data['status_id'], $data['comment']);
            $e[] = 'Дані оновлено';
            $t   = 'Успішно';
        }

        return json_encode(
            array(
                't' => $t,
                's' => $s,
                'm' => implode('<br>', $e),
                'r' => 'orders'
            )
        );

    }
    public function install()
    {
        return 1;
    }
    public function uninstall()
    {
        return 1;
    }
}
