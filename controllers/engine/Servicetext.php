<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 22.03.17
 * Time: 12:23
 */

namespace controllers\engine;


class Servicetext extends Content
{
    public function __construct()
    {
        parent::__construct();

        $this->setType('Servicetext');
    }

    public function index()
    {
        $this->setButtons
        (
            Form::button(
                $this->lang->core['create'],
                Form::icon('icon-file'),
                array(
                    'class' => Form::BTN_TYPE_PRIMARY,
                    'onclick' => "self.location.href='Servicetext/create/'"
                ))
        );


        $t = new DataTables();
        $t -> ajaxConfig("Servicetext/items")
            -> setId('pages')
//            -> setConfig('sortable', true)
            -> setConfig("order",
                array(
                    1,"asc"
                )
            )
//            -> sortable('content','sort')
            -> setDisplayLength(100)
            -> setTitle($this->lang->pages['table_title'])
            -> th($this->lang->content['id'], 'w-20')
            -> th($this->lang->content['name'])
            -> th($this->lang->core['func'] , 'w-180')
        ;

        $this->setContent($t->render());

        return $this->output();
    }

    public function items()
    {

        $t = new DataTables();
        $t
            ->table('content c')
            ->debug(false)
            ->where("c.type_id={$this->type_id}")
            ->join("join content_info i on i.content_id = c.id and i.languages_id = {$this->language_id}");

        $t
            ->get(
                array(
                    'c.id',
                    'i.name',
                    'c.published',
                    'CONCAT("Servicetext/", IF(c.isfolder, "index/", "edit/"), c.id) as href'
                )
            )
            ->searchCol(array('c.id', 'i.name'))
            ->execute();

        $res = array();
        foreach ($t->getResults(false) as $i=>$row) {
            $res[$i][] = $row['id'];
            $res[$i][] = "<a href='{$row['href']}'>{$row['name']}</a>";
            $res[$i][] = "
                    <a class='btn btn-primary' href='Servicetext/edit/{$row['id']}'><i class='icon-edit icon-white'></i></a>
                    <a class='btn btn-danger' onclick='engine.pages.delete({$row['id']});' title='Р’РёРґР°Р»РёС‚Рё' ><i class='icon-remove icon-white'></i></a>
                    ";
        }

        return $t->renderJSON($res, $t->getTotal());
    }

    /**
     * auto crate page id vs default params
     * @param int $parent_id
     * @return mixed|string
     */
    public function create($parent_id = 0)
    {
        return parent::create($parent_id);
    }

    /**
     * edit page
     * @param $id
     * @return mixed|string
     */
    public function edit($id)
    {
        # 1. Встановлюю які поля потрібно відображати у формі
//        $this->configFormField('module', 1);
        $this->configFormField('createdon', 1);


        # 2. Заповнюю контент даними по замовчуванню
        parent::edit($id);

        # 2.a Додаю кнопочку назад

        $this->prependToButtons(
            Form::link(
                $this->lang->core['back'],
                Form::icon('icon-external-link'),
                array(
                    'class' => 'btn-link',
                    'href' => 'Servicetext/index/' . $this->data['parent_id']
                )
            )
        );
        # 3. Додаю параметри для форми

        $selected_nav_menu = $this->ms->selectedNavMenu($id, $this->created);
        $this->formAppendData('form_action', "Servicetext/process/$id")
            ->formAppendData('redirect_url', 'Servicetext' .
                (isset($this->data['parent_id']) && $this->data['parent_id'] ? '/index/' . $this->data['parent_id'] : '')
            );

        # 4. Генерую контент і виводжу на екран

        return $this->renderContent();
    }


    /**
     * @param $id
     * @return mixed|string
     */
    public function process($id)
    {
        parent::process($id);

        return json_encode($this->getFormResponse());
    }

}