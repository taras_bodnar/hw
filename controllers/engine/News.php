<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 31.05.14 18:36
 */

namespace controllers\engine;

defined('SYSPATH') or die();

/**
 * Class News
 * @package controllers\engine
 */
class News extends Content {

    public function __construct()
    {
        parent::__construct();

        // встановлюю тип контенту сторінка
        $this->setType('news');
    }



    /**
     * список сторінок
     * @param string $parent_id
     * @return mixed|string
     */
    public function index($parent_id = '')
    {
        $this->setButtons
            (
                Form::button(
                    $this->lang->core['create'],
                    Form::icon('icon-file'),
                    array(
                        'class'   => Form::BTN_TYPE_PRIMARY,
                        'onclick' => "self.location.href='News/create/{$parent_id}'"
                    ))
            );
        if($parent_id > 0 ){
            $this->data = $this->ms->data('content', $parent_id);
            if($this->data['parent_id'] > 0){
                $this->prependToButtons(
                    Form::link(
                        $this->lang->core['back'],
                        Form::icon('icon-external-link'),
                        array(
                            'class'=>'btn-link',
                            'href' => 'News/index/' . $this->data['parent_id']
                        )
                    )
                );
            }
        }

        $t = new Table('News', "News/items/$parent_id");
        $t
           ->setConf('sortable', true)
           ->setConf('columns', array(
                'orderable'=> false,

            ))
           ->sortableConf('content','id')
           ->setTitle($this->lang->pages['table_title'])
           ->addTh($this->lang->content['id'], 'w-20')
           ->addTh($this->lang->content['name'])
           ->addTh($this->lang->core['func'] , 'w-200')
        ;

        $this->setContent($t->render());

        return $this->output();
    }

    /**
     * ajax response tale items
     * @param int $parent_id
     * @return string
     */
    public function items($parent_id=0)
    {
        $dt = new Table();
        return $dt
            -> table('content c')
            -> where("c.parent_id=$parent_id and c.type_id={$this->type_id}")
            -> join("join content_info i on i.content_id = c.id and i.languages_id = {$this->language_id}")
            -> columns(
                array(
                    'c.id',
                    'i.name',
                    'c.published',
                    'CONCAT("News/", IF(c.isfolder, "index/", "edit/"), c.id) as href'
                )
            )
            -> searchCol(array('c.id','i.name'))
            -> returnCol(array('c.id','name' , 'func'))
            -> formatCol(
                array(
                1 => '<a href="{href}">{name}</a>',
                2 => Form::link(
                        '',
                        Form::icon('icon-eye-{published}'),
                        array(
                           'class'    =>'btn-primary',
                            'title'   => $this->lang->core['published_tip'],
                            'onclick' => 'engine.pages.pub({id},{published})'
                        )
                    )  .
                    Form::link(
                        '',
                        Form::icon('icon-edit'),
                        array(
                           'class'  =>'btn-info',
                            'title' => $this->lang->core['edit'],
                            'href'  => 'News/edit/{id}'
                        )
                    ) .
                    Form::button(
                        '',
                        Form::icon('icon-remove'),
                        array(
                            'title'   => $this->lang->core['delete'],
                            'class'   =>'btn-danger',
                            'onclick' => 'engine.pages.delete({id})'
                        )
                    )
            ))
            -> request();
    }

    /**
     * auto crate page id vs default params
     * @param int $parent_id
     * @return mixed|string
     */
    public function create($parent_id=0)
    {
        return parent::create($parent_id);
    }

    /**
     * edit page
     * @param $id
     * @return mixed|string
     */
    public function edit($id)
    {
        # 1. Встановлюю які поля потрібно відображати у формі
        $this->configFormField('nav_menu',0);
        $this->configFormField('module', 0);
        $this->configFormField('case_title', 0);

        # 2. Заповнюю контент даними по замовчуванню
        parent::edit($id);

        # 2.a Додаю кнопочку назад

        if($this->data['parent_id'] > 0){
            $this->prependToButtons(
                Form::link(
                    $this->lang->core['back'],
                    Form::icon('icon-external-link'),
                    array(
                        'class'=>'btn-link',
                        'href' => 'News/index/' . $this->data['parent_id']
                    )
                )
            );
        }
        # 3. Додаю параметри для форми

        $selected_nav_menu = $this->ms->selectedNavMenu($id, $this->created);
        $this->formAppendData('form_action',       "News/process/$id")
             ->formAppendData('nav_menu',          $this->ms->getNavMenus())
             ->formAppendData('selected_nav_menu', $selected_nav_menu)
             ->formAppendData('redirect_url',     'News' .
                (isset($this->data['parent_id']) && $this->data['parent_id'] ? '/index/' . $this->data['parent_id']  : '')
            );

        # 4. Генерую контент і виводжу на екран

        return $this->renderContent();
    }


    /**
     * @param $id
     * @return mixed|string
     */
    public function process($id)
    {
        parent::process($id);

        $this->saveNavMenus($id);

        return json_encode($this->getFormResponse());
    }

    /**
     * видалення сторінки
     * @param $id
     * @return mixed 1
     */
    public function delete($id)
    {
        return parent::delete($id);
    }

    /**
     * @return string
     */
    public function tree()
    {
        $parent_id = isset($_POST['id']) ? (int)$_POST['id'] : 0;
        $tree=array();
        $r = $this->ms->tree($parent_id, $this->type_id);
        $i=0;
        if(!empty($r)){
            foreach ( $r as $row) {
                $tree[$i]['data'] = $row['name'] . ' #' . $row['id'] ;
                $tree[$i]['state'] = $row['isfolder'] ? 'closed' : '';
                $tree[$i]['attr'] = array(
                    'id' => $row['id'],
                    "rel"=> (($row['isfolder'])? 'folder':'file'),
//                    "href"=>   './News/'. (($row['isfolder'])? 'index' : 'edit') ."/" . $row['id'] ."/",
                    "href"=>   './News/edit/'. $row['id'] ."/",
                );
                $i++;
            }
        }

        return json_encode($tree);
    }

    /**
     * return controller content
     * @return string
     */
    protected function output()
    {
        $this->setSidebar(
            new Tree(
                'content',
                'News/tree/',
                '',
//                Tree::button($this->lang->pages['tree_add'], 'self.location.href="./pages/create";', 'icon-plus-sign'),
                array(
                    Tree::contextMenu($this->lang->pages['tree_cmenu_add'], 'icon-plus' ,'self.location.href="./News/create/"+id;'),
                    Tree::contextMenu($this->lang->pages['tree_cmenu_edit'], 'icon-pencil' ,'self.location.href="./News/edit/"+id;'),
                    Tree::contextMenu($this->lang->pages['tree_cmenu_list'], 'icon-list' ,'self.location.href="./News/index/"+id;'),
                    Tree::contextMenu($this->lang->pages['tree_cmenu_del'], 'icon-remove' ,'engine.pages.delete(id)')
                ),
                true
            )
        );

        return parent::output();
    }
}