<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 13.07.14 9:57
 */

namespace controllers\engine;

 
use controllers\Engine;

defined('SYSPATH') or die();

class Guides  extends Engine{

    public function __construct()
    {
        parent::__construct();

        $this->mg = $this->load->model('engine\Guides');
    }



    /**
     * @param int $parent_id
     * @return mixed
     */
    public function index($parent_id=0)
    {
        if(empty($parent_id)) $parent_id='';
        $buttons = array();

        if($parent_id > 0){
            $buttons[] = Form::button(
                $this->lang->core['back'],
                Form::icon('icon-external-link'),
                array(
                    'class'=>'btn-link',
                    'onclick' => "self.location.href='guides/index/{$parent_id}'"
                )
            );
        }

        $buttons[] = Form::button(
                $this->lang->core['create'],
                Form::icon('icon-file'),
                array(
                    'class'   => Form::BTN_TYPE_PRIMARY,
                    'onclick' => "self.location.href='./guides/create/{$parent_id}'"
                ));

        $t = new DataTables();
        $t -> ajaxConfig("guides/items/$parent_id")
            -> setId('guides')
//            -> setConfig('sortable', true)
            -> setConfig("order",
                array(
                    4,"asc"
                )
            )
            -> sortable('guides','sort')
            -> setDisplayLength(100)
            ->setTitle($this->lang->guides['table_title'])
            ->th($this->lang->guides['id'])
            ->th($this->lang->guides['name'])
            ->th($this->lang->guides['value'])
            ->th('Порядковий номер')
            ->th($this->lang->core['func'] , 'w-120')
        ;

        $this->setButtons($buttons);
        $this->setContent($t->render());

        return $this->output();
    }

    /**
     * table users group items
     * @param $parent_id
     * @return string
     */
    public function items($parent_id=0)
    {
        $t = new DataTables();
        $t
            -> table('guides g')
            -> where("g.parent_id=$parent_id ")
            -> join("join guides_info i on i.guides_id = g.id and i.languages_id = {$this->language_id}");
        $t
            ->get(
                array(
                    'g.id',
                    'i.name',
                    'g.value',
                    'g.sort',
                )
            )
            -> searchCol(array('g.id','g.value','i.name'))
            ->execute();

        $res = array();
        foreach ($t->getResults(false) as $i=>$row) {
            $res[$i][] = '<i class="icon-reorder" style="cursor:move" id="'. $row['id'] .'"></i>';
            $res[$i][] = $row['id'];
            $res[$i][] = "<a href='guides/index/{$row['id']}'>{$row['name']}</a>";
            $res[$i][] = $row['value'];
            $res[$i][] = $row['sort'];
            $res[$i][] = "
                    <a class='btn btn-primary' href='guides/edit/{$row['id']}'><i class='icon-edit icon-white'></i></a>
                    <a class='btn btn-danger' onclick='engine.guides.delete(".$row['id'].");' title='' ><i class='icon-remove icon-white'></i></a>
                    ";
        }

        return $t->renderJSON($res, $t->getTotal());

        $dt = new Table();
        return $dt
            -> table('guides g')
            -> where("g.parent_id=$parent_id ")
            -> join("join guides_info i on i.guides_id = g.id and i.languages_id = {$this->language_id}")
            -> columns(
                array(
                    'g.id',
                    'i.name',
                    'g.value'
                )
            )
            -> searchCol(array('g.id','g.value','i.name'))
            -> returnCol(array('g.id','name','value', 'func'))
            -> formatCol(
                array(
                    1 => '<a title="{name}" href="guides/index/{id}">{name}</a>',
                    3 =>
                        Form::link(
                            '',
                            Form::icon('icon-edit'),
                            array(
                                'class'  =>'btn-info',
                                'title' => $this->lang->core['edit'],
                                'href'  => 'guides/edit/{id}'
                            )
                        ) .
                        Form::button(
                            '',
                            Form::icon('icon-remove'),
                            array(
                                'title'   => $this->lang->core['delete'],
                                'class'   =>'btn-danger',
                                'onclick' => 'engine.guides.delete({id})'
                            )
                        )
                ))
            -> request();
    }

    /**
     * @param int $parent_id
     * @return mixed|string
     */
    public function create($parent_id=0)
    {
        $buttons= array(
            Form::button(
                $this->lang->core['back'],
                Form::icon('icon-external-link'),
                array(
                    'class'=>'btn-link',
                    'onclick' => 'self.location.href=\'guides'. (($parent_id > 0) ? "/index/$parent_id" : '') .'\''
                )
            ),
            Form::button(
                $this->lang->core['save'],
                Form::icon('icon-save', false),
                array(
                    'class'=>'btn-success form-submit'
                ))
        );

        $languages = $this->load->model('engine\Languages');

        $content = $this->load->view('guides_form',array(
            'languages' => $languages->all(),
            'action' => 'add',
            'id'        =>$parent_id,
        ));

        $this->setButtons($buttons);
        $this->setContent($content);

        return $this->output();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
//        $r = Request::instance()->title .= $this->lang->guides['title'];
        $data = $this->mg->data('guides', $id);
        $buttons= array(
            Form::button(
                $this->lang->core['back'],
                Form::icon('icon-external-link'),
                array(
                    'class'=>'btn-link',
                    'onclick' => 'self.location.href=\'guides'. (($data['parent_id'] > 0) ? "/index/{$data['parent_id']}" : '') .'\''
                )
            ),
            Form::button(
                $this->lang->core['save'],
                Form::icon('icon-save', false),
                array(
                    'class'=>'btn-success form-submit'
                ))
        );

        $languages = $this->load->model('engine\Languages');

        $content = $this->load->view('guides_form',array(
            'languages'  => $languages->all(),
            'action'     => 'edit',
            'data'       => $data,
            'info'       => $this->mg->info($id),
            'id'         => $id
        ));

        $this->setButtons($buttons);
        $this->setContent($content);

        return $this->output();
    }

    /**
     * @param $id
     * @return mixed|string
     */
    public function process($id=0)
    {
        $s=0;
        $e = $this->errors['warning'];
        $r = './guides/index/' . $id; // redirect url
        $data = $_POST['data'];
        $info = $_POST['info'];

        foreach ($info as $languages_id=>$arr) {
            if(empty($info[$languages_id]['name'])) {
                $this->error[] = $this->lang->guides['e_name'];
            }
        }

        // add or update values
        if(empty($this->error)) {
//            $data['parent_id'] = isset($data['parent_id']) ? $data['parent_id'] : 0;
            switch($_POST['action']){
                case 'add':
                    if($id > 0) {
                        $data['parent_id'] = $id;
                    }
                    $s = $this->mg->create($data, $info);
                    if($s > 0) {
                        $e = $this->errors['success'];
                        $this->error[] = $this->lang->guides['save_success'];
                    } else {
                        $e = $this->errors['error'];
                        $this->error[] = $this->mg->error();
                    }
                    break;
                case 'edit':
                    if(empty($id)) return '';
                    $r='';
                    $s = $this->mg->update($id, $data, $info);
                    if($s > 0) {
                        $e = $this->errors['success'];
                        $this->error[] = $this->lang->guides['save_success'];
                    } else {
                        $e = $this->errors['error'];
                        $this->error[] = $this->mg->error() ;
                    }
                    break;
                default:
                    break;
            }
        }

        return json_encode(array(
            's' => $s > 0 , // status
            'r' => $r, // redirect url
            'e' => $e, // error class
            't' => $this->lang->core[$e], // error title
            'm' => implode('<br>', $this->error) // error message
        ));
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        return $this->mg->delete($id);
    }
} 