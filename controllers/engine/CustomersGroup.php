<?php
/**
 * Company Otakoyi.com
 * Author: wg
 * Date: 05.01.15 21:30
 */

namespace controllers\engine;

use controllers\Engine;

defined("SYSPATH") or die();

/**
 * Class CustomersGroup
 * @package controllers\engine
 */
class CustomersGroup extends Engine{

    private $rang=array(1, 99);

    public function __construct()
    {
        parent::__construct();

        $this->mg = $this->load->model('engine\UsersGroup');
    }

    /**
     * @param int $parent_id
     * @return mixed
     */
    public function index($parent_id=0)
    {
        if(empty($parent_id)) $parent_id='';
        $buttons= array(
            Form::button(
                $this->lang->core['create'],
                Form::icon('icon-file'),
                array(
                    'class'   => Form::BTN_TYPE_PRIMARY,
                    'onclick' => "self.location.href='CustomersGroup/create/{$parent_id}'"
                ))
        );

        $t = new Table('users_group', "CustomersGroup/items/$parent_id");
        $t
            ->setConf('sortable', true)
            // todo відключення сортування останньої колонки не працює
            ->setConf('columns', array(
                'orderable'=> false,

            ))
            ->setDisplayLength(30)
            ->sortableConf('users_group','id')
            ->setTitle($this->lang->customers_group['table_title'])
            ->addTh($this->lang->customers_group['id'])
            ->addTh($this->lang->customers_group['name'])
            ->addTh($this->lang->customers_group['rang'])
            ->addTh($this->lang->core['func'] , 'w-120')
        ;

        $this->setButtons($buttons);
        $this->setContent($t->render());

        return $this->output();
    }

    /**
     * table users group items
     * @param $parent_id
     * @return string
     */
    public function items($parent_id=0)
    {
        $dt = new Table();
//        $dt->debug(1);
        return $dt
            -> table('users_group g')
            -> where("g.parent_id=$parent_id ")
            -> where("g.rang between {$this->rang[0]} and {$this->rang[1]}")
            -> join("join users_group_info i on i.users_group_id = g.id and i.languages_id = {$this->language_id}")
            -> columns(
                array(
                    'g.id',
                    'i.name',
                    'i.description',
                    'g.rang'
                )
            )
            -> searchCol(array('g.id','g.rang','i.name'))
            -> returnCol(array('g.id','name','rang', 'func'))
            -> formatCol(
                array(
                    1 => '<a title="{description}" href="CustomersGroup/index/{id}">{name}</a>',
                    3 =>
                        Form::link(
                            '',
                            Form::icon('icon-edit'),
                            array(
                                'class'  =>'btn-info',
                                'title' => $this->lang->core['edit'],
                                'href'  => 'CustomersGroup/edit/{id}'
                            )
                        ) .
                        Form::button(
                            '',
                            Form::icon('icon-remove'),
                            array(
                                'title'   => $this->lang->core['delete'],
                                'class'   =>'btn-danger',
                                'onclick' =>  'engine.users.group.delete({id})' // todo зробити модальне підтвердження і подивитись де провав тайтл
                            )
                        )
                ))

            -> request();
    }

    /**
     * auto crate manufacturer id vs default params
     * @param int $parent_id
     * @return mixed|string
     */
    public function create($id='')
    {
        $buttons= array(
            Form::button(
                $this->lang->core['back'],
                Form::icon('icon-external-link'),
                array(
                    'class'=>'btn-link',
                    'onclick' => 'self.location.href=\'CustomersGroup/index/'.$id.'\''
                )
            ),
            Form::button(
                $this->lang->core['save'],
                Form::icon('icon-save', false),
                array(
                    'class'=>'btn-success form-submit'
                ))
        );

        $languages = $this->load->model('engine\Languages');

        $content = $this->load->view(
            'users/group_form',
            array(
                'languages'  => $languages->all(),
                'action'     => 'create',
                'rang'       => $this->rang,
                'id'         => 0,
                'form_action'=> 'CustomersGroup/process',
                'parents'    => $this->mg->parents(0, 0, $this->rang[0], $this->rang[1]),
                'plugins'    => $this->getPlugins($id, 'edit')
            ));

        $this->setButtons($buttons);
        $this->setContent($content);

        return $this->output();
    }


    /**
     * edit manufacturer
     * @param $id
     * @return mixed|string
     */
    public function edit($id)
    {
        $buttons= array(
            Form::button(
                $this->lang->core['back'],
                Form::icon('icon-external-link'),
                array(
                    'class'=>'btn-link',
                    'onclick' => 'self.location.href=\'CustomersGroup/index\''
                )
            ),
            Form::button(
                $this->lang->core['save'],
                Form::icon('icon-save', false),
                array(
                    'class'=>'btn-success form-submit'
                ))
        );

        $languages = $this->load->model('engine\Languages');

        // дерево структури у вигляді select
        $data = $this->mg->data('users_group', $id);

        $content = $this->load->view(
            'users/group_form',
            array(
                'languages'  => $languages->all(),
                'action'     => 'edit',
                'data'       => $data,
                'info'       => $this->mg->info($id),
                'id'         => $id,
                'rang'      => $this->rang,
                'form_action'      => 'CustomersGroup/process',
                'parents'    => $this->mg->parents($data['parent_id'], $id, $this->rang[0], $this->rang[1]),
                'plugins'    => $this->getPlugins($id, 'edit')
            ));

        $this->setButtons($buttons);
        $this->setContent($content);

        return $this->output();
    }


    /**
     * @param $id
     * @return mixed|string
     */

    public function process($id='')
    {
        $s=0;
        $e = $this->errors['warning'];
        $r = 'CustomersGroup/index/'; // redirect url
        $data = $_POST['data'];
        $info = $_POST['info'];

        // check required fields
        if(
        empty($data['rang'])
        ) {
            $this->error[] = $this->lang->customers_group['e_required_fields'];
        }
        foreach ($info as $languages_id=>$arr) {
            if(empty($info[$languages_id]['name'])) {
                $this->error[] = $this->lang->customers_group['e_name'];
            }
        }

        // add or update values
        if(empty($this->error)) {
            $data['parent_id'] = isset($data['parent_id']) ? $data['parent_id'] : 0;
            if($data['parent_id'] > 0){
                $r .= $data['parent_id'];
            }
            switch($_POST['action']){
                case 'create':
                    $s = $this->mg->create($data, $info);
                    if($s > 0) {
                        $e = $this->errors['success'];
                        $this->error[] = $this->lang->customers_group['save_success'];
                    } else {
                        $e = $this->errors['error'];
                        $this->error[] = $this->mg->error();
                    }
                    break;
                case 'edit':
                    if(empty($id)) return '';
                    $r='';
                    $s = $this->mg->update($id, $data, $info);
                    if($s > 0) {
                        $e = $this->errors['success'];
                        $this->error[] = $this->lang->customers_group['save_success'];
                    } else {
                        $e = $this->errors['error'];
                        $this->error[] = $this->mg->error() ;
                    }
                    break;
                default:
                    break;
            }
            $this->getPlugins($id, 'process');
        }

        return json_encode(array(
            's' => $s > 0 , // status
            'r' => $r, // redirect url
            'e' => $e, // error class
            't' => $this->lang->core[$e], // error title
            'm' => implode('<br>', $this->error) // error message
        ));
    }

    /**
     * видалення сторінки
     * @param $id
     * @return mixed 1
     */
    public function delete($id)
    {
        return $this->mg->delete($id);
    }
}