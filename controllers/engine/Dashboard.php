<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 06.05.14 22:34
 */
namespace controllers\engine;

use controllers\Engine;

if ( !defined("SYSPATH") ) die();

/**
 * Class Dashboard
 * @package controllers\engine
 */
class Dashboard extends Engine
{
    /**
     * список віджетів
     * @var array
     */
    private $widgets = array();

    public function __construct()
    {
        parent::__construct();

        $this->md = $this->load->model('engine\Dashboard');
    }



    /**
     * index method
     * @return string
     */
    public function index()
    {
//        $u = new Update();
        return $this->load->view(
            'dashboard/index',
            array(
                'title'   => $this->md->engineName(),
                'content' => $this->quick_launch(),
                'updates' => '', //$u->check(),
                'widgets' => $this->widgets()
            )
        );
    }



    /** ***************************** begin widgets *****************************************  */
    private function widgets()
    {
        $this->widgetLastPages('page','primary');
        $this->widgetLastPages('product','primary', true);
        $this->widgetLastPages('category', 'primary');
        $this->widgetLastPages('manufacturer', 'primary');
        $this->widgetDiscSpace();
        $this->widgetOrders();
        return $this->load->view(
           'dashboard/widgets/index',
            array(
                'content' => implode(' ', $this->widgets)
            )
        );
    }

    /**
     * @return $this
     */
    private function widgetDiscSpace()
    {
        $total = 100;
        $_total = disk_total_space($_SERVER['DOCUMENT_ROOT']);
        $_free  = disk_free_space($_SERVER['DOCUMENT_ROOT']);

        $p = 100 / $_total;
//        $busy = round($_total - $_free  * $p);
        $free = ceil($_free  * $p);
//        $free = $p;
        $busy = $total - $free;


        $this->widgets[] = $this->load->view(
            'dashboard/widgets/disc_space',
            array(
                'data' => array(
                    array(
                        'label' => $this->lang->dashWigets['disc_space_free'],
                        'value' =>  $free
                    ),
                    array(
                        'label' => $this->lang->dashWigets['disc_space_busy'],
                        'value' =>  $busy
                    )
                )
            )
        );

        return $this;
    }

    private function widgetOrders()
    {
        $total = $this->md->getTotalOrders();
        $pendig_all = $this->md->getOrders('pending');
        $confirmed_all = $this->md->getOrders('confirmed');

        $this->md->setWhere(' and createdon BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE()');
        $pendig_30 = $this->md->getOrders('pending');
        $confirmed_30 = $this->md->getOrders('confirmed');



        $this->widgets[] = $this->load->view(
            'dashboard/widgets/orders_diagram',
            array(
                'data' => array(
                    array(
                        'label' => 'Запитів за весь час',
                        'value' =>$pendig_all
                    ),
                    array(
                        'label' => 'Запитів за 30 днів',
                        'value' =>  $pendig_30
                    ),
                    array(
                        'label' => 'Замовлень за весь час',
                        'value' =>  $confirmed_all
                    ),
                    array(
                        'label' => 'Замовлень за 30 днів',
                        'value' =>  $confirmed_30
                    ),

                )
            )
        );

        return $this;
    }

    public function widgetLastPages($type='page', $color='success', $img=false)
    {
        $title = '';
        $pages = $this->md->getLastPages($type, $img);
        if(empty($pages)) return '';

        foreach ($pages as $i=>$page) {
            $pages[$i]['editedon'] = $this->timeAgo(strtotime($page['editedon']));
        }

        switch($type){
            case 'page':
                $title = $this->lang->dashWigets['lp_title'];
                $type = 'pages';
                break;
            case 'category':
                $title = $this->lang->dashWigets['lp_cat'];
                $type = 'categories';
                break;
            case 'manufacturer':
                $title = $this->lang->dashWigets['lp_mf'];
                $type = 'manufacturers';
                break;
            case 'product':
                $title = $this->lang->dashWigets['lp_product'];
                $type = 'manufacturers';
                break;

        }
        $this->widgets[] = $this->load->view(
            'dashboard/widgets/last_pages',
            array(
                'pages' => $pages,
                'type'  => $type,
                'title' => $title,
                'color' => $color
            )
        );
        return $this;
    }
    /** ***************************** end widgets *****************************************  */


    /** ***************************** begin quickLaunch *****************************************  */

    public function quick_launch()
    {
        return $this->load->view(
            'dashboard/quick_launch',
            array(
                'items' => $this->md->quickLaunchItems()
            )
        );
    }

    public function quickLaunchConfig()
    {
        return $this->load->view(
            'dashboard/quick_launch_config',
            array(
                'items' => $this->md->structureItems()
            )
        );
    }

    public function quickLaunchConfigSave()
    {
        // clear previos items
        $this->md->clearQuickLaunchItems();
        if(!isset($_POST['items'])) return 1;

        $i=0;
        foreach ($_POST['items'] as $k=>$components_id) {
            $this->md->quickLaunchAddItem($components_id,$i);
            $i++;
        }
        return 1;
    }
    public function refreshQuickLaunch()
    {
        return json_encode(array('items' => $this->md->quickLaunchItems()));
    }

    public function quickLaunchSort($sort)
    {
        $sort = explode(',', $sort);
        foreach ($sort as $i=>$components_id) {
            $this->md->quickLaunchSort($components_id, $i);
        }
        return 1;
    }

    /** ***************************** end quickLaunch *****************************************  */



    private function timeAgo($ptime)
    {
        $etime = time() - $ptime;

        if ($etime < 1)
        {
            return '0 seconds';
        }

        $a = array( 365 * 24 * 60 * 60  =>  'year',
                    30 * 24 * 60 * 60  =>  'month',
                    24 * 60 * 60  =>  'day',
                    60 * 60  =>  'hour',
                    60  =>  'minute',
                    1  =>  'second'
        );

        foreach ($a as $secs => $str)
        {
            $d = $etime / $secs;
            if ($d >= 1)
            {
                $r = round($d);
                return $r . ' ' .
                    (
                        $r > 1 ?
                            isset($this->lang->time_ago[$str]) ? $this->lang->time_ago[$str] : $str
                        : $str
                    )
                    . ' ' . $this->lang->time_ago['ago'];
            }
        }
    }



    /**
     * @return mixed
     */
    public function create(){}

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id){}
    /**
     * @param $id
     * @return mixed
     */
    public function delete($id){}

    /**
     * @param $id
     * @return mixed
     */
    public function process($id){}

} 