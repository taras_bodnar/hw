<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 16.07.14 15:47
 */

namespace controllers\engine\content;

use controllers\Engine;
use controllers\engine\Pages;
use controllers\core\Config;
use controllers\core\Settings;

defined('SYSPATH') or die();

include_once DOCROOT . "vendor/acimage/AcImage.php";

/**
 * Class Images
 *
 */
class Images extends Engine {

    /**
     * Upload dir
     * @var string
     */
    private $uploaddir;
    /**
     * thumbnails dir
     * @var string
     */
    private $thumnbs_dir = 'thumbnails';
    /**
     * source directory
     * @var string
     */
    private $source_dir  = 'source';

    public function __construct()
    {
        parent::__construct();


        $this->uploaddir   = Settings::instance()->get('content_images_dir');
        $this->thumnbs_dir = Settings::instance()->get('content_images_thumb_dir');
        $this->source_dir  = Settings::instance()->get('content_images_source_dir');
    }

    public function index(){}
    public function create(){}
    public function edit($content_id){}



    /**
     * delete image
     * @param $id
     * @return int|mixed
     */
    public function delete($id)
    {
        $im = $this->load->model('engine\content\Images');

        // ід сторінки
        $data = $im->imageData($id);
        $content_id = $data['content_id'];
        // визначу розміри
        $sizes = $im->getSizes($content_id);
        $sizes[]['name'] = $this->thumnbs_dir;
        $sizes[]['name'] = $this->source_dir;

        // формую шлях
        foreach ($sizes as $size) {
            $path = $_SERVER['DOCUMENT_ROOT'] . $this->uploaddir . $content_id . '/'. $size['name'] . '/' .$data['name'];
            @unlink($path);
        }

        return $im->delete($id);
    }
    public function process($content_id){}

    public function get($content_id)
    {

        $im = $this->load->model('engine\content\Images');

        $images = $im->get($content_id);
        $im_data = array();

        foreach ($images  as $image) {
            $im_data[] = $this->getTemplate(
                $content_id,
                array(
                'image'      => $image,
//                'path'       => $dir,
//                'path_view'  => $dir_preview,
                'content_id' => $content_id
            ));
        }

        return $this->load->view('content/images/list', array(
            'id'=>$content_id,
            'images' => $im_data
        ));
    }

    public function getTemplate($content_id, $im_data=array())
    {
        // get thumbs path
        $thumb_path = $this->thumnbs_dir; // $im->thumbPath();
        $preview_path = $this->source_dir;// $im->previewSize($content_id);

        $dir = APPURL . ltrim($this->uploaddir, DIRECTORY_SEPARATOR) . $content_id.'/' . $thumb_path. '/';
        $dir_preview = APPURL . ltrim($this->uploaddir, DIRECTORY_SEPARATOR) . $content_id.'/' . $preview_path. '/';
        $im_data['path']      = $dir;
        $im_data['path_view'] = $dir_preview;
        $im_data['content_id'] = $content_id;

        return $this->load->view('content/images/template',$im_data);
    }

    public function editInfo($id)
    {
        $languages = $this->load->model('engine\Languages');

        $im = $this->load->model('engine\content\Images');
        $info = $im->getAlt($id);
        return $this->load->view(
            'content/images/info',
            array('info'=>$info, 'id' => $id, 'languages' => $languages->all(0,1)));
    }

    public function crop($content_id, $id)
    {
        $im = $this->load->model('engine\content\Images');
        $img = $this->uploaddir . $content_id.'/' . $this->source_dir . $im->imageData($id, 'name');

        return $this->load->view(
            'content/images/crop',
            array(
                'id'         => $id,
                'content_id' =>$content_id,
                'img'        => $img,
                'sizes'      => $im->cropSizes($content_id)
            )
        );
    }

    public function cropProcess($content_id, $images_id)
    {
        $data = $_POST['data'];
        $size = explode('x', $_POST['size']);
        $im = $this->load->model('engine\content\Images');

        $size_name = $im->getNameBySize($size[0], $size[1]);

        $source_img = DOCROOT . $this->uploaddir . $content_id.'/' . $this->source_dir . $im->imageData($images_id, 'name');
        $dest_img   = DOCROOT . $this->uploaddir . $content_id.'/' . $size_name .'/' . $im->imageData($images_id, 'name');

        $img = \AcImage::createImage($source_img);
        \AcImage::setQuality(100);
        \AcImage::setRewrite(true);
        $rect = new \Rectangle((int)$data['x'], (int)$data['y'], (int)$data['w'], (int)$data['h']);
        $img->crop($rect);
        $img->save($dest_img);

        return 1;
    }

    public function updateInfo($id)
    {
        $im = $this->load->model('engine\content\Images');
        return $im->update($id,$_POST['info']);
    }
    /**
     * upload image
     */
    public function upload($content_id)
    {
        if(!isset($_FILES['image'])) {
            $this->dump($_FILES);
            return 'empty files img';
        }
        // визначити тип сторінки,
        // визначити розміри , на які треба різати зображення
        // створю папки і завантажую
        $error = ""; $fnam='';

        $image = $_FILES['image'];
//        $this->dump($image);
        if(!empty($image['error'][0]))
        {
            switch($image['error'][0])
            {
                case '1':
                    $error = 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
                    break;
                case '2':
                    $error = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
                    break;
                case '3':
                    $error = 'The uploaded file was only partially uploaded';
                    break;
                case '4':
                    $error = 'No file was uploaded.';
                    break;

                case '6':
                    $error = 'Missing a temporary folder';
                    break;
                case '7':
                    $error = 'Failed to write file to disk';
                    break;
                case '8':
                    $error = 'File upload stopped by extension';
                    break;
                case '999':
                default:
                    $error = 'No error code avaiable';
            }
        }elseif(empty($image['tmp_name'][0]) || $image['tmp_name'][0] == 'none'){
            $error = 'No file was uploaded..';
        }else{
            $path = DOCROOT . $this->uploaddir;
            $s=0; // status
            // images model
            $im = $this->load->model('engine\content\Images');

            $ext = '.' . pathinfo($image['name'][0], PATHINFO_EXTENSION);

            $page_name = $im->imgName($content_id);
            $imgId = $im->imgId(); $imgId++;
            $sort  = $im->maxSort($content_id); $sort ++;

            $code = Config::instance()->get('languages.code');

            $image_name = Pages::translit($page_name, $code) . '-'.$imgId . $ext;
            $image_tmp  = $image['tmp_name'][0];

            $upload_dir = $path . $content_id . '/';
            if(!is_dir($upload_dir)) mkdir($upload_dir,0775);

            // upload to default folder
//die('oki');
            try
            {

                if(!is_dir($upload_dir . $this->source_dir)) mkdir($upload_dir . $this->source_dir,0775);
                if(!is_dir($upload_dir . $this->thumnbs_dir)) mkdir($upload_dir . $this->thumnbs_dir,0775);

                // source size
                $source_s = Settings::instance()->get('img_source_size');
                $sz = explode('x',$source_s);

                $img = \AcImage::createImage($image_tmp);
                \AcImage::setRewrite(true);
                \AcImage::setQuality(100);
                //source
                $img->resize((int)$sz[0], (int)$sz[1]);
                $img->save($upload_dir . $this->source_dir. $image_name);
                //thumbnail

//                $img->save($upload_dir . $this->thumnbs_dir. $image_name);

                $this->createSquare($upload_dir . $this->source_dir. $image_name, $upload_dir . $this->thumnbs_dir. $image_name, 125 );

                $sizes = $im->getSizes($content_id);
                if(!empty($sizes)){
                    foreach($sizes as $size){

                        $img = \AcImage::createImage($image_tmp);
                        \AcImage::setRewrite(true);
                        \AcImage::setQuality(100);

                        $size['path'] = $image_tmp;
                        $size_path = $upload_dir . $size['name'] .'/';

                        if(!is_dir($size_path)) mkdir($size_path, 0775);

                        if($size['width'] == 0) {
                            $img->resizeByHeight((int)$size['height']);
                            $img->save($size_path . $image_name);
                        } elseif($size['height'] == 0 ) {
                            $img->resizeByWidth((int)$size['width']);
                            $img->save($size_path . $image_name);
                        } elseif($size['width'] == $size['height']){
//                            $img->cropCenter('1pr', '1pr');
//                            $img->thumbnail((int)$size['width'], (int) $size['height']);
//                            $img->save($size_path . $image_name);
                            $this->createSquare($upload_dir . $this->source_dir. $image_name, $size_path .'/'. $image_name, $size['width'] );
                        } else {
                            if($size['width'] > $size['height']){

                                if(round($size['width'] / $size['height'], 1) == 1.8){
                                    $img->cropCenter('16pr', '9pr');
                                } elseif(round($size['width'] / $size['height'], 1) == 1.3){
                                    $img->cropCenter('4pr', '3pr');
                                }
                            } else{

                                if(round($size['height'] / $size['width'], 1) == 1.8){
                                    $img->cropCenter('9pr', '16pr');
                                } elseif(round($size['height'] / $size['width'], 1) == 1.3){
                                    $img->cropCenter('3pr', '4pr');
                                }
                            }
                            $img->resize((int)$size['width'], (int)$size['height']);
                            $img->save($size_path . $image_name);
                        }
                    }
                }

               $image_id = $im->create(array(
                   'content_id' => $content_id,
                   'sort'       => $sort,
                   'name'       => $image_name
               ));
                return json_encode(array(
                    'id'   => $image_id,
                    'name' => $image_name
                ));
            }
            catch (\FileNotFoundException $ex)
            {
                die($ex->getMessage());
            }
        }
        return json_encode(array(
            'id'   => 0,
            'name' => 'error'
        ));
    }

    /**
     * CREATE SQUARE IMAGE(thumbnail)
     * @param $original_file
     * @param null $destination_file
     * @param int $square_size
     * @return string
     */
    public function createSquare($original_file, $destination_file = null, $square_size = 96){

        // get width and height of original image
        $imagedata = getimagesize($original_file);
        $original_width = $imagedata[0];
        $original_height = $imagedata[1];

        if($original_width > $original_height){
            $new_height = $square_size;
            $new_width = $new_height*($original_width/$original_height);
        }
        if($original_height > $original_width){
            $new_width = $square_size;
            $new_height = $new_width*($original_height/$original_width);
        }
        if($original_height == $original_width){
            $new_width = $square_size;
            $new_height = $square_size;
        }

        $new_width = round($new_width);
        $new_height = round($new_height);

        // load the image
        if(substr_count(strtolower($original_file), ".jpg") or substr_count(strtolower($original_file), ".jpeg")){
            $original_image = imagecreatefromjpeg($original_file);
        }
        if(substr_count(strtolower($original_file), ".gif")){
            $original_image = imagecreatefromgif($original_file);
        }
        if(substr_count(strtolower($original_file), ".png")){
            $original_image = imagecreatefrompng($original_file);
        }

        $smaller_image = imagecreatetruecolor($new_width, $new_height);
        $square_image = imagecreatetruecolor($square_size, $square_size);

        imagecopyresampled($smaller_image, $original_image, 0, 0, 0, 0, $new_width, $new_height, $original_width, $original_height);

        if($new_width>$new_height){
            $difference = $new_width-$new_height;
            $half_difference =  round($difference/2);
            imagecopyresampled($square_image, $smaller_image, 0-$half_difference+1, 0, 0, 0, $square_size+$difference, $square_size, $new_width, $new_height);
        }
        if($new_height>$new_width){
            $difference = $new_height-$new_width;
            $half_difference =  round($difference/2);
            imagecopyresampled($square_image, $smaller_image, 0, 0-$half_difference+1, 0, 0, $square_size, $square_size+$difference, $new_width, $new_height);
        }
        if($new_height == $new_width){
            imagecopyresampled($square_image, $smaller_image, 0, 0, 0, 0, $square_size, $square_size, $new_width, $new_height);
        }


        // if no destination file was given then display a png
        if(!$destination_file){
            imagepng($square_image,NULL,9);
        }

        // save the smaller image FILE if destination file given
        if(substr_count(strtolower($destination_file), ".jpg")){
            imagejpeg($square_image,$destination_file,100);
        }
        if(substr_count(strtolower($destination_file), ".gif")){
            imagegif($square_image,$destination_file);
        }
        if(substr_count(strtolower($destination_file), ".png")){
            imagepng($square_image,$destination_file,9);
        }

        imagedestroy($original_image);
        imagedestroy($smaller_image);
        imagedestroy($square_image);

    }
}