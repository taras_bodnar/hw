<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 05.07.14 20:38
 */

namespace controllers\engine\content;

use controllers\engine\Form;
use controllers\engine\Table;
use controllers\Engine;
use controllers\core\Settings;

defined('SYSPATH') or die();

class Type extends Engine{

    public function __construct()
    {
        parent::__construct();

        $this->mg = $this->load->model('engine\content\Type');
    }

    /**
     * @return mixed
     */
    public function index()
    {
        $buttons= array(
            Form::button(
                $this->lang->core['create'],
                Form::icon('icon-file'),
                array(
                    'class'   => Form::BTN_TYPE_PRIMARY,
                    'onclick' => "self.location.href='./content/type/create'"
                ))
        );

        $t = new Table('content_type', "content/type/items");
        $t  ->setTitle($this->lang->content_type['table_title'])
            ->setConf('sortable', true)
            ->setConf('columns', array(
                'orderable'=> false
            ))
            ->sortableConf('content_type','id', 'id')
            ->addTh($this->lang->content_type['id'])
            ->addTh($this->lang->content_type['type'])
            ->addTh($this->lang->content_type['name'])
            ->addTh($this->lang->core['func'] , 'w-120')
        ;

        // render content view
        $content = $t->render();

        $this->setButtons($buttons);
        $this->setContent($content);

        return $this->output();
    }

    /**
     * table users group items
     * @return string
     */
    public function items()
    {
        $dt = new Table();
        return $dt
            -> table('content_type')
            -> columns(
                array(
                    'id',
                    'type',
                    'name'
                )
            )
            -> searchCol(array('id', 'type', 'name'))
            -> returnCol(array('id', 'type', 'name', 'func'))
            -> formatCol(
                array(
                    1 => '<a href="content/templates/index/{id}">{type}</a>',
                    3 =>
                        Form::link(
                            '',
                            Form::icon('icon-edit'),
                            array(
                                'class'  =>'btn-info',
                                'title' => $this->lang->core['edit'],
                                'href'  => 'content/type/edit/{id}'
                            )
                        ) .
                        Form::button(
                            '',
                            Form::icon('icon-remove'),
                            array(
                                'title'   => $this->lang->core['delete'],
                                'class'   =>'btn-danger',
                                'onclick' => 'engine.content.type.delete({id})'
                            )
                        )
                ))
            -> request();
    }

    /**
     * @return mixed|string
     */
    public function create()
    {
        $buttons= array(
            Form::button(
                $this->lang->core['back'],
                Form::icon('icon-external-link'),
                array(
                    'class'=>'btn-link',
                    'onclick' => 'self.location.href=\'./content/type/index\''
                )
            ),
            Form::button(
                $this->lang->core['save'],
                Form::icon('icon-save', false),
                array(
                    'class'=>'btn-success form-submit'
                ))
        );

        $content = $this->load->view('content/type_form',array(
            'action' => 'create',
            'id'     => 0,
            'images_sizes'    => $this->getSizes(),
        ));

        $this->setButtons($buttons);
        $this->setContent($content);

        return $this->output();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $buttons= array(
            Form::button(
                $this->lang->core['back'],
                Form::icon('icon-external-link'),
                array(
                    'class'=>'btn-link',
                    'onclick' => 'self.location.href=\'content/type/index\''
                )
            ),
            Form::button(
                $this->lang->core['save'],
                Form::icon('icon-save', false),
                array(
                    'class'=>'btn-success form-submit'
                ))
        );

        $content = $this->load->view('content/type_form',array(
            'action'     => 'edit',
            'data'       => $this->mg->data('content_type', $id),
            'id'         => $id,
            'images_sizes'    => $this->getSizes($id)
        ));


        $this->setButtons($buttons);
        $this->setContent($content);

        return $this->output();
    }

    /**
     * @param $id
     * @return mixed|string
     */
    public function process($id=0)
    {
        $s=0;
        $e = $this->errors['warning'];
        $r = './content/type/index'; // redirect url
        $data = $_POST['data'];

        if(
            empty($data['name']) ||
            empty($data['type'])
        ) {
            $this->error[] = $this->lang->core['e_required_fields'];
        }
        // add or update values
        if(empty($this->error)) {
            switch($_POST['action']){
                case 'create':
                    $s = $this->mg->create($data);
                    if($s > 0) {
                        $e = $this->errors['success'];
                        $this->error[] = $this->lang->content_type['save_success'];
                        $this->createDir($data['type']);

                        // create default template
                        $ct = new Templates();
                        $ct->createDefaultTemplate($s);
                    } else {
                        $e = $this->errors['error'];
                        $this->error[] = $this->mg->error();
                    }
                    break;
                case 'edit':
                    if(empty($id)) return '';
                    $r='';
                    $s = $this->mg->update($id, $data);
                    if($s > 0) {
                        $e = $this->errors['success'];
                        $this->error[] = $this->lang->content_type['save_success'];
                    } else {
                        $e = $this->errors['error'];
                        $this->error[] = $this->mg->error() ;
                    }
                    break;
                default:
                    break;
            }
            if($id > 0) {
                $ct = isset($_POST['images_sizes']) ? $_POST['images_sizes'] : array();
                $this->saveSizes($id, $ct);
            }
        }

        return json_encode(array(
            's' => $s > 0 , // status
            'r' => $r, // redirect url
            'e' => $e, // error class
            't' => $this->lang->core[$e], // error title
            'm' => implode('<br>', $this->error) // error message
        ));
    }

    /**
     * create content type dir
     * @param $type
     * @return bool
     */
    public function createDir($type)
    {
        $current = Settings::instance()->get('engine_theme_current');
        $themes_path = Settings::instance()->get('themes_path');
        $views_path = Settings::instance()->get('app_views_path');
        $dir = DOCROOT .'/'. $themes_path . $current . '/' . $views_path . $type;
		if(! is_dir($dir))
		    return mkdir($dir);
    }

    private function removeDir($type)
    {
        $current = Settings::instance()->get('engine_theme_current');
        $themes_path = Settings::instance()->get('themes_path');
        $views_path = Settings::instance()->get('app_views_path');
        $dir = DOCROOT .'/'. $themes_path . $current . '/' . $views_path . $type;

        return $this->deleteFiles($dir);
    }

    private function saveSizes($id,$data)
    {
        $this->mg->clearSelectedSizes($id);
        if(empty($data)) return 0;
        foreach ($data as $k=>$images_sizes_id) {
            $this->mg->addSelectedSize($id, $images_sizes_id);
        }
        return 1;
    }
    /**
     * get content images sizes
     * @param int $type_id selected $type_id
     * @return mixed
     */
    private function getSizes($type_id=0)
    {
        $selected=array();
        if($type_id > 0) {
            $s= $this->mg->getSelectedSizes($type_id);
            foreach ($s as $row) {
                $selected[] = $row['id'];
            }
        }

        $t = $this->load->model('engine\content\Type');

        $sizes =$t->get();

        $res=array();$i=0;
        foreach ($sizes as $size) {
            $res[$i] = $size;
            $res[$i]['selected'] = in_array($size['id'], $selected) ? 'selected' : '';
            $i++;
        }

        return  $res;
    }



    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $type= $this->mg->getType($id);

        // remove dir vs all files
        $this->removeDir($type);

        return $this->mg->delete($id);
    }
} 
