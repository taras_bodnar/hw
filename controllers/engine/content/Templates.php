<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 05.07.14 20:38
 */

namespace controllers\engine\content;

use controllers\engine\Admin;
use controllers\engine\Form;
use controllers\engine\Table;
use controllers\Engine;
use controllers\core\Settings;

defined('SYSPATH') or die();

class Templates extends Engine{

    const EXT = '.tpl';

    public function __construct()
    {
        parent::__construct();

        $this->mg = $this->load->model('engine\content\Templates');
    }



    /**
     * @param int $type_id
     * @return mixed|string
     */
    public function index($type_id = 0)
    {
        if(empty($type_id)) $type_id = '';

        $buttons= array(
            Form::button(
                $this->lang->core['create'],
                Form::icon('icon-file'),
                array(
                    'class'   => Form::BTN_TYPE_PRIMARY,
                    'onclick' => "self.location.href='./content/templates/create/$type_id'"
                ))
        );

        $t = new Table('content_templates', "content/templates/items/$type_id");
        $t  ->setTitle($this->lang->content_templates['table_title'])
            ->setConf('sortable', true)
            ->setConf('columns', array(
                'orderable'=> false
            ))
            ->sortableConf('content_templates','id', 'id')
            ->addTh($this->lang->content_templates['id'])
            ->addTh($this->lang->content_templates['name'])
            ->addTh($this->lang->content_templates['desc'])
            ->addTh($this->lang->content_templates['path'])
            ->addTh($this->lang->content_templates['main'])
            ->addTh($this->lang->core['func'] , 'w-120')
        ;

        // render content view
        $this->setButtons($buttons);
        $this->setContent($t->render());

        return $this->output();
    }

    /**
     * @param int $type_id
     * @return string
     */
    public function items($type_id=0)
    {
        $yes = $this->lang->core['btn_yes'];
        $no = $this->lang->core['btn_no'];
        $dt = new Table();
        return $dt
            -> table('content_templates')
            -> columns(
                array(
                    'id',
                    'name',
                    'description',
                    'path',
                    "IF(main = 1, '$yes', '$no' ) as def"
                )
            )
            ->where(empty($type_id) ? '' : " type_id = $type_id ")
            -> searchCol(array('id', 'path', 'name', 'description'))
            -> returnCol(array('id', 'name','description', 'path', "def",  'func'))
            -> formatCol(
                array(
                    1 => '<a href="content/templates/edit/{id}">{name}</a>',
                    5 =>
                        Form::link(
                            '',
                            Form::icon('icon-edit'),
                            array(
                                'class'  =>'btn-info',
                                'title' => $this->lang->core['edit'],
                                'href'  => 'content/templates/edit/{id}'
                            )
                        ) .
                        Form::button(
                            '',
                            Form::icon('icon-remove'),
                            array(
                                'title'   => $this->lang->core['delete'],
                                'class'   =>'btn-danger',
                                'onclick' => 'engine.content.templates.delete({id})'
                            )
                        )
                ))
            -> request();
    }

    /**
     * @param int $type_id
     * @return mixed|string
     */
    public function create($type_id=0)
    {
        $buttons= array(
            Form::button(
                $this->lang->core['back'],
                Form::icon('icon-external-link'),
                array(
                    'class'=>'btn-link',
                    'onclick' => 'self.location.href=\'./content/templates/index/'. $type_id .'\''
                )
            ),
            Form::button(
                $this->lang->core['save'],
                Form::icon('icon-save', false),
                array(
                    'class'=>'btn-success form-submit'
                ))
        );

        $content = $this->load->view('content/templates_form',array(
            'action' => 'create',
            'id'     => 0,
            'type'   =>$this->mg->types()
        ));

        $this->setButtons($buttons);
        $this->setContent($content);

        return $this->output();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {

        $data = $this->mg->data('content_templates', $id);

        $buttons= array(
            Form::button(
                $this->lang->core['back'],
                Form::icon('icon-external-link'),
                array(
                    'class'=>'btn-link',
                    'onclick' => 'self.location.href=\'content/templates/index/'. $data['type_id'] .'\''
                )
            ),
            Form::button(
                $this->lang->core['save'],
                Form::icon('icon-save', false),
                array(
                    'class'=>'btn-success form-submit'
                ))
        );

        $template = '';
        if($data['path'] != '') {
            $template = $this->readFile($data['type_id'], $data['path']);
        }

        $content = $this->load->view('content/templates_form',array(
            'action'     => 'edit',
            'data'       => $data,
            'id'         => $id,
            'type'   =>$this->mg->types($data['type_id']),
            'template' => $template
        ));

        // output

        $this->setButtons($buttons);
        $this->setContent($content);

        return $this->output();
    }

    /**
     * @param $id
     * @return mixed|string
     */
    public function process($id=0)
    {
        $s=0;
        $e = $this->errors['warning'];
        $r = './content/templates/index'; // redirect url
        $data = $_POST['data'];

        // check required fields
        if(
            empty($data['name'])
        ) {
            $this->error[] = $this->lang->core['e_required_fields'];
        }
        // add or update values
        if(empty($this->error)) {
            switch($_POST['action']){
                case 'create':
                    $id = $this->mg->create($data);
                    if($id > 0) {
                        $s = $id;
                        $e = $this->errors['success'];
                        $this->error[] = $this->lang->content_templates['save_success'];
                    } else {
                        $e = $this->errors['error'];
                        $this->error[] = $this->mg->error();
                    }
                    break;
                case 'edit':
                    if(empty($id)) return '';
                    $r='';
                    $s = $this->mg->update($id, $data);
                    if($s > 0) {
                        $e = $this->errors['success'];
                        $this->error[] = $this->lang->content_templates['save_success'];
                    } else {
                        $e = $this->errors['error'];
                        $this->error[] = $this->mg->error() ;
                    }
                    break;
                default:
                    break;
            }

            if(isset($data['main']) && $data['main'] == 1) {
                $this->mg->changMain($id, $data['type_id']);
            }

            if(!empty($data['path'])) {
                if($_POST['action'] == 'create') {
                    $template = $this->template($data['name'], $data['description'], $_POST['template']);
                    $path = $this->getPath($data['type_id']);
                    if(!empty($path)) {
                        // create file
                        if(!$this->writeFile($path, $data['path'], $template)){
                            $s=0;
                        }
                    }
                }

            }
        }

        return json_encode(array(
            's' => $s > 0 , // status
            'r' => $r, // redirect url
            'e' => $e, // error class
            't' => $this->lang->core[$e], // error title
            'm' => implode('<br>', $this->error) // error message
        ));
    }

    /**
     * @param $name
     * @param $description
     * @param $content
     * @return string
     */
    private function template($name, $description, $content='')
    {
        $author = Admin::data('name');
        return '{*
 * Company Otakoyi.com.
 * Author '. $author.'
 * Date: '. date('d.m.Y') .'
 * Time: '. date('H:i') .'
 * Name: '. $name .'
 * Description: '. $description .'
 *}
' . $content;
    }

    public function createDefaultTemplate($type_id)
    {
        $data = array(
            'type_id' => $type_id,
            'path'    => 'default',
            'name'    => 'Default',
            'description'    => 'Default template',
            'main'    => 1
        );

        $id = $this->mg->create($data);

        if($id > 0){
            $template = $this->template($data['name'], $data['description']);
            $path = $this->getPath($type_id);
            if(!empty($path)) {
                // create file
                if(!$this->writeFile($path, $data['path'], $template)){
                    return 0;
                }
            }
        }

        return $id;
    }
    private function readFile($type_id, $file)
    {
        $path = $this->getPath($type_id);
        $file_path = $path . $file . self::EXT;
        if(!file_exists($file_path)) return '';

        if (!$handle = fopen($file_path, 'r')) {
            echo "I can not open the file ($file_path)";
            exit;
        }
        if(filesize($file_path) == 0) return '';
        $template = fread($handle, filesize($file_path));
        fclose($handle);

        return $template;
    }
    private function createFile($path, $file)
    {
        $h = '';
        try{
            $h= fopen($path . $file . self::EXT, "w+");
        } catch (\Exception $e){
          $this->error[] = "I can not open the file ($path . $file)".  $e->getMessage();
        }
        return $h;
    }

    private function writeFile($path, $file, $data)
    {
        $h= $this->createFile($path, $file);

        if (fwrite($h, $data) === FALSE) {
            $this->error[] = "I can not write to the file ($path)";
            exit;
        }
        fclose($h);
        $fp = $path . $file . self::EXT;
        exec("chmod $fp 0775");
//        @ chmod($path . $file . self::EXT, 0777);
        return true;
    }

    private function getPath($type_id)
    {
        if(empty($type_id)) return '';

        $docroot = $_SERVER['DOCUMENT_ROOT'];

        $themes_dir = Settings::instance()->get('themes_path');
        $cur_theme  = Settings::instance()->get('app_theme_current');
        $views_path = Settings::instance()->get('app_views_path');

        $templates_path = '/'. $themes_dir . $cur_theme . '/' . $views_path;

        $type = $this->mg->getType($type_id);

        $type_path = $templates_path . $type . '/';

        if(!is_dir( $docroot . $type_path )){
            try{
                mkdir( $docroot . $type_path, 0777, true );
            } catch(\Exception $e){
                $this->error[] = $e->getMessage();
                return '';
            }
        }

        return $docroot . $type_path;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        return $this->mg->delete($id);
    }
}