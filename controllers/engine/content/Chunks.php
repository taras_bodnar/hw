<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 05.07.14 20:38
 */

namespace controllers\engine\content;

use controllers\engine\Admin;
use controllers\engine\Form;
use controllers\engine\Table;
use controllers\Engine;
use controllers\core\Settings;

defined('SYSPATH') or die();

class Chunks extends Engine{

    const EXT = '.tpl';

    public function __construct()
    {
        parent::__construct();

        $this->mg = $this->load->model('engine\content\Chunks');
    }



    /**
     * @return mixed|string
     */
    public function index()
    {
        $buttons= array(
            Form::button(
                $this->lang->core['create'],
                Form::icon('icon-file'),
                array(
                    'class'   => Form::BTN_TYPE_PRIMARY,
                    'onclick' => "self.location.href='./content/chunks/create'"
                ))
        );

        $t = new Table('chunks', "content/chunks/items");
        $t  ->setTitle($this->lang->chunks['table_title'])
            ->addTh($this->lang->chunks['id'])
            ->addTh($this->lang->chunks['name'])
            ->addTh($this->lang->chunks['path'])
            ->addTh($this->lang->core['func'] , 'w-120')
        ;

        $this->setButtons($buttons);
        $this->setContent($t->render());

        return $this->output();
    }

    /**
     * @return string
     */
    public function items()
    {
        $dt = new Table();
        return $dt
            -> table('chunks')
            -> columns(
                array(
                    'id',
                    'name',
                    'path'
                )
            )
            -> searchCol(array('id', 'path', 'name'))
            -> returnCol(array('id', 'name', 'path',  'func'))
            -> formatCol(
                array(
                    1 => '<a href="content/chunks/edit/{id}">{name}</a>',
                    3 =>
                        Form::link(
                            '',
                            Form::icon('icon-edit'),
                            array(
                                'class'  =>'btn-info',
                                'title' => $this->lang->core['edit'],
                                'href'  => 'content/chunks/edit/{id}'
                            )
                        ) .
                        Form::button(
                            '',
                            Form::icon('icon-remove'),
                            array(
                                'title'   => $this->lang->core['delete'],
                                'class'   =>'btn-danger',
                                'onclick' => 'engine.content.chunks.delete({id})'
                            )
                        )
                ))
            -> request();
    }

    /**
     * @return mixed|string
     */
    public function create()
    {
        $buttons= array(
            Form::button(
                $this->lang->core['back'],
                Form::icon('icon-external-link'),
                array(
                    'class'=>'btn-link',
                    'onclick' => 'self.location.href=\'./content/chunks/index\''
                )
            ),
            Form::button(
                $this->lang->core['save'],
                Form::icon('icon-save', false),
                array(
                    'class'=>'btn-success form-submit'
                ))
        );

        $content = $this->load->view('content/chunks_form',array(
            'action' => 'create',
            'id'     => 0
        ));

        $this->setButtons($buttons);
        $this->setContent($content);

        return $this->output();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {

        $data = $this->mg->data('chunks', $id);

        $buttons= array(
            Form::button(
                $this->lang->core['back'],
                Form::icon('icon-external-link'),
                array(
                    'class'=>'btn-link',
                    'onclick' => 'self.location.href=\'content/chunks/index\''
                )
            ),
            Form::button(
                $this->lang->core['save'],
                Form::icon('icon-save', false),
                array(
                    'class'=>'btn-success form-submit'
                ))
        );

        $template = '';
        if($data['path'] != '') {
            $template = $this->readFile($data['path']);
        }

        $content = $this->load->view('content/chunks_form',array(
            'action'     => 'edit',
            'data'       => $data,
            'id'         => $id,
            'template' => $template
        ));

        // output
        $this->setButtons($buttons);
        $this->setContent($content);

        return $this->output();
    }

    /**
     * @param $id
     * @return mixed|string
     */
    public function process($id=0)
    {
        $s=0;
        $e = $this->errors['warning'];
        $r = './content/chunks/index'; // redirect url
        $data = $_POST['data'];

        // check required fields
        if(
            empty($data['name'])
        ) {
            $this->error[] = $this->lang->core['e_required_fields'];
        }
        // add or update values
        if(empty($this->error)) {
            switch($_POST['action']){
                case 'create':
                    $id = $this->mg->create($data);
                    if($id > 0) {
                        $s = $id;
                        $e = $this->errors['success'];
                        $this->error[] = $this->lang->chunks['save_success'];
                    } else {
                        $e = $this->errors['error'];
                        $this->error[] = $this->mg->error();
                    }
                    break;
                case 'edit':
                    if(empty($id)) return '';
                    $r='';
                    $s = $this->mg->update($id, $data);
                    if($s > 0) {
                        $e = $this->errors['success'];
                        $this->error[] = $this->lang->chunks['save_success'];
                    } else {
                        $e = $this->errors['error'];
                        $this->error[] = $this->mg->error() ;
                    }
                    break;
                default:
                    break;
            }
            if(!empty($data['path'])) {
                if($_POST['action'] == 'create') {
                $author = Admin::data('name');
$_POST['template'] = '{*
 * Company Otakoyi.com.
 * Author '. $author.'
 * Date: '. date('d.m.Y') .'
 * Time: '. date('H:i') .'
 * Name: '. $data['name'] .'
 *}

' . $_POST['template'];
                }
                $path = $this->getPath();
                if(!empty($path)) {
                    // create file
                    if(!$this->writeFile($path, $data['path'], $_POST['template'])){
                        $s=0;
                    }
                }
            }
        }

        return json_encode(array(
            's' => $s > 0 , // status
            'r' => $r, // redirect url
            'e' => $e, // error class
            't' => $this->lang->core[$e], // error title
            'm' => implode('<br>', $this->error) // error message
        ));
    }

    private function readFile( $file)
    {
        $path = $this->getPath();
        $file_path = $path . $file . self::EXT;
        if(!file_exists($file_path)) return '';

        if (!$handle = fopen($file_path, 'r')) {
            echo "I can not open the file ($file_path)";
            exit;
        }
        if(filesize($file_path) == 0) return '';
        $template = fread($handle, filesize($file_path));
        fclose($handle);

        return $template;
    }
    private function createFile($path, $file)
    {
        $h = '';
        try{
            $h= fopen($path . $file . self::EXT, "w+");
        } catch (\Exception $e){
          $this->error[] = "I can not open the file ($path . $file)".  $e->getMessage();
        }
        return $h;
    }

    private function writeFile($path, $file, $data)
    {
        $h= $this->createFile($path, $file);

        if (fwrite($h, $data) === FALSE) {
            $this->error[] = "I can not write to the file ($path)";
            exit;
        }
        fclose($h);

        $fp = $path . $file . self::EXT;
        exec("chmod $fp 0775");
//        @ chmod($path . $file . self::EXT, 0777);
        return true;
    }

    private function getPath()
    {
        $docroot = $_SERVER['DOCUMENT_ROOT'];

        $themes_dir = Settings::instance()->get('themes_path');
        $cur_theme  = Settings::instance()->get('app_theme_current');
        $views_path = Settings::instance()->get('app_chunks_path');

        $chunks_path = '/'. $themes_dir . $cur_theme . '/' . $views_path;
        if(!is_dir( $docroot . $chunks_path )){
            try{
                mkdir( $docroot . $chunks_path, 0777, true );
            } catch(\Exception $e){
                $this->error[] = $e->getMessage();
                return '';
            }
        }
        return $docroot . $chunks_path;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $file = $this->mg->filePath($id);
        if(!empty($file)){
            $path = $this->getPath() . $file . self::EXT;
            @unlink($path);
        }
        return $this->mg->delete($id);
    }
} 