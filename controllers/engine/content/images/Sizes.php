<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 17.07.14 11:00
 */

namespace controllers\engine\content\images;

 
use controllers\core\Exceptions;
use controllers\Engine;
use controllers\engine\content\Images;
use controllers\engine\Form;
use controllers\engine\Table;
use controllers\core\Settings;

defined('SYSPATH') or die();

include_once DOCROOT . "vendor/acimage/AcImage.php";

class Sizes extends Engine {

    private $upload_dir = '/uploads/content/';

    public function __construct()
    {
        parent::__construct();

        $this->mg = $this->load->model('engine\content\images\Sizes');
    }



    /**
     * @return mixed|string
     */
    public function index()
    {
        $buttons= array(
            Form::button(
                $this->lang->core['create'],
                Form::icon('icon-file'),
                array(
                    'class'   => Form::BTN_TYPE_PRIMARY,
                    'onclick' => "self.location.href='./content/images/sizes/create'"
                ))
        );

        $t = new Table('images_sizes', "content/images/sizes/items");
        $t  ->setTitle($this->lang->images_sizes['table_title'])
            ->setConf('sortable', true)
            ->setConf('columns', array(
                'orderable'=> false
            ))
            ->sortableConf('images_sizes','id', 'id')
            ->addTh($this->lang->core['id'])
            ->addTh($this->lang->images_sizes['name'])
            ->addTh($this->lang->images_sizes['width'])
            ->addTh($this->lang->images_sizes['height'])
            ->addTh($this->lang->core['func'] , 'w-160')
        ;

        $this->setButtons($buttons);
        $this->setContent($t->render());

        return $this->output();
    }

    /**
     * @return string
     */
    public function items()
    {
        $dt = new Table();
        return $dt
            -> table('images_sizes')
            -> columns(
                array(
                    'id',
                    'name',
                    'width',
                    'height'
                )
            )
            -> searchCol(array('id', 'name', 'width','height'))
            -> returnCol(array('id', 'name', 'width','height', 'func'))
            -> formatCol(
                array(
                    1 => "<a href='content/images/sizes/edit/{id}'>{name}</a>",
                    4 =>
                        Form::link(
                            '',
                            Form::icon('icon-crop'),
                            array(
                                'class'  =>'btn-info',
                                'title' => $this->lang->images_sizes['crop_title'],
                                'onclick'  => 'engine.images.sizes.crop({id})'
                            )
                        ) .
                        Form::link(
                            '',
                            Form::icon('icon-edit'),
                            array(
                                'class'  =>'btn-info',
                                'title' => $this->lang->core['edit'],
                                'href'  => 'content/images/sizes/edit/{id}'
                            )
                        ) .
                        Form::button(
                            '',
                            Form::icon('icon-remove'),
                            array(
                                'title'   => $this->lang->core['delete'],
                                'class'   =>'btn-danger',
                                'onclick' => 'engine.images.sizes.delete({id})'
                            )
                        )
                ))
            -> request();
    }

    /**
     * @return mixed|string
     */
    public function create()
    {
        $buttons= array(
            Form::button(
                $this->lang->core['back'],
                Form::icon('icon-external-link'),
                array(
                    'class'=>'btn-link',
                    'onclick' => 'self.location.href=\'./content/images/sizes/index\''
                )
            ),
            Form::button(
                $this->lang->core['save'],
                Form::icon('icon-save', false),
                array(
                    'class'=>'btn-success form-submit'
                ))
        );


        $content = $this->load->view('content/images/sizes_form',array(
            'action'  => 'create',
            'id'      => 0,
            'content_type'    => $this->getType()
        ));

        $this->setButtons($buttons);
        $this->setContent($content);

        return $this->output();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $buttons= array(
            Form::button(
                $this->lang->core['back'],
                Form::icon('icon-external-link'),
                array(
                    'class'=>'btn-link',
                    'onclick' => 'self.location.href=\'content/images/sizes/index\''
                )
            ),
            Form::button(
                $this->lang->core['save'],
                Form::icon('icon-save', false),
                array(
                    'class'=>'btn-success form-submit'
                ))
        );
        $data = $this->mg->data('images_sizes', $id);

        $content = $this->load->view('content/images/sizes_form',array(
            'action'     => 'edit',
            'data'       => $data,
            'id'         => $id,
            'content_type'    => $this->getType($id)
        ));

        // output
        $this->setButtons($buttons);
        $this->setContent($content);

        return $this->output();
    }

    /**
     * @param $id
     * @return mixed|string
     */
    public function process($id=0)
    {
        $s=0;
        $e = $this->errors['warning'];
        $r = './content/images/sizes/index'; // redirect url
        $data = $_POST['data'];

        if(
            empty($data['name'])
//            empty($data['height']) ||
//            empty($data['width'])
        ) {
            $this->error[] = $this->lang->core['e_required_fields'];
        }
        // add or update values
        if(empty($this->error)) {
            switch($_POST['action']){
                case 'create':
                    $id = $this->mg->create($data);
                    if($id > 0) {
                        $s = $id;
                        $e = $this->errors['success'];
                        $this->error[] = $this->lang->images_sizes['save_success'];
                    } else {
                        $e = $this->errors['error'];
                        $this->error[] = $this->mg->error();
                    }
                    break;
                case 'edit':
                    if(empty($id)) return '';
                    $r='';
                    $s = $this->mg->update($id, $data);
                    if($s > 0) {
                        $e = $this->errors['success'];
                        $this->error[] = $this->lang->images_sizes['save_success'];
                    } else {
                        $e = $this->errors['error'];
                        $this->error[] = $this->mg->error() ;
                    }
                    break;
                default:
                    break;
            }
            if($id > 0) {
                $ct = isset($_POST['content_type']) ? $_POST['content_type'] : array();
                $this->saveTypes($id, $ct);
            }
        }

        return json_encode(array(
            's' => $s > 0 , // status
            'r' => $r, // redirect url
            'e' => $e, // error class
            't' => $this->lang->core[$e], // error title
            'm' => implode('<br>', $this->error) // error message
        ));
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        // get size name
        $size_name = $this->mg->data('images_sizes', $id, 'name');

        // remove all images of size
        $dir = DOCROOT . Settings::instance()->get('content_images_dir');
        if ($handle = @opendir($dir)) {
            while (false !== ($folder = readdir($handle))) {
                if ($folder != "." && $folder != "..") {
                    if ($h = @opendir($dir. $folder.'/'.$size_name)) {
                        while (false !== ($file = readdir($h))) {
                            if ($file != "." && $file != "..") {
                                $file_path = "$dir/$folder/$size_name/$file";
                                if(is_file($file_path)){
                                    unlink($file_path);
                                }
                            }
                        }
                        closedir($h);
                    }
                    $path = "$dir/$folder/$size_name";
                    if(is_dir($path)) @rmdir($path);
                }
            }
            closedir($handle);
        }
        return $this->mg->delete($id);
    }

    private function saveTypes($id,$data)
    {
        $this->mg->clearSelectedType($id);
        if(empty($data)) return 0;
        foreach ($data as $k=>$content_type_id) {
            $this->mg->addSelectedType($id,$content_type_id);
        }
        return 1;
    }

    /**
     * get content type
     * @param int $size_id selected size id
     * @return mixed
     */
    private function getType($size_id=0)
    {
        $selected=array();
        if($size_id > 0) {
            $s= $this->mg->getSelectedType($size_id);
            foreach ($s as $row) {
                $selected[] = $row['id'];
            }
        }

        $types =$this->mg->getTypes();

        $res=array();$i=0;
        foreach ($types as $type) {
            $res[$i] = $type;
            $res[$i]['selected'] = in_array($type['id'], $selected) ? 'selected' : '';
            $i++;
        }

        return  $res;
    }

    public function crop($sizes_id)
    {
        return $this->load->view(
            'content/images/sizes_crop',
            array(
                'sizes_id' => $sizes_id,
                'total'    => $this->mg->getTotalPages($sizes_id)
            )
        );
    }

    /**
     * автоматичний ресайз
     * @return int|string
     */
    public function cropProcess()
    {
        $sizes_id = (int) $_POST['sizes_id'];
        $start    = (int) $_POST['start'];
        if(empty($sizes_id)) return 0;

        $size = $this->mg->getSizeData($sizes_id);
        $images = $this->mg->getPageImages($sizes_id, $start);
        $content_id = $images['id'];

        if(! empty($images)){
            foreach ($images['images'] as $row) {

                $src = DOCROOT . $this->upload_dir . $content_id . '/source/' . $row['src'];
                $dest_dir = DOCROOT . $this->upload_dir . $content_id . '/' . $size['name'] .'/';

                if(!is_dir($dest_dir)){
                    if(! mkdir($dest_dir, 0775)){
                        return 'Неможу створити директорію ' . $dest_dir;
                    }
                }

                if(file_exists($dest_dir . $row['src'])){
                    unlink($dest_dir . $row['src']);
                }

//                echo $src , ' : ' , $dest_dir, '<br>';

                    if(!file_exists($src)) continue;
                try {
                    $img = \AcImage::createImage($src);

                    \AcImage::setRewrite(true);
                    \AcImage::setQuality(100);

                    if($size['width'] == 0) {
                        $img->resizeByHeight((int)$size['height']);
                        $img->save($dest_dir . $row['src']);
                    } elseif($size['height'] == 0 ) {
                        $img->resizeByWidth((int)$size['width']);
                        $img->save($dest_dir . $row['src']);
                    } elseif($size['width'] == $size['height']){
                        $im = new Images();
                        $im->createSquare($src, $dest_dir . $row['src'], $size['width'] );
                    } else {
                        $img->resize((int)$size['width'], (int)$size['height']);
                        $img->save($dest_dir . $row['src']);
                    }
                } catch (Exceptions $e){}
            }

        }

        return 1;
    }
} 