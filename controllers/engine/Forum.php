<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author Taras Bodnar.
 * Date: 07.03.16 17:34
 */

namespace controllers\engine;

use controllers\Engine;

defined('SYSPATH') or die();

/**
 * Class Posts
 * @package controllers\engine
 */
class Forum extends Engine {

    private $cat = array();
    protected $parent_id='';

    public function __construct()
    {
        parent::__construct();
    }

    public function install(){return 1;}
    public function uninstall(){return 1;}
    
    /**
     * список сторінок
     * @param string $parent_id
     * @return mixed|string
     */
    public function index($parent_id = '')
    {

        $t = new Table('Forum', "Forum/items/$parent_id");
        $t
//           ->setConf('sortable', true)
           ->setConf('columns', array(
                'orderable'=> false,
            ))
           -> sortableConf('content','id','id')
           -> setTitle($this->lang->blog['table_title'])
           -> addTh($this->lang->content['id'], 'w-20')
           -> addTh($this->lang->core['date'], 'w-20')
           -> addTh($this->lang->core['name'], 'w-20')
           -> addTh($this->lang->core['func'] , 'w-180')
        ;

        $this->setContent($t->render());

        return $this->output();
    }

    /**
     * ajax response tale items
     * @param int $parent_id
     * @return string
     */
    public function items($parent_id=0)
    {
//        die($parent_id);
        $dt = new Table();
        return $dt
            -> table('forum с')
            ->debug(false)
            -> where(($parent_id> 0 ? "с.cid={$parent_id} " : ''))
            -> columns(
                array(
                    'с.id',
                    'с.createdon',
                    'с.name',
                    'с.status'
                )
            )
            -> searchCol(array('с.id','с.createdon','с.name'))
            -> returnCol(array('с.id','createdon','name' , 'func'))
            -> formatCol(
                array(
                0 => '{id}',
                1 => '{createdon}',
                2 => '<a href="{href}">{name}</a>',
                3 =>  Form::button(
                    '',
                    Form::icon('icon-remove'),
                    array(
                        'title'   => $this->lang->core['delete'],
                        'class'   =>'btn-danger',
                        'onclick' => 'engine.blog.posts.del({id})'
                    )
                )
            ))
            -> request();
    }

    /**
     * auto crate page id vs default params
     * @param int $parent_id
     * @return mixed|string
     */
    public function create($parent_id=0)
    {
        $this->parent_id = $parent_id;
        return parent::create($parent_id);
    }

    /**
     * edit page
     * @param $id
     * @return mixed|string
     */
    public function edit($id)
    {
        # 1. Встановлюю які поля потрібно відображати у формі
        $this->configFormField('nav_menu',0);

        # 2. Заповнюю контент даними по замовчуванню
        parent::edit($id);

        # 2.a Додаю кнопочку назад

        $this->prependToButtons(
            Form::button(
                $this->lang->core['back'],
                Form::icon('icon-external-link'),
                array(
                    'class'=>'btn-link',
                    'onclick' => "self.location.href='Forum/index/{$this->parent_id}'"
                )
            )
        );

        # 3. Додаю параметри для форми
//        $data = $this->ms->data('content', $id);

        $this->formAppendData('form_action',       "Forum/process/$id")
             ->formAppendData('redirect_url',     'Forum')
             ->formAppendData('date', '')
             ->formAppendData('categories', $this->categories(0, '', $id));

        # 4. Генерую контент і виводжу на екран

        return $this->renderContent();
    }

    /**
     * categories list
     * @param int $parent_id
     * @param string $parent_name
     * @param array $selected
     * @return array
     */
    private function categories($parent_id=0, $parent_name='', $selected)
    {
        $mb = $this->load->model('engine\Blog');

        $cid=array();
        foreach ($mb->getCategories($selected) as $r) {
            $cid[] = $r['cid'];
        }
        $cat_type = $this->ms->getTypeId('ForumCategories');

        $r = $this->ms->tree($parent_id, $cat_type);
        foreach ($r as $row) {
            if($parent_name != '') {
                $row['name'] = $parent_name .' / '. $row['name'];
            }
            if(in_array($row['id'], $cid)) {
                $row['selected'] = 'selected';
            }
            $this->cat[] = $row;
            if($row['isfolder']) {
                $this->categories($row['id'], $row['name'], $selected);
            }
        }

        return $this->cat;
    }
    /**
     * @param $id
     * @return mixed|string
     */
    public function process($id)
    {
        parent::process($id);
        $this->updateCategories($id);

        return json_encode($this->getFormResponse());
    }

    /**
     * @param $id
     * @return int
     */
    private function updateCategories($id)
    {
        $mb = $this->load->model('engine\Blog');

        $cid=array();
        $c = $mb->getCategories($id);
        foreach ($c as $r) {
            $cid[$r['cid']] = $r['cid'];
        }

        foreach ($_POST['categories'] as $k=>$c) {
            if(in_array($c, $cid)){
                unset($cid[$c]);
                continue;
            }
            $mb->setCategories($id, $c);
        }

        foreach ($cid as $k=>$v) {
            $mb->deleteCategories($id, $v);
        }


        return 1;
    }

    /**
     * видалення сторінки
     * @param $id
     * @return mixed 1
     */
    public function delete($id)
    {
        return parent::delete($id);
    }

    /**
     * @return string
     */
    public function tree()
    {
        $tree=array();
        $parent_id = isset($_POST['id']) ? (int)$_POST['id'] : 0;
        $type= $this->ms->getTypeId('ForumCategories');
        if(empty($type)) return 'wrong type id';
        $r = $this->ms->tree($parent_id, $type);
        $i=0;
        if(!empty($r)){
            foreach ( $r as $row) {
//                $row['isfolder']   = $this->ms->isFolderManual($row['id'], 'postCategory');
                $tree[$i]['data']  = $row['name'] . ' #' . $row['id'] ;
                $tree[$i]['state'] = $row['isfolder'] ? 'closed' : '';
                $tree[$i]['attr']  = array(
                                        'id' => $row['id'],
                                        "rel"=> (($row['isfolder'])? 'folder':'file'),
                                        "href"=>   './Forum/index/'. $row['id'] ,
                                    );
                $i++;
            }
        }

        return json_encode($tree);
    }

    /**
     * return controller content
     * @return string
     */
    protected function output()
    {
        $this->setSidebar(
            new Tree(
                'content',
                'ForumCategories/tree/',
                Tree::button($this->lang->categories['tree_add'], 'engine.forum.categories.create()', 'icon-plus-sign'),
                array(
//                    Tree::contextMenu($this->lang->categories['tree_cmenu_add'], 'icon-plus' ,'engine.blog.categories.create(id)'),
                    Tree::contextMenu($this->lang->categories['tree_cmenu_edit'], 'icon-pencil' ,'engine.forum.categories.edit(id)'),
                    Tree::contextMenu($this->lang->categories['tree_cmenu_del'], 'icon-remove' ,'engine.forum.categories.del(id)'),
                  ),
                true
            )
        );
        return parent::output();
    }
}
