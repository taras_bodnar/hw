<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 01.09.14 22:23
 */
namespace controllers\engine;
 use controllers\Engine;

 defined("SYSPATH") or die();

 /**
  * Генератор меню навігації для фронтенду
  * Class Nav
  * @package controllers\engine
  */
class Nav extends Engine{

    public function __construct()
    {
        parent::__construct();

        $this->mg = $this->load->model('engine\Nav');
    }



    /**
     * @return mixed|string
     */
    public function index()
    {
        $buttons= array(
            Form::button(
                $this->lang->core['create'],
                Form::icon('icon-file'),
                array(
                    'class'   => Form::BTN_TYPE_PRIMARY,
                    'onclick' => "self.location.href='./nav/create'"
                ))
        );

        $t = new Table('nav_menu', "nav/items");
        $t  ->setTitle($this->lang->nav['table_title'])
            ->sortableConf('nav_menu','id','id')
            ->addTh($this->lang->core['id'])
            ->addTh($this->lang->nav['name'])
            ->addTh($this->lang->core['func'] , 'w-120')
        ;

        // render content view
        $this->setButtons($buttons);
        $this->setContent($t->render());

        return $this->output();
    }

    /**
     * @return string
     */
    public function items()
    {
        $dt = new Table();
        return $dt
            -> table('nav_menu')
            -> columns(
                array(
                    'id',
                    'name'
                )
            )
            -> searchCol(array('id', 'name'))
            -> returnCol(array('id', 'name', 'func'))
            -> formatCol(
                array(
                    1 => "<a href='nav/edit/{id}'>{name}</a>",
                    2 =>
                        Form::link(
                            '',
                            Form::icon('icon-edit'),
                            array(
                                'class'  =>'btn-info',
                                'title' => $this->lang->core['edit'],
                                'href'  => 'nav/edit/{id}'
                            )
                        ) .
                        Form::button(
                            '',
                            Form::icon('icon-remove'),
                            array(
                                'title'   => $this->lang->core['delete'],
                                'class'   =>'btn-danger',
                                'onclick' => 'engine.nav.delete({id})'
                            )
                        )
                ))
            -> request();
    }

    /**
     * @return mixed|string
     */
    public function create()
    {
        $buttons= array(
            Form::button(
                $this->lang->core['back'],
                Form::icon('icon-external-link'),
                array(
                    'class'=>'btn-link',
                    'onclick' => 'self.location.href=\'./nav/index\''
                )
            ),
            Form::button(
                $this->lang->core['save'],
                Form::icon('icon-save', false),
                array(
                    'class'=>'btn-success form-submit'
                ))
        );


        $content = $this->load->view('nav_form',array(
            'action'  => 'create',
            'id'      => 0,
            'items'    => $this->mg->getPages()
        ));

        $this->setButtons($buttons);
        $this->setContent($content);

        return $this->output();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $buttons= array(
            Form::button(
                $this->lang->core['back'],
                Form::icon('icon-external-link'),
                array(
                    'class'=>'btn-link',
                    'onclick' => 'self.location.href=\'nav/index\''
                )
            ),
            Form::button(
                $this->lang->core['save'],
                Form::icon('icon-save', false),
                array(
                    'class'=>'btn-success form-submit'
                ))
        );
        $data = $this->mg->data('nav_menu', $id);

        $pages = $this->mg->getPages($id);
//        $this->dump($pages);

        $content = $this->load->view('nav_form',array(
            'action'     => 'edit',
            'data'       => $data,
            'id'         => $id,
            'items'      => $pages
        ));

        // output
        $this->setButtons($buttons);
        $this->setContent($content);

        return $this->output();
    }

    /**
     * @param $id
     * @return mixed|string
     */
    public function process($id=0)
    {
        $s=0;
        $e = $this->errors['warning'];
        $r = './nav/index'; // redirect url
        $data = $_POST['data'];

        if(
            empty($data['name'])
        ) {
            $this->error[] = $this->lang->core['e_required_fields'];
        }
        // add or update values
        if(empty($this->error)) {
            switch($_POST['action']){
                case 'create':
                    $id = $this->mg->create($data);
                    if($id > 0) {
                        $this->addItems($id);
                        $s = $id;
                        $e = $this->errors['success'];
                        $this->error[] = $this->lang->nav['save_success'];
                    } else {
                        $e = $this->errors['error'];
                        $this->error[] = $this->mg->error();
                    }
                    break;
                case 'edit':
                    if(empty($id)) return '';
                    $r='';
                    $s = $this->mg->update($id, $data);
                    $s+= $this->updateItems($id);
                    if($s > 0) {
                        $e = $this->errors['success'];
                        $this->error[] = $this->lang->nav['save_success'];
                    } else {
                        $e = $this->errors['error'];
                        $this->error[] = $this->mg->error() ;
                    }
                    break;
                default:
                    break;
            }
        }

        return json_encode(array(
            's' => $s > 0 , // status
            'r' => $r, // redirect url
            'e' => $e, // error class
            't' => $this->lang->core[$e], // error title
            'm' => implode('<br>', $this->error) // error message
        ));
    }

    /**
     * зберігаю пункти меню
     * @param $id
     * @return int
     */
    private function addItems($id)
    {
        if(!isset($_POST['items'])) return 0;
        $sort = 0; echo '';
        foreach ($_POST['items'] as $k => $content_id) {
            $this->mg->addItem($id, $content_id, $sort);
            $sort++;
        }
        return 1;
    }

    /**
     * оновлюю пункти меню
     * @param $id
     * @return int
     */
    private function updateItems($id)
    {

        $this->mg->deleteItems($id);
        if(!isset($_POST['items'])) return 0;


        return $this->addItems($id);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        return $this->mg->delete($id);
    }

    public function sort($nav_id,$sort)
    {
        $sort = explode(',',$sort);
        $i=1;
        foreach($sort as $k=>$id){
            $this->mg->sort($nav_id, $id, $i);
            $i++;
        }
        return $i;
    }
} 