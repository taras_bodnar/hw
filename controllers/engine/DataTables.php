<?php
/**
 * Company Otakoyi.com
 * Author: wg
 * Date: 02.05.15 16:05
 */

namespace controllers\engine;

use controllers\core\DB;

defined("SYSPATH") or die();

/**
 * Class DataTables
 * @package controllers\engine
 *
 * Examples
 *
 * html example
 *
$t = new DataTables();
$t->setId('content')
->setTitle('table title')
-> th('#')
-> th('name')
-> th('controller')
-> th('rang')
-> th('func')
;

// some html data

$t->tr(array(1,'name','c','r', 'f'));
$t->tr(array(2,'name2','c','r', 'f'));
$t->tr(array(3,'name2','c','r', 'f'));
$t->tr(array(4,'name2','c','r', 'f'));
$t->tr(array(5,'name2','c','r', 'f'));

// render output
return $t->render();
 *
 *
 * JSON VS DB
 *
$t = new DataTables();
$t  -> table('content c')
    -> get('c.id, i.name, c.published')
    -> join("join content_info i on i.content_id=c.id and i.languages_id=1")
    -> execute();

return $t->renderJSON($t->getResults(), $t->getTotal());
 *
 * JSON VS DB CUSTOM FORMAT

$t = new DataTables();
$t  -> table('content c')
    -> get('c.id, i.name, c.published')
    -> join("join content_info i on i.content_id=c.id and i.languages_id=1")
    -> execute();

$res = array();
foreach ($t->getResults(false) as $i=>$row) {
    $res[$i][] = $row['id'];
    $res[$i][] = "<a href='{$row['id']}'>{$row['name']}</a>";
    $res[$i][] = "<a class='btn btn-primary' href='{$row['id']}'><i class='icon-edit icon-white'></i></a>";
}

return $t->renderJSON($res, $t->getTotal());
 *
 */
class DataTables {

    /**
     * set title of table
     * @var string
     */
    private $title;

    /**
     * @var array
     */
    private $config = array();

    /**
     * table ID
     * @var string
     */

    private $table_id;

    /**
     * table heading columns
     * @var array
     */
    private $th = array();

    /**
     * row of table body
     * @var array
     */
    private $tr = array();

    private $db;

    private $table;
    private $join     = array();
    private $where    = array();
    private $order_by = '';
    private $group_by  = '';
    private $rows     = '';
    private $search_cols = array();
    private $limit    = '';
    private $debug    = 0;
    private $total    = 0;
    private $results  = array();

    private $iDisplayLength = 25;

    public function __construct()
    {
        $this->db = DB::instance();
        return $this;
    }

    /**
     * @param $table
     * @return $this
     */
    public function table($table)
    {
        $this->table = $table;

        return $this;
    }

    /**
     * @param $q
     * @return $this
     */
    public function join($q)
    {
        $this->join[] = $q;

        return $this;
    }

    /**
     * @param $q
     * @return $this
     */
    public function where($q)
    {
        $this->where[] = $q;

        return $this;
    }

    /**
     * @param $q
     * @return $this
     */
    public function orderBy($q)
    {
        $this->order_by = $q;

        return $this;
    }

    /**
     * @param $q
     * @return $this
     */
    public function groupBy($q)
    {
        $this->group_by = $q;

        return $this;
    }

    /**
     * @param $start
     * @param $num
     * @return $this
     */
    public function limit($start, $num)
    {
        $this->limit = "limit $start, $num";
        return $this;
    }

    /**
     * @param $rows
     * @return $this
     */
    public function get($rows)
    {
        if(is_array($rows)){
            $this->rows = implode(',', $rows);
        } else {
            $this->rows = $rows;
        }

        return $this;
    }

    /**
     * @param $rows
     * @return $this
     */
    public function searchCol($rows)
    {

        if(is_array($rows)){
            $this->search_cols = $rows;
        } else {
            $this->search_cols = explode(',', $rows);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function execute()
    {
        /**
         * limit
         */
        if(isset($_POST['start']) && isset($_POST['length'])){
            $start = (int)$_POST['start'];
            $num   = (int)$_POST['length'];
            $this->limit($start, $num);
        }

        /**
         * sorting
         */
        if(isset($_POST['order'][0]['column'])){
            $col  = (int)$_POST['order'][0]['column'];
            $rows = explode(',', $this->rows);

            if(isset($_POST['sortable']) && $_POST['sortable'] && isset($_POST['sCol']) && !empty($_POST['sCol'])) {
                array_unshift($rows, $_POST['sCol']);
            }

            $dir = isset($_POST['order'][0]['dir']) && $_POST['order'][0]['dir'] == 'asc' ? 'ASC' : 'DESC';

            if(isset($rows[$col])){
                $this->order_by = " ORDER BY {$rows[$col]} {$dir}";
            }
        }

        if($this->group_by) {
            $this->group_by = " Group BY ".$this->group_by;
        }

        if( !empty($this->search_cols) && isset( $_POST['search']['value']) && strlen($_POST['search']['value']) > 2){
              $q = trim($_POST['search']['value']);
              if(!empty($q) && strlen($q) > 1){
                  $w = array();
                  foreach ($this->search_cols as $k => $row) {
                      $w[] = " {$row} like '%{$q}%'";
                  }

                  $this->where("(". implode(' or ', $w) .")");
              }
//            var_dump($_POST['search']['value']); die();
        }


        $w = empty($this->where) ? '' : trim(implode(' and ', $this->where));
        $w = empty($w) ? '' : 'WHERE ' . $w;

        $j = implode(' ', $this->join);

//        $t = "SELECT COUNT(*) AS t FROM {$this->table} {$j} {$w}";
        $q = "SELECT {$this->rows} FROM {$this->table} {$j} {$w} {$this->group_by} {$this->order_by} {$this->limit}";
        $t = "SELECT {$this->rows} FROM {$this->table} {$j} {$w} {$this->group_by} {$this->order_by}";
//        echo $q;
//        $this->total = $this->db->select($t, $this->debug)->row('t');
        $this->results = $this->db->select($q, $this->debug)->all();
        $this->total = count($this->db->select($t, $this->debug)->all());

        return $this;
    }

    /**
     * @return int
     */
    public function  getTotal()
    {
        return $this->total;
    }

    /**
     * @param bool $format
     * @return array
     */
    public function getResults($format = true)
    {
        if(! $format) {
            return $this->results;
        }
        $rows = explode(',', $this->rows);
        foreach ($rows as $i=>$col) {
            if(strpos($col, '.') !== false){
                $a = explode('.', $col);
                $rows[$i] = $a[1];
            }elseif(strpos($col, 'as') !== false){
                $a = explode('as', $col);
                $rows[$i] = trim($a[1]);
            }
        }

        $res = array();
        foreach ($this->results as $item) {
            $a = array();

            foreach ($rows as $row) {
                $a[] = isset($item[$row]) ? $item[$row] : $row . ' issues';
            }

            $res[] = $a;
        }

        return $res;
    }

    /**
     * @param bool $s
     * @return $this
     */
    public function debug($s = true)
    {
        $this->debug = $s;

        return $this;
    }

    /**
     * @param $num
     * @return $this
     */
    public function setDisplayLength($num)
    {
        $this->iDisplayLength = $num;

        return $this;
    }

    /**
     * @param $name
     * @param string $class
     * @param string $style
     * @return $this
     */
    public function th($name, $class = '', $style = '')
    {
        if(empty($this->th)) {
            if($this->getConfig('groupFunctions')){
                $this->config['ajax']['data']['groupFunction'] = true;
                $this->th[] = array(
                    'name' => "<input type='checkbox' class='check-all'>",
                    'class' => 'w-20'
                );
            }

            $sc = $this->getConfig('sortable');
            if($sc){
                $this->config['ajax']['data']['sortable'] = true;
                $this->config['ajax']['data']['sTable'] = $sc['table'];
                $this->config['ajax']['data']['sCol'] = $sc['col'];
                $this->config['ajax']['data']['sPk'] = $sc['pk'];
                $this->th[] = array(
                    'name' => "<i class='icon-reorder' style='cursor: none'></i>",
                    'class' => 'w-20'
                );
            }
        }

        $this->th[] = array(
            'name'  => $name,
            'class' => $class,
            'style' => $style
        );

        return $this;
    }

    public function sortable($table, $col, $pk = 'id')
    {
        $this->setConfig('sortable', array(
            'table' => $table,
            'pk'    => $pk,
            'col'   => $col
        ));

        return $this;
    }

    /**
     * add row
     * @param $tr array
     * @return $this
     */
    public function tr($tr)
    {
        $_tr = array();

        foreach ($tr as $k => $td) {
            $_tr[] = "<td>{$td}</td>\r\n";
        }

        $this->tr[] = "<tr>". implode('', $_tr) ."</tr>\r\n";

        return $this;
    }

    /**
     * @param $key
     * @param $val
     * @return $this
     */
    public function setConfig($key, $val)
    {
        $this->config = array_merge(
            $this->config,
            array( $key => $val )
        );

        return $this;
    }

    /**
     * @param $key
     * @return mixed
     */
    public function getConfig($key)
    {
        if( ! isset($this->config[$key])) return false;

        return $this->config[$key];
    }

    /**
     * @param $id
     * @return $this
     */
    public function setId($id)
    {
        $this->table_id = $id;

        return $this;
    }

    /**
     * @param $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = "<div id='data-table' class='panel-heading datatable-heading'>
                            <h4 class='section-title'>{$title}</h4>
                        </div>";

        return $this;
    }

    /**
     * @param $url
     * @param array $data
     * @return $this
     */
    public function ajaxConfig($url, $data = array())
    {
        $this ->setConfig('processing', true)
              ->setConfig('serverSide', true)
              ->setConfig(
                'ajax',
                array(
                    'url'  => $url,
                    'type' => 'POST',
//                    'data' => $data
                )
            );
        return $this;
    }

    /**
     * @param array $data
     * @param int $recordsTotal
     * @return string
     */
    public function renderJSON(array $data, $recordsTotal=0)
    {
        $_data = array(); $draw= isset($_POST['draw']) ? (int)$_POST['draw'] : 1;  $recordsFiltered = $recordsTotal;
        if($recordsTotal == 0){
            $recordsTotal = $draw;
        }

        foreach ($data as $row) {
            $_data[] = array_values($row);
        }

        return json_encode(
            array(
                'draw'            => $draw,
                'data'            => $_data,
                'recordsTotal'    => (int) $recordsTotal,
                'recordsFiltered' => (int) $recordsFiltered
            )
        );
    }

    /**
     * render table body
     * @return string
     */
    private function body()
    {
        $c =0; // count th
        $html = '';

        if(!empty($this->title)){
            $html .= "<div class='panel panel-default panel-block'>";
        }

        $html .= $this->title;
        $html .= "
                    <table class='table table-bordered table-striped' id='{$this->table_id}'>
                    <thead>
                    <tr>\r\n";

        foreach ($this->th as $th) {
            $attr = empty($th['class']) ? '' : "class=\"{$th['class']}\"";
            $attr .= empty($th['style']) ? '' : "style=\"{$th['style']}\"";
            $html .= "<th {$attr}>{$th['name']}</th>\r\n";
            $c++;
        }

        $html .= "
                    </tr>
                </thead>
                ";
        if(!empty($this->tr)) {
            $html .= "<tbody>\r\n". implode('', $this->tr) ."</tbody>";
        } else {
            $html .= "
                <tbody>
                    <tr>
                        <td colspan=\"{$c}\" class=\"dataTables_empty\">empty data</td>
                    </tr>
                </tbody>\r\n";
        }
        $html .= "</table>";

        if(!empty($this->title)){
            $html .= "</div>"; // .panel
        }
        return $html;
    }

    /**
     * @return string
     */
    private function js()
    {
       $sc = $this->getConfig('sortable');

       $this->setConfig(
            'fnInitComplete',
            'function(){
                $(\'.dataTables_wrapper\').find(\'input, select\').addClass(\'form-control\');
                $(".dataTables_wrapper ").find(\'.icon-reorder\').each(function(i,e){
                 $(this).parents("tr").attr("id", $(e).attr("id"));
                 });
                $(".dataTables_wrapper tbody").sortable({handle: ".icon-reorder", update: function(event, ui) {
                
                var newOrder = $(this).sortable(\'toArray\').toString();

                  var city = $("#city_id").val();
                  city = typeof city !="undefined" && city.length?city:0;
                  $.ajax({url:\'DataTables/sort/'. $sc['table'] .'/'. $sc['col'] .'/'. $sc['pk'] .'?o=\'+newOrder+\'&city=\'+city, beforeSend: function(){$(\'body\').css(\'cursor\',\'progress!important\');}, success: function(){$(\'body\').css(\'cursor\',\'inherit\'); $.pnotify({ title: engine.lang.core.info, type: \'\',  text: engine.lang.core.sort_done });}}); } }); }');

        $this->setConfig('iDisplayLength', $this->iDisplayLength);

//        $this->setConfig('fnInitComplete','function () {console.log(\'init complete\');}');
        $this->setConfig('fnReloadAjax','function () {console.log(\'fnReloadAjax\');}');

        $config = json_encode($this->config);
        $config = str_replace('\n', '', $config);
        $config = str_replace('\r', '', $config);
        $config = str_replace('"function', 'function', $config);
        $config = str_replace('}"', '}', $config);
        $config = ltrim($config, '"');
        $config = rtrim($config, '"');
        $config = stripslashes($config);
        return "
        <script charset='utf-8'>
            $(document).ready(function() {
                $('#{$this->table_id}').dataTable($config);
            });
        </script>";
    }

    /**
     * @return string
     */
    public function render()
    {
        return $this->body() . $this->js();
    }

    public function sort($table, $col, $pk = 'id')
    {
        $o = isset($_GET['o']) ? $_GET['o'] : null;
        if(empty($o)) return;
        $table = str_replace('%20', ' ', $table);
        $o= explode(',', $o);
        foreach ($o as $k=>$id) {
            if(empty($id)) continue;

            if(isset($_GET['city']) && $_GET['city']>0) {

//                $this->db->customExec("Delete users_sort where users_id={$id}");
                $this->db->delete("users_sort","users_id={$id}",1);
                $this->db->insert("users_sort",array('sort'=>$k,'guides_id'=>$_GET['city'],'users_id'=>$id));
            } else {
                echo "UPDATE {$table} SET {$col} = {$k} WHERE  {$pk} = {$id} limit 1";
                $this->db->customExec("UPDATE {$table} SET {$col} = {$k} WHERE  {$pk} = {$id} limit 1");
            }
        }
        echo 'OK';
    }
} 