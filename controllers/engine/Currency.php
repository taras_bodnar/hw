<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 13.07.14 9:57
 */

namespace controllers\engine;

 
use controllers\Engine;

defined('SYSPATH') or die();

/**
 * Class Currency
 * @package controllers\engine
 */
class Currency  extends Engine{

    public function __construct()
    {
        parent::__construct();

        $this->mg = $this->load->model('engine\Currency');
    }

    /**
     * @return mixed|string
     */
    public function index()
    {
        $buttons = array();

        $buttons[] = Form::button(
                $this->lang->core['create'],
                Form::icon('icon-file'),
                array(
                    'class'   => Form::BTN_TYPE_PRIMARY,
                    'onclick' => "engine.currency.create()"
                ));


        $t = new Table('currency', "currency/items");
        $t
            ->setTitle($this->lang->currency['table_title'])
            ->sortableConf('currency','id','id')
            ->addTh($this->lang->currency['id'])
            ->addTh($this->lang->currency['name'])
            ->addTh($this->lang->currency['code'])
            ->addTh($this->lang->currency['symbol'])
            ->addTh($this->lang->currency['rate'])
            ->addTh($this->lang->currency['is_main'])
            ->addTh($this->lang->core['func'] , 'w-120')
        ;

        $this->setButtons($buttons);
        $this->setContent($t->render());

        return $this->output();
    }

    public function changeCourse(){}

    /**
     * @return string
     */
    public function items()
    {
        $dt = new Table();
        return $dt
            -> table('currency c')
            -> columns(
                array(
                    'id',
                    'name',
                    'code',
                    'rate',
                    'symbol',
                    'is_main',
                    'IF(is_main = 1, "btn-success" , "") as is_main_status ',
                    'IF(is_main = 1, "<i class=\'icon-ok\'></i>" , "<i class=\'icon-minus\'></i>") as icon ',
                    'IF(is_main = 1, "disabled" , "") as disabled ',
                )
            )
            -> searchCol(array('id','name','code'))
            -> returnCol(array('id','name','code','symbol','rate','is_main', 'func'))
            -> formatCol(
                array(
                    1 => '<a title="{name}" href="javascript:engine.currency.edit({id})">{name}</a>',
                    4 => '<input class="form-control" {disabled} type="text" onchange="engine.currency.update(this);" data-id="{id}" value="{rate}">',
                    5 => '<button onclick="engine.currency.changeMain({id},{is_main});" class="btn {is_main_status}">{icon}</button>',
                    6 =>
                        Form::link(
                            '',
                            Form::icon('icon-edit'),
                            array(
                                'class'  =>'btn-info',
                                'title' => $this->lang->core['edit'],
                                'onclick'  => 'engine.currency.edit({id})'
                            )
                        ) .
                        Form::button(
                            '',
                            Form::icon('icon-remove'),
                            array(
                                'title'   => $this->lang->core['delete'],
                                'class'   =>'btn-danger',
                                'onclick' => 'engine.currency.delete({id})'
                            )
                        )
                ))
            -> request();
    }

    /**
     * @return mixed|string
     */
    public function create()
    {
        return $this->load->view('currency/form',array(
            'action' => 'create',
            'id'     => 0
        ));
    }

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        if(empty($id)) return 'Empty ID';

        $data = $this->mg->data('currency', $id);

        return $this->load->view('currency/form',array(
            'action' => 'edit',
            'id'     => $id,
            'data'   => $data
        ));
    }

    /**
     * @param $id
     * @return mixed|string
     */
    public function process($id=0)
    {
        $s=0;
        $e = $this->errors['warning'];
        $data = $_POST['data'];
        if(empty($data['name']) || empty($data['code']) || empty($data['symbol']) || empty($data['rate'])){
            $this->error[] = $this->lang->currency['e_all_fields_required'];
        }
        // add or update values
        if(empty($this->error)) {
             switch($_POST['action']){
                case 'create':
                    $s = $this->mg->create($data);
                    if($s > 0) {
                        $e = $this->errors['success'];
                        $this->error[] = $this->lang->currency['create_success'];
                    } else {
                        $e = $this->errors['error'];
                        $this->error[] = $this->mg->error();
                    }
                    break;
                case 'edit':
                    if(empty($id)) return '';
                    $s = $this->mg->update($id, $data);
                    if($s > 0) {
                        $e = $this->errors['success'];
                        $this->error[] = $this->lang->currency['save_success'];
                    } else {
                        $e = $this->errors['error'];
                        $this->error[] = $this->mg->error() ;
                    }
                    break;
                default:
                    break;
            }
        }

        return json_encode(array(
            's' => $s > 0 , // status
            'r' => '', // redirect url
            'e' => $e, // error class
            't' => $this->lang->core[$e], // error title
            'm' => implode('<br>', $this->error) // error message
        ));
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        return $this->mg->delete($id);
    }

    /**
     * @param $id
     * @param $s
     * @return mixed
     */
    public function changeMain($id, $s)
    {
        return $this->mg->changeMain($id, $s);
    }

    /**
     * @return int
     */
    public function updateRate()
    {
        if(empty($_POST)) return 0;

         $id = (int) $_POST['id'];
         $rate = (float) $_POST['rate'];

        return $this->mg->updateRate($id, $rate);
    }
} 