<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 20.12.14 21:43
 */

namespace controllers\engine\plugins;
use controllers\engine\Plugin;
use controllers\engine\Plugins;

defined("SYSPATH") or die();

/**
 * Class ProductsVariants
 * @name ProductsVariants
 * @description description of plugin
 * @package controllers\engine\plugins
 * @author wmgodyak mailto:wmgodyak@gmail.com
 * @version 1.0
 * @copyright &copy; 2014 Otakoyi.com
 */
class ProductsVariants extends Plugins implements Plugin{
    private $model;
    public function __construct()
    {
        parent::__construct();
        $this->model= $this->load->model('engine\plugins\ProductsVariants');
    }
    /**
     * index method
     * @return mixed|string|void
     */
    public function index(){}

    public function create(){}

    public function edit($id)
    {
        $items = $this->model->get($id);
        return $this->load->view('plugins/ProductsVariants',array('items'=>$items));
    }
    public function delete($id)
    {

    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteItem($id)
    {
        return $this->model->delete($id);
    }
    public function process($id)
    {
        if(empty($_POST['variants'])) return ;
        foreach ($_POST['variants']['id'] as $k=>$v) {
            $iv = array(
                'id'          => $_POST['variants']['id'][$k],
                'products_id' => $id,
                'name'        => $_POST['variants']['name'][$k],
                'price'       => $_POST['variants']['price'][$k],
                'code'        => $_POST['variants']['code'][$k],
                'quantity'    => $_POST['variants']['quantity'][$k],
                'sort'        => $k
            );
//            $this->dump($iv);
            if(empty($iv['id'])){
                if(!empty($iv['name'])){
                    $this->model->create($iv);
                }
            } else{
                $_id=$iv['id'];
                unset($iv['id']);
                $this->model->update($_id, $iv);
            }
        }
    }

    /**
     * call when plugin install
     * @return int|mixed
     */
    public function install()
    {
       return 1;
    }

    /**
     * call when plugin uninstall
     * @return int|mixed
     */
    public function uninstall()
    {
       return 1;
    }
}