<?php
/**
 * Company Otakoyi.com
 * Author: wg
 * Date: 20.12.14 21:43
 */

namespace controllers\engine\plugins;

use controllers\Engine;

defined("SYSPATH") or die();

/**
 * Class Comments
 * @name Comments
 * @description коментарі для сторінок
 * @package controllers\engine\plugins
 * @author wmgodyak mailto:wmgodyak@gmail.com
 * @version 1.0
 * @copyright &copy; 2014 Otakoyi.com
 */
class Comments extends Engine{
    public function index(){}
    public function create(){}

    public function install(){return 1;}
    public function uninstall(){return 1;}
    /**
     * @param $id
     * @return mixed|string
     */
    public function edit($id)
    {
        $mc = $this->load->model('engine\Comments');
        return $this->load->view(
            'plugins/comments',
            array(
                'items' => $mc->get($id)
            )
        );
    }
    public function delete($id){}
    public function process($id){}
}