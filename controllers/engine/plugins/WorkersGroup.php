<?php
/**
 * Company Otakoyi.com
 * Author: wg
 * Date: 20.12.14 21:43
 */

namespace controllers\engine\plugins;

use controllers\core\DB;
use controllers\engine\Plugin;
use controllers\engine\Plugins;

defined("SYSPATH") or die();

/**
 * Class WorkersGroup
 * @name WorkersGroup
 * @description мітки
 * @package controllers\engine\plugins
 * @author wmgodyak mailto:wmgodyak@gmail.com
 * @version 1.0
 * @copyright &copy; 2014 Otakoyi.com
 */
class WorkersGroup extends Plugins implements Plugin{
    private $db;
    public function __construct()
    {
        parent::__construct();
        $this->db = DB::instance();
    }
    public function index(){}
    public function install(){return 1;}
    public function uninstall(){return 1;}
    public function create(){return $this->load->view('plugins/WorkersGroup');}

    /**
     * @param $id
     * @return mixed|string
     */
    public function edit($id)
    {
        $parent_id = $this->db->select("select parent_id from content where id = {$id} limit 1")->row('parent_id');
        $type = $this->db->select("Select type_id from content where id={$id} limit 1")->row("type_id");

        if($parent_id != 3 && $type!=15) return '';

        $group_id = $this->db->select("select users_group_id from pages_workers_group where content_id = {$id} limit 1")->row('users_group_id');

        return $this->load->view(
            'plugins/WorkersGroup',
            array(
                'id' => $id,
                'items' => $this->getUsersGroup(2),
                'group_id' => $group_id
            )
        );
    }

    private function getUsersGroup($parent_id)
    {
        return $this->db->select("
              select g.id, i.name, i.description
              from users_group g, users_group_info i
              where g.parent_id = '{$parent_id}' and i.users_group_id=g.id and i.languages_id = '{$this->language_id}'
              order by abs(g.sort) asc
        ")->all();
    }

    public function delete($id){}

    /**
     * @param $id
     * @return mixed|void
     */
    public function process($id)
    {
        if(!isset($_POST['workers_id'])) return 0;

        $workers_id = (int)$_POST['workers_id'];

        $type = $this->db->select("Select type_id from content where id={$id} limit 1")->row("type_id");

        $this->db->delete("pages_workers_group","where content_id={$id}");

        if($type==15 && empty($workers_id)) return false;

        $this->db->insert(
            "pages_workers_group",
            array(
                'content_id'     => $id,
                'users_group_id' => $workers_id
            )
        );

        return 1;
    }
}