<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 20.12.14 21:43
 */

namespace controllers\engine\plugins;

use controllers\engine\Plugin;
use controllers\engine\Plugins;

defined("SYSPATH") or die();

/**
 * Class UsersSort
 * @name UsersSort
 * @description description of plugin
 * @package controllers\engine\plugins
 * @author Taras Bodnar mailto:bania20091@gmail.com
 * @version 1.0
 * @copyright &copy; 2015 Otakoyi.com
 */
class UsersSort extends Plugins implements Plugin
{
    private $m;
    public function __construct()
    {
        parent::__construct();
        $this->m = new \models\engine\plugins\UsersSort();
    }
    /**
     * index method
     * @return mixed|string|void
     */
    public function index()
    {
    }

    public function create()
    {
    }

    public function edit($id)
    {
        $cities = $this->m->getCities(9,$id);
        $sort = $this->m->getSort($id);
        return  $this->load->view('plugins/UsersSort',array('cities'=>$cities,'sort'=>$sort));
    }

    public function delete($id)
    {

    }

    public function process($id)
    {
        $city = $_POST['city'];
        $sort = $_POST['sortU'];

        if(isset($city) && !empty($city) && isset($sort) && !empty($sort)) {
            $this->m->insert($id,$city,$sort);
        }
    }

    /**
     * call when plugin install
     * @return int|mixed
     */
    public function install()
    {
        return 1;
    }

    /**
     * call when plugin uninstall
     * @return int|mixed
     */
    public function uninstall()
    {
        return 1;
    }
}