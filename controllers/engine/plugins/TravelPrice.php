<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 20.12.14 21:43
 */

namespace controllers\engine\plugins;

use controllers\engine\Plugin;
use controllers\engine\Plugins;

defined("SYSPATH") or die();

/**
 * Class TravelPrice
 * @name TravelPrice
 * @description description of plugin
 * @package controllers\engine\plugins
 * @author Taras Bodnar mailto:bania20091@gmail.com
 * @version 1.0
 * @copyright &copy; 2015 Otakoyi.com
 */
class TravelPrice extends Plugins implements Plugin
{
    private $m;
    public function __construct()
    {
        parent::__construct();
        $this->m = new \models\engine\plugins\TravelPrice();
    }
    /**
     * index method
     * @return mixed|string|void
     */
    public function index()
    {
    }

    public function create()
    {
    }

    public function edit($id)
    {
        $templates_id = $this->m->getParentType($id);
        $price = $this->m->getPrice($id);
        if($templates_id==22) {
        $view = $this->load->view('plugins/TravelPrice',array('price'=>$price));
        } else {
            $view = '';
        }

        return $view;
    }

    public function delete($id)
    {
    }

    public function process($id)
    {
        if(!isset($_POST['travel_price']) && !isset($_POST['travel_price'])) return'';

        $travel_price = $_POST['travel_price'];
        $travel_old_price = $_POST['travel_old_price'];
        return $this->m->addPrice($travel_price,$travel_old_price,$id);
    }

    /**
     * call when plugin install
     * @return int|mixed
     */
    public function install()
    {
        return 1;
    }

    /**
     * call when plugin uninstall
     * @return int|mixed
     */
    public function uninstall()
    {
        return 1;
    }
}