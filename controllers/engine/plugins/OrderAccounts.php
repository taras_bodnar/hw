<?php
namespace controllers\engine\plugins;


use controllers\core\DB;
use controllers\Engine;
use controllers\engine\Plugin;
use controllers\engine\Plugins;
use models\modules\Orders;
use models\modules\Payment;

/**
 * Class OrderAccounts
 * @name OrderAccounts
 * @description OrderAccounts
 * @package controllers\engine\plugins
 * @author gstuffpro mailto:hello@gstuffpro.com
 * @version 1.0
 * @copyright &copy; 2018 gstuffpro.com
 */
class OrderAccounts extends Engine
{
    private $m;
    private  $db;
    public $type = array(
                    'week' => 'Hot Пропозиція (тиждень)',
                    'month' => 'Hot Пропозиція (місяць)',
                    'year' => 'Gold Аккаунт'
                );

    public function __construct()
    {
        parent::__construct();
        $this->m = new Orders();
        $this->db = DB::instance();
    }

    public function index()
    {
        return parent::index(); // TODO: Change the autogenerated stub
    }

    public function edit($id)
    {

        $items = $this->db->select("select * from ordersAccount o 
                            where users_id={$id} order by createdon desc
          ")->all();

        return $this->load->view(
            'plugins/order_account',
            array(
                'items' => $items,
                'uid' => $id,
                'type' => $this->type
            )
        );
    }

    public function add()
    {
        if (isset($_POST['action']) && $_POST['action'] == 'add') {
            $data = $_POST["data"];
            $s = 0;
            $city = '';
            $e = array();

            if (empty($data["price"])) {
                $s = 0;
                $e[] = 'Введіть ціну';
            } elseif(empty($data["type"])) {
                $s = 0;
                $e[] = 'Виберіть тип';
            } elseif(empty($data["pay_date"])) {
                $s = 0;
                $e[] = 'Виберіть дату початку дії';
            } elseif(empty($data["end_date"])) {
                $s = 0;
                $e[] = 'Виберіть дату закінчення дії';
            }  else  {
                $data["status_id"] = $data["pay"];
                $data["code"] = $data["pay"];

                if ($this->m->createPay($data)) {
                    $s = 1;
                    switch($data['type']) {
                        case 'week':
                        case 'month':
                            $key = 'hot';
                            break;
                        case 'year':
                            $key = 'gold';
                            break;
                        default:
                            $key = 'hot';
                            break;
                    }

                    $update = array(
                        $key=>1,
                        $key.'_date'=>$data['end_date']
                    );

                    $this->db->update("users",$update,"id={$data['users_id']} limit 1");
                }
            }


            return json_encode(array(
                's' => $s,
                'city' => $city,
                'e' => implode('<br>', $e)
            ));
        }

        return $this->load->view(
            'plugins/addPlan',
            array(
                'user_id' => (int)$_POST['user_id'],
                'payments' => (new Payment())->getInfo(),
                'type' => $this->type
            )
        );
    }

    public function create()
    {
        parent::create(); // TODO: Change the autogenerated stub
    }

    public function process($id)
    {
        // TODO: Implement process() method.
    }

    public function deleteItem($id)
    {
        $item = $this->m->getOrderInfo($id);
        switch($item['type']) {
            case 'week':
            case 'month':
                $key = 'hot';
                break;
            case 'year':
                $key = 'gold';
                break;
            default:
                $key = 'hot';
                break;
        }

        $update = array(
            $key=>0,
            $key.'_date'=>""
        );

        $s = $this->db->update("ordersAccount",array(
            "status_id" => 0
        ),"id={$id} limit 1");

        if($s) {
            $this->db->update("users",$update,"id={$item['users_id']} limit 1");
        }

        return $s;
    }

    public function activateItem($id)
    {
        $item = $this->m->getOrderInfo($id);
        switch($item['type']) {
            case 'week':
            case 'month':
                $key = 'hot';
                break;
            case 'year':
                $key = 'gold';
                break;
            default:
                $key = 'hot';
                break;
        }



       $s = (new Orders())->confirmPayment($item['code']);

        $item = $this->m->getOrderInfo($id);

        $update = array(
            $key=>1,
            $key.'_date'=>$item['end_date']
        );

        if($s) {
            $this->db->update("users",$update,"id={$item['users_id']} limit 1");
        }

        return $s;
    }

    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    public function install(){return 1;}
    public function uninstall(){return 1;}
}