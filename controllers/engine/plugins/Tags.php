<?php
/**
 * Company Otakoyi.com
 * Author: wg
 * Date: 20.12.14 21:43
 */

namespace controllers\engine\plugins;

use controllers\Engine;

defined("SYSPATH") or die();

/**
 * Class Tags
 * @name Tags
 * @description мітки
 * @package controllers\engine\plugins
 * @author wmgodyak mailto:wmgodyak@gmail.com
 * @version 1.0
 * @copyright &copy; 2014 Otakoyi.com
 */
class Tags extends Engine{
    private $model;
    public function __construct()
    {
        parent::__construct();
        $this->model = $this->load->model('engine\plugins\Tags');
    }
    public function index(){}
    public function install(){return 1;}
    public function uninstall(){return 1;}
    public function create(){return $this->load->view('plugins/tags');}

    /**
     * @param $id
     * @return mixed|string
     */
    public function edit($id)
    {
        $tags = array();

        $l = $this->load->model('engine\Languages');
        $languages =$l->all(1);
        foreach ($languages as $lang) {
            $t = $this->model->get($id, $lang['id']);
            if(!empty($t)){
                foreach ($t as $row) {
                    $tags[$lang['id']][] = $row['name'];
                }
            }
        }

        return $this->load->view(
            'plugins/tags',
            array(
                'tags'      => $tags,
                'id'        => $id,
                'languages' => $languages
            )
        );
    }

    public function delete($id){}

    /**
     * @param $id
     * @return mixed|void
     */
    public function process($id)
    {
        if(empty($_POST['tags'])) return ;
        foreach ($_POST['tags'] as $languages_id => $tags) {

            $tags = explode(',', $tags);
            $this->model->set($id, $languages_id, $tags);
        }
    }

    /**
     * @return mixed
     */
    public function remove()
    {
        $content_id = $_POST['id'];
        $languages_id = $_POST['lang_id'];
        $name = $_POST['name'];

        return $this->model->remove($content_id, $languages_id, $name);
    }
}