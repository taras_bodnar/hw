<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 20.12.14 21:43
 */

namespace controllers\engine\plugins;

use controllers\engine\Plugin;
use controllers\engine\Plugins;

defined("SYSPATH") or die();

/**
 * Class TravelDate
 * @name TravelDate
 * @description description of plugin
 * @package controllers\engine\plugins
 * @author Taras Bodnar mailto:bania20091@gmail.com
 * @version 1.0
 * @copyright &copy; 2015 Otakoyi.com
 */
class TravelDate extends Plugins implements Plugin
{
    private $m;
    public function __construct()
    {
        parent::__construct();
        $this->m = new \models\engine\plugins\TravelDate();
    }
    /**
     * index method
     * @return mixed|string|void
     */
    public function index()
    {
    }

    public function create()
    {
    }

    public function edit($id)
    {
        $templates_id = $this->m->getParentType($id);
        $date = $this->m->getDate($id);
        if($templates_id==22) {
        $view = $this->load->view('plugins/TravelDate',array('travel_date'=>$date));
        } else {
            $view = '';
        }

        return $view;
    }

    public function delete($id)
    {
    }

    public function process($id)
    {
        if(!isset($_POST['travel_date'])) return'';
        $data = $_POST['travel_date'];
        return $this->m->addDate($data,$id);
    }

    /**
     * call when plugin install
     * @return int|mixed
     */
    public function install()
    {
        return 1;
    }

    /**
     * call when plugin uninstall
     * @return int|mixed
     */
    public function uninstall()
    {
        return 1;
    }
}