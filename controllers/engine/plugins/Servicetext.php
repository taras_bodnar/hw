<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 20.12.14 21:43
 */

namespace controllers\engine\plugins;

use controllers\engine\Plugin;
use controllers\engine\Plugins;

defined("SYSPATH") or die();

/**
 * Class ServiceText
 * @name ServiceText
 * @description description of plugin
 * @package controllers\engine\plugins
 * @author wmgodyak mailto:wmgodyak@gmail.com
 * @version 1.0
 * @copyright &copy; 2014 Otakoyi.com
 */
class Servicetext extends Plugins implements Plugin
{
    /**
     * index method
     * @return mixed|string|void
     */
    private $m;

    public function index()
    {
    }

    public function __construct()
    {
        parent::__construct();
        $this->m = $this->load->model('engine\plugins\Servicetext');
    }

    public function create()
    {
        $service = $this->m->getService();
        $city = $this->m->getCity();

        return $this->load->view('plugins/Servicetext', array(
            'service' => $service,
            'city' => $city
        ));
    }

    public function edit($id)
    {
        $service = $this->m->getService($id);
        $city = $this->m->getCity($id);

        return $this->load->view('plugins/Servicetext', array(
            'service' => $service,
            'city' => $city
        ));
    }

    public function delete($id)
    {
    }

    public function process($id)
    {
        $service = $this->request->post('service');
        $city = $this->request->post('city');

        $this->m->save($service, $city, $id);
    }

    /**
     * call when plugin install
     * @return int|mixed
     */
    public function install()
    {
        return 1;
    }

    /**
     * call when plugin uninstall
     * @return int|mixed
     */
    public function uninstall()
    {
        return 1;
    }
}