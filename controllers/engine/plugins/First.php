<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 20.12.14 21:43
 */

namespace controllers\engine\plugins;
use controllers\engine\Plugin;
use controllers\engine\Plugins;

defined("SYSPATH") or die();

/**
 * Class First
 * @name First
 * @description description of plugin
 * @package controllers\engine\plugins
 * @author wmgodyak mailto:wmgodyak@gmail.com
 * @version 1.0
 * @copyright &copy; 2014 Otakoyi.com
 */
class First extends Plugins implements Plugin{
    /**
     * index method
     * @return mixed|string|void
     */
    public function index(){}

    public function create(){}
    public function edit($id){return $this->load->view('plugins/first');}
    public function delete($id){}
    public function process($id){}

    /**
     * call when plugin install
     * @return int|mixed
     */
    public function install()
    {
       return 1;
    }

    /**
     * call when plugin uninstall
     * @return int|mixed
     */
    public function uninstall()
    {
       return 1;
    }
}