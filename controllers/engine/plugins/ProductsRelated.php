<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 20.12.14 21:43
 */

namespace controllers\engine\plugins;
use controllers\engine\Plugin;
use controllers\engine\Plugins;

defined("SYSPATH") or die();

/**
 * Class ProductsRelated
 * @name ProductsRelated
 * @description description of plugin
 * @package controllers\engine\plugins
 * @author wmgodyak mailto:wmgodyak@gmail.com
 * @version 1.0
 * @copyright &copy; 2014 Otakoyi.com
 */
class ProductsRelated extends Plugins implements Plugin{
    private $model;
    public function __construct()
    {
        parent::__construct();
        $this->model= $this->load->model('engine\plugins\ProductsRelated');
    }

    /**
     * index method
     * @return mixed|string|void
     */
    public function index(){}

    public function create(){}
    public function edit($id)
    {
        return $this->load->view(
            'plugins/ProductsRelated',
            array(
                'products_id' => $id,
                'items'       => $this->model->get($id)
            )
        );
    }

    public function sort()
    {
        $s = explode(',', $_POST['sort']);
        foreach ($s as $k=>$id) {
            if(empty($id)) continue;

            $this->model->sort($id, $k);
        }

        return 1;
    }

    public function deleteItem($id){return $this->model->delete($id);}

    public function delete($id){}
    public function process($id){}

    public function add($products_id, $related_id)
    {
        return $this->model->add($products_id,$related_id);
    }
    public function getJSON()
    {
        $products_id = (int) $_POST['pid'];
        $q  = strip_tags(trim($_POST['q']));
        $where ='';

        if(!empty($q)){
            $q = explode(' ', $q);
            foreach ($q as $k=>$v) {
                $v = trim($v);
                if(empty($v)) continue;
                $where .= " and i.name like '{$v}%'";
            }
        }
        $items = $this->model->getProducts($products_id, $where);
        $res = array(
                'total_count' => count($items),
                'incomplete_results' => false,
                'items' => $items
              );

        return json_encode($res);
    }
    /**
     * call when plugin install
     * @return int|mixed
     */
    public function install()
    {
       return 1;
    }

    /**
     * call when plugin uninstall
     * @return int|mixed
     */
    public function uninstall()
    {
       return 1;
    }
}