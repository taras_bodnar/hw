<?php
/**
 * Created by PhpStorm.
 * User: wg
 * Date: 11.03.15
 * Time: 10:42
 */

namespace controllers\engine;

defined('SYSPATH') or die();

use controllers\Engine;

/**
 * Class Features
 * @package controllers\engine
 */
class Features extends Engine{
    
    private $model;
    private $cat;
    
    public function __construct()
    {
        parent::__construct();

        $this->model = $this->load->model('engine\Features');
        $this->languages= $this->load->model('engine\Languages');
    }
    
    public function index()
    {
        $buttons= array(
            Form::button(
                $this->lang->core['create'],
                Form::icon('icon-file'),
                array(
                    'class'   => Form::BTN_TYPE_PRIMARY,
                    'onclick' => "self.location.href='features/create'"
                ))
        );

        $t = new Table('features', "features/items");
        $t
            ->setTitle($this->lang->features['table_title'])
            ->setConf('sortable', true)
            ->setConf('columns', array(
                'orderable'=> false
            ))
            ->sortableConf('features','id', 'f.id')
            ->addTh($this->lang->core['id'], 'w-20')
            ->addTh($this->lang->features['name'])
            ->addTh($this->lang->features['type'])
            ->addTh($this->lang->features['show_list'])
            ->addTh($this->lang->features['show_compare'])
            ->addTh($this->lang->features['show_filter'])
            ->addTh($this->lang->core['func'] , 'w-160')
        ;

        $this->setButtons($buttons);
        $this->setContent($t->render());

        return $this->output();
    }

    public function items()
    {
        $dt = new Table();
        return $dt
            -> table('features f')
            -> where("f.auto=0")
            -> join("join features_info i on i.features_id = f.id and i.languages_id = {$this->language_id}")
            -> columns(
                array(
                    'f.id',
                    'i.name',
                    'f.type',
                    'f.published',
                    'f.show_list',
                    'f.show_compare',
                    'f.show_filter',
                    'CONCAT("features/edit/", f.id) as href'
                )
            )
            -> searchCol(array('f.id','i.name'))
            -> returnCol(
                array(
                    'f.id',
                    'i.name',
                    'f.type',
                    'f.show_list',
                    'f.show_compare',
                    'f.show_filter',
                    'func'
                )
            )
            -> formatCol(
                array(
                    1 => '<a href="{href}">{name}</a>',
                    3 => Form::link(
                        '',
                        Form::icon('icon-eye-{show_list}'),
                        array(
                            'class'    =>'btn-primary',
                            'onclick' => 'engine.content.features.show({id},\'show_list\',{show_list})'
                        )
                    ),
                    4 => Form::link(
                        '',
                        Form::icon('icon-eye-{show_compare}'),
                        array(
                            'class'    =>'btn-primary',
                            'onclick' => 'engine.content.features.show({id},\'show_compare\',{show_compare})'
                        )
                    ),
                    5 => Form::link(
                        '',
                        Form::icon('icon-eye-{show_filter}'),
                        array(
                            'class'    =>'btn-primary',
                            'onclick' => 'engine.content.features.show({id},\'show_filter\',{show_filter})'
                        )
                    ),
                    6 => Form::link(
                            '',
                            Form::icon('icon-eye-{published}'),
                            array(
                                'class'    =>'btn-primary',
                                'title'   => $this->lang->core['published_tip'],
                                'onclick' => 'engine.content.features.pub({id},{published})'
                            )
                        ) .
                        Form::link(
                            '',
                            Form::icon('icon-edit'),
                            array(
                                'class'  =>'btn-info',
                                'title' => $this->lang->core['edit'],
                                'href'  => 'features/edit/{id}'
                            )
                        ) .
                        Form::link(
                            '',
                            Form::icon('icon-remove'),
                            array(
                                'title'   => $this->lang->core['delete'],
                                'class'   =>'btn-danger',
                                'onclick' => 'engine.content.features.delete({id})' // todo зробити модальне підтвердження і подивитись де провав тайтл
                            )
                        )
                ))
            -> request();
    }

    public function create()
    {
        $owner_id = Users::adminInfo('id');
        $id = $this->model->createAuto($owner_id);
        return $this->edit($id);
    }

    public function edit($id)
    {
        $buttons= array(
            Form::button(
                $this->lang->core['back'],
                Form::icon('icon-external-link'),
                array(
                    'class'=>'btn-link',
                    'onclick' => 'self.location.href=\'./features/index\''
                )
            ),
            Form::button(
                $this->lang->core['save'],
                Form::icon('icon-save', false),
                array(
                    'class'=>'btn-success form-submit'
                ))
        );
        $languages = $this->load->model('engine\Languages');
        $data = $this->model->data('features', $id);

        $content = $this->load->view(
            'features/form',
            array(
                'action'      => 'edit',
                'languages'   => $languages->all(1),
                'id'          => $id,
                'data'        => $data,
                'info'        => $this->model->fulInfo('features_info', 'features_id', $id),
                'type'        => $this->model->getType($data['type']),
                'categories'  => $this->categories(0,'', $this->model->getSelectedCategories($id)),
                'content_type' => $this->model->getContentType($id),
                'features_values' => $this->getValues($id)
            )
        );

        $this->setButtons($buttons);
        $this->setContent($content);

        return $this->output();
    }

    public function getValues($features_id, $type=null)
    {
        if(! $type){
            $type = $this->model->data('features', $features_id, 'type');
        }

        if(! in_array($type, array('radiogroup', 'checkboxgroup' ,'select', 'sm','so'))) return '';

        $values = $this->model->getValues($features_id);

        $res = '';

        foreach ($values as $values_id => $info) {
            $res .= $this->load->view(
                "features/value",
                array(
                    'features_id' => $features_id,
                    'value_id'    => $values_id,
                    'values'      => $info
                )
            );
        }

        return $this->load->view(
            'features/values',
            array(
                'features_id' => $features_id,
                'values'      => $res
            )
        );
    }


    public function createValue($features_id)
    {
        $value_id = $this->model->createValue($features_id);

        $values = array();
        foreach ($this->languages->all(1) as $row) {
            $values[] = array(
                'languages_id' => $row['id'],
                'placeholder'  => $row['name'],
                'value'        => ''
            );
        }

        return $this->load->view(
            "features/value",
            array(
                'features_id' => $features_id,
                'value_id'    => $value_id,
                'values'      => $values
            )
        );
    }

    public function removeValue($id)
    {
        return $this->model->removeValue($id);
    }


    /**
     * delete image
     * @param $id
     * @return int|mixed
     */
    public function delete($id)
    {
        return $this->model->delete($id);
    }

    /**
     * @param $id
     * @return mixed|string
     */
    public function process($id=0)
    {
        $s=0;
        $e = $this->errors['warning'];
        $r = 'features/index'; // redirect url
        $data = $_POST['data'];
        $info = $_POST['info'];

        // add or update values
        if(empty($this->error)) {
            switch($_POST['action']){
                case 'edit':
                    if(empty($id)) return '';
                    $data['auto'] = 0;
                    $r='';
                    $s = $this->model->update($id, $data, $info);
                    if($s > 0) {
                        $e = $this->errors['success'];
                        $this->error[] = $this->lang->features['save_success'];
                    } else {
                        $e = $this->errors['error'];
                        $this->error[] = $this->model->error() ;
                    }
                    break;
                default:
                    break;
            }
            if($id > 0){

                // привязка до контенту
                $this->setContentFeatures($id);

//              ривязка до типів контенту
                $this->setContentTypeFeatures($id);

//              оновлюю значення параметрів
                if(isset($_POST['features_value'])){
                    $this->model->updateValuesInfo($_POST['features_value']);
                }
            }

        }

        return json_encode(array(
            's' => $s > 0 , // status
            'id' => $id,
            'r' => $r, // redirect url
            'e' => $e, // error class
            't' => $this->lang->core[$e], // error title
            'm' => implode('<br>', $this->error) // error message
        ));
    }

    /**
     * @param $features_id
     * @return int
     */     
    private function setContentFeatures($features_id)
    {
        $this->model->deleteAllContentFeatures($features_id);
        if(isset($_POST['content_features']))
            foreach ($_POST['content_features'] as $k=>$content_id) {
            $this->model->createContentFeatures($features_id, $content_id );
        }

        return 1;
    }

    /**
     * categories list
     * @param int $parent_id
     * @param string $parent_name
     * @param array $selected
     * @return array
     */
    private function categories($parent_id=0, $parent_name='', $selected=array())
    {
        $r = $this->model->categories($parent_id);
        foreach ($r as $row) {
            if($parent_name != '') {
                $row['name'] = $parent_name .' / '. $row['name'];
            }
            if(in_array($row['id'], $selected)) {
                $row['selected'] = 'selected';
            }
            $this->cat[] = $row;
            if($row['isfolder']) {
                $this->categories($row['id'], $row['name'], $selected);
            }
        }

        return $this->cat;
    }


    /**
     * @param $features_id
     * @return int
     */
    private function setContentTypeFeatures($features_id)
    {
        // clear all content features
        $this->model->deleteAllContentTypeFeatures($features_id);

        if(isset($_POST['content_type_features']) && !empty($_POST['content_type_features'])){
            // не може бути і те і те
            $this->model->deleteAllContentFeatures($features_id);
            foreach ($_POST['content_type_features'] as $k=>$content_type_id) {
                $this->model->createContentTypeFeatures($features_id, $content_type_id );
            }
        }

        return 1;
    }


    /**
     * toggle published status
     * @param $id
     * @param $published
     * @return mixed
     */
    public function pub($id, $published)
    {
        $published = $published == 1 ? 0 : 1;
        return $this->model->pub($id, $published);
    }
    /**
     * toggle show status
     * @param $id
     * @param $col
     * @param $published
     * @return mixed
     */
    public function show($id, $col, $published)
    {
        $published = $published == 1 ? 0 : 1;
        return $this->model->show($id, $col, $published);
    }


    public function quickAddValue()
    {
        $content_id = (int) $_POST['content_id'];
        $features_id = (int) $_POST['features_id'];

        // вищначу тип параметру і чи мультимовний
        $data = $this->model->data('features', $features_id);

        return json_encode(array(
            't' => $this->lang->features['new_co_value'],
            'c' => $this->load->view(
                'features/quickAddValue',
                array(
                    'data'     => $data,
                    'languages'  => $this->languages->all(1),
                    'content_id' => $content_id,
                    'features_id' => $features_id
                )
            )
        ));
    }

    public function quickAddValueProcess()
    {
        $content_id = (int) $_POST['content_id'];
        $features_id = (int) $_POST['features_id'];
//        $this->dump($_POST);
        if(isset($_POST['value'])){
            $values_id = $this->model->createValue($features_id);
            $i=0; $name='';
            foreach ($_POST['value'] as $languages_id => $value) {
                $this->model->createValuesInfo($values_id, $languages_id, $value);
                if($i == 0) $name = $value;
                $i++;
            }

            $element = $this->load->view(
                'features/element',
                array(
                    'name'       => $name,
                    'features_id' => $features_id,
                    'values_id'   => $values_id,
                    'type'       => $_POST['type']
                )
            );

            return json_encode(array(
                'features_id'=> $features_id,
                'content_id' => $content_id,
                'v'          => $element,
                'type'       => $_POST['type']
            ));
        }
    }
}