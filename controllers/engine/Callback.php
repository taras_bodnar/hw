<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 31.05.14 18:36
 */
namespace controllers\engine;

use controllers\Engine;

defined('SYSPATH') or die();

class Callback extends Engine{
    private $mCallback;

    public function __construct()
    {
        parent::__construct();

        $this->mCallback = $this->load->model('engine\Callback');
    }
    /**
     * @return mixed|string
     */
    public function index()
    {
        $t = new DataTables();

        $t->setId('Callback')
            ->ajaxConfig('Callback/items')
            ->setTitle('Зворотні звінки')
            ->th('#')
            ->th('П.І.Б.')
            ->th('Телефон')
            ->th('Дата')
            ->th('Опрацьовано')
            ->th('Функції');

        $this->setContent($t->render());

        return $this->output();
    }

    /**
     * Callback list
     * @return string
     */
    public function items()
    {
        $t = new DataTables();
        $t  -> table('callbacks')
//            -> debug()
            -> searchCol('phone,name')
            -> get("id,phone,name,processed, date")
            -> execute();

        $r   = $t->getResults(false);

        $res = array();

        foreach ($r as $row) {
            $res[] = array(
                $row['id'],
                $row['name'],
                $row['phone'],
                $row['date'],
                $row['processed'] ? "<i title='ТАК' class='icon-ok'></i>" : '',
                "
                    <button onclick='engine.callback.edit({$row['id']});'class='btn btn-info' title='Редагувати'><i class='icon-pencil'></i></button>
                    <button onclick='engine.callback.del({$row['id']});' class='btn btn-danger' title='Видалити'><i class='icon-trash'></i></button>
                "
            );
        }


        return $t->renderJSON($res, $t->getTotal());
    }

    public function create(){}


    public function edit($id)
    {

        $data    = $this->mCallback->getData($id);

        return  $this->load->view('callback/form',array(
            'action'     => 'edit',
            'data'       => $data,
            'id'         => $id
        ));


    }

    public function delete($id)
    {
        return $this->mCallback->delete($id);
    }

    public function process($id)
    {
        if(empty($id)) return '';

        $data = $_POST['data'];
        $e=array(); $s=0; $t = 'Помилка';

        /*if(empty($data['date_from']) || empty($data['date_from'])){
            $e[] = 'Вкажіть період бронювання';
        } */

        if(empty($e)){
            // оновлюю замовлення
            $s   = $this->mCallback->update($id,$data);
            $e[] = 'Дані оновлено';
            $t   = 'Успішно';
        }

        return json_encode(
            array(
                't' => $t,
                's' => $s,
                'm' => implode('<br>', $e),
                'r' => 'Callback'
            )
        );

    }

    public function install()
    {
        return 1;
    }
    public function uninstall()
    {
        return 1;
    }
}
