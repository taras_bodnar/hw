<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wg.
 * Date: 31.05.14 18:36
 */
namespace controllers\engine;

use controllers\Engine;

defined('SYSPATH') or die();

class Backup extends Engine {

    /**
     * @return mixed|string
     */
    public function index()
    {
        $url = APPURL ."vendor/sxd/";
        $this->setContent("<div style='margin: 0 auto;width:600px;'><iframe src=\"{$url}\" width=\"586\" height=\"462\" frameborder=\"0\" style=\"margin:0;\"></iframe></div>");
        return $this->output();
    }

    public function edit($id){}
    public function create(){}
    public function delete($id){}
    public function process($id){}
    public function install()
    {
        return 1;
    }
    public function uninstall()
    {
        return 1;
    }


}
