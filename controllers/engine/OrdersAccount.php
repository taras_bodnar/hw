<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 31.05.14 18:36
 */
namespace controllers\engine;

use controllers\core\Settings;
use controllers\Engine;

defined('SYSPATH') or die();

class OrdersAccount extends Engine{

    public function __construct()
    {
        parent::__construct();
    }
    /**
     * @return mixed|string
     */
    public function index()
    {
        $t = new DataTables();

        $t->setId('orders')
            ->ajaxConfig('OrdersAccount/items')
            ->setTitle('Оплата Акаунтів')
            ->th('#')
            ->th('Виконавець')
            ->th('Сума')
            ->th('Оплачений')
            ->th('Дата');


        $this->setContent($t->render());

        return $this->output();
    }

    /**
     * orders list
     * @return string
     */
    public function items()
    {
        $t = new DataTables();
        $t  -> table('ordersAccount o')
//            -> debug()
            -> searchCol('o.code,u.email, u.name,u.surname')
            -> get("o.id,o.users_id,o.code,o.phone, u.name,u.email,u.surname,o.pay,o.pay_date,o.type,o.price,o.createdon
                ")
            -> join("join users u on u.id=o.users_id")
            -> execute();

        $r   = $t->getResults(false);

        $res = array();

        foreach ($r as $row) {
            switch($row['type']) {
                case 'week':
                    $total = Settings::instance()->get('priceWeek');
                    $type = 'здійснив платіж Hot Виконавця на 14 днів';
                    break;
                case 'month':
                    $total = Settings::instance()->get('priceMonth');
                    $type = 'здійснив платіж Hot Виконавця на 30 днів';
                    break;
                case 'year':
                    $total = Settings::instance()->get('priceYear');
                    $type = 'здійснив платіж gold Акаунта';
                    break;
                case 'oneC':
                    $total = Settings::instance()->get('priceOneC');
                    $type = 'здійснив платіж на одне місто';
                    break;
                case 'twoC':
                    $total = Settings::instance()->get('priceTwoC');
                    $type = 'здійснив платіж на два міста';
                    break;

            }
            $total = $row["price"];
            $res[] = array(
                $row['code'] ,
                "<a href='customers/edit/{$row['users_id']}'>{$row['name']} {$row['surname']}</a><br>
                тел: {$row['phone']} <a href='mailto:{$row['email']}'>{$row['email']}</a>
                ",
                $total . ' грн.',
                $type,
                ($row['pay']? "<label class='label label-success'>ТАК</label> {$row['pay_date']}" : 'НІ'),
                $row['pay_date'],
                ($row['pay'] == 0) ? 'НІ':'ТАК'
            );
        }


        return $t->renderJSON($res, $t->getTotal());
    }

    public function create(){}
    public function edit($id){}
    public function process($id){}
    public function delete($id){}


    public function install()
    {
        return 1;
    }
    public function uninstall()
    {
        return 1;
    }
}
