<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 05.07.14 20:38
 */

namespace controllers\engine;

use controllers\Engine;

defined('SYSPATH') or die();

class Routers extends Engine {

    public function __construct()
    {
        parent::__construct();

        $this->mg = $this->load->model('engine\Routers');
    }

    /**
     * @return mixed|string
     */
    public function index()
    {
        $buttons= array(
            Form::button(
                $this->lang->core['create'],
                Form::icon('icon-file'),
                array(
                    'class'   => Form::BTN_TYPE_PRIMARY,
                    'onclick' => "self.location.href='./routers/create'"
                ))
        );

        $t = new Table('routers', "routers/items");
        $t  ->setTitle($this->lang->routers['table_title'])
            ->setConf('sortable', true)
            ->setConf('columns', array(
                'orderable'=> false
            ))
            ->sortableConf('routers','id', 'id')
            ->addTh($this->lang->routers['id'])
            ->addTh($this->lang->routers['from'])
            ->addTh($this->lang->routers['to'])
            ->addTh($this->lang->routers['header'])
            ->addTh($this->lang->core['func'] , 'w-120')
        ;

        $this->setButtons($buttons);
        $this->setContent($t->render());

        return $this->output();
    }

    /**
     * @return string
     */
    public function items()
    {
        $dt = new Table();
        return $dt
            -> table('routers')
            -> columns(
                array(
                    'id',
                    'url_from',
                    'url_to',
                    'header'
                )
            )
            -> searchCol(array('id', 'url_from', 'url_to','header'))
            -> returnCol(array('id', 'url_from', 'url_to','header', 'func'))
            -> formatCol(
                array(
                    4 =>
                        Form::link(
                            '',
                            Form::icon('icon-edit'),
                            array(
                                'class'  =>'btn-info',
                                'title' => $this->lang->core['edit'],
                                'href'  => 'routers/edit/{id}'
                            )
                        ) .
                        Form::button(
                            '',
                            Form::icon('icon-remove'),
                            array(
                                'title'   => $this->lang->core['delete'],
                                'class'   =>'btn-danger',
                                'onclick' => 'engine.routers.delete({id})'
                            )
                        )
                ))
            -> request();
    }

    /**
     * @return mixed|string
     */
    public function create()
    {
        $buttons= array(
            Form::button(
                $this->lang->core['back'],
                Form::icon('icon-external-link'),
                array(
                    'class'=>'btn-link',
                    'onclick' => 'self.location.href=\'./routers\''
                )
            ),
            Form::button(
                $this->lang->core['save'],
                Form::icon('icon-save', false),
                array(
                    'class'=>'btn-success form-submit'
                ))
        );


        $content = $this->load->view('routers_form',array(
            'action'  => 'create',
            'id'      => 0,
            'headers' => $this->getHeaders()
        ));


        $this->setButtons($buttons);
        $this->setContent($content);

        return $this->output();
    }
    private function getHeaders($selected='')
    {
        $headers = '';
        $h = $this->mg->getHeaders();
        foreach ($h as $k => $header) {
            $headers[] = array(
                'value' =>$header,
                'name' =>$header,
                'selected' => $selected == $header ? 'selected': ''
            );
        }

        return $headers;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $buttons= array(
            Form::button(
                $this->lang->core['back'],
                Form::icon('icon-external-link'),
                array(
                    'class'=>'btn-link',
                    'onclick' => 'self.location.href=\'routers\''
                )
            ),
            Form::button(
                $this->lang->core['save'],
                Form::icon('icon-save', false),
                array(
                    'class'=>'btn-success form-submit'
                ))
        );
        $data = $this->mg->data('routers', $id);
        $content = $this->load->view('routers_form',array(
            'action'     => 'edit',
            'data'       => $data,
            'id'         => $id,
            'headers' => $this->getHeaders()
        ));

        // output

        $this->setButtons($buttons);
        $this->setContent($content);

        return $this->output();
    }

    /**
     * @param $id
     * @return mixed|string
     */
    public function process($id=0)
    {
        $s=0;
        $e = $this->errors['warning'];
        $r = './routers/index'; // redirect url
        $data = $_POST['data'];

        // check required fields
        if(
            empty($data['url_from']) ||
            empty($data['url_to'])
        ) {
            $this->error[] = $this->lang->core['e_required_fields'];
        }
        // add or update values
        if(empty($this->error)) {
            switch($_POST['action']){
                case 'create':
                    $id = $this->mg->create($data);
                    if($id > 0) {
                        $s = $id;
                        $e = $this->errors['success'];
                        $this->error[] = $this->lang->routers['save_success'];
                    } else {
                        $e = $this->errors['error'];
                        $this->error[] = $this->mg->error();
                    }
                    break;
                case 'edit':
                    if(empty($id)) return '';
                    $r='';
                    $s = $this->mg->update($id, $data);
                    if($s > 0) {
                        $e = $this->errors['success'];
                        $this->error[] = $this->lang->routers['save_success'];
                    } else {
                        $e = $this->errors['error'];
                        $this->error[] = $this->mg->error() ;
                    }
                    break;
                default:
                    break;
            }
        }

        return json_encode(array(
            's' => $s > 0 , // status
            'r' => $r, // redirect url
            'e' => $e, // error class
            't' => $this->lang->core[$e], // error title
            'm' => implode('<br>', $this->error) // error message
        ));
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        return $this->mg->delete($id);
    }
} 