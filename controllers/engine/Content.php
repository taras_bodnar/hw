<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 31.05.14 18:36
 */

namespace controllers\engine;

use controllers\Engine;
use controllers\engine\content\Images;
use controllers\engine\content\Type;
use controllers\core\Config;
use controllers\core\Settings;

defined('SYSPATH') or die();

class Content extends Engine {
    /**
     * content type id
     * @var int
     */
    protected $type_id = 1;
    /**
     * content type
     * @var string
     */
    private   $type    = 'page';
    /**
     * autocreated label
     * @var bool
     */
    protected $created = false;
    /**
     * form vars array
     * @var array
     */
    private $form_data = array();
    /**
     * path to form view
     * @var string
     */
    private $form_path = 'content/content_form';

    /**
     * form response array
     * @var array
     */
    private $form_response = array();
    /**
     * default form fields
     * @var array
     */
    private $form_fields = array(
        'name',
        'title',
        'alias',
        'keywords',
        'description',
        'content',
        'templates_id',
        'content_images',
        'content_options'
    );

    /**
     * content data
     * @var array
     */
    protected $data = array();

    public function __construct()
    {
        parent::__construct();

        $this->ms = $this->load->model('engine\Content');
        $this->languages = $this->load->model('engine\Languages');
    }



    public function index(){}
    /**
     * встановлює тип контенту
     * @param $type
     * @return $this
     */
    protected function setType($type)
    {
        $this->type = $type;
        $this->setTypeId();

        return $this;
    }

    /**
     * вертає тип контенту
     * @return string
     */
    protected function getType()
    {
        return $this->type;
    }

    /**
     * встановлює ід типу контенту по вказаному типу
     * @return $this
     */
    protected function setTypeId()
    {
        $this->type_id = $this->ms->getTypeId($this->type);

        if(empty($this->type_id)){
            // відсутній тип автостворення
            $mtype = $this->load->model('engine\content\Type');
            $this->type_id = $mtype->create(array('name'=> ucfirst($this->type), 'type' => $this->type));
            if($this->type_id>0){
                $ctype = new Type();
                $ctype->createDir($this->type);
            }
        }

        return $this;
    }

    /**
     * вертає ід типу контенту
     * @return int
     */
    protected function getTypeId()
    {
        return $this->type_id;
    }

    /**
     * @param int $parent_id
     * @return mixed|string
     */
    public function create($parent_id=0)
    {
        $templates_id = $this->ms->defaultTemplatesId($this->type, $parent_id);

        if(empty($templates_id)) return '<div class="alert-danger alert text-center">WRONG templates ID</div>';

        $type_id      = $this->type_id;
        $owner_id     = Users::adminInfo('id');
        if($this->type == 'product'){
            $parent_id=0;
        }
        $id = $this->ms->autoCreate($parent_id, $type_id, $owner_id, $templates_id);
        $this->created = true;
        return $this->edit($id);
    }

    /**
     * add Form data
     * @param $name
     * @param $value
     * @return $this
     */
    protected function formAppendData($name, $value)
    {
        $this->form_data[$name] = $value;

        return $this;
    }

    /**
     * config form field
     * @param $field
     * @param int $display
     * @return $this
     */
    protected function configFormField($field, $display = 1)
    {
        if($display == 0){
            $k = array_search($field, $this->form_fields);
            if($k){
                unset($this->form_fields[$k]);
            }
        } else {
            if(!in_array($field, $this->form_fields)) {
                $this->form_fields[] = $field;
            }
        }

        return $this;
    }

    /**
     * edit content
     * @param $id
     * @return mixed|string
     */
    public function edit($id)
    {
        $this->data = $this->ms->data('content', $id);
        if(empty($id) || empty($this->data)) die( '<div class="alert-danger alert text-center">WRONG ID</div>' ) ;

        // додаю кнопки справа

        $this->setButtons(
            Form::button(
                $this->lang->core['save'],
                Form::icon('icon-save', false),
                array(
                    'class'=>'btn-success form-submit'
                )
            )
        );

        $languages = $this->load->model('engine\Languages');

        // images
        $images = new Images();

        // default form data
        $this ->formAppendData('form_action',        "content/process/$id")
              ->formAppendData('title',              $this->lang->pages['title'])
              ->formAppendData('languages',          $languages->all(1))
              ->formAppendData('action',             'edit')
              ->formAppendData('data',               $this->data)
              ->formAppendData('info',               $this->ms->info($id))
              ->formAppendData('id',                 $id)
              ->formAppendData('parent_id',          $this->data['parent_id'])
              ->formAppendData('templates',          $this->ms->templates($this->type, $this->data['templates_id']))

              ->formAppendData('autofil_title',      Settings::instance()->get('autofil_title'))
              ->formAppendData('autotranslit_alias', Settings::instance()->get('autotranslit_alias'))
              ->formAppendData('images',             $images->get($id))
              ->formAppendData('type',               $this->type)
              ->formAppendData('parent_id',          $this->data['parent_id'])
              ->formAppendData('plugins',            $this->getPlugins($id,'edit'))
              ->formAppendData('created',            $this->created)
              ->formAppendData('features',           $this->getFeatures($id))
        ;

    }

    /**
     * @param $content_id
     * @return string
     * @throws \controllers\core\Exceptions
     */
    public function getFeatures($content_id)
    {
        $features = $this->ms->getFeatures($content_id, $this->type);

        return $this->load->view(
            'content/features/values',
            array(
                'items'=> $features,
                'use_remove_link' => !in_array($this->type, array('product')),
                'enable_values'   => !in_array($this->type, array('category')),
                'content_id'      => $content_id,
                'enable_button'   => 1
            )
        );
    }

    /**
     *
     */
    public function sortFeatures()
    {
        $content_id = (int)$_POST['content_id'];
        $sort = explode(',',$_POST['sort']);
        foreach ($sort as $i=>$features_id) {
            $features_id = ltrim($features_id, 'co-');
            if(empty($features_id)) continue;

            $this->ms->sortFeatures($content_id, $features_id, $i);
        }
    }
    public function removeFeatures()
    {
        $content_id = (int)$_POST['content_id'];
        $features_id = (int)$_POST['features_id'];
        if(empty($content_id) || empty($features_id)) return 0;

        return $this->ms->removeFeatures($content_id, $features_id);
    }

    public function AddModalFeatures()
    {
        $content_id = (int)$_POST['content_id'];

        return $this->load->view(
            'content/features/picklist',
            array(
               'content_id' => $content_id,
               'features'   => $this->ms->getFeaturesPicklist($content_id)
            )
        );

    }

    public function pickFeatures()
    {
        $content_id = (int)$_POST['content_id'];
        $type= $this->ms->getContentType($content_id);
        $this->type = $type;

        $features = $this->ms->pickFeatures($content_id);
        $out='';
        if(!empty($features)){
            $features = $this->ms->getFeatures($content_id, $this->type, implode(',', $features));

            $out = $this->load->view(
                'content/features/values',
                array(
                    'items'=> $features,
                    'use_remove_link' => 1,
                    'enable_values'   => !in_array($this->type, array('category')) ? 1 : 0,
                    'content_id'      => $content_id,
                    'enable_button'   => 0,
                )
            );
        }
        return json_encode(array(
            's' => 1,
            'c' => $out,
            'type' => $this->type
        ));
    }
    public function createModalFeatures($content_id)
    {
        $this->mOpt = $this->load->model('engine\Features');
        $owner_id = Users::adminInfo('id');
        $id = $this->mOpt->createAuto($owner_id);
        $type = $this->mOpt->getType();
        foreach ($type as $k=>$row) {
            $type[$k]['name'] = $this->lang->features['type_' . $row['name']];

            if(
                in_array($this->type, array('category', 'product'))
                && !in_array($row['name'], array('sm'))
            ) {
                unset($type[$k]);
            }
        }
//        $this->dump($type);
        return json_encode(array(
            't' => $this->lang->features['create_features_title'],
            'c' => $this->load->view(
                'content/features/create_modal',
                array(
                    'id' => $id,
                    'content_id' => $content_id,
                    'action'     => 'edit',
                    'languages'  => $this->languages->all(1),
                    'type'       => $type,
                )
            )
        ));
    }

    public function processCreateModal($id)
    {
        $this->mOpt = $this->load->model('engine\Features');
        $s=0;
        $e = $this->errors['warning'];
        $data = $_POST['data'];
        $info = $_POST['info'];
        $values='';

        // add or update values

        if(empty($this->error)) {
            $data['auto'] = 0;
            $s = $this->mOpt->update($id, $data, $info);
            if($s > 0) {
                $e = $this->errors['success'];
                $this->error[] = $this->lang->features['save_success'];
            } else {
                $e = $this->errors['error'];
                $this->error[] = $this->mOpt->error() ;
            }

            $cid=$content_id = $_POST['content_id'];
            $this->type = $this->ms->getContentType($content_id);

            if($this->type == 'product'){
                $cid = $this->ms->getProductsCategory($content_id);
            }

            $this->mOpt->createContentFeatures($id, $cid);
            $features = $this->ms->getFeatures($content_id, $this->type, $id);

            $values = $this->load->view(
                'content/features/values',
                array(
                    'items'=> $features,
                    'use_remove_link' => $this->type == 'product' ? 0 : 1,
                    'enable_values'   => !in_array($this->type, array('category')),
                    'content_id'      => $content_id,
                    'enable_button'   => 0
                )
            );

        }

        return json_encode(array(
            's' => $s > 0 , // status
            'e' => $e, // error class
            'v' => $values,
            'r' =>'',
            't' => $this->lang->core[$e], // error title
            'm' => implode('<br>', $this->error) // error message
        ));
    }
    /**
     * @param bool $output render content and display all or only set content data
     * @return string
     */
    protected function renderContent($output=true)
    {
        // налаштування , показувати / ховати ті чи інші поля

        $this->formAppendData('form_fields', $this->form_fields);

        $content = $this->load->view($this->form_path, $this->form_data);

        $this->setContent($content);

        if($output){
            return $this->output();
        }
    }

    /**
     * @param $id
     * @return mixed|string
     */
    public function process($id)
    {
        if(empty($id)) die('WRONG ID');

        $data = $_POST['data'];
        $info = $_POST['info'];

        $s=0;
        $e = $this->errors['error'];

        // add or update values
        if(empty($this->error)) {
            $data['auto']    = 0;
            $data['type_id'] = $this->type_id;
            if(empty($id)) return '';

            $s = $this->ms->update($id, $data, $info);
            if($s > 0) {
                if(isset ($data['parent_id']) && $data['parent_id'] > 0) $this->ms->toggleFolder($data['parent_id']);
                $e = $this->errors['success'];
                $this->error[] = $this->lang->pages['save_success'];
            } else {
                $e = $this->errors['error'];
                $this->error[] = $this->ms->error() ;
            }

           $s+= $this->ms->saveFeatures($id);


           $this->getPlugins($id,'process');
        }

        $this->setFormResponse('s',  $s > 0)
             ->setFormResponse('r',  isset($_POST['redirect_url']) ? $_POST['redirect_url'] : '')
             ->setFormResponse('e',  $e)
             ->setFormResponse('id', $id)
             ->setFormResponse('t',  $this->lang->core[$e])
             ->setFormResponse('m',  implode('<br>', $this->error))
        ;

    }

    protected function saveNavMenus($id)
    {
        if(!isset($_POST['nav_menu'])) return 0;

        $selected = $this->ms->selectedNavMenu($id);

        foreach ($_POST['nav_menu'] as $nav_menu_id => $s) {
            if($s == 1 && !in_array($nav_menu_id, $selected)) {
                $this->ms->addMenuItem($nav_menu_id,$id);
            }
            if($s == 0) $this->ms->deleteNavMenus($id, $nav_menu_id);
        }

        return 1;
    }
    /**
     * @param $name
     * @param $value
     * @return $this
     */
    private function setFormResponse($name, $value)
    {
        $this->form_response[$name] = $value;

        return $this;
    }


    /**
     * getter form response
     * @param $key
     * @return array
     */
    protected function getFormResponse($key=null)
    {
        if($key && isset($this->form_response[$key])) {
            return $this->form_response[$key];
        }

        return $this->form_response;
    }

    /**
     * delete item from pages
     * @param $id
     * @return mixed 1
     */
    public function delete($id)
    {
        $s = $this->ms->delete($id);
        if( $s > 0 ){
            $this->deleteFiles(DOCROOT . '/uploads/content/' . $id . '/');
        }

        // delete plugins data
        $this->getPlugins($id,'delete');

        return $s;
    }

    /**
     * toggle published status
     * @param $id
     * @param $published
     * @return mixed
     */
    public function pub($id, $published)
    {
        $published = $published == 1 ? 0 : 1;
        return $this->ms->pub($id,$published);
    }

    public function mkAlias($parent_id, $languages_id, $languages_code, $alias=null)
    {
        if(!$alias){
            $alias = $_POST['alias'];
        }

        if(empty($alias)) return '';

        // ПОТРІБНО СФОРМУВАТИ СПИСОК ВЕРХНІХ КАТЕГОРІЙ
        $parent_alias = $parent_id > 1 ? $this->ms->parentAlias($parent_id, $languages_id) : '';
        return ( empty($parent_alias) ? '' : $parent_alias . '/' )  . self::translit($alias, $languages_code);
    }

    public static function translit($text,$code){

        $text = mb_strtolower(trim($text),'utf8');
        $text = preg_replace("/[^A-Za-z0-9а-яА-Яіїєёыэъñéèàùêâôîûëïüÿç\- \/]/u", "", $text);
        $text = str_replace('/','-', $text);
        $table = Config::instance()->get('translit.' . $code);
        if(empty($table)) {
            $table = Config::instance()->get('translit.def');
        }

        $text = strtr($text,$table);

        return $text;
    }

}
