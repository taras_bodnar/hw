<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 11.11.14 15:04
 */

namespace controllers\engine;

defined("SYSPATH") or die();

class Admin {

    /**
     * @param $key
     * @param null $value
     * @return null
     */
    final public static function data($key, $value=null)
    {
        if($value) {
            $_SESSION['admin'][$key] = $value;
        }

        return isset($_SESSION['admin'][$key]) ? $_SESSION['admin'][$key] : null;
    }

} 