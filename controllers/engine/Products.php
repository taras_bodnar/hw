<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 31.05.14 18:36
 */

namespace controllers\engine;

use controllers\core\Settings;

defined('SYSPATH') or die();

/**
 * Class products
 * @package controllers\engine
 */
class Products extends Content implements Component {

    private $cat = array();// categories array
    private $parent_id='';
    /**
     * global products settings
     * @var array
     */
    private $settings = array();

    public function __construct()
    {
        parent::__construct();

        // встановлюю тип контенту сторінка
        $this->setType('product');
        $this->mCat = $this->load->model('engine\products\Categories');
        $this->mPo = $this->load->model('engine\products\Options');

        $this->settings['update_prices'] = Settings::instance()->get('products_list_update_price');
    }

    public function install()
    {
        return 1;
    }
    public function uninstall()
    {
        return 1;
    }

    /**
     * список сторінок
     * @param string $parent_id
     * @return mixed|string
     */
    public function index($parent_id = '')
    {
        $this->setButtons
            (
                Form::button(
                    $this->lang->core['create'],
                    Form::icon('icon-file'),
                    array(
                        'class'   => Form::BTN_TYPE_PRIMARY,
                        'onclick' => "self.location.href='products/create/{$parent_id}'"
                    ))
            );

        $t = new DataTables();
        $t  -> ajaxConfig("products/items/$parent_id")
            -> setTitle($this->lang->products['table_title'])
            -> setId('products')
            -> th($this->lang->content['id'], 'w-20')
            -> th($this->lang->content['name']);
            if($this->settings['update_prices']) {
                $t ->th($this->lang->products['price'], 'w-160');
            }
            $t->th($this->lang->core['func'] , 'w-180')
            ;

        $this->setContent( $this->filterForm() . $t->render());

        return $this->output();
    }

    /**
     * @return string
     */
    private function filterForm()
    {
        $m = $this->ms->manufacturers();

        $ms = array(array(
            'id' => '',
            'name' => $this->lang->products['f_all']
        ));
        foreach ($m as $row) {
            $ms[] = $row;
        }
//        $this->dump($ms);

        return $this->load->view(
            'shop/filter',
            array(
                'manufacturers' => $ms,
                'status' => array(
                    array(
                        'id' => '',
                        'name' => $this->lang->products['f_all'],
                        'selected' => ''
                    ),
                    array(
                        'id' => 'hit',
                        'name' => $this->lang->products['f_hit'],
                        'selected' => ''
                    ),
                    array(
                        'id' => 'new',
                        'name' => $this->lang->products['f_new'],
                        'selected' => ''
                    ),
                    array(
                        'id' => 'sale',
                        'name' => $this->lang->products['f_sale'],
                        'selected' => ''
                    ),
                    array(
                        'id' => 'published',
                        'name' => $this->lang->products['f_pub'],
                        'selected' => ''
                    ),
                    array(
                        'id' => 'hidden',
                        'name' => $this->lang->products['f_unpub'],
                        'selected' => ''
                    ),
                    array(
                        'id' => 'availability',
                        'name' => $this->lang->products['f_av'],
                        'selected' => ''
                    ),
                    array(
                        'id' => 'not_availability',
                        'name' => $this->lang->products['f_not_av'],
                        'selected' => ''
                    )
                )
            )
        );
    }

    public function items($cid=0)
    {
        $t = new DataTables();
        $t  -> table('content c ')
//            -> debug()
            -> searchCol('c.id, i.name,code')
            -> get("c.id, c.published, i.name,i.alias, po.code, po.price,po.availability,po.hit,po.sale,po.new")
            -> join("join content_type ct on ct.type='product' and ct.id=c.type_id")
            -> join("join content_info i on i.content_id=c.id and i.languages_id={$this->language_id}")
            -> join("left join products_options po on po.products_id=c.id and po.is_variant = 0 ");

        if($cid>0){
          $t->join("join products_categories pc on pc.pid=c.id and pc.cid={$cid}");
        }
        $t  -> execute();

        $r   = $t->getResults(false);

        $res = array();

        foreach ($r as $row) {
            $row['img'] = $this->images->cover($row['id'], 'thumbnails');
            $btn_update = Form::link(
                '',
                Form::icon('icon-refresh'),
                array(
                    'class'    =>'btn-primary',
                    'title'   => $this->lang->products['update_price'],
                    'onclick' => 'engine.products.updatePrice('.$row['id'].',$(\'#t-p-'.$row['id'].'\').val())'
                )
            );
            $res[] = array(
                $row['id'],
                ($row['img'] == ''? '' : "<img class='img-thumbnail' style='max-width: 120px;float: left; margin-right: 10px;' src='{$row['img']['src']}'>").
                " <a href='/{$row['alias']}' target='_blank'>{$row['name']} <i class='icon-share-alt'></i> </a>",
                '<div class="input-group">
                        <input type="text" class="form-control " id="t-p-'.$row['id'].'" value="'.$row['price'].'">
                        <span class="input-group-addon">'. $btn_update .'</span>
                     </div>',

                Form::link(
                    '',
                    Form::icon('icon-money'),
                    array(
                        'class'    =>'btn-primary ' . ($row['sale']? 'btn-success' : ''),
                        'title'   => $this->lang->products['sale'],
                        'onclick' => 'engine.products.sale('. $row['id'] .','.$row['sale'].')'
                    )
                ) . Form::link(
                    '',
                    Form::icon('icon-star'),
                    array(
                        'class'    =>'btn-primary ' . ($row['hit']? 'btn-success' : ''),
                        'title'    => $this->lang->products['hit'],
                        'onclick'  => 'engine.products.hit('. $row['id'] .','.$row['hit'].')'
                    )
                ) . Form::link(
                    '',
                    Form::icon('icon-asterisk '),
                    array(
                        'class'    =>'btn-primary ' . ($row['new']? 'btn-success' : ''),
                        'title'   => $this->lang->products['new'],
                        'onclick' => 'engine.products.toggleNew('. $row['id'] .','.$row['new'].')'
                    )
                ).
                Form::link(
                    '',
                    Form::icon('icon-eye-' . ($row['published'] ? 'open' : 'close')),
                    array(
                        'class'    =>'btn-primary',
                        'title'   => $this->lang->core['published_tip'],
                        'onclick' => 'engine.products.pub('. $row['id'] .','.$row['published'].')'
                    )
                ) .
                Form::link(
                    '',
                    Form::icon('icon-edit'),
                    array(
                        'class'  =>'btn-info',
                        'title' => $this->lang->core['edit'],
                        'href'  => 'products/edit/' . $row['id']
                    )
                ) .
                Form::link(
                    '',
                    Form::icon('icon-remove'),
                    array(
                        'title'   => $this->lang->core['delete'],
                        'class'   =>'btn-danger',
                        'onclick' => 'engine.products.delete('.$row['id'].')'
                    )
                )
            );
        }


        return $t->renderJSON($res, $t->getTotal());
    }
    /**
     * ajax response tale items
     * @param int $parent_id
     * @return string
     */
    public function _items($parent_id=0)
    {
//        $img= APPURL . 'uploads/content/{id}/thumbnails/{img}';
        $parent_id = (int) $parent_id;
        $dt = new Table();
        $returnCol = array('id','name' ,'price', 'func');
        $btn_update = Form::link(
            '',
            Form::icon('icon-refresh'),
            array(
                'class'    =>'btn-primary',
                'title'   => $this->lang->products['update_price'],
                'onclick' => 'engine.products.updatePrice({id},$(\'#t-p-{id}\').val())'
            )
        );
        $formatCol =
            array(
                1 => '{img}
                      <a title="'. $this->lang->core['edit'] .'" href="{href}">{name}</a>
                      ',
                2 => '<div class="input-group">
                        <input type="text" class="form-control " id="t-p-{id}" value="{price}">
                        <span class="input-group-addon">'. $btn_update .'</span>
                     </div>',
                3 => Form::link(
                        '',
                        Form::icon('icon-money'),
                        array(
                            'class'    =>'btn-primary {sale_status}',
                            'title'   => $this->lang->products['sale'],
                            'onclick' => 'engine.products.sale({id},{sale})'
                        )
                    ) . Form::link(
                        '',
                        Form::icon('icon-star'),
                        array(
                            'class'    =>'btn-primary {hit_status}',
                            'title'    => $this->lang->products['hit'],
                            'onclick'  => 'engine.products.hit({id},{hit})'
                        )
                    ) . Form::link(
                        '',
                        Form::icon('icon-asterisk '),
                        array(
                            'class'    =>'btn-primary {new_status}',
                            'title'   => $this->lang->products['new'],
                            'onclick' => 'engine.products.toggleNew({id},{new})'
                        )
                    ).
                    Form::link(
                        '',
                        Form::icon('icon-eye-{published}'),
                        array(
                            'class'    =>'btn-primary',
                            'title'   => $this->lang->core['published_tip'],
                            'onclick' => 'engine.products.pub({id},{published})'
                        )
                    ) .
                    Form::link(
                        '',
                        Form::icon('icon-edit'),
                        array(
                            'class'  =>'btn-info',
                            'title' => $this->lang->core['edit'],
                            'href'  => 'products/edit/{id}'
                        )
                    ) .
                    Form::link(
                        '',
                        Form::icon('icon-remove'),
                        array(
                            'title'   => $this->lang->core['delete'],
                            'class'   =>'btn-danger',
                            'onclick' => 'engine.products.delete({id})' // todo зробити модальне підтвердження і подивитись де провав тайтл
                        )
                    )
            );
        if($this->settings['update_prices'] == 0) {
            $formatCol[2] = $formatCol[3];
            unset($formatCol[3]);
        }

        $dt
            -> table('content c')
            -> where("c.type_id={$this->type_id}");
        if($parent_id > 0) {
            $dt->join("join products_categories pt on pt.pid=c.id and pt.cid={$parent_id}");
        }
//        Filter
        if(isset($_POST['filter'])){
            $filter = $_POST['filter'];
            if(isset($filter['mid']) && !empty($filter['mid'])){
                $mid = (int) $filter['mid'];
                if($mid > 0){
                    $dt->where("o.manufacturers_id = '$mid' ");
                }
            }
            if(isset($filter['status']) && !empty($filter['status'])){

                switch($filter['status']){
                    case 'hit':
                        $dt->where("o.hit = 1");
                        break;
                    case 'new':
                        $dt->where("o.new = 1");
                        break;
                    case 'sale':
                        $dt->where("o.sale = 1");
                        break;
                    case 'published':
                        $dt->where("c.published = 1");
                        break;
                    case 'hidden':
                        $dt->where("c.published = 0");
                        break;
                    case 'availability':
                        $dt->where("o.availability = 1");
                        break;
                    case 'not_availability':
                        $dt->where("o.availability = 0");
                        break;
                    default:
                        break;
                }
            }

        }

//        Filter

        $dt
            -> join("join content_info i on i.content_id = c.id and i.languages_id = {$this->language_id}")
            -> join("left join content_images im on im.content_id = c.id and im.sort = 1 ")
            -> join("left join products_options o on o.products_id = c.id and o.is_variant=0")
            -> columns(
                array(
                    'c.id', 'o.sale', 'o.hit', 'o.new','o.price',
                    'i.name',
                    'c.published',
                    'IF(o.sale = 1, "btn-success" , "") as sale_status ',
                    'IF(o.new = 1, "btn-success" , "") as new_status ',
                    'IF(o.hit = 1, "btn-success" , "") as hit_status ',
                    'CONCAT("products/edit/", c.id) as href',
                    "IF(
                    im.name <>'',
                     CONCAT(
                               '<img src=\"',
                               '/uploads/content/',
                               c.id,
                               '/thumbnails/',
                               im.name,
                               '\"
                               class=\"img-polaroid\" style=\"max-width:120px;max-height:70px; float:left;\">'
                           ),
                     '<span style=\"font-size: 62px;float:left;line-height: 0;color:#ccc\"><i class=\"icon-instagram\">&nbsp;</i></span>'
                     ) as img"
                )
            )
            -> searchCol(array('c.id','i.name'))
            -> returnCol($returnCol)
            -> formatCol($formatCol);
        return $dt-> request();
    }

    /**
     * auto crate page id vs default params
     * @param int $parent_id
     * @return mixed|string
     */
    public function create($parent_id=0)
    {
        return parent::create($parent_id);
    }

    /**
     * edit page
     * @param $id
     * @return mixed|string
     */
    public function edit($id)
    {
        # 1. Встановлюю які поля потрібно відображати у формі
        $this->configFormField('nav_menu',0);

        # 2. Заповнюю контент даними по замовчуванню
        parent::edit($id);

        # 2.a Додаю кнопочку назад

        $this->prependToButtons(
            Form::button(
                $this->lang->core['back'],
                Form::icon('icon-external-link'),
                array(
                    'class'=>'btn-link',
                    'onclick' => 'self.location.href=\'products/index/'.$this->parent_id.'\''
                )
            )
        );

        # 3. Додаю параметри для форми

        $selected_nav_menu = $this->ms->selectedNavMenu($id, $this->created);

        $sc = $this->mCat->get($id);
        $po = $this->mPo->getData($id);

        $this->formAppendData('form_action',       "products/process/$id")
             ->formAppendData('nav_menu',          $this->ms->getNavMenus())
             ->formAppendData('selected_nav_menu', $selected_nav_menu)
             ->formAppendData('use_product_options', true)
             ->formAppendData('manufacturers',     $this->ms->manufacturers())
             ->formAppendData('categories',        $this->categories(0, '', $sc))
             ->formAppendData('product_options',   $po)
             ->formAppendData('redirect_url',     'products');


        # 4. Генерую контент і виводжу на екран

        return $this->renderContent();
    }


    /**
     * @param $id
     * @return mixed|string
     */
    public function process($id)
    {
        parent::process($id);

        $this->updateCategories($id);
        $this->updateProductOptions($id);

        return json_encode($this->getFormResponse());
    }

    /**
     * видалення сторінки
     * @param $id
     * @return mixed 1
     */
    public function delete($id)
    {
        return parent::delete($id);
    }


    /**
     * categories list
     * @param int $parent_id
     * @param string $parent_name
     * @param array $selected
     * @return array
     */
    private function categories($parent_id=0, $parent_name='', $selected=array())
    {
        $r = $this->ms->categories($parent_id);
        foreach ($r as $row) {
            if($parent_name != '') {
                $row['name'] = $parent_name .' / '. $row['name'];
            }
            if(in_array($row['id'], $selected)) {
                $row['selected'] = 'selected';
            }
            $this->cat[] = $row;
            if($row['isfolder']) {
                $this->categories($row['id'], $row['name'], $selected);
            }
        }

        return $this->cat;
    }

    /**
     * update products categories
     * @param $pid
     * @return int
     */
    private function updateCategories($pid)
    {
        // clear categories
        $this->mCat->delete($pid);

        foreach ($_POST['categories'] as $k=>$cid) {
            $this->mCat->create(array(
                'pid' => $pid,
                'cid' => $cid
            ));
        }

        return 1;
    }

    /**
     *  update products options
     * @param $id
     * @return mixed
     */
    private function updateProductOptions($id)
    {
        return $this->mPo->update($id, $_POST['product_options']);
    }

    public function tree()
    {
        $parent_id = isset($_POST['id']) ? (int)$_POST['id'] : 0;
        $tree=array();

        $type_id = $this->ms->getTypeId('category');
        $r = $this->ms->tree($parent_id, $type_id);
        $i=0;
        if(!empty($r)){
            foreach ( $r as $row) {
                $tree[$i]['data'] = $row['name'] . ' #' . $row['id'] ;
                $tree[$i]['state'] = $row['isfolder'] ? 'closed' : '';

                //todo нада повіксати шлях

                $tree[$i]['attr'] = array(
                    'id' => $row['id'],
                    "rel"=> (($row['isfolder'])? 'folder':'file'),
                    "href"=> (($row['isfolder'])? 'categories/index' : 'products/index') ."/" . $row['id'] ."/"
                );
                $i++;
            }
        }

        return json_encode($tree);
    }
    /**
     * toggle status by column
     * @param $col
     * @param $id
     * @param $status
     * @return mixed
     */
    public function toggleStatus($col, $id, $status)
    {
        return $this->mPo->toggleStatus($col, $id, $status == 1 ? 0 : 1);
    }

    /**
     * on table list change price
     * @param $id
     * @param $price
     * @return mixed
     */
    public function updatePrice($id, $price)
    {
        return $this->mPo->updatePrice($id, $price);
    }

    public function createMaunfacturers()
    {
        $languages    = $this->load->model('engine\Languages');

        return json_encode(array(
            't' => $this->lang->products['create_manufacturers'],
            'c' => $this->load->view(
                    'content/manufacturer_quick',
                    array(
                        'languages'  => $languages->all(1),
                        'autofil_title'       => Settings::instance()->get('autofil_title'),
                        'autotranslit_alias'  => Settings::instance()->get('autotranslit_alias')
                    )
                )
        ));
    }

    public function createManufacturersProcess()
    {

        $s=0; $v='';
        $e = $this->errors['warning'];
        $info = $_POST['info'];
        $name = ''; $i=0;
        foreach ($info as $languages_id=>$arr) {
            if($i == 0) $name = $info[$languages_id]['name'];
            $info[$languages_id]['title'] = $info[$languages_id]['name'];
            $info[$languages_id]['keywords'] = '';
            $info[$languages_id]['description'] = '';
            $info[$languages_id]['content'] = '';
            $info[$languages_id]['title'] = $info[$languages_id]['name'];
            if(empty($info[$languages_id]['name'])) {
                $this->error[] = $this->lang->manufacturers['e_name'];
            }
            $i++;
        }

        if(empty($this->error)) {
            $type_id = $this->ms->getTypeId('manufacturer');
            $templates_id = $this->ms->defaultTemplatesId('manufacturer');

            $owner_id     = Users::adminInfo('id');

            $data = array(
                'type_id'      => $type_id,
                'templates_id' => $templates_id,
                'owner_id'     => $owner_id,
                'auto'         => 0,
                'published'    => 1,
                'createdon'    => date('Y-m-d H:i:s')
            );

            $s = $this->ms->create($data, $info);
            if($s > 0) {
                $e = $this->errors['success'];
                $v = "<option value='{$s}' selected>{$name}</option>";
                $this->error[] = $this->lang->manufacturers['save_success'];
            } else {
                $e = $this->errors['error'];
                $this->error[] = $this->ms->error() ;
            }
        }

        return json_encode(array(
            's' => $s > 0 , // status
            'e' => $e, // error class
            't' => $this->lang->core[$e], // error title
            'm' => implode('<br>', $this->error), // error message,
            'v' => $v
        ));
    }

    /**
     * видалення обраних товарів
     * @return int
     */
    public function deleteGroup()
    {
        if(!isset($_POST['items'])){
            return 0;
        }

        foreach ($_POST['items'] as $k=>$id) {
            $this->delete($id);
        }


        return 1;
    }

    /**
     * зміна публікації обраних товарів
     * @return int
     */
    public function pubGroup()
    {
        if(!isset($_POST['items'])){
            return 0;
        }
        $status = $_POST['status'] == 1 ? 0 : 1;

        foreach ($_POST['items'] as $k=>$id) {
            $this->pub($id, $status);
        }

        return 1;
    }

    /**
     * return controller content
     * @return string
     */
    protected function output()
    {
        $this->setSidebar(
            new Tree(
                'content',
                'products/tree/',
                Tree::button($this->lang->categories['tree_add'], 'self.location.href="./categories/create";', 'icon-plus-sign'),
                array(
                    Tree::contextMenu($this->lang->categories['tree_cmenu_add'], 'icon-plus' ,'self.location.href="./categories/create/"+id;'),
                    Tree::contextMenu($this->lang->categories['tree_cmenu_edit'], 'icon-pencil' ,'self.location.href="./categories/edit/"+id;'),
                    Tree::contextMenu($this->lang->categories['tree_cmenu_del'], 'icon-remove' ,'engine.categories.delete(id)'),
                    Tree::contextMenu($this->lang->categories['tree_cmenu_add_product'], 'icon-plus' ,'self.location.href="./products/create/"+id;'),
                    Tree::contextMenu($this->lang->categories['tree_cmenu_list_product'], 'icon-list' ,'self.location.href="./products/index/"+id;'),
                ),
                true
            )
        );

        return parent::output();
    }
}
