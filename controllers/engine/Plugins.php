<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 31.05.14 18:36
 */
namespace controllers\engine;

use controllers\Engine;

defined('SYSPATH') or die();

class Plugins extends Engine implements Plugin{
    /**
     * path to plugins
     * @var
     */
    private $cpath = '/controllers/engine/plugins/';

    private $ns;
    /**
     * model instance
     * @var
     */
    private $model;

    public function __construct()
    {
        parent::__construct();

        $this->model = $this->load->model('engine\Plugins');

        $this->ns = str_replace('/', '\\', $this->cpath);
    }
    /**
     * @return mixed|string
     */
    public function index()
    {
//        $this->setButtons
//        (
//            Form::button
//            (
//                $this->lang->plugins['install'],
//                Form::icon('icon-archive'),
//                array(
//                    'class'   => Form::BTN_TYPE_PRIMARY,
//                    'onclick' => "self.location.href='./plugins/install'"
//                )
//            )
//        );

        $t = new Table('plugins', 'plugins/items');

        $t ->setTitle($this->lang->plugins['table_title']);
        $t ->addTh($this->lang->plugins['name']);
        $t ->addTh($this->lang->plugins['author']);
        $t ->addTh($this->lang->plugins['version']);
        $t ->addTh($this->lang->core['func'] , 'w-120');
        ;
        $this->setContent($t->render());

        return $this->output();
    }

    /**
     * plugins list
     * @return string
     */
    public function items()
    {
       $dt = new Table();
       $plugins = $this->listPlugins();
       return $dt->renderJSONData($plugins);
    }

    /**
     * return plugins list
     * @return array
     */
    private function listPlugins()
    {
        $plugins = array();
        if ($handle = opendir(DOCROOT . $this->cpath)) {
            while (false !== ($entry = readdir($handle))) {
                if ($entry != "." && $entry != "..") {

                    $plugin = str_replace('.php', '', $entry);

                    $row = $this->readPhpDoc($plugin);

                    $row['func'] = "";

                    if($id=$this->model->isInstalled($this->ns.$plugin)){
                        $row['func'] .= "<button class='btn btn-primary' onclick=\"engine.plugins.edit('{$id}')\"><i class=\"icon-edit icon-white\"></i></button>";
                        $row['func'] .= "<button class='btn btn-danger uninstall-plugin' onclick=\"engine.plugins.uninstall('{$id}')\"><i class=\"icon-off icon-white\"></i></button>";
                    } else {
                        $row['func'] .= "<button class='btn btn-success' onclick=\"engine.plugins.install('{$plugin}')\"><i class=\"icon-off icon-white\"></i></button>";
                    }


                    $row['name']    = "<b>{$row['name']}</b>";
                    $row['name']   .= "<br>" . $row['description'];
                    $row['name']   .= "<br>package: " . $row['package'];
                    $row['author'] .= "<br>" . $row['copyright'];

                    unset($row['description']);
                    unset($row['package']);
                    unset($row['copyright']);

                    $plugins[] = $row;
                }
            }
            closedir($handle);
        }

        return $plugins;
    }

    /**
     * @param $plugin
     * @return array
     */
    private function readPhpDoc($plugin)
    {
        $row = array();
        $rc = new \ReflectionClass($this->ns . $plugin);
        $dc = $rc->getDocComment();

        //Get the comment
        if(preg_match('#^/\*\*(.*)\*/#s', $dc, $comment) === false){
            $row['name'] = 'Невірно внесено DocComment';
            return $row;
        }
        $comment = trim($comment[1]);
        if(preg_match_all('#^\s*\*(.*)#m', $comment, $lines) === false){
            $row['name'] = 'Невірно внесено DocComment';
            return $row;
        }
        foreach ($lines[1] as $line) {
            $line = trim($line);

            if(empty($line)) continue;

            if(strpos($line, '@') === 0) {
                $param = substr($line, 1, strpos($line, ' ') - 1); //Get the parameter name
                $value = substr($line, strlen($param) + 2); //Get the value
                $row[$param] = $value;
            }
        }

        return $row;
    }

    /**
     * інсталяція плагіну
     * @return string
     */
    public function install()
    {
        if(!isset($_POST['plugin'])) return '';

        $plugin = $_POST['plugin'];

        if(isset($_POST['action']) && $_POST['action'] == 'install'){
            return $this->processInstall();
        }

        return $this->load->view(
            'plugin_form',
            array(
                'action' => 'install',
                'plugin' => $plugin,
                'structure' => $this->model->getStructure(),
                'position'  => $this->getPosition()
            )
        );
    }

    public function edit($id)
    {
        if($this->request->isPost()){
           return $this->process($id);
        }

        $data = $this->model->getData($id);

        return $this->load->view(
            'plugin_form',
            array(
                'action' => 'edit',
                'id' => $id,
                'structure' => $this->model->getStructure($id),
                'position'  => $this->getPosition($data['position'])
            )
        );
    }

    /**
     * @return string
     */
    private function processInstall()
    {
        if(!isset($_POST['plugin'])) return '';
        $m=''; $c= null; $id=0;
        $plugin = $_POST['plugin'];

        $data = $_POST['data'];
        $structure = $_POST['structure'];
        $data['controller'] = $this->ns . $plugin;

        try{
            $c = new $data['controller'];
        } catch(\Exception $e){
            $m = $e->getCode() . $e->getMessage();
        }
        if($c != null && $c->install()){
            $id = $this->model->install($data, $structure);
        }


        return json_encode(
            array(
                's' => $id > 0,
                'm' => $m
            )
        );
    }

    /**
     * @param $selected
     * @return array
     */
    private function getPosition($selected='')
    {
        $r = array();
        $res = $this->model->getPosition();
        foreach ($res as $row) {
            $r[] = array(
                'id' => $row,
                'name' => isset($this->lang->plugins['pos_' . $row]) ? $this->lang->plugins['pos_' . $row] : $row,
                'selected' => $selected == $row ? 'selected' : ''
            );
        }

        return $r;
    }

    /**
     * uninstall plugin
     * @return mixed
     */
    public function uninstall()
    {
        $id = (int) $_POST['id'];
        $plugin = $this->model->getData($id, 'controller');
        $c = new $plugin;
        if($c->uninstall()){
            return $this->model->uninstall($id);
        }

        return 0;
    }

    public function create(){}

    public function delete($id){}

    public function process($id)
    {
        if(empty($_POST['structure'])) return '';

        $s = $this->model->updateData($id, $_POST['data'], $_POST['structure']);
        return json_encode(
            array(
                's' => $s,
                'm' => ''
            )
        );
    }

    /**
     * @param $id
     * @param $action
     * @return string
     */
    public function get($id, $action)
    {
        $ns = $this->request->namespace;
        $c = $this->request->controller;
        $s = str_replace('\\','/',$ns.$c);
        $plugins = $this->model->getStructurePlugins($s);

        $out = array();

        foreach ($plugins as $row) {
            $p = new $row['controller'];
            $out[$row['position']][] = $p->$action($id);
        }

        return $out;
    }

    protected function executeSQL($sql)
    {
        return $this->model->execute($sql);
    }
}
