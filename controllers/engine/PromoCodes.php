<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 05.07.14 20:38
 */

namespace controllers\engine;

use controllers\Engine;

defined('SYSPATH') or die();

class PromoCodes extends Engine {

    public function __construct()
    {
        parent::__construct();

        $this->mg = $this->load->model('engine\PromoCodes');
    }

    /**
     * @return mixed|string
     */
    public function index()
    {
        $buttons= array(
            Form::button(
                $this->lang->core['create'],
                Form::icon('icon-file'),
                array(
                    'class'   => Form::BTN_TYPE_PRIMARY,
                    'onclick' => "engine.promoCodes.create()"
                ))
        );

        $t = new Table('PromoCodes', "PromoCodes/items");
        $t  ->setTitle($this->lang->promo_codes['table_title'])
            ->setConf('sortable', true)
            ->setConf('columns', array(
                'orderable'=> false
            ))
            ->sortableConf('promo_codes','id', 'id')
            ->addTh($this->lang->promo_codes['code'])
            ->addTh($this->lang->promo_codes['discount'])
            ->addTh($this->lang->promo_codes['expire'])
            ->addTh($this->lang->core['func'] , 'w-120')
        ;

        $this->setButtons($buttons);
        $this->setContent($t->render());

        return $this->output();
    }

    /**
     * @return string
     */
    public function items()
    {
        $dt = new Table();
        $dt
            -> table('promo_codes')
            -> columns(
                array(
                    'id',
                    'code',
                    'discount',
                    'type',
                    "DATE_FORMAT(expire, '%d.%m.%Y') as expire",
                    'single',
                    'minp'
                )
            )
            -> searchCol(array('id', 'code'));
        $currency = $this->mg->getDefaultCurrency();
        $rows = $dt->getRows();
//        $this->dump($rows);
        $out = array();
        foreach ($rows as $row) {

            $row['discount'] = $row['discount'] .' '. ( $row['type'] == 'abs' ? $currency['symbol'] : '%');
            if($row['minp'] > 0){
                $row['discount'] .= "<br> <span class='label label-info'>{$this->lang->promo_codes['minp_from']} {$row['minp']} {$currency['symbol']}</span>";
            }

            $expire = $row['expire'] == '00.00.0000' ? $this->lang->promo_codes['expire_empty'] : $row['expire'];
            if($row['single']){
                $expire .= "<br><span class='label label-info'>{$this->lang->promo_codes['single']}</span>";
            }

            $out[] = array(
                $row['id'],
                "<a href='javascript:void(0)' onclick='engine.promoCodes.edit({$row['id']})'>{$row['code']}</a>",
                $row['discount'],
                $expire,
                Form::link(
                    '',
                    Form::icon('icon-edit'),
                    array(
                        'class'  =>'btn-info',
                        'title' => $this->lang->core['edit'],
                        'onclick'  => "engine.promoCodes.edit({$row['id']})"
                    )
                ) .
                Form::button(
                    '',
                    Form::icon('icon-remove'),
                    array(
                        'title'   => $this->lang->core['delete'],
                        'class'   =>'btn-danger',
                        'onclick'  => "engine.promoCodes.del({$row['id']})"
                    )
                )
            );
        }
        $dt->beforeRender($out);

        return $dt-> request();
    }

    /**
     * @return mixed|string
     */
    public function create()
    {
        $type= array(); $currency = $this->mg->getDefaultCurrency();
        foreach ($this->mg->getType() as $k=>$t) {
            if($t == 'abs') {
                $name = $currency['symbol'];
            } else {
                $name = '%';
            }
            $type[] = array(
                'id'    => $t,
                'name' => $name,
                'selected' => ''
            );
        }

        return $this->load->view('promo_codes',array(
            'action'   => 'create',
            'id'       => 0,
            'type'     => $type
        ));
    }

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $type= array(); $currency = $this->mg->getDefaultCurrency();
        $data = $this->mg->getData($id);
        foreach ($this->mg->getType() as $k=>$t) {
            if($t == 'abs') {
                $name = $currency['symbol'];
            } else {
                $name = '%';
            }
            $type[] = array(
                'id'    => $t,
                'name' => $name,
                'selected' => $data['type'] == $t ? 'selected' : ''
            );
        }

        if($data['expire'] == '0000-00-00') $data['expire'] = '';
        return $this->load->view('promo_codes',array(
            'action' => 'edit',
            'id'     => $id,
            'type'   => $type,
            'data'   => $data
        ));
    }

    /**
     * @param $id
     * @return mixed|string
     */
    public function process($id=0)
    {
        $s=0;
        $e = $this->errors['warning'];
        $data = $_POST['data'];

        // check required fields
        if(
            empty($data['code']) ||
            empty($data['discount'])
        ) {
            $this->error[] = $this->lang->core['e_required_fields'];
        }
        // add or update values
        if(empty($this->error)) {
            switch($_POST['action']){
                case 'create':
                    $id = $this->mg->create($data);
                    if($id > 0) {
                        $s = $id;
                        $e = $this->errors['success'];
                        $this->error[] = $this->lang->promo_codes['save_success'];
                    } else {
                        $e = $this->errors['error'];
                        $this->error[] = $this->mg->error();
                    }
                    break;
                case 'edit':
                    if(empty($id)) return '';
                    $r='';
                    $s = $this->mg->update($id, $data);
                    if($s > 0) {
                        $e = $this->errors['success'];
                        $this->error[] = $this->lang->promo_codes['save_success'];
                    } else {
                        $e = $this->errors['error'];
                        $this->error[] = $this->mg->error() ;
                    }
                    break;
                default:
                    break;
            }
        }

        return json_encode(array(
            's' => $s > 0 , // status
            'r' => '', // redirect url
            'e' => $e, // error class
            't' => $this->lang->core[$e], // error title
            'm' => implode('<br>', $this->error) // error message
        ));
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        return $this->mg->delete($id);
    }
} 