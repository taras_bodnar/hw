<?php
/**
 * Company Otakoyi.com
 * Author: wmgodyak mailto:wmgodyak@gmail.com
 * Date: 27.12.14 21:43
 */

namespace controllers\engine;

defined("SYSPATH") or die();

interface Component {

    /**
     * call function when install component
     * @return mixed
     */
    public function install();

    /**
     * call function when uninstall component
     * @return mixed
     */
    public function uninstall();
}
