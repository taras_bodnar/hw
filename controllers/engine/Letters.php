<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 01.09.14 22:23
 */
namespace controllers\engine;

use controllers\core\Settings;
use controllers\core\Template;
use controllers\Engine;
use models\app\Guides;
use models\modules\Users;

defined("SYSPATH") or die();

/**
 * Генератор меню навігації для фронтенду
 * Class Letters
 * @package controllers\engine
 */
class Letters extends Engine
{
    private $mg;

    public function __construct()
    {
        parent::__construct();

        $this->mg = new \models\engine\Letters();

        $this->languages = $this->load->model('engine\Languages');
    }

    public function install()
    {
        return 1;
    }

    public function uninstall()
    {
        return 1;
    }


    /**
     * @return mixed|string
     */
    public function index()
    {
        $buttons = array();

        $buttons = array(
            Form::button(
                $this->lang->core['create'],
                Form::icon('icon-file'),
                array(
                    'class' => Form::BTN_TYPE_PRIMARY,
                    'onclick' => "self.location.href='./Letters/create'"
                ))
        );

        $t = new DataTables();
        $t  -> setId('Letters')
            -> setTitle($this->lang->letters['table_title'])
            -> th($this->lang->letters['id'], 'w-20')
            -> th($this->lang->letters['name'], 'w-50')
            -> th($this->lang->letters['status'])
//            -> th($this->lang->letters['aresend'])
//            -> th($this->lang->letters['total'])
            -> th($this->lang->core['func'] , 'w-180')
            -> ajaxConfig('Letters/items');

        $this->setButtons($buttons);
        $this->setContent($this->getProgressView().$t->render());

        return $this->output();
    }

    /**
     * @return string
     */
    public function items()
    {
        $t = new DataTables();
        $t  -> table('letters l')
//            ->debug(true)
            -> get('l.id, l.name, if(l.status=1,"Надіслано","Не надіслано") as status')
//            ->join("  left join letters_emails le on le.letters_id=l.id and le.status=1")
//            ->join(" left join letters_emails le1 on le1.letters_id=l.id and le1.status=0")
            -> orderBy("l.id")
            -> execute();

        $res = array();
        foreach ($t->getResults(false) as $i=>$row) {
            if(empty($row['id'])) continue;
            $res[$i][] = $row['id'];
            $res[$i][] = $row['name'];
            $res[$i][] = $row['status'];
//            $res[$i][] = $row['count'];
//            $res[$i][] = $row['count']+$row['total'];
            $res[$i][] =
                Form::link(
                    '',
                    Form::icon('icon-edit'),
                    array(
                        'class' => 'btn-info',
                        'title' => $this->lang->core['edit'],
                        'href' => 'Letters/edit/'.$row['id']
                    )
                ) .
                Form::button(
                    '',
                    Form::icon('icon-remove'),
                    array(
                        'title' => $this->lang->core['delete'],
                        'class' => 'btn-danger',
                        'onclick' => 'engine.letters.delete('.$row['id'].')'
                    )
                ) .
                Form::button(
                    '',
                    Form::icon('icon-envelope-alt'),
                    array(
                        'title' => $this->lang->letters['send'],
                        'class' => 'btn-info',
                        'onclick' => 'engine.letters.askBeforeSend('.$row['id'].')'
                    )
                );
        }

        return $t->renderJSON($res, $t->getTotal());
    }

    private function getProgressView()
    {
        return $this->load->view("progressbar");
    }

    private function totalEmailForSend($id)
    {
        return $this->mg->getTotalEmail($id);
    }

    public function getInforSend($id)
    {
        return json_encode(
            array(
                'total' => $this->totalEmailForSend($id),
                'start' => 0
            )
        );
    }

    public function sendProccess()
    {
        if (isset($_POST['updatels']) && $_POST['updatels'] == 1) {
            $lid = $_POST['lid'];
            $this->mg->updateLstatus($lid);
        }

        $lid = $_POST['lid'];
        $start = $_POST['start'];

        $item = $this->mg->getEmailSend($lid, $start);

        include_once DOCROOT . "vendor/phpmailer/PHPMailer.php";
        include_once DOCROOT . "vendor/phpmailer/SMTP.php";

        $mt = $this->load->model('app\Mailer');
        $mail = new \PHPMailer;
        $data = $this->mg->getData($lid);
        $ui = (new Users())->getInfoByEmail($item['name']);

        if(!empty($ui['languages_id']) && $ui['languages_id']==2) {
            $data['content'] = $data['content_ru'];
        }

        $fromTo = explode('/', $data['fromto']);

        $error = array();
        $s = 0;

        if (!\PHPMailer::validateAddress($item['name'])) {
            return 1;
        } else {
            $mail->CharSet = "UTF-8";
            $mail->setFrom('no-reply@hotwed.com.ua','HotWed');


            $mail->IsSMTP();
//            $mail->SMTPDebug = 2;
            $mail->SMTPSecure = 'ssl';
//            $mail->Host       = 'smtp.zoho.com';
            $mail->Host       = 'smtp.gmail.com';
            $mail->Port       = 465;

            $mail->Username   = 'no-reply@hotwed.com.ua';
//            $mail->Username   = 'no-reply@hotwed.com.ua';
            $mail->Password   = 'HotWed_2018';
            $mail->SMTPAuth   = true;

            $mail->addAddress($item['name']);

            $mail->isHTML(true);

            $header = Settings::instance()->get('et_header');
            $footer = Settings::instance()->get('et_footer');
            $mail->Subject = $data['themes'];
            $re = '/src\s*=\s*"(.+?)"/';
            $str = $data['content'];

            preg_match_all($re, $str, $matches, PREG_SET_ORDER, 0);

            foreach ($matches as $match) {
                $path = strpos($match[1],$_SERVER['SERVER_NAME'])>0?$match[1]:APPURL.$match[1];
                $replace = 'src="'.$path.'"';
                $data['content'] = str_replace($match[0],$replace,$data['content']);
            }

            $data['content'] = preg_replace_callback(
                '({[a-z-_]+})',
                function ($matches) use ($ui) {
                    $matches[0] = str_replace(array('{','}'), array(), $matches[0]);
                    return isset($ui[$matches[0]]) ? $ui[$matches[0]] : $matches[0];
                },
                $data['content']
            );
            
            $data['content'] = $header.$data['content'].$footer;

            $mail->Body = Template::instance()->fetch('string:' . $data['content']);


            if (!$mail->send()) {
                $error[] = $mail->ErrorInfo;
            } else {
                $this->mg->updateSendStatus($item['id']);
                // $error[] = $this->translation['feedback_success'];
            }
        }

        return 1;

    }

    /**
     * @return mixed|string
     */
    public function create()
    {
        $buttons = array(
            Form::button(
                $this->lang->core['back'],
                Form::icon('icon-external-link'),
                array(
                    'class' => 'btn-link',
                    'onclick' => 'self.location.href=\'./Letters/index\''
                )
            ),
            Form::button(
                $this->lang->core['save'],
                Form::icon('icon-save', false),
                array(
                    'class' => 'btn-success',
                    'form' => "form"
                ))
        );

        $clientGroup = $this->mg->getClientGroup();

        $projects = $this->mg->getProjects();

        $cities = (new Guides())->get(9);
        foreach ($cities as $k=>$city) {
            $cities[$k]['value'] = $city['id'];
            $cities[$k]['id'] = $city['value'];
        }

        foreach ($projects as $k=>$project) {
            $projects[$k]['data']['data-type'] = $project['type'];
        }

        $clientGroup = array_merge($clientGroup,$projects);

        $subscription = array(
            'id' => 9999,
            'name' => 'Підписники',
            'type' => 'subscribe',
            'data' => array(
                'data-type' => 'subscribe'
            )
        );

        $clientGroup[count($clientGroup)] = $subscription;

        $content = $this->load->view('letters', array(
            'action' => 'create',
            'id' => 0,
            //'emails' => $this->mg->getEmails(),
            'client_group' => $clientGroup,
            'cities' => $cities,
//            'languages' => $this->languages->all(1)
        ));

        $this->setButtons($buttons);
        $this->setContent($content);

        return $this->output();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $buttons = array(
            Form::button(
                $this->lang->core['back'],
                Form::icon('icon-external-link'),
                array(
                    'class' => 'btn-link',
                    'onclick' => 'self.location.href=\'Letters/index\''
                )
            ),
            Form::button(
                $this->lang->core['save'],
                Form::icon('icon-save', false),
                array(
                    'class' => 'btn-success form-submit'
                ))
        );
        $data = $this->mg->getData($id);
        $emails = $this->mg->getEmailEdit($id);
        $out = "";
        foreach ($emails as $item) {
            $out .= $item['name'] . "\n";
        }

        $clientGroup = $this->mg->getClientGroup();

        $projects = $this->mg->getProjects();

        $cities = (new Guides())->get(9);

        foreach ($cities as $k=>$city) {
            $cities[$k]['value'] = $city['id'];
            $cities[$k]['id'] = $city['value'];
        }

        foreach ($projects as $k=>$project) {
            $projects[$k]['data']['data-type'] = $project['type'];
        }

        $clientGroup = array_merge($clientGroup,$projects);

        $subscription = array(
            'id' => 9999,
            'name' => 'Підписники',
            'type' => 'subscribe',
            'data' => array(
                'data-type' => 'subscribe'
            )
        );

        $clientGroup[count($clientGroup)] = $subscription;

        $content = $this->load->view('letters', array(
            'action' => 'edit',
            'data' => $data,
            'id' => $id,
            'emails' => $out,
            'cities' => $cities,
            'client_group' => $clientGroup
        ));

        // output
        $this->setButtons($buttons);
        $this->setContent($content);

        return $this->output();
    }

    public function filter()
    {
        if(!$this->request->isXhr()) return false;

        $clientGroup = $this->request->post('client_group');
        $cities = $this->request->post('cities');
        $filter = $this->request->post('filter');

        if(!empty($clientGroup)) {
            $clientIn = implode(',',$clientGroup);
            $this->mg->setWhere(" and users_group_id in ({$clientIn})");
        }

        if(!empty($cities)) {
            $citiesIn = implode(',',$cities);
            $this->mg->setJoin("join users_cities c on c.users_id=u.id");
            $this->mg->setWhere(" and c.guides_id in ({$citiesIn})");
        }

        if(!empty($filter['last_log_date'])) {
            $this->mg->setWhere("and UNIX_TIMESTAMP(last_login)<UNIX_TIMESTAMP('{$filter['last_log_date']}')");
        }

        if(!empty($filter['last_update'])) {
            $this->mg->setJoin(" join users_uploads uu on uu.id = (select id from users_uploads where users_id=u.id order by id desc limit 1)");
            $this->mg->setWhere("and UNIX_TIMESTAMP(uu.createdon)<=UNIX_TIMESTAMP('{$filter['last_update']}')");
        }

        if(!empty($filter['completeness'])) {
            $this->mg->setJoin(" join users_uploads uu on uu.users_id = u.id ");
            $this->mg->setHaving(" count(uu.id)<{$filter['completeness']}");
        }

        $emails = $this->mg->getEmails();
        $out = "";

        foreach ($emails as $item) {
            $out .= $item['name'] . "\n";
        }

        return json_encode(
            array(
                'emails' => $out
            )
        );
    }

    /**
     * @param $id
     * @return mixed|string
     */
    public function process($id = 0)
    {
        $s = 0;
        $e = $this->errors['warning'];
        $r = './Letters/index'; // redirect url
        $data = $_POST['data'];
        $emails = explode("\n", $_POST['emails']);

        if (
        empty($data['name'])
        ) {
            $this->error[] = $this->lang->core['e_required_fields'];
        }
        // add or update values
        if (empty($this->error)) {
            switch ($_POST['action']) {
                case 'create':
                    $id = $this->mg->create($data, $emails);
                    if ($id > 0) {
                        $s = $id;
                        $e = $this->errors['success'];
                        $this->error[] = $this->lang->letters['save_success'];
                    } else {
                        $e = $this->errors['error'];
                        $this->error[] = $this->mg->error();
                    }
                    break;
                case 'edit':
                    if (empty($id)) return '';
                    $r = '';
                    $s = $this->mg->update($id, $data, $emails);
                    if ($s > 0) {
                        $e = $this->errors['success'];
                        $this->error[] = $this->lang->letters['save_success'];
                    } else {
                        $e = $this->errors['error'];
                        $this->error[] = $this->mg->error();
                    }
                    break;
                default:
                    break;
            }
        }

        return json_encode(array(
            's' => $s > 0, // status
            'r' => $r, // redirect url
            'e' => $e, // error class
            't' => $this->lang->core[$e], // error title
            'm' => implode('<br>', $this->error) // error message
        ));
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        return $this->mg->delete($id);
    }

    public function getEmails()
    {
        $client_group = $_POST['cg'];
        $type = $_POST['type'];

        $emails = $_POST['emails'];
        $out = "";
        $out .= $emails;
        $cui = implode(',', $_POST['cg']);
        if (empty($client_group)) return;
        if($type=='projects') {
            $email = $this->mg->getEmailsByProjectsID($cui);
        } elseif($type=='subscribe') {
            $email = $this->mg->getSubscribes($cui);
        } else {
            $email = $this->mg->getEmailsByGroupID($cui);
        }



        if(!empty($emails)) {
            $emails = explode("\n",$emails);
            if(!empty($email)) {
                foreach($email as $k=>$item) {
                    if(empty($email)) continue;
                    if(in_array($item['name'],$emails)) {
                        unset($email[$k]);
                    }
                }
            }
        }

        foreach ($email as $item) {
            $out .= $item['name'] . "\n";
        }

        return json_encode(
            array(
                'emails' => $out
            )
        );
    }

} 