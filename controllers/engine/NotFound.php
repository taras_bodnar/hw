<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 11.11.14 13:22
 */

namespace controllers\engine;

use controllers\Engine;
use controllers\core\Request;

defined("SYSPATH") or die();

class NotFound extends Engine{

    public function index()
    {
        Request::instance()->title = '404 Not Found';
        return $this->load->view('404');
    }


    public function create(){}
    public function process($id){}
    public function edit($id){}
    public function delete($id){}
}