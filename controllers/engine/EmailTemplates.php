<?php
/**
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 01.09.14 22:23
 */
namespace controllers\engine;
 use controllers\Engine;

 defined("SYSPATH") or die();

 /**
  * Генератор меню навігації для фронтенду
  * Class EmailTemplates
  * @package controllers\engine
  */
class EmailTemplates extends Engine{
//    private $languages;

    public function __construct()
    {
        parent::__construct();

        $this->mg = $this->load->model('engine\EmailTemplates');

        $this->languages = $this->load->model('engine\Languages');
    }

     public function install(){}
     public function uninstall(){}

    /**
     * @return mixed|string
     */
    public function index()
    {
        $buttons= array(
            Form::button(
                $this->lang->core['create'],
                Form::icon('icon-file'),
                array(
                    'class'   => Form::BTN_TYPE_PRIMARY,
                    'onclick' => "self.location.href='./EmailTemplates/create'"
                ))
        );

        $t = new Table('EmailTemplates', "EmailTemplates/items");
        $t  ->setTitle($this->lang->EmailTemplates['table_title'])
            -> sortableConf('email_templates','id','id')
            ->addTh($this->lang->core['id'], 'w-20')
            ->addTh($this->lang->EmailTemplates['name'])
            ->addTh($this->lang->EmailTemplates['code'])
            ->addTh($this->lang->core['func'] , 'w-140')
        ;

        // render content view

        $this->setButtons($buttons);
        $this->setContent($t->render());

        return $this->output();
    }

    /**
     * @return string
     */
    public function items()
    {
        $dt = new Table();
        return $dt
            -> table('email_templates')
            -> columns(
                array(
                    'id',
                    'name',
                    'code'
                )
            )
            -> searchCol(array('id', 'name', 'code'))
            -> returnCol(array('id', 'name', 'code', 'func'))
            -> formatCol(
                array(
                    1 => "<a href='EmailTemplates/edit/{id}'>{name}</a>",
                    3 =>
                        Form::link(
                            '',
                            Form::icon('icon-edit'),
                            array(
                                'class'  =>'btn-info',
                                'title' => $this->lang->core['edit'],
                                'href'  => 'EmailTemplates/edit/{id}'
                            )
                        ) .
                        Form::button(
                            '',
                            Form::icon('icon-remove'),
                            array(
                                'title'   => $this->lang->core['delete'],
                                'class'   =>'btn-danger',
                                'onclick' => 'engine.emailTemplates.delete({id})'
                            )
                        )
                ))
            -> request();
    }

    /**
     * @return mixed|string
     */
    public function create()
    {
        $buttons= array(
            Form::button(
                $this->lang->core['back'],
                Form::icon('icon-external-link'),
                array(
                    'class'=>'btn-link',
                    'onclick' => 'self.location.href=\'./EmailTemplates/index\''
                )
            ),
            Form::button(
                $this->lang->core['save'],
                Form::icon('icon-save', false),
                array(
                    'class'=>'btn-success form-submit'
                ))
        );


        $content = $this->load->view('email_templates',array(
            'action'  => 'create',
            'id'      => 0,
            'languages' => $this->languages->all(1)
        ));

        $this->setButtons($buttons);
        $this->setContent($content);

        return $this->output();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $buttons= array(
            Form::button(
                $this->lang->core['back'],
                Form::icon('icon-external-link'),
                array(
                    'class'=>'btn-link',
                    'onclick' => 'self.location.href=\'EmailTemplates/index\''
                )
            ),
            Form::button(
                $this->lang->core['save'],
                Form::icon('icon-save', false),
                array(
                    'class'=>'btn-success form-submit'
                ))
        );
        $data = $this->mg->data('email_templates', $id);

        $content = $this->load->view('email_templates',array(
            'action'     => 'edit',
            'data'       => $data,
            'id'         => $id,
            'languages' => $this->languages->all(1),
            'info'       => $this->mg->info($id),
        ));

        // output
        $this->setButtons($buttons);
        $this->setContent($content);

        return $this->output();
    }

    /**
     * @param $id
     * @return mixed|string
     */
    public function process($id=0)
    {
        $s=0;
        $e = $this->errors['warning'];
        $r = './EmailTemplates/index'; // redirect url
        $data = $_POST['data'];
        $info = $_POST['info'];

        if(
            empty($data['name'])
        ) {
            $this->error[] = $this->lang->core['e_required_fields'];
        }
        // add or update values
        if(empty($this->error)) {
            switch($_POST['action']){
                case 'create':
                    $id = $this->mg->create($data, $info);
                    if($id > 0) {
                        $s = $id;
                        $e = $this->errors['success'];
                        $this->error[] = $this->lang->EmailTemplates['save_success'];
                    } else {
                        $e = $this->errors['error'];
                        $this->error[] = $this->mg->error();
                    }
                    break;
                case 'edit':
                    if(empty($id)) return '';
                    $r='';
                    $s = $this->mg->update($id, $data, $info);
                    if($s > 0) {
                        $e = $this->errors['success'];
                        $this->error[] = $this->lang->EmailTemplates['save_success'];
                    } else {
                        $e = $this->errors['error'];
                        $this->error[] = $this->mg->error() ;
                    }
                    break;
                default:
                    break;
            }
        }

        return json_encode(array(
            's' => $s > 0 , // status
            'r' => $r, // redirect url
            'e' => $e, // error class
            't' => $this->lang->core[$e], // error title
            'm' => implode('<br>', $this->error) // error message
        ));
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        return $this->mg->delete($id);
    }

} 