<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 26.06.14 12:58
 */

namespace controllers\engine;

use controllers\Engine;

defined('SYSPATH') or die();

class Languages extends Engine {
    public function __construct()
    {
        parent::__construct();

        $this->mg = $this->load->model('engine\Languages');
    }



    /**
     * @return mixed
     */
    public function index()
    {
        $buttons= array(
            Form::button(
                $this->lang->core['create'],
                Form::icon('icon-file'),
                array(
                    'class'   => Form::BTN_TYPE_PRIMARY,
                    'onclick' => "self.location.href='./languages/create'"
                ))
        );

        $t = new Table('languages', "languages/items");
        $t
            ->setTitle($this->lang->languages['table_title'])
            ->sortableConf('languages','id','id')
            ->addTh($this->lang->languages['id'])
            ->addTh($this->lang->languages['name'])
            ->addTh($this->lang->languages['code'])
            ->addTh($this->lang->languages['front'])
            ->addTh($this->lang->languages['back'])
            ->addTh($this->lang->languages['front_default'])
            ->addTh($this->lang->languages['back_default'])
            ->addTh($this->lang->core['func'] , 'w-160')
        ;

        $this->setButtons($buttons);
        $this->setContent(
            $this->notification() .
            $t->render()
        );

        return $this->output();
    }

    private function notification()
    {
        $l = $this->getNotTranslatedLanguage();

        if(empty($l)) return '';

        $info_tables = $this->getInfoTables();

        return $this->load->view(
            'languages/notifications',
            array(
                'languages' => $l,
                'tables' => $info_tables
            )
        );
    }

    public function processTranslate()
    {
        if(!isset($_POST['table'])) return 0;

        $languages_id = (int) $_POST['languages_id'];
        $res= array();
        if(empty($languages_id)) return 0;
        $def_lang_id = $this->mg->getFrontendDefault('id');
        foreach ($_POST['table'] as $table) {
            $col = array();
            $tbl_info = $this->mg->describe($table);
            // формую масив полів
            foreach ($tbl_info as $row) {
                $iv = array();
                $iv['translate'] = 0;
                if($row['Extra'] == 'auto_increment') continue;
                $table_fields[$table]['iv'][] = $row['Field'];

                if(preg_match('/varchar|text|longtext/i',$row['Type'])){
                    $table_fields[$table]['to_tranlate'][] = $row['Field'];
                    $iv['translate'] = 1;
                }
                $iv['field'] = $row['Field'];
                $col[] = $iv;
            }

            $total = $this->mg->getTotalTableRecords($table, $def_lang_id);
            $res[] = array(
                'start' => 0,
                'total' => $total,
                'table' => $table,
                'col'   => $col,
                'from_lang' => $def_lang_id,
                'to_lang'   => $languages_id
            );
        }

        return json_encode(array('t' => $res));
    }

    private function getInfoTables()
    {
        $res = array();
        $r = $this->mg->getInfoTables();
        foreach ($r as $row) {
            foreach ($row as $n=>$table) {
                $res[] = $table;
            }

        }

        return $res;
    }

    public function translate()
    {
//        $this->dump($_POST);
        $t = new Translator();
        $table = $_POST['table'];
        $start = (int) $_POST['start'];
        $total = (int) $_POST['total'];
        $col = $_POST['col'];
        $from_lang = (int) $_POST['from_lang'];
        $to_lang   = (int) $_POST['to_lang'];

        $from = $this->mg->rowData($from_lang, 'code');
        $to   = $this->mg->rowData($to_lang, 'code');

        $iv = array();
        $i = time();
        $rowInfo = $this->mg->getTableRow($table, $from_lang, $start);
        foreach ($col as $field) {
            $value = $rowInfo[$field['field']];

            if($field['field'] == 'languages_id') {
                $value = $to_lang;
            }

            if($field['field'] == 'alias') {
                $value .=  $i;
            }
            if($field['translate'] == 1) {
                $value = $t->translate($value, $from, $to);
            }

            $iv[$field['field']] = $value;
            $i++;
        }

        $this->mg->insertTranslatedData($table, $iv);

//        $this->dump($iv);
        $start++;
        //, d.col, d.from_lang, d.to_lang
        return json_encode(
            array(
                'table' => $table,
                'start' => $start,
                'total' => $total,
                'col'   => $col,
                'from_lang' => $from_lang,
                'to_lang'   => $to_lang
            )
        );
    }

    /**
     * @return mixed
     */
    private function getNotTranslatedLanguage()
    {
        $l = $this->mg->getNotTranslatedLanguage();

        foreach ($l as $k=> $row) {
            if(!empty($row['c_name'])) { unset($l[$k]);}
            unset($l[$k]['c_name']);
        }

        return $l;
    }

    public function generateAlias($lang)
    {
        $total = $this->mg->getTotalTableRecords('content_info', $lang);
        return $this->load->view(
            'languages/generate_alias',
            array(
                'lng'  => $lang,
                'total' => $total
            )
        );
    }

    public function processGenerateAlias()
    {
        $total = (int) $_POST['total'];
        $start = (int) $_POST['start'];
        $languages_id = (int) $_POST['languages_id'];
        $code = $this->mg->rowData($languages_id, 'code');
        $c = new Content();
        $row = $this->mg->getAliasInfo($languages_id,$start);

        $alias = '';
        if($row['content_id'] != 1){
           $alias = $c->mkAlias($row['parent_id'], $languages_id, $code, $row['name']);
        }

        $this->mg->updateAlias($row['id'], $alias);

        $start ++ ;

        return json_encode(array(
            'total' => $total,
            'start' => $start
        ));
    }

    /**
     * table languages group items
     * @return string
     */
    public function items()
    {
        $yes = $this->lang->core['btn_yes'];
        $no = $this->lang->core['btn_no'];
        $dt = new Table();
        return $dt
            -> table('languages u')
            -> columns(
                array(
                    'id',
                    'name',
                    'code',
                    "IF(front=1,'$yes', '$no') as front",
                    "IF(back=1,'$yes', '$no') as back",
                    "IF(front_default=1,'$yes', '$no') as front_default",
                    "IF(back_default=1,'$yes', '$no') as back_default"
                )
            )
            -> searchCol(array('id','name','code'))
            -> returnCol(array('id', 'name', 'code', 'front', 'back', 'front_default', 'back_default', 'func'))
            -> formatCol(
                array(
                    1 => '<a href="languages/edit/{id}">{name}</a>',
                    7 =>
                        Form::button(
                            '',
                            Form::icon('icon-sort-by-alphabet'),
                            array(
                                'class'  =>'btn-primary',
                                'title' => $this->lang->languages['agena_btn'],
                                'onclick'  => 'engine.languages.autoGenerateAlias({id});'
                            )
                        ) .
                        Form::link(
                            '',
                            Form::icon('icon-edit'),
                            array(
                                'class'  =>'btn-info',
                                'title' => $this->lang->core['edit'],
                                'href'  => 'languages/edit/{id}'
                            )
                        ) .
                        Form::button(
                            '',
                            Form::icon('icon-remove'),
                            array(
                                'title'   => $this->lang->core['delete'],
                                'class'   =>'btn-danger',
                                'onclick' => 'engine.languages.delete({id})' // todo зробити модальне підтвердження і подивитись де провав тайтл
                            )
                        )
                ))
            -> request();
    }

    /**
     * @return mixed|string
     */
    public function create()
    {
        $buttons= array(
            Form::button(
                $this->lang->core['back'],
                Form::icon('icon-external-link'),
                array(
                    'class'=>'btn-link',
                    'onclick' => 'self.location.href=\'languages\''
                )
            ),
            Form::button(
                $this->lang->core['save'],
                Form::icon('icon-save', false),
                array(
                    'class'=>'btn-success form-submit'
                ))
        );

        $content = $this->load->view('languages/form',array(
            'action' => 'add',
            'id'        =>0,
        ));

        $this->setButtons($buttons);
        $this->setContent($content);

        return $this->output();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $buttons= array(
            Form::button(
                $this->lang->core['back'],
                Form::icon('icon-external-link'),
                array(
                    'class'=>'btn-link',
                    'onclick' => 'self.location.href=\'languages\''
                )
            ),
            Form::button(
                $this->lang->core['save'],
                Form::icon('icon-save', false),
                array(
                    'class'=>'btn-success form-submit'
                ))
        );

        $content = $this->load->view('languages/form',array(
            'action'     => 'edit',
            'data'       => $this->mg->data('languages', $id),
            'id'         => $id
        ));

        $this->setButtons($buttons);
        $this->setContent($content);

        return $this->output();
    }

    /**
     * @param $id
     * @return mixed|string
     */
    public function process($id=0)
    {
        $s=0;
        $e = $this->errors['warning'];
        $r = './languages'; // redirect url
        $data = $_POST['data'];

        // check required fields
        if(
            empty($data['name']) ||
            empty($data['code'])
        ) {
            $this->error[] = $this->lang->languages['e_required_fields'];
        }
        // add or update values
        if(empty($this->error)) {
            switch($_POST['action']){
                case 'add':
                    $id = $this->mg->create($data);
                    if($id > 0) {
                        $s=1;
                        $e = $this->errors['success'];
                        $this->error[] = $this->lang->languages['save_success'];
                    } else {
                        $e = $this->errors['error'];
                        $this->error[] = $this->mg->error();
                    }
                    break;
                case 'edit':
                    if(empty($id)) return '';
                    $r='';
                    $s = $this->mg->update($id, $data);
                    if($s > 0) {
                        $e = $this->errors['success'];
                        $this->error[] = $this->lang->languages['save_success'];
                    } else {
                        $e = $this->errors['error'];
                        $this->error[] = $this->mg->error() ;
                    }
                    break;
                default:
                    break;
            }

            if($data['front_default'] > 0) {
                $this->mg->changeDefault($id, 'front_default');
            }
            if($data['back_default'] > 0) {
                $this->mg->changeDefault($id, 'back_default');
            }
        }

        return json_encode(array(
            's' => $s > 0 , // status
            'r' => $r, // redirect url
            'e' => $e, // error class
            't' => $this->lang->core[$e], // error title
            'm' => implode('<br>', $this->error) // error message
        ));
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        return $this->mg->delete($id);
    }
} 