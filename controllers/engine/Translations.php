<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 26.06.14 12:58
 */

namespace controllers\engine;

use controllers\Engine;

defined('SYSPATH') or die();

class Translations extends Engine {

    public function __construct()
    {
        parent::__construct();

        $this->mg = $this->load->model('engine\Translations');
    }



    /**
     * @return mixed
     */
    public function index()
    {
        $buttons= array(
            Form::button(
                $this->lang->core['create'],
                Form::icon('icon-file'),
                array(
                    'class'   => Form::BTN_TYPE_PRIMARY,
                    'onclick' => "self.location.href='./translations/create'"
                ))
        );

        $t = new Table('translations', "translations/items");
        $t
            ->setTitle($this->lang->translations['table_title'])
            ->setConf('sortable', true)
            ->setConf('columns', array(
                'orderable'=> false
            ))
            ->sortableConf('brands','id', 'g.id')
            ->addTh($this->lang->translations['id'])
            ->addTh($this->lang->translations['code'])
            ->addTh($this->lang->translations['value'])
            ->addTh($this->lang->core['func'] , 'w-120')
        ;

        $this->setButtons($buttons);
        $this->setContent($t->render());

        return $this->output();
    }

    /**
     * table translations items
     * @return string
     */
    public function items()
    {
        $dt = new Table();
        return $dt
            -> table('translations g')
            -> join("join translations_info i on i.translations_id = g.id and i.languages_id = {$this->language_id}")
            -> columns(
                array(
                    'g.id',
                    'g.code',
                    'i.value'
                )
            )
            -> searchCol(array('g.code','i.value'))
            -> returnCol(array('id','code','value', 'func'))
            -> formatCol(
                array(
                    1 => '<input type="text" class="form-control" value="{code}">',
                    3 =>
                        Form::link(
                            '',
                            Form::icon('icon-edit'),
                            array(
                                'class'  =>'btn-info',
                                'title' => $this->lang->core['edit'],
                                'href'  => 'translations/edit/{id}'
                            )
                        ) .
                        Form::button(
                            '',
                            Form::icon('icon-remove'),
                            array(
                                'title'   => $this->lang->core['delete'],
                                'class'   =>'btn-danger',
                                'onclick' => 'engine.translations.delete({id})'
                            )
                        )
                ))
            -> request();
    }

    /**
     * @return mixed|string
     */
    public function create()
    {
        $buttons= array(
            Form::button(
                $this->lang->core['back'],
                Form::icon('icon-external-link'),
                array(
                    'class'=>'btn-link',
                    'onclick' => 'self.location.href=\'translations\''
                )
            ),
            Form::button(
                $this->lang->core['save'],
                Form::icon('icon-save', false),
                array(
                    'class'=>'btn-success form-submit'
                ))
        );

        $languages = $this->load->model('engine\Languages');

        $content = $this->load->view('translations/form',array(
            'languages' => $languages->all(1),
            'action' => 'add',
            'id'        =>0,
        ));

        $this->setButtons($buttons);
        $this->setContent($content);

        return $this->output();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $buttons= array(
            Form::button(
                $this->lang->core['back'],
                Form::icon('icon-external-link'),
                array(
                    'class'=>'btn-link',
                    'onclick' => 'self.location.href=\'translations\''
                )
            ),
            Form::button(
                $this->lang->core['save'],
                Form::icon('icon-save', false),
                array(
                    'class'=>'btn-success form-submit'
                ))
        );

        $languages = $this->load->model('engine\Languages');

        // дерево структури у вигляді select
        $data = $this->mg->data('translations', $id);

        $content = $this->load->view('translations/form',array(
            'languages'  => $languages->all(1),
            'action'     => 'edit',
            'data'       => $data,
            'info'       => $this->mg->info($id),
            'id'         => $id
        ));

        // output
        $this->setButtons($buttons);
        $this->setContent($content);

        return $this->output();
    }

    /**
     * @param $id
     * @return mixed|string
     */
    public function process($id=0)
    {
        $s=0;
        $e = $this->errors['warning'];
        $r = './translations/'; // redirect url
        $data = $_POST['data'];
        $info = $_POST['info'];

        // check required fields
        if(
            empty($data['code'])
        ) {
            $this->error[] = $this->lang->translations['e_required_fields'];
        }
        foreach ($info as $languages_id=>$arr) {
            if(empty($info[$languages_id]['value'])) {
                $this->error[] = $this->lang->translations['e_required_fields'];
            }
        }

        // add or update values
        if(empty($this->error)) {
            switch($_POST['action']){
                case 'add':
                    $s = $this->mg->create($data, $info);
                    if($s > 0) {
                        $e = $this->errors['success'];
                        $this->error[] = $this->lang->translations['save_success'];
                    } else {
                        $e = $this->errors['error'];
                        $this->error[] = $this->mg->error();
                    }
                    break;
                case 'edit':
                    if(empty($id)) return '';
                    $r='';
                    $s = $this->mg->update($id, $data, $info);
                    if($s > 0) {
                        $e = $this->errors['success'];
                        $this->error[] = $this->lang->translations['save_success'];
                    } else {
                        $e = $this->errors['error'];
                        $this->error[] = $this->mg->error() ;
                    }
                    break;
                default:
                    break;
            }
        }

        return json_encode(array(
            's' => $s > 0 , // status
            'r' => $r, // redirect url
            'e' => $e, // error class
            't' => $this->lang->core[$e], // error title
            'm' => implode('<br>', $this->error) // error message
        ));
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id){
        return $this->mg->delete($id);
    }
}