<?php
/**
 * Company Otakoyi.com
 * Author: wg
 * Date: 12.01.15 13:24
 */

namespace controllers\engine;

use controllers\Engine;

defined("SYSPATH") or die();

class Delivery extends Engine {
    private $model;

    public function __construct()
    {
        parent::__construct();

        $this->model = $this->load->model('engine\Delivery');

    }
    /**
     * @return mixed|string
     */
    public function index()
    {
        $this->setButtons(
            Form::button
                (
                    $this->lang->core['create'],
                    Form::icon('icon-file'),
                    array(
                        'class'   => Form::BTN_TYPE_PRIMARY,
                        'onclick' => "self.location.href='delivery/create'"
                    )
                )
        );
        $t = new Table('delivery', 'delivery/items');

        $t
            ->setTitle($this->lang->delivery['table_title'])
            ->sortableConf('delivery','d.id','d.sort');
        $t ->addTh('#');
        $t ->addTh($this->lang->delivery['name']);
        $t ->addTh($this->lang->delivery['price']);
        $t ->addTh($this->lang->delivery['free_from']);
        $t ->addTh($this->lang->core['func'] , 'w-180');

        $this->setContent($t->render());

        return $this->output();
    }

    /**
     * delivery list
     * @return string
     */
    public function items()
    {
        $dt = new Table();
        $dt
            -> table('delivery d')
            -> join("join delivery_info i on i.delivery_id = d.id and i.languages_id = {$this->language_id}")
            -> columns(
                array(
                    'd.id',
                    'i.name',
                    'd.price',
                    'd.free_from',
                    'd.published'
                )
            )
            -> searchCol(array('i.name'))
//            -> returnCol(array('d.id','d.pib','d.message'))
        ;
        $rows = $dt->getRows();
        $out = array();
        foreach ($rows as $row) {
            $out[] = array(
                $row['id'],
                "<a href='delivery/edit/{$row['id']}'>{$row['name']}</a>",
                $row['price'],
                $row['free_from'],
                Form::link(
                    '',
                    Form::icon('icon-eye-'. $row['published']),
                    array(
                        'class'    =>'btn-primary',
                        'onclick' => 'engine.delivery.pub('.$row['id'].','. $row['published'] .')'
                    )
                ) .
                Form::link(
                    '',
                    Form::icon('icon-edit'),
                    array(
                        'title' => $this->lang->core['edit'],
                        'class'    =>'btn-primary',
                        'href' => 'delivery/edit/'.$row['id']
                    )
                ) .
                Form::button(
                    '',
                    Form::icon('icon-remove'),
                    array(
                        'title'   => $this->lang->core['delete'],
                        'class'   =>'btn-danger',
                        'onclick' => 'engine.delivery.del('.$row['id'].')'
                    )
                )
            );
        }
        $dt->beforeRender($out);

        return $dt->request();
    }

    public function create()
    {
        $buttons= array(
            Form::link(
                $this->lang->core['back'],
                Form::icon('icon-external-link'),
                array(
                    'class'=>'btn-link',
                    'href' => 'delivery'
                )
            ),
            Form::button(
                $this->lang->core['save'],
                Form::icon('icon-save', false),
                array(
                    'class'=>'btn-success form-submit'
                ))
        );

        $languages = $this->load->model('engine\Languages');

        $content = $this->load->view('delivery/form',array(
            'languages' => $languages->all(),
            'action' => 'create',
            'id'        => 0,
            'payment'  => $this->model->getPayment(0)
        ));

        $this->setButtons($buttons);
        $this->setContent($content);

        return $this->output();
    }

    public function edit($id)
    {

        $buttons= array(
            Form::link(
                $this->lang->core['back'],
                Form::icon('icon-external-link'),
                array(
                    'class'=>'btn-link',
                    'href' => 'delivery'
                )
            ),
            Form::button(
                $this->lang->core['save'],
                Form::icon('icon-save', false),
                array(
                    'class'=>'btn-success form-submit'
                ))
        );

        $languages = $this->load->model('engine\Languages');

        $content = $this->load->view('delivery/form',array(
            'languages' => $languages->all(),
            'action' => 'edit',
            'id'        => $id,
            'data'   => $this->model->params($id),
            'info'   => $this->model->info($id),
            'payment'  => $this->model->getPayment($id)
        ));

        $this->setButtons($buttons);
        $this->setContent($content);

        return $this->output();

    }

    public function delete($id)
    {
        return $this->model->delete($id);
    }

    /**
     * publish comment
     * @param $id
     * @param $published
     * @return mixed
     */
    public function pub($id, $published)
    {
        $published = $published == 1 ? 0 : 1;
        return $this->model->pub($id, $published);
    }

    public function process($id=0)
    {
        $s=0;
        $e = $this->errors['warning'];
        $r = 'delivery';
        $data = $_POST['data'];
        $info = $_POST['info'];
/*
        if(
            empty($data['price'])
        ){
            $this->error[] = $this->lang->delivery['e_required_fields'];
        }
*/
        foreach ($info as $languages_id=>$arr) {
            if(empty($info[$languages_id]['name'])) {
                $this->error[] = $this->lang->delivery['e_name'];
            }
        }

        if(empty($this->error)) {
            switch($_POST['action']){
                case 'create':
                    $id = $this->model->create($data, $info);
                    if($id > 0) {
                        $e = $this->errors['success'];
                        $this->error[] = $this->lang->delivery['save_success'];
                    } else {
                        $e = $this->errors['error'];
                        $this->error[] = $this->model->error();
                    }
                    break;
                case 'edit':
                    if(empty($id)) return '';
                    $r='';
                    $s = $this->model->update($id, $data, $info);
                    if($s > 0) {
                        $e = $this->errors['success'];
                        $this->error[] = $this->lang->delivery['save_success'];
                    } else {
                        $e = $this->errors['error'];
                        $this->error[] = $this->model->error() ;
                    }
                    break;
                default:
                    break;
            }
            if($id > 0){
                $dp = isset($_POST['dp']) ? $_POST['dp'] : array();
                $this->model->setPayment($id, $dp);
            }
        }

        return json_encode(array(
            's' => $s > 0 , // status
            'r' => $r, // redirect url
            'e' => $e, // error class
            't' => $this->lang->core[$e], // error title
            'm' => implode('<br>', $this->error) // error message
        ));
    }
    public function sort($delivery_id, $sort)
    {
        $s = explode(',', $sort);

        foreach ($s as $k=> $payment_id) {
            $this->model->updateSort($delivery_id, $payment_id, $k);
        }
        return 1;
    }

    public function install()
    {
        return 1;
    }
    public function uninstall()
    {
        return 1;
    }

} 