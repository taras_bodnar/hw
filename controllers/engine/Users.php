<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 26.06.14 12:58
 */

namespace controllers\engine;

use controllers\Engine;

defined('SYSPATH') or die();

class Users extends Engine {

    private $rang = array(100,999);

    public function __construct()
    {
        parent::__construct();

        $this->mg = $this->load->model('engine\Users');
    }

    /**
     * @param int $parent_id
     * @return mixed
     */
    public function index($parent_id=0)
    {
        if(empty($parent_id)) $parent_id='';
        $buttons= array(
            Form::button(
                $this->lang->core['create'],
                Form::icon('icon-file'),
                array(
                    'class'   => Form::BTN_TYPE_PRIMARY,
                    'onclick' => "self.location.href='users/create/{$parent_id}'"
                ))
        );

        $t = new Table('users', "users/items/$parent_id");
        $t
//            ->setConf('sortable', true)
            // todo відключення сортування останньої колонки не працює
            ->setConf('columns', array(
                'orderable'=> false,

            ))
            ->sortableConf('users','u.sort','u.sort')
            ->setTitle($this->lang->users['table_title'])
            ->addTh($this->lang->users['id'])
            ->addTh($this->lang->users['name'])
            ->addTh($this->lang->users['phone'])
            ->addTh($this->lang->users['email'])
            ->addTh("Востаннє авторизований")
            ->addTh($this->lang->users['createdon'])
            ->addTh($this->lang->core['func'] , 'w-120')
        ;

        $this->setButtons($buttons);
        $this->setContent($t->render());

        return $this->output();
    }

    /**
     * table users group items
     * @param $users_group_id
     * @return string
     */
    public function items($users_group_id=0)
    {
        $dt = new Table();
        return $dt
            -> table('users u')
            -> where($users_group_id > 0 ? "u.users_group_id=$users_group_id " : '')
            -> join("join users_group g on g.id=u.users_group_id and g.rang between {$this->rang[0]} and {$this->rang[1]}")
            -> columns(
                array(
                    'u.id',
                    'u.name',
                    'u.phone',
                    'u.email',
                    'u.last_login',
                    'u.createdon'
                )
            )
            -> searchCol(array('u.id','u.phone','u.email','u.name'))
            -> returnCol(array('u.id','name','phone','email','last_login','createdon', 'func'))
            -> formatCol(
                array(
                    1 => '<a href="users/edit/{id}">{name}</a>',
                    4 =>'<p>{last_login}</p>',
                    6 =>
                        Form::link(
                            '',
                            Form::icon('icon-edit'),
                            array(
                                'class'  =>'btn-info',
                                'title' => $this->lang->core['edit'],
                                'href'  => 'users/edit/{id}'
                            )
                        ) .
                        Form::button(
                            '',
                            Form::icon('icon-remove'),
                            array(
                                'title'   => $this->lang->core['delete'],
                                'class'   =>'btn-danger',
                                'onclick' => 'engine.users.delete({id})'
                            )
                        )
                ))
            -> request();
    }


    public function create($parent_id='')
    {
        $buttons= array(
            Form::button(
                $this->lang->core['back'],
                Form::icon('icon-external-link'),
                array(
                    'class'=>'btn-link',
                    'onclick' => 'self.location.href=\'users/index/'.$parent_id.'\''
                )
            ),
            Form::button(
                $this->lang->core['save'],
                Form::icon('icon-save', false),
                array(
                    'class'=>'btn-success form-submit'
                ))
        );

        $content = $this->load->view('users/form',array(
            'action' => 'create',
            'id'        => 0,
            'group'     => $this->mg->group(),
            'languages' => $this->mg->languages(),
            'form_action' => 'users/process',
            'plugins'   => $this->getPlugins(0, 'create')
        ));

        $this->setButtons($buttons);
        $this->setContent($content);

        return $this->output();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {

        $buttons= array(
            Form::button(
                $this->lang->core['back'],
                Form::icon('icon-external-link'),
                array(
                    'class'=>'btn-link',
                    'onclick' => 'self.location.href=\'users\''
                )
            ),
            Form::button(
                $this->lang->core['save'],
                Form::icon('icon-save', false),
                array(
                    'class'=>'btn-success form-submit'
                ))
        );



        $content = $this->load->view('users/form',array(
            'action'     => 'edit',
            'data'       => $this->mg->data('users', $id),
            'id'         => $id,
            'group'     => $this->mg->group(),
            'form_action' => 'users/process',
            'languages' => $this->mg->languages(),
            'plugins'   => $this->getPlugins($id, 'create')
        ));

        // output
        $this->setButtons($buttons);
        $this->setContent($content);

        return $this->output();
    }

    /**
     * @param $id
     * @return mixed|string
     */
    public function process($id='')
    {
        error_reporting(0);
        $s=0;
        $e = $this->errors['warning'];
        $r = 'users'; // redirect url
        $data = $_POST['data'];

        // check required fields
        if(
            empty($data['name']) ||
//            empty($data['phone']) ||
            empty($data['email'])
        ) {
            $this->error[] = $this->lang->users['e_required_fields'];
        }

        if($_POST['action'] == 'create' && empty($data['password'])) {
            $this->error[] = $this->lang->users['e_enter_pass'];
        }
        // add or update values
        if(empty($this->error)) {
            switch($_POST['action']){
                case 'create':
                    $data['password'] = $this->cryptPass($data['password']);
                    $data['createdon'] = date('Y-m-d H:i:s');
                    $data['url'] = uniqid();
                    $s = $this->mg->create($data);
                    if($s > 0) {
                        $e = $this->errors['success'];
                        $this->error[] = $this->lang->users['save_success'];
                    } else {
                        $e = $this->errors['error'];
                        $this->error[] = $this->mg->error();
                    }
                    break;
                case 'edit':
                    if(empty($id)) return '';
                    $r='';

                    if(empty($data['password'])){
                        unset($data['password']);
                    } else {
                        $data['password'] = crypt($data['password']);
                    }

                    $s = $this->mg->update($id, $data);

                    if($s > 0) {
                        $e = $this->errors['success'];
                        $this->error[] = $this->lang->users['save_success'];
                    } else {
                        $e = $this->errors['error'];
                        $this->error[] = $this->mg->error() ;
                    }

                    break;
                default:
                    break;
            }

            $this->getPlugins($id, 'process');
        }

        return json_encode(array(
            's' => $s > 0 , // status
            'r' => $r, // redirect url
            'e' => $e, // error class
            't' => $this->lang->core[$e], // error title
            'm' => implode('<br>', $this->error) // error message
        ));
    }

    /**
     * crypt passw vs salt
     * @param $pass
     * @return string
     */
    private function cryptPass($pass)
    {
        return crypt($pass);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $this->getPlugins($id, 'delete');
        return $this->mg->delete($id);
    }

    /**
     * users group tree
     * @return string
     */
    public function tree()
    {
        $parent_id = isset($_POST['id']) ? (int)$_POST['id'] : 0;
        $tree=array();
        $r = $this->mg->tree($parent_id);
        $i=0;
        if(!empty($r)){
            foreach ( $r as $row) {
                $tree[$i]['data'] = $row['name'] . ' #' . $row['id'] ;
                $tree[$i]['state'] = $row['isfolder'] > 0  ? 'closed' : '';

                //todo нада повіксати шлях

                $tree[$i]['attr'] = array(
                    'id'   => $row['id'],
                    "rel"  => (($row['isfolder'])? 'folder':'file'),
                    "href" =>   'users/index/' . $row['id'] ."/"
                );
                $i++;
            }
        }

        return json_encode($tree);
    }

    /**
     * get admin info
     * @param null $key
     * @return mixed
     */
    public static function adminInfo($key=null)
    {
        if(!isset($_SESSION['admin'])) return null;

        return $key ? isset($_SESSION['admin'][$key]) : $_SESSION['admin'];
    }

    /**
     * return controller content
     * @return string
     */
    protected function output()
    {
        $this->setSidebar(
            new Tree(
                'users',
                 'users/tree/',
                Tree::button($this->lang->users['tree_add'], 'self.location.href="users/create";', 'icon-plus-sign'),
                array(
                    Tree::contextMenu($this->lang->users['tree_cmenu_add'], 'icon-plus' ,'self.location.href="UsersGroup/create/"+id;'),
                    Tree::contextMenu($this->lang->users['tree_cmenu_edit'], 'icon-pencil' ,'self.location.href="UsersGroup/edit/"+id;'),
                    Tree::contextMenu($this->lang->users['tree_cmenu_del'], 'icon-remove' , 'engine.users.delete(id)')
                ),
                true
            )
        );

        return parent::output();
    }
}