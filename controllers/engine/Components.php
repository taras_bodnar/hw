<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 31.05.14 18:36
 */

namespace controllers\engine;

use controllers\Engine;

defined('SYSPATH') or die();

class Components extends Engine {

    public function __construct()
    {
        parent::__construct();

        $this->ms = $this->load->model('engine\Components');
    }

    public function index($parent_id = 1)
    {
        $buttons= array(
            Form::button(
                $this->lang->core['create'],
                Form::icon('icon-file'),
                array(
                    'class'   => Form::BTN_TYPE_PRIMARY,
                    'onclick' => "self.location.href='components/create/{$parent_id}'"
                ))
        );

        $t = new Table('components', "components/items/$parent_id");
        $t
           ->setConf('sortable', true)
            // todo відключення сортування останньої колонки не працює
           ->setConf('columns', array(
                'orderable'=> false,

            ))
           ->sortableConf('components','id')
           ->setTitle($this->lang->components['table_title'])
           ->addTh($this->lang->components['id'])
           ->addTh($this->lang->components['name'])
           ->addTh($this->lang->components['controller'])
           ->addTh($this->lang->components['rang'])
           ->addTh($this->lang->core['func'] , 'w-160')
        ;

        $this->setButtons($buttons);
        $this->setContent($t->render());

        return $this->output();
    }

    public function items($parent_id=0)
    {
        $parent_id = (int) $parent_id;
        $dt = new Table();
        return $dt
            -> table('components s')
            -> where("s.parent_id=$parent_id ")
            -> join("join components_info i on i.components_id = s.id and i.languages_id = {$this->language_id}")
            -> columns(
                array(
                    's.id',
                    'i.name',
                    'REPLACE(s.controller, "#", "/") as controller',
                    's.rang',
                    's.published',
                    'i.description',
                    'CONCAT("components/", IF(s.isfolder, "index/", "edit/"), s.id) as href'
                )
            )
            -> searchCol(array('s.controller','i.name'))
            -> returnCol(array('s.id','name','controller','rang', 'func'))
            -> formatCol(
                array(
                1 => '<a title="{description}" href="{href}">{name}</a>',
                4 => Form::link(
                        '',
                        Form::icon('icon-eye-{published}'),
                        array(
                           'class'    =>'btn-primary',
                            'title'   => $this->lang->core['published_tip'],
                            'onclick' => 'engine.components.pub({id},{published})'
                        )
                    ) .
                    Form::link(
                        '',
                        Form::icon('icon-edit'),
                        array(
                           'class'  =>'btn-info',
                            'title' => $this->lang->core['edit'],
                            'href'  => 'components/edit/{id}'
                        )
                    ) .
                    Form::button(
                        '',
                        Form::icon('icon-remove'),
                        array(
                            'title'   => $this->lang->core['delete'],
                            'class'   =>'btn-danger',
                            'onclick' => 'engine.components.delete({id})' // todo зробити модальне підтвердження і подивитись де провав тайтл
                        )
                    )
            ))
            -> request();
    }

    public function create($parent_id=0)
    {
        $buttons= array(
            Form::button(
                $this->lang->core['back'],
                Form::icon('icon-external-link'),
                array(
                    'class'=>'btn-link',
                    'onclick' => 'self.location.href=\'components\''
                )
            ),
            Form::button(
                $this->lang->core['save'],
                Form::icon('icon-save', false),
                array(
                    'class'=>'btn-success form-submit'
                ))
        );

        $languages = $this->load->model('engine\Languages');

        $content = $this->load->view('components/form',array(
            'languages' => $languages->all(0,1),
            'action' => 'add',
            'id'        =>$parent_id,
            'parents'   => $this->ms->parents($parent_id),
            'controllers'=> $this->controllersList(DOCROOT . 'controllers/engine/', 'controllers/engine/')
        ));

        $this->setButtons($buttons);
        $this->setContent($content);

        return $this->output();
    }
    public function edit($id)
    {
        $buttons= array(
            Form::button(
                $this->lang->core['back'],
                Form::icon('icon-external-link'),
                array(
                    'class'=>'btn-link',
                    'onclick' => 'self.location.href=\'components\''
                )
            ),
            Form::button(
                $this->lang->core['save'],
                Form::icon('icon-save', false),
                array(
                    'class'=>'btn-success form-submit'
                ))
        );

        $languages = $this->load->model('engine\Languages');

        // дерево структури у вигляді select
        $data = $this->ms->data('components', $id);
//        $this->dump($data);
        $content = $this->load->view('components/form',array(
            'languages'  => $languages->all(0,1),
            'action'     => 'edit',
            'data'       => $data,
            'info'       => $this->ms->info($id),
            'id'         => $id,
            'parents'    => $this->ms->parents($data['parent_id'], $id),
            'controllers'=> $this->controllersList(DOCROOT . 'controllers/engine/', 'controllers/engine/', $data['controller'])
        ));

        // output
        $this->setButtons($buttons);
        $this->setContent($content);

        return $this->output();
    }

    /**
     * @param $dir
     * @param string $prefix
     * @param string $selected
     * @return array
     */
    private function controllersList($dir, $prefix='', $selected='')
    {
        $res=array();
        $c = $this->readControllers($dir);
        foreach($c as $k=>$co){
//            $v = $prefix . mb_strtolower($co);
            $v = $prefix . $co;
            $s= $v==$selected ? 'selected' : '';

            $res[] = array(
                'value' => $v,
                'name'  => $co,
                'selected'=> $s
            );
        }
//        $this->dump($res);
        return $res;
    }

    private function readControllers($dir, $prefix = '')
    {
        $dir = rtrim($dir, '\\/');
        $result = array();

        $h = opendir($dir);
        while (($f = readdir($h)) !== false) {
            if ($f !== '.' and $f !== '..') {
                if (is_dir("$dir/$f")) {
                    $result = array_merge($result, $this->readControllers("$dir/$f", "$prefix$f/"));
                } else {
                    $f = strtr($f, array('.php'=>''));
                    $result[] = $prefix.$f;
                }
            }
        }
        closedir($h);
        return $result;
    }

    /**
     * @param $id
     * @return mixed|string
     */
    public function process($id=0)
    {
        $s=0;
        $e = $this->errors['warning'];
        $r = './components/index/'. $_POST['data']['parent_id']; // redirect url
        $data = $_POST['data'];
        $info = $_POST['info'];

        // check required fields
        if(
            empty($data['controller']) ||
            empty($data['rang'])
        ) {
            $this->error[] = $this->lang->components['e_required_fields'];
        }
        foreach ($info as $languages_id=>$arr) {
            if(empty($info[$languages_id]['name'])) {
                $this->error[] = $this->lang->components['e_name'];
            }
        }

        // add or update values
        if(empty($this->error)) {
            $data['parent_id'] = isset($data['parent_id']) ? $data['parent_id'] : 0;
            switch($_POST['action']){
                case 'add':
                    if($this->install($data['controller'])){
                        $s = $this->ms->create($data, $info);
                    }

                    if($s > 0) {
                        $this->ms->toggleFolder($data['parent_id']);
                        $e = $this->errors['success'];
                        $this->error[] = $this->lang->components['save_success'];
                    } else {
                        $e = $this->errors['error'];
                        $this->error[] = $this->ms->error();
                    }
                    break;
                case 'edit':
                    if(empty($id)) return '';
                    $r='';
                    $s = $this->ms->update($id, $data, $info);
                    if($s > 0) {
                        $this->ms->toggleFolder($data['parent_id']);
                        $e = $this->errors['success'];
                        $this->error[] = $this->lang->components['save_success'];
                    } else {
                        $e = $this->errors['error'];
                        $this->error[] = $this->ms->error() ;
                    }
                    break;
                default:
                    break;
            }
        }

        return json_encode(array(
            's' => $s > 0 , // status
            'r' => $r, // redirect url
            'e' => $e, // error class
            't' => $this->lang->core[$e], // error title
            'm' => implode('<br>', $this->error) // error message
        ));
    }

    /**
     * @param $controller
     * @return mixed
     */
    private function install($controller)
    {
        $c = $this->load->component($controller);
        if(method_exists($c,'install')){
            return $c->install();
        }
        return 1;
    }

    /**
     * @param $id
     * @return mixed
     */
    private function uninstall($id)
    {
        $c = $this->ms->getData($id, 'controller');
        if(empty($c)) return 1;
        $c = $this->ms->formatController($c);
        $c = $this->load->component($c);
        if(method_exists($c,'uninstall')){
            return $c->uninstall();
        }
        return 1;
    }
    /**
     * delete item from components
     * @param $id
     * @return mixed 1
     */
    public function delete($id)
    {
        if($this->uninstall($id)){
            return $this->ms->delete($id);
        }
        return 0;
    }

    /**
     * toggle published status
     * @param $id
     * @param $published
     * @return mixed
     */
    public function pub($id, $published)
    {
        $published = $published == 1 ? 0 : 1;
        return $this->ms->pub($id,$published);
    }

    public function tree()
    {
        $parent_id = isset($_POST['id']) ? (int)$_POST['id'] : 0;
        $tree=array();
        $r = $this->ms->tree($parent_id);
        $i=0;
        if(!empty($r)){
            foreach ( $r as $row) {
                $tree[$i]['data'] = $row['name'] . ' #' . $row['id'] ;
                $tree[$i]['state'] = $row['isfolder'] ? 'closed' : '';

                //todo нада повіксати шлях

                $tree[$i]['attr'] = array(
                    'id' => $row['id'],
                    "rel"=> (($row['isfolder'])? 'folder':'file'),
                    "href"=>   './components/'. (($row['isfolder'])? 'index' : 'edit') ."/" . $row['id'] ."/"
                );
                $i++;
            }
        }

        return json_encode($tree);
    }

    /**
     * return controller content
     * @return string
     */
    protected function output()
    {
        $this->setSidebar(
            new Tree(
                'components',
                'components/tree/',
                '',
                array(
                    Tree::contextMenu($this->lang->components['tree_cmenu_add'], 'icon-plus' ,'self.location.href="./components/create/"+id;'),
                    Tree::contextMenu($this->lang->components['tree_cmenu_edit'], 'icon-pencil' ,'self.location.href="./components/edit/"+id;'),
                    Tree::contextMenu($this->lang->components['tree_cmenu_del'], 'icon-remove' ,'engine.components.delete(id)')
                ),
                true
            )
        );

        return parent::output();
    }
}
