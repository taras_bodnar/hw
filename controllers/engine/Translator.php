<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 02.07.14 11:09
 */

namespace controllers\engine;

use controllers\engine\translator\Google;
use controllers\engine\translator\Yandex;
use controllers\core\Controller;
use controllers\core\Settings;

defined('SYSPATH') or die();

class Translator extends Controller{
    public function __construct()
    {
        parent::__construct();
        $this->request->mode = "engine"; // engine | app
    }
    public function index(){}
    public function translate($text, $from, $to){
        $translator = Settings::instance()->get('translator');

        switch($translator){
            case 'google':
                $t = new Google();
                break;
            case 'yandex':
                $t = new Yandex();
                break;
            default :
                return 'Could to instance Translator ' . $translator;
                break;
        }

        return  $t->translate($text, $from, $to);
    }

    public function translateAll($id, $code)
    {

        $sys_l = $_POST['s_languages'];
        $def = array(); $out = array();
//        var_dump($_POST);
// todo якась помилка з циклом , вертає пусті значення
//        $this->dump($_POST['info']);
        foreach($_POST['info'] as $_id => $field ){
            foreach ($field as $n => $v) {
//                echo $_id, ',' , $n ,',' , $v;
                if( $_id == $id ){
                    $def[$n] = $v;
                } else {

                    if(empty($def[$n])) continue;

                    $out[] = array(
                        'n' => 'info_' . $_id .'_'. $n,
                        'v' => $this->translate($def[$n], $code, $sys_l[$_id] )
                    );
                }
//                echo $def[$n], '|' , $code , '|' ,  $sys_l[$_id] , '<br>';
            }
        }
//        print_r($out);
        return json_encode($out);
    }

    public function translateOptionsValue($code)
    {
        $sys_l=array();
        foreach ($_POST['s_languages'] as $k=>$v) {
            $sys_l[] = $v;
        }

        $def = array(); $out = array(); $i=0;
        if(isset($_POST['options_value_info'])){

            foreach($_POST['options_value_info'] as $_id => $value ){
                if($i == 0) {
                    $def = $value;
                }
//            echo $def , '|', $code , '|', $sys_l[$i],'<br>';
                $out[] = array(
                    'n' => 'options_value_info_' . $_id,
                    'v' => $this->translate($def, $code, $sys_l[$i] )
                );
                $i++;
            }
        }
        $i=0;
        if(isset($_POST['value'])){
            foreach ($_POST['value'] as $languages_id => $value) {
                if($i == 0){
                    $def = $value;
                }
                $out[] = array(
                    'n' => 'value_'. $languages_id,
                    'v' => $this->translate($def, $code, $_POST['s_languages'][$languages_id] )
                );
                $i++;
            }

        }

        //new_options_value_info[22][7]
        //new_options_value_info_22_7
        if(isset($_POST['new_options_value_info'])){
            if(!isset($_POST['options_value_info'])) $i=0;
            foreach($_POST['new_options_value_info'] as $_id => $a ){
                foreach ($a as $languages_id => $value) {
                    if($i == 0){
                        $def = $value;
                    }
                    $out[] = array(
                        'n' => 'new_options_value_info_'. $_id .'_'. $languages_id,
                        'v' => $this->translate($def, $code, $_POST['s_languages'][$languages_id] )
                    );
                }
                $i++;
            }
        }

        return json_encode($out);
    }

    /**
     * автоматичний перекла назви параметру в модальному вікні
     * @param $code
     * @return string
     */
    public function translateModalFeaturesName($code)
    {
        $sys_l = $_POST['s_languages'];
        $def = array(); $out = array(); $i = 0;
        foreach($_POST['info'] as $languages_id => $field ){

            foreach ($field as $n => $v) {
                if( $i== 0 ){
                    $def[$n] = $v;
                } else {
                    if(empty($def[$n])) continue;

                    $out[] = array(
                        'n' => 'info_' . $languages_id .'_'. $n,
                        'v' => $this->translate($def[$n], $code, $sys_l[$languages_id] )
                    );
                }
            }
            $i++;
        }

        return json_encode($out);
    }
}