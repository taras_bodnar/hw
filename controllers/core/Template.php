<?php
/**
 * Company Otakoyi.com
 * Author: wg
 * Date: 18.01.15 15:59
 */

namespace controllers\core;

//use controllers\App;

defined("SYSPATH") or die();

require_once DOCROOT. "vendor/smarty/Smarty.class.php";

/**
 * Class Template
 * @package controllers\core
 */
class Template{
    private static $instance;
    private $settings;
    private $smarty;

    public function __construct()
    {
        // load config
        $config = Config::instance()->get('smarty');
        $this->settings = Settings::instance()->get();

        // init smarty
        $this->smarty = new \Smarty();
        $this->smarty->compile_check = $config['compile_check'];
        $this->smarty->caching = $config['caching'];
        $this->smarty->cache_lifetime = $config['cache_lifetime'];
        $this->smarty->debugging = $config['debugging'];
        $this->smarty->error_reporting = E_ALL & ~E_NOTICE;

        // get theme
        $theme = $this->settings['themes_path'] .
                 $this->settings['app_theme_current'] . '/' .
                 $this->settings['app_views_path'] .'/';

        $this->smarty->setCompileDir(DOCROOT . '/tmp/compiled/' . $this->settings['app_theme_current']);
        $this->smarty->setTemplateDir(DOCROOT . $theme);

        if(!is_dir($this->smarty->getCompileDir()))
            mkdir($this->smarty->getCompileDir(), 0777, true);

        $this->smarty->setCacheDir('cache');

        // custom vars

        $template_url = APPURL . $this->settings['themes_path'] . $this->settings['app_theme_current'] . '/';

        $this->smarty->assign('template_url', $template_url);
        $this->smarty->assign('base_url', APPURL);
        $this->smarty->assign('skey',    SKEY);
    }

    /**
     * @return Template
     */
    public static function instance()
    {
        if(self::$instance == null){
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * @param $tpl_var
     * @param null $value
     * @param bool $nocache
     * @return $this
     */
    public function assign($tpl_var, $value = null, $nocache = false)
    {
        $this->smarty->assign($tpl_var, $value, $nocache);

        return $this;
    }

    /**
     * @param null $template
     * @param null $cache_id
     * @param null $compile_id
     * @param null $parent
     * @param bool $display
     * @param bool $merge_tpl_vars
     * @param bool $no_output_filter
     * @return string
     */
    public function fetch($template = null, $cache_id = null, $compile_id = null, $parent = null, $display = false, $merge_tpl_vars = true, $no_output_filter = false)
    {

        if(!empty($template) && strpos($template, 'string:') === false && strpos($template, '.tpl') === false){
            $template .= '.tpl';
        }

        return $this->smarty->fetch($template, $cache_id, $compile_id, $parent, $display, $merge_tpl_vars, $no_output_filter);
    }
} 