<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 21.05.14 23:10
 */

namespace controllers\core;

defined('SYSPATH') or die();

class Languages
{
    private static $instance;
    protected $storage=array();
    private $dir;

    private function __construct()
    {
        $conf = Config::instance()->get('languages');
        $themes_path = Settings::instance()->get('themes_path');
        $current     = Settings::instance()->get('engine_theme_current');

        $this->dir = DOCROOT . $themes_path . $current.'/languages/';
        $this->load($conf['code']);
    }

    public static function instance()
    {
        if(!self::$instance instanceof self){
            self::$instance = new self;
        }
        return self::$instance;
    }
    /**
     * Load localization by file
     * if need load some file set name or load all files in folder
     * @code string
     */
    public function load($code)
    {
        if(!isset($this->storage['lang'])) {
            $this->storage['lang'] = array();
        }

        if ($handle = opendir( $this->dir. $code )){
            while (false !== ($file = readdir($handle))) {
                if ($file != "." && $file != "..") {
                    $path = $this->dir . $code . '/' . $file;
                    if(is_readable( $path )){
                        $arr = parse_ini_file($path, true);
                        $this->storage['lang'] = array_merge($this->storage['lang'],$arr);
                    } else {
                        throw new \Exception("Не можу завантажити фацл локалізації {$path}");
                    }
                }
            }
            closedir($handle);
        }
        return $this;
    }

    /**
     * load array of translation and convert to object
     * [core]
     * save='Зберегти'
     * edit='Редагувати'
     * delete='Видалити'
     * ...
     * stdClass Object
     * return object
     * (
     * [core] => Array
     *  (
     * [save] => Зберегти
     * [edit] => Редагувати
     * [delete] => Видалити
     * )
     *
     * use echo $lang->core['save'] to didplay core -> save translation
     * */
    final public function getTranslations()
    {
        if(empty($this->storage['lang'])) return null;

        $object = new \stdClass();
        foreach ($this->storage['lang'] as $key => $value)
        {
            if(!isset($object->$key)){
                $object->$key=null;
            }
            $object->$key = $value;
        }

        return $object;
    }

    /**
     *	Setter method
     *	@param string $index
     *	@param mixed $value
     */
    final public function __set($index, $value)
    {
        $this->storage[$index] = $value;
    }

    /**
     *	Getter method
     *	@param string $index
     *  @return array
     */
    final public function __get($index)
    {
        return isset($this->storage[$index]) ? $this->storage[$index] : null;
    }
} 