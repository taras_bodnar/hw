<?php
/**
 * Created by PhpStorm.
 * User: wg
 * Date: 02.05.14
 * Time: 16:25
 */

namespace controllers\core;

if ( !defined('SYSPATH') ) die();

/**
 * Class Config
 * @package system\core
 */
class Config {
    
    private static $_instance;
    private static $_data;
    private static $path = "system/config.php";

    public function __construct()
    {
        $config_file = DOCROOT . self::$path;

        if(!is_readable($config_file)){
            header('Location: /install');
            die();
        }
        // зчитую конфіг
        self::$_data = include($config_file);

        if(self::$_data['db']['db'] == '%db%') {
            header('Location: /install');
            die();
        }

    }

    /**
     * @return Config
     */
    public static function instance(){
        if(self::$_instance == null){
            self::$_instance = new Config();
        }

        return self::$_instance;
    }

    public function get($key=null){

        if($key){

            $data = '';

            if(strpos($key,'.')){

                $parts = explode('.', $key);
                $c = count($parts);

                if($c == 1){
                    if(isset(self::$_data[$parts[0]])){
                        $data = self::$_data[$parts[0]];
                    }
                }else if($c == 2){
                    if(isset(self::$_data[$parts[0]][$parts[1]])){
                        $data = self::$_data[$parts[0]][$parts[1]];
                    }
                }else if($c == 3){
                    if(isset(self::$_data[$parts[0]][$parts[1]][$parts[2]])){
                        $data = self::$_data[$parts[0]][$parts[1]][$parts[2]];
                    }
                }

                return $data;
            }
        }

        return $key ? self::$_data[$key] : self::$_data;
    }

    public function __set($key,$val){

        if(isset(self::$_data[$key])){
            self::$_data[$key] = $val;
        }

        return self::$_instance;
    }

    public function __unset($k){unset($this->d[$k],$this->c[$k]);}
} 