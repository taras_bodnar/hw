<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 03.05.14 21:47
 */

namespace controllers\core;

if ( !defined('SYSPATH') ) die();

/**
 * Base controller
 */
abstract class Controller {

    protected $storage;

    protected $load;
    protected $request;
    /**
     * error handler
     * @var object
     */
    protected $error;

    protected $sys_name = 'OYi.Engine';

    public function __construct()
    {
        $this->load = Load::instance();
        $this->request = Request::instance();
    }

    /**
     * @return mixed
     */
    abstract public function index();
    /**
     *	Setter method
     *	@param string $index
     *	@param mixed $value
     */
    final public function __set($index, $value)
    {
        $this->storage[$index] = $value;
    }

    /**
     *	Getter method
     *	@param string $index
     *  @return array
     */
    final public function __get($index)
    {
        return isset($this->storage[$index]) ? $this->storage[$index] : null;
    }

    final public static function dump($var)
    {
        echo '<pre>'; print_r($var); echo '</pre>';
    }
} 