SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;


DROP TABLE IF EXISTS `posts_categories`;
CREATE TABLE IF NOT EXISTS `posts_categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cid` int(11) unsigned NOT NULL,
  `pid` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cid` (`cid`,`pid`),
  KEY `fk_posts_categories_content1_idx` (`cid`),
  KEY `fk_posts_categories_content2_idx` (`pid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `chunks`;
CREATE TABLE IF NOT EXISTS `chunks` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `path` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `comments`;
CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) unsigned NOT NULL DEFAULT '0',
  `content_id` int(11) unsigned NOT NULL,
  `pib` varchar(60) NOT NULL,
  `email` varchar(60) NOT NULL,
  `message` text NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `createdon` datetime DEFAULT NULL,
  `ip` char(15) DEFAULT NULL,
  PRIMARY KEY (`id`,`content_id`),
  KEY `fk_comments_content1_idx` (`content_id`),
  KEY `email` (`email`),
  KEY `pib` (`pib`),
  KEY `published` (`published`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `components`;
CREATE TABLE IF NOT EXISTS `components` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` tinyint(3) unsigned NOT NULL,
  `isfolder` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `icon` varchar(30) DEFAULT NULL,
  `controller` varchar(150) DEFAULT NULL,
  `sort` tinyint(3) unsigned DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `rang` int(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`),
  KEY `isfolder` (`isfolder`),
  KEY `sort` (`sort`),
  KEY `published` (`published`),
  KEY `module` (`controller`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=46 ;

INSERT INTO `components` (`id`, `parent_id`, `isfolder`, `icon`, `controller`, `sort`, `published`, `rang`) VALUES
(1, 0, 1, 'icon-home nav-icon', 'controllers/engine/dashboard', 0, 1, 101),
(2, 29, 0, 'icon-sitemap', 'controllers/engine/components', 1, 1, 101),
(3, 1, 1, 'icon-user', 'controllers/engine/Dashboard', 7, 1, 400),
(4, 38, 0, 'icon-group', 'controllers/engine/UsersGroup', 1, 1, 400),
(5, 38, 0, 'icon-user', 'controllers/engine/users', 3, 1, 400),
(7, 8, 0, 'icon-flag', 'controllers/engine/languages', 0, 1, 400),
(8, 1, 1, 'icon-flag', 'controllers/engine/dashboard', 3, 1, 200),
(9, 8, 0, 'icon-flag', 'controllers/engine/translations', 0, 1, 300),
(10, 1, 1, 'icon-wrench', 'controllers/engine/dashboard', 8, 1, 200),
(11, 10, 0, 'icon-building', 'controllers/engine/content/type', 0, 1, 200),
(12, 10, 0, 'icon-building', 'controllers/engine/content/templates', 0, 1, 400),
(13, 8, 0, 'icon-book', 'controllers/engine/guides', 4, 1, 300),
(14, 10, 0, 'icon-code', 'controllers/engine/content/chunks', 0, 1, 300),
(15, 10, 0, 'icon-random', 'controllers/engine/routers', 0, 1, 500),
(16, 1, 0, 'icon-folder-open', 'controllers/engine/pages', 1, 1, 200),
(17, 10, 0, 'icon-crop', 'controllers/engine/content/images/sizes', 0, 1, 200),
(22, 10, 0, 'icon-list', 'controllers/engine/features', 0, 1, 200),
(23, 10, 0, 'icon-desktop', 'controllers/engine/themes', 0, 1, 100),
(24, 10, 0, 'icon-th-list', 'controllers/engine/nav', 0, 1, 200),
(25, 8, 0, 'icon-list-alt', 'controllers/engine/emailTemplates', 0, 1, 400),
(26, 29, 0, 'icon-puzzle-piece', 'controllers/engine/modules', 4, 1, 400),
(27, 1, 0, 'icon-cog', 'controllers/engine/settings', 9, 1, 800),
(29, 1, 1, 'icon-puzzle-piece', 'controllers/engine/dashboard', 5, 1, 300),
(30, 29, 0, 'icon-puzzle-piece', 'controllers/engine/plugins', 0, 1, 300),
(31, 1, 1, 'icon-shopping-cart', 'controllers/engine/Component', 2, 1, 300),
(32, 31, 0, 'icon-book', 'controllers/engine/categories', 1, 1, 300),
(33, 31, 0, 'icon-suitcase', 'controllers/engine/manufacturers', 2, 1, 300),
(34, 31, 0, 'icon-tablet', 'controllers/engine/products', 3, 1, 300),
(35, 31, 0, 'icon-money', 'controllers/engine/currency', 4, 1, 300),
(36, 3, 0, 'icon-user', 'controllers/engine/CustomersGroup', 1, 1, 300),
(37, 3, 0, 'icon-user', 'controllers/engine/Customers', 2, 1, 300),
(38, 1, 1, 'icon-group', 'controllers/engine/Dashboard', 6, 1, 300),
(39, 31, 0, 'icon-barcode', 'controllers/engine/PromoCodes', 5, 1, 300),
(40, 1, 1, 'icon-book', 'controllers/engine/Dashboard', 2, 1, 300),
(41, 40, 0, 'icon-file-text-alt', 'controllers/engine/Posts', 1, 1, 300),
(42, 1, 0, 'icon-comment', 'controllers/engine/Comments', 4, 1, 300),
(43, 31, 0, 'icon-suitcase', 'controllers/engine/Delivery', 6, 1, 300),
(44, 31, 0, 'icon-credit-card', 'controllers/engine/Payment', 6, 1, 300),
(45, 10, 0, 'icon-download', 'controllers/engine/Backup', 8, 1, 300);

DROP TABLE IF EXISTS `components_info`;
CREATE TABLE IF NOT EXISTS `components_info` (
  `id` smallint(11) unsigned NOT NULL AUTO_INCREMENT,
  `components_id` tinyint(3) unsigned NOT NULL,
  `languages_id` tinyint(3) unsigned NOT NULL,
  `name` varchar(60) NOT NULL,
  `description` tinytext,
  PRIMARY KEY (`id`),
  KEY `fk_components_idx` (`components_id`),
  KEY `fk_components_info_languages1_idx` (`languages_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=132 ;


INSERT INTO `components_info` (`id`, `components_id`, `languages_id`, `name`, `description`) VALUES
  (1, 1, 1, 'OYi.Engine 6.x ', 'OYi.Engine 6.x :: Система керування контентом'),
  (2, 2, 1, 'Список компонентів', 'Структура системи. Ви можете додавати / редагувати компоненти. Міняти права доступу і тд.'),
  (3, 3, 1, 'Клієнти', 'Містить список клієнтів'),
  (4, 4, 1, 'Групи', 'Список груп адміністраторів'),
  (5, 5, 1, 'Список ', 'Список адміністраторів'),
  (7, 7, 1, 'Мови', 'Список доступних мов системи'),
  (10, 8, 1, 'Локалізація', ''),
  (13, 9, 1, 'Переклади', 'Список ресурсних слів і текстів, які використовуються на сайті'),
  (18, 10, 1, 'Інструменти', 'Набір інструментів для роботи з сайтом'),
  (21, 11, 1, 'Типи і шаблони', 'Список типів контенту і дступні шаблони'),
  (24, 12, 1, 'Шаблони', 'Шаблони сторінок / категорій / товарів і т. д.'),
  (27, 13, 1, 'Довідники', ''),
  (30, 14, 1, 'Чанки', 'Шматки html коду. Ви можете вставляти їх в шаблони.'),
  (33, 15, 1, 'Редиректи', ''),
  (36, 16, 1, 'Сторінки', 'Список сторінок сайту'),
  (39, 17, 1, 'Розміри зображень', 'Список розмірів зображень'),
  (54, 22, 1, 'Динамічні параметри', 'Список паарметрів, якими можна розширити властивості товарів  / сторінок'),
  (55, 23, 1, 'Теми', 'Список тем сайту'),
  (56, 24, 1, 'Менеджер меню', 'Дозволяє генерувати навігаційне меню для сайту'),
  (57, 25, 1, 'Шаблони E-mail повідомлень', 'Сипсок шаблонів email повідомлень'),
  (58, 26, 1, 'Модулі', 'Список модулів'),
  (59, 27, 1, 'Налаштування ', 'Глобальні налаштування системи'),
  (115, 29, 1, 'Компоненти', 'Список компонентів'),
  (116, 30, 1, 'Плагіни', 'Плагіни це засоби, якими ви можете розширити базові параметри сторінки'),
  (117, 31, 1, 'Магазин', 'Каталог товарів та категорій'),
  (118, 32, 1, 'Категорії', 'Категорії товарів'),
  (119, 33, 1, 'Виробники', 'Список виробників'),
  (120, 34, 1, 'Товари', 'Список товарів'),
  (121, 35, 1, 'Валюти', 'Список валют'),
  (122, 36, 1, 'Групи клієнтів', 'Список груп клієнтів'),
  (123, 37, 1, 'Клієнти', ''),
  (124, 38, 1, 'Адміністратори', 'Адміністратори'),
  (125, 39, 1, 'Промо коди', 'Промо коди на знижки'),
  (126, 40, 1, 'Блог', 'Блог з категоріями та мітками'),
  (127, 41, 1, 'Статті', 'Список публікацій'),
  (128, 42, 1, 'Коментарі', 'коментарі'),
  (129, 43, 1, 'Доставка', 'Способи достакви'),
  (130, 44, 1, 'Оплата', 'Способи оплати'),
  (131, 45, 1, 'Резервне копіювання БД', 'Резервне копіювання та відновлення БД');

DROP TABLE IF EXISTS `content`;
CREATE TABLE IF NOT EXISTS `content` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) unsigned NOT NULL,
  `isfolder` tinyint(1) unsigned NOT NULL,
  `type_id` tinyint(3) unsigned NOT NULL,
  `owner_id` int(11) unsigned NOT NULL,
  `templates_id` tinyint(3) unsigned NOT NULL,
  `published` tinyint(1) unsigned NOT NULL,
  `module` varchar(60) NOT NULL,
  `sort` smallint(5) unsigned NOT NULL,
  `createdon` datetime NOT NULL,
  `editedon` datetime NOT NULL,
  `auto` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`,`type_id`,`owner_id`,`templates_id`),
  KEY `xindex` (`parent_id`,`isfolder`,`published`,`sort`,`auto`),
  KEY `fk_content_content_type1_idx` (`type_id`),
  KEY `fk_content_users1_idx` (`owner_id`),
  KEY `fk_content_content_templates1_idx` (`templates_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `content_images`;
CREATE TABLE IF NOT EXISTS `content_images` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `content_id` int(11) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort` tinyint(5) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_content_images_content1_idx` (`content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `content_images_info`;
CREATE TABLE IF NOT EXISTS `content_images_info` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `images_id` int(11) unsigned NOT NULL,
  `languages_id` tinyint(3) unsigned NOT NULL,
  `alt` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `languages_id` (`languages_id`),
  KEY `fk_images_info_content_images1_idx` (`images_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `content_info`;
CREATE TABLE IF NOT EXISTS `content_info` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `content_id` int(11) unsigned NOT NULL,
  `languages_id` tinyint(3) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `content` longtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `languages_id` (`languages_id`,`alias`),
  KEY `fk_content_info_content1_idx` (`content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `content_type`;
CREATE TABLE IF NOT EXISTS `content_type` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(45) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `type_UNIQUE` (`type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

INSERT INTO `content_type` (`id`, `type`, `name`) VALUES
  (1, 'page', 'Сторінки'),
  (3, 'category', 'Категорії товарів'),
  (4, 'manufacturer', 'Виробники'),
  (5, 'product', 'Товар'),
  (8, 'post', 'Post'),
  (9, 'PostsCategories', 'PostsCategories');

DROP TABLE IF EXISTS `content_templates`;
CREATE TABLE IF NOT EXISTS `content_templates` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `type_id` tinyint(3) unsigned NOT NULL,
  `path` varchar(30) DEFAULT NULL,
  `name` varchar(60) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `main` tinyint(1) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`,`type_id`),
  KEY `fk_content_templates_content_type1_idx` (`type_id`),
  KEY `main` (`main`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

INSERT INTO `content_templates` (`id`, `type_id`, `path`, `name`, `description`, `main`) VALUES
  (1, 1, 'default', 'Default', 'Сторінка по замовчуванню', 1),
  (3, 3, 'default', 'Default', 'Default template', 1),
  (4, 4, 'default', 'Default', 'Default template', 1),
  (5, 5, 'default', 'Default', 'Default template', 1),
  (8, 9, 'default', 'Default', 'Default template', 1),
  (9, 8, 'default', 'Default', 'Шаблон статті по замовчуванню', 1);

DROP TABLE IF EXISTS `content_type_images`;
CREATE TABLE IF NOT EXISTS `content_type_images` (
  `id` tinyint(5) unsigned NOT NULL AUTO_INCREMENT,
  `content_type_id` tinyint(3) unsigned NOT NULL,
  `images_sizes_id` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`,`content_type_id`,`images_sizes_id`),
  KEY `fk_content_type_images_content_type1_idx` (`content_type_id`),
  KEY `fk_content_type_images_images_sizes1_idx` (`images_sizes_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `currency`;
CREATE TABLE IF NOT EXISTS `currency` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `symbol` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rate` decimal(7,3) DEFAULT NULL,
  `is_main` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `code` (`code`),
  KEY `is_main` (`is_main`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

INSERT INTO `currency` (`id`, `name`, `code`, `symbol`, `rate`, `is_main`) VALUES
(1, 'Долар', 'USD', '$', 1.000, 1);

DROP TABLE IF EXISTS `dashboard_quick_launch`;
CREATE TABLE IF NOT EXISTS `dashboard_quick_launch` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `components_id` tinyint(3) unsigned NOT NULL,
  `sort` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`,`components_id`),
  KEY `fk_dashboard_quick_launch_components1_idx` (`components_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

INSERT INTO `dashboard_quick_launch` (`id`, `components_id`, `sort`) VALUES
(1, 9, 0),
(2, 16, 1),
(3, 32, 2),
(4, 33, 3),
(5, 34, 4),
(6, 41, 5),
(7, 42, 6);

DROP TABLE IF EXISTS `delivery`;
CREATE TABLE IF NOT EXISTS `delivery` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `free_from` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `published` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `sort` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `delivery_info`;
CREATE TABLE IF NOT EXISTS `delivery_info` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `delivery_id` tinyint(3) unsigned NOT NULL,
  `languages_id` tinyint(3) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`,`delivery_id`,`languages_id`),
  KEY `fk_delivery_info_delivery1_idx` (`delivery_id`),
  KEY `fk_delivery_info_languages1_idx` (`languages_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `delivery_payment`;
CREATE TABLE IF NOT EXISTS `delivery_payment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `delivery_id` tinyint(3) unsigned NOT NULL,
  `payment_id` tinyint(3) unsigned NOT NULL,
  `sort` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`,`delivery_id`,`payment_id`),
  UNIQUE KEY `delivery_id` (`delivery_id`,`payment_id`),
  KEY `fk_delivery_payment_delivery1_idx` (`delivery_id`),
  KEY `fk_delivery_payment_payment1_idx` (`payment_id`),
  KEY `sort` (`sort`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `email_templates`;
CREATE TABLE IF NOT EXISTS `email_templates` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(45) NOT NULL,
  `name` varchar(60) NOT NULL,
  `email` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code_UNIQUE` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `email_templates_info`;
CREATE TABLE IF NOT EXISTS `email_templates_info` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `templates_id` tinyint(3) unsigned NOT NULL,
  `languages_id` tinyint(3) unsigned NOT NULL,
  `reply_to` varchar(60) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `body` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_email_templates_info_email_templates1_idx` (`templates_id`),
  KEY `fk_email_templates_info_languages1_idx` (`languages_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `guides`;
CREATE TABLE IF NOT EXISTS `guides` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) unsigned NOT NULL,
  `sort` tinyint(3) unsigned NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`),
  KEY `sort` (`sort`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `guides_info`;
CREATE TABLE IF NOT EXISTS `guides_info` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `guides_id` int(11) unsigned NOT NULL,
  `languages_id` tinyint(3) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`,`guides_id`,`languages_id`),
  KEY `fk_guides_info_languages2_idx` (`languages_id`),
  KEY `fk_guides_info_guides2_idx` (`guides_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `images_sizes`;
CREATE TABLE IF NOT EXISTS `images_sizes` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(16) NOT NULL,
  `width` int(5) unsigned NOT NULL,
  `height` int(5) unsigned NOT NULL,
  `crop` tinyint(1) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `image_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `languages`;
CREATE TABLE IF NOT EXISTS `languages` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `code` char(2) NOT NULL,
  `name` varchar(30) NOT NULL,
  `front` tinyint(1) unsigned DEFAULT '0',
  `back` tinyint(1) unsigned DEFAULT '0',
  `front_default` tinyint(1) unsigned DEFAULT '0',
  `back_default` tinyint(1) unsigned DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `code_UNIQUE` (`code`),
  KEY `def` (`front`,`back`,`back_default`,`front_default`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

INSERT INTO `languages` (`id`, `code`, `name`, `front`, `back`, `front_default`, `back_default`) VALUES
  (1, 'uk', 'Українська', 1, 1, 1, 1);

DROP TABLE IF EXISTS `modules`;
CREATE TABLE IF NOT EXISTS `modules` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `module` varchar(60) NOT NULL,
  `settings` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `module_UNIQUE` (`module`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `nav_menu`;
CREATE TABLE IF NOT EXISTS `nav_menu` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `auto_add_pages` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `auto_add_pages` (`auto_add_pages`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

INSERT INTO `nav_menu` (`id`, `name`, `auto_add_pages`) VALUES
(1, 'Default', 0);

DROP TABLE IF EXISTS `nav_menu_items`;
CREATE TABLE IF NOT EXISTS `nav_menu_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nav_menu_id` tinyint(3) unsigned NOT NULL,
  `content_id` int(11) unsigned NOT NULL,
  `sort` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`,`nav_menu_id`,`content_id`),
  UNIQUE KEY `nav_menu_id` (`nav_menu_id`,`content_id`),
  KEY `fk_nav_menu_items_nav_menu1_idx` (`nav_menu_id`),
  KEY `fk_nav_menu_items_content1_idx` (`content_id`),
  KEY `sort` (`sort`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `payment`;
CREATE TABLE IF NOT EXISTS `payment` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `currency_id` tinyint(3) unsigned NOT NULL,
  `published` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `module` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `sort` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`currency_id`),
  KEY `fk_payment_currency1_idx` (`currency_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `payment_info`;
CREATE TABLE IF NOT EXISTS `payment_info` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `payment_id` tinyint(3) unsigned NOT NULL,
  `languages_id` tinyint(3) unsigned NOT NULL,
  `name` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`,`payment_id`,`languages_id`),
  KEY `fk_payment_info_payment1_idx` (`payment_id`),
  KEY `fk_payment_info_languages1_idx` (`languages_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `plugins`;
CREATE TABLE IF NOT EXISTS `plugins` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `controller` varchar(100) CHARACTER SET utf8 NOT NULL,
  `position` enum('left','right') CHARACTER SET utf8 NOT NULL DEFAULT 'right',
  `settings` text CHARACTER SET utf8,
  PRIMARY KEY (`id`),
  UNIQUE KEY `controller` (`controller`),
  KEY `position` (`position`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

INSERT INTO `plugins` (`id`, `controller`, `position`, `settings`) VALUES
(1, '\\controllers\\engine\\plugins\\ProductsVariants', 'left', NULL),
(2, '\\controllers\\engine\\plugins\\ProductsRelated', 'right', NULL),
(3, '\\controllers\\engine\\plugins\\Tags', 'right', NULL),
(4, '\\controllers\\engine\\plugins\\Comments', 'right', NULL);

DROP TABLE IF EXISTS `plugins_structure`;
CREATE TABLE IF NOT EXISTS `plugins_structure` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `components_id` tinyint(3) unsigned NOT NULL,
  `plugins_id` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`,`components_id`,`plugins_id`),
  KEY `fk_plugins_structure_components1_idx` (`components_id`),
  KEY `fk_plugins_structure_plugins_idx` (`plugins_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

INSERT INTO `plugins_structure` (`id`, `components_id`, `plugins_id`) VALUES
(1, 34, 1),
(2, 34, 2),
(7, 34, 4),
(4, 41, 3),
(6, 41, 4);

DROP TABLE IF EXISTS `products_categories`;
CREATE TABLE IF NOT EXISTS `products_categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cid` int(11) unsigned NOT NULL,
  `pid` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_category_products_content1_idx` (`cid`),
  KEY `fk_category_products_content2_idx` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `products_options`;
CREATE TABLE IF NOT EXISTS `products_options` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `products_id` int(11) unsigned NOT NULL,
  `manufacturers_id` int(11) unsigned NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `availability` tinyint(1) unsigned NOT NULL,
  `quantity` tinyint(3) unsigned NOT NULL,
  `price` decimal(10,2) unsigned NOT NULL,
  `sale` tinyint(1) unsigned NOT NULL,
  `hit` tinyint(1) unsigned NOT NULL,
  `new` tinyint(1) unsigned NOT NULL,
  `sort` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `is_variant` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`products_id`,`manufacturers_id`),
  KEY `manufacturers_id` (`manufacturers_id`),
  KEY `price` (`price`),
  KEY `sale` (`sale`),
  KEY `hit` (`hit`),
  KEY `new` (`new`),
  KEY `code` (`code`),
  KEY `fk_products_idx` (`products_id`),
  KEY `is_variant` (`is_variant`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `products_related`;
CREATE TABLE IF NOT EXISTS `products_related` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `products_id` int(11) unsigned NOT NULL,
  `related_id` int(11) unsigned NOT NULL,
  `sort` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`,`products_id`),
  UNIQUE KEY `products_id` (`products_id`,`related_id`),
  KEY `fk_products_related_content1_idx` (`products_id`),
  KEY `fk_products_related_content2_idx` (`related_id`),
  KEY `sort` (`sort`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `promo_codes`;
CREATE TABLE IF NOT EXISTS `promo_codes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `expire` date DEFAULT NULL,
  `discount` decimal(10,2) DEFAULT NULL,
  `type` enum('abs','per') COLLATE utf8_unicode_ci DEFAULT 'abs',
  `minp` decimal(10,2) DEFAULT NULL,
  `single` tinyint(1) DEFAULT '0',
  `usages` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code_UNIQUE` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `routers`;
CREATE TABLE IF NOT EXISTS `routers` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `url_from` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url_to` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `header` enum('HTTP/1.1 301 Moved Permanently','HTTP/1.1 307 Temporary Redirect','HTTP/1.1 302 Moved Temporarily') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'HTTP/1.1 301 Moved Permanently',
  PRIMARY KEY (`id`),
  UNIQUE KEY `url_from` (`url_from`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

DROP TABLE IF EXISTS `settings`;
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `value` varchar(45) NOT NULL,
  `description` varchar(160) DEFAULT NULL,
  `autoload` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uname` (`name`),
  KEY `autoload` (`autoload`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=29 ;

INSERT INTO `settings` (`id`, `name`, `value`, `description`, `autoload`) VALUES
(1, 'autofil_title', '1', 'Автоматично заповнювати title в стоірнках на основі назви', 1),
(2, 'autotranslit_alias', '1', 'Автоматично генерувати аліас', 1),
(3, 'editor_language', 'uk', 'Мова редактора по замовчуванню', 1),
(4, 'editor_bodyId', 'cms_content', 'ID body в шаблоні ', 1),
(5, 'editor_bodyClass', 'cms-content', 'Клас контенту для редкатора', 1),
(6, 'editor_contentsCss', 'css/style.css', 'Шлях до стилів сайту, можна задавати через кому декілька', 1),
(7, 'products_list_update_price', '1', 'Оновляти ціни в списку товарів\r\n', 1),
(8, 'languages_create_info', '1', 'При створенні мови, автоматично згенерувати переклади у всіх розділах системи', 1),
(9, 'app_theme_current', 'default', 'Тема по замовчуванню для сайту', 1),
(10, 'app_views_path', 'views/', 'Шлях до шаблонів', 1),
(11, 'app_chunks_path', 'chunks/', 'Шлях до чанків', 1),
(12, 'themes_path', 'themes/', 'Папка з темами', 1),
(13, 'content_images_dir', '/uploads/content/', 'Шлях до зображень', 1),
(14, 'content_images_thumb_dir', 'thumbnails/', 'Шлях до превюшок', 1),
(15, 'content_images_source_dir', 'source/', 'Шлях до sources', 1),
(16, 'translator', 'google', 'Google or yandex', 1),
(17, 'engine_theme_current', 'engine', 'Активна тема engine', 1),
(18, 'mod_path', '\\controllers\\modules\\', 'Абсолютний шлях до модулів ', 1),
(19, 'page_404', '12', 'Ід сторінки 404', 1),
(20, 'img_source_size', '1600x1200', 'Розмір зображень source по замовчуванню', 1),
(21, 'et_header', '', 'Глобальний header ', 1),
(22, 'et_footer', '', 'Глобальний footer', 1),
(23, 'google_ananytics_id', '', 'Google Analytics ID', 1),
(24, 'google_webmaster_id', '', 'G.Webmaster ID', 1),
(25, 'yandex_webmaster_id', '', 'Yandex Webmaster ID', 1),
(26, 'yandex_metrika', '', 'Yandex Metrika ID', 1),
(27, 'version', '6.01.02', 'Версія змін адра.бд.системи', 1),
(28, 'version_update', '1', 'Автоматичне оновлення версій системи', 1);

DROP TABLE IF EXISTS `tags`;

CREATE TABLE IF NOT EXISTS `tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `languages_id` tinyint(3) unsigned NOT NULL,
  `name` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `languages_id_2` (`languages_id`,`alias`),
  UNIQUE KEY `languages_id` (`languages_id`,`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `tags_content`;
CREATE TABLE IF NOT EXISTS `tags_content` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content_id` int(11) unsigned NOT NULL,
  `tags_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`,`content_id`,`tags_id`),
  UNIQUE KEY `content_id` (`content_id`,`tags_id`),
  KEY `fk_tags_content_content1_idx` (`content_id`),
  KEY `fk_tags_content_tags1_idx` (`tags_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `translations`;
CREATE TABLE IF NOT EXISTS `translations` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(60) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `translations_info`;
CREATE TABLE IF NOT EXISTS `translations_info` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `translations_id` int(11) unsigned NOT NULL,
  `languages_id` tinyint(3) unsigned NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_translations_info_translations1_idx` (`translations_id`),
  KEY `fk_translations_info_languages1_idx` (`languages_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `users_group_id` tinyint(3) unsigned NOT NULL,
  `languages_id` tinyint(3) unsigned NOT NULL,
  `sessid` char(35) NOT NULL,
  `address` varchar(100) NOT NULL,
  `name` varchar(60) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(60) NOT NULL,
  `password` varchar(35) NOT NULL,
  `skey` varchar(35) NOT NULL,
  `createdon` datetime NOT NULL,
  `editetedon` datetime NOT NULL,
  PRIMARY KEY (`id`,`users_group_id`,`languages_id`),
  UNIQUE KEY `email` (`email`),
  KEY `fk_users_users_group1_idx` (`users_group_id`),
  KEY `fk_users_languages1_idx` (`languages_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `users_group`;
CREATE TABLE IF NOT EXISTS `users_group` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` tinyint(3) unsigned NOT NULL,
  `rang` smallint(3) unsigned NOT NULL,
  `sort` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pid` (`parent_id`),
  KEY `sort` (`sort`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

INSERT INTO `users_group` (`id`, `parent_id`, `rang`, `sort`) VALUES
(1, 0, 999, 0);

DROP TABLE IF EXISTS `users_group_info`;
CREATE TABLE IF NOT EXISTS `users_group_info` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `users_group_id` tinyint(3) unsigned NOT NULL,
  `languages_id` tinyint(3) unsigned NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` tinytext,
  PRIMARY KEY (`id`),
  KEY `fk_users_group_info_users_group1_idx` (`users_group_id`),
  KEY `fk_users_group_info_languages1_idx` (`languages_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `features`;
CREATE TABLE IF NOT EXISTS `features` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('text','checkbox','radiogroup','textarea','checkboxgroup','select','sm','number') NOT NULL DEFAULT 'text',
  `required` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `published` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `show_list` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `show_compare` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `show_filter` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `auto` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `owner_id` int(11) unsigned NOT NULL,
  `extends` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `auto` (`auto`,`owner_id`),
  KEY `owner_id` (`owner_id`),
  KEY `extends` (`extends`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `features_info`;
CREATE TABLE IF NOT EXISTS `features_info` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `features_id` int(11) unsigned NOT NULL,
  `languages_id` tinyint(3) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`,`features_id`),
  KEY `fk_features_info_languages1_idx` (`languages_id`),
  KEY `fk_features_info_features1_idx` (`features_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `content_features`;
CREATE TABLE IF NOT EXISTS `content_features` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `features_id` int(11) unsigned NOT NULL,
  `content_id` int(11) unsigned NOT NULL,
  `sort` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`,`features_id`),
  UNIQUE KEY `features_id` (`features_id`,`content_id`),
  KEY `fk_content_features_content1_idx` (`content_id`),
  KEY `sort` (`sort`),
  KEY `fk_content_features_features1_idx` (`features_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `content_type_features`;
CREATE TABLE IF NOT EXISTS `content_type_features` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `features_id` int(11) unsigned NOT NULL,
  `content_type_id` tinyint(3) unsigned NOT NULL,
  `sort` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`,`features_id`,`content_type_id`),
  KEY `fk_content_type_features_features1_idx` (`features_id`),
  KEY `fk_content_type_features_content_type1_idx` (`content_type_id`),
  KEY `sort` (`sort`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `features_content_values`;
CREATE TABLE IF NOT EXISTS `features_content_values` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `features_id` int(11) unsigned NOT NULL,
  `content_id` int(11) unsigned NOT NULL,
  `languages_id` tinyint(3) unsigned NOT NULL,
  `features_values_id` int(11) unsigned NOT NULL,
  `value` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`,`features_id`,`content_id`,`languages_id`,`features_values_id`),
  KEY `fk_features_content_values_content1_idx` (`content_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `features_values`;
CREATE TABLE IF NOT EXISTS `features_values` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `features_id` int(11) unsigned NOT NULL,
  `sort` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`,`features_id`),
  KEY `fk_features_values_features1_idx` (`features_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `features_values_info`;
CREATE TABLE IF NOT EXISTS `features_values_info` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `values_id` int(11) unsigned NOT NULL,
  `languages_id` tinyint(3) unsigned NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`,`values_id`,`languages_id`),
  KEY `fk_features_info_features_values1_idx` (`values_id`),
  KEY `fk_features_info_languages1_idx` (`languages_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

ALTER TABLE `content_features`
ADD CONSTRAINT `fk_content_features_content10` FOREIGN KEY (`content_id`) REFERENCES `content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_content_features_features1` FOREIGN KEY (`features_id`) REFERENCES `features` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `content_type_features`
ADD CONSTRAINT `fk_content_type_features_1` FOREIGN KEY (`content_type_id`) REFERENCES `content_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_content_type_features_features1` FOREIGN KEY (`features_id`) REFERENCES `features` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `features`
ADD CONSTRAINT `fk_options_users10` FOREIGN KEY (`owner_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `features_content_values`
ADD CONSTRAINT `fk_features_content_values_content1` FOREIGN KEY (`content_id`) REFERENCES `content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `features_info`
ADD CONSTRAINT `fk_features_info_features1` FOREIGN KEY (`features_id`) REFERENCES `features` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_features_info_languages10` FOREIGN KEY (`languages_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `features_values`
ADD CONSTRAINT `fk_features_values_features1` FOREIGN KEY (`features_id`) REFERENCES `features` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `features_values_info`
ADD CONSTRAINT `fk_features_info_features_values1` FOREIGN KEY (`values_id`) REFERENCES `features_values` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_features_info_languages1` FOREIGN KEY (`languages_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `users_group_info`
ADD CONSTRAINT `fk_users_group_info_languages1` FOREIGN KEY (`languages_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_users_group_info_users_group1` FOREIGN KEY (`users_group_id`) REFERENCES `users_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `posts_categories`
  ADD CONSTRAINT `fk_posts_categories_content1` FOREIGN KEY (`cid`) REFERENCES `content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_posts_categories_content2` FOREIGN KEY (`pid`) REFERENCES `content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `comments`
  ADD CONSTRAINT `fk_comments_content1` FOREIGN KEY (`content_id`) REFERENCES `content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `components_info`
  ADD CONSTRAINT `fk_components_info_components1` FOREIGN KEY (`components_id`) REFERENCES `components` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_components_info_languages1` FOREIGN KEY (`languages_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `content`
  ADD CONSTRAINT `fk_content_content_templates1` FOREIGN KEY (`templates_id`) REFERENCES `content_templates` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_content_content_type1` FOREIGN KEY (`type_id`) REFERENCES `content_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_content_users1` FOREIGN KEY (`owner_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `content_images`
  ADD CONSTRAINT `fk_content_images_content1` FOREIGN KEY (`content_id`) REFERENCES `content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `content_images_info`
  ADD CONSTRAINT `fk_images_info_content_images1` FOREIGN KEY (`images_id`) REFERENCES `content_images` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_images_info_languages1` FOREIGN KEY (`languages_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `content_info`
  ADD CONSTRAINT `fk_content_info_content1` FOREIGN KEY (`content_id`) REFERENCES `content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_content_info_languages1` FOREIGN KEY (`languages_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `content_templates`
  ADD CONSTRAINT `fk_content_templates_content_type1` FOREIGN KEY (`type_id`) REFERENCES `content_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `content_type_images`
  ADD CONSTRAINT `fk_content_type_images_content_type1` FOREIGN KEY (`content_type_id`) REFERENCES `content_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_content_type_images_images_sizes1` FOREIGN KEY (`images_sizes_id`) REFERENCES `images_sizes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `dashboard_quick_launch`
  ADD CONSTRAINT `fk_dashboard_quick_launch_components1` FOREIGN KEY (`components_id`) REFERENCES `components` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `delivery_info`
  ADD CONSTRAINT `fk_delivery_info_delivery1` FOREIGN KEY (`delivery_id`) REFERENCES `delivery` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_delivery_info_languages1` FOREIGN KEY (`languages_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `delivery_payment`
  ADD CONSTRAINT `fk_delivery_payment_delivery1` FOREIGN KEY (`delivery_id`) REFERENCES `delivery` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_delivery_payment_payment1` FOREIGN KEY (`payment_id`) REFERENCES `payment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `email_templates_info`
  ADD CONSTRAINT `fk_email_templates_info_email_templates1` FOREIGN KEY (`templates_id`) REFERENCES `email_templates` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_email_templates_info_languages1` FOREIGN KEY (`languages_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `guides_info`
  ADD CONSTRAINT `fk_guides_info_guides2` FOREIGN KEY (`guides_id`) REFERENCES `guides` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_guides_info_languages2` FOREIGN KEY (`languages_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `nav_menu_items`
  ADD CONSTRAINT `fk_nav_menu_items_content1` FOREIGN KEY (`content_id`) REFERENCES `content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_nav_menu_items_nav_menu1` FOREIGN KEY (`nav_menu_id`) REFERENCES `nav_menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `payment`
  ADD CONSTRAINT `fk_payment_currency1` FOREIGN KEY (`currency_id`) REFERENCES `currency` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `payment_info`
  ADD CONSTRAINT `fk_payment_info_languages1` FOREIGN KEY (`languages_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_payment_info_payment1` FOREIGN KEY (`payment_id`) REFERENCES `payment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `plugins_structure`
  ADD CONSTRAINT `fk_plugins_structure_components1` FOREIGN KEY (`components_id`) REFERENCES `components` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_plugins_structure_plugins` FOREIGN KEY (`plugins_id`) REFERENCES `plugins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `products_categories`
  ADD CONSTRAINT `fk_category_products_content1` FOREIGN KEY (`cid`) REFERENCES `content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_category_products_content2` FOREIGN KEY (`pid`) REFERENCES `content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `products_options`
  ADD CONSTRAINT `fk_products_options_content1` FOREIGN KEY (`products_id`) REFERENCES `content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `products_related`
  ADD CONSTRAINT `fk_products_related_content1` FOREIGN KEY (`products_id`) REFERENCES `content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_products_related_content2` FOREIGN KEY (`related_id`) REFERENCES `content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `tags`
  ADD CONSTRAINT `fk_languages_id1` FOREIGN KEY (`languages_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `tags_content`
  ADD CONSTRAINT `fk_tags_content_content1` FOREIGN KEY (`content_id`) REFERENCES `content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_tags_content_tags1` FOREIGN KEY (`tags_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `translations_info`
  ADD CONSTRAINT `fk_translations_info_languages1` FOREIGN KEY (`languages_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_translations_info_translations1` FOREIGN KEY (`translations_id`) REFERENCES `translations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `users`
  ADD CONSTRAINT `fk_users_languages1` FOREIGN KEY (`languages_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_users_users_group1` FOREIGN KEY (`users_group_id`) REFERENCES `users_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */
