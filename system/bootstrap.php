<?php
/**
 * OYiEngine 6.x
 * Company Otakoyi.com
 * Author wmgodyak mailto:wmgodyak@gmail.com
 * Date: 03.05.14 13:36
 */

    if ( !defined('SYSPATH') ) die();

    define('MODELSPATH', 'models\\');

$protocol = !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https://' : 'http://';

define('IS_SSL', $protocol == 'https://');
define('APPURL', $protocol . "{$_SERVER['HTTP_HOST']}/");
define('HOTWEDURL', $protocol . "hotwed.com.ua/");
//echo phpversion();var_dump(version_compare(phpversion(), '5.3.0', '<')); die();
if (version_compare(phpversion(), '5.3.0', '<') == true) { die ('PHP >= 5.3 Only'); }
/*
* Set the server timezone
* see: http://us3.php.net/manual/en/timezones.php
*/
date_default_timezone_set("Europe/Kiev");

/*
* Set everything to UTF-8
*/
setlocale(LC_ALL, 'uk_UA.utf-8');
iconv_set_encoding("internal_encoding", "UTF-8");
mb_internal_encoding('UTF-8');


if (!ini_get('zlib.output_compression') && function_exists('ob_gzhandler')) ob_start('ob_gzhandler');

/*
* PHP error settings (Report all PHP errors)
*/
error_reporting(E_ALL);
ini_set('display_errors','On');

if ( !function_exists( 'array_partition' ) ) {
    function array_partition( $list, $p ) {
        $listlen = count( $list );
        $partlen = floor( $listlen / $p );
        $partrem = $listlen % $p;
        $partition = array();
        $mark = 0;
        for ($px = 0; $px < $p; $px++) {
            $incr = ($px < $partrem) ? $partlen + 1 : $partlen;
            $partition[$px] = array_slice( $list, $mark, $incr );
            $mark += $incr;
        }
        return $partition;
    }
}
if ( !function_exists( 'hex2bin' ) ) {
    function hex2bin( $str ) {
        $sbin = "";
        $len = strlen( $str );
        for ( $i = 0; $i < $len; $i += 2 ) {
            $sbin .= pack( "H*", substr( $str, $i, 2 ) );
        }

        return $sbin;
    }
}


require_once DOCROOT . "/controllers/core/Autoload.php";


// register autoload system controllers
//    $sysLoader = new Autoload('system\core', DOCROOT);
//    $sysLoader->register();

// register autoload app controllers
$appLoader = new Autoload('controllers',DOCROOT);
$appLoader->register();

// register autoload app models
$appLoader = new Autoload('models',DOCROOT);
$appLoader->register();

$sysLoader = new Autoload('vendor', DOCROOT);
$sysLoader->register();

// register autoload app modules
//    $appLoader = new Autoload('modules',DOCROOT);
//    $appLoader->register();

// init session
\controllers\core\Session::init();
header("Content-Type: text/html; charset=utf-8");

//    $skey = isset($_SESSION['skey']) ? $_SESSION['skey'] : md5(str_shuffle(chr(mt_rand(32, 126)) . uniqid() . microtime(TRUE)));
define('SKEY', md5(session_id()) );

// Маршрутизація

$router = new \controllers\core\Route($_SERVER['REQUEST_URI']);

/**
 * PHP as CGI
 */
if (get_magic_quotes_gpc()) {
    function antimagic($array)
    {
        return is_array($array) ? array_map('antimagic', $array) : stripslashes($array);
    }

    $_COOKIE = antimagic($_COOKIE);
    $_FILES = antimagic($_FILES);
    $_GET = antimagic($_GET);
    $_POST = antimagic($_POST);
    $_REQUEST = antimagic($_REQUEST);
}
/**
 * BACKEND
 */
$router ->set
(
    '/engine/content/images/([a-zA-Z_]+)/([a-zA-Z_]+)/?(.*)',
    'controllers\engine\content\images\:controller:action'
)

    //http://engine_6x.loc/engine/content/group/edit/3
    ->set('/engine/plugins/([a-zA-Z_]+)/([a-zA-Z_]+)/?(.*)', 'controllers\engine\plugins\:controller:action')
    ->set('/engine/content/([a-zA-Z_]+)/([a-zA-Z_]+)/?(.*)', 'controllers\engine\content\:controller:action')

    //http://engine_6x.loc/engine/users/group/edit/3
    ->set('/engine/users/([a-zA-Z_]+)/([a-zA-Z_]+)/?(.*)', 'controllers\engine\users\:controller:action')

    //http://engine_6x.loc/engine/structure/edit/3
    ->set('/engine/([a-zA-Z_]+)/([a-zA-Z_]+)/?(.*)', 'controllers\engine\:controller:action')

    //http://engine_6x.loc/engine/structure/edit
    ->set('/engine/([a-zA-Z_]+)/([a-zA-Z_]+)/?', 'controllers\engine\:controller:action')

    //http://engine_6x.loc/engine/structure
    ->set('/engine/([a-zA-Z_]+)/?', 'controllers\engine\:controller')

    //http://engine_6x.loc/engine/
    ->set('/engine/?', 'controllers\engine\Dashboard');

/**
 * FRONTEND
 */

// коли чітко задано які змінні мають бути

$router
//        ->set('/([a-z]{2})/forum/([a-zA-Z0-9-_]+)/([a-zA-Z0-9-_]+)/?', 'controllers\App', 'lang/category/post')
//        ->set('/forum/([a-zA-Z0-9-_]+)/([a-zA-Z0-9-_]+)/?', 'controllers\App', 'category/post')
    ->set('/([a-z]{2})/photo/([0-9]+)/?', 'controllers\modules\PhotoDay', 'lang/handle')
    ->set('/photo/([0-9]+)/?', 'controllers\modules\PhotoDay', 'handle')
    ->set('/forum/article/([0-9]+)/?', 'controllers\modules\Forum', 'handle')
    ->set('/vykonavets/([a-z0-9-]+)/?', 'controllers\modules\Parse', 'handle')
    ->set('/([a-z]{2})/ispolnitel/([a-z0-9-]+)/?', 'controllers\modules\Parse', 'lang/handle')

    ->set('/([a-z]{2})/([a-z0-9_\-\/]*\.[a-z0-9-_\.]*)/?([0-9]*)/?', 'controllers\modules\CityRouter', 'lang/handle/p')
    ->set('/([a-z]{2})/([a-z0-9_\-\/]*\.[a-z0-9-_\.]*)/?', 'controllers\modules\CityRouter', 'lang/handle')
    ->set('/([a-z0-9-_\/]*\.[a-z0-9-_\.]*)/?([0-9]*)/?', 'controllers\modules\CityRouter', 'handle/p')

    ->set('/sitemap/?(.*)', 'controllers\modules\XmlSitemap', 'index')
    ->set('/install/?', 'controllers\Install')
    ->set('/ajax/payment/([a-zA-Z0-9-_/]+)/([a-zA-Z0-9-_/]+)/?', 'controllers\modules\payment\:controller:action')
    ->set('/ajax/([a-zA-Z0-9-_/]+)/([a-zA-Z0-9-_/]+)/([0-9a-zA-Z]+)/?', 'controllers\modules\:controller:action:param')
    ->set('/ajax/([a-zA-Z0-9-_/]+)/([a-zA-Z0-9-_/]+)/?', 'controllers\modules\:controller:action')
    ->set('/route/([a-zA-Z0-9-_/]+)/([a-zA-Z0-9-_/]+)/?', 'controllers\modules\:controller:action')

    // posts tags
    ->set('/([a-z]{2})/([a-zA-Z0-9-_/]+)/tag/([0-9a-zA-Z]+)?', 'controllers\App', 'lang/alias/tag/tag')
    ->set('/([a-zA-Z0-9-_/]+)/tag/([0-9a-zA-Z]+)?', 'controllers\App', 'alias/tag/tag')


    ->set('/([a-z]{2})/([a-zA-Z0-9-_/]+)/([0-9]+)/?', 'controllers\App', 'lang/alias/p')
    ->set('/([a-zA-Z0-9-_/]+)/([0-9]+)/?', 'controllers\App', 'alias/p')
    ->set('/([a-z]{2})/([a-zA-Z0-9-_/]+)/?', 'controllers\App', 'lang/alias')
    ->set('/([a-z]{2})/?', 'controllers\App', 'lang')
    ->set('/([a-zA-Z0-9-_/]*)/?', 'controllers\App', 'alias');